/*
 * CTScript.cpp
 *
 *  Created on: 09.01.2012
 *      Author: Kulpanov
 */

#include <QtXML/QDomDocument>
#include <stdio.h>
#include <unistd.h>
#include <iostream>

#include "debug.h"
#include <math.h>
#include "dxe-ts-utils.h"

namespace DXE{

#define frequency 8000
double abs_double(double& _value){
  if(_value < 0) return -_value;
  return _value;
}

CWavTester::CWavTester(IWavStream* _stream_gauge, IWavStream* _stream_fortest, int _measure_time):
  stream_gauge(_stream_gauge),stream_fortest(_stream_fortest)
  ,max_error_Db(0)
//  ,gauge_min_Db(10000000.0),gauge_max_Db(-10000000.0)
  ,fortest_min_Db(10000000.0),fortest_max_Db(-10000000.0)
 {
  if(NULL == stream_gauge)
    throw E(0, "CWavTester:не создан опорный поток");
  if(NULL == stream_fortest)
    throw E(0, "CWavTester:не создан тестовый поток");

  //обрезаем с начала и с конца
  work_time = _measure_time - begin_offset - end_offset;
  if(work_time < 0) work_time = 0;
  saveTestStreamToFile = false;
 }

bool CWavTester::test(int _time_ms){
  LOG1(INFO, "WavTester:test(%d)\n", _time_ms);
  //...
  if(_time_ms <= 0)
    throw E(0, "Неположительное значение времени замера");

  int64_t idealAverage = 0;
  int64_t srcAverage = 0;
  //количество точек в промежуток времени
  int period = _time_ms * frequency / 1000;//частота 8000 - это 0,000125с
  double per_ms = (double)1000 / frequency;
  int curTime = 0;
  double timeRead = 0;
  while((timeRead < begin_offset + work_time)){
    if(stream_fortest->isEndOfStream()){
      throw E(0, "Поток для измерений преждевременно закончился");
    }
    CPacket_RTP* tmp_src = stream_fortest->read();
    CPacket_RTP* tmp_ideal = stream_gauge->read();
    int length = tmp_src->getLengthOfPayload();

    if(timeRead + per_ms * length < begin_offset){
      //отступ
      delete tmp_src;
    }else{
      int offset = 0;
      char* data_ideal = (char*)tmp_ideal->getDataOfPayload();
      char* data_src = (char*)tmp_src->getDataOfPayload();
      if(timeRead < begin_offset)
        offset = (begin_offset - timeRead) / per_ms;
      for(int i = offset; i < length; i++){
        curTime++;
        //сравниваем чтоб не привысить
        if(timeRead + per_ms * curTime > begin_offset + work_time){
          //выходим
          delete tmp_ideal;
          delete tmp_src;
          return true;
        };
//        short lCode = a2l(data_ideal[i]);
//        idealAverage += (lCode * lCode);
//
//        lCode = a2l(data_src[i]);
//        srcAverage += (lCode * lCode);
        //std::cout << lCode << ":" << (int)data_src[i] << " ";
        short lCode = a2l((int)data_ideal[i]);
        idealAverage += pow(lCode,2);
        lCode = a2l((int)data_src[i]);
        srcAverage += pow(lCode,2);
        
        if(curTime == period){
          //производим все измерения
          double idealDb, srcDb;
          if(idealAverage == 0){
            idealDb = -60;
          }else{
            double tmp = sqrt((double)idealAverage / curTime);
            idealDb = 20 * log10(tmp / 2015);//2015);
          }

          if(srcAverage == 0){
            srcDb = -60;
          }else{
            double tmp = sqrt((double)srcAverage / curTime);
            srcDb = 20 * log10(tmp / 2015);
            //std::cout << endl << tmp << " " << srcDb << " " << srcAverage << " " << lCode << " " << (int)data_src[i]<< endl;
          }
          double errDb = srcDb - idealDb;

          //LOG3(INFO, "WavTester:test: %f, %f, %f\n", srcDb, idealDb, errDb);
          if(fortest_min_Db > srcDb)
            fortest_min_Db = srcDb;
          if(fortest_max_Db < srcDb)
            fortest_max_Db = srcDb;

//          if(gauge_min_Db > idealDb)
//            gauge_min_Db = idealDb;
//          if(gauge_max_Db < idealDb)
//            gauge_max_Db = idealDb;

          if(abs_double(max_error_Db) < abs_double(errDb))
            max_error_Db = errDb;

          curTime = 0;
          idealAverage = 0;
          srcAverage = 0;
        };
      };
      delete tmp_ideal;
      delete tmp_src;
    }
    timeRead += (per_ms * length);
  }
  //будет остаток (наверно можно побрезговать)
  return true;
}

double getRMS(CPacket_RTP* packet){
  if(packet == NULL) return .0;
  int packet_length  = packet->getLengthOfPayload();
  if(packet_length == 0) return .0;

  const uint8_t* packet_data = packet->getDataOfPayload();
  if(packet_data == NULL) return .0;

  double rms = .0;
  for(int i=0; i<packet_length; ++i){
    int lCode = a2l(packet_data[i]);
    rms += (lCode * lCode);
  }
  rms /= packet_length;
  rms = sqrt(rms);
  return rms;
}

double getDb(double _value){
  if(_value < 0.001)
    return -60;
  return (log10(_value / 2015) * 20)-3;//todo: откуда взялась -3?
}

bool CWavTester::test2(int _measure_time){
  LOG1(INFO, "WavTester:test(%d)\n", _measure_time);
  //...
  if(_measure_time <= 0)
    throw E(0, "Неположительное значение время замера");

  //CPacket_RTP* tmp_src = stream_gauge->read();
  double idealDb = stream_gauge->getdB();//round(getDb(getRMS(tmp_src)));
  //delete tmp_src;
// std::cout << "idealDb=" << idealDb << std::endl;

  int curTime = 0;
  //пропускаем пакеты c тишиной..до начала данных для анализа
  while( curTime < begin_offset){
    CPacket_RTP* tmp_src = stream_fortest->read();
    int packet_length = tmp_src->getLengthOfPayload();
    if(packet_length == 0)
      packet_length = 60;
    curTime += (packet_length * ((double)1000 / frequency));
    delete tmp_src;
  }

  IWavStream* stream_forSave = NULL;
  if(saveTestStreamToFile){
    static int fnameCounter = 0;
    QString opt = QString("<item type=\"file_out\" filename=\"test_%0.wav\" />").arg(++fnameCounter);
    string strerr;
    stream_forSave = IWavStream::create(opt.toStdString(),strerr);
  }
  //анализируем данные
  int packet_count = 0;
  int analyse_period  = begin_offset + _measure_time - end_offset;
  while(curTime < analyse_period){
    CPacket_RTP* tmp_src = stream_fortest->read();
    int packet_length = tmp_src->getLengthOfPayload();
    if(packet_length == 0)
      packet_length = 60;
    curTime += (packet_length * ((double)1000 / frequency));
    double srcDb   = getDb(getRMS(tmp_src));
    double errDb = srcDb - idealDb;
//    std::cout << "srcDb=" << srcDb << " errDb=" << errDb << std::endl;

    if(fortest_min_Db > srcDb)  fortest_min_Db = srcDb;
    if(fortest_max_Db < srcDb) fortest_max_Db = srcDb;

    max_error_Db *= packet_count;
    max_error_Db += errDb;
    max_error_Db /= ++packet_count;
    if(stream_forSave)stream_forSave->write(tmp_src);
    delete tmp_src;
  }
  if(stream_forSave)stream_forSave->close();
  return true;
}


////////////////////////////////////////////////////////////////////////////////
//
CConnection::CConnection(ICallManager* m1, ICallManager* m2, CLogger_ptr _log){
  logger = _log;
  call_out = m1;
  call_in  = m2;
  LOG2(TSTL1, "Connection: ctor: call_out=0x%p, call_in=0x%p\n", call_out, call_in );
}

CConnection::~CConnection(){
  LOG2(TSTL1, "Connection: dtor: call_out=0x%p, call_in=0x%p\n", call_out, call_in );
  if(call_out)call_out->close();
  if(call_in)call_in->waitforClose(WSEC(2));// - неработат в параллели!

  if(call_out)call_out = call_out->release();
  if(call_in)call_in  = call_in->release();
}

////методы логгирования
static const QString msg_script = QString().fromUtf8("Сценарий '%0'");
static const QString msg_message= QString().fromUtf8("\nзавершён c сообщением:%0");
static const QString msg_act    = QString().fromUtf8("'%0'");
static const QString msg_params = QString().fromUtf8("\nс параметрами:%0");

const QString CLogger::str_empty = "";
const QString CLogger::str_dash = "_";

void CLogger::scriptBegin(const QString& _descr){
  //SMsg_log logmsg();
  logTo(SMsg_log(logId, msg_script.arg(_descr)
        , ILogListener::started, ILogListener::fulldate));
}

void CLogger::scriptFail(const QString& _descr, const QString& _msg){
  QString msg = msg_script.arg(_descr)
    + ((_msg.size() > 4)? msg_message.arg(_msg) : "");
  logTo(SMsg_log(logId, msg
         , ILogListener::fail, ILogListener::fulldate));
//
//  QString log_str = QString().fromUtf8("Сценарий '%0' завершился неудачно ")
//                  .arg(_descr);
//    log_str += QString().fromUtf8("c сообщением\n  %0")
//                    .arg(_msg);
//  //level --;
//  logTo(log_str, level + ILogListener::replace + ILogListener::red);
}

void CLogger::scriptPass(const QString& _descr){
  logTo(SMsg_log(logId, msg_script.arg(_descr)
        , ILogListener::pass, ILogListener::fulldate));
//  logTo(QString().fromUtf8("Сценарий '%0' выполнен").arg(_descr)
//      , level + ILogListener::replace);
}

void CLogger::actBegin(const QString& _descr, const QString& _params){
  if(_descr.size() == 0) return ;
  QString msg = msg_act.arg(_descr)
      + ((_params.size() != 0)?msg_params.arg(_params) : "");
  logTo(SMsg_log(logId, msg, ILogListener::started));
}

void CLogger::actPass(const QString& _descr, const QString& _correct_id){
  if(_descr.size() == 0) return ;
  logTo(SMsg_log(logId + _correct_id, msg_act.arg(_descr), ILogListener::pass));
//  QString log_str = QString().fromUtf8("Акт '%0' выполнен")
//                  .arg(_descr);
//  logTo(log_str, level + ILogListener::add);
}

void CLogger::actMessage(const QString& _msg){
  //logTo(_msg, level + ILogListener::add);

  logTo(SMsg_log(logId, _msg
        , ILogListener::msg));

}

void CLogger::actFail(const QString& _descr, const QString& _msg, const QString& _correct_id){
  if(_descr.size() == 0) return ;

  QString msg = msg_act.arg(_descr)
      + ((_msg.size() > 4)?msg_message.arg(_msg): "");
  logTo(SMsg_log(logId + _correct_id, msg, ILogListener::fail));
//
//  QString log_str = QString().fromUtf8("Акт '%0' завершился неудачно ")
//                  .arg(_descr);
//  if(_msg.size() > 4)
//    log_str += QString().fromUtf8("c сообщением\n  %0")
//                    .arg(_msg);
//  logTo(log_str, level + ILogListener::add + ILogListener::red);
}

CLogger_ptr CLogger::getChild(int _n){
  QString log_id = logId + '.' + QString().setNum(_n);
  return CLogger_ptr(new CLogger(listener, log_id));
}

//CLogger_ptr CLogger::nextStep(){
//  QStringList ids = _msg.id.split(".");//1.4.5.120
//
//
//  return CLogger_ptr(new CLogger(listener, log_id));
//}

};
