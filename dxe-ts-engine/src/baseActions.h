/*
 * baseActions.h
 *
 *  Created on: 05.02.2012
 *      Author: Kulpanov
 */

#ifndef BASEACTIONS_H_
#define BASEACTIONS_H_
#include "debug.h"

#include "dxe-ts-engine.h"
using namespace DXE;
namespace DXE{

/** \brief Акт: начало сценария
 */
class CTScriptAction_begin: public CTScriptAction {
  typedef CTScriptAction base;
public:
  virtual QString getDescr(){ return ""; };//размер!

  CTScriptAction_begin(const QDomElement& _e, CTScript* _s)
  :base(_e, _s, -1 ,0){
  }

  virtual ~CTScriptAction_begin(){

  }
public:
  virtual bool execute();
};

/** \brief Акт: конец сценария
 */
class CTScriptAction_end: public CTScriptAction {
  typedef CTScriptAction base;
public:
  virtual QString getDescr(){ return ""; };

public:
  CTScriptAction_end(const QDomElement& _e, CTScript* _s)
    :base(_e, _s, -1 ,0){
  }
  virtual ~CTScriptAction_end(){
  }
public:
  virtual bool execute();
};

/** \brief Акт: установить парное соединение
 * Делает исходящий вызов и ждет установления входящего вызова
 */
class CTScriptAction_connect: public CTScriptAction {
  typedef CTScriptAction base;
public:
  CTScriptAction_connect(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }

  virtual ~CTScriptAction_connect(){
  }
  virtual QString getParams(){
    QString target = source.attribute("target", "100");
    QString target_ip = source.attribute("target_ip", "127.0.0.1");
    return QString().fromUtf8("соед.номер %0, номер вызова %1, ip %2")
        .arg( id)
        .arg(source.attribute("target")
            , source.attribute("target_ip") );
  }

public:
  virtual bool execute();

//
//  virtual QString toString(){
//    return base::toString()
//            + ", connect, target=" + target + ", source=" + source;
//  }
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  }

private:
};

/** \brief Акт: установить непарное соединение
 * Делает исходящий вызов
 */
class CTScriptAction_connectHalf: public CTScriptAction {
  typedef CTScriptAction base;
public:
  CTScriptAction_connectHalf(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }

  virtual ~CTScriptAction_connectHalf(){
  }
  virtual QString getParams(){
    QString target = source.attribute("target", "100");
    QString target_ip = source.attribute("target_ip", "127.0.0.1");
    return QString().fromUtf8("соед.номер %0, номер вызова %1, ip %2")
        .arg( id)
        .arg(source.attribute("target")
            , source.attribute("target_ip") );
  }

public:
  virtual bool execute();

private:
};


/** \brief Акт: разъединить
 * Завершить открытые исходящие и входящие соединения.
 */
class CTScriptAction_disconnect: public CTScriptAction {
  typedef CTScriptAction base;
public:
  virtual bool mustBe_forStandalone(){return true; };
public:
  //virtual QString getDescr(){ return QString().fromUtf8("Прервать связь"); };

  CTScriptAction_disconnect(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){

  }

  virtual ~CTScriptAction_disconnect(){
  }
public:
  virtual bool execute();

//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", disconnect";
//  }
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  }
private:
};

/** \brief Акт: вызвать сценарий
 * Вызывает вложенный сценарий из файла сценария
 */
class CTScriptAction_call: public CTScriptAction {
  typedef CTScriptAction base;
public:
//  virtual QString getDescr(){
//    return QString().fromUtf8("Сценарий");
//  };

  virtual QString getParams(){
    return QString().fromUtf8("вызов %0")
        .arg( source.attribute("sname"));
  }

  CTScriptAction_call(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_call(){ }

public:
  virtual bool execute();

//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  };

private:
};

/** \brief Акт: установить транслятор звукового потока
 * Транслятором могут быть:
 * файл, генератор синуса
 */
class CTScriptAction_play: public CTScriptAction {
  typedef CTScriptAction base;
public:
  CTScriptAction_play(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_play(){ }

  virtual QString getParams(){

    QString sname = source.attribute("type");
    if(sname == "sin"){
      return QString().fromUtf8("соед.номер %0, со стороны %1, частота %2Гц, уровень(rms) %3дБ")
       .arg(id)
       .arg(source.attribute("dir")=="1"? input_side() : output_side())
       .arg(source.attribute("freq"), source.attribute("ampl"));
    }
    if(sname == "silence")
      return QString().fromUtf8("соед.номер %0, со стороны %1, тишина, уровень(rms) -60дБ")
          .arg(id)
          .arg(source.attribute("dir")=="1"? input_side() : output_side());
    if(sname == "file_in")
      return QString().fromUtf8("соед.номер %0, со стороны %1\n, из файла %2")
          .arg( id)
          .arg(source.attribute("dir")=="1"? input_side() : output_side())
          .arg(source.attribute("filename"));
    return "";
  }
public:
  virtual bool execute();

//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  };

private:
};

/** \brief Акт: остановить транслятор
 * После чего соединение будет вещать тишину.
 */
class CTScriptAction_stopPlay: public CTScriptAction {
  typedef CTScriptAction base;
public:

  CTScriptAction_stopPlay(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_stopPlay(){ }

public:
  virtual bool execute();
//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  };

private:
};

/** \brief Акт: измерить хар-ки входящего сигнала
 * Подрузамевается, что сигнал "синус", меряются
 * амплитуда в дБ(средне-квадратичная)  и сравнивается с исходящим сигналом. */
class CTScriptAction_test: public CTScriptAction {
  typedef CTScriptAction base;
public:
  CTScriptAction_test(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_test(){ }

public:
  virtual bool execute();
//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  };

private:
};


/** \brief Акт: измерить хар-ки входящего сигнала
 * Подрузамевается, что сигнал "синус", меряются
 * амплитуда в дБ(средне-квадратичная)*/
class CTScriptAction_testAbs: public CTScriptAction {
  typedef CTScriptAction base;
public:

  CTScriptAction_testAbs(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_testAbs(){ }

public:
  virtual bool execute();
//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  };

private:
};

/** \brief Акт: plot
 * Отрисовать АЧХ, в соответствии с измеренными данными
 * */
class CTScriptAction_plot: public CTScriptAction {
  typedef CTScriptAction base;
public:
  //
  CTScriptAction_plot(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){  }
  //
  virtual ~CTScriptAction_plot(){ }

public:
  virtual bool execute();
private:
};

///** \brief action listen

/** \brief Акт: установить слушателя
 * слушателями могут быть: файл, звуковой вывод */
class CTScriptAction_listen: public CTScriptAction {
  typedef CTScriptAction base;
public:

  CTScriptAction_listen(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_listen(){ }

public:
  virtual bool execute();

//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  };

private:
};


/** \brief Акт: отключить слушателя
 * после чего все входящие RTP-пакеты фиксироваться не будут */
class CTScriptAction_stopListen: public CTScriptAction {
  typedef CTScriptAction base;
public:

  CTScriptAction_stopListen(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_stopListen(){ }

public:
  virtual bool execute();

private:
};

/** \brief Акт: отключить запись файла
 * после чего все входящие RTP-пакеты фиксироваться не будут */
class CTScriptAction_stopAccept: public CTScriptAction {
  typedef CTScriptAction base;
public:

  CTScriptAction_stopAccept(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_stopAccept(){ }

public:
  virtual bool execute();

private:
};

/** \brief Акт: разъединить всех
 *Закрыть все соединения */
class CTScriptAction_disconnect_all: public CTScriptAction {
  typedef CTScriptAction base;
public:
  virtual bool mustBe_forStandalone(){return true; };
public:
  CTScriptAction_disconnect_all(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_disconnect_all(){ }

public:
  virtual bool execute();

//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  }

private:
};


/** \brief Акт: Задать параметры dtmf
 * Все последующие dtmf сигналы будут генерироваться с этими параметрами
 */
class CTScriptAction_dtmf: public CTScriptAction {
  typedef CTScriptAction base;
public:
//  virtual QString getParams(){
//      return QString().fromUtf8("%0сек")
//       .arg(source.attribute("pause").toInt() / 1000);
//  }
  virtual bool mustBe_forStandalone(){return true; };

  CTScriptAction_dtmf(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_dtmf(){ }

public:
  virtual bool execute();
private:
};

/** \brief Акт: пауза
 */
class CTScriptAction_pause: public CTScriptAction {
  typedef CTScriptAction base;
  int p;
  QString comment;
public:
  virtual QString getParams(){
    QString templ = getDescr();
    if(p != 0) templ += QString().fromUtf8(" %0сек").arg(p / 1000);
    if(comment.size() != 0) templ += QString().fromUtf8(" %1").arg(comment);
    return templ;
  }

  CTScriptAction_pause(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
    p = source.attribute("pause", "0").toInt();
    comment= source.attribute("comment", "");
  }
  virtual ~CTScriptAction_pause(){ }

public:
  virtual bool execute();

//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  }

private:
};

/** \brief Акт: RTPParams
 * Задать параметры RTP, при отработке акта показывается блокирующие диалог
 * с параметрами RTP
 */
class CTScriptAction_RTPParams:public CTScriptAction {
  typedef CTScriptAction base;
public:
  virtual QString getParams(){
    QString templ = getDescr();
    return templ;
  }

  CTScriptAction_RTPParams(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_RTPParams(){ }

public:
  virtual bool execute();

private:
};

/** \brief action listen
 */
class CTScriptAction_log: public CTScriptAction {
  typedef CTScriptAction base;
public:

  CTScriptAction_log(const QDomElement& _e, CTScript* _s)
  :base(_e, _s){
  }
  virtual ~CTScriptAction_log(){ }

public:
  virtual bool execute();

//  //
//  virtual QString toString(){
//    return base::toString()
//            + ", call script";
//  };
//
//  //
//  virtual QDomElement toDomElement(){
//    return base::toDomElement();
//  }

private:
};

}

#endif /* BASEACTIONS_H_ */



