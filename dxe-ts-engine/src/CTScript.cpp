/*
 * CTScript.cpp
 *
 *  Created on: 09.01.2012
 *      Author: Kulpanov
 */

#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <QtXML/QDomDocument>
#include <QtCore/QtCore>
#include <QtCore/QList>

#include "debug.h"


#include "dxe-call-server.h"
#include "dxe-ts-engine.h"

#include "CTScript.h"
#include "CTScriptExecuter.h"

namespace DXE {
typedef QSharedPointer<QThreadPool> QThreadPool_ptr;

///сервис работы с соединениями
static ICallService_ptr call_service;
///список активных соединений
static CListOfConnections_ptr connections;
///логгер работы
static ILogListener_ptr log_listener;
///пул потоков исполнителей сценариев
//static QThreadPool* executers_pool;
//static std::list<QThread*> executers_pool;

//отмена исполнения сценария
bool CTestScript::need_cancel = false;
int  CTestScript::count_executers = 0;
QString CTestScript::testsPath  = ".";

//
void CTestScript::cancelAll(){
  CTestScript::need_cancel = true;
}

//
void CTestScript::init(ILogListener_ptr _log_listener){
  if(call_service != NULL){
    throw E(0, "Повторный вызов CTestScript::init()");
  }
  try {
    call_service =
        ICallService_ptr(ICallService::create("noopts"),
                                        &ICallService::release);
    call_service->start(CTScript::settings_fname);

    connections =
            CListOfConnections_ptr(new CListOfConnections());

    log_listener = _log_listener;
    count_executers = 0;

    //executers_pool = QThreadPool::globalInstance();
//    executers_pool = new QThreadPool(NULL);
//    executers_pool->setMaxThreadCount(150);
  } catch (EXC e) {
    CLOG1(ERRR, "%s\n", e.what(), NULL);
    throw e;
  } catch (P_EXC e) {
    CLOG1(ERRR, "%s\n", e.what(), NULL);
    throw e;
  }
}

//
void CTestScript::release(){
  //executers_pool.clear();
  cancelAll();
  //Sleep(2000);
  connections.clear();
  call_service.clear();
  //logger.clear();
}

//
///** Класс исполнителя сценария. */
//class CTScriptExecuter: public QRunnable//QObject//QRunnable
//{
//  //TODO: реализовать как QSharedData ?
//  CTScript* scriptExecuter;
//protected:
//  virtual void run(){
//    CTestScript::count_executers ++;
//    //setTerminationEnabled(true);
//    LOG(TSTL1, "CTScriptExecuter::run: begin\n");
//    try{
//      scriptExecuter->execute();
//    }catch(P_EXC e){
//      LOG(ERRR, e.what());
//      //scriptExecuter->result = -100;
//    }
//    LOG(TSTL1, "CTScriptExecuter::run: end\n");
//    delete scriptExecuter;
//    //delete this;
//    CTestScript::count_executers --;
//  }
//public:
//  void start(){
//    setAutoDelete(true);
//    QThreadPool *threadPool = QThreadPool::globalInstance();
//    threadPool->start(this);
//  }
//
//  void start2(){
//    return run();
//  }
//
//
//public:
//  CTScriptExecuter(CTScript* _master)
//  :scriptExecuter(_master)
//  {    }
//
//  virtual ~CTScriptExecuter(){
//    //delete scriptExecuter;
//  }
//};

//
bool CTestScript::executeFile(const QString& _fname, const QString& _logid, bool _nowait){
  bool result = false;
  CLogger_ptr logger = CLogger_ptr(new CLogger(log_listener, _logid));
  try{
    CLOG(TSTL1, "CTestScript::executeFile: begin\n", NULL);

    //TODO: когда удалять scriptExecuter? Решить проблему! QSharedData?
    CTScript* script = //CTScript_ptr(
        new CTScript(call_service, connections, logger);
    //scriptExecuter->self = scriptExecuter;
    script->loadFile(_fname);
    //проверить на допустимость запуска сценария
    if(false == script->checkForStandalone()){
      result = false;
    }else{
      CTScriptExecuter* executer = new CTScriptExecuter(script);
//      executers_pool.push_back(executer);
      if(_nowait){
        //asynchro
        //executers_pool->start(
        executer->start();
      }else{
        //synchro
        executer->startSynchro();
      }
      result = true;//scriptExe cuter->IsPASS();
    }
//      delete scriptExecuter;
  }catch(CException& e){
    CLOG1(ERRR, "%s\n", e.what(), NULL);
    //logger->scriptBegin((" "), _fname);
    logger->scriptFail(_fname, QString().fromUtf8(e.what2()) );
    result = false;
  }catch(P_EXC e){
    CLOG1(ERRR, "%s\n", e.what(), NULL);
    result = false;
  }
  CLOG(TSTL1, "CTestScript::executeFile: end\n", NULL);
  return result;
}

void CTestScript::waitForDoneOfExecute(){
  //executers_pool->waitForDone();
//  bool all = true;
//  for(auto it = executers_pool.begin(); it!=executers_pool.end(); it++){
//    if((*it)->isRunning()){
//
//    }
//  }

}

////////////////////////////////////////////////////////////////////////////////
//
const QString& CTScriptAction::input_side(){
  static QString value = QString().fromUtf8("'Входящего'");
  return value;
}

//
const QString& CTScriptAction::output_side(){
  static QString value = QString().fromUtf8("'Исходящего'");
  return value;
}

//
bool CTScriptAction::execute(){
  LOG1(TSTL1, "Execute:%s\n", toString().toUtf8().constData());
  return true;
}

QString CTScriptAction::toString(){
  //  QString qstr;
  //  QTextStream q_stream(&qstr);
  //  n.save(q_stream, 4);
  QString str = "<" + source.tagName();
  QDomNamedNodeMap attrs = source.attributes();
  for(int i = 0; i<attrs.size(); i++)
  {
      QDomAttr attr = attrs.item(i).toAttr();
      str +=
          QString::fromUtf8(" %0=\"%1\"")
          .arg(attr.name())
          .arg(attr.value());
  }
  str += "/>";

  return str;
};

////////////////////////////////////////////////////////////////////////////////
//
const std::string CTScript::settings_fname = "call-server.settings.xml";

//
CTScript::CTScript(ICallService_ptr _call_service,
    CListOfConnections_ptr _connections,
    CLogger_ptr _logger)
    :logger(_logger)
  ,call_service(_call_service), connections(_connections)
{
  my_connections.reserve(127);
  my_connections.insert(0, 127, 0);
  def_connect_id = 0;
  result = -1;
  ref = 1;
  LOG(TSTL1, "CTScript:ctor\n" );
}

//
CTScript::CTScript(CTScript* _parent, CLogger_ptr _actLogger, int _connect_id)
  : logger(NULL)
  , call_service(CHECK_NULL(_parent)->call_service)
  , connections(_parent->connections)
  , def_stream(_parent->def_stream)
  , def_connect_id(_connect_id)
{
  logger = _actLogger->getChild(1);
  my_connections.reserve(127);
  my_connections.insert(0, 127, 0);

  result = -1;
  ref = 1;
  LOG1(TSTL1, "CTScript:ctor connnect_id=%d\n", def_connect_id);
}

//
CTScript::~CTScript() {
  //зачистить только свои соединения
  auto conn_it = my_connections.begin();
  for( ; conn_it != my_connections.end(); conn_it++){
    if(*conn_it != 0){
      connections->operator [](*conn_it).clear();
      *conn_it = 0;
    }
  }
  LOG(TSTL1, "CTScript:dtor\n");

//вот для чего нужны shared_ptr
//  while (!isEmpty()) {
//    delete first();
//    removeFirst();
//  }
//
//    if(NULL == parent_script){
//      //
//      call_service->closeAll();
//    }
}

//
CConnection_ptr CTScript::getConnection(int _id) {
//  ASSERT (connections);
  //имеем доступ к своим и к соед. parent
  //ASSERT(_id <= connections->size());
//  if((connections->at(_id).isNull()) ){
//    //спросить у parent
//    throw Ev(0, "Нет такого соединения, id=%d", _id);
//  }
  return connections->at(_id);
}

//
void CTScript::addConnection(int _id, ICallManager* m1, ICallManager* m2) {
  LOG1(INFO, "add connection at %d\n", _id);
  //добавляем только в свой скрипт
  //if(parent_script)parent_script->addConnection(_id, m1, m2);

  if(! connections->at(_id).isNull() ){
    throw Ev(0, "Соединение %d уже занято", _id);
  }
  CConnection_ptr conn = QSharedPointer<CConnection>(
                                  new CConnection(m1, m2, logger));

  connections->operator[](_id) = conn;
  my_connections[_id] = (_id);//сохраняем свои соединения
}

//
void CTScript::removeConnection(int _id) {
  //ASSERT (connections);
  if(_id == -1){
    //зачистить все соединения

    int id = 0;
    for( ; id != 100; id++){
      removeConnection(id);
    }
    Sleep(2000);
    call_service->closeAll();
    return ;
  }
  if( connections->at(_id).isNull() )
    return ;
  LOG1(INFO, "remove connection at %d\n", _id);

  connections->operator[](_id).clear();
  my_connections[_id] = (0);
}

//
bool CTScript::checkForStandalone(){
  //проверить на наличие актов "Закрыть" или "Закрыть все"
  bool script_pass = true;
  auto it = listOfAction.begin();
  for (; it != listOfAction.end(); it++) {
    if((script_pass = (*it)->mustBe_forStandalone()) == true){
      break;
    }
  }
  if(script_pass == false){
    logger->scriptBegin(getDescr());
    logger->scriptFail(getDescr()
        , QString::fromUtf8("Отсутствуют акты 'Разъединить' или 'Разъединить всех' "));
    return false;
  }
  return true;
}

//
void CTScript::execute() {
//  if(_nowait){
//    executers_pool->start(new CTScriptExecuter(this));
//    return;
//  }

  result = EOK;
  CTestScript::need_cancel = false;

  //Сценарий "..." выполняю
  LOG2(TSTL1, "CTScript::execute: thread started: %s:%s\n", getDescr().toUtf8().begin(), getFileName().toUtf8().begin());
//  std::cout << getDescr().toUtf8().begin() << getFileName().toUtf8().begin()
//      <<endl << flush;
  logger->scriptBegin(getDescr());

  auto it_action = listOfAction.begin();
  for (; it_action != listOfAction.end(); it_action++) {
    int res;
    try {
      res = ((*it_action)->execute());

      if((false == res)){
        log_message = (*it_action)->getLogMessage();
        result = 1; //script will be fail
      }

      //отмена исполнения
      if(CTestScript::need_cancel){
        LOG(ERRR, "CTScript::execute()::Script cancel\n");
        log_message = QString().fromUtf8("Сценарий отменён");
        result = -1;
      }

    } catch (CException& e) {
      LOG1(ERRR, "%s\n", e.what());
      log_message = QString().fromUtf8(e.what2());
      result =  -2; // - выйти из цикла
    }catch(std::exception& e){
      LOG1(ERRR, "%s\n", e.what());
      log_message = QString().fromUtf8(e.what());
      result =  -2; // - выйти из цикла
    }catch(...){
      LOG(ERRR, "CTScript::execute()::Unexpected exception\n");
      log_message = QString().fromUtf8("Неизвестная ошибка");
      result =  -3;// - выйти из цикла
    }

    if(! IsPASS()){
      logger->scriptFail(getDescr(), log_message);
      if(result<0){
        break;//выйти из цикла
      }
    }
  }
  if(IsPASS()){
    logger->scriptPass(getDescr());
  }
}

////////////////////////////////////////////////////////////////////////////////
//
void CTScript::loadFile(const QString& _fname){
  if(QDir::isAbsolutePath(_fname))
    fname = _fname;//QString::fromUtf8(_fname);
  else{
    fname = DXE::CTestScript::testsPath + "/" +_fname;//QString::fromUtf8(_fname);
  }
  LOG1(TSTL1, "CTScript::loadFile: %s\n", fname.toLocal8Bit().begin());
  //спользовать Qt::decodeFileName()?
  ifstream f(fname.toLocal8Bit().begin());
  if(f.fail()){
    throw Ev(0, "Ошибка: не могу открыть файл: \n%s", fname.toUtf8().begin());
  }
  std::string str((std::istreambuf_iterator<char>(f))
      , std::istreambuf_iterator<char>());
  loadScript(str.c_str());
}

//
void CTScript::loadScript(const char* _script){
  if(NULL == _script)
    return;
  //распарсить поток как XML, создать список Actions
  QString errorMsg;
  int errorLine, errorColumn;
  QByteArray _input(_script);

  QDomDocument doc;
  if (false == doc.setContent(_input, &errorMsg, &errorLine, &errorColumn)) {
    throw Ev(0, "Ошибка разбора сценария:\n"
        "\t%s:\n\tline:%d, column:%d"
        , errorMsg.toStdString().c_str(), errorLine, errorColumn);
  }


  QDomElement e = doc.documentElement();
  if (e.tagName() != "script") {
    throw E(0, "Ошибка разбора сценария: это не сценарий, нет элемента script<>");
  }
  descr = e.attribute("descr", "noname");

  int actCounter = 0;

  listOfAction.push_back(CTScriptAction_ptr(
      CTScriptAction::create(e, this, 0)));

  QDomNode n = e.firstChild();
  while (!n.isNull()) {
    e = n.toElement(); // try to convert the node to an element.
    if (!e.isNull()) {
      if (e.tagName() == "action") {
        //создать и добавить action
        listOfAction.push_back(CTScriptAction_ptr(
            CTScriptAction::create(e, this, ++++actCounter)));
      }

    }
    n = n.nextSibling();
  }

  e = QDomElement();
  e.setTagName("action");
  e.setAttribute("name", "endofscript");
  listOfAction.push_back(CTScriptAction_ptr(
      CTScriptAction::create(e, this, 0)));
}

////

SMsg_log::SMsg_log(const QString& _id, const QString& _msg
    , int _result, bool _fulldate)
  :id(_id), msg(_msg), result(_result), fulldate(_fulldate){
}

SMsg_log::SMsg_log(const SMsg_log& _msg):
  id(_msg.id), msg(_msg.msg), result(_msg.result), fulldate(_msg.fulldate)
{
}

SMsg_log::SMsg_log():
  result(-1),fulldate(false)
{  }

QString SMsg_log::toString() const{
  return QString("%0, %1, %2 %3")
      .arg(id).arg(msg)
      .arg(result==0?"pass":result==1?"fail":result==2?"msg":"started")
      .arg(fulldate?",fulldata":" ");
}

}

