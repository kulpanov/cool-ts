/*
 * baseActions.cpp

 *
 *  Created on: 09.01.2012
 *      Author: Kulpanov
 */
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <future>
#include <thread>

#include <QtCore/QMap>
#include <QtXML/QDomDocument>
#include <QtCore/QThread>

#include "debug.h"

#include "CTScript.h"
#include "baseActions.h"
#include "CTScriptExecuter.h"
#include <QtCore/QtCore>

#include "CXMLReader.h"
__declspec(dllexport) int plotSleepTime = 300;

namespace DXE {
static const char* const E_NO_SUCH_CONNECTION = "Нет такого соединения.";
static const char* const E_NO_SUCH_DIRECTION  = "Соед.в эту сторону не установлено.";
static const char* const E_EMPTY = "Нет данных для измерений.";

int runAsynchro(ICallManager* _call, int _call_timeout, string _callback_number){
  _call->setConnectData(_callback_number, "", uint32_t(0));
  int result = _call->waitforCall(_call_timeout);
  //CLOG(TSTL1, NULL, "runAsynchro \n");
  return result;
}


CTScriptAction::CTScriptAction(const QDomElement& _source, CTScript* _script):
source(_source), script(_script)
{
  id = _source.attribute("id", "0").toInt();
  if(id == 0){
    LOG1(TSTL1, "CTScriptAction: set default id %d\n", id);
    id = (_script->getConnectId());
  }
  if(id < -1 )
    throw E(0, "invalid_argument, id<0");

  dir = _source.attribute("dir", "0").toInt();
  int actNumber = _source.attribute("actNumber", "0").toInt();
//  descr = source.attribute("descr", "noname");


  LOG2(TSTL1, "CTScriptAction:ctor:%s: %d\n", toString().toUtf8().constData(), id );

  logger = script->getLoggerChild(actNumber);
}

CTScriptAction::CTScriptAction(const QDomElement& _source, CTScript* _script,
    int _id, int _dir):
  source(_source), script(_script)
  ,id(_id), dir(_dir)
{  log_message = QString().fromUtf8("без сообщения");
  //actNumber = 0;
}

QString CTScriptAction::getDescr(){
//    return QString("Акт %0: ").arg(actNumber) +
//        (source.attribute("descr", "noname"));
  QString actNo = source.attribute("actNo");
  QString descr = source.attribute("descr");
  QString res = QString().fromUtf8("Акт ") + (actNo.size()>0?actNo:" ")
      +  QString(":") + descr;
//  LOG1(DET, "getDescr:%s\n", descr.toUtf8().constData());
   return res;
}

bool CTScriptAction_begin::execute() {
  //начало скрипта: проверить что все создано, при необходимости создать
  if(script->getCallService().isNull()){
    throw E(0, "CallService не создан");
  }
  LOG2(INFO, "Action: begin %d: %s\n", id, source.text().toUtf8().begin());
  return true;
}

bool CTScriptAction_end::execute() {
  //
  LOG1(INFO, "Action: end %d\n", id);
  return true;
}


bool checkValidTargetIP(const IP& _targetIP, pair<const IP&, const IP&> _gate_netMask){
  IP _netIP = (uint32_t)_gate_netMask.first & (uint32_t)_gate_netMask.second;
  if ( ((uint32_t)_targetIP & (uint32_t)_gate_netMask.second) == _netIP)
    return true;
  return false;
}

bool CTScriptAction_connect::execute() {
  //1. создать соединение(target)
  LOG2(INFO, "Action: connect  %d: %s\n", id, source.text().toUtf8().begin());
  logger->actBegin(getDescr(), getParams());
  try{
    if(NULL != script->getConnection(id)){
      throw Ev(0, "Cоединение(id=%d)уже занято", id);
    }

    ICallService_ptr call_service = script->getCallService();
    ICallManager* call_out = call_service->createCallManager(ICall::_reserved);
    ICallManager* call_in = call_service->createCallManager();

    string target = source.attribute("target", "100").toStdString();
    string callback_number = QString("%0").arg(id, 3, 10, QChar('0')).toStdString();
    IP target_ip = source.attribute("target_ip", "127.0.0.1").toStdString();
//    int call_timeout = source.attribute("timeout", "40").toInt();//сек
    CXMLReader xml("call-server.settings.xml");
    int call_timeout = xml.GetAttribute("Params/RTP/call_timeout", "value", "40").toInt();

    LOG4(TSTL1, "Connect(%d): call in/out: %s %s %s\n", id, target.c_str(), callback_number.c_str(), target_ip.toString().c_str());
    LOG3(TSTL1, "Connect(%d): call in=0x%x /out=0x%x\n", id, call_in, call_out);

    if(! checkValidTargetIP(target_ip, call_service->getIPNet())){
      throw Ev(0, "Указанный IP(%s) находится вне подсети", target_ip.toString().c_str());
    }

    if("connection->waitfor( WSEC(call_timeout))"){
      volatile bool breakWaiting = false;
      struct SHelper{
        static int runAsynchro(ICallManager* _call, int _call_timeout, volatile bool *breakWaiting){
          _call_timeout *= 2;//in 2 times more
          while(!CTestScript::need_cancel && ! (*breakWaiting) && EOK != _call->waitforCall(1000)){
            if( 0 >= (_call_timeout -= 1000) ) return ETIMEDOUT;
          }
          return EOK;
        } };
      //
      call_in->setConnectData(callback_number, "", uint32_t(0));
      LOG1(DEVL3, "Connect(%d): QtConcurrent::run\n", id);
      //QFuture<int> f1 = QtConcurrent::run(&SHelper::runAsynchro, call_in, WSEC(call_timeout), &breakWaiting);
      std::future<int> f1 = std::async(std::launch::async, &SHelper::runAsynchro, call_in, WSEC(call_timeout), &breakWaiting);
      LOG1(DEVL3, "Connect(%d): call_out->call\n", id);
      call_out->call(target, callback_number, INFINITE, target_ip);
      //
      int res_out = 0;//call_out->waitforCall(WSEC(call_timeout));

      int callOut_timeout = WSEC(call_timeout);
      while(!CTestScript::need_cancel && EOK != call_out->waitforCall(1000)){
        if( 0 >= (callOut_timeout -= 1000) ){
          res_out = ETIMEDOUT;
          break;
        }
      }

      if(EOK !=  res_out){
        LOG1(TSTL1, "Connect(%d): SHelper::breakWaiting = true\n", id);
        breakWaiting = true;
        log_message =
            QString::fromUtf8("Истекло время(%0 сек) на установку исходящего вызова\n%1")
            .arg(call_timeout)
            .arg(QString::fromUtf8(call_out->getLastMsg().c_str()));
      }
      LOG1(TSTL1, "Connect(%d): f1.result()\n", id);
      int res_in = f1.get();

      if(EOK !=  res_in){
        log_message =
            QString::fromUtf8("Истекло время(%0 сек) на установку входящего вызова\n%1")
            .arg(call_timeout)
            .arg(QString::fromUtf8(call_out->getLastMsg().c_str()));
      }
      if(CTestScript::need_cancel){
        LOG1(INFO, "Connect(%d): cancel connect\n", id);
        log_message = QString::fromUtf8("Действие отменено");
        res_out = -1;
      }else
        LOG5(TSTL1, "Connect(%d): call in/out wait completed: res i/o=%d/%d, call_in=%d, call_out=%d\n", id, res_in, res_out, (int)call_in->getStatus(), (int)call_out->getStatus());

      if(EOK !=  res_out || EOK !=  res_in){
        if(call_out) call_out->release();
        if(call_in) call_in->release();
        call_out = NULL;call_in = NULL;

        throw E(0x1000, log_message.toUtf8().begin());
      }
    }
    //соед установлено!
    script->addConnection(id, call_out, call_in);
  }catch(CException& e){
    LOG1(INFO, "Action: fall exc:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2())/*, CLogger::str_dash */);
    throw e;
//  } catch (EXC e) {
//    E_CATCH(e);
//    throw e;
  }catch(P_EXC e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    CLOG(ERRR, e.what(), NULL);
    throw e;
  }
  logger->actPass(getDescr()/*, CLogger::str_dash*/);
  return true;
}

bool CTScriptAction_connectHalf::execute() {
  //1. создать соединение(target)
  LOG2(INFO, "Action: connectHalf  %d: %s\n", id, source.text().toUtf8().begin());
  logger->actBegin(getDescr(), getParams());
  try{
    if(NULL != script->getConnection(id)){
      throw Ev(0, "Cоединение(id=%d)уже занято", id);
    }

    ICallService_ptr call_service = script->getCallService();
    ICallManager* call_out = call_service->createCallManager(ICall::_reserved);

    string target = source.attribute("target", "100").toStdString();
    string callback_number = "0";
    IP target_ip = source.attribute("target_ip", "127.0.0.1").toStdString();
//    int call_timeout = source.attribute("timeout", "40").toInt();//сек
    CXMLReader xml("call-server.settings.xml");
    int call_timeout = xml.GetAttribute("Params/RTP/call_timeout", "value", "40").toInt();

    LOG3(TSTL1, "Connect(%d): call out: %s %s\n", id, target.c_str(), target_ip.toString().c_str());

    if("connection->waitfor( WSEC(call_timeout))"){

      call_out->call(target, callback_number, INFINITE, target_ip);
      int res_out = 0;//call_out->waitforCall(WSEC(call_timeout));

      int callOut_timeout = WSEC(call_timeout);
      while(!CTestScript::need_cancel && EOK != call_out->waitforCall(1000)){
        if( 0 >= (callOut_timeout -= 1000) ){
          res_out = ETIMEDOUT;
          break;
        }
      }
      if(EOK !=  res_out){
          log_message =
              QString::fromUtf8("Истекло время(%0 сек) на установку исходящего вызова\n%1")
              .arg(call_timeout)
              .arg(QString::fromUtf8(call_out->getLastMsg().c_str()));
      }
      if(CTestScript::need_cancel){
        LOG1(INFO, "Connect(%d): cancel connect\n", id);
        log_message = QString::fromUtf8("Действие отменено");
        res_out = -1;
      }else
      LOG3(TSTL1, "Connect(%d): call out wait completed: res o=%d, call_out=%d\n", id, res_out, (int)call_out->getStatus());
      if(EOK !=  res_out){
        if(call_out) call_out = call_out->release();

        throw E(0x1000, log_message.toUtf8().begin());
      }
    }
    //соед установлено!
    script->addConnection(id, call_out, NULL);

  }catch(CException& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2())/*, CLogger::str_dash */);
    throw e;
  }
  logger->actPass(getDescr()/*, CLogger::str_dash*/);
  return true;
}


bool CTScriptAction_disconnect::execute() {
  LOG2(INFO, "Action: disconnect %d: %s\n", id, source.text().toUtf8().begin());
  try{
  CConnection_ptr conn = script->getConnection(id);
  if(conn.isNull()){
    throw Ev(0, "Cоединение(id=%d)уже свободно", id);
  }
  do{
    ICallManager* call = conn->call_byDir(0);
    if(NULL == call || 0xDB != call->isPlaying()) break;
//    LOG1(DEVL1, "Action: disconnect %d:wait for end of playing\n", id);
    Sleep(100);
  }while(1);
  do{
    ICallManager* call = conn->call_byDir(1);
    if(NULL == call || 0xDB != call->isPlaying()) break;
//    LOG1(DEVL1, "Action: disconnect %d:wait for end of playing\n", id);
    Sleep(100);
  }while(1);

  //conn->close();//закрыли!
  script->removeConnection(id);
  } catch (EXC e) {
    E_CATCH(e);
    throw e;
  }catch(P_EXC e){
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what()) );
    throw e;
  }
  LOG2(INFO, "Action: disconnect completed %d: %s\n", id, source.text().toUtf8().begin());
  logger->actPass(getDescr());
  return true;
}

bool CTScriptAction_call::execute() {
  LOG1(INFO, "Action: call %s\n", source.text().toUtf8().begin());
  logger->actBegin(getDescr());
  int res = 0;     int def_nloop2 = 0;
  try{
    static const QString sname = "sname";

    if(! source.hasAttribute(sname)){
      throw Ev(0, "%s", "Нет тега sname");
    }

    QString fname = source.attribute("sname", " ");
    int def_connnect_id = source.attribute("conn_id", "0").toInt();
    int def_nloop       = source.attribute("loop", "0").toInt();

    //int parallel        = source.attribute("parallel", "0").toInt();
    LOG2(TSTL1, "Action: call connnect_id=%d, nloop=%d\n", def_connnect_id, def_nloop);

    do{
      logger->actMessage(QString().fromUtf8("Вызов %0 раз").arg(++def_nloop2));
      CTScript scriptExecuter(script, logger, def_connnect_id);

      scriptExecuter.loadFile(fname);
      scriptExecuter.execute();
      log_message = scriptExecuter.log_message;
      res = scriptExecuter.IsPASS()? 0 : -1;
//      if(res>=0)
//        logger->actPass(getDescr());
//      else
//        logger->actFail(getDescr(), "");
    }while(res>=0 && --def_nloop > 0 );
  }catch(CException& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(),  log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  if(res>=0)
    logger->actPass(getDescr() + QString().fromUtf8(", всего %0 вызовов").arg(def_nloop2));
  else
    logger->actFail(getDescr(), QString().fromUtf8(", сбой при %0 вызове").arg(def_nloop2));
  return (res>=0);
}

bool CTScriptAction_play::execute() {
  LOG2(INFO, "Action: play %d: %s\n", id, source.text().toUtf8().begin());
  logger->actBegin(getDescr(), getParams());
  try{
  //try{
  CConnection_ptr conn = script->getConnection(id);
  if(NULL == conn){
    throw E(0, E_NO_SUCH_CONNECTION);
  }
  ICallManager* call = conn->call_byDir(dir);
  if(NULL == call)
    throw E(0, E_NO_SUCH_DIRECTION);

  string strerror;
  string opt = toString().toUtf8().begin();

//  LOG2(INFO, "Action: play: %s\n", id, toString().toStdWString().c_str());

  IWavStream* stream = IWavStream::create(opt, strerror);
  if(NULL == stream){
    throw Ev(0, "Ошибка при создании потока\n\t%s\n", strerror.c_str());
  }
  call->setPlayer(stream);
  Sleep(1000); //для строгого начала вещания
  //
  //сохранить как player
  conn->stream_byDir(dir) = opt;
  script->setDefStream( opt);
  }catch(CException& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }catch(std::exception& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what()) );
    throw e;
  }
//  logger->actPass(getDescr());
  return true;
}

bool CTScriptAction_stopPlay::execute() {
  LOG2(INFO, "Action: stopPlay  %d: %s\n", id, source.text().toUtf8().begin());
  try{
  CConnection_ptr conn = script->getConnection(id);
  if(NULL == conn){
    throw E(0, E_NO_SUCH_CONNECTION);
  }
  ICallManager* call = conn->call_byDir(dir);
  if(NULL == call)
    throw E(0, E_NO_SUCH_DIRECTION);

  call->stopPlaying();
  Sleep(1000);//для строгого конца вещания
  conn->stream_byDir(dir) = "";
  }catch(CException& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
//  logger->actPass(getDescr());
  return true;
}

bool CTScriptAction_listen::execute() {
  LOG2(INFO, "Action: listen %d: %s\n", id, source.text().toUtf8().begin());
  try {
  CConnection_ptr conn = script->getConnection(id);
    if(NULL == conn){
      throw E(0, E_NO_SUCH_CONNECTION);
    }

    ICallManager* call = script->getConnection(id)->call_byDir(dir);
    if(NULL == call)
      throw E(0, E_NO_SUCH_DIRECTION);

    string strerror;
    string opt = toString().toUtf8().begin();
    IWavStream* stream =  IWavStream::create(opt, strerror);
    if(NULL == stream)
      throw Ev(0, "Ошибка при создании потока\n\t%s\n", strerror.c_str());

    if(string::npos != opt.find("sound_out"))
      call->setListener(stream, ICallManager::_variantAudio); //слушаем сеть и пишем в поток
    if(string::npos != opt.find("file_out"))
      call->setListener(stream, ICallManager::_variantFile); //слушаем сеть и пишем в поток
    else
      call->setListener(stream, ICallManager::_variantDef); //слушаем сеть и пишем в поток
    //TODO: а еще лучше сделать id = addListener()/removeListener(id)
  }catch(CException& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }catch(std::exception& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what()) );
    throw e;
  }
  logger->actPass(getDescr());
  return true;
}

bool CTScriptAction_stopListen::execute() {
  LOG2(INFO, "Action: stopListen  %d: %s\n", id, source.text().toUtf8().begin());
  try{
  CConnection_ptr conn = script->getConnection(id);
  if(NULL == conn){
    throw E(0, E_NO_SUCH_CONNECTION);
  }

  ICallManager* call = script->getConnection(id)->call_byDir(dir);
  if(NULL == call)
    throw E(0, E_NO_SUCH_DIRECTION);

  call->stopListen(ICallManager::_variantAudio);
  }catch(CException& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  logger->actPass(getDescr());
  return true;
}


bool CTScriptAction_stopAccept::execute() {
  LOG2(INFO, "Action: stopAccept  %d: %s\n", id, source.text().toUtf8().begin());
  try{
  CConnection_ptr conn = script->getConnection(id);
  if(NULL == conn){
    throw E(0, E_NO_SUCH_CONNECTION);
  }

  ICallManager* call = script->getConnection(id)->call_byDir(dir);
  if(NULL == call)
    throw E(0, E_NO_SUCH_DIRECTION);

  call->stopListen(ICallManager::_variantFile);
  }catch(CException& e){
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  logger->actPass(getDescr());
  return true;
}


bool CTScriptAction_test::execute() {
  //4.Измерить(dir, hi, low)
  LOG2(INFO, "Action: test %d:%s\n", id, source.text().toUtf8().begin());
  bool res = true;
  try{
  int measure_time  = 1000; //source.attribute("pause", "3000").toInt();
  int mean_time     = 1000; //source.attribute("mtime", "100").toInt();
  double hi         = source.attribute("hi", "10").toFloat();
  double lo        = source.attribute("low", "-10").toFloat();
  int check        = source.attribute("check", "1").toInt();

  CConnection_ptr conn = script->getConnection(id);
  if(NULL == conn)
    throw E(0, E_NO_SUCH_CONNECTION);

  ICallManager* call_forTest = conn->call_byDir(dir);
  if(NULL == call_forTest)
    throw E(0, E_NO_SUCH_DIRECTION);

  call_forTest->stopListen(ICallManager::_variantDef);//Измерить останавливает пред слушателя
  //поток для прослушки
  CString opt = "<item type=\"mem\" />";//25kbytes
        //"<item type=\"file_out\" filename=\"D:/_prj/amt/ws2/dxe/install/11.wav\" />";
  string strerr;
  IWavStream* stream_forTest = IWavStream::create(opt, strerr);
  call_forTest->setListener(stream_forTest, ICallManager::_variantDef);//слушаем в память
  call_forTest->waitfor(measure_time+100);// копим, что услышали //400
  call_forTest->stopListen(ICallManager::_variantDef, false);//стоп прослушки-записи

  if(stream_forTest->isEndOfStream())
    throw E(0, E_EMPTY);
  //создать тестер и провести тест!
  std::string other_stream = conn->stream_byDir(!dir);
  if(other_stream.size() < 5){
    //сравниваем с последним созданным генераторм
    other_stream = script->getDefStream();
  }
  //тестер для измерений параметров потока
  CWavTester tester(
      IWavStream::create(other_stream, strerr), stream_forTest, measure_time );
  IFLOG(DEVL2) tester.saveTestStreamToFile = true;

  tester.test2(mean_time);
  if(tester.getMaxError()  > (hi + 0.05))
    res = false;
  if(tester.getMaxError()  < (lo - 0.05))
    res = false;

  QString pattern =  QString()
      .fromUtf8("Результат измерений со стороны %0:\n  коэф.передачи=%1дБ"
          ", измеренный уровень(rms):от %2дБ до %3дБ"
          )
      .arg(source.attribute("dir")=="1"? input_side() : output_side())
      .arg(tester.getMaxError(), 0, 'f', 1)
      .arg(tester.getForTestMinDb(), 0, 'f', 1)
      .arg(tester.getForTestMaxDb(), 0, 'f', 1);

  if(0 == check)
    res = true;
  else
    pattern.insert(pattern.indexOf(", ")
        , QString().fromUtf8(", при границах: от %0дБ до %1дБ\n").arg(lo).arg(hi));

  if(false == res){
    log_message =
        QString::fromUtf8("Cоединение (id=%0): измерение вышло за пределы допустимого\n  %1")
          .arg(id).arg(pattern);
  }else{
    log_message =
        QString::fromUtf8("Cоединение (id=%0): измерение выполнено успешно\n  %1")
          .arg(id).arg(pattern);
    logger->actMessage(log_message);
  }

  } catch (CException& e) {
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  if(! res){
    logger->actFail(getDescr(), log_message);
    LOG1(WARN, "%s\n",log_message.toUtf8().begin());
    return false;
  }

  logger->actPass(getDescr());
  return true;
}

bool CTScriptAction_testAbs::execute() {
  //4.Измерить(dir, hi, low)
  LOG2(INFO, "Action: testAbs %d: %s\n", id, source.text().toUtf8().begin());
  bool res = true;
  try{
  int measure_time  = 1000; //source.attribute("pause", "3000").toInt();
  int mean_time     = 1000; //source.attribute("mtime", "100").toInt();
  double hi         = source.attribute("hi", "10").toFloat();
  double lo        = source.attribute("low", "-10").toFloat();
  int check        = source.attribute("check", "1").toInt();

  CConnection_ptr conn = script->getConnection(id);
  if(NULL == conn){
    throw E(0, E_NO_SUCH_CONNECTION);
    return false;
  }
  ICallManager* call_forTest = conn->call_byDir(dir);
  if(NULL == call_forTest)
    throw E(0, E_NO_SUCH_DIRECTION);

  call_forTest->stopListen(ICallManager::_variantDef);//Измерить останавливает пред слушателя

  //поток для прослушки
  CString opt = "<item type=\"mem\" />";//25kbytes
  string strerr;
  IWavStream* stream_forTest = IWavStream::create(opt,strerr);
  call_forTest->setListener(stream_forTest, ICallManager::_variantDef);//слушаем в память

  call_forTest->waitfor(measure_time+100);// копим, что услышали

  call_forTest->stopListen(ICallManager::_variantDef, false);//стоп прослушки-записи

  if(stream_forTest->isEndOfStream())
    throw E(0, E_EMPTY);

  //создать тестер и провести тест!
  std::string other_stream = conn->stream_byDir(!dir);

  CWavTester tester(
      IWavStream::create(other_stream, strerr), stream_forTest, measure_time );

  tester.test2(mean_time);
  if(tester.getForTestMaxDb() > (hi + 0.05))
    res = false;
  if(tester.getForTestMinDb() < (lo - 0.05))
    res = false;

  QString pattern =  QString()
      .fromUtf8("Результат измерений со стороны %0:\n измеренный уровень(rms):от %1дБ до %2дБ")
      .arg(source.attribute("dir")=="1"? input_side() : output_side())
      .arg(tester.getForTestMinDb(), 0, 'f', 1)
      .arg(tester.getForTestMaxDb(), 0, 'f', 1);

  if(0 == check)
    res = true;
  else
    pattern +=  QString().fromUtf8(", при границах: от %0дБ до %1дБ\n").arg(lo).arg(hi);

  if(false == res){
    log_message =
        QString::fromUtf8("Cоединение (id=%0): измерение вышло за пределы допустимого\n  %1")
          .arg(id).arg(pattern);
  }else{
    log_message =
        QString::fromUtf8("Cоединение (id=%0): измерение выполнено успешно\n  %1")
          .arg(id).arg(pattern);
    logger->actMessage(log_message);
  }


  } catch (CException& e) {
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  if(! res){
    logger->actFail(getDescr(), log_message);
    LOG1(WARN, "%s\n",log_message.toUtf8().begin());
    return false;
  }

  logger->actPass(getDescr());
  return true;
}

bool CTScriptAction_plot::execute() {
  //4.Измерить(dir, hi, low)
   LOG2(INFO, "Action: plot %d:%s\n", id, source.text().toUtf8().begin());
   logger->actBegin(getDescr());
   try{
      bool res = true;
      try{
      QVector<QPair<double, double> >  testResults;
      QString type       = source.attribute("name", "plot_rms");
      int measid        = id;
      int measSide      = dir;
      int genid         = source.attribute("genid", "0").toInt();
      int genSide       = source.attribute("genside", "0").toInt();
      int step          = source.attribute("step", "100").toInt();
      int firstFreq     = source.attribute("first", "350").toInt();
      int lastFreq      = source.attribute("last", "3500").toInt();
      int genAmpl       = source.attribute("ampl", "0").toInt();

      QString fname     = source.attribute("fname", "def.wav");
      int measure_time = 100;

      CConnection_ptr conn_forTest = script->getConnection(measid);
      CConnection_ptr conn_forGen  = script->getConnection(genid);
      if(NULL == conn_forTest)
        throw E(0, E_NO_SUCH_CONNECTION);
      if(NULL == conn_forGen)
        throw E(0, E_NO_SUCH_CONNECTION);

      ICallManager* call_forTest = conn_forTest->call_byDir(measSide);
      ICallManager* call_forGen = conn_forGen->call_byDir(genSide);
      if(NULL == call_forTest)
        throw E(0, E_NO_SUCH_DIRECTION);
      if(NULL == call_forGen)
        throw E(0, E_NO_SUCH_DIRECTION);

      call_forTest->stopListen(ICallManager::_variantDef);
      Sleep(300);

      IFLOG(DEVL2){
        static int fnameCounter = 0;
        QString opt = QString("<item type=\"file_out\" filename=\"test_%0.wav\" />").arg(++fnameCounter);
        string strerr;
        IWavStream* stream_forSave = IWavStream::create(opt.toStdString(),strerr);
        call_forTest->setListener(stream_forSave, ICallManager::_variantAudio);//слушаем в память
      }

      if(step <= 0) step = 10;
      for(int freq = firstFreq; freq<=lastFreq; freq += step){
        call_forGen->stopPlaying(false);
        Sleep(100);//0.1 of silence
        string strerr; QString opt;
        opt = QString("<action freq=\"%0\" ampl=\"%1\" type=\"sin\" />").arg(freq).arg(genAmpl);

        IWavStream* stream_forGen = IWavStream::create(opt.toStdString(), strerr);
        if(NULL == stream_forGen)
          throw Ev(0, "Ошибка при создании потока\n\t%s\n", strerr.c_str());
        if(CTestScript::need_cancel){
          log_message = QString().fromUtf8("Сценарий отменён");
          logger->actFail(getParams(), log_message);
          return false;
        }
        //поток для прослушки
        opt = "<item type=\"mem\" />";//25kbytes
        IWavStream* stream_forTest = IWavStream::create(opt.toStdString(), strerr);
        if(NULL == stream_forTest)
          throw Ev(0, "Ошибка при создании потока\n\t%s\n", strerr.c_str());

        call_forGen->setPlayer(stream_forGen);
        Sleep(plotSleepTime);
        call_forTest->setListener(stream_forTest, ICallManager::_variantDef);//слушаем в память

        call_forTest->waitfor(measure_time+100);// копим, что услышали
        call_forTest->stopListen(ICallManager::_variantDef, false);//стоп прослушки-записи

        if(stream_forTest->isEndOfStream())
          throw E(0, E_EMPTY);

        //тестер для измерений параметров потока
        CWavTester tester(stream_forGen, stream_forTest, 100 );
        IFLOG(DEVL2) tester.saveTestStreamToFile = true;
        tester.test2(measure_time);
        if(type == "plot_rms")
          testResults.push_back( { (double)freq, tester.getForTestMaxDb() } );
        else
          testResults.push_back( { (double)freq, tester.getMaxError() } );
      }

      call_forTest->stopListen(ICallManager::_variantAudio, true);//стоп прослушки-записи

      QFileInfo fi(script->getFileName());
      QString s_script = fi.baseName() + QString().fromUtf8(", дБ");  // base = "archive"
 //     LOG1(D2, "s_script:%s\n", s_script.toUtf8().begin());
      logger->scriptVec(fname, s_script, testResults);

      } catch (CException& e) {
        LOG1(INFO, "Action: fall:%s\n", e.what());
        logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
        throw e;
      }
      if(! res){
        logger->actFail(getDescr(), log_message);
        LOG1(WARN, "%s\n",log_message.toUtf8().begin());
        return false;
      }

   } catch (CException& e) {
     LOG1(INFO, "Action: fall:%s\n", e.what());
     logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
     throw e;
   }
   logger->actPass(getDescr());
   return true;

}

bool CTScriptAction_disconnect_all::execute() {
  LOG1(INFO, "Action: disconnect_all %s\n", source.text().toUtf8().begin());
  try{
//  logger->actBegin(getDescr());
  script->removeConnection(-1);
  Sleep(2000);
  } catch (CException& e) {
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  logger->actPass(getDescr());
  return true;
}

bool CTScriptAction_pause::execute() {
  LOG1(INFO, "Action: pause %s\n", source.text().toUtf8().begin());
  try{
  //logger->actBegin(getDescr());

  //int p = source.attribute("pause", "0").toInt();

  clock_t start_pause = clock();
  while((clock() - start_pause) < p){
    if(CTestScript::need_cancel){
      log_message = QString().fromUtf8("Сценарий отменён");
      logger->actFail(getParams(), log_message);
      return false;
    }
    Sleep(1);
  }
  } catch (CException& e) {
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getParams(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  logger->actPass(getParams());
  return true;
}

bool CTScriptAction_dtmf::execute() {
  LOG1(INFO, "Action: dtmf %s\n", source.text().toUtf8().begin());
  try{
  //logger->actBegin(getDescr());

  int ampl = source.attribute("amplitude", "0").toInt();
  int duration = source.attribute("duration", "60").toInt();
  int pause = source.attribute("pause", "60").toInt();

  ICallService_ptr call_service = script->getCallService();
  call_service->setDTMFParams(ampl, duration, pause);
  } catch (CException& e) {
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getDescr(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  logger->actPass(getDescr());
  return true;
}


bool CTScriptAction_log::execute() {
  LOG1(INFO, "Action: log IS NOT IMPLEMENT!, %s\n", source.text().toUtf8().begin());
  //script->getLogger()->
  //TODO: реализовать
  return true;
}

//
CTScriptAction *CTScriptAction::create(
    const QDomElement & e1, CTScript* _script, int _actNumber) {
  CTScriptAction* action = NULL;
  QDomElement e = e1;
  if (e.tagName() == "script") {
    //первый элемент
    action = new CTScriptAction_begin(e, _script);

    return action;
  }
  e.setAttribute("actNumber", QString().setNum(_actNumber));

  QString name_action = e.attribute("name", "endofscript");
  //создаём из списка предопределённых
  if (name_action == "connect") {
    action = new CTScriptAction_connect(e, _script);
  } else if (name_action == "connect_half") {
    action = new CTScriptAction_connectHalf(e, _script);
  } else if (name_action == "disconnect") {
    action = new CTScriptAction_disconnect(e, _script);
  } else if (name_action == "call_script") {
    action = new CTScriptAction_call(e, _script);
  } else if (name_action == "play") {
    action = new CTScriptAction_play(e, _script);
  } else if (name_action == "stopPlay") {
    action = new CTScriptAction_stopPlay(e, _script);
  } else if (name_action == "test_koef") {
    action = new CTScriptAction_test(e, _script);
  } else if (name_action == "test_abs") {
    action = new CTScriptAction_testAbs(e, _script);
  } else if (name_action == "listen") {
    action = new CTScriptAction_listen(e, _script);
  } else if (name_action == "stopListen") {
    action = new CTScriptAction_stopListen(e, _script);
  } else if (name_action == "stopAccept") {
    action = new CTScriptAction_stopAccept(e, _script);
  } else if (name_action == "disconnect_all") {
    action = new CTScriptAction_disconnect_all(e, _script);
  } else if (name_action == "pause") {
    action = new CTScriptAction_pause(e, _script);
  } else if (name_action == "log") {
    action = new CTScriptAction_log(e, _script);
  } else if (name_action == "dtmf") {
    action = new CTScriptAction_dtmf(e, _script);
  } else if (name_action == "endofscript") {
    action = new CTScriptAction_end(e, _script);
  } else if (name_action == "plot_ratio") {
    action = new CTScriptAction_plot(e, _script);
  } else if (name_action == "plot_rms") {
    action = new CTScriptAction_plot(e, _script);
  } else if (name_action == "rtp_params") {
    action = new CTScriptAction_RTPParams(e, _script);
  } else {
    ASSERT(0 && "unknown action");
  }
  return action;
}



bool CTScriptAction_RTPParams::execute() {
  LOG1(INFO, "Action: RTPParams %s\n", source.text().toUtf8().begin());
  try{
  logger->actBegin(getDescr());

  CConnection_ptr conn = script->getConnection(id);
  if(NULL == conn){
    throw E(0, E_NO_SUCH_CONNECTION);
  }

  ICallManager* call = script->getConnection(id)->call_byDir(dir);
  if(NULL == call)
    throw E(0, E_NO_SUCH_DIRECTION);

  logger->runDlg(0, call);

  call->setParams(0, 0); //set default params

  } catch (CException& e) {
    LOG1(INFO, "Action: fall:%s\n", e.what());
    logger->actFail(getParams(), log_message = QString::fromUtf8(e.what2()) );
    throw e;
  }
  logger->actPass(getParams());
  return true;

}

}
