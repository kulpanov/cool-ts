/*
 * CTScript.cpp
 *
 *  Created on: 09.01.2012
 *      Author: Kulpanov
 */

#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <QtXML/QDomDocument>
#include <QtCore/QtCore>
#include <QtCore/QList>

#include "debug.h"


#include "dxe-call-server.h"
#include "dxe-ts-engine.h"

#include "CTScriptExecuter.h"

namespace DXE {

////////////////////////////////////////////////////////////////////////////////////////////////////
CTScriptExecuter::CTScriptExecuter(CTScript* _master)
: script(_master), thRunner(NULL){

}
//
CTScriptExecuter::~CTScriptExecuter(){
  //delete scriptExecuter;
}

void CTScriptExecuter::run(){
  CTestScript::count_executers ++;
  //setTerminationEnabled(true);
  LOG(TSTL1, "CTScriptExecuter::run: begin\n");
  try{
    script->execute();
  }catch(P_EXC e){
    LOG(ERRR, e.what());
  }
  LOG(TSTL1, "CTScriptExecuter::run: end\n");
  delete script;
  CTestScript::count_executers --;
  emit finished();
}

void CTScriptExecuter::start(){
  thRunner = new QThread;
  this->moveToThread(thRunner);

  connect(thRunner, SIGNAL(started()), this, SLOT(run()));
  connect(this, SIGNAL(finished()), thRunner, SLOT(quit()));
  connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
  connect(thRunner, SIGNAL(finished()), thRunner, SLOT(deleteLater()));
  thRunner->start();
}

};
