/*
 * CTScript.h
 *
 *  Created on: 09.01.2012
 *      Author: Kulpanov
 */

#ifndef CDXETSCRIPTEXECUTER_H_
#define CDXETSCRIPTEXECUTER_H_

#include <QObject>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QTextStream>
#include <QtCore/QSharedPointer>
#include <QtXML/QDomElement>
#include <QThread>

#include "dxe-ts-utils.h"
#include "dxe-ts-engine.h"
#include "CTScript.h"

namespace DXE{
using namespace DXE;

/** Класс исполнителя сценария. */
class CTScriptExecuter: public QObject
{
  Q_OBJECT;
  //TODO: реализовать как QSharedData ?
  CTScript* script;
  QThread* thRunner;

signals:
  void finished();

protected slots:
  void run();

public:
  void start();

  void startSynchro(){
    return run();
  }

public:
  CTScriptExecuter(CTScript* _master);

  virtual ~CTScriptExecuter();
};

////
//class QThread_waitCallback:public QObject
//{  Q_OBJECT;
//  QThread thRunner;
//
//  ICallManager* call;
//  int call_timeout;
//  string callback_number;
//  int result;
//signals:
//  void finished();
//
//protected slots:
//  void run();
//public:
//  virtual ~QThread_waitCallback(){
//    //delete thRunner;
//  }
//public:
//  QThread_waitCallback(ICallManager* _call, int _call_timeout, string _callback_number);
//
//  int getResult();
//};


}

#endif /* CDXETSCRIPT_H_ */
