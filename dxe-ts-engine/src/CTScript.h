/*
 * CTScript.h
 *
 *  Created on: 09.01.2012
 *      Author: Kulpanov
 */

#ifndef CDXETSCRIPT_H_
#define CDXETSCRIPT_H_

#include <QObject>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QTextStream>
#include <QtCore/QSharedPointer>
#include <QtXML/QDomElement>

#include "dxe-ts-utils.h"
#include "dxe-ts-engine.h"


namespace DXE{
using namespace DXE;

class CTScript;
/** \brief CTScriptAction - класс акта скрипта
 * */
class DLL_DECL CTScriptAction {
protected:
  QDomElement source;
  CTScript* script;
protected:
  int id;
  int dir;
protected:
  static const QString& input_side();
  static const QString& output_side();
protected:
  CLogger_ptr logger;
  QString log_message;
public:
  const QString& getLogMessage(){ return log_message;}
  //int actNumber;
public:
  //flags
  virtual bool mustBe_forStandalone(){return false; };
public:
  ///выполнить акт
  virtual bool execute();

  virtual QString toString();

  virtual const QDomElement& toDomElement(){
    return source;
  }

  //virtual QWidget* getWidget(){return NULL;}

  virtual QString getParams(){
    return "";
  }

public:
  virtual QString getDescr();

public:
  CTScriptAction(const QDomElement& _source, CTScript* _script);

  CTScriptAction(const QDomElement& _source, CTScript* _script,
      int _id, int _dir);

  virtual ~CTScriptAction(){  }
public:
  static CTScriptAction* create(
      const QDomElement & e, CTScript* _script, int _actNumber = 0);

};

typedef QSharedPointer<CTScriptAction>  CTScriptAction_ptr;

/** \brief CTScript - класс скрипта
 * */
class DLL_DECL CTScript {
protected:
  QList<CTScriptAction_ptr> listOfAction;
protected:
  //shared pointers
  CLogger_ptr logger;
  ICallService_ptr call_service;
//
  CListOfConnections_ptr connections;
  QVector<int> my_connections;
public:
  //def settings file name
  static const std::string settings_fname;// = "call-server.settings.xml";
  QString log_message;
protected:
  //TODO: он нужен?, этот поток.
  string def_stream;
public:
  const string& getDefStream(){return def_stream;}
  void setDefStream( const string& _stream){def_stream = _stream;}
protected:
  ///Параметр: connect_id по умолчанию
  int def_connect_id;
public:
  int getConnectId(){return def_connect_id;}
protected:
  ///результат работы сценария - тест прошел успешно
  int result;
public:
  bool IsPASS(){return result == EOK;}

public:
  ICallService_ptr getCallService(){return call_service;};

  CLogger_ptr getLoggerChild(int _n){return logger->getChild(_n);};

  CListOfConnections& getConnections();

  CConnection_ptr getConnection(int _id);

  void addConnection(int _id, ICallManager* m1, ICallManager* m2);

  void removeConnection(int _id);

private:
  QString descr;
  QString fname;
public:
  const QString& getDescr(){return descr;};
  const QString& getFileName(){return fname;};
public:
  ///Проверить на возможность запуска
  bool checkForStandalone();
  ///Выполнить сценарий
  void execute();
public:
  ///Загрузить файл сценария
  void loadFile(const QString& _fname);
  ///Загрузить сценарий как текст
  void loadScript(const char* _script);
public:
  ///конструктор для первичного вызова
  CTScript(ICallService_ptr _call_service,
      CListOfConnections_ptr _connections,
      CLogger_ptr _logger);
  ///конструктор для вызова из акта
  CTScript(CTScript* _parent, CLogger_ptr _actLogger, int _connect_id);
public:
  virtual ~CTScript();
public:
  //QSharedPointer<CTScript> self;1
  int ref;
};
//typedef QSharedPointer<CTScript> CTScript_ptr;
typedef CTScript* CTScript_ptr;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
}

#endif /* CDXETSCRIPT_H_ */
