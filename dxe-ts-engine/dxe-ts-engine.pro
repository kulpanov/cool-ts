#-------------------------------------------------
#
# Project created by QtCreator 2012-01-08T18:22:09
#
#-------------------------------------------------

QT       += xml

QT       += gui

TARGET = dxe-ts-engine
TEMPLATE = lib

DEFINES += DXETESTSYSTEM_LIBRARY

SOURCES += src/CTScript.cpp src/baseActions.cpp src/utils/dxe-ts-utils.cpp src/CTScriptExecuter.cpp\
	src/utils/CXMLReader.cpp 

HEADERS += include/dxe-ts-engine.h include/dxe-ts-utils.h src/CTScriptExecuter.h include/CXMLReader.h

QMAKE_CXXFLAGS += -std=c++11

INCLUDEPATH += ../dxe-utils ../dxe-call-server/include ../dxe-streams/include\
  ../dxe-ts-engine/include 
 
QMAKE_LIBDIR += ../dxe/install
#LIBS += -static -ldxe-utils -ldxe-call-server -ldxe-streams
LIBS += -ldxe-utils -ldxe-call-server -ldxe-streams
DESTDIR = ../dxe/install

DEFINES += __GXX_EXPERIMENTAL_CXX0X__ SRCDIR=\\\"$$PWD/\\\"  
