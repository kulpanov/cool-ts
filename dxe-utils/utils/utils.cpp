/** \brief Различные утилиты, реализация
 * utils.cpp
 *
 *  Created on: 05.09.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <ctime>
#include <windows.h>
#include "../libdefine.h"
#include <iostream>

#include "utils.h"
const CString nullStr = "";

//Подсчитать кол-во файлов в каталоге
int getCountOfFilesInDir(const char* _dirname){
  int countOfFiles = 0;
  WIN32_FIND_DATA FindFileData;
  HANDLE hFind;

  hFind = FindFirstFile(_dirname /*"abonents\\*"*/, &FindFileData);
  if (hFind == INVALID_HANDLE_VALUE){
    throw E(GetLastError(), "FindFirstFile failed");
  }
  do{
    if(FindFileData.cFileName[0] != '.')
      countOfFiles ++; //кроме скрытых

  }while(FindNextFile(hFind, &FindFileData) ) ;

  FindClose(hFind);
  return countOfFiles;
}

double jitter_array[] = {
    1
    ,-0.99
    ,-0.47
    ,-0.7
    ,0.85
    ,-0.2
    ,0.11
    ,0.39
    ,-0.79
    ,0.8
    ,0
};
int j_counter = 0;

CTimer_balanced::CTimer_balanced(){
  hTimer = CreateWaitableTimer(NULL, false, NULL);
  if (NULL == hTimer){
    //LOG1(ALRM, "call_process:CreateWaitableTimer failed (%d)\n", (int)GetLastError());
    throw E(GetLastError(), "CreateWaitableTimer failed");
  }
  if(TIMERR_NOERROR != timeBeginPeriod(1)){//set win minimal sheduling period
    //LOG1(ALRM, "call_process:timeBeginPeriod failed (%d)\n", (int)GetLastError());
    throw E(GetLastError(), "timeBeginPeriod failed");
  }
  std::srand(std::time(0));
  QueryPerformanceFrequency(&freq);
  skew = 0;
  quant = 0;//quantCounter = 1000;
  setBasePeriod(60, 0, 0);
  QueryPerformanceCounter(&ms_internal_prev);
}

CTimer_balanced::~CTimer_balanced(){
  //закрыть таймер
  if(hTimer)  CancelWaitableTimer(hTimer);
  timeEndPeriod(1);
}

void CTimer_balanced::setTimer(){
  if (!SetWaitableTimer(hTimer, &next_period, 0, NULL, NULL, 0)){
    //LOG1(ALRM, "SetWaitableTimer failed, %d\n", (int)GetLastError());
    throw E(GetLastError(), "SetWaitableTimer failed");
  }
}

void CTimer_balanced::setBasePeriod(int _basePeriod, int _maxJitter, int _quant){
  basePeriod = _basePeriod*1000*10;

  measures_pos = 0;
  maxJitter = _maxJitter;
  quant = _quant;
  if(quant == 0 || quant == 80000) quant = 0;
  else{
    basePeriod *= (double)8000*10/quant;
  }
// printf("basePeriod=%d\n", basePeriod);
 next_period.QuadPart = -1 * basePeriod;
// measures_ave = basePeriod;
// for(double& item: measures)  item = basePeriod;
 ms_internal_prev.QuadPart = 0;
// next_period.QuadPart += 1*1000*10;//-1ms (next_period is  negative!)
}

LARGE_INTEGER& CTimer_balanced::calc_balance(){
  //балансировка загрузки таймера, что должна делать win
  //частота процессора, для целей балансировки таймера
    int d = 0;
    if(ms_internal_prev.QuadPart == 0){
      QueryPerformanceCounter(&ms_internal);
      d = basePeriod;
    }else
    do{//засеч реальное время между вызовами calc_balance
      //, и ждать точно пока время не сравняется с basePeriod
      QueryPerformanceCounter(&ms_internal);
      d = ((ms_internal.QuadPart-ms_internal_prev.QuadPart)*1000*1000*10)/freq.QuadPart;
      //строго ждем пока расчитанный next_period закончится
      if((d - abs(next_period.QuadPart-999*10)) >= 0) break;
    }while(1);
    ms_internal_prev = ms_internal;
    //считаем на след период
//    {
//      measures_ave -= measures[measures_pos]/16;//выкинуть устаревшше
//      measures_ave += (measures[measures_pos] = d)/16;//добавить новое
//    }
//    if(++measures_pos == 16) measures_pos = 0;

    //рассчитать next_period на след раз, учитывая джиттер
     next_period.QuadPart = -1 * basePeriod;
     next_period.QuadPart += 1*1000*10;//-1ms (next_period is  negative!)
 //d 100ns
     skew += (basePeriod - d);
 ///    cout << ("calc_balance" <<, d, next_period.QuadPart, skew);
 //    d = measures_ave - basePeriod;
     next_period.QuadPart += ( - skew);//укоротить(next_period is  negative!)
     if(abs(basePeriod - d)>20000)
       cout << "T:"<< d << " skew:"<< skew << endl;

    if(maxJitter>0){
      int jitter = jitter_array[j_counter] * 1.8 * maxJitter;
      if(++j_counter == 11) j_counter = 0;
      jitter *= 1000 * 10;
      next_period.QuadPart += jitter;
    }
    if( next_period.QuadPart > 0 )  next_period.QuadPart = 0;
  return next_period;
}
