/*
 * netutils.h
 *
 *  Created on: 26.02.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
//
//#include <winsock2.h>
//#include <pthread.h>
//#include <list>
//
#include <iosfwd>
#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <time.h>

#include "../libdefine.h"
#include "../debug.h"
#include "netutils.h"
#include "utils.h"

/*Теперь таблица, наглядно показывающая структуру WAV файла.
Местоположение  Поле  Описание
0..3 (4 байта)  chunkId   Содержит символы "RIFF" в ASCII кодировке (0x52494646 в big-endian представлении).
 Является началом RIFF-цепочки.
4..7 (4 байта)  chunkSize   Это оставшийся размер цепочки, начиная с этой позиции.
Иначе говоря, это размер файла - 8, то есть, исключены поля chunkId и chunkSize.
8..11 (4 байта)   format  Содержит символы "WAVE" (0x57415645 в big-endian представлении)
12..15 (4 байта)  subchunk1Id   Содержит символы "fmt " (0x666d7420 в big-endian
представлении)
16..19 (4 байта)  subchunk1Size   16 для формата PCM. Это оставшийся размер
подцепочки, начиная с этой позиции.
20..21 (2 байта)  audioFormat   Аудио формат, полный список можно получить здесь.
Для PCM = 1 (то есть, Линейное квантование). Значения, отличающиеся от 1,
обозначают некоторый формат сжатия.
22..23 (2 байта)  numChannels   Количество каналов. Моно = 1, Стерео = 2 и т.д.
24..27 (4 байта)  sampleRate  Частота дискретизации. 8000 Гц, 44100 Гц и т.д.
28..31 (4 байта)  byteRate  Количество байт, переданных за секунду воспроизведения.
32..33 (2 байта)  blockAlign  Количество байт для одного сэмпла, включая все каналы.
34..35 (2 байта)  bitsPerSample   Количество бит в сэмпле.
Так называемая "глубина" или точность звучания. 8 бит, 16 бит и т.д.
36..39 (4 байта)  subchunk2Id   Содержит символы "data"
(0x64617461 в big-endian представлении)
40..43 (4 байта)  subchunk2Size   Количество байт в области данных.
44..  data  Непосредственно WAV-данные.
*/

using std::ifstream;
using std::ios_base;
using std::string;

CPacket_RTP::CPacket_RTP(uint8_t _type, uint16_t _seqNumber, uint32_t _timestamp
    , const uint8_t* _payload, const int _len)
  :base(NULL, _len + 12)
{
  data[0] = 0x80;
  data[1] = _type;
  *((uint16_t*)&data[2]) = htons(_seqNumber);
  *((uint32_t*)&data[4]) = htonl(_timestamp);
  *((uint32_t*)&data[8]) = 0; //sync source id = 0
  memcpy(&data[12], _payload, _len);
};

//Загрузить из wav-файл
int tVoiceMsg::Load_qmsg_fromWAV(tVoiceMsg& _qmsg, const char*  fileName, int packetSize) {
  //printf("%s\n",fileName.c_str());
  wavHeader header;
  //const char* fileName = _fileName.c_str();
  ifstream wavFile;
  wavFile.open(fileName, ios_base::in | ios_base::binary);
  if(!wavFile)
  {
    CLOG1(WARN, "Load_qmsg_fromWAV:%s:File not found\n", NULL, fileName);
    return -1;
  };
  //разбираем header
  //сначала секция RIFF

  wavFile.read(header.riff,4);
  header.chunkSize = 0;

  if(strcmp(header.riff,"RIFF") != 0)
  {
    CLOG1(WARN, "Load_qmsg_fromWAV:%s:no RIFF tag, error struct file\n", NULL, fileName);
    return -2;
  };

  //размер секции
  wavFile.ignore(4);

  wavFile.read(header.format,4);
  header.subchunk1Id[0] = 0;

  //секция WAVE
  if(strcmp(header.format,"WAVE") != 0)
  {
    CLOG1(WARN, "Load_qmsg_fromWAV:%s:no WAV tag, error struct file\n", NULL, fileName);
    return -2;
  };


  //секция fmt
  wavFile.read(header.subchunk1Id,4);
  header.subchunk1Size = 0;

  if(strcmp(header.subchunk1Id,"fmt ") != 0)
  {
    CLOG1(WARN, "Load_qmsg_fromWAV:%s:no WAV tag, error struct file\n", NULL, fileName);
    return -2;
  };

  wavFile.read((char*)&header.subchunk1Size,4);
  if(header.subchunk1Size == 0)
  {
    CLOG1(WARN, "Load_qmsg_fromWAV:%s:no sizeFmt tag, error struct file\n", NULL, fileName);
    return -2;
  };

  //
  wavFile.read((char*)&header.audioFormat,2);
  wavFile.read((char*)&header.numChannels,2);
  wavFile.read((char*)&header.sampleRate,4);
  wavFile.read((char*)&header.byteRate,4);
  wavFile.read((char*)&header.blockAlign,2);
  wavFile.read((char*)&header.bitsPerSample,2);

  wavFile.ignore(header.subchunk1Size - 16);

  //далее может быть или нет секция fact везде пишут что не нужная
  wavFile.read(header.subchunk2Id,4);
  header.subchunk2Size = 0;

  if(strcmp(header.subchunk2Id,"fact") == 0)
  {
    //промежуточная сессия
    int factSize;
    wavFile.read((char*)&factSize,4);
    wavFile.ignore(factSize);

  }else if(strcmp(header.subchunk2Id,"data") == 0){
    wavFile.read((char*)&header.subchunk2Size,4);
  };

  //провевряем может все уже и посчитали
  if(header.subchunk2Size == 0)
  {
    wavFile.read(header.subchunk2Id,4);
    header.subchunk2Size = 0;
    if(strcmp(header.subchunk2Id,"data") == 0){
      wavFile.read((char*)&header.subchunk2Size,4);
    }else{
      CLOG1(WARN, "Load_qmsg_fromWAV:%s:no data tag, error struct file\n", NULL, fileName);
      return -2;
    };
  };

  uint8_t _data[500];
  uint8_t type = 8;
  if(packetSize > CPacket_RTP::length)
    packetSize = CPacket_RTP::length;

  uint32_t timeStamp = 0, seqNumber = 0;

 { int real_len;
   while (! wavFile.eof()) {
      wavFile.read((char*)&_data, packetSize);
      real_len = wavFile.gcount();
//      int i = len - real_len;
//      if(i > 0){
//        memset(&_data[real_len], 0xd5, i);
//        CLOG2(WARN, "Load_qmsg_fromWAV:%s:pad added, size:%d\n", NULL, fileName, i);
//      }
     _qmsg.push_back(new CPacket_RTP(type, seqNumber, timeStamp, _data, packetSize));
     seqNumber++;
     timeStamp += real_len;
   }
  }

  wavFile.close();
  return 0;
}

//Сохранить  в wav-файл
int tVoiceMsg::Save_qmsg_toWAV(tVoiceMsg& _qmsg, const char*  fileName) {

  wavHeader header;

  //количество сообщений на длину
  int dataSize = _qmsg.size() * 480;
  //const char* fileName = _fileName.c_str();
  strcpy( header.riff, "RIFF" );
  header.chunkSize = dataSize + 36; //44 - 8
  strcpy( header.format, "WAVE" );
  strcpy( header.subchunk1Id, "fmt ");
  header.subchunk1Size = 16;
  //1 - стандарт, 6 - это наш кодек
  header.audioFormat = 6;
  header.numChannels = 1;
  header.sampleRate = 8000;
  header.bitsPerSample = 8;
  int bytePerSample = (int) header.bitsPerSample / 8;
  header.byteRate = header.sampleRate * bytePerSample * header.numChannels;
  header.blockAlign = header.numChannels * bytePerSample;

  strcpy( header.subchunk2Id, "data" );
  header.subchunk2Size = dataSize;


  FILE* wavFile = fopen( fileName, "wb" );
  if(NULL == wavFile){
    CLOG2(WARN, "tVoiceMsg::Save_qmsg_toWAV:%s: unable to open file, reason %d\n", NULL, fileName, errno);
    return 0;
  }
  {
    fwrite( &header, sizeof( header ), 1, wavFile );
    CLOG1(TSTL1, "tVoiceMsg::Save_qmsg_toWAV:%s:the header has been saved\n", NULL, fileName);
    //TODO: отсортировать пакеты по timestamp

    //записать всё в файл
    CPacket_RTP *packet;
    while (!_qmsg.empty()) {
      packet = _qmsg.front();
      fwrite(packet->getDataOfPayload(), packet->getLengthOfPayload(), 1, wavFile);
      _qmsg.pop_front();
      delete packet;
    }
    CLOG1(INFO, "tVoiceMsg::Save_qmsg_toWAV:%s: has been saved\n", NULL, fileName);
    _qmsg.clear();
    fflush(wavFile);
    fclose(wavFile);
  };
  return 0;
}

//Сформировать уникальное имя файла в папке
const string&  tVoiceMsg::Get_uniqueName(const string& _prefix, string& res){
  char fileName[0x20];
  long int t = time(NULL);
  FILE* wavFile = NULL;  int indx = 0;
  do{
    if(NULL != wavFile)fclose(wavFile);

    //файл уже существует, изменить имя
    sprintf(fileName, "%s/%ld_%d.wav"
        , _prefix.c_str(),t, indx++);
    //printf(fileName);

    if(indx>100){
      //чёто не так
      CLOG1(ERRR, "tVoiceMsg::Get_uniqueName: unable to get name %s\n", NULL, fileName);
      return res = "noname";
    }
  }while(NULL != (wavFile = fopen( fileName, "rb" ) ));

  return res = fileName;
}

////////////////////////////////////////////////////////////////////////////////
///
//void fCopy(const CString& _from, const CString& _to){
//  try{
//    ifstream from(_from.c_str());
//    if(from.fail()) throw 1;
//
//    ofstream to(_to.c_str());
//    to << from.rdbuf();;
//    to.flush();
//    to.close(); from.close();
//  }catch(std::exception* e){
//    printf("Unable to copy: %s to %s, reason: %s\n", _from.c_str(), _to.c_str(), e->what());
//  }catch(...){
//    printf("Unable to copy: %s to %s, reason: %d\n", _from.c_str(), _to.c_str(), errno);
//  }
//};

