/**
 * \brief Различные утилиты и вспомогательные классы
 * netutils.h
 *
 *  Created on: 26.02.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef NETUTILS_H_
#define NETUTILS_H_

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <list>
#include <pthread.h>
//#include <boost/asio.hpp>

#include "../libdefine.h"
#include "utils.h"

//using std::string;
/** \brief IP-адрес
 * Класс представления IP v4 адреса*/
class CAddr_IPv4{
  protected:
  in_addr ip;
  public:
    uint32_t getIP()const {return ip.S_un.S_addr;}
    const in_addr& getIP_addr()const {return ip;}
    const uint8_t* getIP_i8()const{return (uint8_t*)&(ip.S_un.S_un_b); }
    operator uint32_t()const{return getIP(); }
    operator const uint8_t*()const{return getIP_i8(); }
//    const boost::asio::ip::address_v4 getBoostIPv4()const{
//      return boost::asio::ip::address_v4();
//    }
  public:
    std::string& toString(std::string& _str, int _type = 0)const{
      char buf[24];
      if(1 == _type){
        sprintf(buf, "%lx", (long unsigned int) ip.S_un.S_addr);
      }else{
        sprintf(buf, "%d.%d.%d.%d"
            , getIP_i8()[3], getIP_i8()[2], getIP_i8()[1], getIP_i8()[0]);
      }
      return (_str = buf);
    }

    std::string toString(int _type = 0)const{
      string str;
      return toString(str, _type);
    }
  public:
    CAddr_IPv4()
    { ip.S_un.S_addr = 0x7f000001; };

    CAddr_IPv4(uint32_t _ip)
    { ip.S_un.S_addr = _ip;  };

    CAddr_IPv4(const uint8_t* _ip)
    { memcpy(&(ip.S_un.S_addr), _ip, 4); };

    CAddr_IPv4(uint8_t _ip1, uint8_t _ip2,
               uint8_t _ip3, uint8_t _ip4)
    { ip.S_un.S_un_b.s_b1 =  _ip1;
      ip.S_un.S_un_b.s_b2 =  _ip2;
      ip.S_un.S_un_b.s_b3 =  _ip3;
      ip.S_un.S_un_b.s_b4 =  _ip4;
    };

    CAddr_IPv4(const std::string& _str)
    { uint32_t _ip1, _ip2, _ip3, _ip4;
      sscanf(_str.c_str(), "%d.%d.%d.%d", &_ip4, &_ip3, &_ip2, &_ip1);
      ip.S_un.S_un_b.s_b1 =  _ip1;
      ip.S_un.S_un_b.s_b2 =  _ip2;
      ip.S_un.S_un_b.s_b3 =  _ip3;
      ip.S_un.S_un_b.s_b4 =  _ip4;
    };

//    CAddr_IPv4(const boost::asio::ip::address_v4& _boost_ip)
//    {
//      CAddr_IPv4(_boost_ip.to_ulong());
//    };

    CAddr_IPv4(const CAddr_IPv4& _ip)
    :ip(_ip.ip)
    {   };

    virtual ~CAddr_IPv4(){  };
};
#define IP CAddr_IPv4

/** \brief MAC-адрес
 * Класс представления MAC адреса*/
class CAddr_MAC{
  protected:
    uint8_t mac[6];
  public:
    const uint8_t* getMAC()const {return mac;};
  public:
    std::string& toString(std::string& _str){
      char buf[24];
      sprintf(buf, "%x.%x.%x.%x.%x.%x"
          , getMAC()[5], getMAC()[4], getMAC()[3]
          , getMAC()[2], getMAC()[1], getMAC()[0]);
      return (_str = buf);
    };
  public:
    CAddr_MAC()
    { memset(mac, 0xFF, 6); };

    CAddr_MAC(const uint8_t* _mac)
    { memcpy(mac, _mac, 6);  };

    CAddr_MAC(uint8_t _mac1, uint8_t _mac2,
              uint8_t _mac3, uint8_t _mac4,
              uint8_t _mac5, uint8_t _mac6)
    { mac[0] =  _mac1; mac[1] =  _mac2; mac[2] =  _mac3;
      mac[3] =  _mac4; mac[4] =  _mac5; mac[5] =  _mac6;
    };

    CAddr_MAC(const std::string& _str)
    { uint32_t _mac1, _mac2, _mac3, _mac4, _mac5, _mac6;
      sscanf(_str.c_str(), "%x.%x.%x.%x.%x.%x"
        , &_mac1, &_mac2, &_mac3, &_mac4, &_mac5, &_mac6);

      mac[0] =  _mac1; mac[1] =  _mac2; mac[2] =  _mac3;
      mac[3] =  _mac4; mac[4] =  _mac5; mac[5] =  _mac6;
    };

    CAddr_MAC(const CAddr_MAC& _mac)
    { memcpy(mac, _mac.mac, 6);  };

    virtual ~CAddr_MAC(){  };
};
#define MAC CAddr_MAC

/** \brief UDP-пакет
 * Класс представления UDP пакета*/
class CPacket_UDP{
  protected:
    uint8_t* data;
    int len;
   // bool inHeap;
  public:
    int GetLength()const{return len;}

    const uint8_t* GetData()const{return data;}

    //operator const void*()const{return (void*)GetData(); }

  public:
    CPacket_UDP()
      :len(0)
    {  };

    CPacket_UDP(const uint8_t* _data, const int _len)
      :len(_len)//, inHeap(true)
    {
      data = new uint8_t[len];
      if(NULL != _data)
        memcpy(data, _data, _len);
//      else
//        memset(data, '0', _len);
    };

    virtual ~CPacket_UDP()
    { delete data; };
};
#define UDP_PACKET CPacket_UDP

/** \brief RTP-пакет
 * Класс представления RTP пакета*/
class CPacket_RTP: public CPacket_UDP{
  typedef CPacket_UDP base;
  protected:
  public:
  static const int length = 480;
  public:
    uint8_t getTypeOfPayload()const{return data[1];}
    uint16_t getSeqNumber()const{return ntohs(*((uint16_t*)&data[2]));}
    uint32_t getTimestamp()const{return ntohl(*((uint32_t*)&data[4]));}

    int getLengthOfPayload()const{return len - 12;}
    const uint8_t* getDataOfPayload()const{return &data[12];}

    void setCounts(uint16_t _seqNumber, uint32_t _timestamp){
      *((uint16_t*)&data[2]) = htons(_seqNumber);
      *((uint32_t*)&data[4]) = htonl(_timestamp);
    }

  public:
    CPacket_RTP(const uint8_t* _data, const int _len)
      :base(_data, _len)
    {   };

    CPacket_RTP(const CPacket_UDP* _udp)
      :base(_udp->GetData(), _udp->GetLength())
    {   };

    CPacket_RTP(uint8_t _type, uint16_t _seqNumber, uint32_t _timestamp
        , const uint8_t* _payload, const int _len);

};

/** \brief Очередь RTP-пакетов
 * */
class tVoiceMsg: public std::list<CPacket_RTP*> {
public:
  struct wavHeader{
    char riff[4];
    int chunkSize;
    char format[4];
    char subchunk1Id[4];
    int subchunk1Size;
    short audioFormat;
    short numChannels;
    int sampleRate;
    int byteRate;
    short blockAlign;
    short bitsPerSample;
    char subchunk2Id[4];
    int subchunk2Size;
  };
public:
  tVoiceMsg(const char *_path = NULL){
    if(NULL != _path){
      Load_qmsg_fromWAV(*this, _path, 480);
    }
  }
  virtual ~tVoiceMsg(){
    Clear_qmsg(*this);
  }


public:
  ///Зачистить
  static void Clear_qmsg(tVoiceMsg& _qmsg) {
    while (!_qmsg.empty()) {
      CPacket_RTP *packet = _qmsg.front();
      _qmsg.pop_front();
      delete packet;
    }
    _qmsg.clear();
  }

  ///Сохранить
  /** Сохранить в файл RTP-пакетов
   * @param _qmsg очередь
   * @param fileName имя файла
   */
  static void Save_qmsg(tVoiceMsg & _qmsg, const string&  fileName) {
    CPacket_RTP *packet;
    FILE *rec = fopen(fileName.c_str(), "wb");
    if(NULL == rec){
      CLOG2(6,
          "tVoiceMsg::Save_qmsg:%s: unable to open file, reason %d\n", NULL
          , fileName.c_str(), errno);
      return ;
    }

    while (!_qmsg.empty()) {
      packet = _qmsg.front();
      fwrite(packet->getDataOfPayload(), packet->getLengthOfPayload(), 1, rec);
      _qmsg.pop_front();
      delete packet;
    }
    fflush(rec);
    fclose(rec);
  }

  ///Загрузить
  /** Загрузить из файла RTP-пакетов
   * @param _qmsg очередь
   * @param fileName имя файла
   * @return результат операции, 0 = ок */
  static int Load_qmsg(tVoiceMsg & _qmsg, const string& fileName) {
    FILE* rec_file = fopen(fileName.c_str(), "rb");

    if(NULL == rec_file){
      CLOG2(6,
          "tVoiceMsg::Load_qmsg:%s: unable to open file, reason %d\n", NULL
          , fileName.c_str(), errno);
      return -1;
    }
    uint8_t _data[500];
    uint8_t type = 8;
    uint32_t len = CPacket_RTP::length;
    uint32_t timeStamp = 480, seqNumber = 0;

    while (!feof(rec_file)) {
      fread(_data, len, 1, rec_file);
      _qmsg.push_back(new CPacket_RTP(type, seqNumber, timeStamp, _data, len));
      seqNumber++;
      timeStamp += 480;
    }
    fclose(rec_file);
    return EOK;
  }


  ///Загрузить из wav-файл
  /** Загрузить из wav-файла
   * @param _qmsg очередь
   * @param fileName имя файла
   * @return результат операции, 0 = ок */
  static int Load_qmsg_fromWAV(tVoiceMsg& _qmsg, const char*  fileName, int packetSize);

  ///Сохранить  в wav-файл
  /** Сохранить в wav-файл
   * @param _qmsg очередь
   * @param fileName имя файла
   * @return результат операции, 0 = ок */
  static int Save_qmsg_toWAV(tVoiceMsg& _qmsg, const char*  fileName);

  ///Сформировать уникальное имя файла в папке
  static const string& Get_uniqueName(const string& _prefix, string& res);
};


#define IP_PORT uint16_t

//void fCopy(const CString& _from, const CString& _to);

#endif /* NETUTILS_H_ */
