/** \brief Различные утилиты, объявления
 * utils.h
 *
 *  Created on: 05.09.2011
 *  \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include <stdlib.h>
#include <stdio.h>
#include "../debug.h"

using namespace std;
////////////////////////////////////////////////////////////////////////////////

/*
///{@ Вспмогательные макросы синхринизации
///Залокировать объект
#define LOCK(lock)      pthread_mutex_lock(lock)
///Разлокировать объект
#define UNLOCK(lock)    pthread_mutex_unlock(lock)

///Послать сигнал через condvar
#define SIGNAL(cond)    pthread_cond_signal(cond)
///Заблокировать поток в ожидании ответа на сигнала
#define WAITREPLY(cond, lock) pthread_cond_wait(cond, lock)
///Послать сигнал через condvar и ждать ответа
#define SIGNALandWAITREPLY(cond, lock)    SIGNAL(cond);WAITREPLY(cond, lock);

///Заблокировать поток в ожидании сигнала
#define WAITSIGNAL(cond, lock) pthread_cond_wait(cond, lock)
///Заблокировать поток в ожидании сигнала + timeout
#define WAITSIGNALTIMED(cond, lock, abstime) pthread_cond_timedwait(cond, lock, abstime)
///Заблокировать поток в ожидании сигнала
#define LOCKandWAITSIGNAL(cond, lock) LOCK(lock); WAITSIGNAL(cond, lock)


///Оветить на сигнал
#define REPLY(cond)     pthread_cond_signal(cond)
///Оветить на сигнал
#define REPLYandUNLOCK(cond, lock)    REPLY(cond); UNLOCK(lock);
*/

///@}

#include <windows.h>

class CTimer_balanced{
private:
  HANDLE hTimer;
  double measures[16];
  int measures_pos;
  double measures_ave;
  LARGE_INTEGER ms_internal_prev, ms_internal;
  LARGE_INTEGER freq;
  LARGE_INTEGER next_period;
  int basePeriod;
  int maxJitter;
  int quant;
  int quantCounter;
  int skew;
public:
  CTimer_balanced();

  ~CTimer_balanced();

public:
  void setBasePeriod(int _basePeriod, int _maxJitter, int _quant);

  void setTimer();

  LARGE_INTEGER& calc_balance();

  HANDLE getTimer(){return hTimer;}


};


//строка
///Класс-обёртка для std::string
#define CString std::string

struct cstr
{
  ///ук-ль на данные, для отладочных целей
//  const char* s;
//public:
  static const char* toChar(const CString& str) {
    return str.c_str();
  }

  static int32_t toInt(const CString& str/*, bool _hex = false*/) {
    //TODO: реализовать atoh
//    if(_hex)
//      return atoh(str.c_str());
//    else
    return atoi(str.c_str());
  }

  static double toDouble(const CString& str) {
    return atof(str.c_str());
  }

//  operator const char*(const  CString& rhs) {
//    return toChar(rhs);
//  }
//
//  operator int(const  CString& rhs)  {
//    return toInt(rhs);
//  }
//
//  operator double(const  CString& rhs)  {
//    return toDouble(rhs);
//  }
//
//  operator string() const {
//    return toString();
//  }
//
//  const string toString() const {return (string)(*this);};
  static CString fromDouble(const double &_d) {
    char buf[20];
    sprintf(buf, "%f", _d);
    CString lhs(buf);
    return lhs;
  }

  static CString fromInt(const int _i, bool _hex = false) {
    char buf[20];
    if (!_hex)
      sprintf(buf, "%i", _i);
    else
      sprintf(buf, "0x%x", _i);
    CString lhs(buf);
    return lhs;
  }
};

extern const CString nullStr;
//#define CCHAR (const char*)

class CPacket_RTP;

///** \brief Интерфейс класса wav-потока
// *TODO: перенести с поддержкой Qt
// */
//class IdxeWavStream {
//public:
//
//  virtual void reset() =0;
//
//  virtual void close() =0;
//
//  virtual CPacket_RTP* read() = 0;
//
//  virtual void write(CPacket_RTP* _packet) = 0;
//
//  virtual bool next() = 0;
//
//  virtual bool isEndOfStream() = 0;
//
//public:
//  virtual ~IdxeWavStream (){  }
//
//  virtual IdxeWavStream* release() = 0;
//public:
//  static IdxeWavStream* create(const std::string& _opt){  return NULL;};
//};

/** \brief Подсчитать кол-во файлов в каталоге
 * Считает всё, файлы и подкаталоги, пропускает файлы, что начинаются с '.' - точки
 * @param _dirname имя каталога
 * @return Число файлов/каталогов
 */
int getCountOfFilesInDir(const char* _dirname = ".");


#endif /* UTILS_H_ */
