/** define.h
 * Общие настройки компиляции.
 *
 *  Created on: 21.12.2010
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef LIBDEFINE_H_
#define LIBDEFINE_H_

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include "debug.h"
//#include "utils/utils.h"

#define EOK 0

#ifdef VERSION

#else
///Номер и дата версии
#define VERSION "ver K0.14 "__DATE__" "__TIME__

#endif
/** Сделать.
TODO: сделать, общее
 - vista, win7 - права администратора?

История.
см history.txt
*/

#endif /* DEFINE_H_ */
