/**debug.h
 * Отладочные макросы и журналирование работы
 *  Created on: 17.01.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#ifndef DEBUG_H_
#define DEBUG_H_

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <exception>
#include <stdexcept>
#include <string.h>
#include "libdefine.h"
#include <stdarg.h>

extern int log_level;
extern FILE* log_file;
extern const char* programName;
extern void init_debug(int _log_level, const char* _programName, const char* version
    , const char* _logFile);

extern int flogf (FILE *__stream, const char *__format, ...);

#define ALRM    0
#define ERRR    1
#define WARN    2
#define INFO    3
#define TSTL1   4
#define DEVL1   4
#define TSTL0   5
#define DEVL2   5
#define DEVL3   6
#define DEVL4   7

#ifndef compiletime_log_level
#define compiletime_log_level 6
#endif

#define IFLOG(level)  if(compiletime_log_level>=level)if(log_level>=level)
//&& ((WARN>=level)?fprintf(log_file, "%s:","W"): )
#define LOG(level, p1) CLOG(level, p1, this)
#define CLOG(level, p1, p0)\
  IFLOG(level) flogf(log_file, #level":%s:%s:%s", getTimeOfDay(p0).c_str(), programName, p1)
#define LOG0 LOG

#define LOG1(level, format, p1) CLOG1(level, format, this, p1)
#define CLOG1(level, format, p0, p1)\
    IFLOG(level) flogf((FILE*)log_file,  #level":%s:%s:" format, getTimeOfDay(p0).c_str(), programName, p1)

#define LOG2(level, format, p1, p2)  CLOG2(level, format, this, p1, p2)
#define CLOG2(level, format, p0, p1, p2)\
    IFLOG(level) flogf(log_file,  #level":%s:%s:" format, getTimeOfDay(p0).c_str(), programName, p1, p2)

#define LOG3(level, format, p1, p2, p3) CLOG3(level, format, this, p1, p2, p3)
#define CLOG3(level, format, p0, p1, p2, p3)\
    IFLOG(level) flogf(log_file,  #level":%s:%s:" format, getTimeOfDay(p0).c_str(), programName, p1, p2, p3)

#define LOG4(level, format, p1, p2, p3, p4) CLOG4(level, format, this, p1, p2, p3, p4)
#define CLOG4(level, format, p0, p1, p2, p3, p4)\
    IFLOG(level) flogf(log_file,  #level":%s:%s:" format, getTimeOfDay(p0).c_str(), programName, p1, p2, p3, p4)

#define LOG5(level, format, p1, p2, p3, p4, p5) CLOG5(level, format, this, p1, p2, p3, p4, p5)
#define CLOG5(level, format, p0, p1, p2, p3, p4, p5)\
    IFLOG(level) flogf(log_file,  #level":%s:%s:" format, getTimeOfDay(p0).c_str(), programName, p1, p2, p3, p4, p5)

#define LOG10(level, format, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) CLOG10(level, format, this, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)
#define CLOG10(level, format, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)\
    IFLOG(level) flogf(log_file,  #level":%s:%s:" format, getTimeOfDay(p0).c_str(), programName, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)

#define LOGTIME(level)\
    IFLOG(level) {flogf(log_file, "T%s\n", LogTime(NULL)); fflush(log_file); }
char* LogTime(char* _str);

int getMS();
std::string getTimeOfDay(const void* ptr);
////////////////////////////////////////////////////////////////////////////////
//

class CException: public std::exception
{
  char msg[0x100];
  char msg2[0x100];
  public:
    virtual const char* what() const throw(){
      return msg;
    };

    virtual const char* what2() const throw(){
      return msg2;
      //TODO: реализовать
      std::string s = msg;
      //s = s.substr(s.find(":")+1);
      s = s.substr(0, s.find("\n\tat "));
      return s.c_str();
    };
    virtual int code() const throw(){
      return atoi(msg);
    };
  public:
    CException(const CException& _e)
    { strcpy(msg, _e.msg);
      strcpy(msg2, _e.msg2);
      }

    CException(int _code, int _line, const char* _file, const char* _msg)
    { sprintf(msg, "%d:%s\n\tat %s:%d", _code, _msg, _file, _line);
      sprintf(msg2, "%d:%s", _code, _msg);
    }

    CException(int _code, int _line, const char* _file, const char *_format, ...)
    {
      int offs = 0;
      sprintf(msg+offs, "%d:", _code);

      offs = strlen(msg);
      va_list arglist;
      va_start(arglist, _format);
      vsprintf(msg+offs, _format, arglist);
      va_end(arglist);

      strcpy(msg2, msg+offs);

      offs = strlen(msg);
      sprintf(msg+offs, "\n\tat %s:%d", _file, _line);
    }

    virtual ~CException() throw() { }
};
#define P_EXC  std::exception& //CException&
#define EXC  CException&

#define E(_code, _msg)\
    CException(_code, __LINE__, __FILE__, "%s", _msg)

#define Ev(_code, _format, ...)\
    CException(_code, __LINE__, __FILE__, _format, __VA_ARGS__)

#define E_CATCH(exc)\
  LOG1(ERRR, "%s\n", exc.what());

#ifdef ASSERT
#undef ASSERT
#endif

#define CHECK_NULL(expr)\
  ((expr==NULL) ? throw Ev(0xAAAA, "Assertion failed: %s", #expr): expr)

#define ASSERT(expr)\
    if(! (expr) ) throw Ev(0xAAAA, "Assertion failed: %s", #expr)

#endif
