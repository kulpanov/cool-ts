#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <fstream>
#include <memory>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>
#include <QtXML/QtXML>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "dxe-call-server.h"

#include "dxe-ts-utils.h"
#include "dxe-ts-engine.h"

#include "CTScript.h"//for CScriptAction

using namespace DXE;
//ВНИМАНИЕ: все эхо тесты требуют запущенного "dxe-ts-test echo" !!!
class CTS_engine;

//clas1s CItem:public std::pair<int*, int*>{
//public:
//  virtual ~CItem(){
//    qDebug() << "CItem dtor";
//    delete first;
//    delete second;
//  }
//  CItem(){
//    qDebug() << "CItem default";
//  }
//  CItem(int* _i, int* _j)
//  {
//    first = _i;
//    second = _j;
//    qDebug() << "CItem";
//  }
//
//  CItem(const CItem& _item)
//  :std::pair<int*, int*>(_item)
//  {
//    qDebug() << "CItem copy";
//  }
//
//  CItem(CItem&& _item){
//    qDebug() << "CItem move";
//    *this = _item;
//    _item.first = NULL; _item.second = NULL;
//  }
//};
class CLogListener4: public QObject, public ILogListener{
Q_OBJECT;
public:
virtual void recvMsg(const DXE::SMsg_log& _msg){
  //emit logTo(_msg);
  std::cout << _msg.toString().toUtf8().begin() << std::endl << std::flush;
  //flog  << _msg.toString().toUtf8().begin() << flush;
}
virtual void recvMsg(const QString& _fname, const QString& _fname_script, const QVector<QPair<double, double> >& _vec){

}
//запустить диалог с формы
virtual void runDlg(int , void*){

}
signals:
  void logTo(const SMsg_log& _msg);

public:
  CLogListener4(CTS_engine* _log_receiver);

  static std::string transliterateMsg(const std::string& _msg)
  {
    return _msg;
//    static std::string const Rus[]= "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя";
//    static std::string const Lat[] = {"A", "B", "V", "G", "D", "E", "E", "Zh", "Z", "I", "I", "K",
//      "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "C", "Ch", "Sh", "Sh'", "E",
//      "Yu", "Ya", "a", "b", "v", "g", "d", "e", "e", "zh", "z", "i", "i", "k", "l", "m", "n",
//      "o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "sh'", "'", "i", "'", "e", "yu",
//      "ya"};
//
//    std::string Res;
//    for (auto it = _msg.begin() ; it != _msg.end() ; ++it)
//    {
//      auto Pos = Rus.find(*it);
//      if (Pos != std::string::npos)
//        Res += Lat[Pos];
//      else
//      {
//        if (*it < 0)
//          Res += ' ';
//        else
//          Res += *it;
//      }
//    }
//    return Res;
  }

};

//тесты для call server
class CTS_engine: public QObject {

Q_OBJECT;

public:
  CTS_engine() {

  }

  virtual ~CTS_engine() {
    std::cout << ("press ENTER key\n") << flush;
   // getchar();
  }

private Q_SLOTS:
  void logRecv(const QString& _log, int _rules){
//    c  << _log.toUtf8().begin()
//            << std::string(" with rules ")
//            << hex <<_rules << endl;
//    QDebug qdbg = QDebug(QtDebugMsg);
//    qdbg.setCodec(QTextCodec::codecForUtfText());

//    std::cout << LogListener::transliterateMsg(_log.toUtf8().begin())
//            << "  " << hex <<_rules << endl;
  }

  void initTestCase() {
  }

  void cleanupTestCase() {
  }

  void test_createActions() {
    //создание скрипта, исполнение его
    try {
      //позвонить, отранслировать файл
       QByteArray in(
  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
  "<script name=\"script1\" descr=\"Описание\" >"
  "<action name=\"connect\" id=\"1\" target=\"560388001\" add=\"0\" target_ip=\"192.168.1.5\" />"
  "<action name=\"setPlayer\" id=\"1\" dir=\"0\" type=\"sin\" freq=\"500\" ampl=\"20\" />"
  "<action name=\"listen\" id=\"1\" dir=\"1\" type=\"file_out\" filename=\"11.wav\" />"
  "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"10000\"/>"
  "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
  "</script>");

      //распарсить поток как XML, создать список Actions
      QString errorMsg;
      int errorLine, errorColumn;
      const char *name;
      CTScriptAction_ptr action;

      QDomDocument doc;
      if (false == doc.setContent(in, &errorMsg, &errorLine, &errorColumn)) {
        throw E(-1, "xml");
      }
      QDomElement e = doc.documentElement();
      QDomNode n = e.firstChild();
      QVERIFY(!n.isNull());
      e = n.toElement();
      action = CTScriptAction_ptr(CTScriptAction::create(e, NULL));//name=\"connect\"
      QVERIFY(
        QString(typeid(*action).name()).indexOf("CTScriptAction_connect") > 0 );
      qDebug() << (name = typeid(*action).name());

      //delete action;

      n = n.nextSibling();
      QVERIFY(!n.isNull());
      e = n.toElement();
      action = CTScriptAction_ptr(CTScriptAction::create(e, NULL));//name=\"setPlayer\"
      QVERIFY(
        QString(typeid(*action).name()).indexOf("CTScriptAction_setPlayer") > 0 );
      qDebug() << (name = typeid(*action).name());

      //delete action;

      n = n.nextSibling();
      QVERIFY(!n.isNull());
      e = n.toElement();
      action = CTScriptAction_ptr(CTScriptAction::create(e, NULL));//name=\"listen\"
      QVERIFY(
        QString(typeid(*action).name()).indexOf("CTScriptAction_listen") > 0 );
      qDebug() << (name = typeid(*action).name());
      //delete action;

      n = n.nextSibling();
      QVERIFY(!n.isNull());
      e = n.toElement();
      action = CTScriptAction_ptr(CTScriptAction::create(e, NULL));//name=\"pause\"
      QVERIFY(
        QString(typeid(*action).name()).indexOf("CTScriptAction_pause") > 0 );
      qDebug() << (name = typeid(*action).name());
      //delete action;

      n = n.nextSibling();
      QVERIFY(!n.isNull());
      e = n.toElement();
      action = CTScriptAction_ptr(CTScriptAction::create(e, NULL));//name=\"disconnect\"
      QVERIFY(
        QString(typeid(*action).name()).indexOf("CTScriptAction_disconnect") > 0 );
      qDebug() << (name = typeid(*action).name());
      //delete action;
    } catch (P_EXC e) {
      QFAIL(e.what());
    }
  }

  //CConnection
  void test_createConnection(){

//    std::map<int, CItem> map_item;
//    map_item.insert(pair<int, CItem>(0, CItem(new int(12345), new int(6789))));
//
//    CItem& item = map_item[0];
//    qDebug() << *(item.first) ;
//    qDebug() << *(item.second) ;
//
//    CItem item2 = item;
//
//    CItem item3 = CItem(new int(1), new int(2));
//
//    item2 = item3;
////    CItem& item1 = map_item[1];
////    qDebug() << item1.i;

    ICallService* call_service = ICallService::create("noopts");


//    {
////#define ICallManager  std::shared_ptr<ICallManager>
//      ICallManager* call_out = call_service->createCallManager(ICall::_reserved);
//      ICallManager* call_in = call_service->createCallManager();
//
//      CConnection conn(call_out, call_in);
////      QVERIFY(call_out == conn.call_out);
////      QVERIFY(call_in == conn.call_in);
//      QVERIFY(call_out == conn.call_byDir(0));
//      QVERIFY(call_in == conn.call_byDir(1));
//    }
//
//    CConnection conn1(CConnection(call_out, call_in));
//    QVERIFY(call_out == conn1.first);
//    QVERIFY(call_in == conn1.second);

//    {
//      ICallManager* call_out = call_service->createCallManager(ICall::_reserved);
//      ICallManager* call_in = call_service->createCallManager();
//
//      CListOfConnections connections;
//      connections[0]= CConnection_ptr(new CConnection(call_out, call_in));
//      QVERIFY(call_out == connections[0]->call_byDir(0));
//      QVERIFY(call_in == connections[0]->call_byDir(1));
//      QVERIFY(NULL == connections[1]);
//    }
  }


  void test_script_conn_disconn() {
    //создание скрипта, исполнение его
      {
        static const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
"<indx value=\"1\" />\n"
"<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
"<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
"<SIPin value=\"5060\" name=\"port_in\" />\n"
"<SIPout value=\"5061\" name=\"port_out\" />\n"
"<logLevel value=\"4\" name=\"logLevel\" />\n"
"</General>\n"
"</Params>\n"};
        ofstream f;
        f.open("call-server.settings.xml");
        f << settings1_xml;
        f.close();
      }
      {
        //позвонить, отранслировать файл
        static const char* dxe_test_xml={
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
"<!-- **  -->"
"<script name=\"script\" >"
"<action name=\"connect\" id=\"1\" target=\"001\" add=\"001\" target_ip=\"127.0.0.1\" />"
"<action descr=\"Пауза\" name=\"pause\" pause=\"3000\" />"
"<action name=\"disconnect\" id=\"1\" />"
"</script>"
        };
        ofstream f;
        f.open("dxe-tests/echo/conn_disconn.tst");
        f << dxe_test_xml;
        f.close();
      }
      DXE::CTestScript::init(NULL);
      bool res = DXE::CTestScript::executeFile("dxe-tests/echo/conn_disconn.tst");
      if (false == res) {
        QFAIL("script execute");
        //...
      }
      DXE::CTestScript::release();

  }


  void test_script_conn_call_disconn() {
    //
      {
        static const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
"<indx value=\"1\" />\n"
"<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
"<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
"<SIPin value=\"5060\" name=\"port_in\" />\n"
"<SIPout value=\"5061\" name=\"port_out\" />\n"
"<logLevel value=\"4\" name=\"logLevel\" />\n"
"</General>\n"
"</Params>\n"};
        ofstream f;
        f.open("call-server.settings.xml");
        f << settings1_xml;
        f.close();
      }
      {
        //позвонить, отранслировать файл
        static const char* dxe_test_xml={
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
"<!-- **  -->"
"<script name=\"script\" >"
"<action name=\"connect\" id=\"2\" target=\"380\" add=\"301\" target_ip=\"127.0.0.1\" />"
"<action name=\"call_script\" sname=\"dxe-tests/echo/subscript.tst\" />"
"<action name=\"disconnect\" id=\"2\" />"
"</script>"
        };
        ofstream f;
        f.open("dxe-tests/echo/conn_disconn.tst");
        f << dxe_test_xml;
        f.close();
      }

      {
        //позвонить, отранслировать файл
        static const char* dxe_test_xml={
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
"<!-- **  -->"
"<script name=\"script\" >"
"<action descr=\"Пауза\" name=\"pause\" pause=\"3000\" />"
"</script>"
        };
        ofstream f;
        f.open("dxe-tests/echo/subscript.tst");
        f << dxe_test_xml;
        f.close();
      }

      DXE::CTestScript::init(NULL);
      bool res = DXE::CTestScript::executeFile("dxe-tests/echo/conn_disconn.tst");
      if (false == res) {
        QFAIL("script execute");
        //...
      }
      DXE::CTestScript::release();

  }

  void test_echo_soundout() {
     //создание скрипта, исполнение его
    QProcess echo;
    //echo.start("dxe-ts-test echo");
    //Sleep(6000);

     try {
       {
 static const char* settings1_xml={
   "<?xml version=\"1.0\" ?>\n"
   "<Params>\n"
   "<General name=\"General\">\n"
     "<indx value=\"1\" />\n"
     "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
     "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
     "<SIPin value=\"5060\" name=\"port_in\" />\n"
     "<SIPout value=\"5061\" name=\"port_out\" />\n"
     "<logLevel value=\"4\" name=\"logLevel\" />\n"
   "</General>\n"
   "</Params>\n"
     };

 ofstream f;
 f.open("call-server.settings.xml");
 f << settings1_xml;
 f.close();

       }
       cout << "create the head script" << endl << flush;
       CTestScript::init();

       //позвонить, отранслировать файл
       const char* in = {
     "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
     "<!-- **  -->"
     "<script name=\"script1\" descr=\"Описание\" >"
     "<action name=\"connect\" id=\"1\" target=\"380\" add=\"301\" target_ip=\"127.0.0.1\" />"

     "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"440\" ampl=\"-6\" />"
     "<action name=\"listen\" id=\"1\" dir=\"1\" type=\"sound_out\" />"

     "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"15000\"/>"
     "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
     "</script>"};
       ofstream f_tst;
       f_tst.open("dxe-tests/echo/soundout.tst");
       f_tst << in;
       f_tst.close();

       cout << "first step"  << endl << flush;;
       if (false == CTestScript::executeFile("dxe-tests/echo/soundout.tst")) {
         QFAIL("script execute");
       }

//       cout << "second step"  << endl << flush;;
//       if (false == CTestScript::executeScript(in)) {
//         QFAIL("script execute");
//       }

     } catch (P_EXC e) {
       QFAIL(e.what());
     }
     CTestScript::release();
     echo.kill();
     Sleep(3000);
   }

  void test_echo_sin_file_3times() {
     //создание скрипта, исполнение его
    QProcess echo;
    //echo.start("dxe-ts-test echo");
    //Sleep(6000);

     try {
       {
 static const char* settings1_xml={
   "<?xml version=\"1.0\" ?>\n"
   "<Params>\n"
   "<General name=\"General\">\n"
     "<indx value=\"1\" />\n"
     "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
     "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
     "<SIPin value=\"5060\" name=\"port_in\" />\n"
     "<SIPout value=\"5061\" name=\"port_out\" />\n"
     "<logLevel value=\"4\" name=\"logLevel\" />\n"
   "</General>\n"
   "<Alt>\n"
     "<SIPin value=\"5060\" name=\"port_in\" />\n"
     "<SIPout value=\"5060\" name=\"port_out\" />\n"
   "</Alt>\n"
   "</Params>\n"
     };

 ofstream f;
 f.open("call-server.settings.xml");
 f << settings1_xml;
 f.close();

       }
       cout << "create the head script" << endl << flush;
       CLogListener4* log_listener =
           new CLogListener4(this);

//       connect(this        , SIGNAL(logTo(const QString&, int))
//           , _log_receiver , SLOT(_log_receiver->logRecv(const QString&, int))
//           , Qt::ConnectionType::QueuedConnection);
//       connect(log_listener, SIGNAL(logTo(const QString&, int))
//             , this , SLOT(logRecv(const QString&, int))
//             , Qt::ConnectionType::AutoConnection);
             //, Qt::ConnectionType::QueuedConnection);

       CTestScript::init(log_listener);

       //позвонить, отранслировать файл
       const char* in = {
     "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
     "<!-- **  -->"
     "<script name=\"script1\" descr=\"ТestX_echo\" >"
     "<action name=\"connect\" id=\"1\" target=\"380\" add=\"301\" target_ip=\"127.0.0.1\" />"

     "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"440\" ampl=\"-6\" />"
     "<action name=\"listen\" id=\"1\" dir=\"0\" type=\"file_out\" filename=\"10.wav\" />"
     "<action name=\"setPlayer\"   id=\"1\" dir=\"1\" type=\"sin\" freq=\"440\" ampl=\"-12\" />"
     "<action name=\"listen\" id=\"1\" dir=\"1\" type=\"file_out\" filename=\"11.wav\" />"

     "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"5000\"/>"
     "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
     "</script>"};
       ofstream f_tst;
       f_tst.open("dxe-tests/echo/sin_file.tst");
       f_tst << in;
       f_tst.close();

       cout << "first step"  << endl << flush;;
       if (false == CTestScript::executeFile("dxe-tests/echo/sin_file.tst")) {
         QFAIL("script execute");
       }

       cout << "second step"  << endl << flush;;
       if (false == CTestScript::executeFile("dxe-tests/echo/sin_file.tst")) {
         QFAIL("script execute");
       }

       cout << "third step"  << endl << flush;;
       if (false == CTestScript::executeFile("dxe-tests/echo/sin_file.tst")) {
         QFAIL("script execute");
       }
     } catch (P_EXC e) {
       QFAIL(e.what());
     }
     CTestScript::release();

     echo.kill();
     Sleep(3000);
   }

  void test_echo_sin_file() {
     //создание скрипта, исполнение его
    //QProcess echo;
    //echo.start("dxe-ts-test echo");
    //Sleep(6000);

     try {
       {
 static const char* settings1_xml={
   "<?xml version=\"1.0\" ?>\n"
   "<Params>\n"
   "<General name=\"General\">\n"
     "<indx value=\"1\" />\n"
     "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
     "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
     "<SIPin value=\"5060\" name=\"port_in\" />\n"
     "<SIPout value=\"5061\" name=\"port_out\" />\n"
     "<logLevel value=\"4\" name=\"logLevel\" />\n"
   "</General>\n"
   "</Params>\n"
     };

 ofstream f;
 f.open("call-server.settings.xml");
 f << settings1_xml;
 f.close();
       }

       //позвонить, отранслировать файл
       const char* in(
     "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
     "<!-- **  -->"
     "<script name=\"script1\" descr=\"Описание\" >"
     "<action name=\"connect\" descr=\"соединить\"  "
            "id=\"1\" target=\"001001001\" out=\"001\" src=\"001\" gw=\"001\" target_ip=\"127.0.0.1\" />"

     "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" "
           "type=\"sin\" freq=\"440\" ampl=\"0\" />"
     "<action name=\"listen\" descr=\"fileout\" id=\"1\" dir=\"1\" type=\"file_out\" filename=\"11.wav\" />"
     "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"9000\"/>"
     "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
     "</script>");
       ofstream f_tst;
       f_tst.open("dxe-tests/echo/echo-test.tst");
       f_tst << in;
       f_tst.close();

       CTestScript::init();
     if (false == CTestScript::executeFile("dxe-tests/echo/echo-test.tst")) {
       QFAIL("script execute");
     }

     CTestScript::release();

     } catch (P_EXC e) {
       QFAIL(e.what());
     }
     //echo.kill();
     //Sleep(3000);
   }

  void test_echo_test() {
     //создание скрипта, исполнение его
    //QProcess echo;
    //echo.start("dxe-ts-test echo");
    //Sleep(6000);

     try {
       {
 static const char* settings1_xml={
   "<?xml version=\"1.0\" ?>\n"
   "<Params>\n"
   "<General name=\"General\">\n"
     "<indx value=\"1\" />\n"
     "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
     "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
     "<SIPin value=\"5060\" name=\"port_in\" />\n"
     "<SIPout value=\"5061\" name=\"port_out\" />\n"
     "<logLevel value=\"4\" name=\"logLevel\" />\n"
   "</General>\n"
   "</Params>\n"
     };

 ofstream f;
 f.open("call-server.settings.xml");
 f << settings1_xml;
 f.close();
       }

       //позвонить, отранслировать файл
       const char* in(
     "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
     "<!-- **  -->"
     "<script name=\"script1\" descr=\"эхо-тест\" >"
     "<action name=\"connect\" descr=\"соединить\"  "
           "id=\"1\" target=\"001001001\" out=\"001\" src=\"001\" gw=\"001\" target_ip=\"127.0.0.1\" />"
     "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" "
           "type=\"sin\" freq=\"440\" ampl=\"-6\" />"
     "<action name=\"listen\" id=\"1\" dir=\"1\""
           "type=\"file_out\" filename=\"11.wav\" />"
     "<action name=\"pause\" pause=\"400\" />"
     "<action name=\"test_koef\" descr=\"Измерить\" id=\"1\" dir=\"1\" "
           "mtime=\"3000\" hi=\"1\" low=\"-1\"/>"
     "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
     "</script>");

       ofstream f_tst;
       f_tst.open("dxe-tests/echo/echo-test.tst");
       f_tst << in;
       f_tst.close();

       CTestScript::init();
     if (false == CTestScript::executeFile("dxe-tests/echo/echo-test.tst")) {
       QFAIL("script execute");
     }

     CTestScript::release();

     } catch (P_EXC e) {
       QFAIL(e.what());
     }
     //echo.kill();
     //Sleep(3000);
   }

  void test_echo_test2() {
     //создание скрипта, исполнение его
    //QProcess echo;
    //echo.start("dxe-ts-test echo");
    //Sleep(6000);

     try {
       {
 static const char* settings1_xml={
   "<?xml version=\"1.0\" ?>\n"
   "<Params>\n"
   "<General name=\"General\">\n"
     "<indx value=\"1\" />\n"
     "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
     "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
     "<SIPin value=\"5060\" name=\"port_in\" />\n"
     "<SIPout value=\"5061\" name=\"port_out\" />\n"
     "<logLevel value=\"4\" name=\"logLevel\" />\n"
   "</General>\n"
   "</Params>\n"
     };

 ofstream f;
 f.open("call-server.settings.xml");
 f << settings1_xml;
 f.close();
       }

       //позвонить, отранслировать файл
       const char* in(
     "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
     "<!-- **  -->"
     "<script name=\"script1\" descr=\"Описание\" >"
     "<action name=\"connect\" id=\"1\" target=\"380\" add=\"301\" target_ip=\"127.0.0.1\" />"

     "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" "
               "type=\"sin\" freq=\"440\" ampl=\"0\" />"
     "<action name=\"test\" descr=\"Измерить\" id=\"1\" dir=\"1\" "
           "mtime=\"3000\" hi=\"1\" low=\"0\"/>"

     "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"15000\"/>"
     "<action name=\"test\" descr=\"Измерить\" id=\"1\" dir=\"1\" "
          "mtime=\"3000\" hi=\"1\" low=\"0\"/>"

     "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
     "</script>");
       ofstream f_tst;
       f_tst.open("dxe-tests/echo/echo-test2.tst");
       f_tst << in;
       f_tst.close();

       CTestScript::init();
     if (false == CTestScript::executeFile("dxe-tests/echo/echo-test2.tst")) {
       QFAIL("script execute");
     }

     CTestScript::release();

     } catch (P_EXC e) {
       QFAIL(e.what());
     }
     //echo.kill();
     //Sleep(3000);
   }

  //echo-sin-file
  void test_echo_sin_file_test() {
     //создание скрипта, исполнение его
    //QProcess echo;
    //echo.start("dxe-ts-test echo");
    //Sleep(6000);

       {  static const char* settings1_xml={
           "<?xml version=\"1.0\" ?>\n"
           "<Params>\n"
           "<General name=\"General\">\n"
             "<myIP value=\"192.168.233.222\" name=\"IP\" />\n"
             "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
             "<SIPin value=\"5060\" name=\"port_in\" />\n"
             "<SIPout value=\"5061\" name=\"port_out\" />\n"
             "<realIP value=\"127.0.0.1\" />\n"
             "<gwIP value=\"192.168.233.22\" />\n"
             "<logLevel value=\"4\" name=\"logLevel\" />\n"
           "</General>\n"
           "</Params>\n"
             };

         ofstream f;
//         f.open("call-server.settings.xml");
//         f << settings1_xml;
//         f.close();
       }
       //CLogListener4* log_listener = new CLogListener4(this);
       CTestScript::init(new CLogListener4(this));

       const int t_count = 20;//дб не более кол-ву потоков эхо-сервера
       for(int j=1; j<50; j++){
       boost::thread_group tg;
       for(int i=0; i<t_count; i++){
         tg.create_thread(boost::bind<void>(
             [this](int _i){
         try {
           string str_i = boost::lexical_cast<std::string, int>(_i+1);
           //позвонить, отранслировать файл
           const string in(
         "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
         "<!-- **  -->"
         "<script name=\"script1\" descr=\"Описание\" >"
         "<action name=\"connect\" id=\""+str_i+"\" target=\""+str_i+"\" add=\""+str_i+"\" target_ip=\"192.168.1.3\" />"
         "<action name=\"play\" id=\""+str_i+"\" dir=\"0\" type=\"sin\" freq=\"440\" ampl=\"-"+str_i+"\" />"
         "<action name=\"listen\" id=\""+str_i+"\" dir=\"1\" type=\"file_out\" filename=\""+str_i+".wav\" />"
         "<action name=\"pause\" descr=\"Пауза\" id=\""+str_i+"\" pause=\"5000\"/>"
         "<action name=\"disconnect\" descr=\"Прервать связь\" id=\""+str_i+"\" />"
         "</script>");

           string script_fname = "dxe-tests/echo/echo_sin_file_test_"+str_i+".tst";
           ofstream f_tst;
           f_tst.open(script_fname.c_str());
           f_tst << in;
           f_tst.close();

           if (false == CTestScript::executeFile(script_fname.c_str())) {
             QFAIL("script execute");
           }
         } catch (P_EXC e) {
           QFAIL(e.what());
         }
             }, i));
       }
     tg.join_all();
     Sleep(3000);
//тест размеров файлов
     {using namespace boost::filesystem;
       path p ("tmp");
      try{
        for (directory_iterator dir_itr(p); dir_itr != directory_iterator(); ++dir_itr)
          if (is_regular_file(dir_itr->status()))
            if(file_size(dir_itr->path())<20000){
              std::cout << "we've some troubles: " << dir_itr->path().filename() << file_size(dir_itr->path()) << endl;
              j = 100;
            }
      }catch (const exception& ex)
      {
        cout << ex.what() << '\n';
      }
       }//namespace
       }
     CTestScript::release();

     //echo.kill();
     //Sleep(3000);
   }

  void test_echo_disconn_all() {
     //создание скрипта, исполнение его
     try {
       {
 static const char* settings1_xml={
   "<?xml version=\"1.0\" ?>\n"
   "<Params>\n"
   "<General name=\"General\">\n"
     "<indx value=\"1\" />\n"
     "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
     "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
     "<SIPin value=\"5060\" name=\"port_in\" />\n"
     "<SIPout value=\"5061\" name=\"port_out\" />\n"
     "<logLevel value=\"4\" name=\"logLevel\" />\n"
   "</General>\n"
   "</Params>\n"
     };

 ofstream f;
 f.open("call-server.settings.xml");
 f << settings1_xml;
 f.close();
       }

       //позвонить, отранслировать файл
       const char* in(
     "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
     "<!-- **  -->"
     "<script name=\"script1\" descr=\"Описание\" >"
     "<action descr=\"Разъединить всех\" name=\"disconnect_all\" />"
     "</script>");
       ofstream f_tst;
       f_tst.open("dxe-tests/echo/disconn_all.tst");
       f_tst << in;
       f_tst.close();

       CTestScript::init(NULL);
       bool res = DXE::CTestScript::executeFile("dxe-tests/echo/disconn_all.tst");
       if (false == res) {
         QFAIL("script execute");
       }
       CTestScript::release();

     } catch (P_EXC e) {
       QFAIL(e.what());
     }
     //echo.kill();
     //Sleep(3000);
   }

  void testX() {
    //создание скрипта, исполнение его
    try {
      {
static const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
  "<indx value=\"1\" />\n"
  "<myIP value=\"192.168.1.4\" name=\"IP\" />\n"
  "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
  "<SIPin value=\"5060\" name=\"port_in\" />\n"
  "<SIPout value=\"5060\" name=\"port_out\" />\n"
  "<logLevel value=\"4\" name=\"logLevel\" />\n"
"</General>\n"
"</Params>\n"};
ofstream f;
f.open("call-server.settings.xml");
f << settings1_xml;
f.close();
      }

      //позвонить, отранслировать файл
      QByteArray in(
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
    "<!-- **  -->"
    "<script name=\"script1\" descr=\"Описание\" >"
    "<action name=\"connect\" id=\"2\" target=\"560388001\" add=\"301\" target_ip=\"192.168.1.5\" />"
//
//    //"<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"200\" ampl=\"-6\" />"
//    //"<action name=\"listen\" id=\"1\" dir=\"0\" type=\"file_out\" filename=\"10.wav\" />"
//
//    "<action name=\"listen\" id=\"1\" dir=\"1\" type=\"file_out\" filename=\"11.wav\" />"
//
//    "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"200\" ampl=\"-12\" />"
////    "<action name=\"test\" descr=\"Измерить\" id=\"1\" dir=\"0\" "
////          "mtime=\"3000\" hi=\"1.1\" low=\"0.9\"/>"
//    "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"3000\"/>"
//
//    "<action name=\"stopPlay\"   id=\"1\" dir=\"0\" />"
//    "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"440\" ampl=\"-6\" />"
////    "<action name=\"test\" descr=\"Измерить\" id=\"1\" dir=\"0\" "
////          "mtime=\"3000\" hi=\"1.1\" low=\"0.9\"/>"
//    "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"3000\"/>"
//    "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
    "</script>");

//TODO: вернуть тест
//      qDebug() << "start";
//      DXE::CTScript script;
//      script.loadScript(in);
//      if (false == script.execute()) {
//        QFAIL("script execute");
//      }
//      qDebug() << "stop";

    } catch (P_EXC e) {
      QFAIL(e.what());
      LOG(INFO, "exception");
    }
  }

  void testX_file() {
    //создание скрипта, исполнение его
      {
        static const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
"<indx value=\"1\" />\n"
"<myIP value=\"192.168.1.2\" name=\"IP\" />\n"
"<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
"<SIPin value=\"5060\" name=\"port_in\" />\n"
"<SIPout value=\"5060\" name=\"port_out\" />\n"
"<logLevel value=\"5\" name=\"logLevel\" />\n"
"</General>\n"
"</Params>\n"};
        ofstream f;
        f.open("call-server.settings.xml");
        f << settings1_xml;
        f.close();
      }
      {
        //позвонить, отранслировать файл
        static const char* dxe_test_xml={
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
"<!-- **  -->"
"<script name=\"script1\" descr=\"Описание\" >"
"<action name=\"connect\" target_ip=\"192.168.1.5\" target=\"820*-828002\" out=\"820\" descr=\"Установить соединение\" id=\"2\" src=\"388\" gw=\"*-828\"/>"
//"<action name=\"connect\" id=\"1\" target=\"820*-828001\" add=\"301\" target_ip=\"192.168.1.5\" />"
//"<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"200\" ampl=\"-12\" />"
//"<action name=\"listen\" id=\"1\" dir=\"1\" type=\"file_out\" filename=\"11.wav\" />"
//"<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"20000\"/>"
//"<action name=\"test\" descr=\"Измерить\" id=\"1\" dir=\"1\" "
//    "pause =\"20000\" mtime=\"100\" hi=\"1.1\" low=\"0.9\"/>"
"<action descr=\"Пауза\" id=\"2\" name=\"pause\" pause=\"5000\"/>"
"<action name=\"disconnect\" descr=\"Прервать связь\" id=\"2\" />"
"</script>"
        };
        ofstream f;
        f.open("dxe-tests/1.tst.xml");
        f << dxe_test_xml;
        f.close();
      }
      DXE::CTestScript::init(new CLogListener4(this));

      bool res = DXE::CTestScript::executeFile("dxe-tests/1.tst.xml");
      if (false == res) {
        QFAIL("script execute");
        //...
      }

      DXE::CTestScript::release();
  }

  void testX_par() {
    //создание скрипта, исполнение его
    try {
      {
static const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
  "<indx value=\"1\" />\n"
  "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
  "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
  "<SIPin value=\"5060\" name=\"port_in\" />\n"
  "<SIPout value=\"5061\" name=\"port_out\" />\n"
  "<logLevel value=\"4\" name=\"logLevel\" />\n"
"</General>\n"
"</Params>\n"};
ofstream f;
f.open("call-server.settings.xml");
f << settings1_xml;
f.close();
      }

      //позвонить, отранслировать файл
      QByteArray in(
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
    "<!-- **  -->"
    "<script name=\"script1\" descr=\"Описание\" >"
    "<action name=\"connect\" id=\"1\" target=\"560388001\" add=\"301\" target_ip=\"192.168.1.5\" />"
//
//    //"<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"200\" ampl=\"-6\" />"
//    //"<action name=\"listen\" id=\"1\" dir=\"0\" type=\"file_out\" filename=\"10.wav\" />"
//
//    "<action name=\"listen\" id=\"1\" dir=\"1\" type=\"file_out\" filename=\"11.wav\" />"
//
//    "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"200\" ampl=\"-12\" />"
////    "<action name=\"test\" descr=\"Измерить\" id=\"1\" dir=\"0\" "
////          "mtime=\"3000\" hi=\"1.1\" low=\"0.9\"/>"
//    "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"3000\"/>"
//
//    "<action name=\"stopPlay\"   id=\"1\" dir=\"0\" />"
//    "<action name=\"setPlayer\"   id=\"1\" dir=\"0\" type=\"sin\" freq=\"440\" ampl=\"-6\" />"
////    "<action name=\"test\" descr=\"Измерить\" id=\"1\" dir=\"0\" "
////          "mtime=\"3000\" hi=\"1.1\" low=\"0.9\"/>"
//    "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"3000\"/>"
//    "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
    "</script>");

//TODO: вернуть тест
      qDebug() << "start";
//      DXE::CTScript script;
//      script.loadScript(in);
//      if (false == script.execute()) {
//        QFAIL("script execute");
//      }
      qDebug() << "stop";

    } catch (P_EXC e) {
      QFAIL(e.what());
      LOG(INFO, "exception");
    }
  }

};


CLogListener4::CLogListener4(CTS_engine* _log_receiver){
  connect(this, SIGNAL(logTo(const SMsg_log& _msg)), _log_receiver , SLOT(onLog(const SMsg_log& _msg))
      , Qt::ConnectionType::QueuedConnection);
}

int main_test4(int argc, char *argv[]) {
  CTS_engine tc;
  return QTest::qExec(&tc, argc, argv);
}

#include "test4.moc"
