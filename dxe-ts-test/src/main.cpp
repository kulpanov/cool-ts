#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <boost/asio.hpp>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "dxe-call-server.h"


#include "debug.h"

#include "dxe-ts-engine.h"

extern int main_test1(int argc, char *argv[]);
extern int main_test2(int argc, char *argv[]);
extern int main_test3(int argc, char *argv[]);
extern int main_test4(int argc, char *argv[]);
extern int main_test5(int argc, char *argv[]);
extern int main_test6(int argc, char *argv[]);
extern int main_echo(int argc, char *argv[]);
extern int main_echo_thread(int argc, char *argv[]);
extern int main_listen(int argc, char *argv[]);


int main(int argc, char *argv[])
{
//  WSADATA lpWSAData;
//  if (WSAStartup(MAKEWORD(2,2), &lpWSAData)!=0) {
//     //throw E(WSAGetLastError(), "VMS::CVoiceMailService:WSAStartup");
//
// }

  if(argc > 1){
  if(0 == strcmp(argv[1], "echo")){
    return main_echo_thread(argc, argv);
  }
  if(0 == strcmp(argv[1], "listen")){
    return main_listen(argc, argv);
  }
  char* argv_1 = argv[1];
  argc--; argv = &argv[1];
  QCoreApplication app(argc, argv);
  char deflogname[0x100];
  sprintf(deflogname, "%s.%s.log", argv[1], "ts");
//  QFile::remove("ts.log");
  init_debug(TSTL1, "ts", " ", argv[1]);
  if(0 == strcmp(argv_1, "1")){
    return main_test1(argc, argv);
  }
  if(0 == strcmp(argv_1, "2")){
    return main_test2(argc, argv);
  }
  if(0 == strcmp(argv_1, "3")){
    return main_test3(argc, argv);
  }
  if(0 == strcmp(argv_1, "4")){
    return main_test4(argc, argv);
  }
  if(0 == strcmp(argv_1, "5")){
    return main_test5(argc, argv);
  }
  if(0 == strcmp(argv_1, "6")){
    return main_test6(argc, argv);
  }
  }
  QCoreApplication app(argc, argv);
  QFile::remove(" TS.log");
  init_debug(TSTL1, "TS", " ", " ");
  main_test1(argc, argv);
  main_test2(argc, argv);
  main_test3(argc, argv);
  main_test4(argc, argv);
  main_test5(argc, argv);
  main_test6(argc, argv);
  return 0;
}
