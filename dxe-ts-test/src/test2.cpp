#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "dxe-call-server.h"
#include "dxe-ts-engine.h"
#include "dxe-ts-utils.h"

using namespace DXE;
using std::isdigit;


void parseCallNumber(CString& _number, CString& _dtmfNumber){
  //"1234*-5678-12"
  unsigned int starPos = _number.find('*');
  if(starPos == string::npos){
    _dtmfNumber = "";
    return;
  }
  _dtmfNumber = _number.substr(starPos + 1, string::npos);
  _number = _number.substr(0, starPos);
}

//[-,0-9]
map<char, IWavStream*> setPlayerList;
void loadDTMFs(){
string strerror;
  setPlayerList['-']
    = IWavStream::create(
        "<item type=\"file_in\" filename=\"wavs/dtmf-.wav\" />", strerror);

  setPlayerList['0']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf0.wav\" />", strerror);
  setPlayerList['1']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf1.wav\" />", strerror);
  setPlayerList['2']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf2.wav\" />", strerror);
  setPlayerList['3']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf3.wav\" />", strerror);
  setPlayerList['4']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf4.wav\" />", strerror);
  setPlayerList['5']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf5.wav\" />", strerror);
  setPlayerList['6']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf6.wav\" />", strerror);
  setPlayerList['7']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf7.wav\" />", strerror);
  setPlayerList['8']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf8.wav\" />", strerror);
  setPlayerList['9']
     = IWavStream::create(
         "<item type=\"file_in\" filename=\"wavs/dtmf9.wav\" />", strerror);
}

void sendDTMF(const CString& _dtmfNumbers){
  CString::const_iterator it = _dtmfNumbers.begin();
  for(;it!=_dtmfNumbers.end();it++){
    if( !isdigit(*it) && (*it != '-'))
      continue; //всё остальное игнорим

//    IWavStream* dtmfSignal = setPlayerList[*it];
//    setPlayer(dtmfSignal);
//    setPlayerNTimes(2);

    //Sleep(100);//между действиями 100мс
  }
}

class CTS_tester: public QObject {
  Q_OBJECT

public:
  CTS_tester(){

  }

  virtual ~CTS_tester(){
    //printf("press ENTER key\n");
    //getchar();
  }


private Q_SLOTS:
void init(){

}

void cleanup(){

}

void test_CWavTester() {
  string strerror;
  try{
//  IWavStream* stream_forTest = IWavStream::create("<s type=\"sin\" freq=\"440\" ampl=\"-30\"/>");
    for(int i=0; i<61; i+=5){
      QString s = QString("<s type=\"sin\" freq=\"1000\" ampl=\"-%0\"/>").arg(i);

  IWavStream* stream_forTest = IWavStream::create(s.toStdString(), strerror);

  IWavStream* stream_gauge = IWavStream::create("<s type=\"sin\" freq=\"440\" ampl=\"-10\"/>", strerror);

  CWavTester tester(stream_gauge, stream_forTest, 2000);
  bool res = tester.test2(1000);
  //QVERIFY(res);
  auto err = tester.getMaxError();
  auto mindB = tester.getForTestMinDb();
  auto maxdB = tester.getForTestMaxDb();
  QString pattern =  QString()
      .fromUtf8("err=%0dB, test mindB:%1dB, maxdB:%2dB, res = %3")
      .arg(err, 0, 'f', 1)
      .arg(mindB, 0, 'f', 1)
      .arg(maxdB, 0, 'f', 1)
      .arg(res);

      qDebug()<<(pattern);
    }
  }catch(P_EXC e){
    //E_CATCH(e);
    QFAIL(e.what());
  }

}

void test_dtmf_signals() {

  try{
    {
    CString number = "1234*-5678-12",dtmfNumber;
    parseCallNumber(number, dtmfNumber);
    QVERIFY(number == "1234");
    QVERIFY(dtmfNumber == "-5678-12");
    }

    {
    CString number = "1234",dtmfNumber;
    parseCallNumber(number, dtmfNumber);
    QVERIFY(number == "1234");
    QVERIFY(dtmfNumber == "");
    }

  }catch(...){
    //E_CATCH(e);
    QFAIL("uncatched exception");
  }

}
void test_dtmf_signals_setPlayer() {

  try{
    QVERIFY(setPlayerList.size() == 0);
    loadDTMFs();
    QVERIFY(setPlayerList.size() == 11);

    sendDTMF("123456789-0");

  }catch(...){
    //E_CATCH(e);
    QFAIL("uncatched exception");
  }

}
};

int main_test2(int argc, char *argv[]){
  CTS_tester tc;
  return QTest::qExec(&tc, argc, argv);
}

#include "test2.moc"
