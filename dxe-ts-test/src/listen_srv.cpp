/*
 * echo_sip.cpp
 *
 *  Created on: 29.02.2012
 *      Author: Kulpanov
 */
#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <fstream>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtCore/QCoreApplication>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "utils/utils.h"

#include "dxe-call-server.h"


static const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
  "<indx value=\"1\" />\n"
  "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
  "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
  "<SIPin value=\"5061\" name=\"port_in\" />\n"
  "<SIPout value=\"5060\" name=\"port_out\" />\n"
  "<logLevel value=\"4\" name=\"logLevel\" />\n"
"</General>\n"
"</Params>\n"
    };

#include "debug.h"

using namespace DXE;

int main_listen(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);

  QFile::remove("TS-echo.log");

  init_debug(INFO, "TS-echo", " ", "");
    if("settings.xml.h"){
      ofstream f;
      f.open("ts-echo.settings.xml");
      f<<settings1_xml;
      f.close();
    }

    std::string listen_name
      = "<s type=\"echo\" filename=\"echo.raw\" />\n";
    std::string setPlayer_name
      = "<s type=\"echo\" text=\"this is echo!!!\" />\n";

    ICallService* call_service = NULL;
    try{
      call_service = ICallService::create("no_opts");
      call_service->start("ts-echo.settings.xml");

      do{
        ICallManager* call_in = call_service->createCallManager();
        cout << "i'm ready, wait for call";
        cout << endl << flush;
        call_in->waitforCall();

        cout << "callin is ok";
        cout << call_in->getConnectNumber() << " ";
        cout << call_in->getConnectDigits() << " ";
        cout << call_in->getConnectIP().toString();
        cout << endl << flush;

        if(EOK == call_in->waitforClose(WSEC(60))){
        }else{
          cout << "time out for callin";
          cout << endl << flush;
        }


        call_in->release();//удаляяет также запись в call_service

        cout << "echo session completed, next turn";
        cout << endl << flush;
      }while(1);
      }catch(P_EXC e){
        CLOG(ERRR, e.what(), NULL);
    }
    if(call_service)
      call_service->release();
    return 0;
}
