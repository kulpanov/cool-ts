/*
 * echo_sip.cpp
 *
 *  Created on: 29.02.2012
 *      Author: Kulpanov
 */
#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <fstream>

#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtCore/QCoreApplication>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "utils/utils.h"

#include "dxe-call-server.h"


static const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
  "<indx value=\"1\" />\n"
  "<myIP value=\"192.168.233.22\" name=\"IP\" />\n"
  "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
  "<SIPin value=\"5061\" name=\"port_in\" />\n"
  "<SIPout value=\"5060\" name=\"port_out\" />\n"
  "<realIP value=\"127.0.0.1\" />\n"
  "<gwIP value=\"192.168.233.222\" />\n"
  "<logLevel value=\"4\" name=\"logLevel\" />\n"
"</General>\n"
"</Params>\n"
    };
/* for real connections:
 *"<myIP value=\"192.168.1.3\" name=\"IP\" />\n"
  "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
  "<SIPin value=\"5060\" name=\"port_in\" />\n"
  "<SIPout value=\"5060\" name=\"port_out\" />\n"
 * */
#include "debug.h"

using namespace DXE;
ICallService* call_service = NULL;

//echo 30
int main_echo_thread(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);

  QFile::remove("TS-echo.log");
  int t_count = 1;
  if(argc == 3){
    t_count = atoi(argv[2]);
    if(t_count>30) t_count = 30;
  }

  init_debug(INFO, "TS-echo", " ", "");
  CLOG(INFO, "start test\n", NULL);
    if("settings.xml.h"){
//      ofstream f;
//      f.open("ts-echo.settings.xml");
//      f<<settings1_xml;
//      f.close();
    }

    try{
      call_service = ICallService::create("no_opts");
      call_service->start("ts-echo.settings.xml");

      boost::thread_group tg;
      for(int i=0; i<t_count; i++){
        tg.create_thread(boost::bind<void>(
            [](int _i){
        try{
          string strerror;
            do{
               ICallManager* call_in = call_service->createCallManager();
               ICallManager* call_out = call_service->createCallManager(ICallManager::_reserved);
               CLOG2(INFO, "begin, step0: call_in=%p, call_out=%p\n", (void*)_i, call_in, call_out);

               int pair_id = _i + 0;
               { string str_i = boost::lexical_cast<std::string, int>(pair_id);
                 call_in->setListener(IWavStream::create(
                     "<s type=\"echo\" id=\"" + str_i + "\" />\n", strerror ));
                 call_out->setPlayer(IWavStream::create(
                     "<s type=\"echo\" id=\"" + str_i + "\" />\n", strerror ));
               }
               pair_id += 30;
               { string str_i = boost::lexical_cast<std::string, int>(pair_id);
                 call_out->setListener(IWavStream::create(
                     "<s type=\"echo\" id=\"" + str_i + "\" />\n", strerror ));
                 call_in->setPlayer(IWavStream::create(
                     "<s type=\"echo\" id=\"" + str_i + "\" />\n", strerror ));
               }
               cout << boost::this_thread::get_id()<< ": i'm ready,wait for call" << endl << flush;
               CLOG2(INFO, "step1: call_in->waitforCall() call_in=%p, call_out=%p\n", (void*)_i, call_in, call_out);
               call_in->waitforCall();

               cout << boost::this_thread::get_id()<<": callin is ok, doing echo callout to: ";
               cout << call_in->getConnectNumber()<< " ";
               cout << call_in->getConnectDigits()<< " ";
               cout << call_in->getConnectIP().toString() << endl << flush;

        //       string callback_number = call_in->getConnectNumber();
        //       callback_number.substr(callback_number.size() - 3);
               boost::this_thread::sleep(boost::posix_time::milliseconds(9500));
               CLOG2(INFO, "step2: call_in=%p, call_out=%p\n", (void*)_i, call_in, call_out);
               CLOG3(INFO, "step2: call_out->call(%s, %s, %s)\n", (void*)_i, call_in->getConnectDigits().c_str(), call_in->getConnectDigits().c_str(), call_in->getConnectIP().toString().c_str());
               call_out->call(
                   //call_in->getConnectNumber(),
                   call_in->getConnectDigits(),
                   call_in->getConnectDigits(),  INFINITE
                 , call_in->getConnectIP());

               if(EOK == call_out->waitforCall(WSEC(10))){
                 cout << boost::this_thread::get_id()<<": callout is ok, wait for close"<< endl << flush;

                 CLOG(INFO, "step3: call_in->waitforClose(WSEC(60))\n", (void*)_i);
                 call_in->waitforClose(WSEC(30), ICallManager::andReserv);
                 CLOG(INFO, "step4: call_in->waitforClose() completed\n", (void*)_i);
               }else{
                 cout << boost::this_thread::get_id() << ": time out for out call" << endl << flush;
                 CLOG(INFO, "step3: time out for out call", (void*)_i);
               }
               //Sleep(20);

               CLOG2(INFO, "step5: release call_in=%p, call_out=%p\n", (void*)_i, call_in, call_out);
               call_out->release();
               call_in->release();//удаляяет также запись в call_service
               cout << boost::this_thread::get_id()<<": echo session completed, next turn" << endl << flush;
            }while(1);
        }catch(P_EXC e){
          cout <<"fault:" << (e.what())<< endl << flush;
        }
        }, i));
      }
      tg.join_all();
      }catch(P_EXC e){
      //E_CATCH(e);
      CLOG(ERRR, e.what(), NULL);
    }
    if(call_service) call_service->release();
    return 0;
}

