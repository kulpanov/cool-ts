#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>
#include <QDomDocument>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
//#include "dxe-call-server.h"
#include "dxe-ts-engine.h"
#include "wavStreams.h"

//#define MAKEWORD(low, high) ((int8_t)((((int16_t)(high)) << 8) | ((int8_t)(low))))

QByteArray ConvertToALaw(QByteArray _pcm){
#define __INT12_MAX__  4096

  QByteArray res; int len = _pcm.size()/2; char* data = _pcm.data();
  for(int i = 0 ; i < len; i++){
    short value = MAKEWORD(data[i * 2], data[i * 2 +1]);
    res.append(l2a(value));
  }
  return res;
}

class CTS_stream: public QObject {
Q_OBJECT
string strerror;
public:
  CTS_stream(){
    printf("contstruct\n");
  }

  virtual ~CTS_stream(){
    printf("press ENTER key\n");
    //getchar();
  }

private Q_SLOTS:
  void initTestCase(){
    printf("init\n");
  }

  void cleanupTestCase(){
    printf("cleanup\n");
  }

  void test1() {
      //создание скрипта, исполнение его
      try {
        //позвонить, отранслировать файл
         QByteArray in(
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
    "<script name=\"script1\" descr=\"Описание\" >"
    "<action name=\"connect\" id=\"1\" target=\"560388001\" add=\"0\" target_ip=\"192.168.1.5\" />"
    "<action name=\"setPlayer\" id=\"1\" dir=\"0\" type=\"sin\" freq=\"500\" ampl=\"20\" />"
    "<action name=\"listen\" id=\"1\" dir=\"1\" type=\"file_out\" filename=\"11.wav\" />"
    "<action name=\"pause\" descr=\"Пауза\" id=\"1\" pause=\"10000\"/>"
    "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
    "</script>");

        //распарсить поток как XML, создать список Actions
        QString errorMsg;
        int errorLine, errorColumn;
        const char *name;
        IWavStream* stream;

        QDomDocument doc;
        if (false == doc.setContent(in, &errorMsg, &errorLine, &errorColumn)) {
          throw E(-1, "xml");
        }
        QDomElement e = doc.documentElement();
        QDomNode n = e.firstChild();
        n = n.nextSibling();

        QVERIFY(!n.isNull());
        e = n.toElement();

        QString attr_name = e.attribute("name", "connect");
        QString attr_id = e.attribute("id", "1");
        QString attr_target = e.attribute("target", "301");
        qDebug() << attr_name << attr_id << attr_target;

//        QString qstr;
//        QTextStream q_stream(&qstr);
//        n.save(q_stream, 4);

        QString qstr = "<" + e.tagName();
        QDomNamedNodeMap attrs = e.attributes();
        for(int i = 0; i<attrs.size(); i++)
        {
            QDomAttr attr = attrs.item(i).toAttr();
            qstr +=
                QString::fromLatin1(" %0=\"%1\"")
                .arg(attr.name())
                .arg(attr.value());
        }
        qstr += "/>";

        qDebug() << qstr;
        string str = qstr.toStdString();

        stream = IWavStream::create(str, strerror);//name=\"connect\"
        qDebug() << (name = typeid(*stream).name());
        //QVERIFY(0 == strcmp(name, "N3DXE22CTScriptAction_connectE"));

        stream->release();

//        n = n.nextSibling();
//        QVERIFY(!n.isNull());
//        e = n.toElement();
//        action = CTScriptAction::create(e, NULL);//name=\"setPlayer\"
//        qDebug() << (name = typeid(*action).name());
//        //QVERIFY(0 == strcmp(name, "N3DXE20CTScriptAction_setPlayerE"));
//        delete action;
//
//        n = n.nextSibling();
//        QVERIFY(!n.isNull());
//        e = n.toElement();
//        action = CTScriptAction::create(e, NULL);//name=\"listen\"
//        qDebug() << (name = typeid(*action).name());
//        delete action;
//
//        n = n.nextSibling();
//        QVERIFY(!n.isNull());
//        e = n.toElement();
//        action = CTScriptAction::create(e, NULL);//name=\"pause\"
//        qDebug() << (name = typeid(*action).name());
//        delete action;
//
//        n = n.nextSibling();
//        QVERIFY(!n.isNull());
//        e = n.toElement();
//        action = CTScriptAction::create(e, NULL);//name=\"disconnect\"
//        qDebug() << (name = typeid(*action).name());
//        delete action;

  //
  //      QDomElement e;
  //      e.setTagName("action");
  //      e.setAttribute("name", "connect");
  //      e.setAttribute("id", "1");
  //      e.setAttribute("target", "301");
  //      CTScriptAction* action = CTScriptAction::Create(e);
  //      QVERIFY(action);
  //      if (false == action->execute()) {
  //        QFAIL("script execute");
  //      }

      } catch (P_EXC e) {
        QFAIL(e.what());
      }
    }

  //тесты потоков: sin
  void testCase_sin(){
    printf("1sin\n");
    int i = 1;
    while(i++<9)
    {
      //создать используя строку из xml
      QString opt = QString("<any_item name=\"GenOn\" type=\"sin\" freq=\"%0\" ampl=\"2\" />").arg(1500+i*100);
      IWavStream* stream = IWavStream::create(CString(opt.toStdString()), strerror);//shared_ptr
      opt = QString("<action name=\"listen\" id=\"1\" dir=\"1\" type=\"file_out\" filename=\"%0.wav\" />").arg(1500+i*100);
      IWavStream* stream_out = IWavStream::create(CString(opt.toStdString()), strerror);//shared_ptr
      QVERIFY(stream != NULL);
      QVERIFY(stream_out != NULL);
      int counter = 0;
      do{
        //прочитать след пакет с генератора
        CPacket_RTP* rtp_packet = stream->read();
        QVERIFY(rtp_packet != NULL);
        //каки то тесты!
        stream_out->write(rtp_packet);
        //пока не конец генератора и не было команды на прервать поток
      }while((++counter < 1000));
      stream->close();
      stream_out->close();
    }
  }

  //тесты потоков: file
  void testCase_file(){
    printf("1file-file\n");
      //создать используя строку из xml
      CString opt = "<action type=\"file_in\" filename=\"voiceLeaveMessage.wav\" />";
      //CString opt = "test_filein";
      IWavStream* stream_in = IWavStream::create(opt, strerror);//shared_ptr
      //opt = "test_fileout";
      opt = "<action type=\"file_out\" filename=\"1.wav\" />";
      IWavStream* stream_out = IWavStream::create(opt, strerror);//shared_ptr
      QVERIFY(stream_in != NULL);
      QVERIFY(stream_out != NULL);

      int counter = 0;
      do{
        CPacket_RTP* rtp_packet = stream_in->read();
          QVERIFY(rtp_packet != NULL);
        //прочитать след пакет с генератора
        //каки то тесты!
        stream_out->write(rtp_packet);
        //пока не конец генератора и не было команды на прервать поток
        delete rtp_packet;
      }while(! stream_in->isEndOfStream());
      stream_in->close();
      stream_out->close();


  }

  //тесты потоков: memory
  void testCase_mem(){
    printf("3mem\n");
    CString opt = "<any_item type=\"mem\" />";
    IWavStream* stream = IWavStream::create(opt, strerror);//shared_ptr

    IWavStream* s1 = stream;

    stream->release();

    s1->release();
  }

  //тесты потоков: sound_out
  void testCase_sound_out(){
    CString opt_out = "<any_item type=\"sound_out\" />";
    IWavStream* stream_out = IWavStream::create(opt_out, strerror);//shared_ptr

    //CString opt_in = "<any_item type=\"sin\" freq=\"440\" ampl=\"-6\" />";
    CString opt_in = "<action type=\"file_in\" filename=\"wavs\\voiceAfterPlay.wav\" />";
    IWavStream* stream_in = IWavStream::create(opt_in, strerror);//shared_ptr

    QVERIFY(stream_out != NULL);
    QVERIFY(stream_in != NULL);
    int counter = 0;
    do{
      //прочитать след пакет с генератора
      CPacket_RTP* rtp_packet = stream_in->read();
      QVERIFY(rtp_packet != NULL);
      //каки то тесты!
      stream_out->write(rtp_packet);
      Sleep(60);
      //пока не конец генератора и не было команды на прервать поток
    }while((++counter < 200));
    stream_out->release();
    stream_in->release();
    Sleep(5000);
    cout << ("4sound_out") << endl << flush;
  }

//  //тесты потоков:
//  void testCase_toALaw(){
//    CString opt_out = "<any_item type=\"file_out\" filename=\"wavs\\dtmf0_a.wav\" />";
//    IWavStream* stream_out = IWavStream::create(opt_out);
//
//    CString opt_in = "<action type=\"file_in\" filename=\"wavs\\dtmf0_11.wav\" />";
//    IWavStream* stream_in = IWavStream::create(opt_in);
//
//    QVERIFY(stream_out != NULL);
//    QVERIFY(stream_in != NULL);
//    do{
//      //прочитать след пакет с генератора
//      CPacket_RTP* rtp_packet = stream_in->read();
//      QVERIFY(rtp_packet != NULL);
//      QByteArray alaw = ConvertToALaw(
//          QByteArray((const char*)rtp_packet->getDataOfPayload(),rtp_packet->getLengthOfPayload()));
//
//      CPacket_RTP rtp_alaw((uint8_t*)alaw.data(), alaw.size());
//
//      stream_out->write(&rtp_alaw);
//      //пока не конец генератора и не было команды на прервать поток
//    }while(! stream_in->isEndOfStream());
//    stream_out->release();
//    stream_in->release();
//    Sleep(5000);
//    cout << ("4sound_out") << endl << flush;
//  }

  //тесты потоков: sound_in
  void testCase_sound_in(){
    printf("5sound_in\n");
  }
};


int main_test1(int argc, char *argv[]){
  CTS_stream tc;
  return QTest::qExec(&tc, argc, argv);
}

/*
  void testCase1(){
    QFETCH(QString, data);
    //QVERIFY2(true, "Failure");

    QVERIFY(vmservice != NULL);


    try{

  //    QFile file("scripts/script1.ts");
  //    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
  //        throw E(-1, "the script file don't open");
      QByteArray in(
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
          "<!-- **  -->"
          "<script name=\"script1\" descr=\"Описание\" >"
            "<action name=\"connect\" descr=\"Установить связь\" id=\"1\" target=\"301\" source=\"300\"/>"
            "<action name=\"listen\" descr=\"Транслировать звук\" id=\"1\" type=file_in filename=\"file.wav\" / -->"
//            "<action name=\"GenOn\" descr=\"Подключить генератор\" id=\"1\" type=\"sin\" />"
//            "<action name=\"measure\" descr=\"Измерить\" id=\"1\" />"
//            "<action name=\"GenOff\" descr=\"Выключить генератор\" id=\"1\" />"

            "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
          "</script>");

//      DXE::CTScript script(in);
//      if(false == script.execute()){
//        QFAIL("script1.ts");
//      }


      //1:распределение входящих на ожидаемые звонки:
      //если нет созданных и ожидающих - отбой, все линии заняты!
      //2:Все методы неблокирующие на завершение операции,
      //только задают команду и отдают управление.
      //Исключение методы waitforXXX() - ожидают завершение команды!
      //прерываем состояние waitfor через breakWait()
      //3.Cостояния:
        //ожидаю звонка - по умолчанию <->
        //в процессе соединения ->
        //соединён:подстатусы ->
        //  слушаю в слушателя
        //  проигрываю с проигрывателя
      //getId() - id себя
      //getTargetId() - id соединённого
      //TODO: тесты потока
      //TODO: тесты измерителя
      //TODO: алгоритмы методов setPlayer()/stopPlay(), listen()/stopListen(), waitforXXX()
      //TODO: ход тестов, первичный алгоритм

      CCallManager *call, *callIn, *callOut;
      vector<CConnection*> connections;

      //пример работы:
      //получить рабочий экземпляр
      callIn = vmservice->getCallManager();
      //ожидать вызова
      callIn->waitforCall(); //- по умолчанию
      //получить id звонившего
      number = callIn->getTargetId();
      sleep(10);//sec - подождать
      //закрыть соединение
      callIn->close();

      //1. создать соедниние(target)
      id = "1";
      callIn = vmservice->getCallManager();
      //callIn->waitforCall();//задать ожидание. Указать конкретного входящего?
      callOut = vmservice->getCallManager();

      if(EOK != callOut->call(target, 60) ){
        ;//timeout
      }
//      if(target != callIn->getId()){
//        ;//unknown call source
//      }
//      string target = callIn->getTarget();//с кем связался
      connections[id] = new CConnection(callIn, callOut);

      //2.закрыть соединение()
      //connections[id] =
      connections[id]->close();//должен переприсвоить NULL по ссылке

      //3.Подключить генератор(dir, freq, ampl) - проиграть в отдельном потоке!
      {
        ICallManager* call = connection[id][dir];

        CString opt = "<action name=\"GenOn\" type=\"sin\" freq=\"1000\" ampl=\"20\" />";
        IWavStream* stream = IWavStream::create(opt);//shared_ptr
        call->setPlayer(stream);
        if(! call->isPlaying()){
          //проблемы?
        }
      }
      //4.Измерить(dir, hi, low)
      {
        ICallManager* call = connection[id][dir];
        //поток для прослушки
        CString opt = "<item type=\"mem\" maxsize=25 />";//25kbytes
        IWavStream* stream_listen = IWavStream::create(opt);
        int res = call->listen(stream_listen);//слушаем в память
        if(! call->isListening()){
          ;//проблемы?
        }
        call->waitfor(1);//sec - копим, что услышали
        call->stopListen();//стоп прослушки-записи

        //эталонный поток
        CString opt = "<action name=\"GenOn\" type=\"sin\" freq=\"1000\" ampl=\"20\" />";
        IWavStream* stream_setPlayer = IWavStream::create(opt);//shared_ptr
        //создать тестер и провести тест!
        tester = new CWavTester(
            stream_setPlayer, stream_listen );
        bool res = tester->test(hi, low);
      }
      //5.отключить(dir)
      ICallManager* call = connection[id][dir];
      call->stopPlay();

      //6.передать файл(dir, filename)
      ICallManager* call = connection[id][dir];
      CString opt = "<action type=\"file\" filename=\"filename.wav\" />";
      IWavStream* stream = IWavStream::create(opt);

      call->setPlayer(stream);//играем файл
      call->waitforEndOfStream();//ждём конца потока

      //7.прослушать(dir)
      ICallManager* call = connection[id][dir];
      CString opt = "<stream type=\"sound_out\" />";
      IWavStream* stream = IWavStream::create(opt);
      call->listen(stream);//слушаем и пишем в поток
      //далее как опция!
      call->waitforClose();//ждем закрытия соединения
      call->stopListen();
      //call->breakWait();

      //8.записать(dir)
      ICallManager* call = connection[id][dir];
      CString opt = "<stream type=\"sound_in\" />";
      IWavStream* stream = IWavStream::create(opt);
      call->setPlayer(stream);//играем, что получаем от потока
      //далее как опция!
      call->waitfor(5);//5 cекунд
      call->stopPlay();

      //? определить конец прослушки
      //? как сделать их неблок
      virtual void CCallManager::setPlayer(CWavStream* _wav){
        ASSERT(_wav);
        //далее в потоке проигрывания!
        //ждем сигнал на старт проигрывания
        //...
        //получили сигнал, данные содержат CWaveGenerator
        //см. setPlayerVoiceMsg()

        //начали вещать
        status |= _status_setPlayer;
        //CPacket_RTP* rtp_packet;
        _wav->reset();
        do{
          //прочитать след пакет с генератора
          CPacket_RTP* rtp_packet = _wav.read();
          //и закинуть его в сеть
          if (-1 == (sendto(socket_rtp_out, (const char*) rtp_packet->GetData(),
              rtp_packet->GetLength(), 0, (sockaddr*) &(addr_rtp_out), sizeof(sockaddr_in))))
            throw E(WSAGetLastError(), "Call_in:sendto");

          _wav.next();
          //пока не конец генератора и не было команды на прервать поток
        }while((! _wav->endofStream()) && (0 != (status & _status_setPlayered)));
        //прекратили вещать
        status &= ~_status_setPlayer;

      }

      virtual void CCallManager::listen(CWaveStream* _wav){
        ASSERT(_wav);
        //далее в потоке проигрывания!
        //ждем сигнал на старт проигрывания
        //...
        //получили сигнал, данные содержат CWaveGenerator
        //см. setPlayerVoiceMsg()

        //начали слушать
        status |= _status_listen;
        //CPacket_RTP* rtp_packet;
        _wav->reset();
        do{

          recvfrom(socket_rtp_in, (char*) buf, BUFLEN, 0
              ,(sockaddr *) &rtp_other, &slen);
          //прочитать след пакет с генератора
          CPacket_RTP* rtp_packet = new CPacket_RTP(buf, len);
          CWaveListener->write(rtp_packet);

          //пока не конец генератора и не было команды на прервать поток
        }while((! _wav->endofStream()) && (0 != (status & _status_listen)));
        //прекратили вещать
        status &= ~_status_listen;

      }

    }catch(Exception & e){
      QWARN(e.what());
      QFAIL("exception");
    }
  }

  void testCase1_data(){
    QTest::addColumn<QString>("data");
    QTest::newRow("0") << QString();
  }
*/

#include "test1.moc"
