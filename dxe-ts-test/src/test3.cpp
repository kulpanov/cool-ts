#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <fstream>

#include <qobjectdefs.h>
#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>
#include <QtNetwork/QtNetwork>

#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "dxe-call-server.h"
//#include "CCallManager.h"
//#include "CCallService.h"

using namespace DXE;

#define _T(res) if(ETIMEDOUT == (res)){QWARN("timeout!");goto L_exit; }

static int filesize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::binary);
    in.seekg(0, std::ifstream::end);
    return in.tellg();
}

//тесты для call server
class CTS_vms: public QObject {
Q_OBJECT
string strerror;
public:
  CTS_vms() {
  }

  virtual ~CTS_vms() {
    qDebug() << ("press ENTER key\n");
    // getchar();
  }

  ICallService *call_service = NULL;
  const int t_count = 30;

private Q_SLOTS:
  void initTestCase() {
    try {
      call_service = ICallService::create("noopts");
      QVERIFY(call_service);
    } catch (P_EXC e) {
      QFAIL(e.what());
    }
  }

  void cleanupTestCase() {
    //if(call_service)call_service->release();
  }

  //исходящее, в паре с test_callin
  void test_callout() {
    try {
      //Старт диспетчера, создание управляющих соединением, для исходящего звонка.
      //Вещение из файла и запись входящего звукового сигнала в файл
      //после чего закрытие соединения
      {//создание файла с настройками диспетчера
         static const char* settings1_xml={
         "<?xml version=\"1.0\" ?>\n"
         "<Params>\n"
         "<General name=\"General\">\n"
           "<indx value=\"1\" />\n"
           "<myIP value=\"192.168.233.22\" name=\"IP\" />\n"
           "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
           "<SIPin value=\"5061\" name=\"port_in\" />\n"//дб callout
           "<SIPout value=\"5060\" name=\"port_out\" />\n"
           "<realIP value=\"192.168.233.2\" />\n"
           "<gwIP value=\"192.168.233.222\" />\n"
           "<logLevel value=\"6\" name=\"logLevel\" />\n"
         "</General>\n"
         "</Params>\n"};
         ofstream f;
         f.open("settings1.xml");
         f << settings1_xml;
         f.close();
       }
      //старт диспетчера
      call_service->start("settings1.xml");

      //создание управляющего для исходящего звонка,
      ICallManager* call_out = call_service->createCallManager();

      //задание параметров потоков для входящего и исходящего звукового потока
      //создание и установка RTP-потока для трансляции звука
      call_out->setPlayer(IWavStream::create(
          "<s type=\"sin\" freq=\"400\" ampl=\"0\" />", strerror));
//          "<s type=\"sin\" filename=\"out.wav\" />", strerror));

      //создание и установка RTP-потока для записи звука
      call_out->setListener(IWavStream::create(
          "<s type=\"file_out\" filename=\"callout.wav\" />\n", strerror));

      //Попытка установить соединение на номер 301, с номера 388
      //ожидать установки 10сек, IP шлюза 192.168.1.3
      call_out->call("301", "388", WSEC(10), IP("192.168.233.222"));
      //ожидаем 20сек, в течении этого времени весь
      //входящий трафик сохраняется в файле in.wav
      //исходящий трафик берется из out.wav
      call_out->waitforClose(WSEC(10));
      //завершить соединение
      call_out->release();
    } catch (P_EXC e) {
      QFAIL(e.what());
    }
//    Sleep(3000);
//    exit(0) ;
  }

  //ждет входящее, отвечает
  void test_callin() {
    try {
      {
 static const char* settings1_xml={
 "<?xml version=\"1.0\" ?>\n"
 "<Params>\n"
 "<General name=\"General\">\n"
   "<indx value=\"1\" />\n"
   "<myIP value=\"192.168.233.222\" name=\"IP\" />\n"
   "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
   "<SIPin value=\"5060\" name=\"port_in\" />\n"
   "<SIPout value=\"5061\" name=\"port_out\" />\n"
   "<realIP value=\"192.168.233.2\" name=\"IP\" />\n"
   "<gwIP value=\"192.168.233.22\" name=\"IP\" />\n"
   "<logLevel value=\"6\" name=\"logLevel\" />\n"
 "</General>\n"
 "</Params>\n"};
 ofstream f;
 f.open("settings2.xml");
 f << settings1_xml;
 f.close();
       }
      //старт диспетчера
       call_service->start("settings2.xml");
       //создание управляющим соединением
      ICallManager* call_in = call_service->createCallManager();

      //установка генератора синуса
      call_in->setPlayer(IWavStream::create(
         "<s type=\"sin\" freq=\"500\" ampl=\"0\"/>", strerror));
      //установка сохранения потока в файл
      call_in->setListener(IWavStream::create(
          "<s type=\"file_out\" filename=\"callin.wav\" />", strerror));

      //ожидание входящего соединения
      call_in->waitforCall(WSEC(30));
      //ожидание закрытие соединения с другой стороны
      call_in->waitforClose(WSEC(10));
      //завершение работы
      call_in->release();

    } catch (P_EXC e) {
      QFAIL(e.what());
    }
  }

  //исходящее, много соединений, в паре с test_callin_m
  void test_callout_m() {
    try {
      //Старт диспетчера, создание управляющих соединением, для исходящего звонка.
      //Вещение из файла и запись входящего звукового сигнала в файл
      //после чего закрытие соединения
      {//создание файла с настройками диспетчера
         static const char* settings1_xml={
         "<?xml version=\"1.0\" ?>\n"
         "<Params>\n"
         "<General name=\"General\">\n"
           "<indx value=\"1\" />\n"
           "<myIP value=\"192.168.233.22\" name=\"IP\" />\n"
           "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
           "<SIPin value=\"5061\" name=\"port_in\" />\n"//дб callout
           "<SIPout value=\"5060\" name=\"port_out\" />\n"
           "<realIP value=\"192.168.233.2\" />\n"
           "<gwIP value=\"192.168.233.222\" />\n"
           "<logLevel value=\"5\" name=\"logLevel\" />\n"
         "</General>\n"
         "</Params>\n"};
         ofstream f;
         f.open("settings1.xml");
         f << settings1_xml;
         f.close();
       }
      //старт диспетчера
      call_service->start("settings1.xml");
      boost::thread_group tg;
      for(int i=0; i<t_count; i++){
        tg.create_thread(boost::bind<void>(
            [this](int _i){
        try{
        string str_i = boost::lexical_cast<std::string, int>(_i);
        //создание управляющего для исходящего звонка,
        ICallManager* call_out = call_service->createCallManager();
        //задание параметров потоков для входящего и исходящего звукового потока
        //создание и установка RTP-потока для трансляции звука
        call_out->setPlayer(IWavStream::create(
            "<s type=\"sin\" freq=\"400\" ampl=\"-"+str_i+"\" />", strerror));

        string sfilename = "callout_"+str_i+ ".wav";
        //создание и установка RTP-потока для записи звука
        call_out->setListener(IWavStream::create(
            "<s type=\"file_out\" filename=\""+sfilename+"\" />\n", strerror));

        //Попытка установить соединение на номер 301, с номера 388
        //ожидать установки 10сек, IP шлюза 192.168.1.3
        call_out->call(str_i, "388", WSEC(10), IP("192.168.233.222"));
        //ожидаем 20сек, в течении этого времени весь
        //входящий трафик сохраняется в файле in.wav
        //исходящий трафик берется из out.wav
        call_out->waitforClose(WSEC(10));
        //завершить соединение
        call_out->release();

        int sz = filesize(string("tmp/" + sfilename).c_str());
        if(sz < 50000){
          cout << ("sz < 50000") << endl;
          exit(1);
        }
        }catch(P_EXC e){
          QFAIL(e.what());
        }
        }, i));
      }
      tg.join_all();
    } catch (P_EXC e) {
      QFAIL(e.what());
    }
  }

  //ждет входящее, держит, отвечает, много соединений
  void test_callin_m() {
    try {
      {
 static const char* settings1_xml={
 "<?xml version=\"1.0\" ?>\n"
 "<Params>\n"
 "<General name=\"General\">\n"
   "<indx value=\"1\" />\n"
   "<myIP value=\"192.168.233.222\" name=\"IP\" />\n"
   "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
   "<SIPin value=\"5060\" name=\"port_in\" />\n"
   "<SIPout value=\"5061\" name=\"port_out\" />\n"
   "<realIP value=\"192.168.233.2\" name=\"IP\" />\n"
   "<gwIP value=\"192.168.233.22\" name=\"IP\" />\n"
   "<logLevel value=\"5\" name=\"logLevel\" />\n"
 "</General>\n"
 "</Params>\n"};
 ofstream f;
 f.open("settings2.xml");
 f << settings1_xml;
 f.close();
       }
      //старт диспетчера
       call_service->start("settings2.xml");
       boost::thread_group tg;
       for(int i=0; i<t_count; i++){
         tg.create_thread(boost::bind<void>(
             [this](int _i){
         try{
           //создание управляющим соединением
          ICallManager* call_in = call_service->createCallManager();
          string str_i = boost::lexical_cast<std::string, int>(_i);
          //установка генератора синуса
          call_in->setPlayer(IWavStream::create(
              "<s type=\"sin\" freq=\"800\" ampl=\"-"+str_i+"\" />", strerror));
          //установка сохранения потока в файл

          string sfilename = "callin_"+str_i+ ".wav";
          //создание и установка RTP-потока для записи звука
          call_in->setListener(IWavStream::create(
              "<s type=\"file_out\" filename=\""+sfilename+"\" />\n", strerror));

          //ожидание входящего соединения
          call_in->waitforCall(WSEC(30));
          //ожидание закрытие соединения с другой стороны
          call_in->waitforClose(WSEC(10));
          //завершение работы
          call_in->release();

          int sz = filesize(string("tmp/" + sfilename).c_str());
          if(sz < 50000){
            cout << ("sz < 50000") << endl;
            exit(1);
          }
         }catch(P_EXC e){
           QFAIL(e.what());
         }
         }, i));
       }
       tg.join_all();
     } catch (P_EXC e) {
       QFAIL(e.what());
     }
  }

  //исходящее, ждет входящее, в паре с echo, синхронизировать realIP!
  void test_call_in_out(){
//    QProcess echo;
//    echo.start("dxe-ts-test echo 1");
//    Sleep(5000);
    try {
      {
        static const char* settings1_xml={
        "<?xml version=\"1.0\" ?>\n"
        "<Params>\n"
        "<General name=\"General\">\n"
            "<myIP value=\"192.168.233.222\" name=\"IP\" />\n"
            "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
            "<SIPin value=\"5060\" name=\"port_in\" />\n"
            "<SIPout value=\"5061\" name=\"port_out\" />\n"
            "<realIP value=\"127.0.0.1\" />\n"
            "<gwIP value=\"192.168.233.22\" />\n"
            "<logLevel value=\"6\" name=\"logLevel\" />\n"
        "</General>\n"
        "</Params>\n"};
        ofstream f;
        f.open("settings1.xml");
        f << settings1_xml;
        f.close();
      }
      call_service->start("settings1.xml");

      ICallManager* call_out = call_service->createCallManager(ICall::_reserved);
      ICallManager* call_in = call_service->createCallManager();

      qDebug() << ("let's start the party!");
      call_out->call("380", "301", INFINITE, string("192.168.233.22"));
      qDebug() << ("waitforCall, 10 sec!");
      _T(call_in->waitforCall(WSEC(10)));

      call_in->setListener(IWav::create(
          "<s type=\"file_out\" filename=\"11.wav\" />", strerror));

      call_out->setPlayer(IWav::create(
          //"<s type=\"file_in\" filename=\"1.wav\""));
          "<s type=\"text\" text=\"0123456789\" />", strerror));
      // = "<s type=\"sin\" freq=\"500\" ampl=\"15\"/>";

      qDebug() << "call_out to: ";
      qDebug() << call_in->getConnectNumber().c_str();
      qDebug() << call_in->getConnectDigits().c_str();
      qDebug() << call_in->getConnectIP().toString().c_str();
      qDebug() << ("waitfor Close 10 sec");

      call_out->waitforClose(WSEC(10));

      qDebug() << ("close all connections");
L_exit:
      call_out->release();
      call_in->release();

    } catch (P_EXC e) {
      QFAIL(e.what());
    }
//    echo.kill();
//    Sleep(5000);
  }

  //геннерация dtmf
  void test_echo_dtmf() {
     //трансляция dtmf - требует запущенного setListener сервера
     try {
       {
 static const char* settings1_xml={
   "<?xml version=\"1.0\" ?>\n"
   "<Params>\n"
   "<General name=\"General\">\n"
     "<indx value=\"1\" />\n"
     "<myIP value=\"192.168.233.4\" name=\"IP\" />\n"
     "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
     "<SIPin value=\"5060\" name=\"port_in\" />\n"
     "<SIPout value=\"5060\" name=\"port_out\" />\n"
     "<logLevel value=\"4\" name=\"logLevel\" />\n"
   "</General>\n"
   "</Params>\n"
     };

 ofstream f;
 f.open("call-server.settings.xml");
 f << settings1_xml;
 f.close();
       }

       call_service->start("call-server.settings.xml");

       ICallManager* call_out = call_service->createCallManager();

       call_out->call("380*-0123456789-9876543210", "301", INFINITE, string("192.168.233.1"));
       Sleep(15000);
       call_out->release();

       qDebug() << ("close");
     } catch (P_EXC e) {
       QFAIL(e.what());
     }
     //echo.kill();
     //Sleep(3000);
   }

  //тесты на реальном эхо-сервере,проверка возвращающихся вызовов
  void test3() {
    try {
      {
        static const char* settings1_xml={
            "<?xml version=\"1.0\" ?>\n"
            "<Params>\n"
            "<General name=\"General\">\n"
              "<indx value=\"1\" />\n"
              "<myIP value=\"192.168.1.4\" name=\"IP\" />\n"
              "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
              "<SIPin value=\"5060\" name=\"port_in\" />\n"
              "<SIPout value=\"5060\" name=\"port_out\" />\n"
              "<logLevel value=\"4\" name=\"logLevel\" />\n"
            "</General>\n"
            "<Alt>\n"
              "<SIPin value=\"5060\" name=\"port_in\" />\n"
              "<SIPout value=\"5060\" name=\"port_out\" />\n"
            "</Alt>\n"
            "</Params>\n"};
        ofstream f;
        f.open("settings1.xml");
        f << settings1_xml;
        f.close();
      }
      call_service->start("settings1.xml");

      ICallManager* call_out = call_service->createCallManager(ICall::_reserved);
      ICallManager* call_in = call_service->createCallManager();

      //call_in->setPlayer(IWav::create("<s type=\"file_out\" filename=\"11.wav\" />"));

      qDebug() << ("let's start the party!");
      call_out->call("560388001", "388", INFINITE, string("192.168.1.5"));
      qDebug() << ("waitforCall, 10 sec!");
      _T(call_in->waitforCall(WSEC(10)));

      qDebug() << "call_out to: ";
      qDebug() << call_in->getConnectNumber().c_str();
      qDebug() << call_in->getConnectDigits().c_str();
      qDebug() << call_in->getConnectIP().toString().c_str();
      qDebug() << ("waitfor Close 10 sec");
      call_in->setListener(IWav::create(
          "<s type=\"file_out\" filename=\"11.wav\" />", strerror));

      call_out->setPlayer(IWav::create(
          //"<s type=\"file_in\" filename=\"1.wav\""
          //"<s type=\"text\" text=\"0123456789\" />"
          "<s type=\"sin\" freq=\"500\" ampl=\"15\"/>", strerror
          ));

      call_out->waitforClose(WSEC(10));

      //тестировать sin-11.wav

      qDebug() << ("close all connections");
      call_out->close();
      call_in->close();

L_exit:
      call_out->release();
      call_in->release();

    } catch (P_EXC e) {
      QFAIL(e.what());
    }
    //    Sleep(3000);
    //    exit(0) ;
    //
  }

};

int main_test3(int argc, char *argv[]) {
  CTS_vms tc;
  return QTest::qExec(&tc, argc, argv);
}

#include "test3.moc"
