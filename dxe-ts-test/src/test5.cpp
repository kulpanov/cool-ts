#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
#include <fstream>
#include <memory>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>
#include <QtXML/QtXML>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "dxe-call-server.h"

#include "dxe-ts-utils.h"
#include "dxe-ts-engine.h"

#include "CTScript.h"//for CScriptAction

using namespace DXE;
//ВНИМАНИЕ: все эхо тесты требуют запущенного "dxe-ts-test echo" !!!
class CTS5;

class LogListener5: public QObject, public ILogListener{
Q_OBJECT;
//QFile log;
//std::ofstream flog;
public:
  virtual void recvMsg(const DXE::SMsg_log& _msg){
    emit logTo(_msg);
    std::cout << _msg.toString().toUtf8().begin() << endl << flush;
    //flog  << _msg.toString().toUtf8().begin() << flush;
  }

  virtual void recvMsg(const QString& _fname, const QString& _fname_script, const QVector<QPair<double, double> >& _vec){

  }
  //запустить диалог с формы
  virtual void runDlg(int , void*){

  }

signals:
  void logTo(const DXE::SMsg_log& _msg);

public:
  LogListener5(CTS5* _log_receiver);
};

//тесты для call server
class CTS5: public QObject {

Q_OBJECT;
public:
  CTS5(){
    counter = 0;

  }

  virtual ~CTS5() {
//    std::cout << ("press ENTER key\n") << flush;
//    getchar();
  }

protected:
  ICallService_ptr call_service;
  CListOfConnections_ptr connections;
  CLogger_ptr log;
  int counter;

  class CThread_script: public QThread{
    CTS5* master;
    virtual void run(){
      CTScript scriptExecuter(master->call_service
          , master->connections, master->log);

      //script
    QString in;
      if(1){
      //позвонить, отранслировать файл
      in = QString(
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
    "<!-- **  -->"
    "<script name=\"script1\" descr=\"Описание\" >"
    "<action name=\"connect\" id=\"%0\" target=\"30038800%0\" add=\"301\" target_ip=\"127.0.0.1\" />"
    "<action name=\"listen\" id=\"%0\" dir=\"1\" type=\"file_out\" filename=\"%0.wav\" />"
    "<action name=\"setPlayer\"   id=\"%0\" dir=\"0\" type=\"sin\" freq=\"200\" ampl=\"-12\" />"
    "<action name=\"pause\" descr=\"Пауза\" id=\"%0\" pause=\"3000\"/>"
    "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"%0\" />"
    "</script>").arg(++master->counter);
      }
      //qDebug() << in;

      scriptExecuter.loadScript(in.toStdString().c_str());

      qDebug() << "start";
      scriptExecuter.execute();

//      if (false == res) {
//        QFAIL("script execute");
//      }
      qDebug() << "stop";
      quit();
    }

  public:
    CThread_script(CTS5* _master)
    :master(_master){

    }
  };

  class CScriptRunner: public QRunnable{
    CTScript* scriptExecuter;

    virtual void run(){
      qDebug() << "start";
      scriptExecuter->execute();
//      if (false == res) {
//        QFAIL("script execute");
//      }
      qDebug() << "stop";
    }

  public:
    CScriptRunner(CTScript* _scriptExecuter)
      :scriptExecuter(_scriptExecuter){  }
  };

private Q_SLOTS:
  void logRecv(const DXE::SMsg_log& _msg){
    //QDebug qdbg = QDebug(QtDebugMsg);
    //flog  << _msg.toString().toUtf8().begin() << flush;
  }

  void initTestCase() {
  }

  void cleanupTestCase() {
  }

  void testX_par() {
    try {
      //settings
      if(1){
static const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
  "<indx value=\"1\" />\n"
  "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
  "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
  "<SIPin value=\"5060\" name=\"port_in\" />\n"
  "<SIPout value=\"5061\" name=\"port_out\" />\n"
  "<logLevel value=\"4\" name=\"logLevel\" />\n"
"</General>\n"
"</Params>\n"};
ofstream f;
f.open("call-server.settings.xml");
f << settings1_xml;
f.close();
      }

      call_service =
          ICallService_ptr(ICallService::create("noopts"),
                                          &ICallService::release);

      call_service->start(CTScript::settings_fname);

      // что были открыта этим сценарием - закрыты ?
      connections = CListOfConnections_ptr(new CListOfConnections());
      log = CLogger_ptr(new CLogger(new LogListener5(this), "1"));

      counter = 0;

      CThread_script* script[10]
           = {new CThread_script(this), new CThread_script(this)
             ,new CThread_script(this), new CThread_script(this)
             ,new CThread_script(this), new CThread_script(this)
             ,new CThread_script(this), new CThread_script(this)
             ,new CThread_script(this), new CThread_script(this)};

      for(int i=0; i<10; i++){
        script[i]->start();
      }
      for(int i=0; i<10; i++){
        script[i]->wait();
      }


      //call_service->release();

    } catch (P_EXC e) {
      QFAIL(e.what());
      LOG(INFO, "exception");
    }
  }

  void testX_par1() {
     try {
       //settings
       if(1){
 static const char* settings1_xml={
 "<?xml version=\"1.0\" ?>\n"
 "<Params>\n"
 "<General name=\"General\">\n"
   "<indx value=\"1\" />\n"
   "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
   "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
   "<SIPin value=\"5060\" name=\"port_in\" />\n"
   "<SIPout value=\"5061\" name=\"port_out\" />\n"
   "<logLevel value=\"4\" name=\"logLevel\" />\n"
 "</General>\n"
 "</Params>\n"};
 ofstream f;
 f.open("call-server.settings.xml");
 f << settings1_xml;
 f.close();
       }

       call_service =
           ICallService_ptr(ICallService::create("noopts"),
                                           &ICallService::release);

       call_service->start(CTScript::settings_fname);

       // что были открыта этим сценарием - закрыты ?
       connections = CListOfConnections_ptr(new CListOfConnections());
       log = CLogger_ptr(new CLogger(new LogListener5(this), "1"));

       counter = 0;

       QThreadPool pool;
       pool.setMaxThreadCount(100);
       for(int i=0; i<40; i++){
         CTScript* scriptExecuter = new CTScript(call_service, connections, log);
         //script
         QString in;
         if(1){
           //позвонить, отранслировать файл
           in = QString(
               "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
               "<!-- **  -->"
               "<script name=\"script1\" descr=\"Описание\" >"
               "<action name=\"connect\" id=\"%0\" target=\"30038800%0\" add=\"301\" target_ip=\"127.0.0.1\" />"
               "<action name=\"listen\" id=\"%0\" dir=\"1\" type=\"file_out\" filename=\"%0.wav\" />"
               "<action name=\"setPlayer\"   id=\"%0\" dir=\"0\" type=\"sin\" freq=\"200\" ampl=\"-12\" />"
               "<action name=\"pause\" descr=\"Пауза\" id=\"%0\" pause=\"3000\"/>"
               "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"%0\" />"
               "</script>").arg(++counter);
         }
         scriptExecuter->loadScript(in.toStdString().c_str());

         pool.start(new CScriptRunner(scriptExecuter));
       }
       pool.waitForDone();
//       std::cout << ("done!\n") << flush;
//       getchar();

     } catch (P_EXC e) {
       QFAIL(e.what());
       LOG(INFO, "exception");
     }
   }

  void testX_par2() {
    //создание скрипта, исполнение его
    try {
      {
        static const char* settings1_xml={
            "<?xml version=\"1.0\" ?>\n"
            "<Params>\n"
            "<General name=\"General\">\n"
            "<indx value=\"1\" />\n"
            "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
            "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
            "<SIPin value=\"5060\" name=\"port_in\" />\n"
            "<SIPout value=\"5061\" name=\"port_out\" />\n"
            "<logLevel value=\"4\" name=\"logLevel\" />\n"
            "</General>\n"
            "</Params>\n"};
        ofstream f;
        f.open("call-server.settings.xml");
        f << settings1_xml;
        f.close();
         if(1){
          //позвонить, отранслировать файл
          static const char* dxe_test_xml={
              "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
              "<!-- **  -->"
              "<script name=\"script1\" descr=\"Описание\" >"
              "<action name=\"connect\" target_ip=\"127.0.0.1\" target=\"001001001\" out=\"001\" descr=\"Установить соединение\" id=\"1\" src=\"388\" gw=\"001\"/>"
              "<action descr=\"Пауза\" id=\"1\" name=\"pause\" pause=\"10000\"/>"
              "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
              "</script>"
          };
          ofstream f;
          f.open("dxe-tests/1.tst");
          f << dxe_test_xml;
          f.close();
        }
//        if(2){
//          //позвонить, отранслировать файл
//          static const char* dxe_test_xml={
//              "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
//              "<!-- **  -->"
//              "<script name=\"script2\" descr=\"Описание\" >"
//              "<action name=\"connect\" target_ip=\"192.168.1.5\" target=\"821*-828002\" out=\"821\" descr=\"Установить соединение\" id=\"2\" src=\"388\" gw=\"*-828\"/>"
//              "<action descr=\"Пауза\" id=\"2\" name=\"pause\" pause=\"10000\"/>"
//              "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"2\" />"
//              "</script>"
//          };
//          ofstream f;
//          f.open("dxe-tests/2.tst");
//          f << dxe_test_xml;
//          f.close();
//        }
//        if(3){
//          //позвонить, отранслировать файл
//          static const char* dxe_test_xml={
//              "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
//              "<!-- **  -->"
//              "<script name=\"script2\" descr=\"Описание\" >"
//              "<action name=\"connect\" target_ip=\"192.168.1.5\" target=\"822*-828003\" out=\"822\" descr=\"Установить соединение\" id=\"3\" src=\"388\" gw=\"*-828\"/>"
//              "<action descr=\"Пауза\" id=\"3\" name=\"pause\" pause=\"10000\"/>"
//              "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"3\" />"
//              "</script>"
//          };
//          ofstream f;
//          f.open("dxe-tests/3.tst");
//          f << dxe_test_xml;
//          f.close();
//        }
//        if(4){
//          //позвонить, отранслировать файл
//          static const char* dxe_test_xml={
//              "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
//              "<!-- **  -->"
//              "<script name=\"script2\" descr=\"Описание\" >"
//              "<action name=\"connect\" target_ip=\"192.168.1.5\" target=\"823*-828004\" out=\"823\" descr=\"Установить соединение\" id=\"4\" src=\"388\" gw=\"*-828\"/>"
//              "<action descr=\"Пауза\" id=\"4\" name=\"pause\" pause=\"10000\"/>"
//              "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"4\" />"
//              "</script>"
//          };
//          ofstream f;
//          f.open("dxe-tests/4.tst");
//          f << dxe_test_xml;
//          f.close();
//        }
        DXE::CTestScript::init(new LogListener5(this));

        DXE::CTestScript::executeFile("dxe-tests/1.tst", "1", CTestScript::nowaitForExecute);
    //    Sleep(200);
    //    DXE::CTestScript::executeFile("dxe-tests/2.tst", "2", CTestScript::nowaitForExecute);
    //    Sleep(300);
    //    DXE::CTestScript::executeFile("dxe-tests/3.tst", "3", CTestScript::nowaitForExecute);
    //    Sleep(100);
    //    DXE::CTestScript::executeFile("dxe-tests/4.tst", "4", CTestScript::nowaitForExecute);

    //    if (false == res) {
    //      QFAIL("script execute");
    //      //...
    //    }

        DXE::CTestScript::waitForDoneOfExecute();
        Sleep(1000);
        DXE::CTestScript::release();
      }

    } catch (P_EXC e) {
      QFAIL(e.what());
      LOG(INFO, "exception");
//      if(3>=3)if(log_level>=3)
//        fprintf(log_file, "%s:%s", programName, "exception");
    }
  }

  void testX_par_real() {
    //создание скрипта, исполнение его
    if(1){
      static const char* settings1_xml={
          "<?xml version=\"1.0\" ?>\n"
          "<Params>\n"
          "<General name=\"General\">\n"
          "<indx value=\"1\" />\n"
          "<myIP value=\"192.168.1.4\" name=\"IP\" />\n"
          "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
          "<SIPin value=\"5060\" name=\"port_in\" />\n"
          "<SIPout value=\"5060\" name=\"port_out\" />\n"
          "<logLevel value=\"5\" name=\"logLevel\" />\n"
          "</General>\n"
          "</Params>\n"};
      ofstream f;
      f.open("call-server.settings.xml");
      f << settings1_xml;
      f.close();
    }
    if(1){
      //позвонить, отранслировать файл
      static const char* dxe_test_xml={
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
          "<!-- **  -->"
          "<script name=\"script1\" descr=\"Описание\" >"
          "<action name=\"connect\" target_ip=\"192.168.1.5\" target=\"820*-828001\" out=\"820\" descr=\"Установить соединение\" id=\"1\" src=\"388\" gw=\"*-828\"/>"
          "<action descr=\"Пауза\" id=\"1\" name=\"pause\" pause=\"10000\"/>"
          "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"1\" />"
          "</script>"
      };
      ofstream f;
      f.open("dxe-tests/1.tst");
      f << dxe_test_xml;
      f.close();
    }
    if(2){
      //позвонить, отранслировать файл
      static const char* dxe_test_xml={
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
          "<!-- **  -->"
          "<script name=\"script2\" descr=\"Описание\" >"
          "<action name=\"connect\" target_ip=\"192.168.1.5\" target=\"821*-828002\" out=\"821\" descr=\"Установить соединение\" id=\"2\" src=\"388\" gw=\"*-828\"/>"
          "<action descr=\"Пауза\" id=\"2\" name=\"pause\" pause=\"10000\"/>"
          "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"2\" />"
          "</script>"
      };
      ofstream f;
      f.open("dxe-tests/2.tst");
      f << dxe_test_xml;
      f.close();
    }
    if(3){
      //позвонить, отранслировать файл
      static const char* dxe_test_xml={
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
          "<!-- **  -->"
          "<script name=\"script2\" descr=\"Описание\" >"
          "<action name=\"connect\" target_ip=\"192.168.1.5\" target=\"822*-828003\" out=\"822\" descr=\"Установить соединение\" id=\"3\" src=\"388\" gw=\"*-828\"/>"
          "<action descr=\"Пауза\" id=\"3\" name=\"pause\" pause=\"10000\"/>"
          "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"3\" />"
          "</script>"
      };
      ofstream f;
      f.open("dxe-tests/3.tst");
      f << dxe_test_xml;
      f.close();
    }
    if(4){
      //позвонить, отранслировать файл
      static const char* dxe_test_xml={
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
          "<!-- **  -->"
          "<script name=\"script2\" descr=\"Описание\" >"
          "<action name=\"connect\" target_ip=\"192.168.1.5\" target=\"823*-828004\" out=\"823\" descr=\"Установить соединение\" id=\"4\" src=\"388\" gw=\"*-828\"/>"
          "<action descr=\"Пауза\" id=\"4\" name=\"pause\" pause=\"10000\"/>"
          "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"4\" />"
          "</script>"
      };
      ofstream f;
      f.open("dxe-tests/4.tst");
      f << dxe_test_xml;
      f.close();
    }
    DXE::CTestScript::init(new LogListener5(this));

    DXE::CTestScript::executeFile("dxe-tests/1.tst", "1", CTestScript::nowaitForExecute);
//    Sleep(200);
//    DXE::CTestScript::executeFile("dxe-tests/2.tst", "2", CTestScript::nowaitForExecute);
//    Sleep(300);
//    DXE::CTestScript::executeFile("dxe-tests/3.tst", "3", CTestScript::nowaitForExecute);
//    Sleep(100);
//    DXE::CTestScript::executeFile("dxe-tests/4.tst", "4", CTestScript::nowaitForExecute);

//    if (false == res) {
//      QFAIL("script execute");
//      //...
//    }

    DXE::CTestScript::waitForDoneOfExecute();
    Sleep(1000);
    DXE::CTestScript::release();
  }

};


LogListener5::LogListener5(CTS5* _log_receiver)
  /*: flog("ts.test5.log")*/{
//  connect(this, SIGNAL(logTo(const DXE::SMsg_log& _msg))
//      , _log_receiver , SLOT(logRecv(const DXE::SMsg_log& _msg))
//      , Qt::ConnectionType::AutoConnection);
}


int main_test5(int argc, char *argv[]) {
  CTS5 tc;
  return QTest::qExec(&tc, argc, argv);
}

#include "test5.moc"
