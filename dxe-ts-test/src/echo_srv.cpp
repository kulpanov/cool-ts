/*
 * echo_sip.cpp
 *
 *  Created on: 29.02.2012
 *      Author: Kulpanov
 */
#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <fstream>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtCore/QCoreApplication>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "utils/utils.h"

#include "dxe-call-server.h"


const char* settings1_xml={
"<?xml version=\"1.0\" ?>\n"
"<Params>\n"
"<General name=\"General\">\n"
  "<indx value=\"1\" />\n"
  "<myIP value=\"127.0.0.1\" name=\"IP\" />\n"
  "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
  "<SIPin value=\"5061\" name=\"port_in\" />\n"
  "<SIPout value=\"5060\" name=\"port_out\" />\n"
  "<logLevel value=\"4\" name=\"logLevel\" />\n"
"</General>\n"
"<Alt>\n"
    "<SIPin value=\"5060\" name=\"port_in\" />\n"
    "<SIPout value=\"5060\" name=\"port_out\" />\n"
"</Alt>\n"
"</Params>\n"
    };

#include "debug.h"

using namespace DXE;

int main_echo(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);

  QFile::remove("TS-echo.log");

  init_debug(INFO, "TS-echo", " ", "");
    if("settings.xml.h"){
      ofstream f;
      f.open("ts-echo.settings.xml");
      f<<settings1_xml;
      f.close();
    }

    std::string listen_name
      = "<s type=\"echo\" filename=\"echo.raw\" />\n";
    std::string setPlayer_name
      = "<s type=\"echo\" text=\"this is echo!!!\" />\n";

    ICallService* call_service = NULL;
    try{
      call_service = ICallService::create("no_opts");
      call_service->start("ts-echo.settings.xml");

      do{
        ICallManager* call_in = call_service->createCallManager();
        ICallManager* call_out = call_service->createCallManager(
            ICallManager::_reserved);
        std::string strerror;
        call_in->setListener(IWavStream::create(
            "<s type=\"echo\" id=\"0\" />\n", strerror
        ));
        call_out->setPlayer(IWavStream::create(
            "<s type=\"echo\" id=\"0\" />\n", strerror
        ));

        call_out->setListener(IWavStream::create(
            "<s type=\"echo\" id=\"1\" />\n", strerror
        ));
        call_in->setPlayer(IWavStream::create(
            "<s type=\"echo\" id=\"1\" />\n", strerror
        ));
        cout << "i'm ready,wait for call";
        cout << endl << flush;
        call_in->waitforCall();

        cout << "callin is ok, doing echo callout to: ";
        cout << call_in->getConnectNumber();
        cout << call_in->getConnectDigits();
        cout << call_in->getConnectIP().toString();
        cout << endl << flush;

        call_out->call(
            /*call_in->getConnectNumber()*/"001"
          , call_in->getConnectDigits(), INFINITE
          , call_in->getConnectIP());

        if(EOK == call_out->waitforCall(WSEC(30))){
          cout << "callout is ok, wait for close";
          cout << endl << flush;
          call_in->waitforClose(WSEC(60));
        }else{
          cout << "time out for out call";
          cout << endl << flush;
        }
        call_in->release();//удаляяет также запись в call_service
        call_out->release();
        cout << "echo session completed, next turn";
        cout << endl << flush;
      }while(1);
      }catch(P_EXC e){
        CLOG(ERRR, e.what(), NULL);
    }
    if(call_service)
      call_service->release();
    return 0;
}



//#define SIP_IN  5061
//#define SIP_OUT 5060
//#define BUFLEN 0x300
//
//class CSIP_Echo : public QThread
//{
//  Q_OBJECT;
//protected:
//  QUdpSocket* sip_socket;
//  QUdpSocket* sip_out;
//public:
//  CSIP_Echo(){
//  }
//
//public:
//  void run() {
//    sip_out = new QUdpSocket(this);
//
//    sip_socket = new QUdpSocket(this);
//    sip_socket->bind(QHostAddress::Any, SIP_IN);
//    //sip_socket->setReadBufferSize(7);
//    connect(sip_socket, SIGNAL(readyRead()),
//            this, SLOT(readPendingDatagrams()), Qt::DirectConnection);
//    exec();
//
////    int socket_sip_out, socket_sip_in;
////    sockaddr_in socket_sip_out_addr, socket_sip_in_addr;
////    try {
////      //создать сокет исходящего трафика
////      if (-1 == (socket_sip_out = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
////        throw E(WSAGetLastError(), "callBegin:socket");
////      //  memset((char *) &socket_sip_out_addr, 0, sizeof(sockaddr_in));
////      //  socket_sip_out_addr.sin_family = AF_INET;
////      //  socket_sip_out_addr.sin_port = htons(SIP_OUT);
////      //  socket_sip_out_addr.sin_addr.S_un.S_addr = htonl(INADDR_LOOPBACK);
////
////      //создать сокет входящего трафика
////      if (-1 == (socket_sip_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
////        throw E(WSAGetLastError(), ":callBegin::socket");
////
////      memset((char *) &socket_sip_in_addr, 0, sizeof(socket_sip_in_addr));
////      socket_sip_in_addr.sin_family = AF_INET;
////      socket_sip_in_addr.sin_port = htons(SIP_IN);
////      socket_sip_in_addr.sin_addr.s_addr = htonl(INADDR_ANY);
////
////      if (bind(socket_sip_in, (sockaddr *) &socket_sip_in_addr,
////          sizeof(socket_sip_in_addr)) == -1)
////        throw E(WSAGetLastError(), ":callBegin::bind");
////
////      int slen = sizeof(socket_sip_out_addr);
////      char buf[0x1000];
////#define BUFLEN 0x1000
////
////      int len = 0;
////      while (1) {
////        //принять пакет
////        if (-1
////            == (len = recvfrom(socket_sip_in, (char*) buf, BUFLEN, 0,
////                (sockaddr *) &socket_sip_out_addr, &slen)))
////          throw E(WSAGetLastError(), "SIP_listener:recvfrom");
////        printf("Received packet from %s:%d\nData:\n%s\n"
////           , inet_ntoa(socket_sip_out_addr.sin_addr),
////            ntohs(socket_sip_out_addr.sin_port), buf);
//////        if (-1
//////            == (sendto(socket_sip_out, (const char*) buf, len, 0,
//////                (sockaddr*) &(socket_sip_out_addr), sizeof(sockaddr_in))))
//////          throw E(WSAGetLastError(), "UDPDR_long:sendto");
////      }
////
////    } catch (P_EXC e) {
////      E_CATCH(e);
////    }
////    closesocket(socket_sip_in);
////    closesocket(socket_sip_out);
//
//    return ;
//  }
//
//private:
//
//  char buf[BUFLEN];
//  QHostAddress sender;
//  quint16 senderPort;
//
//private Q_SLOTS:
//  void readPendingDatagrams(){
//    memset(buf, 0, BUFLEN);
//    while (sip_socket->hasPendingDatagrams()) {
//      sip_socket->readDatagram(buf, BUFLEN,
//          &sender, &senderPort);
//
//      QString s(buf);
//      int pos = s.indexOf("Call-ID: 01270011@");
//
//      //qDebug() << buf;
//      //echo!
//      sip_out->writeDatagram(buf, strlen(buf),
//          sender, SIP_OUT);
//    }
//  }
//};
//
//CSIP_Echo* sip_echo;
//
//void start_echo(){
//  sip_echo = new CSIP_Echo();
//  sip_echo->start();
//}

//#include "../debug/echo_sip.moc"
