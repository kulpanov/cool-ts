#include <stdio.h>
#include <typeinfo>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
#include <fstream>
#include <memory>

#include <QtCore/QtCore>
#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>
#include <QtXML/QtXML>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>

#include "define.h"
#include "debug.h"

#include "utils/netutils.h"
#include "dxe-call-server.h"

#include "dxe-ts-utils.h"
#include "dxe-ts-engine.h"

#include "CTScript.h"//for CScriptAction

using namespace DXE;
//ВНИМАНИЕ: все эхо тесты требуют запущенного "dxe-ts-test echo" !!!
class CTS6;

bool forgetExecuteNewTest = false;
int counter = 0;

class LogListener6: public QObject, public ILogListener{
Q_OBJECT;
private:
public:
  virtual void recvMsg(const DXE::SMsg_log& _msg){
    //emit logTo(_msg);
    std::cout << _msg.toString().toUtf8().begin() << endl << flush;
    if(_msg.fulldate){
      if(_msg.result == 2){
        counter ++;
        if(counter>5)
          forgetExecuteNewTest = true;

      }
      if(_msg.result == 0){
        counter --;
      }
      if(_msg.result == 1){
        //script fault
        forgetExecuteNewTest = true;
        //QFAIL("script fault");
        //QTest::qFail("script fault", __FILE__, __LINE__);
        //exit(1);
      }
    }
    //flog  << _msg.toString().toUtf8().begin() << flush;
  }
//
//signals:
//  void logTo(const DXE::SMsg_log& _msg);
  virtual void recvMsg(const QString& _fname, const QString& _fname_script, const QVector<QPair<double, double> >& _vec){

  }
  //запустить диалог с формы
  virtual void runDlg(int , void*){

  }
public:
  LogListener6(CTS6* _log_receiver);
};

//тесты для call server
class CTS6: public QObject {

Q_OBJECT;
public:
  CTS6(){
    //counter = 0;
  }

  virtual ~CTS6() {
    //char d;
    std::cerr << ("press ENTER key\n") << flush << std::cin;
    //std::cin >> d;
    //std::cin >> std::cout;
  }

private Q_SLOTS:
  void logRecv(const DXE::SMsg_log& _msg){
    //QDebug qdbg = QDebug(QtDebugMsg);
    //flog  << _msg.toString().toUtf8().begin() << flush;
  }

  void initTestCase() {
  }

  void cleanupTestCase() {
  }

  void createTestScript(int _indx){
    if(1){
      //позвонить, отранслировать файл
      const char* dxe_test_xml={
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
          "<script name=\"script%0\" descr=\"Сценарий%0\">\n"
          "<action name=\"connect\" target_ip=\"192.168.233.1\" target=\"00%0000%0000%0\" out=\"00%0\" descr=\"Установить соединение\" id=\"%0\" src=\"00%0\" gw=\"00%0\"/>\n"
          "<action dir=\"0\" freq=\"440\" ampl=\"-4\" descr=\"Подключить генератор\" type=\"sin\" id=\"%0\" name=\"play\"/>\n"
          "<action dir=\"1\" freq=\"880\" ampl=\"-2\" descr=\"Подключить генератор\" type=\"sin\" id=\"%0\" name=\"play\"/>\n"
          "<action descr=\"Пауза\" id=\"\" name=\"pause\" pause=\"2500\"/>\n"
          "<action hi=\"1\" check=\"0\" mtime=\"100\" dir=\"0\" descr=\"Измерить коэф передачи\" id=\"%0\" name=\"test_koef\" low=\"-1\"/>\n"
          "<action hi=\"1\" check=\"0\" mtime=\"100\" dir=\"1\" descr=\"Измерить коэф передачи\" id=\"%0\" name=\"test_koef\" low=\"-1\"/>\n"
          "<action dir=\"0\" descr=\"Отключить генератор\" id=\"%0\" name=\"stopPlay\"/>\n"
          "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"%0\" />\n"
          "</script>"};
       const char* dxe_test_fname = {"dxe-tests/test6/%0.tst"};

      ofstream f;
      f.open(QString(dxe_test_fname).arg(_indx).toAscii().begin());

      f << QString(dxe_test_xml).arg(_indx).toAscii().begin();
      f.close();
    }
  }

  void executeFile(int ind){
    createTestScript(ind);
    DXE::CTestScript::executeFile(QString("dxe-tests/test6/%0.tst").arg(ind).toAscii().begin()
        , QString("%0").arg(ind).toAscii().begin(), CTestScript::nowaitForExecute);
  }

  void testX_par_real() {
    //создание скрипта, исполнение его
    if(1){
      static const char* settings1_xml={
          "<?xml version=\"1.0\" ?>\n"
          "<Params>\n"
          "<General name=\"General\">\n"
          "<myIP value=\"192.168.233.4\" name=\"IP\" />\n"
          "<myNet value=\"255.255.255.0\" name=\"NET\" />\n"
          "<SIPin value=\"5060\" name=\"port_in\" />\n"
          "<SIPout value=\"5060\" name=\"port_out\" />\n"
          "<logLevel value=\"4\" name=\"logLevel\" />\n"
          "</General>\n"
          "</Params>\n"};
      ofstream f;
      f.open("call-server.settings.xml");
      f << settings1_xml;
      f.close();
    }
    boost::random::mt19937 gen;
    boost::random::uniform_int_distribution<> dist(0, 30);
    DXE::CTestScript::init(new LogListener6(this));
    for(int i =0; i<100; ++i)
    {
      LOGTIME(INFO);
      for(int j = 1; j<10; ++j){
        executeFile(j);
        int pause = dist(gen)*100;
        cout << "pause="<<pause<<endl;
        Sleep(pause);
        if(forgetExecuteNewTest){
          break;
        }
      }
      Sleep(10000);if(forgetExecuteNewTest || counter >0)break;
    }
    DXE::CTestScript::waitForDoneOfExecute();
    std::cerr<< "DONE" << endl;
    Sleep(1000);
    std::cerr<< "DONE" << endl;

    DXE::CTestScript::release();
    if(false == forgetExecuteNewTest){
      std::cerr<< "PASS" << endl;
    }else
      QFAIL("script fault");
  }

};

LogListener6::LogListener6(CTS6* _log_receiver){
  counter = 0;
}

int main_test6(int argc, char *argv[]) {
  CTS6 tc;
  return QTest::qExec(&tc, argc, argv);
}

#include "test6.moc"
