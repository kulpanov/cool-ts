#-------------------------------------------------
#
# Project created by QtCreator 2012-01-08T22:35:57
#
#-------------------------------------------------

QT       += testlib 
QT       += network
QT       += xml
QT       += gui

TARGET = dxe-ts-test
CONFIG   += console
#CONFIG   += app_bundle

TEMPLATE = app

INCLUDEPATH += ../dxe-utils ../dxe-ts-engine/include ../dxe-ts-engine/src\
 ../dxe-call-server/include ../dxe-streams/include 
 
QMAKE_LIBDIR += ../dxe/install d:/mingw32/i686-w64-mingw32/lib/boost

QMAKE_CXXFLAGS += -std=c++11 

DESTDIR = ../dxe/install

LIBS +=-lws2_32  -lpthread -lboost_system -lboost_thread -lboost_filesystem -ldxe-utils -ldxe-call-server -ldxe-streams  -ldxe-ts-engine -dynamic 

SOURCES += src/test1.cpp src/test2.cpp \
src/test3.cpp src/echo_srv.cpp src/test5.cpp src/echo_srv_mthread.cpp\
src/test4.cpp src/main.cpp src/listen_srv.cpp src/test6.cpp
# ../dxe-ts-engine/src/dxe-ts-utils.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"  
