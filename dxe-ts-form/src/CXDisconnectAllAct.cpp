#include "CXDisconnectAllAct.h"

CXDisconnectAllAct::CXDisconnectAllAct(QWidget *parent) : CXBaseAct(parent, true)
{
	mType = E_DisconnectAllAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	//убираем
	//mIDEdit->setVisible(false);
	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);
  centralLayout->addStretch();
//	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);
	//centralLayout->addStretch();

//	QLabel* caption = new QLabel(tr("Разъединить всех"), mContainer);
//	centralLayout->addWidget(caption);
}

CXDisconnectAllAct::~CXDisconnectAllAct()
{

}

void CXDisconnectAllAct::load(const QDomElement& aElement)
{
	Q_UNUSED(aElement)
}

QDomElement CXDisconnectAllAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("disconnect_all"));
	data << SXXMLData("descr", tr("Разъединить всех"));

	return CXBaseAct::getXMLElement(aDomDocument, data);
}
