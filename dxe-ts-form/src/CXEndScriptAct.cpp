#include "CXEndScriptAct.h"

#include <QVBoxLayout>
#include <QLabel>

CXEndScriptAct::CXEndScriptAct(QWidget *parent) : AXAbstractAct(parent)
{
	QVBoxLayout* centralLayout = new QVBoxLayout(this);

	QLabel* caption = new QLabel(tr("Конец сценария"), this);
	centralLayout->addWidget(caption);
}

CXEndScriptAct::~CXEndScriptAct()
{

}

void CXEndScriptAct::load(const QDomElement& aElement)
{
	Q_UNUSED(aElement)
}

QDomElement CXEndScriptAct::getXMLElement(QDomDocument& aDomDocument)
{
	Q_UNUSED(aDomDocument)

	return QDomElement();
}
