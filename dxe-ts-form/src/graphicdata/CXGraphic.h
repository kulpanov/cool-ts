#ifndef CXGRAPHIC_H
#define CXGRAPHIC_H

//#include "graphiclibrary_global.h"

#include <QFont>

#include "CXGraphicData.h"

/*!
	Класс генерации графика.
*/
class  CXGraphic
{
public:
	//! Конструктор.
	CXGraphic();

	//! Деструктор.
	~CXGraphic();

	/*!
		Функция установки кисти для вспомогательных линий сетки.
		\param aPen - кисть.
	*/
	void setGridPen(const QPen& aPen);

	/*!
		Функция установки кисти для основных линий сетки.
		\param aPen - кисть.
	*/
	void setPen(const QPen& aPen);

	/*!
		Функция установки цвета текста подписей.
		\param aTextColor - цвет текста.
	*/
	void setTextColor(const QColor& aTextColor);

	/*!
		Функция установки цвета фона.
		\param aTextColor - цвет фона.
	*/
	void setBackgroundColor(const QColor& aBackgroundColor);

	/*!
		Функция задания пределов по осям.
        \param aXMin, aXMax - пределы по оси абсцисс.
        \param aYMin, aYMax - пределы по оси ординат.
		\note Для задания пределов только по одной оси, необходимо диапазон для другой оси задать равным значением.
    */
    void setAxisRange(qreal aXMin, qreal aXMax, qreal aYMin, qreal aYMax);

	/*!
		Функция установки размера выходного изорбражения.
		\param aSize - размер выходного изорбражения.
	*/
	void setSize(const QSize& aSize);

	qreal height() { return mSize.height(); }
	qreal width() { return mSize.width(); }

	QPixmap getGraphic();

	/*!
		Функция добавления новых данных на график.
		\param aData - данные.
	*/
	void setData(const QVector<QPair<double, double> >& aData);

	//! Функция установки подписей к осям.
	void setAxisTitles(const QString& aXTitle, const QString& aYTitle);

private:
    /*!
        Слот подстройки масштаба для отображения указанной области.
        \param aBoundRect - область видимости.
    */
    void fitInView();

	//! Функция обновления области, занимаемой данными.
	void updateBound();

	//! Функция подсчета размера для подписей оси Y.
	void updateYMargin();

	/*!
		Функция получения масштаба, при котором будут видны все данные с учетом указанных отступов от краев окна.
		\param aIsVertical - флаг масштаба по-горизонтали (по умолчанию возвращает значение по-вертикали).
		\return Значение мастаба, при котором будут видны все данные.
	*/
	qreal getFitScale(bool aIsVertical = false);

	/*!
		Функция отрисовки сетки.
		\param aRect - видимый прямоугольник.
	*/
	void paintGrid(QPainter* aPainter, const QRectF& aRect);

	//! Фукция получения размера области, занимаемой подписами для оси X.
	qreal getXMargin();

	//! Фукция получения размера области, занимаемой подписами для оси Y.
	qreal getYMargin();

	//! Функция перевода числа в строку.
	QString format(double aValue, int f);

	//! Функция получения видимого прямоугольника.
	QRectF visibleRect();

	//! Функция получения размера ячейки для текущего мастаба.
	QSizeF getCellSize();

	//! Функция получения прямоугольника, кратного размеру ячеек.
	QRectF getCellRectangle(const QRectF& aVisibleRect, const QSizeF& aCellSize);

	QPair <qreal, qreal> getStep(qreal aLength);

	qreal getSize(qreal aLength);

private:
	QSize mSize;
	QFont mFont;

	qreal mXScale;		//текущий мастаб по X.
	qreal mYScale;		//текущий мастаб по Y.

	QString mXTitle;
	QString mYTitle;

	QPointF mCurPosition;	//текущее смещение отображения.
	QRectF mBoundRect;	//область, занимаемая данными.
	QRectF mManualBoundRect; //область, занимаемая данными, заданная вручную.

	qreal mVertSize;
	qreal mHorSize;
	qreal mTextSize;
	QSizeF mCellSize;	//Размер ячейки для построения координатной сетки.

    QPen mPen;
    QPen mGridPen;

	QColor mTextColor;
	QColor mBackgroundColor;

	QVector <CXGraphicData*> mDataList;
};

#endif // CXGRAPHIC_H
