#include "CXGraphicData.h"

#define RECT_SIZE 7.0

CXGraphicData::CXGraphicData()
{
	mPen.setColor(Qt::red);
	mPen.setStyle(Qt::SolidLine);

	mXScale = -1;
	mYScale = -1;
}

CXGraphicData::~CXGraphicData()
{

}

void CXGraphicData::setPen(const QPen& aPen)
{
	mPen = aPen;
}

void CXGraphicData::setPoints(const QVector <QPointF>& aPoints)
{
	mBound = QRectF();
	mPath = QPainterPath();

	QPointF topLeft;
	QPointF rightBottom;

	QPointF curPoint;
	mPoints = aPoints;
	for (int i = 0; i < mPoints.count(); i++)
	{
		curPoint = mPoints.at(i);
		curPoint.setY(-curPoint.y());

		if (i == 0)
		{
			topLeft = curPoint;
			rightBottom = curPoint;

			continue;
		}

		if (topLeft.x() > curPoint.x()) topLeft.setX(curPoint.x());
        if (topLeft.y() > curPoint.y()) topLeft.setY(curPoint.y());

		if (rightBottom.x() < curPoint.x()) rightBottom.setX(curPoint.x());
        if (rightBottom.y() < curPoint.y()) rightBottom.setY(curPoint.y());
	}

	if (topLeft != rightBottom) mBound = QRectF(topLeft, rightBottom);
	
	if (!mBound.isNull()) 
	{ 
		if (mBound.height() == 0) mBound.adjust(0, -0.00001, 0, 0.00001); 
		if (mBound.width() == 0) mBound.adjust(-0.00001, 0, 0.00001, 0); 
	}	
}

QRectF CXGraphicData::boundingRect()
{
	return mBound;
}

void CXGraphicData::paint(QPainter* aPainter, qreal aXScale, qreal aYScale, const QPointF& aTranslate)
{
	if (aPainter == NULL) return;

	aPainter->setPen(mPen);

	if (mPath.isEmpty() || aXScale != mXScale || aYScale != mYScale)
	{
		mXScale = aXScale;
		mYScale = aYScale;

		mPath = QPainterPath();
		
		QPointF p;

		QVector<QPointF>::iterator iter;
		for (iter = mPoints.begin(); iter != mPoints.end(); iter++)
		{
			p = *iter;
			p.setY(-p.y());

			if (mPath.elementCount() == 0)
			{
				mPath.moveTo(p);
				continue;
			}

			mPath.lineTo(p);
		}
	}

	if (mPath.elementCount() > 1)
	{
		aPainter->drawPath(mPath);

		// Рисовать прямоугольники вокруг точек.
		QTransform transform;
		transform.scale(aXScale, aYScale);
		transform.translate(aTranslate.x(), aTranslate.y());

		aPainter->translate(-aTranslate);
		aPainter->scale(1.0 / aXScale, 1.0 / aYScale);

		QRectF rect;

		for (int i = 0; i < mPath.elementCount(); i++)
		{
			rect = QRectF(transform.map(mPath.elementAt(i)), QSizeF(RECT_SIZE, RECT_SIZE));
			rect.translate(-rect.width() / 2.0, -rect.height() / 2.0);

			aPainter->drawRect(rect);
		}

		aPainter->scale(aXScale, aYScale);
		aPainter->translate(aTranslate);
	}
}
