#include "CXGraphic.h"

#include <QPainter>
#include <QPixmap>
#include <qmath.h>

#define MARGIN 5.0	//отступ графика от краев
#define GRIDMARGIN 5.0	//отступ линейки от границ
#define GRIDSPACE 2.0	//отступ подписей от линейки и от подписей к осям.

CXGraphic::CXGraphic()
{
	mSize.setWidth(400);
	mSize.setHeight(400);

  int fontSize = mFont.pointSize ( );
  mFont.setPointSize ( fontSize + 5 );
	QFontMetricsF fontMetrics(mFont);
	mTextSize = fontMetrics.height();
	mFont.setPointSize ( fontSize);

	//размер области под подписи для вертикальной шкалы
    mVertSize = 20;

	//размер области под подписи для горизонтальной шкалы
    mHorSize = mTextSize;

    mCellSize = QSizeF(40, 40);

    mXScale = 1.0;
    mYScale = 1.0;

    mGridPen = QPen(Qt::black, 0, Qt::DotLine);
    mPen = QPen(Qt::black, 0, Qt::SolidLine);

    mTextColor = Qt::black;
    mBackgroundColor = Qt::white;
	
	//mTextSize = 0;
}

CXGraphic::~CXGraphic()
{
}

void CXGraphic::setGridPen(const QPen& aPen)
{
    mGridPen = aPen;
}

void CXGraphic::setPen(const QPen& aPen)
{
    mPen = aPen;
}

void CXGraphic::setTextColor(const QColor& aTextColor)
{
    mTextColor = aTextColor;
}

void CXGraphic::setBackgroundColor(const QColor& aBackgroundColor)
{
    mBackgroundColor = aBackgroundColor;
}

void CXGraphic::setAxisRange(qreal aXMin, qreal aXMax, qreal aYMin, qreal aYMax)
{
	mManualBoundRect = QRectF(QPointF(aXMin, -aYMax), QPointF(aXMax, -aYMin));
	updateBound();
	fitInView();
}

void CXGraphic::setSize(const QSize& aSize)
{
	mSize = aSize;

	updateYMargin();
	updateBound();
	fitInView();
}

void CXGraphic::setData(const QVector<QPair<double, double> >& aData)
{
	qDeleteAll(mDataList);
	mDataList.clear();

	CXGraphicData* newData = new CXGraphicData;
	QVector <QPointF> points;
	for (int i = 0; i < aData.count(); i++)
	{
		points.append(QPointF(aData.at(i).first, aData.at(i).second));
	}

	newData->setPoints(points);

	mDataList.append(newData);

	updateYMargin();
    updateBound();
	fitInView();
}

void CXGraphic::setAxisTitles(const QString& aXTitle, const QString& aYTitle)
{
	mXTitle = aXTitle;
	mYTitle = aYTitle;
}

QPixmap CXGraphic::getGraphic()
{
	QPixmap pixmap(mSize);//, "PNG");
	pixmap.fill(mBackgroundColor);

	QPainter painter;
	painter.begin(&pixmap);

	paintGrid(&painter, visibleRect());

	painter.scale(mXScale, mYScale);
	painter.translate(-mCurPosition);

	painter.setRenderHint(QPainter::Antialiasing);

	foreach (CXGraphicData* data, mDataList)
	{
		if (data != NULL) data->paint(&painter, mXScale, mYScale, QPointF(0, 0));
	}

	painter.end();

	return pixmap;
}

void CXGraphic::fitInView()
{
	mXScale = getFitScale(false);
	mYScale = getFitScale(true);

	QSizeF formSize = mSize - QSizeF(getYMargin() + 2.0 * MARGIN, getXMargin() + 2.0 * MARGIN);
	formSize.setWidth(formSize.width() / mXScale);
	formSize.setHeight(formSize.height() / mYScale);
	formSize = (mBoundRect.size() - formSize) / 2.0;

	mCurPosition = mBoundRect.topLeft() - QPointF((getYMargin() + MARGIN) / mXScale, MARGIN / mYScale) + QPointF(formSize.width(), formSize.height());
}

void CXGraphic::updateBound()
{
	mBoundRect = QRectF();

	foreach (CXGraphicData* data, mDataList)
	{
		if (data == NULL) continue;

		if (mBoundRect.isNull()) mBoundRect = data->boundingRect();
		else mBoundRect = mBoundRect.united(data->boundingRect());
	}

	qreal w = 100;

	/*if (mBoundRect.left() > 0) mBoundRect.setLeft(0.0);
	else */mBoundRect.setLeft(int(mBoundRect.left()) - w);

	if (mBoundRect.right() < 0) mBoundRect.setRight(0.0);
	else mBoundRect.setRight(int(mBoundRect.right()) + w);

	if (mBoundRect.top() > 0) mBoundRect.setTop(0.0);
	if (mBoundRect.bottom() < 0) mBoundRect.setBottom(0.0);

	qreal h = getSize(mBoundRect.height());

	qreal b = int(mBoundRect.bottom() / h) * h;
	qreal t = int(mBoundRect.top() / h) * h;

	if (b < mBoundRect.bottom()) b += h;
	if (t > mBoundRect.top()) t -= h;

	mBoundRect.setBottom(b);
	mBoundRect.setTop(t);

	QPair <qreal,qreal> data = getStep(mBoundRect.height());
	qreal step = data.first;
	mBoundRect.setHeight(data.second);
/*
	qreal top = int(mBoundRect.top() / step) * step + step;

	if (qAbs(top - mBoundRect.top()) > 0.7 * step) top -= 2.0 * step;
	else top -= step;

	mBoundRect.setTop(top);

	qreal bottom = int(mBoundRect.bottom() / step) * step;

	if (qAbs(bottom - mBoundRect.bottom()) > 0.7 * step) bottom += 2.0 * step;
	else bottom += step;

	mBoundRect.setBottom(bottom);
/**/
	if (mBoundRect.top() > 0) mBoundRect.setTop(0.0);
	if (mBoundRect.bottom() < 0) mBoundRect.setBottom(0.0);

	if (mManualBoundRect.width() > 0)
	{
		mBoundRect.setX(mManualBoundRect.x());
		mBoundRect.setWidth(mManualBoundRect.width());
	}

	if (mManualBoundRect.height() > 0)
	{
		mBoundRect.setY(mManualBoundRect.y());
		mBoundRect.setHeight(mManualBoundRect.height());
	}
}

void CXGraphic::updateYMargin()
{
	QFontMetricsF fontMetrics(mFont);

	QSizeF cellSize = getCellSize();
	QRectF rect = getCellRectangle(visibleRect(), cellSize);

	qreal curY = 0;
	for (qreal y = rect.top(); y <= rect.bottom(); y += cellSize.height())
	{
		mVertSize = qMax(mVertSize, fontMetrics.width(format(-y, 0)));
	}
}

qreal CXGraphic::getFitScale(bool aIsVertical)
{
    QSizeF s = mBoundRect.size();

    if (s.width() <= 0 || s.height() <= 0) return 1;

    if (aIsVertical)
    {
        return (height() - getXMargin() - 2.0 * MARGIN) / s.height();
    }
    else
    {
        return (width() - getYMargin() - 2.0 * MARGIN) / s.width();
    }
}

void CXGraphic::paintGrid(QPainter* aPainter, const QRectF& aRect)
{
    if (aPainter == NULL) return;

    aPainter->setPen(mPen);

	qreal x = getYMargin();
	qreal y = height() - getXMargin();

    aPainter->drawLine(QPointF(x, 0), QPointF(x, y));
    aPainter->drawLine(QPointF(x, y), QPointF(width(), y));

	QSizeF cellSize = getCellSize();
	QRectF cellRect = getCellRectangle(aRect, cellSize);

    QFontMetricsF fontMetrics(mFont);

    QRectF oldRect;
    QRectF textRect(0, height() - getXMargin() + GRIDSPACE, mCellSize.width(), mHorSize);
    qreal curX = 0;
    QString value;
    for (qreal x = cellRect.left(); x <= cellRect.right(); x += cellSize.width())
	{
		curX = x - mCurPosition.x();
		curX *= mXScale;

        aPainter->setPen(mGridPen);
        aPainter->drawLine(QPointF(curX, 0), QPointF(curX, textRect.top() - GRIDSPACE));

        value = format(x, 1);

        textRect.setWidth(fontMetrics.width(value));
        textRect.moveTo(curX - textRect.width() / 2.0, textRect.y());

        if (x != cellRect.left() && oldRect.intersects(textRect))
        {
            continue;
        }

        oldRect = textRect;

        aPainter->setPen(mTextColor);
        aPainter->drawText(textRect, value, QTextOption(Qt::AlignTop | Qt::AlignHCenter));
    }

    textRect = QRectF(getYMargin() - mVertSize - GRIDSPACE, 0, mVertSize, mHorSize);

    qreal curY = 0;
    for (qreal y = cellRect.top(); y <= cellRect.bottom(); y += cellSize.height())
	{
		curY = y - mCurPosition.y();
		curY *= mYScale;

        aPainter->setPen(mGridPen);
        aPainter->drawLine(QPointF(textRect.right() + GRIDSPACE, curY), QPointF(width(), curY));
        textRect.moveTo(textRect.x(), curY - textRect.height() / 2.0);

        aPainter->setPen(mTextColor);
		value = format(-y, 0);
        aPainter->drawText(textRect, value, QTextOption(Qt::AlignVCenter | Qt::AlignRight));
	}

	textRect = QRectF(getYMargin(), height() - mTextSize - MARGIN, width() - getYMargin(), mTextSize);

	aPainter->setPen(mTextColor);

  QFont font=aPainter->font() ;
  int fontSize = font.pointSize ( );
  font.setPointSize ( fontSize + 5 );
  aPainter->setFont(font);

	aPainter->drawText(textRect, mXTitle, QTextOption(Qt::AlignCenter));

	textRect = QRectF(GRIDMARGIN, height() - mTextSize - MARGIN, height() - getXMargin(), mTextSize);
	//textRect = QRectF(GRIDMARGIN, 0, mTextSize, height() - getXMargin());

	aPainter->save();
	aPainter->setRenderHint(QPainter::TextAntialiasing);

	aPainter->translate(textRect.topLeft());
	aPainter->rotate(-90);
	aPainter->drawText(QRectF(QPointF(0, 0), textRect.size()), mYTitle, QTextOption(Qt::AlignCenter));

	font.setPointSize ( fontSize );
  aPainter->setFont(font);
	aPainter->restore();
}

qreal CXGraphic::getXMargin()
{
	return GRIDMARGIN + mTextSize + GRIDSPACE + mHorSize + GRIDSPACE;
}

qreal CXGraphic::getYMargin()
{
	return GRIDMARGIN + mTextSize + GRIDSPACE + mVertSize + GRIDSPACE;
}

QString CXGraphic::format(double aValue, int _frac)
{
	return QString("%1").arg(aValue, 2, 'f',1, QChar()).replace(QRegExp("\\.0*$"), "");
}

QRectF CXGraphic::visibleRect()
{
	return QRectF(mCurPosition.x() + getYMargin() / mXScale, mCurPosition.y(), (width() - getYMargin()) / mXScale, (height() - getXMargin()) / mYScale);
}

QSizeF CXGraphic::getCellSize()
{
	qreal cellSizeX = 100;
	qreal cellSizeY = getStep(mBoundRect.height()).first;
/*
	qreal cellSizeX = mCellSize.width();
	if (mXScale > 1.0)
	{
		while (cellSizeX * mXScale > 1.5 * mCellSize.width()) cellSizeX /= 2;
	}
	else
	{
		while (cellSizeX * mXScale < mCellSize.width() / 1.5) cellSizeX *= 2;
	}

	qreal cellSizeY = mCellSize.height();

	if (mYScale > 1.0)
	{
		while (cellSizeY * mYScale > 1.5 * mCellSize.height()) cellSizeY /= 2;
	}
	else
	{
		while (cellSizeY * mYScale < mCellSize.height() / 1.5) cellSizeY *= 2;
	}
*/
	return QSizeF(cellSizeX, cellSizeY);
}

QRectF CXGraphic::getCellRectangle(const QRectF& aVisibleRect, const QSizeF& aCellSize)
{
	qreal cellSizeX = aCellSize.width();
	qreal cellSizeY = aCellSize.height();

	qreal left = cellSizeX * int(aVisibleRect.left() / cellSizeX);
	qreal top = cellSizeY * int(aVisibleRect.top() / cellSizeY);
	qreal right = cellSizeX * int(aVisibleRect.right() / cellSizeX + 1);
	qreal bottom = cellSizeY * int(aVisibleRect.bottom() / cellSizeY);

	if (left < aVisibleRect.left()) left += cellSizeX;
	if (bottom > aVisibleRect.bottom()) bottom -= cellSizeY;

	return QRectF(QPointF(left, top), QPointF(right, bottom));
}
/*
qreal getStep1(qreal aLength)
{
	int pow = QString("%1").arg(int(aLength)).length() - 1;
	return qPow(10, pow);
}
*/

bool isDivide(qreal a, qreal b)
{
	return b * int(a / b) == a;
}

QPair <qreal, qreal> CXGraphic::getStep(qreal aLength)
{
	//int pow = QString("%1").arg(int(aLength)).length() - 1;
	qreal pow = getSize(aLength);

	qreal step1 = 0.5 * pow;
	qreal step2 = int(step1 / 2.0);

	if (step2 == 0) step2 = step1;
	QVector<int> linesCount;
	linesCount << 6 << 8 << 10;

	bool isBreak = false;

	while (step1 < aLength && step2 < aLength)
	{
		for (int i = 0; i < linesCount.count(); i++)
		{
			if (isDivide(aLength, step1) && isDivide(aLength / step1, linesCount.at(i)))
			{
				int v = int(aLength / step1 / linesCount.at(i));

				if (v == 1 || v % 2)
				{
					isBreak = true;
					break;
				}
			}
			
			if (isDivide(aLength, step2) && isDivide(aLength / step2, linesCount.at(i)))
			{
				int v = int(aLength / step2 / linesCount.at(i));

				if (v == 1 || v % 2)
				{
					isBreak = true;
					break;
				}
			}
		}

		if (isBreak) break;

		aLength += pow;
	}
	
	qreal tempValue = 0;
	qreal resStep = step2;
	qreal minValue = step1;
	qreal minLines = linesCount.first();

	while (aLength / minLines > step2)
	{
		for (int i = 0; i < linesCount.count(); i++)
		{
			tempValue = aLength / linesCount.at(i);

			if (tempValue - step1 <= 0 && qAbs(minValue) >= qAbs(tempValue - step1))
			{
				minValue = tempValue - step1;
				resStep = step1;
			}

			if (tempValue - step2 <= 0 && qAbs(minValue) >= qAbs(tempValue - step2))
			{
				minValue = tempValue - step2;
				resStep = step2;
			}
		}

		step1 *= 2.0;
		step2 *= 2.0;
	}

	return QPair<qreal,qreal> (resStep, aLength);
}

qreal CXGraphic::getSize(qreal aLength)
{
	int pow = QString("%1").arg(int(aLength)).length() - 1;
	return qPow(10, pow);
}
