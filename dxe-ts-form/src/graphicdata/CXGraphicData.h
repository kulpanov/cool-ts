#ifndef CXGRAPHICDATA_H
#define CXGRAPHICDATA_H

//#include "graphiclibrary_global.h"

#include <QPen>
#include <QPainter>

/*!
	Класс для отображения одного графика.
*/
class CXGraphicData
{
public:
	//! Конструктор.
	CXGraphicData();

	//! Деструктор.
	virtual ~CXGraphicData();

	/*!
		Установка стиля для отрисовки линии (кисти).
		\param aPen - кисть для рисования.
	*/
	void setPen(const QPen& aPen);

	/*!
		Установка точек для графика.
		\param aPoints - список точек графика.
	*/
	void setPoints(const QVector <QPointF>& aPoints);

	/*!
		Функция получения прямоугольника, занимаемого объектом.
		\return Прямоугольник, занимаемый объектом.
	*/
	virtual QRectF boundingRect();

	/*!
		Функци отрисовки данных.
		\param aPainter - указатель на объект QPainter.
		\param aXScale - масштаб по X.
		\param aYScale - масштаб по Y.
		\param aTranslate - текущее смещение.
	*/
	virtual void paint(QPainter* aPainter, qreal aXScale, qreal aYScale, const QPointF& aTranslate);

private:
	QPen mPen;
	QVector <QPointF> mPoints;
	QRectF mBound;

	QPainterPath mPath;
	qreal mXScale;
	qreal mYScale;
};

#endif // CXGRAPHICDATA_H
