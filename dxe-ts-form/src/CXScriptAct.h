#ifndef CXSCRIPTACT_H
#define CXSCRIPTACT_H
#include <stddef.h>
#include "AXAbstractAct.h"

#include <QLineEdit>
#include <QLabel>
#include <QPushButton>

/*!
	Класс акта "Сценарий".
*/
class CXScriptAct : public AXAbstractAct
{
	Q_OBJECT

public:
	CXScriptAct(const QString& aName = QString(), QWidget* parent = 0);
	~CXScriptAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

	/*!
		Функция установки пути с именем редактируемого файла.

		\param aFileName Путь с именем редактируемого файла
	*/
	void setEditFile(const QString& aFileName);

private slots:
	void onCreate();

private:
	QLineEdit* mDescr;
	QLabel* mFileName;
	QPushButton* mCreateButton;
};

#endif // CXSCRIPTACT_H
