#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QFileSystemModel>
#include <QMenu>
#include <QTimer>
#include <QObject>

//#define TEST_FORM
//#define TEST_X


#include "ui_mainwindow.h"
#include "CXEditWindow.h"

#ifndef TEST_FORM
#include "dxe-ts-engine.h"
#include "dxe-ts-engine.h"
#else
namespace DXE{
struct SMsg_log{
  //кто запустил (ex 1.2)
  QString id;
  //сообщение
  QString msg;
  //код возврата
  int result;
  //показывать ли полную дату
  bool fulldate;
public:
  SMsg_log(const QString& _id, const QString& _msg
      , int _result, bool _fulldate = false)
    :id(_id), msg(_msg), result(_result), fulldate(_fulldate){
  }

  SMsg_log():
    result(-1),fulldate(false)
  {  }

  QString toString() const{
    return QString("%0, %1, %2 %3")
        .arg(id).arg(msg)
        .arg(result==0?"pass":result==1?"fail":result==2?"msg":"started")
        .arg(fulldate?",fulldata":" ");
  }
};

class ILogListener{
public:
  enum{
    pass = 0
    ,fail
    ,msg
    ,started
  };
  enum{
    fulldate = true
  };

  virtual void recvLog(const SMsg_log& _msg) = 0;
protected:
  virtual ~ILogListener(){  }
};
}

#endif

class MainWindow;
using namespace DXE;
/*!
	Класс главного окна приложения.
*/
class MainWindow : public QWidget
{
	Q_OBJECT;

	//bool scriptIsExecuting;
	bool logIsCleared;

public:
	MainWindow(QWidget* parent = 0, Qt::WFlags flags = 0);
	~MainWindow();
protected:
	virtual void closeEvent(QCloseEvent *event);
public:
	/*!
		Функция на выполнение файла.

		\param aFileName Путь с имененм файла на выполнение.
	*/
	void executeFile(const QString& aFileName);

	/*!
		Функция на правку файла.

		\param aFileName Путь с имененм файла на правку.
	*/
	void editFile(const QString& aFileName);

//#ifdef TEST_FORM

//#endif


private:
	Ui::MainWindowClass ui;

	QFileSystemModel* mModel;
	QMenu* mMenu;
  QMenu* mLogMenu;

	CXEditWindow* mEditWindow;
	//MainWindow_waitThread* waitThread;
	//QThreadPool* testPool;
	bool permissionOfExecute;
	//история смен каталогов
	QModelIndexList indexHistory;
public:
	QString test_fname;
	bool test_result;
	QString test_log;
	int cur_test_id;

	QTimer testTimer;
private slots:
  void DoTestStep(/*int _ntest*/);

  /*!
    Слот обработки двойного нажатия на элемент в списке файлов и каталогов.

    \param aIndex QModelIndex элемента.
  */
  void onItemDoubleClick(const QModelIndex& aIndex);

  /*!
    Слот обработки показа контестного меню в списке файлов и каталогов.

    \param aPoint Координаты нажатия.
  */
  void onContextMenu(const QPoint& aPoint);

  void onContextMenu_LogTree(const QPoint& aPoint);

  void onMenuTrigger_LogTree(QAction* aAction);

  /*!
    Слот показа настроек приложения.
  */
  void onShowSettings();

  /*!
    Слот обработки перехода на каталог выше.
  */
  void onBack();

  /*!
    Слот обработки показа информации о приложении.
  */
  void onAbout();

  /*!
    Слот открытия проводника с текущим путем.
  */
  void onOpenExplorer();

  /*!
    Слот обработки нажатия на элементы контекстного меню.

    \param aAction Указатель на пункт меню.
  */
  void onMenuTrigger(QAction* aAction);

  /*!
    Слот создания нового файла с данными.
  */
  void onNew();

  /** Сменить рабочий каталог, слот
   */
  void onDir();

  void onLoopAll();
  /*!
    Слот завершения работы тесты
  */
  //void onLogMsg(const QString&, const QString&, int, int);
  ///
  void onLogMsg(const SMsg_log& _msg);

  void onDrawVec(const QString& _fname, const QString& _fname_script, const QVector<QPair<double, double> >& _vec);
  /**Отложенный старт
   */
  void onDelayedStart();
  //
  void onPeriodicalTimer();

  void onRunDlg(int _dlg, void* _param);
protected:
  virtual void resizeEvent(QResizeEvent *);
private:
  QTimer startTimer;
};

using namespace DXE;
///
class CLogListener_form: public QObject, public DXE::ILogListener{
Q_OBJECT;
public:
  virtual void recvMsg(const SMsg_log& _msg);

  virtual void recvMsg(const QString& _fname, const QString& _fname_script, const QVector<QPair<double, double> >& _vec);

  virtual void runDlg(const int , void*);
signals:
  //void logTo(const QString& _id, const QString& _msg, int _result, int _fulldate);
  void logTo(const SMsg_log& );

  void logVector(const QString& ,const QString& , const QVector<QPair<double, double> >& );

  void sgn_runDlg(const int, void*);
public:
  CLogListener_form(MainWindow* _log_receiver);
};

#endif // MAINWINDOW_H
