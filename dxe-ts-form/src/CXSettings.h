#ifndef CXSETTINGS_H
#define CXSETTINGS_H

#include <QMap>
#include <QVariant>

//! Перечень доступных настроек.
enum eSettingType
{
	E_None = 0,

	E_DirPath,			//!< Путь к рабочему каталогу.
	E_DefaultEditView,	//!< Вид редактора по-умолчанию
	E_WavPath      //!< Путь к каталогу записанных файлов.
};

/*!
	Singleton класс хранения, получения и сохранения настроек приложения.
*/
class CXSettings
{
public:
	/*!
		Статическая функция полечения экземпляра класса в соответствии с паттерном Singleton.
	*/
	static CXSettings* inst();

	/*!
		Статическая функция получения значения параметра настройки.
	*/
	static QVariant valueStat(eSettingType aType);

	/*!
		Статическая функция установки значения параметра настройки.
	*/
	static void setValueStat(eSettingType aType, const QVariant& aValue);

private:
	CXSettings();

public:
	~CXSettings();

	/*!
		Функция загрузки настроек из xml-файла.

		\param aFileName - имя загружаемого xml-файла.
	*/
	void load(const QString& aFileName);

	/*!
		Функция сохранения настроек в xml-файл.

		\param aFileName - имя сохраняемого xml-файла.
	*/
	void save(QString aFileName = QString());

	/*!
		Функция установки значения параметра настройки.

		\param aType - тип параметра настройки.
		\param aValue - значение параметра настройки.
	*/
	void setValue(eSettingType aType, const QVariant& aValue);

	/*!
		Функция получения значения параметра настройки.

		\param aType - тип параметра настройки.

		\return Значение параметра настройки.
	*/
	QVariant value(eSettingType aType);

private:
	static CXSettings* mSettings;

	QMap <int, QVariant> mSettingsValue;

	QMap <int, QString> mTypeDescription;
};

#endif // CXSETTINGS_H
