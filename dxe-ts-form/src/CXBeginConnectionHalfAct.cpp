#include "CXBeginConnectionHalfAct.h"

#include "CXActConnectHelper.h"

#include <QLabel>

CXBeginConnectionHalfAct::CXBeginConnectionHalfAct(QWidget* parent) : CXBaseAct(parent)
{
	mType = E_BeginConnectionHalfAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

//	centralLayout->addWidget(new QLabel(tr("id"), mContainer));
//
//	mIDEdit = new QLineEdit(mContainer);
//	mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
//	centralLayout->addWidget(mIDEdit);

	centralLayout->addWidget(new QLabel(tr("исходящий на номер"), mContainer));

	mOutNumberEdit = new QLineEdit(mContainer);
	mOutNumberEdit->setValidator(new QRegExpValidator(QRegExp("[\\d]+"), mIDEdit));
	mOutNumberEdit->setMaximumWidth(50);
	//задаем маску обязательных 6 цифр
	mOutNumberEdit->setMaxLength(7);
	centralLayout->addWidget(mOutNumberEdit);

//	centralLayout->addWidget(new QLabel(tr("номер шлюза входящего вызова"), mContainer));
//
//  mGwNumberEdit = new QLineEdit(mContainer);
//  //задаем маску обязательных 3 цифр
//  mGwNumberEdit->setValidator(new QRegExpValidator(QRegExp("[\\d-*]+"), mIDEdit));
//  mGwNumberEdit->setMaxLength(20);
//  mGwNumberEdit->setMaximumWidth(50);
//  centralLayout->addWidget(mGwNumberEdit);
//
//	//centralLayout->addWidget(new QLabel(tr("доп"), mContainer));
//
//	mSrcEdit = new QLineEdit(mContainer);
//	mSrcEdit->setValidator(new QRegExpValidator(QRegExp("[\\d]+"), mIDEdit));
//	centralLayout->addWidget(mSrcEdit);
//	mSrcEdit->setText("388");
//	mSrcEdit->setVisible(false);

	centralLayout->addWidget(new QLabel(tr("ip-адрес шлюза"), mContainer));

	mTargetIpEdit = new QLineEdit(mContainer);
	mTargetIpEdit->setValidator(new QRegExpValidator(QRegExp(
	"^((1?\\d{1,2}|2[0-4]\\d|25[0-5])\\.){3}(1?\\d{1,2}|2[0-4]\\d|25[0-5])$"), mIDEdit));
	mTargetIpEdit->setFixedWidth(150);
	centralLayout->addWidget(mTargetIpEdit);

	centralLayout->addStretch();
}

CXBeginConnectionHalfAct::~CXBeginConnectionHalfAct()
{

}

void CXBeginConnectionHalfAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
	QString t_str = aElement.attribute("target");

	mOutNumberEdit->setText(aElement.attribute("out"
	    , aElement.attribute("target").mid(0, 3)));
//	mGwNumberEdit->setText(aElement.attribute("gw"
//	    , aElement.attribute("target").mid(3, 3)));
//
//	mSrcEdit->setText("388"); //aElement.attribute("add"));
	mTargetIpEdit->setText(aElement.attribute("target_ip"));
}

QDomElement CXBeginConnectionHalfAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("connect_half"));
	data << SXXMLData("descr", tr("Cоединиться с абонентом"));
	//data << SXXMLData("id", mIDEdit->text());
	QString trg = mOutNumberEdit->text() ;//+ mGwNumberEdit->text();
//	//забиваем 0 до 3 символов
//	for(int i = mIDEdit->text().size(); i < 3; i++)
//	  trg += "0";
//	trg +=  mIDEdit->text();

	data << SXXMLData("target", trg);
	data << SXXMLData("out", mOutNumberEdit->text());
//	data << SXXMLData("gw", mGwNumberEdit->text());
//	data << SXXMLData("src", mSrcEdit->text());
	data << SXXMLData("target_ip", mTargetIpEdit->text());

	return CXBaseAct::getXMLElement(aDomDocument, data);
}



