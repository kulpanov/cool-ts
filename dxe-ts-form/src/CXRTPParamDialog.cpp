#include <iostream>
#include <QMessageBox>

#include "CXRTPParamDialog.h"

#include "utils/CXMLReader.h"

#include "dxe-call-server.h"

//#include "wavStream.h"

CXRTPParamDlg::CXRTPParamDlg(QWidget *parent, void* _param) :
    QDialog(parent) {
  ui.setupUi(this);
  call = _param;

  connect(ui.packetPlus, SIGNAL(pressed()),  ui.packetSize, SLOT(stepUp()));
  connect(ui.packetMinus, SIGNAL(pressed()), ui.packetSize, SLOT(stepDown()));

  connect(ui.jitterPlus, SIGNAL(pressed()),  ui.jitter, SLOT(stepUp()));
  connect(ui.jitterMinus, SIGNAL(pressed()), ui.jitter, SLOT(stepDown()));

  connect(ui.quantPlus, SIGNAL(pressed()),  ui.quant, SLOT(stepUp()));
  connect(ui.quantMinus, SIGNAL(pressed()), ui.quant, SLOT(stepDown()));

  connect(ui.packetSize, SIGNAL(valueChanged(int)), this, SLOT(onPacket(int)));
  connect(ui.jitter, SIGNAL(valueChanged(int)), this, SLOT(onJitter(int)));
  connect(ui.quant, SIGNAL(valueChanged(double)), this, SLOT(onQuant(double)));

  ui.packetSize->setValue(IWavStream::rtp_packet_size / 8);

  this->setWindowTitle(QString::fromUtf8("Параметры RTP для номера %1")
      .arg(static_cast<DXE::ICallManager*>(call)->getConnectNumber().c_str()));
//  IWavStream::rtp_packet_size
//  ui.packetSize->setText(QString().number(60));
//  ui.packetSize->setValidator( new QRegExpValidator(QRegExp("\\d+\\d+"),  ui.packetSize));
//
//  ui.jitter->setText(QString().number(0));
//  ui.jitter->setValidator( new QRegExpValidator(QRegExp("\\d+\\d+"),  ui.jitter));
//
//  ui.quant->setText(QString().number(8000));
//  ui.quant->setValidator( new QRegExpValidator(QRegExp("\\d+\\d+"),  ui.quant));

}

CXRTPParamDlg::~CXRTPParamDlg() {
  static_cast<DXE::ICallManager*>(call)->
      setParams(0, 0);
}

void CXRTPParamDlg::onPacket(int v){
  static_cast<DXE::ICallManager*>(call)->
      setParams(DXE::ICallManager::rtpPacketSize, v);
//  if(ui.jitter->value() > v/2)
    ui.jitter->setMaximum(v/2);
}

void CXRTPParamDlg::onJitter(int v){
  static_cast<DXE::ICallManager*>(call)->
      setParams(DXE::ICallManager::rtpJitter, v);
}

void CXRTPParamDlg::onQuant(double v){
  static_cast<DXE::ICallManager*>(call)->
      setParams(DXE::ICallManager::rtpQuant, v*10);
}
