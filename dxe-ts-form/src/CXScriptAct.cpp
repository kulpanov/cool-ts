#include "CXScriptAct.h"

#include <QVBoxLayout>

#include "CXActsManager.h"

CXScriptAct::CXScriptAct(const QString& aName, QWidget* parent) : AXAbstractAct(parent)
{
	QHBoxLayout* centralLayout  = new QHBoxLayout(this);

	QVBoxLayout* verticallLayout = new QVBoxLayout;

	verticallLayout->addWidget(new QLabel(tr("Начало сценария")));

	QHBoxLayout* horizontalLayout = new QHBoxLayout;

	horizontalLayout->addWidget(new QLabel(tr("Наименование сценария:")));

	mDescr = new QLineEdit(this);

	if (aName.isEmpty()) mDescr->setText(tr("Сценарий"));
	else mDescr->setText(aName);

	horizontalLayout->addWidget(mDescr);

	verticallLayout->addLayout(horizontalLayout);

	mFileName = new QLabel(this);
	verticallLayout->addWidget(mFileName);

	centralLayout->addLayout(verticallLayout);

	mCreateButton = new QPushButton(this);
	mCreateButton->setToolTip(tr("Создать новый акт"));
	mCreateButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	mCreateButton->setIcon(QIcon(":/copydown.png"));
	mCreateButton->setIconSize(QSize(20, 20));

	centralLayout->addWidget(mCreateButton);

	connect(mCreateButton,	SIGNAL(clicked()), this, SLOT(onCreate()));
}

CXScriptAct::~CXScriptAct()
{

}

void CXScriptAct::load(const QDomElement& aElement)
{
	Q_UNUSED(aElement)
}

QDomElement CXScriptAct::getXMLElement(QDomDocument& aDomDocument)
{
	QDomElement element = aDomDocument.createElement("script");

	element.setAttribute("descr", mDescr->text());

	return element;
}

void CXScriptAct::setEditFile(const QString& aFileName)
{
	mFileName->setText(QString().fromUtf8("Место расположения сценария: ") + aFileName);
}

void CXScriptAct::onCreate()
{
	if (AXAbstractAct::mActsManager != NULL)
	{
		AXAbstractAct::mActsManager->appendAct(E_BeginConnectionAct);
	}
}
