#ifndef CXENDSCRIPTACT_H
#define CXENDSCRIPTACT_H

#include "AXAbstractAct.h"

/*!
	Класс акта "Конец сценария".
*/
class CXEndScriptAct : public AXAbstractAct
{
public:
	CXEndScriptAct(QWidget* parent = 0);
	~CXEndScriptAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);
};

#endif // CXENDSCRIPTACT_H
