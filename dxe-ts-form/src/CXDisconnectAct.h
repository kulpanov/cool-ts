#ifndef CXDISCONNECTACT_H
#define CXDISCONNECTACT_H

#include "CXBaseAct.h"

#include <QLineEdit>

/*!
	Класс акта "Разъединить".
*/
class CXDisconnectAct : public CXBaseAct
{
public:
	CXDisconnectAct(QWidget* parent = 0);
	~CXDisconnectAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
};

#endif // CXDISCONNECTACT_H
