#ifndef CXSETTINGSDIALOG_H
#define CXSETTINGSDIALOG_H

#include <QDialog>
#include <QList>
#include <QNetworkInterface>

#include "ui_CXSettingsDialog.h"

/*!
	Класс диалога настроек.
*/
class CXSettingsDialog : public QDialog
{
	Q_OBJECT
public:
	CXSettingsDialog(QWidget *parent = 0);
	~CXSettingsDialog();
public:
	bool checkAdapters();
	QString getCurrAdapter();
public slots:
	virtual void accept();

private slots:
  void setInterface();
  void onItemClick(QTreeWidgetItem* item, int column);
  void onClickExportLog();
private:
  void addAdapters();

private:
  //QList <QNetworkInterface> mInterfaces;
  QList<QNetworkAddressEntry> addr;
  int mIndex;

private:
	Ui::CXSettingsDialog ui;
};

#endif // CXSETTINGSDIALOG_H
