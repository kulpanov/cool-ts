#ifndef CXSYNTAXHIGHLIGHTER_H
#define CXSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextEdit>

/*!
	Класс подсветки синтаксиса
*/
class CXSyntaxHighlighter : public QSyntaxHighlighter
{
public:
	CXSyntaxHighlighter(QTextEdit* aTextEdit);
	~CXSyntaxHighlighter();

protected:
	/*!
		Переопределенная функция обработки подсветки.

		\param aText Текущий обрабатываемый текст.
	*/
	virtual void highlightBlock(const QString& aText);

private:
	QRegExp tagBodyStart;
	QRegExp tagBodyEnd;
	QRegExp tagNameEnd;
	QRegExp attrName;
	QRegExp attrBodyStart;
	QRegExp attrBodyEnd;

	QTextCharFormat tagBodyFormat;
	QTextCharFormat tagNameFormat;
	QTextCharFormat attrNameFormat;
	QTextCharFormat attrBodyFormat;
};

#endif // CXSYNTAXHIGHLIGHTER_H
