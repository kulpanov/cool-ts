#include "CXTransmitFileAct.h"

#include <QLabel>
#include <QToolButton>
#include <QFileDialog>

#include "CXSettings.h"

CXTransmitFileAct::CXTransmitFileAct(QWidget* parent) : CXBaseAct(parent)
{
	mType = E_TransmitFileAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

//	centralLayout->addWidget(new QLabel(tr("id"), mContainer));
//
//	mIDEdit = new QLineEdit(mContainer);
//	mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
//	centralLayout->addWidget(mIDEdit);

	centralLayout->addWidget(new QLabel(tr("со стороны"), mContainer));

	mDirectionBox = new QComboBox(mContainer);
	mDirectionBox->addItem(tr("Исходящего"));
	mDirectionBox->addItem(tr("Входящего"));
	centralLayout->addWidget(mDirectionBox);

	centralLayout->addWidget(new QLabel(tr("файл"), mContainer));

	mFileNameEdit = new QLineEdit(mContainer);
//	mFileNameEdit->setReadOnly(true);
	centralLayout->addWidget(mFileNameEdit);

	QToolButton* openButton = new QToolButton(mContainer);
	openButton->setText("...");
	centralLayout->addWidget(openButton);

	connect(openButton, SIGNAL(clicked()), this, SLOT(onOpenFile()));

	//centralLayout->addStretch();
}

CXTransmitFileAct::~CXTransmitFileAct()
{

}

void CXTransmitFileAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
  //mIDEdit->setText(aElement.attribute("id"));
	mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
	mFileNameEdit->setText(aElement.attribute("filename"));
}

QDomElement CXTransmitFileAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("play"));
	data << SXXMLData("descr", tr("Передать файл"));
	//data << SXXMLData("id", mIDEdit->text());
	data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));
	data << SXXMLData("type", tr("file_in"));
	data << SXXMLData("filename", mFileNameEdit->text());

	return CXBaseAct::getXMLElement(aDomDocument, data);
}

void CXTransmitFileAct::onOpenFile()
{
  QString testDir = CXSettings::valueStat(E_WavPath).toString().replace("\\", "/");
	QString fileName = QFileDialog::getOpenFileName(0, "", testDir, "WAV (*.wav);;RAW (*.raw)");

	if (!fileName.isEmpty())
	{
		QString testDir = CXSettings::valueStat(E_WavPath).toString().replace("\\", "/");
		if (testDir.at(testDir.length() - 1) != '/') testDir += "/";
		mFileNameEdit->setText(fileName.replace("\\", "/"));//.replace(testDir, ""));
	}
}
