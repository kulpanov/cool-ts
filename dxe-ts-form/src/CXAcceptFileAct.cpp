#include "CXAcceptFileAct.h"

#include <QLabel>
#include <QToolButton>
#include <QFileDialog>

#include "CXSettings.h"
CXAcceptFileAct::CXAcceptFileAct(QWidget* parent) : CXBaseAct(parent)
{
	mType = E_AcceptFileAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

//	centralLayout->addWidget(new QLabel(tr("id"), mContainer));
//
//	mIDEdit = new QLineEdit(mContainer);
//	mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
//	centralLayout->addWidget(mIDEdit);

  centralLayout->addWidget(new QLabel(tr("на стороне"), mContainer));

  mDirectionBox = new QComboBox(mContainer);
  mDirectionBox->addItem(tr("Исходящего"));
  mDirectionBox->addItem(tr("Входящего"));
  centralLayout->addWidget(mDirectionBox);
  centralLayout->addWidget(new QLabel(tr("вызова"), mContainer));

	centralLayout->addWidget(new QLabel(tr("в файл"), mContainer));

	mFileNameEdit = new QLineEdit(mContainer);
	//mFileNameEdit->setReadOnly(true);
	centralLayout->addWidget(mFileNameEdit);

	QToolButton* openButton = new QToolButton(mContainer);
	openButton->setText("...");
	centralLayout->addWidget(openButton);

	connect(openButton, SIGNAL(clicked()), this, SLOT(onOpenFile()));

	//centralLayout->addStretch();
}

CXAcceptFileAct::~CXAcceptFileAct()
{

}

void CXAcceptFileAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
  //mIDEdit->setText(aElement.attribute("id"));
	mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
	mFileNameEdit->setText(aElement.attribute("filename"));
}

QDomElement CXAcceptFileAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("listen"));
	data << SXXMLData("descr", tr("Записать в файл ") + mFileNameEdit->text());
	//data << SXXMLData("id", mIDEdit->text());
	data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));
	data << SXXMLData("type", tr("file_out"));
	data << SXXMLData("filename", mFileNameEdit->text());

	return CXBaseAct::getXMLElement(aDomDocument, data);
}

void CXAcceptFileAct::onOpenFile()
{
  QString testDir = CXSettings::valueStat(E_WavPath).toString().replace("\\", "/");
	QString fileName = QFileDialog::getSaveFileName(NULL, "", testDir, "WAV (*.wav);;RAW (*.raw)");

	if (!fileName.isEmpty())
	{
		//QString testDir = CXSettings::valueStat(E_DirPath).toString().replace("\\", "/");
		if (testDir.at(testDir.length() - 1) != '/') testDir += "/";
		mFileNameEdit->setText(fileName.replace("\\", "/"));//.replace(testDir, ""));
	}
}

CXAcceptFileStopAct::CXAcceptFileStopAct(QWidget* parent) : CXBaseAct(parent)
{
  mType = E_StopAcceptFileAct;
  mActTypeBox->blockSignals(true);
  mActTypeBox->setCurrentIndex(mType);
  mActTypeBox->blockSignals(false);

  QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

  centralLayout->addWidget(new QLabel(tr("на стороне"), mContainer));

  mDirectionBox = new QComboBox(mContainer);
  mDirectionBox->addItem(tr("Исходящего"));
  mDirectionBox->addItem(tr("Входящего"));
  centralLayout->addWidget(mDirectionBox);
  centralLayout->addWidget(new QLabel(tr("вызова"), mContainer));

  centralLayout->addStretch();
}

CXAcceptFileStopAct::~CXAcceptFileStopAct()
{

}

void CXAcceptFileStopAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
  //mIDEdit->setText(aElement.attribute("id"));
  mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
}

QDomElement CXAcceptFileStopAct::getXMLElement(QDomDocument& aDomDocument)
{
  QList <SXXMLData> data;

  data << SXXMLData("name", tr("stopAccept"));
  data << SXXMLData("descr", tr("Остановить запись"));
  data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));

  return CXBaseAct::getXMLElement(aDomDocument, data);
}

