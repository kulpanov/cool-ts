#ifndef CXBaseAct_H
#define CXBaseAct_H

#include "AXAbstractAct.h"

#include "ui_CXActForm.h"

/*!
	Структура атрибута акта для генерации его XML-представления.
*/
struct SXXMLData
{
	SXXMLData(){}
	SXXMLData(const QString& aName, const QString& aValue)
	{
		mName = aName;
		mValue = aValue;
	}

	//! Название xml-атрибута
	QString mName;
	//! Значение xml-атрибута
	QString mValue;
};

/*!
	Базовый класс для актов с выбором типа.
	С реализованным базовым функционалом и общими графическими элементами.
*/
class CXBaseAct : public AXAbstractAct
{
	Q_OBJECT

public:
	CXBaseAct(QWidget* parent = 0, bool _disabled = false);
	~CXBaseAct();
protected:
	int actNumber;
public:
	virtual void setCurrentNumber(int _n){
	  ui.nAct->setText(QString().fromUtf8("Акт %1").arg(_n));
	  actNumber = _n;
	}
protected:
    /*!
        Защищенная функция получения XML-объекта акта по списку атрибутов.

        \param aData Список атрибутов.
        \return Объект QDomElement с заполненными атрибутами из списка aData.
    */
 QDomElement getXMLElement(QDomDocument& aDomDocument, QList <SXXMLData>& aData);

protected:
	QWidget* mContainer;
	QComboBox* mActTypeBox;
  QLineEdit* mIDEdit;
protected:
  virtual void load(const QDomElement& aElement);
private slots:
	/*!
		Слот на изменение типа акта.

		\param aType Новый тип акта.
	*/
	void onTypeChange(int aType);

	/*!
		Слот удаления акта.
	*/
	void onRemove();
	
	/*!
		Слот копирования акта.
	*/
	void onCopy();

	void onIdFinished();
private:
	Ui::CXActForm ui;
};

#endif // CXBaseAct_H
