#ifndef CXSTOPGENERATORACT_H
#define CXSTOPGENERATORACT_H

#include "CXBaseAct.h"

#include <QLineEdit>
#include <QSpinBox>

/*!
	Класс акта "Отключить генератор".
*/
class CXStopGeneratorAct : public CXBaseAct
{
public:
	CXStopGeneratorAct(QWidget* parent = 0);
	~CXStopGeneratorAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
	//QLineEdit* mIDEdit;
	QComboBox* mDirectionBox;
};

#endif // CXSTOPGENERATORACT_H
