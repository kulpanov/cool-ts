#include "CXEditWindow.h"

#include <QFile>
#include <QDomDocument>
#include <QTextStream>
#include <QTextCodec>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>

#include "debug.h"

#include "CXSyntaxHighlighter.h"
#include "CXActsManager.h"
#include "CXSettings.h"

CXEditWindow::CXEditWindow(QWidget* parent) : QWidget(parent)
{
	ui.setupUi(this);

	new CXSyntaxHighlighter(ui.mTextEdit);

	connect(ui.mCheckButton,		SIGNAL(clicked()), this, SLOT(onCheckXML()));
	connect(ui.mSaveButton,			SIGNAL(clicked()), this, SLOT(onSave()));
	connect(ui.mChangeViewButton,	SIGNAL(clicked()), this, SLOT(onChangeView()));
  connect(ui.mStackWidget, SIGNAL(currentChanged(int)), this, SLOT(onCurrentChanged(int)));

  void onCurrentChanged();

	mActsManager = new CXActsManager();
	ui.mScriptTree->setAlternatingRowColors(true);
	ui.mScriptTree->setSelectionMode(QAbstractItemView::NoSelection);

	QPalette p = ui.mScriptTree->palette();
	p.setColor(QPalette::AlternateBase, QColor("#DDDDDD"));
	ui.mScriptTree->setPalette(p);

	mActsManager->setView(ui.mScriptTree);
}

CXEditWindow::~CXEditWindow()
{
	delete mActsManager;
}

class Stack_Busy{
  QWidget* master;
public:
  Stack_Busy(QWidget* _master):master(_master){
    master->setWindowTitle(QString().fromUtf8("Подождите, обрабатываю .... "));
    master->setCursor(QCursor(Qt::WaitCursor));
  }
  ~Stack_Busy(){
    master->setCursor(QCursor(Qt::ArrowCursor));
    master->setWindowTitle(QString().fromUtf8("Редактор сценариев"));
  }
};

void CXEditWindow::setEditFile(const QString& aFileName)
{
  Stack_Busy sbusy(this);
	mFileName = aFileName;

	if (QFile::exists(mFileName))
	{
		QFile file(mFileName);
		file.open(QIODevice::ReadOnly);

		QTextStream in(&file);
		in.setCodec(QTextCodec::codecForName("UTF-8"));

		ui.mTextEdit->setPlainText(in.readAll());

		file.close();
	}

	mActsManager->setEditFile(mFileName);

	ui.mStackWidget->setCurrentIndex(CXSettings::valueStat(E_DefaultEditView).toInt());
}

void CXEditWindow::onCheckXML()
{
	//onSave();
	QDomDocument document;
	QFile file(mFileName);

	QString errorMessage;
	int errorLine, errorColumn;

	if (!document.setContent(&file, &errorMessage, &errorLine, &errorColumn))
	{
		QMessageBox::critical(this, "", tr("Ошибка в %1 строке, %2 столбце:\n%3").arg(errorLine).arg(errorColumn).arg(errorMessage));
	}
}

void CXEditWindow::onSave()
{
	if (QFileInfo(mFileName).isDir())
	{
		QString fileName = QFileDialog::getSaveFileName(this, "", mFileName, "TST (*.tst)");
		if (fileName.isEmpty()) return;

		mFileName = fileName;
	}
	QFile file(mFileName);
	file.open(QIODevice::WriteOnly);

	QTextStream out(&file);
	out.setCodec(QTextCodec::codecForName("UTF-8"));

	if(ui.mStackWidget->currentIndex() == 0){
	  //text view
	  out << ui.mTextEdit->toPlainText();
	  file.close();
//	  mActsManager->setEditFile(mFileName);
	}else{
	  //graphic view
	  QString text = mActsManager->serializeToXML();
	  out << text;
	  ui.mTextEdit->setPlainText(text);
    file.close();
	}
//	onCheckXML();
}

void CXEditWindow::onChangeView()
{
//  Stack_Busy sbusy(this);
  setWindowTitle(QString().fromUtf8("Подождите, обрабатываю .... "));
  setCursor(QCursor(Qt::WaitCursor));

  onSave();
  onCheckXML();
	if (ui.mStackWidget->currentIndex() == 0){
    mActsManager->setEditFile(mFileName);
	  ui.mStackWidget->setCurrentIndex(1);
	}else
	{
		//ui.mTextEdit->setPlainText(mActsManager->serializeToXML());
		ui.mStackWidget->setCurrentIndex(0);
	}
}

void CXEditWindow::onCurrentChanged(int _i) {
  //
  setCursor(QCursor(Qt::ArrowCursor));
  setWindowTitle(QString().fromUtf8("Редактор сценариев"));

}
