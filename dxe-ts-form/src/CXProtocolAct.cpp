#include "CXProtocolAct.h"

CXProtocolAct::CXProtocolAct(QWidget *parent) : CXBaseAct(parent, true)
{
	//mType = E_ProtocolAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

	mGenerateCheck = new QCheckBox(mContainer);
	mGenerateCheck->setText(tr("Генерировать"));
	mGenerateCheck->setChecked(true);
	centralLayout->addWidget(mGenerateCheck);

	//centralLayout->addStretch();
}

CXProtocolAct::~CXProtocolAct()
{

}

void CXProtocolAct::load(const QDomElement& aElement)
{
	mGenerateCheck->setChecked((aElement.attribute("turn") == "on")? true : false);
}

QDomElement CXProtocolAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("log"));
	data << SXXMLData("descr", tr("Протокол"));
	data << SXXMLData("turn", (mGenerateCheck->isChecked())? "on" : "off");

	return CXBaseAct::getXMLElement(aDomDocument, data);
}
