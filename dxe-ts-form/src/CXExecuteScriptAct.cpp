
#include <QLabel>
#include <QToolButton>
#include <QFileDialog>

#include "CXExecuteScriptAct.h"
#include "CXSettings.h"

CXExecuteScriptAct::CXExecuteScriptAct(QWidget* parent) : CXBaseAct(parent, true)
{
	mType = E_ExecuteScriptAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

	mIDEdit->setEnabled(false);

	centralLayout->addWidget(new QLabel(tr("сценарий"), mContainer));

	mFileNameEdit = new QLineEdit(mContainer);
	//mFileNameEdit->setReadOnly(true);
	centralLayout->addWidget(mFileNameEdit);

	QToolButton* openButton = new QToolButton(mContainer);
	openButton->setText("...");
	centralLayout->addWidget(openButton);

	connect(openButton, SIGNAL(clicked()), this, SLOT(onOpenFile()));

	centralLayout->addWidget(new QLabel(tr("для соединения"),mContainer));
	mConnectEdit = new QLineEdit(mContainer);
	mConnectEdit->setValidator(
    new QRegExpValidator(QRegExp("^-?\\d+"), mConnectEdit));
	mConnectEdit->setMaximumWidth(30);
  centralLayout->addWidget(mConnectEdit);

  centralLayout->addWidget(new QLabel(tr(",повторить"),mContainer));
  mLoopEdit = new QLineEdit(mContainer);
  mLoopEdit->setValidator(
    new QRegExpValidator(QRegExp("^-?\\d+"), mConnectEdit));
  mLoopEdit->setText("0");
  mLoopEdit->setMaximumWidth(30);
  centralLayout->addWidget(mLoopEdit);
  centralLayout->addWidget(new QLabel(tr("раз, "),mContainer));

//  centralLayout->addWidget(new QLabel(tr("параллельно"), mContainer));
//  centralLayout->addWidget(mParallel = new QCheckBox(this));

	//centralLayout->addStretch();
}

CXExecuteScriptAct::~CXExecuteScriptAct()
{

}

void CXExecuteScriptAct::load(const QDomElement& aElement)
{
	mFileNameEdit->setText(aElement.attribute("sname"));
	mConnectEdit->setText(aElement.attribute("conn_id"));
	mLoopEdit->setText(aElement.attribute("loop"));
  //mLoopEdit->setText(aElement.attribute("parallel"));
}

QDomElement CXExecuteScriptAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("call_script"));
	data << SXXMLData("descr", tr("Вызвать сценарий"));
	data << SXXMLData("sname", mFileNameEdit->text());
	data << SXXMLData("conn_id", mConnectEdit->text());
	data << SXXMLData("loop", mLoopEdit->text());
  //data << SXXMLData("parallel", mParallel->checkState()==0? "0": "1");
	return CXBaseAct::getXMLElement(aDomDocument, data);
}

void CXExecuteScriptAct::onOpenFile()
{
  QString testDir = CXSettings::valueStat(E_DirPath).toString();
	QString fileName = QFileDialog::getOpenFileName(0, "", testDir, "TST (*.tst)");

	if (!fileName.isEmpty())
	{
		mFileNameEdit->setText(fileName);
	}
}
