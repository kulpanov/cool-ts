#ifndef CXDIRECTORYWIDGET_H
#define CXDIRECTORYWIDGET_H

#include <QWidget>
#include <QLineEdit>

/*!
	Виджет установки директории.
*/
class CXDirectoryWidget : public QWidget
{
	Q_OBJECT

public:
	CXDirectoryWidget(QWidget* parent = 0);
	~CXDirectoryWidget();

	/*!
		Функция получения текущего установленного пути.

		\return Текущий установленный путь.
	*/
	QString path();

	/*!
		Функция установки текущего пути.

		\param aPath Устанавливаемый путь.
	*/
	void setPath(const QString& aPath);

private slots:
	/*!
		Слот показа диалога выбора каталога.
	*/
	void onShowDialog();

	/*!
		Слот окончания редактирования строки с путем.
	*/
	void onEditFinish();

private:
	QLineEdit* mPathEdit;

	QString mPath;
};

#endif // CXDIRECTORYWIDGET_H
