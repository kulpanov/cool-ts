#include "CXBaseAct.h"

#include "CXActsManager.h"
static const QString str_auto = QString().fromUtf8("авто");

CXBaseAct::CXBaseAct(QWidget* parent, bool _disabled) : AXAbstractAct(parent)
{
	ui.setupUi(this);
  ui.mIDEdit->setVisible(! _disabled);
  ui.label->setVisible(! _disabled);

	mActTypeBox = ui.mActTypeBox;
	mActTypeBox->setMaxVisibleItems(20);
	mActTypeBox->addItem(tr("Установить соединение"));
	mActTypeBox->addItem(tr("Соединить c абонентом"));
	mActTypeBox->addItem(tr("Подключить генератор"));
	mActTypeBox->addItem(tr("Измерить коэф. передачи"));
	mActTypeBox->addItem(tr("Измерить уровень(rms)"));
	mActTypeBox->addItem(tr("Отключить генератор"));
	mActTypeBox->addItem(tr("Передать файл"));
	mActTypeBox->addItem(tr("Прослушать"));
  mActTypeBox->addItem(tr("Остановить прослушку"));
	mActTypeBox->addItem(tr("Разъединить всех"));
	mActTypeBox->addItem(tr("Разъединить"));
	mActTypeBox->addItem(tr("Пауза"));
//	mActTypeBox->addItem(tr("Протокол"));
	mActTypeBox->addItem(tr("Выполнить сценарий"));
	mActTypeBox->addItem(tr("Начать запись в файл"));
  mActTypeBox->addItem(tr("Остановить запись"));
  mActTypeBox->addItem(tr("Параметры DTMF"));
  mActTypeBox->addItem(tr("Параметры RTP"));
  mActTypeBox->addItem(tr("Построить АЧХ для RMS"));
  mActTypeBox->addItem(tr("Построить АЧХ для КоэфПередачи"));

	mContainer = ui.mContainer;
	//mContainer->addStretch();
	mIDEdit = ui.mIDEdit;
  mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
	connect(mActTypeBox,		SIGNAL(currentIndexChanged(int)), this, SLOT(onTypeChange(int)));
	connect(ui.mRemoveButton,	SIGNAL(clicked()), this, SLOT(onRemove()));
	connect(ui.mCreateButton,	SIGNAL(clicked()), this, SLOT(onCopy()));
	connect(ui.mIDEdit, SIGNAL(editingFinished()), this, SLOT(onIdFinished()));
  mIDEdit->setMinimumWidth(35);
	mIDEdit->setPlaceholderText(str_auto);
	//
	actNumber = 0;
}

CXBaseAct::~CXBaseAct()
{

}

void CXBaseAct::load(const QDomElement& aElement){

  QString id_str = aElement.attribute("id", "0");
  if(id_str.toInt() == 0)
    id_str = "";
  mIDEdit->setText(id_str);
}

QDomElement CXBaseAct::getXMLElement(QDomDocument& aDomDocument, QList <SXXMLData>& aData)
{
  QString str_id =  mIDEdit->text();
  if( str_id.toInt() == 0){
    str_id = "";
  }
  aData << SXXMLData("id", str_id);
  aData << SXXMLData("actNo", QString("%0").arg(actNumber));

	QDomElement element = aDomDocument.createElement("action");

	for (int i = 0; i < aData.count(); ++i)
	{
		const SXXMLData& curData = aData.at(i);

		element.setAttribute(curData.mName, curData.mValue);
	}

	return element;
}

void CXBaseAct::onTypeChange(int aType)
{
	if (AXAbstractAct::mActsManager != NULL)
	{
		AXAbstractAct::mActsManager->replaceType(this, aType);
	}
}

void CXBaseAct::onRemove()
{
	if (AXAbstractAct::mActsManager != NULL)
	{
		AXAbstractAct::mActsManager->remove(this);
	}
}


//#include "CXActConnectHelper.h"

void CXBaseAct::onCopy()
{
//  CXActConnectHelper helper(this);
//  helper.exec();
	if (AXAbstractAct::mActsManager != NULL)
	{
		AXAbstractAct::mActsManager->copy(this);
	}
}


void CXBaseAct::onIdFinished(){
  QString s = mIDEdit->text();
  if(s.size() == 0 || s.toInt() == 0)
    mIDEdit->setText("");
}

