﻿#include "CXAdapterWidget.h"


CXAdapterWidget::CXAdapterWidget(const QString& _humanReadableName
    , const QNetworkAddressEntry& addr, QWidget* parent) : QWidget(parent)
{
  ui.setupUi(this);

  ui.mNameLabel->setText(_humanReadableName);

	//if (!aInterface.addressEntries().isEmpty())
	{
		//QNetworkAddressEntry addr = aInterface.addressEntries().last();

		mIP = addr.ip().toString();
		mMask = addr.netmask().toString();

		ui.mIPLabel->setText(QString("IP: %1").arg(mIP));
		ui.mMaskLabel->setText(QString("Mask: %1").arg(mMask));
	}
}

CXAdapterWidget::~CXAdapterWidget()
{

}
