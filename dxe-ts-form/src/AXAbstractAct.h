#ifndef AXAbstractAct_H
#define AXAbstractAct_H

#include <QFrame>
#include <QDomElement>

class CXActsManager;

/*!
	Типы актов.
*/
enum eActTypes
{
	E_NoneAct = -1,
	E_BeginConnectionAct = 0,
	E_BeginConnectionHalfAct,
	E_GeneratorAct,
	E_MeasureAct,
	E_AbsMeasureAct,
	E_StopGeneratorAct,
	E_TransmitFileAct,
	E_ListenAct,
	E_ListenStopAct,
	E_DisconnectAllAct,
	E_DisconnectAct,
	E_PauseAct,
//	E_ProtocolAct,
	E_ExecuteScriptAct,
	E_AcceptFileAct,
	E_StopAcceptFileAct,
	E_DTMFSetting,
  E_RTPSetting,
  E_PlotRMS,
  E_PlotRatio
};

/*!
	Абстрактный класс для актов.
*/
class AXAbstractAct : public QFrame
{
public:
	AXAbstractAct(QWidget* parent = 0);
	~AXAbstractAct();

	/*!
		Функция получения типа акта.
	*/
	eActTypes type();

	/*!
		Функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement) = 0;

	virtual void setCurrentNumber(int _n){
	  Q_UNUSED(_n);
	}

	/*!
		Функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument) = 0;

	/*!
		Статическая функция установки менеджера.

		\param aActsManager указатель на объект менеджера.
		\note Необходимо для оповещения менеджера о событиях удаления объекта и копирования.
	*/
	static void setManager(CXActsManager* aActsManager);

protected:
	static CXActsManager* mActsManager;

	eActTypes mType;
};

#endif // AXAbstractAct_H
