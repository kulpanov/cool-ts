#include "CXActsManager.h"

#include <QFile>
#include <QTextStream>
#include <QFileInfo>

#include "CXScriptAct.h"
#include "CXBeginConnectionAct.h"
#include "CXBeginConnectionHalfAct.h"
#include "CXGeneratorAct.h"
#include "CXMeasureAct.h"
#include "CXAbsMeasureAct.h"
#include "CXStopGeneratorAct.h"
#include "CXTransmitFileAct.h"
#include "CXListenAct.h"
#include "CXDisconnectAllAct.h"
#include "CXDisconnectAct.h"
#include "CXPauseAct.h"
#include "CXProtocolAct.h"
#include "CXEndScriptAct.h"
#include "CXExecuteScriptAct.h"
#include "CXAcceptFileAct.h"

CXActsManager::CXActsManager()
{
	mTreeWidget = NULL;

	AXAbstractAct::setManager(this);
}

CXActsManager::~CXActsManager()
{

}

void CXActsManager::setView(QTreeWidget* aTreeWidget)
{
	mTreeWidget = aTreeWidget;
/*
	if (mTreeWidget != NULL)
	{
		QList <eActTypes> types;
		types << E_BeginConnectionAct << E_GeneratorAct << E_MeasureAct << E_StopGeneratorAct << E_TransmitFileAct << E_ListenAct << E_DisconnectAllAct << E_DisconnectAct << E_PauseAct << E_ProtocolAct;

		QTreeWidgetItem* item = NULL;

		item = new QTreeWidgetItem;
		mTreeWidget->addTopLevelItem(item);
		mTreeWidget->setItemWidget(item, 0, new CXScriptAct());

		for (int i = 0; i < types.count(); ++i)
		{
			item = new QTreeWidgetItem;
			mTreeWidget->addTopLevelItem(item);
			mTreeWidget->setItemWidget(item, 0, getAct(types.at(i)));
		}

		item = new QTreeWidgetItem;
		mTreeWidget->addTopLevelItem(item);
		mTreeWidget->setItemWidget(item, 0, new CXEndScriptAct());
	}
 */
}

QList <AXAbstractAct*> CXActsManager::getActs()
{
	QList <AXAbstractAct*> res;

	if (mTreeWidget != NULL)
	{
		int count = mTreeWidget->topLevelItemCount();
		QTreeWidgetItem* item = NULL;
		AXAbstractAct* act = NULL;

		for (int i = 0; i < count; ++i)
		{
			item = mTreeWidget->topLevelItem(i);
			act = dynamic_cast<AXAbstractAct*>(mTreeWidget->itemWidget(item, 0));

			if (act != NULL)
			{
				res.append(act);
			}
		}
	}

	return res;
}

void CXActsManager::setEditFile(const QString& aFileName)
{
	if (mTreeWidget != NULL)
	{
		mTreeWidget->clear();

		if (QFileInfo(aFileName).isDir())
		{
			QTreeWidgetItem* item = new QTreeWidgetItem;
			mTreeWidget->addTopLevelItem(item);

			CXScriptAct* scriptAct = new CXScriptAct(QObject::tr("Сценарий"));
			scriptAct->setEditFile(aFileName);
			mTreeWidget->setItemWidget(item, 0, scriptAct);

			item = new QTreeWidgetItem;
			mTreeWidget->addTopLevelItem(item);
			mTreeWidget->setItemWidget(item, 0, new CXEndScriptAct());

			return;
		}

		QFile xmlFile(aFileName);
		xmlFile.open(QIODevice::ReadOnly);

		QDomDocument domDocument;
		domDocument.setContent(&xmlFile);

		QDomElement rootElement = domDocument.documentElement();

		if (rootElement.tagName() == "script")
		{
			QTreeWidgetItem* item = new QTreeWidgetItem;
			mTreeWidget->addTopLevelItem(item);

			CXScriptAct* scriptAct = new CXScriptAct(rootElement.attribute("descr"));
			scriptAct->setEditFile(aFileName);
			mTreeWidget->setItemWidget(item, 0, scriptAct);

			QMap <QString, eActTypes> actTypes;

			actTypes.insert("connect", E_BeginConnectionAct);
			actTypes.insert("connect_half", E_BeginConnectionHalfAct);
			actTypes.insert("sin", E_GeneratorAct);
			actTypes.insert("silence", E_GeneratorAct);
			actTypes.insert("test_koef", E_MeasureAct);
			actTypes.insert("test_abs", E_AbsMeasureAct);
			actTypes.insert("stopPlay", E_StopGeneratorAct);
			actTypes.insert("file_in", E_TransmitFileAct);
			actTypes.insert("sound_out", E_ListenAct);
			actTypes.insert("stopListen", E_ListenStopAct);
			actTypes.insert("disconnect_all", E_DisconnectAllAct);
			actTypes.insert("disconnect", E_DisconnectAct);
			actTypes.insert("pause", E_PauseAct);
//			actTypes.insert("log", E_ProtocolAct);
			actTypes.insert("call_script", E_ExecuteScriptAct);
			actTypes.insert("file_out", E_AcceptFileAct);
			actTypes.insert("stopAccept", E_StopAcceptFileAct);
			actTypes.insert("dtmf", E_DTMFSetting);
      actTypes.insert("plot_rms", E_PlotRMS);
      actTypes.insert("plot_ratio", E_PlotRatio);
      actTypes.insert("rtp_params", E_RTPSetting);

			eActTypes type = E_NoneAct;
			AXAbstractAct* curAct = NULL;

			QDomElement curActElement = rootElement.firstChildElement("action");
			while (curActElement.isElement())
			{
				QString name = curActElement.attribute("name");
				if (name == "play" || name == "listen") name = curActElement.attribute("type");

				if (!actTypes.contains(name)) type = E_NoneAct;
				else type = actTypes.value(name);
				curAct = getAct(type);

				if (curAct != NULL)
				{
					curAct->load(curActElement);

					item = new QTreeWidgetItem;
					mTreeWidget->addTopLevelItem(item);
					mTreeWidget->setItemWidget(item, 0, curAct);
				}

				curActElement = curActElement.nextSiblingElement("action");
			}

			item = new QTreeWidgetItem;
			mTreeWidget->addTopLevelItem(item);
			mTreeWidget->setItemWidget(item, 0, new CXEndScriptAct());

			recalcActs();
		}
	}
}

AXAbstractAct* CXActsManager::getAct(int aType)
{
	switch (aType)
	{
		case E_BeginConnectionAct:	return new CXBeginConnectionAct();
		case E_BeginConnectionHalfAct:  return new CXBeginConnectionHalfAct();
		case E_GeneratorAct:		return new CXGeneratorAct();
		case E_MeasureAct:			return new CXMeasureAct();
		case E_AbsMeasureAct:      return new CXAbsMeasureAct();
		case E_StopGeneratorAct:	return new CXStopGeneratorAct();
		case E_TransmitFileAct:		return new CXTransmitFileAct();
		case E_ListenAct:			return new CXListenAct();
		case E_ListenStopAct:     return new CXListenStopAct();
		case E_DisconnectAllAct:	return new CXDisconnectAllAct();
		case E_DisconnectAct:		return new CXDisconnectAct();
		case E_PauseAct:			return new CXPauseAct();
//		case E_ProtocolAct:			return new CXProtocolAct();
		case E_ExecuteScriptAct:	return new CXExecuteScriptAct();
		case E_AcceptFileAct:		return new CXAcceptFileAct();
	  case E_StopAcceptFileAct:   return new CXAcceptFileStopAct();
	  case E_DTMFSetting: return new CXAct_DTMF();
    case E_RTPSetting: return new CXAct_RTP();
    case E_PlotRMS: return new CXAct_Plot(E_PlotRMS);
    case E_PlotRatio: return new CXAct_Plot(E_PlotRatio);
	}

	return NULL;
}

QTreeWidgetItem* CXActsManager::findItem(AXAbstractAct* aAct)
{
	if (mTreeWidget != NULL)
	{
		int count = mTreeWidget->topLevelItemCount();
		QTreeWidgetItem* item = NULL;

		for (int i = 0; i < count; ++i)
		{
			item = mTreeWidget->topLevelItem(i);

			if (mTreeWidget->itemWidget(item, 0) == aAct) return item;
		}
	}

	return NULL;
}

int CXActsManager::findIndex(AXAbstractAct* aAct)
{
	if (mTreeWidget != NULL)
	{
		int count = mTreeWidget->topLevelItemCount();
		QTreeWidgetItem* item = NULL;

		for (int i = 0; i < count; ++i)
		{
			item = mTreeWidget->topLevelItem(i);

			if (mTreeWidget->itemWidget(item, 0) == aAct) return i;
		}
	}

	return -1;
}

void CXActsManager::replaceType(AXAbstractAct* aAct, int aType)
{
	if (mTreeWidget != NULL)
	{
		QTreeWidgetItem* item = findItem(aAct);
		if (item != NULL) mTreeWidget->setItemWidget(item, 0, getAct(aType));
    recalcActs();
	}
}

void CXActsManager::remove(AXAbstractAct* aAct)
{
	if (mTreeWidget != NULL)
	{
		QTreeWidgetItem* item = findItem(aAct);
		delete item;
		recalcActs();
	}
}

void CXActsManager::copy(AXAbstractAct* aAct)
{
	if (mTreeWidget != NULL)
	{
		int index = findIndex(aAct);

		AXAbstractAct* act = getAct(aAct->type());

		if (act == NULL) return;

		//act->setCurrentNumber(index + 1);
		QTreeWidgetItem* item = new QTreeWidgetItem;
		mTreeWidget->insertTopLevelItem(index + 1, item);
		mTreeWidget->setItemWidget(item, 0, act);
///
		item = new QTreeWidgetItem;
		mTreeWidget->addTopLevelItem(item);
		delete item;
		recalcActs();
	}
}

void CXActsManager::appendAct(int type)
{
	if (mTreeWidget != NULL)
	{
		AXAbstractAct* act = getAct(type);

		if (act == NULL) return;

		QTreeWidgetItem* item = new QTreeWidgetItem;
		mTreeWidget->insertTopLevelItem(1, item);
		mTreeWidget->setItemWidget(item, 0, act);
		///
		item = new QTreeWidgetItem;
		mTreeWidget->addTopLevelItem(item);
		delete item;
		recalcActs();
	}
}

void CXActsManager::recalcActs(){
  if (mTreeWidget != NULL)
  {
    int count = mTreeWidget->topLevelItemCount();
    QTreeWidgetItem* item = NULL;

    int actNumber = 0;
    for (int i = 0; i < count; ++i)
    {
      item = mTreeWidget->topLevelItem(i);

      AXAbstractAct* aAct =
          dynamic_cast<AXAbstractAct*>(mTreeWidget->itemWidget(item, 0));
      aAct->setCurrentNumber(actNumber++);
    }
  }
}

QString CXActsManager::serializeToXML()
{
	QString res = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

	if (mTreeWidget != NULL)
	{
		QDomDocument domDocument;

		int count = mTreeWidget->topLevelItemCount();
		QTreeWidgetItem* item = NULL;
		AXAbstractAct* act = NULL;

		act = dynamic_cast<AXAbstractAct*>(mTreeWidget->itemWidget(mTreeWidget->topLevelItem(0), 0));

		if (act == NULL) return res;

		QDomElement rootElement = act->getXMLElement(domDocument);

		for (int i = 1; i < count - 1; ++i)
		{
			item = mTreeWidget->topLevelItem(i);
			act = dynamic_cast<AXAbstractAct*>(mTreeWidget->itemWidget(item, 0));

			if (act != NULL)
			{
				rootElement.appendChild(act->getXMLElement(domDocument));
			}
		}

		QTextStream textOut(&res);

		rootElement.save(textOut, 1);
	}

	return res;
}
