#ifndef CXEDITWINDOW_H
#define CXEDITWINDOW_H

#include <QWidget>
#include "ui_CXEditWindow.h"

class CXActsManager;

/*!
	Класс окна с редактированием файла.
*/
class CXEditWindow : public QWidget
{
	Q_OBJECT

public:
	CXEditWindow(QWidget *parent = 0);
	~CXEditWindow();\

	/*!
		Функция установки пути с именем редактируемого файла.

		\param aFileName Путь с именем редактируемого файла
	*/
	void setEditFile(const QString& aFileName);

private slots:
	/*!
		Слот проверки валидности xml
	*/
	void onCheckXML();

	/*!
		Слот сохранения имзмененных данных.
	*/
	void onSave();

	/*!
		Слот изменения редактора.
	*/
	void onChangeView();

	void onCurrentChanged(int _i);
private:
	Ui::CXEditWindow ui;

	QString mFileName;
	CXActsManager* mActsManager;
};

#endif // CXEDITWINDOW_H
