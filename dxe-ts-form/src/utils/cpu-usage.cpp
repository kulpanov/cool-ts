/*
 * cpu-usage.cpp
 *
 *  Created on: 13 янв. 2014 г.
 *      Author: Kulpanov
 */
#include <inttypes.h>
#include <windows.h>

  static FILETIME idleTime, last_idleTime;
  static FILETIME kernelTime, last_kernelTime;
  static FILETIME userTime, last_userTime;


int getCPUUsage() {
  struct CLargeUInt {
    union {
     uint64_t data;
     struct {
       uint32_t low;
       uint32_t high;
     }i32;
    }u;

    CLargeUInt(const FILETIME& _ft){
     u.i32.low = _ft.dwLowDateTime;
     u.i32.high= _ft.dwHighDateTime;
   }
  };

  BOOL res = GetSystemTimes( &idleTime, &kernelTime, &userTime );
  if(! res) return -1;
  int usr = CLargeUInt(userTime).u.data - CLargeUInt(last_userTime).u.data;
  int krn = CLargeUInt(kernelTime).u.data - CLargeUInt(last_kernelTime).u.data;
  int idl = CLargeUInt(idleTime).u.data - CLargeUInt(last_idleTime).u.data;

  last_userTime = userTime;
  last_kernelTime = kernelTime;
  last_idleTime = idleTime;

  int sys = krn + usr;

  int cpu = int( (sys - idl) *100 / sys );

  return cpu;
}



