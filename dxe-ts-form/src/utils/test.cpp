/*
 * test.cpp
 *
 *  Created on: 31.08.2012
 *      Author: Kulpanov
 */
#include <iostream>
#include <fstream>
#include <exception>

#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>

#include "debug.h"
#include "mainwindow.h"
#include "CXSettings.h"

using std::exception;
extern QList<QString> wstr_list;

#ifdef TEST_X
#include <boost/random.hpp>
#include <boost/thread.hpp>

std::string createTestScript(int _indx){
  //позвонить, отранслировать файл
  const char* dxe_test_xml={
      "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
      "<script name=\"script%0\" descr=\"Сценарий%0\">\n"
      "<action name=\"connect\" target_ip=\"192.168.233.1\" target=\"00%0000%0000%0\" out=\"00%0\" descr=\"Установить соединение\" id=\"%0\" src=\"00%0\" gw=\"00%0\"/>\n"
      "<action dir=\"0\" freq=\"440\" ampl=\"-4\" descr=\"Подключить генератор\" type=\"sin\" id=\"%0\" name=\"play\"/>\n"
      "<action dir=\"1\" freq=\"880\" ampl=\"-2\" descr=\"Подключить генератор\" type=\"sin\" id=\"%0\" name=\"play\"/>\n"
      "<action descr=\"Пауза\" id=\"\" name=\"pause\" pause=\"2500\"/>\n"
      "<action hi=\"1\" check=\"0\" mtime=\"100\" dir=\"0\" descr=\"Измерить коэф передачи\" id=\"%0\" name=\"test_koef\" low=\"-1\"/>\n"
      "<action hi=\"1\" check=\"0\" mtime=\"100\" dir=\"1\" descr=\"Измерить коэф передачи\" id=\"%0\" name=\"test_koef\" low=\"-1\"/>\n"
      "<action dir=\"0\" descr=\"Отключить генератор\" id=\"%0\" name=\"stopPlay\"/>\n"
      "<action name=\"disconnect\" descr=\"Прервать связь\" id=\"%0\" />\n"
      "</script>"};
  const char* dxe_test_fname = {"dxe-tests/test6/%0.tst"};

  std::ofstream f;
  std::string fname = QString(dxe_test_fname).arg(_indx).toStdString();
  f.open(fname.c_str());

  f << QString(dxe_test_xml).arg(_indx).toAscii().begin();
  f.close();
  return fname;
}

extern bool forgetExecuteNewTest;

void executeFile(MainWindow* mf, int ind){
  mf->executeFile(QString().fromStdString(createTestScript(ind)));
}

void testX_par_real(MainWindow* mf) {
  //создание скрипта, исполнение его
  boost::random::mt19937 gen;
  boost::random::uniform_int_distribution<> dist(0, 30);
  for(int i =0; i<100; ++i)
  {
    for(int j = 1; j<10; ++j){
      executeFile(mf, j);
      int pause = dist(gen)*100;
      std::cout << "pause="<<pause<<std::endl;
      boost::this_thread::sleep(boost::posix_time::milliseconds(pause));
      if(forgetExecuteNewTest){
        break;
      }
    }
    boost::this_thread::sleep(boost::posix_time::seconds(10));
    if(forgetExecuteNewTest) break;
  }
}

void DoTestStep_xPar(MainWindow* mf){
  createTestScript(1);
  createTestScript(2);
  createTestScript(3);
  createTestScript(4);
  createTestScript(5);
  createTestScript(6);
  createTestScript(7);
  createTestScript(8);
  createTestScript(9);
  boost::thread(testX_par_real, mf);
}
#endif
void MainWindow::DoTestStep(/*int _ntest*/){
  static int count = 0;
  LOG1(INFO, "DoTestStep: %d\n", ++count);
  int timerMS = 60000;
  {using namespace boost::filesystem;
   try{
     {std::wstring wavpath = CXSettings::valueStat(E_WavPath).toString().toStdWString();
     path p (wavpath);
     for (directory_iterator dir_itr(p); dir_itr != directory_iterator(); ++dir_itr)
       if (is_regular_file(dir_itr->status()))
         if(file_size(dir_itr->path())<80000)
         {
           LOG2(ERRR, "we've some troubles: %s, %d\n", dir_itr->path().filename().c_str(), file_size(dir_itr->path().c_str()));
//           wstr_list.clear();
//           testTimer.stop();
//           return ;
         }
     }
     if(wstr_list.empty()){
       count = 0;
       testTimer.stop();
       return ;
     }
     if( CTestScript::count_executers == 0)
       for(auto sc_it: wstr_list){
         LOG1(INFO, "form:execute test: %s\n", sc_it.toUtf8().begin() );
         executeFile(sc_it);
       }
     else //some test still is running
       timerMS = 1000;//repeat after 1sec

//     {
//     path p ("dxe-tests");
//     for (directory_iterator dir_itr(p); dir_itr != directory_iterator(); ++dir_itr)
//       if (is_regular_file(dir_itr->status())){
//         std::wstring str_path = dir_itr->path().filename().native();
//         LOG1(INFO, "form:execute test: %s\n", str_path.c_str() );
//         executeFile(QString::fromStdWString(str_path));
//     }
//     }
   }catch (const exception& ex)
   {
     std::cout << ex.what() << '\n';
   }

  }//namespace
  //QTimer::singleShot(15000, this, SLOT(DoTestStep()));

  testTimer.start(timerMS);

//
//  switch(_ntest){
//  case 0:{
//#ifdef TEST_X
//    DoTestStep_xPar(this);
//#endif
//  }break;
//  case 1:
//  {
//#ifdef TEST_FORM
//  SMsg_log msg;
//  static int curTest = 0;
//
//   //первый тест просто запуск сценариев
//  switch(curTest){
//    case 0:
//      msg.id = "1";
//      msg.msg = QString::fromUtf8("Сценарий1");
//      msg.fulldate = true;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 1:
//      msg.id = "1.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 601");
//      msg.fulldate = false;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 2:
//      msg.id = "1.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 601");
//      msg.fulldate = false;
//      msg.result = 2;
//      onLogMsg(msg);
//      msg.id = "1.2";
//      msg.msg = QString::fromUtf8("Акт2: Подключить генератор синус 440Гц -6Дб");
//      msg.fulldate = false;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 3:
//      msg.id = "1";
//      msg.msg = QString::fromUtf8("Сценарий1");
//      msg.fulldate = true;
//      msg.result = 2;
//      onLogMsg(msg);
//      msg.id = "1.2";
//      msg.msg = QString::fromUtf8("Акт2: Подключить генератор синус 440Гц -6Дб");
//      msg.fulldate = false;
//      msg.result = 2;
//      onLogMsg(msg);
//    break;
//    case 4:
//      msg.id = "2";
//      msg.msg = QString::fromUtf8("Сценарий2");
//      msg.fulldate = true;
//      msg.result = 1;
//      onLogMsg(msg);
//      msg.id = "3";
//      msg.msg = QString::fromUtf8("Сценарий3");
//      msg.fulldate = true;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 5:
//      msg.id = "2.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 602");
//      msg.fulldate = false;
//      msg.result = 1;
//      onLogMsg(msg);
//      msg.id = "3.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 603");
//      msg.fulldate = false;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 6:
//      msg.id = "2.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 602");
//      msg.fulldate = false;
//      msg.result = 2;
//      onLogMsg(msg);
//      msg.id = "3.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 603");
//      msg.fulldate = false;
//      msg.result = 2;
//      onLogMsg(msg);
//      msg.id = "2.2";
//      msg.msg = QString::fromUtf8("Акт2: Подключить генератор синус 440Гц -6Дб");
//      msg.fulldate = false;
//      msg.result = 1;
//      onLogMsg(msg);
//      msg.id = "3.2";
//      msg.msg = QString::fromUtf8("Акт2: Подключить генератор синус 440Гц -6Дб");
//      msg.fulldate = false;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 7:
//      msg.id = "2";
//      msg.msg = QString::fromUtf8("Сценарий2");
//      msg.fulldate = true;
//      msg.result = 2;
//      onLogMsg(msg);
//      msg.id = "2.2";
//      msg.msg = QString::fromUtf8("Акт2: Подключить генератор синус 440Гц -6Дб");
//      msg.fulldate = false;
//      msg.result = 2;
//      onLogMsg(msg);
//    break;
//    case 8:
//      msg.id = "3";
//      msg.msg = QString::fromUtf8("Сценарий3");
//      msg.fulldate = true;
//      msg.result = 3;
//      onLogMsg(msg);
//      msg.id = "3.2";
//      msg.msg = QString::fromUtf8("Акт2: Подключить генератор синус 440Гц -6Дб");
//      msg.fulldate = false;
//      msg.result = 3;
//      onLogMsg(msg);
//    break;
//    case 9:
//      msg.id = "4";
//      msg.msg = QString::fromUtf8("Сценарий4");
//      msg.fulldate = true;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 10:
//      msg.id = "4.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 604");
//      msg.fulldate = true;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 11:
//      msg.id = "4.1.1";
//      msg.msg = QString::fromUtf8("Сценарий4_1");
//      msg.fulldate = true;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 12:
//      msg.id = "4.1.1.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 604");
//      msg.fulldate = true;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 13:
//      msg.id = "4.1.1.1";
//      msg.msg = QString::fromUtf8("Акт1: Установить соединение на номер 604");
//      msg.fulldate = true;
//      msg.result = 2;
//      onLogMsg(msg);
//      msg.id = "4.1.1.2";
//      msg.msg = QString::fromUtf8("Акт2: Подключить генератор синус 440Гц -6Дб");
//      msg.fulldate = true;
//      msg.result = 1;
//      onLogMsg(msg);
//    break;
//    case 14:
//      msg.id = "4";
//      msg.msg = QString::fromUtf8("Сценарий4");
//      msg.fulldate = true;
//      msg.result = 3;
//      onLogMsg(msg);
//      msg.id = "4.1.1";
//      msg.msg = QString::fromUtf8("Сценарий4_1");
//      msg.fulldate = true;
//      msg.result = 3;
//      onLogMsg(msg);
//      msg.id = "4.1.1.2";
//      msg.msg = QString::fromUtf8("Акт2: Подключить генератор синус 440Гц -6Дб");
//      msg.fulldate = true;
//      msg.result = 3;
//      onLogMsg(msg);
//    break;
//  }
//  curTest ++;
//#endif
//  };break;
//  }//case
}

void backup_log(const char* fname)
{using namespace boost::filesystem;//copy log file to logs dir
try{
  path p (fname);
  if(file_size(p)> (5*1024*1024)){
    rename(p, unique_path( path("logs/cool-test-%%%%-%%%%.log")));
    remove(p);
  }
  }catch(...){

  }
}
