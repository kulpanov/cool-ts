#ifndef CXTRANSMITFILEACT_H
#define CXTRANSMITFILEACT_H

#include "CXBaseAct.h"

#include <QLineEdit>
#include <QSpinBox>

/*!
	Класс акта "Передать файл".
*/
class CXTransmitFileAct : public CXBaseAct
{
	Q_OBJECT

public:
	CXTransmitFileAct(QWidget* parent = 0);
	~CXTransmitFileAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private slots:
	void onOpenFile();

private:
	//QLineEdit* mIDEdit;
	QComboBox* mDirectionBox;
	QLineEdit* mFileNameEdit;
};

#endif // CXTRANSMITFILEACT_H
