#ifndef CXABOUTWINDOW_H
#define CXABOUTWINDOW_H

#include <QWidget>
#include "ui_CXAboutWindow.h"

/*!
	Класс формы About (О программе).
*/
class CXAboutWindow : public QDialog
{
public:
	CXAboutWindow(QWidget* parent = 0);
	~CXAboutWindow();

private:
	Ui::CXAboutWindow ui;
};

#endif // CXABOUTWINDOW_H
