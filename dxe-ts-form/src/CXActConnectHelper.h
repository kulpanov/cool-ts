#ifndef CXEDITWINDOW_H
#define CXEDITWINDOW_H

#include <QWidget>
#include "ui_CXActConnectHelper.h"


/*!
	Класс окна с редактированием файла.
*/
class CXActConnectHelper: public QDialog
{
	Q_OBJECT
private slots:
  void OnExitButton();
public:
  enum ERules{
    _generator_in  = 0x1
    ,_measurer_in  = 0x2
    ,_generator_out= 0x11
    ,_measurer_out = 0x12
  };

  void setMark(int _rule);

  void setNumber(const QString& _number);

  void setGateIP(const QString& _number);

  void setOutNumber(const QString& _number);
public:
	CXActConnectHelper(QWidget *parent = 0);
	~CXActConnectHelper();

private:
	Ui::CXActConnectHelper ui;

};

#endif // CXEDITWINDOW_H
