#include <QMessageBox>

#include "CXActConnectHelper.h"


CXActConnectHelper::CXActConnectHelper(QWidget* parent) :
    QDialog(parent) {
  ui.setupUi(this);

  connect(ui.mRemoveButton, SIGNAL(clicked()), this, SLOT(OnExitButton()));
  //frame->setStyleSheet("color:red")

}

CXActConnectHelper::~CXActConnectHelper() {

}

void CXActConnectHelper::setMark(int _rule){
  switch(_rule){
  case _generator_in:
    ui.l_out_gen->setStyleSheet("color:red");
  return;
  case _generator_out:
    ui.l_in_gen->setStyleSheet("color:red");
  return;
  case _measurer_in:
    ui.l_in_meas->setStyleSheet("color:red");
  return;
  case _measurer_out:
    ui.l_in_meas->setStyleSheet("color:red");
  return;
  }
}

void CXActConnectHelper::setNumber(const QString& _number){
  ui.l_connId->setText(QString().fromUtf8("Соединение номер ") + _number);
}

void CXActConnectHelper::setGateIP(const QString& _number){
  //ui.l_gateIP->setText(_number);
}

void CXActConnectHelper::setOutNumber(const QString& _number){
  //ui.l_out_number->setText(_number);
}

void CXActConnectHelper::OnExitButton(){
  this->close();
}


