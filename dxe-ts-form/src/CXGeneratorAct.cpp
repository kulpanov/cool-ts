#include "CXGeneratorAct.h"
#include "CXActConnectHelper.h"

#include <QLabel>

CXGeneratorAct::CXGeneratorAct(QWidget* parent) : CXBaseAct(parent)
{
	mType = E_GeneratorAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);
//
//	centralLayout->addWidget(new QLabel(tr("id"), mContainer));
//
//	mIDEdit = new QLineEdit(mContainer);
//	mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
//	centralLayout->addWidget(mIDEdit);

	centralLayout->addWidget(new QLabel(tr("со стороны"), mContainer));

	mDirectionBox = new QComboBox(mContainer);
	mDirectionBox->addItem(tr("Исходящего"));
	mDirectionBox->addItem(tr("Входящего"));
	centralLayout->addWidget(mDirectionBox);
  centralLayout->addWidget(new QLabel(tr("вызова, с частотой"), mContainer));

	//centralLayout->addWidget(new QLabel(tr("с частотой"), mContainer));
	mFrequencyEdit = new QSpinBox(mContainer);
	mFrequencyEdit->setRange(300, 3500);
	mFrequencyEdit->setValue(1000);
	//mFrequencyEdit->setMinimumSize(QSize(50, 16777215));
	mFrequencyEdit->setMaximumSize(QSize(60, 16777215));
	centralLayout->addWidget(mFrequencyEdit);
	centralLayout->addWidget(new QLabel(tr("Гц, "), mContainer));

	centralLayout->addWidget(new QLabel(tr("c уровнем(rms)"), mContainer));

	mAmplitudeEdit = new QLineEdit(mContainer);
	mAmplitudeEdit->setText("-10");
	mAmplitudeEdit->setValidator(new QRegExpValidator(QRegExp("^-?\\d+\\d+"), mAmplitudeEdit));
	mAmplitudeEdit->setMaximumSize(QSize(50, 16777215));
	centralLayout->addWidget(mAmplitudeEdit);
	centralLayout->addWidget(new QLabel(tr("дБ"), mContainer));

	silenceCheck = new QCheckBox(mContainer);
	silenceCheck->setChecked(false);
	connect(silenceCheck, SIGNAL(stateChanged(int)), this, SLOT(onCheckChange(int)));
	silenceCheck->setVisible(false);
	centralLayout->addWidget(silenceCheck);

  silenceEdit = new QLabel(mContainer);
  silenceEdit->setText(tr("Генерировать тишину(-60дБ)"));
  silenceEdit->setVisible(false);
  centralLayout->addWidget(silenceEdit);


  mHelperButton = new QPushButton(mContainer);
  mHelperButton->setText(QString().fromUtf8("Схема"));
  mHelperButton->setWhatsThis(QString().fromUtf8("Показать схему соединения"));
  connect(mHelperButton,    SIGNAL(clicked ()), this, SLOT(onHelperWindow()));
  centralLayout->addWidget(mHelperButton);

  centralLayout->addStretch();
}

CXGeneratorAct::~CXGeneratorAct()
{

}

void CXGeneratorAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
  //mIDEdit->setText(aElement.attribute("id"));
	mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
	mFrequencyEdit->setValue(aElement.attribute("freq").toInt());
	mAmplitudeEdit->setText(aElement.attribute("ampl"));
	(aElement.attribute("type") == "silence") ? silenceCheck->setChecked(true)
	                                          : silenceCheck->setChecked(false);
}

QDomElement CXGeneratorAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("play"));
  data << SXXMLData("descr", tr("Подключить генератор"));
	//data << SXXMLData("id", mIDEdit->text());
	data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));
	data << SXXMLData("type", silenceCheck->isChecked() ? tr("silence") : tr("sin"));
	data << SXXMLData("freq", QString("%1").arg(mFrequencyEdit->value()));
	data << SXXMLData("ampl", mAmplitudeEdit->text());

	return CXBaseAct::getXMLElement(aDomDocument, data);
}

void CXGeneratorAct::onCheckChange(int _check){
  if(silenceCheck->isChecked()){
    mFrequencyEdit->setEnabled(false);
    mAmplitudeEdit->setEnabled(false);
  }else{
    mFrequencyEdit->setEnabled(true);
    mAmplitudeEdit->setEnabled(true);
  };
};

void CXGeneratorAct::onHelperWindow(){
  CXActConnectHelper helper(this);
  int dir = mDirectionBox->currentIndex();
  helper.setMark(CXActConnectHelper::_generator_in + (dir << 4));
  helper.setNumber(mIDEdit->text());
  if (helper.exec())
  {
//    ui.mFileView->setRootIndex(mModel->setRootPath(CXSettings::valueStat(E_DirPath).toString()));
//    ui.mBackButton->setEnabled(false);
  }
}
