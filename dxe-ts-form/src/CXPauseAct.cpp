#include "CXPauseAct.h"
#include <QToolButton>
#include <QFileDialog>

CXPauseAct::CXPauseAct(QWidget *parent) : CXBaseAct(parent, true)
{
	mType = E_PauseAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);
//	mIDEdit->setVisible(false);
//	ui.label->setVisible(false);
	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

	QLabel* caption = new QLabel(tr("пауза"), mContainer);
	centralLayout->addWidget(caption);

	mDurationEdit = new QTimeEdit(mContainer);
	centralLayout->addWidget(mDurationEdit);

	QLabel* cap2 = new QLabel(tr("коммент"), mContainer);
  centralLayout->addWidget(cap2);

  mCommentEdit = new QLineEdit(mContainer);
  centralLayout->addWidget(mCommentEdit);

	centralLayout->addStretch();
}

CXPauseAct::~CXPauseAct()
{

}

void CXPauseAct::load(const QDomElement& aElement)
{
	mDurationEdit->setTime(QTime().addMSecs(aElement.attribute("pause").toInt()));
	mCommentEdit->setText(aElement.attribute("comment"));
}

QDomElement CXPauseAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("pause"));
	data << SXXMLData("descr", tr("Пауза"));

	QTime time = mDurationEdit->time();
	data << SXXMLData("pause", QString::number(time.msec() + (time.second() + (time.minute() + time.hour() * 24) * 60) * 1000));
  data << SXXMLData("comment", mCommentEdit->text());

	return CXBaseAct::getXMLElement(aDomDocument, data);
}

//--------------------------------------------------------------------------------------------------
///
CXAct_DTMF::CXAct_DTMF(QWidget* parent){
  mType = E_DTMFSetting;
  mActTypeBox->blockSignals(true);
  mActTypeBox->setCurrentIndex(mType);
  mActTypeBox->blockSignals(false);

  mIDEdit->setEnabled(false);
  QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

  centralLayout->addWidget(new QLabel(tr("Амплитуда"), mContainer));

  mAmplitude = new QLineEdit(mContainer);
  mAmplitude->setText("-5");
  centralLayout->addWidget(mAmplitude);

  centralLayout->addWidget(new QLabel(tr("дб, длина"), mContainer));
  mDuration = new QLineEdit(mContainer);
  mDuration->setText("30");
  centralLayout->addWidget(mDuration);

  centralLayout->addWidget(new QLabel(tr("мс, пауза"), mContainer));
  mPause = new QLineEdit(mContainer);
  mPause->setText("30");
  centralLayout->addWidget(mPause);

  centralLayout->addStretch();
}

CXAct_DTMF::~CXAct_DTMF(){

}

void CXAct_DTMF::load(const QDomElement& aElement){
  mAmplitude->setText(aElement.attribute("amplitude"));
  mDuration->setText(aElement.attribute("duration"));
  mPause->setText(aElement.attribute("pause"));
}

QDomElement CXAct_DTMF::getXMLElement(QDomDocument& aDomDocument){
  QList <SXXMLData> data;

  data << SXXMLData("name", tr("dtmf"));
  data << SXXMLData("descr", tr("DTMF"));

  data << SXXMLData("amplitude", mAmplitude->text());
  data << SXXMLData("duration", mDuration->text());
  data << SXXMLData("pause", mPause->text());

  return CXBaseAct::getXMLElement(aDomDocument, data);
}

//--------------------------------------------------------------------------------------------------
///
CXAct_RTP::CXAct_RTP(QWidget* parent):CXBaseAct(parent, false){
  mType = E_RTPSetting;
  mActTypeBox->blockSignals(true);
  mActTypeBox->setCurrentIndex(mType);
  mActTypeBox->blockSignals(false);

  mIDEdit->setEnabled(true);
  QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

  centralLayout->addWidget(new QLabel(tr("Задать параметры RTP во время отрабокти сценария"), mContainer));
//
//  mLength = new QLineEdit(mContainer);
//  mLength->setText("60");
//  centralLayout->addWidget(mLength);
//
//  centralLayout->addWidget(new QLabel(tr("мс, допустимый джиттер"), mContainer));
//  mJitter = new QLineEdit(mContainer);
//  mJitter->setText("0");
//  centralLayout->addWidget(mJitter);
//  centralLayout->addWidget(new QLabel(tr("мс"), mContainer));

//  centralLayout->addWidget(new QLabel(tr("мс, пауза"), mContainer));
//  mPause = new QLineEdit(mContainer);
//  mPause->setText("30");
//  centralLayout->addWidget(mPause);

  centralLayout->addStretch();
}

CXAct_RTP::~CXAct_RTP(){

}

void CXAct_RTP::load(const QDomElement& aElement){
  CXBaseAct::load(aElement);
}

QDomElement CXAct_RTP::getXMLElement(QDomDocument& aDomDocument){
  QList <SXXMLData> data;

  data << SXXMLData("name", tr("rtp_params"));
  data << SXXMLData("descr", tr("RTPParams"));

  data << SXXMLData("id", mIDEdit->text());

  return CXBaseAct::getXMLElement(aDomDocument, data);
}

