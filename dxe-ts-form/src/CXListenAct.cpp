#include "CXListenAct.h"

#include <QLabel>

CXListenAct::CXListenAct(QWidget* parent) : CXBaseAct(parent)
{
	mType = E_ListenAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

//	centralLayout->addWidget(new QLabel(tr("id"), mContainer));
//
//	mIDEdit = new QLineEdit(mContainer);
//	mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
//	centralLayout->addWidget(mIDEdit);

	centralLayout->addWidget(new QLabel(tr("со стороны"), mContainer));

	mDirectionBox = new QComboBox(mContainer);
	mDirectionBox->addItem(tr("Исходящего"));
	mDirectionBox->addItem(tr("Входящего"));
	centralLayout->addWidget(mDirectionBox);

	centralLayout->addStretch();
}

CXListenAct::~CXListenAct()
{

}

void CXListenAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
  //mIDEdit->setText(aElement.attribute("id"));
	mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
}

QDomElement CXListenAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("listen"));
	data << SXXMLData("descr", tr("Прослушать"));
	//data << SXXMLData("id", mIDEdit->text());
	data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));
	data << SXXMLData("type", tr("sound_out"));

	return CXBaseAct::getXMLElement(aDomDocument, data);
}


CXListenStopAct::CXListenStopAct(QWidget* parent) : CXBaseAct(parent)
{
  mType = E_ListenStopAct;
  mActTypeBox->blockSignals(true);
  mActTypeBox->setCurrentIndex(mType);
  mActTypeBox->blockSignals(false);

  QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

  centralLayout->addWidget(new QLabel(tr("со стороны"), mContainer));

  mDirectionBox = new QComboBox(mContainer);
  mDirectionBox->addItem(tr("Исходящего"));
  mDirectionBox->addItem(tr("Входящего"));
  centralLayout->addWidget(mDirectionBox);

  centralLayout->addStretch();
}

CXListenStopAct::~CXListenStopAct()
{

}

void CXListenStopAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
  //mIDEdit->setText(aElement.attribute("id"));
  mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
}

QDomElement CXListenStopAct::getXMLElement(QDomDocument& aDomDocument)
{
  QList <SXXMLData> data;

  data << SXXMLData("name", tr("stopListen"));
  data << SXXMLData("descr", tr("Остановить прослушку"));
  //data << SXXMLData("id", mIDEdit->text());
  data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));
  //data << SXXMLData("type", tr("stop_sound_out"));

  return CXBaseAct::getXMLElement(aDomDocument, data);
}

