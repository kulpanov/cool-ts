#ifndef CXEXECUTESCRIPTACT_H
#define CXEXECUTESCRIPTACT_H


#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>

#include "CXBaseAct.h"
/*!
	Класс акта "Выполнить скрипт".
*/
class CXExecuteScriptAct : public CXBaseAct
{
	Q_OBJECT

public:
	CXExecuteScriptAct(QWidget* parent = 0);
	virtual ~CXExecuteScriptAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private slots:
	void onOpenFile();

private:
	QLineEdit* mFileNameEdit;
	QLineEdit* mConnectEdit;
  //QCheckBox* mParallel;
	QLineEdit* mLoopEdit;
};

#endif // CXEXECUTESCRIPTACT_H
