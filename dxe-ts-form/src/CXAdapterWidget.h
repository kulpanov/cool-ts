﻿#ifndef CXADAPTERWIDGET_H
#define CXADAPTERWIDGET_H

#include <QWidget>
#include <QNetworkInterface>

#include "ui_CXAdapterWidget.h"

/*!
*/
class CXAdapterWidget : public QWidget
{
	Q_OBJECT
protected:
	Ui::CXAdapterWidget ui;
public:
	CXAdapterWidget(const QString& _humanReadableName
	    , const QNetworkAddressEntry& addr, QWidget* parent = 0);
	~CXAdapterWidget();

public:
	QString mIP;
	QString mMask;
};

#endif // CXADAPTERWIDGET_H
