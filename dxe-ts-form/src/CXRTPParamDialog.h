#ifndef CXRTPDIALOG_H
#define CXRTPSDIALOG_H

#include <QDialog>
#include <QList>
#include <QNetworkInterface>

#include "ui_rtpparam-dlg.h"

/*!
	Класс диалога настроек.
*/
class CXRTPParamDlg : public QDialog
{
	Q_OBJECT
public:
	CXRTPParamDlg(QWidget *parent, void* _param);
	~CXRTPParamDlg();
public slots:
void onPacket(int );

void onJitter(int );

void onQuant(double );

private:
	Ui::rtpParamDialog ui;
	void* call;
};

#endif // CXSETTINGSDIALOG_H
