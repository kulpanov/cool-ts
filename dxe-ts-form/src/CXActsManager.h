#ifndef CXACTSMANAGER_H
#define CXACTSMANAGER_H
#include <stddef.h>
#include <QTreeWidget>

class AXAbstractAct;

/*!
	Класс менеджера актов.
*/
class CXActsManager
{
public:
	CXActsManager();
	~CXActsManager();

	/*!
		Функция установки вью.

		\param aTreeWidget Указатель на вью.
		\note Для манипуляции с удалением и созданием новых элементов.
	*/
	void setView(QTreeWidget* aTreeWidget);

	/*!
		Функция получения списка актов.

		\return Список актов.
	*/
	QList <AXAbstractAct*> getActs();

	/*!
		Функция установки пути с именем редактируемого файла.

		\param aFileName Путь с именем редактируемого файла
	*/
	void setEditFile(const QString& aFileName);

	/*!
		Получения объекта акта по типу.

		\param aType Тип акта.
		\return Указатель на объект AXAbstractAct либо <i>NULL</i>.
	*/
	AXAbstractAct* getAct(int aType);

	/*!
		Функция поиска объекта акта в дереве.

		\param aAct Указатель на объект акта.
		\return Указатель на узел дерева.
	*/
	QTreeWidgetItem* findItem(AXAbstractAct* aAct);

	/*!
		Функция поиска индекса акта в дереве.

		\param aAct Указатель на объект акта.
		\return Индекс акта в дереве.
	*/
	int findIndex(AXAbstractAct* aAct);

	/*!
		Функция замены объекта акта на указанный тип.

		\param aAct Указатель на объект акта.
		\param aType Тип нового акта.
	*/
	void replaceType(AXAbstractAct* aAct, int aType);

	/*!
		Функция удаления акта.

		\param aAct Указатель на удаляемый акт.
	*/
	void remove(AXAbstractAct* aAct);

	/*!
		Функция копирования акта.

		\param aAct указатель на копируемый акт.
	*/
	void copy(AXAbstractAct* aAct);

	/*!
		Функция добавления акта в конец списка.

		\param eActTypes - тип добавляемого акта.
	*/
	void appendAct(int type);

	/*!
	 * Пересчитать акты, пронумеровав их
	 */
	void recalcActs();

	/*!
		Функция получения XML-представления данных.

		\return XML-представление данных.
	*/
	QString serializeToXML();

private:
	QTreeWidget* mTreeWidget;
};

#endif // CXACTSMANAGER_H
