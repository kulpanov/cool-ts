#ifndef CXPROTOCOLACT_H
#define CXPROTOCOLACT_H

#include "CXBaseAct.h"

#include <QCheckBox>

/*!
	Класс акта "Протокол".
*/
class CXProtocolAct : public CXBaseAct
{
public:
	CXProtocolAct(QWidget* parent = 0);
	~CXProtocolAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
	QCheckBox* mGenerateCheck;
};

#endif // CXPROTOCOLACT_H
