#include <QMessageBox>

#include "CXSettingsDialog.h"
#include "CXAdapterWidget.h"

#include "CXSettings.h"
#include "utils/CXMLReader.h"
#include "dxe-ts-engine.h"

#include "wavStreams.h"
#include <QFileDialog>

static int calltimeout = 30;
extern __declspec(dllimport) int plotSleepTime;

QSpinBox* plotSleepTimeSpinBox = NULL;
CXSettingsDialog::CXSettingsDialog(QWidget* parent) :
    QDialog(parent) {
  mIndex = -1;
  ui.setupUi(this);
  ui.tabWidget->setCurrentIndex(0);
  ui.mDirPathEdit->setPath(CXSettings::valueStat(E_DirPath).toString());
  ui.mWavPathEdit->setPath(CXSettings::valueStat(E_WavPath).toString());
  ui.mEditViewBox->setCurrentIndex(CXSettings::valueStat(E_DefaultEditView).toInt());
  addAdapters();
  connect(ui.mAdaptersList, SIGNAL(itemClicked(QTreeWidgetItem*, int)), SLOT(onItemClick(QTreeWidgetItem*, int)));

  CXMLReader xml("call-server.settings.xml");
  int rtpSize = xml.GetAttribute("Params/RTP/packetSize", "value", "60").toInt();
  ui.packetSize->setValue(rtpSize);
  IWavStream::rtp_packet_size = rtpSize * 8;

  calltimeout = xml.GetAttribute("Params/RTP/call_timeout", "value", "30").toInt();
  ui.callTimeOut->setValue(calltimeout);

  {
    QGroupBox* groupBox = new QGroupBox(ui.tab_3);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    QHBoxLayout* horizontalLayout_2 = new QHBoxLayout(groupBox);
    horizontalLayout_2->setSpacing(6);
    horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
    horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
    QLabel* label_2 = new QLabel(groupBox);
    label_2->setObjectName(QString::fromUtf8("label_2"));

    horizontalLayout_2->addWidget(label_2);

    plotSleepTimeSpinBox = new QSpinBox(groupBox);
    plotSleepTimeSpinBox->setObjectName(QString::fromUtf8("callTimeOut"));
    plotSleepTimeSpinBox->setMinimum(300);
    plotSleepTimeSpinBox->setMaximum(60000);
    plotSleepTimeSpinBox->setValue(600);
    horizontalLayout_2->addWidget(plotSleepTimeSpinBox);

    QLabel* label_3 = new QLabel(groupBox);
    label_3->setObjectName(QString::fromUtf8("label_3"));

    horizontalLayout_2->addWidget(label_3);

    QSpacerItem* horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_2->addItem(horizontalSpacer_2);

    ui.verticalLayout_3->addWidget(groupBox);

    label_3->setText(tr("Задержка измерения"));
    plotSleepTimeSpinBox->setValue(plotSleepTime);
  }
  {
    connect(ui.exportLog, SIGNAL(clicked()), SLOT(onClickExportLog()));
  }
}

CXSettingsDialog::~CXSettingsDialog() {

}

void CXSettingsDialog::accept() {
  CXSettings::setValueStat(E_DirPath, ui.mDirPathEdit->path());
  CXSettings::setValueStat(E_WavPath, ui.mWavPathEdit->path());
  CXSettings::setValueStat(E_DefaultEditView, ui.mEditViewBox->currentIndex());

  if(plotSleepTimeSpinBox)
    plotSleepTime = plotSleepTimeSpinBox->value();

  int changed = 0;
  if(mIndex == -1){
    QMessageBox::question(this, tr("DXE-test-system"),
        tr("Необходимо выбрать интерфейс.\n"), QMessageBox::Ok);
    return;
  }
  //TODO: form: проверить на изм маски 255.255.0.0<->255.255.255.0
  QString s = addr.at(mIndex).ip().toString();
  QString m = addr.at(mIndex).netmask().toString();
  CXMLReader xml("call-server.settings.xml");
  if (xml.GetAttribute("Params/General/myIP", "value", "127.0.0.1") != s) {
    xml.SetAttribute("Params/General/myIP", "value", s);
    xml.SetAttribute("Params/General/myNet", "value", m);
    changed = 1;
  }

  if(ui.packetSize->value() != IWavStream::rtp_packet_size / 8){
    int packetSize = ui.packetSize->value();
    xml.SetAttribute("Params/RTP/packetSize", "value", QString().number(packetSize));
    IWavStream::rtp_packet_size = packetSize * 8;
    changed |= 0x10;
  }
  if(ui.callTimeOut->value() != calltimeout){
    calltimeout = ui.callTimeOut->value();
    xml.SetAttribute("Params/RTP/call_timeout", "value", QString().number(calltimeout));
    changed |= 0x10;
  }
  if (changed != 0){
    xml.WriteFile("call-server.settings.xml");

    if((changed & 1) != 0){
      int ret = QMessageBox::question(this, tr("DXE-test-system"),
          tr("Сетевые настройки call-server были изменены.\n"
              "Необходимо перезапустить программу, сделать это сейчас?"),
              QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
      if(ret == QMessageBox::Yes){
        QApplication::exit(0);
      }
    }
  }

  QDialog::accept();
}


void CXSettingsDialog::setInterface()
{

}

void CXSettingsDialog::onItemClick(QTreeWidgetItem* item, int column)
{ column++;
  if (item->checkState(0) == Qt::Unchecked)
  {
    QTreeWidgetItem* curItem = NULL;
    for (int i = 0; i < ui.mAdaptersList->topLevelItemCount(); i++)
    {
      curItem = ui.mAdaptersList->topLevelItem(i);

      if (curItem != item && curItem->checkState(0) == Qt::Checked) curItem->setCheckState(0, Qt::Unchecked);
    }

    item->setCheckState(0, Qt::Checked);
    mIndex = ui.mAdaptersList->indexOfTopLevelItem(item);
  }
  else
  {
    mIndex = -1;
    item->setCheckState(0, Qt::Unchecked);
  }
}

void CXSettingsDialog::addAdapters()
{
  CXMLReader xml("call-server.settings.xml");
  QString curIP   = xml.GetAttribute("Params/General/myIP", "value", "127.0.0.1");
  QString curMask = xml.GetAttribute("Params/General/myNet", "value", "127.0.0.1");

  if(curIP == "127.0.0.1"){
    mIndex = 0;
    return;
  }
  auto interfaces = QNetworkInterface::allInterfaces();
  auto it_interf = interfaces.begin();
  for (; it_interf != interfaces.end(); ++it_interf)
  {

    if (!( ((*it_interf).flags() & QNetworkInterface::IsUp)
        && ((*it_interf).flags() & QNetworkInterface::IsRunning)
        && !((*it_interf).flags() & QNetworkInterface::IsLoopBack))) continue ;

    //      && aInterface.addressEntries().last().ip() != QHostAddress::LocalHost)
    //  if (!aInterface.addressEntries().isEmpty() && aInterface.addressEntries().last().ip() != QHostAddress::LocalHost)
    auto addresses = (*it_interf).addressEntries();
    if(addresses.isEmpty())  continue ;
    auto it_addr = addresses.begin(); //begin()==ipv6 address
    for( ; it_addr != addresses.end(); it_addr++)
    {
      if((*it_addr).ip().protocol() != QAbstractSocket::IPv4Protocol)
        continue;

      addr.push_back(*it_addr);
      CXAdapterWidget* widget = new CXAdapterWidget((*it_interf).humanReadableName(), *it_addr);

      QTreeWidgetItem* newItem = new QTreeWidgetItem(ui.mAdaptersList);
      newItem->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);

      if (widget->mIP == curIP && widget->mMask == curMask){
        newItem->setCheckState(0, Qt::Checked);
        mIndex = addr.count() - 1;
      }else
        newItem->setCheckState(0, Qt::Unchecked);

      ui.mAdaptersList->addTopLevelItem(newItem);
      ui.mAdaptersList->setItemWidget(newItem, 0, widget);

    }
  }
  return ;
}

void CXSettingsDialog::onClickExportLog(){
//  QFile::remove("cool-ts.logs.7z");
  auto fname = QFileDialog::getSaveFileName(this, QString().fromUtf8("сохранить архив как"), "cooll-ts.log.7z", "*.7z");
//  qDebug() << fname;
  if(fname.size() == 0) return;
  LOG1(INFO, "%s\n", QString("\"" + QApplication::applicationDirPath() + QString("/7z.exe\" a \"%0\" tmp\\ wavs\\ *.log *.xml ").arg(fname)).toStdString().c_str());
  QProcess::execute("\"" + QApplication::applicationDirPath() + QString("/7z.exe\" a \"%0\" tmp\\ wavs\\ *.log *.xml ").arg(fname));
}
//
bool CXSettingsDialog::checkAdapters(){
  return mIndex >= 0;
}

QString CXSettingsDialog::getCurrAdapter() {
  if(checkAdapters())
    return addr.at(mIndex).ip().toString();
  return "";
}
