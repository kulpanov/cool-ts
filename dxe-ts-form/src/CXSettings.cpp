﻿//#include <unistd.h>

#include <QFile>
#include <QTextStream>
#include <QApplication>
#include <QDomDocument>
#include <QStringList>

#include "CXSettings.h"
CXSettings* CXSettings::mSettings = NULL;

CXSettings* CXSettings::inst()
{
	if (mSettings == NULL) mSettings = new CXSettings;

	return mSettings;
}

QVariant CXSettings::valueStat(eSettingType aType)
{
	return inst()->value(aType);
}

void CXSettings::setValueStat(eSettingType aType, const QVariant& aValue)
{
	inst()->setValue(aType, aValue);
}

CXSettings::CXSettings()
{
	mTypeDescription.insert(E_DirPath, "Dir Path");
	mTypeDescription.insert(E_WavPath, "Wav Path");

	QMap <int, QString>::iterator iter;
	for (iter = mTypeDescription.begin(); iter != mTypeDescription.end(); ++iter)
	{
		mSettingsValue.insert(iter.key(), QVariant());
	}

	load(QApplication::applicationDirPath() + "/settings.xml");
}

CXSettings::~CXSettings()
{
	save(QApplication::applicationDirPath() + "/settings.xml");
}

void CXSettings::load(const QString& aFileName)
{
	QFile xmlFile(aFileName);

	if (xmlFile.open(QIODevice::ReadOnly))
	{
		QDomDocument domDocument;

		domDocument.setContent(&xmlFile);
		QDomElement rootElement = domDocument.documentElement();

		if (rootElement.tagName() == "Settings")
		{
			QDomElement paramElement = rootElement.firstChildElement("Param");
			while (paramElement.isElement())
			{
				mSettingsValue.insert(
				    paramElement.attribute("type").toInt(), paramElement.attribute("value"));

				paramElement = paramElement.nextSiblingElement("Param");
			}
		}
	}

}

void CXSettings::save(QString aFileName)
{
	if (aFileName.isEmpty()) aFileName = QApplication::applicationDirPath() + "/settings.xml";

	QFile xmlFile(aFileName);
	if (xmlFile.open(QIODevice::WriteOnly))
	{
		QTextStream textStream(&xmlFile);
		textStream.setCodec("UTF-8");

		textStream << "<?xml version=\"1.0\"  encoding=\"UTF-8\" ?>\n";
		textStream << "<Settings>\n";

		QMap <int, QVariant>::iterator iter;
		for (iter = mSettingsValue.begin(); iter != mSettingsValue.end(); ++iter)
		{
			textStream
			<< QString("	<Param desrc=\"%1\" type=\"%2\" value=\"%3\"/>\n")
        .arg(mTypeDescription.value(iter.key()))
        .arg(iter.key())
        .arg(iter.value().toString().replace("&", "&amp;").replace('"', "&quot;"));
		}

		textStream << "</Settings>";

		xmlFile.close();
	}
}

void CXSettings::setValue(eSettingType aType, const QVariant& aValue)
{
	switch (aValue.type())
	{
		case QVariant::StringList:
		{
			mSettingsValue.insert(aType, aValue.toStringList().join("|||"));
			break;
		}
		default:
		{
			mSettingsValue.insert(aType, aValue);
			break;
		}
	}
}

QVariant CXSettings::value(eSettingType aType)
{
	switch (aType)
	{
		default: return mSettingsValue.value(aType);
	}
}
