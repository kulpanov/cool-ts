#include <stddef.h>
#include <iostream>
#include <winsock2.h>
#include <QDesktopServices>
#include <QUrl>
#include <QDebug>
#include <QTreeWidgetItem>
#include <QFileDialog>
#include <QDateTime>
#include <QUdpSocket>

#include <stddef.h>
#include "mainwindow.h"
#include "qmessagebox.h"

#include "CXSettings.h"
#include "CXSettingsDialog.h"
#include "CXAboutWindow.h"

#include "CXAboutWindow.h"
#include <QtGui>

#include "graphicdata/CXGraphic.h"

#include "../../dxe/version.h"
//#define TEST_FORM

#include "debug.h"

#include "CXRTPParamDialog.h"
#ifndef TEST_FORM
#include "dxe-ts-engine.h"
#include "wavStreams.h"
#endif

#define BATCH_TEST
using namespace DXE;

#ifdef TEST_X
bool forgetExecuteNewTest = false;
#endif

CLogListener_form::CLogListener_form(MainWindow* _log_receiver){
  moveToThread(QThread::currentThread());

  connect(this, SIGNAL(logTo(const SMsg_log&))
      , _log_receiver , SLOT(onLogMsg(const SMsg_log&))
      , Qt::ConnectionType::QueuedConnection);

  connect(this, SIGNAL(logVector(const QString&, const QString&, const QVector<QPair<double, double> >& ))
      , _log_receiver , SLOT(onDrawVec(const QString&, const QString&,const QVector<QPair<double, double> >& ))
      , Qt::ConnectionType::BlockingQueuedConnection);

  connect(this, SIGNAL(sgn_runDlg(int , void* ))
      , _log_receiver , SLOT(onRunDlg(int , void* ))
      , Qt::ConnectionType::BlockingQueuedConnection);

}


void CLogListener_form::recvMsg(const SMsg_log& _msg){
  //SMsg_log* pmsg = new SMsg_log(_msg);
//  std::cout << "LogTo:" << _msg.toString().toUtf8().begin() << std::endl;
  emit logTo(_msg);
}

void CLogListener_form::recvMsg(const QString& _fname, const QString& _fname_script, const QVector<QPair<double, double> >& _vec){
//  std::cout << "LogTo:" << _fname_script.toUtf8().begin() << std::endl;
  emit logVector(_fname, _fname_script, _vec);
}

void CLogListener_form::runDlg(const int _dlg, void* _call){
  emit sgn_runDlg(_dlg, _call);
}


extern void backup_log(const char* fname);

static bool wasFault = false;
MainWindow::MainWindow(QWidget* parent, Qt::WFlags flags) : QWidget(parent, flags)
, /*scriptIsExecuting(false),*/ logIsCleared(false), cur_test_id(0)
{
	ui.setupUi(this);

	mModel = new QFileSystemModel(this);
	mModel->setNameFilterDisables(false);
	mModel->setNameFilters(QStringList() << "*.tst");
	mModel->setHeaderData(0, Qt::Orientation::Horizontal, QString().fromUtf8("Имя файла"));

	ui.mFileView->setModel(mModel);
	ui.mFileView->setRootIndex(
	    mModel->setRootPath(CXSettings::valueStat(E_DirPath).toString()));
  ui.mBackButton->setEnabled(false);

  	ui.mFileView->setSelectionMode(QAbstractItemView::ExtendedSelection);

	ui.mFileView->setContextMenuPolicy(Qt::CustomContextMenu);
	ui.mFileView->setIconSize(QSize(25, 25));
	ui.mFileView->setViewMode(QListView::ListMode);
  ui.mFileView->setWrapping(true);
  ui.mFileView->setResizeMode(QListView::Adjust);
  //
	ui.mBackButton->setEnabled(false);

	mMenu = new QMenu(ui.mFileView);
	mMenu->addAction(tr("Выполнить"));
	mMenu->addAction(tr("Правка"));
	mMenu->addAction(tr("Прервать все"));
  mMenu->addAction(tr("------------"));
	mMenu->addAction(tr("Выполнить циклически"));

	//mMenu->addAction(tr("Очистить лог"));
	//mMenu->addAction(tr("Прервать исполнение"));

  mEditWindow = NULL;

	connect(ui.mFileView, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(onItemDoubleClick(const QModelIndex&)));
	connect(ui.mFileView, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(onContextMenu(const QPoint&)));

	connect(mMenu, SIGNAL(triggered(QAction*)), this, SLOT(onMenuTrigger(QAction*)));

	ui.mLogView->headerItem()->setText(0, QString::fromUtf8("События"));
	ui.mLogView->headerItem()->setText(1, QString::fromUtf8("Результат"));
	ui.mLogView->setContextMenuPolicy(Qt::CustomContextMenu);
  mLogMenu = new QMenu(ui.mLogView);
  mLogMenu->addAction(tr("Очистить"));
  mLogMenu->addAction(tr("Сохранить"));
  connect(ui.mLogView, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(onContextMenu_LogTree(const QPoint&)));
  connect(mLogMenu, SIGNAL(triggered(QAction*)), this, SLOT(onMenuTrigger_LogTree(QAction*)));
//
	connect(ui.mSettingsButton,	SIGNAL(clicked()), this, SLOT(onShowSettings()));
	connect(ui.mBackButton,		SIGNAL(clicked()), this, SLOT(onBack()));
	connect(ui.mExplorerButton,	SIGNAL(clicked()), this, SLOT(onOpenExplorer()));
	connect(ui.mNewButton,		SIGNAL(clicked()), this, SLOT(onNew()));
  connect(ui.mDirButton,    SIGNAL(clicked()), this, SLOT(onDir()));

	connect(&testTimer, SIGNAL(timeout()), this, SLOT(onLoopAll()));
	wasFault = false;
	this->setWindowTitle(tr("Подождите, идёт инициализация..."));
  QFile::remove("TS.log");
  QDir().remove("tmp");
  QDir().mkdir("tmp");
  QDir().mkdir("logs");

  backup_log("cool.test.log");
  init_debug(TSTL1, "test", VERSION_STRING , "cool");//""
  LOG(INFO, "MainWindow::start\n");
  startTimer.singleShot(0, this,  SLOT(onDelayedStart()));
}


extern int getCPUUsage();

void MainWindow::onDelayedStart(){
  LOG(INFO, "MainWindow::onDelayedStart\n");
#ifndef TEST_FORM

  try{
    QString str_curr_addr;
    //permissionOfExecute = true;
    if("check valid IP"){
      //проверить на доступность ip
      CXSettingsDialog dialog;
      permissionOfExecute = dialog.checkAdapters();
      str_curr_addr = dialog.getCurrAdapter();
      if(!permissionOfExecute){
        LOG(ERRR, "MainWindow::wrong adapter, permissionOfExecute\n");

        QMessageBox::critical(this, tr("Cool-Test"),
            tr("Неверный IP, проверьте сетевые настройки .\nЗапуск сценариев запрещен"),
                QMessageBox::Close, QMessageBox::Close);
      }
    }
    if("test for free sip-socket" && !str_curr_addr.isEmpty()){
      static int count_of_attempts = 30;
      QUdpSocket sip_test_socket;
      //_socket.set_option(boost::asio::socket_base::reuse_address(true), ec);
      if(! sip_test_socket.bind(QHostAddress(str_curr_addr), 5060/*, QUdpSocket::ReuseAddressHint*/)){
        if(--count_of_attempts <= 0){
          LOG(ALRM, "MainWindow::5060 is busy\n");

          QMessageBox::critical(this, tr("Cool-Test"),
              tr("Порт SIP-протокола(5060)занят.\nВозможно приложение запущено повторно.\nПриложение будет закрыто"),
                  QMessageBox::Close, QMessageBox::Close);
          startTimer.singleShot(0, this,  SLOT(close()));
        }else
          startTimer.singleShot(200, this,  SLOT(onDelayedStart()));
        return ;
      }
    }
  //иницилизировать скрипт-систему
    qRegisterMetaType<SMsg_log>("SMsg_log");
    CLogListener_form* log_listener = new CLogListener_form(this);
    DXE::CTestScript::init(log_listener);

    DXE::CTestScript::testsPath = CXSettings::valueStat(E_DirPath).toString();
    IWavStream::wavsPath   = CXSettings::valueStat(E_WavPath).toString().toUtf8().constData();
    LOG1(TSTL1, "MainWindow::onDelayedStart: IWavStream::wavsPath %s\n", IWavStream::wavsPath.c_str());

  }catch(P_EXC e){
    QMessageBox::critical(this, tr("Cool-Test"),
        tr(e.what())+tr("\nПриложение будет закрыто"),
            QMessageBox::Close, QMessageBox::Close);

    startTimer.singleShot(0, this,  SLOT(close()));
  }
  if(permissionOfExecute){
    this->setWindowTitle(VERSION_STRING);
    getCPUUsage();//init call
    startTimer.singleShot(1000, this,  SLOT(onPeriodicalTimer()));
  }else
    this->setWindowTitle(tr(VERSION_STRING "(Запуск сценариев запрещен)"));

#endif
}

//
void MainWindow::onPeriodicalTimer(){
  static const auto versionString = QString(tr(VERSION_STRING"(Загрузка CPU:"));
  int cpu = getCPUUsage();
  this->setWindowTitle(versionString + QString().number(cpu>0?cpu:0, 10)+ QString("%)"));
  if(CTestScript::count_executers >0 )
  if(cpu>85){
    QMessageBox::critical(this, tr("Cool-Test"),
        tr("\nЗагрузка процессоров более 85%, тест, возможно, будет не адекватен"),
            QMessageBox::Close, QMessageBox::Close);
  }
  startTimer.singleShot(1000, this,  SLOT(onPeriodicalTimer()));
}

void MainWindow::closeEvent(QCloseEvent *event)
{ //static int released = 0;
//  this->setWindowTitle(tr("Подождите, идёт завершение..."));
// // if(released == 0){
//    DXE::CTestScript::release();
//    released = 1;
//    event->ignore();
    //return;
//  }
//  close();
  event->accept();
}

//
MainWindow::~MainWindow()
{
  LOG(INFO, "MainWindow::stop\n");
	CXSettings::inst()->save();
#ifndef TEST_FORM
	DXE::CTestScript::release();
#endif

	if (mEditWindow != NULL)
	{
		delete mEditWindow;
		mEditWindow = NULL;
	}
  WSACleanup();
  LOG(INFO, "MainWindow::exit\n");
	//exit(0);
}

void MainWindow::onItemDoubleClick(const QModelIndex& aIndex)
{
	if (!aIndex.isValid()) return;

	if (mModel->isDir(aIndex))
	{
	  indexHistory.push_back(ui.mFileView->rootIndex());
		ui.mBackButton->setEnabled(true);
		ui.mFileView->setRootIndex(aIndex);
//		ui.mBackButton->setEnabled(ui.mFileView->rootIndex() != mModel->index(CXSettings::valueStat(E_DirPath).toString()));
	}
	else
	{
		//executeFile(mModel->filePath(ui.mFileView->currentIndex()));
	  editFile(mModel->filePath(ui.mFileView->currentIndex()));
	}
}

//
void MainWindow::onContextMenu(const QPoint& aPoint)
{
	if (!mModel->isDir(ui.mFileView->currentIndex()))
	{
		mMenu->popup(ui.mFileView->mapToGlobal(aPoint));
	}
}

//
void MainWindow::onContextMenu_LogTree(const QPoint& aPoint)
{
 // if(ui.mLogView->topLevelItemCount () == 0) return;
  mLogMenu->popup(ui.mLogView->mapToGlobal(aPoint));
}

//
void MainWindow::onMenuTrigger_LogTree(QAction* aAction)
{
  switch (mLogMenu->actions().indexOf(aAction))
    {
      //Очистить
      case 0:
      {
        if(DXE::CTestScript::count_executers == 0){
          cur_test_id = 0;
          ui.mLogView->clear();
          QFile::remove("cool.test.log");
          wasFault = false;
        }
      }
      break;
      //Сохранить
      case 1:
      {
        if(DXE::CTestScript::count_executers != 0){
          break;
        }
        QString fileName = QFileDialog::getSaveFileName(this,
                                        tr("Сохранить лог"),
                                        QDir::currentPath() + "/logs",
                                        tr("Архив (*.log)") );

        if (fileName.isEmpty())
          return;
        else {
          QFile file(fileName);
          if (!file.open(QIODevice::WriteOnly)) {
             QMessageBox::information(this, tr("Не могу открыть файл для записи"),
                 file.errorString());
             return;
          }

          class CTreeLogger{
            QTreeWidget* tree;
            QTreeWidgetItemIterator it;

            CTreeLogger(QTreeWidget* _tree, const QTreeWidgetItemIterator& _it):
              tree(_tree), it(_it){

            }
          public:
            CTreeLogger(QTreeWidget* _tree):
              tree(_tree), it(_tree){
            }

            void log(QFile& _file){
              const QTreeWidgetItem *head = *it;
              if(NULL == head) return;
              _file.write(head->text(0).toLocal8Bit());//toUtf8 - нет
              _file.write("\n");
              CTreeLogger(tree, ++it).log(_file);
            }
          };


          CTreeLogger(ui.mLogView).log(file);
          file.close();

          QFile::copy(QDir::currentPath() + "/cool.test.log", fileName + ".log");

//          {
//          QFile file_log(QDir::currentPath() + "/Cool-TS.log");
//          file_log.open(QIODevice::Truncate);
//          file_log.close();
//          }
        }
      }
      break;
    }
}

//
void MainWindow::onShowSettings()
{
	CXSettingsDialog dialog;
	dialog.setWindowModality(Qt::ApplicationModal);
	if (dialog.exec())
	{
		if(ui.mFileView->rootIndex() != mModel->index(CXSettings::valueStat(E_DirPath).toString())){
		  indexHistory.push_back(ui.mFileView->rootIndex());
		  ui.mBackButton->setEnabled(true);
		  ui.mFileView->setRootIndex(mModel->setRootPath(CXSettings::valueStat(E_DirPath).toString()));
		}
//		ui.mBackButton->setEnabled(false);
#ifndef TEST_FORM
    DXE::CTestScript::testsPath = CXSettings::valueStat(E_DirPath).toString();
    IWavStream::wavsPath   = CXSettings::valueStat(E_WavPath).toString().toUtf8().constData();
#endif
	}
}

void MainWindow::onBack()
{
  if(indexHistory.size() > 0){
    ui.mFileView->setRootIndex(indexHistory.back());
    indexHistory.pop_back();
  }
  ui.mBackButton->setEnabled(indexHistory.size() > 0);
//  QModelIndex index = ui.mFileView->rootIndex();
}

void MainWindow::onAbout()
{
	CXAboutWindow about(this);
	about.exec();
//  logRecv(QString("replace :%0").arg(++iii), LogListener::second
//      + LogListener::replace);
}

void MainWindow::onOpenExplorer()
{
	QDesktopServices::openUrl(QUrl(QString("\"%1\"").arg(mModel->filePath(ui.mFileView->rootIndex())), QUrl::TolerantMode));
  //QFileDialog::getExistingDirectory(NULL, "caption", mModel->filePath(ui.mFileView->rootIndex()), QFileDialog::Options()));
}

QList<QString> wstr_list;

void MainWindow::onMenuTrigger(QAction* aAction)
{
	switch (mMenu->actions().indexOf(aAction))
	{
		//Выполнить
		case 0:
		{
		  QModelIndexList index_list = ui.mFileView->selectionModel()->selectedIndexes();
		  for(auto ind_it = index_list.begin(); ind_it!=index_list.end(); ++ind_it){
		    if(! mModel->isDir(*ind_it)){
		      LOG1(INFO, "form:execute test: %s\n", (*ind_it).data().toString().toUtf8().begin() );
		      executeFile(mModel->filePath(*ind_it));
		    }
		  }
			break;
		}
		//Правка
		case 1:
		{
			editFile(mModel->filePath(ui.mFileView->currentIndex()));
			break;
		}
		case 2:
		{
#ifndef TEST_FORM
		  CTestScript::cancelAll();
#endif
		  wstr_list.clear();
		  wasFault = false;
		  testTimer.stop();
		  break;
		}
		case 4:{
		  wstr_list.clear();testTimer.stop();wasFault = false;
      QModelIndexList index_list = ui.mFileView->selectionModel()->selectedIndexes();
      for(auto ind_it = index_list.begin(); ind_it!=index_list.end(); ++ind_it){
        if(! mModel->isDir(*ind_it)){
          LOG1(INFO, "form:execute test: %s\n", (*ind_it).data().toString().toUtf8().begin() );
          wstr_list.push_back(mModel->filePath(*ind_it));
        }
      }
      onLoopAll();
		}
	  break;
	}
}

void MainWindow::onNew()
{
#ifdef TEST_X
  DoTestStep(0);
  return ;
#endif

#ifdef TEST_FORM
//  DoTestStep(1);
//  return
#endif
//#ifdef BATCH_TEST
//     DoTestStep();
//      return;
//#endif
  QString curFolder = mModel->filePath(ui.mFileView->rootIndex());
  editFile(curFolder);
}

void MainWindow::onDir(){
  QString s = QFileDialog::getExistingDirectory(this, tr("Выбрать каталог тестов"),
      CXSettings::valueStat(E_DirPath).toString());

  if(s != CXSettings::valueStat(E_DirPath).toString()){
    indexHistory.push_back(ui.mFileView->rootIndex());
    ui.mBackButton->setEnabled(true);
    ui.mFileView->setRootIndex(mModel->setRootPath(s));
    CXSettings::setValueStat(E_DirPath, s);
  }
}

void MainWindow::onLoopAll(){
  if(wasFault) {
    //wasFault = false;
    testTimer.stop();
    return ;
  }
  DoTestStep();
}

void MainWindow::executeFile(const QString& _aFileName)
{
  test_fname = _aFileName;

  //копим счетчик сбрасываем по очистить лог
#ifndef TEST_FORM
  if(permissionOfExecute)
    DXE::CTestScript::executeFile(
       test_fname, QString("%0").arg(++cur_test_id), dontwaitForExecute);
#endif

}

void CheckNeedRepaint(bool _err, QTreeWidgetItem* _item){
  if(NULL == _item) return;

  if((_item->text(0).indexOf(QString::fromUtf8("Сценарий"), 0) != -1) && _err){
    //это сценарий-перекрашиваем
    _item->setForeground(0, QBrush(Qt::red));
    _item->setForeground(1, QBrush(Qt::red));
  }else{
    //_item->setForeground(0, QBrush(Qt::black));
  }
}

QString GetResultStr(int _res){
  QString res = "";
  //создаем сообщение об отработке
  switch(_res){
    case ILogListener::msg:
      res = "";
    break;
    case ILogListener::started:
      res = QString::fromUtf8("Запущен");
    break;
    case ILogListener::pass:
      res = QString::fromUtf8("Выполнен успешно");
    break;
    case ILogListener::fail:
      res = QString::fromUtf8("Неудачно");
    break;
  }
  return res;
}

//void MainWindow::onLogMsg(const QString& _id, const QString& _message, int _result, int _fulldate) {
//  SMsg_log _msg(_id, _message, _result, _fulldate);
//  onLogMsg(_msg);
//}

void MainWindow::onLogMsg(const SMsg_log& _pmsg) {

  const SMsg_log& _msg = _pmsg;
  //return;
  //последний top-элемент
  QTreeWidgetItem* item = NULL, *child= NULL;
  //разбиваем ключ (вид 1.3.2)
  QStringList ids = _msg.id.split(".");
#ifdef TEST_X
  {
    static int counter = 0;
    if(_msg.fulldate){
      if(_msg.result == 2){
        counter ++;
        if(counter>5)
          forgetExecuteNewTest = true;

      }
      if(_msg.result == 0){
        counter --;
      }
      if(_msg.result == 1){
        //script fault
        forgetExecuteNewTest = true;
      }
    }
  }
#endif

  //собираем сообщение
  QString msg = QString("[") + QDateTime::currentDateTime().time().toString()
      + QString("]") + _msg.msg;
  //проверяем нужно ли отобразить полную дату
  if(_msg.fulldate){
    msg.insert(0, QString("--------------------------\n[")+QDateTime::currentDateTime().date().toString()+" ");
  }
  //нужно ли перекрашивать
  bool errPaint = (_msg.result == ILogListener::fail) ? true : false;

  QString result = GetResultStr(_msg.result);

  //ищем элемент
  if(ui.mLogView->topLevelItemCount() < ids[0].toInt()){
    //создаем элемент
    item = new QTreeWidgetItem(ui.mLogView);
    item->setText(0,"Noname");
    ui.mLogView->insertTopLevelItem(ids[0].toInt() - 1, item);
  }else{
    item = ui.mLogView->topLevelItem(ids[0].toInt() - 1);
    CheckNeedRepaint(errPaint, item);
  }
  //теперь работаем с потомками
  for(int i = 1; i < ids.size(); i++){
    if(item->childCount() < ids[i].toInt()){
      child = new QTreeWidgetItem(item);
      child->setText(0,"Noname");
      item->insertChild(ids[i].toInt() - 1, child);
    }else child = item->child(ids[i].toInt() - 1);
    item = child;
    CheckNeedRepaint(errPaint, item);
  };
  //теперь заполняем поля
  item->setText(0, msg);
  if(errPaint){
    item->setForeground(0, QBrush(Qt::red));
    item->setForeground(1, QBrush(Qt::red));
    wasFault = true;
  }
  if(result != "")
    item->setText(1,result);
  ui.mLogView->header()->setResizeMode(0, QHeaderView::ResizeToContents);
  //скроллируем вниз
  ui.mLogView->scrollToBottom();

//  delete _pmsg;
}


void MainWindow::onDrawVec(const QString& _fname, const QString& _fname_script, const QVector<QPair<double, double> >& _vec){
  LOG1(INFO, "form: Get vector, sz=%d", _vec.size());

  CXGraphic graphic;
  graphic.setAxisTitles(QString().fromUtf8("Частота, Гц"), QFileInfo(_fname).fileName());

  graphic.setAxisRange(200, 3600, 0, 0 );
  graphic.setSize({1200, 800});
  graphic.setData(_vec);
  graphic.getGraphic().save(_fname);
  QFile file(_fname+".txt");

  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    return;
  QTextStream ss(&file);
  ss << "Freq"<<"\t"<<"RMS"<<endl;
  for(auto item: _vec){
    ss << item.first<<"\t"<<item.second<<endl;
  }
}

void MainWindow::editFile(const QString& aFileName)
{
  setCursor(QCursor(Qt::WaitCursor));

	if (mEditWindow == NULL)
	{
		mEditWindow = new CXEditWindow;
		mEditWindow->setCursor(QCursor(Qt::WaitCursor));
		mEditWindow->setWindowModality(Qt::ApplicationModal);
		mEditWindow->setWindowState(Qt::WindowMaximized);
	}

	mEditWindow->setEditFile(aFileName);
	mEditWindow->show();
  setCursor(QCursor(Qt::ArrowCursor));
}

void MainWindow::resizeEvent(QResizeEvent *reszEv){
//  ui.mFileView->setColumnWidth(0, reszEv->size().width() * .60);
//  ui.mFileView->setColumnWidth(1, reszEv->size().width() * .07);
//  ui.mFileView->setColumnWidth(2, reszEv->size().width() * .07);
//  ui.mFileView->setColumnWidth(3, reszEv->size().width() * .25);
}

void MainWindow::onRunDlg(int _dlg, void* _param){
  if(_dlg == 0){
    CXRTPParamDlg dlg(this, _param);
    dlg.exec();
  }
}
