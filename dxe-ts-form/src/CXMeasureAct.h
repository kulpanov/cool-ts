#ifndef CXMEASUREACT_H
#define CXMEASUREACT_H

#include "CXBaseAct.h"

#include <QLineEdit>
#include <QCheckBox>
#include <QTimeEdit>

/*!
	Класс акта "Измерить".
*/
class CXMeasureAct : public CXBaseAct
{
  Q_OBJECT

public:
	CXMeasureAct(QWidget* parent = 0);
	~CXMeasureAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
	//QLineEdit* mIDEdit;
	QComboBox* mDirectionBox;
	QLineEdit* mMaxCoeffEdit;
	QLineEdit* mMinCoeffEdit;
	QTimeEdit* mDurationEdit;
	QCheckBox* mCheckKoef;
	QLineEdit* mLoopEdit;
  QLineEdit* mFileNameEdit;

	int mPause;
	int mTime;
private slots:
  void onCheckChange(int _check);
  void onOpenFile();
};

//
class CXAct_Plot : public CXBaseAct
{
  Q_OBJECT
public:
  CXAct_Plot(int _type);
  virtual ~CXAct_Plot();

  /*!
    Переопределенная функция загрузки акта по XML-объекту.

    \param aElement QDomElement акта.
  */
  virtual void load(const QDomElement& aElement);

  /*!
    Переопределенная функция получения XML-объекта акта.

    \return QDomElement с заполненными атрибутами.
  */
  virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private slots:
  void onOpenFile();
private:
  QComboBox* mDirectionBox;
  QLineEdit *mGenId;
  QComboBox* mGenSide;
  QLineEdit* mFileNameEdit;
  QLineEdit *mStep;
  QLineEdit *mFirst;
  QLineEdit *mLast;
  QLineEdit *mAmpl;
};


#endif // CXMEASUREACT_H
