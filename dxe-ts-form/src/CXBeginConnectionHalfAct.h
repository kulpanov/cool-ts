#ifndef CXBEGINCONNECTIONHALFACT_H
#define CXBEGINCONNECTIONHALFACT_H

#include "CXBaseAct.h"

#include <QLineEdit>

/*!
	Класс акта "Установить соединение с абонентом - полсоединения".
*/
class CXBeginConnectionHalfAct : public CXBaseAct
{
public:
	CXBeginConnectionHalfAct(QWidget* parent = 0);
	~CXBeginConnectionHalfAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
//	QLineEdit* mIDEdit;
	//QLineEdit* mGwNumberEdit;
	QLineEdit* mOutNumberEdit;
  //QLineEdit* mSrcEdit;
  QLineEdit* mTargetIpEdit;
};

#endif // CXBEGINCONNECTIONACT_H
