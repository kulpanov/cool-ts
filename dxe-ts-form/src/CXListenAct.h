#ifndef CXLISTENACT_H
#define CXLISTENACT_H

#include "CXBaseAct.h"

#include <QLineEdit>
#include <QSpinBox>

/*!
	Класс акта "Прослушать".
*/
class CXListenAct : public CXBaseAct
{
public:
	CXListenAct(QWidget* parent = 0);
	~CXListenAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
	//QLineEdit* mIDEdit;
	QComboBox* mDirectionBox;
};

/*!
  Класс акта "Остановить прослушку".
*/
class CXListenStopAct : public CXBaseAct
{
public:
  CXListenStopAct(QWidget* parent = 0);
  ~CXListenStopAct();

  /*!
    Переопределенная функция загрузки акта по XML-объекту.

    \param aElement QDomElement акта.
  */
  virtual void load(const QDomElement& aElement);

  /*!
    Переопределенная функция получения XML-объекта акта.

    \return QDomElement с заполненными атрибутами.
  */
  virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
  //QLineEdit* mIDEdit;
  QComboBox* mDirectionBox;
};

#endif // CXLISTENACT_H
