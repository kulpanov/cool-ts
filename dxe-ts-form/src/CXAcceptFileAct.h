#ifndef CXACCEPTFILEACT_H
#define CXACCEPTFILEACT_H

#include "CXBaseAct.h"

#include <QLineEdit>
#include <QSpinBox>

/*!
	Класс акта "Принять файл".
*/
class CXAcceptFileAct : public CXBaseAct
{
	Q_OBJECT

public:
	CXAcceptFileAct(QWidget* parent = 0);
	~CXAcceptFileAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private slots:
	void onOpenFile();

private:
	QComboBox* mDirectionBox;
	QLineEdit* mFileNameEdit;
};

class CXAcceptFileStopAct : public CXBaseAct
{
  Q_OBJECT

public:
  CXAcceptFileStopAct(QWidget* parent = 0);

  ~CXAcceptFileStopAct();

  /*!
    Переопределенная функция загрузки акта по XML-объекту.

    \param aElement QDomElement акта.
  */
  virtual void load(const QDomElement& aElement);

  /*!
    Переопределенная функция получения XML-объекта акта.

    \return QDomElement с заполненными атрибутами.
  */
  virtual QDomElement getXMLElement(QDomDocument& aDomDocument);
private:
  QComboBox* mDirectionBox;
};
#endif // CXACCEPTFILEACT_H
