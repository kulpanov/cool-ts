#include "CXMeasureAct.h"

#include <QLabel>
#include <QToolButton>
#include <QFileDialog>

CXMeasureAct::CXMeasureAct(QWidget* parent) : CXBaseAct(parent)
{
	mType = E_MeasureAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);
	//QVBoxLayout* vLayout = new QVBoxLayout(mContainer);

	{
	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

//  centralLayout->addWidget(new QLabel(tr("id"), mContainer));
//
//	mIDEdit = new QLineEdit(mContainer);
//	mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
//	centralLayout->addWidget(mIDEdit);

	centralLayout->addWidget(new QLabel(tr("на стороне"), mContainer));

	mDirectionBox = new QComboBox(mContainer);
	mDirectionBox->addItem(tr("Исходящего"));
	mDirectionBox->addItem(tr("Входящего"));
	centralLayout->addWidget(mDirectionBox);
  centralLayout->addWidget(new QLabel(tr("вызова,"), mContainer));

  mCheckKoef = new QCheckBox(mContainer);
  connect(mCheckKoef, SIGNAL(stateChanged(int)), this, SLOT(onCheckChange(int)));
  centralLayout->addWidget(mCheckKoef);
  centralLayout->addWidget(new QLabel(tr("проверить коэф. передачи:"), mContainer));

	centralLayout->addWidget(new QLabel(tr("мин."), mContainer));
	mMinCoeffEdit = new QLineEdit(mContainer);
	mMinCoeffEdit->setText("0");
	mMinCoeffEdit->setValidator(
	    new QRegExpValidator(QRegExp("^-?\\d+\\d+"), mMinCoeffEdit));
	mMinCoeffEdit->setMaximumWidth(30);
	centralLayout->addWidget(mMinCoeffEdit);
	centralLayout->addWidget(new QLabel(tr("дБ, "), mContainer));

	centralLayout->addWidget(new QLabel(tr("макс"), mContainer));
	mMaxCoeffEdit = new QLineEdit(mContainer);
	mMaxCoeffEdit->setText("0");
	mMaxCoeffEdit->setValidator(
	    new QRegExpValidator(QRegExp("^-?\\d+\\d+"), mMaxCoeffEdit));
	mMaxCoeffEdit->setMaximumWidth(30);
	centralLayout->addWidget(mMaxCoeffEdit);
	centralLayout->addWidget(new QLabel(tr("дБ"), mContainer));

	//по умолчанию
	onCheckChange(mCheckKoef->isChecked());
//  QLabel* caption = new QLabel(tr("В течении"), mContainer);
//  centralLayout->addWidget(caption);
//
//  mDurationEdit = new QTimeEdit(mContainer);
//  mDurationEdit->setTime(QTime().addSecs(10));
//  centralLayout->addWidget(mDurationEdit);

	centralLayout->addStretch();
//	vLayout->addLayout(centralLayout);
	}
//	{
//	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);
//	centralLayout->addWidget(new QLabel(tr("повторить "), mContainer));
//	centralLayout->addWidget(mLoopEdit = new QLineEdit(mContainer));
//  mLoopEdit->setValidator(new QRegExpValidator(QRegExp("^-?\\d+"), mContainer));
//  mLoopEdit->setMaxLength(3);
//  mLoopEdit->setText("0"); mLoopEdit->setMaximumWidth(30);
//  centralLayout->addWidget(new QLabel(tr("раз и записать в файл"), mContainer));
//
//  mFileNameEdit = new QLineEdit(mContainer);
//  //mFileNameEdit->setReadOnly(true);
//  centralLayout->addWidget(mFileNameEdit);
//
//  QToolButton* openButton = new QToolButton(mContainer);
//  openButton->setText("...");
//  centralLayout->addWidget(openButton);
//
//  connect(openButton, SIGNAL(clicked()), this, SLOT(onOpenFile()));
//
//	centralLayout->addStretch();
//  vLayout->addLayout(centralLayout);
//	}

	mTime = 100;
	}

CXMeasureAct::~CXMeasureAct()
{

}

void CXMeasureAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
	//mIDEdit->setText(aElement.attribute("id"));
	mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
	mMinCoeffEdit->setText(aElement.attribute("low"));
	mMaxCoeffEdit->setText(aElement.attribute("hi"));
	(aElement.attribute("check") == "1") ? mCheckKoef->setChecked(true)
                                       : mCheckKoef->setChecked(false);
 	mTime = 100;
}

QDomElement CXMeasureAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("test_koef"));
	data << SXXMLData("descr", tr("Измерить коэф передачи"));
	//data << SXXMLData("id", mIDEdit->text());
	data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));
	//data << SXXMLData("pause", QString::number(mPause));
	data << SXXMLData("mtime", QString::number(mTime));
	data << SXXMLData("hi", mMaxCoeffEdit->text());
	data << SXXMLData("low", mMinCoeffEdit->text());
	data << SXXMLData("check", mCheckKoef->isChecked() ? "1" : "0");
	//умолчание
	onCheckChange(mCheckKoef->isChecked());
//  QTime time = 100; //mDurationEdit->time();
//  data << SXXMLData("pause", QString::number(time.msec() + (time.second() + (time.minute() + time.hour() * 24) * 60) * 1000));

	return CXBaseAct::getXMLElement(aDomDocument, data);
}

void CXMeasureAct::onCheckChange(int _check){
  if(!mCheckKoef->isChecked()){
    mMinCoeffEdit->setEnabled(false);
    mMaxCoeffEdit->setEnabled(false);
  }else{
    mMinCoeffEdit->setEnabled(true);
    mMaxCoeffEdit->setEnabled(true);
  };
};

void CXMeasureAct::onOpenFile()
{
  QString testDir = "tmp";//CXSettings::valueStat(E_WavPath).toString().replace("\\", "/");
  QString fileName = QFileDialog::getSaveFileName(NULL, "", testDir, "jpg (*.jpg)");

  if (!fileName.isEmpty())
  {
    if (testDir.at(testDir.length() - 1) != '/') testDir += "/";
    mFileNameEdit->setText(fileName.replace("\\", "/"));//.replace(testDir, ""));
  }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
///
CXAct_Plot::CXAct_Plot(int _type): CXBaseAct(NULL)
{
  mType = _type==E_PlotRMS?E_PlotRMS:E_PlotRatio;
  mActTypeBox->blockSignals(true);
  mActTypeBox->setCurrentIndex(mType);
  mActTypeBox->blockSignals(false);

  mIDEdit->setEnabled(true);
  QVBoxLayout* vLayout = new QVBoxLayout(mContainer);
  {
    QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);
    centralLayout->addWidget(new QLabel(tr("на стороне"), mContainer));
    mDirectionBox = new QComboBox(mContainer);
    mDirectionBox->addItem(tr("Исходящего"));
    mDirectionBox->addItem(tr("Входящего"));
    centralLayout->addWidget(mDirectionBox);

    centralLayout->addWidget(new QLabel(tr("вызова и записать в файл"), mContainer));
    mFileNameEdit = new QLineEdit(mContainer);
    centralLayout->addWidget(mFileNameEdit);
    QToolButton* openButton = new QToolButton(mContainer);
    openButton->setText("...");
    centralLayout->addWidget(openButton);
    connect(openButton, SIGNAL(clicked()), this, SLOT(onOpenFile()));

    centralLayout->addStretch();
    vLayout->addLayout(centralLayout);
  }
  {
    QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);
//    centralLayout->addWidget(new QLabel(tr(",cтроить"), mContainer));
//    mTypeMeasuresBox = new QComboBox(mContainer);
//    mTypeMeasuresBox->addItem(tr("Уровень RMS"));
//    mTypeMeasuresBox->addItem(tr("Коэф передачи"));
//    centralLayout->addWidget(mTypeMeasuresBox);
    centralLayout->addWidget(new QLabel(tr("подключить генератор на номер соед:"), mContainer));
    mGenId = new QLineEdit(mContainer);
    mGenId->setValidator(new QRegExpValidator(QRegExp("\\d+"), mGenId));
    mGenId->setMaximumWidth(35);
    mGenId->setMinimumWidth(35);
    mGenId->setPlaceholderText("auto");

    centralLayout->addWidget(mGenId);
    centralLayout->addWidget(new QLabel(tr("со стороны:"), mContainer));
    mGenSide= new QComboBox(mContainer);
    mGenSide->addItem(tr("Исходящего"));
    mGenSide->addItem(tr("Входящего"));
    centralLayout->addWidget(mGenSide);

    centralLayout->addWidget(new QLabel(tr("уровень(дБ):"), mContainer));
    mAmpl= new QLineEdit(mContainer);
    mAmpl->setValidator(new QRegExpValidator(QRegExp("^-?\\d+\\d+"), mAmpl));
    mAmpl->setMaximumWidth(35);
    mAmpl->setMinimumWidth(35);
    centralLayout->addWidget(mAmpl);

    centralLayout->addWidget(new QLabel(tr(", частота(Гц) от:"), mContainer));
    mFirst = new QLineEdit(mContainer);
    mFirst->setValidator(new QRegExpValidator(QRegExp("\\d+"), mFirst));
    mFirst->setMaximumWidth(35);
    mFirst->setMinimumWidth(35);
    centralLayout->addWidget(mFirst);

    centralLayout->addWidget(new QLabel(tr("до:"), mContainer));
    mLast = new QLineEdit(mContainer);
    mLast->setValidator(new QRegExpValidator(QRegExp("\\d+"), mLast));
    mLast->setMaximumWidth(35);
    mLast->setMinimumWidth(35);
    centralLayout->addWidget(mLast);

    centralLayout->addWidget(new QLabel(tr("c шагом:"), mContainer));
    mStep = new QLineEdit(mContainer);
    mStep->setValidator(new QRegExpValidator(QRegExp("\\d+"), mStep));
    mStep->setMaximumWidth(35);
    mStep->setMinimumWidth(35);
    centralLayout->addWidget(mStep);

    centralLayout->addStretch();
    vLayout->addLayout(centralLayout);
  }

}

CXAct_Plot::~CXAct_Plot(){

}

void CXAct_Plot::load(const QDomElement& aElement){
  CXBaseAct::load(aElement);
  mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
  mGenId->setText(aElement.attribute("genid"));
  mGenSide->setCurrentIndex(aElement.attribute("genside").toInt());
  mFileNameEdit->setText(aElement.attribute("fname"));
  mStep->setText(aElement.attribute("step"));
  mFirst->setText(aElement.attribute("first"));
  mLast->setText(aElement.attribute("last"));
  mAmpl->setText(aElement.attribute("ampl"));
}


QString checkValue(QLineEdit* w, int min, int max) {
  int value; //QString res;
  value = w->text().toInt();
  if(value <min){
    value = min;
    //w->setText(res = QString().number(value));
  }else
  if(value >max){
    value = max;
    //w->setText(res= QString().number(value));
  }
  return QString().number(value);
}

QDomElement CXAct_Plot::getXMLElement(QDomDocument& aDomDocument){
  QList <SXXMLData> data;

  data << SXXMLData("name", mType==E_PlotRMS? tr("plot_rms"): tr("plot_ratio"));
  data << SXXMLData("descr", tr("Отрисовать АЧХ"));
  data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));
  data << SXXMLData("genside", QString::number(mGenSide->currentIndex()));
  data << SXXMLData("genid", mGenId->text());
  data << SXXMLData("step", checkValue(mStep, 10, 1000));
  data << SXXMLData("first", checkValue(mFirst, 299, 3500));
  data << SXXMLData("last", checkValue(mLast, 299, 3500));
  data << SXXMLData("fname", mFileNameEdit->text());
  data << SXXMLData("ampl", checkValue(mAmpl, -60, 3));

  return CXBaseAct::getXMLElement(aDomDocument, data);
}

void CXAct_Plot::onOpenFile()
{
  QString testDir = "tmp";//CXSettings::valueStat(E_WavPath).toString().replace("\\", "/");
  QString fileName = QFileDialog::getSaveFileName(NULL, "", testDir, "png (*.png)");

  if (!fileName.isEmpty())
  {
    if (testDir.at(testDir.length() - 1) != '/') testDir += "/";
    mFileNameEdit->setText(fileName.replace("\\", "/"));//.replace(testDir, ""));
  }
}
