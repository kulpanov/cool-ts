#include "AXAbstractAct.h"

#include "CXActsManager.h"

CXActsManager* AXAbstractAct::mActsManager = NULL;

AXAbstractAct::AXAbstractAct(QWidget *parent) : QFrame(parent)
{
	mType = E_NoneAct;

//	setAutoFillBackground(true);
	setFrameShape(QFrame::StyledPanel);
	setFrameShadow(QFrame::Plain);

	setContentsMargins(2, 2, 2, 2);
}

AXAbstractAct::~AXAbstractAct()
{

}

eActTypes AXAbstractAct::type()
{
	return mType;
}

void AXAbstractAct::setManager(CXActsManager* aActsManager)
{
	mActsManager = aActsManager;
}
