#include <QLabel>

#include "CXAbsMeasureAct.h"

CXAbsMeasureAct::CXAbsMeasureAct(QWidget* parent) : CXBaseAct(parent)
{
	mType = E_AbsMeasureAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);

	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);

//  centralLayout->addWidget(new QLabel(tr("id"), mContainer));
//
//	mIDEdit = new QLineEdit(mContainer);
//	mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
//	centralLayout->addWidget(mIDEdit);

	centralLayout->addWidget(new QLabel(tr("на стороне"), mContainer));

	mDirectionBox = new QComboBox(mContainer);
	mDirectionBox->addItem(tr("Исходящего"));
	mDirectionBox->addItem(tr("Входящего"));
	centralLayout->addWidget(mDirectionBox);
  centralLayout->addWidget(new QLabel(tr("вызова,"), mContainer));

  mCheckKoef = new QCheckBox(mContainer);
  connect(mCheckKoef, SIGNAL(stateChanged(int)), this, SLOT(onCheckChange(int)));
  centralLayout->addWidget(mCheckKoef);
  centralLayout->addWidget(new QLabel(tr("проверить уровень(rms):"), mContainer));

	centralLayout->addWidget(new QLabel(tr("мин."), mContainer));
	mMinCoeffEdit = new QLineEdit(mContainer);
	mMinCoeffEdit->setText("0");
	mMinCoeffEdit->setValidator(
	    new QRegExpValidator(QRegExp("^-?\\d+\\d+"), mMinCoeffEdit));
	centralLayout->addWidget(mMinCoeffEdit);
	mMinCoeffEdit->setMaximumWidth(30);
	centralLayout->addWidget(new QLabel(tr("дБ, "), mContainer));

	centralLayout->addWidget(new QLabel(tr("макс"), mContainer));
	mMaxCoeffEdit = new QLineEdit(mContainer);
	mMaxCoeffEdit->setText("0");
	mMaxCoeffEdit->setValidator(
	    new QRegExpValidator(QRegExp("^-?\\d+\\d+"), mMaxCoeffEdit));
	mMaxCoeffEdit->setMaximumWidth(30);
	centralLayout->addWidget(mMaxCoeffEdit);
	centralLayout->addWidget(new QLabel(tr("дБ"), mContainer));

	//по умолчанию
	onCheckChange(mCheckKoef->isChecked());
//  QLabel* caption = new QLabel(tr("В течении"), mContainer);
//  centralLayout->addWidget(caption);
//
//  mDurationEdit = new QTimeEdit(mContainer);
//  mDurationEdit->setTime(QTime().addSecs(10));
//  centralLayout->addWidget(mDurationEdit);

	centralLayout->addStretch();
	mTime = 100;
	}

CXAbsMeasureAct::~CXAbsMeasureAct()
{

}

void CXAbsMeasureAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
  //mIDEdit->setText(aElement.attribute("id"));
	mDirectionBox->setCurrentIndex(aElement.attribute("dir").toInt());
	mMinCoeffEdit->setText(aElement.attribute("low"));
	mMaxCoeffEdit->setText(aElement.attribute("hi"));
	(aElement.attribute("check") == "1") ? mCheckKoef->setChecked(true)
                                       : mCheckKoef->setChecked(false);
 	mTime = 100;
}

QDomElement CXAbsMeasureAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("test_abs"));
	data << SXXMLData("descr", tr("Измерить уровень(rms)"));
	//data << SXXMLData("id", mIDEdit->text());
	data << SXXMLData("dir", QString::number(mDirectionBox->currentIndex()));
	//data << SXXMLData("pause", QString::number(mPause));
	data << SXXMLData("mtime", QString::number(mTime));
	data << SXXMLData("hi", mMaxCoeffEdit->text());
	data << SXXMLData("low", mMinCoeffEdit->text());
	data << SXXMLData("check", mCheckKoef->isChecked() ? "1" : "0");
	//умолчание
	onCheckChange(mCheckKoef->isChecked());
//  QTime time = 100; //mDurationEdit->time();
//  data << SXXMLData("pause", QString::number(time.msec() + (time.second() + (time.minute() + time.hour() * 24) * 60) * 1000));

	return CXBaseAct::getXMLElement(aDomDocument, data);
}

void CXAbsMeasureAct::onCheckChange(int _check){
  if(!mCheckKoef->isChecked()){
    mMinCoeffEdit->setEnabled(false);
    mMaxCoeffEdit->setEnabled(false);
  }else{
    mMinCoeffEdit->setEnabled(true);
    mMaxCoeffEdit->setEnabled(true);
  };
};
