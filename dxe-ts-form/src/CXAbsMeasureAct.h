#ifndef CXABSMEASUREACT_H
#define CXABSMEASUREACT_H

#include "CXBaseAct.h"

#include <QLineEdit>
#include <QCheckBox>
#include <QTimeEdit>

/*!
	Класс акта "Измерить".
*/
class CXAbsMeasureAct : public CXBaseAct
{
  Q_OBJECT

public:
	CXAbsMeasureAct(QWidget* parent = 0);
	~CXAbsMeasureAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
	//QLineEdit* mIDEdit;
	QComboBox* mDirectionBox;
	QLineEdit* mMaxCoeffEdit;
	QLineEdit* mMinCoeffEdit;
	QTimeEdit* mDurationEdit;
	QCheckBox* mCheckKoef;

	int mPause;
	int mTime;
private slots:
  void onCheckChange(int _check);
};

#endif // CXABSMEASUREACT_H
