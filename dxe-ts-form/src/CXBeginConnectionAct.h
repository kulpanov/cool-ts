#ifndef CXBEGINCONNECTIONACT_H
#define CXBEGINCONNECTIONACT_H

#include "CXBaseAct.h"

#include <QLineEdit>

/*!
	Класс акта "Установить соединение".
*/
class CXBeginConnectionAct : public CXBaseAct
{
public:
	CXBeginConnectionAct(QWidget* parent = 0);
	~CXBeginConnectionAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
//	QLineEdit* mIDEdit;
	QLineEdit* mGwNumberEdit;
	QLineEdit* mOutNumberEdit;
  QLineEdit* mSrcEdit;
  QLineEdit* mTargetIpEdit;
};

#endif // CXBEGINCONNECTIONACT_H
