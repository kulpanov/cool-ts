#ifndef CXGENERATORACT_H
#define CXGENERATORACT_H

#include "CXBaseAct.h"

#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QLabel>

/*!
	Класс акта "Подключить генератор".
*/
class CXGeneratorAct : public CXBaseAct
{
  Q_OBJECT

public:
	CXGeneratorAct(QWidget* parent = 0);
	~CXGeneratorAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
	QComboBox* mDirectionBox;
	QSpinBox* mFrequencyEdit;
	QLineEdit* mAmplitudeEdit;
	QCheckBox* silenceCheck;
	QLabel* silenceEdit;
  QPushButton* mHelperButton;

private slots:
  void onCheckChange(int _check);
  void onHelperWindow();
};

#endif // CXGENERATORACT_H
