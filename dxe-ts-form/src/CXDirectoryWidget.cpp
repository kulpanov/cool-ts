#include "CXDirectoryWidget.h"

#include <QHBoxLayout>
#include <QToolButton>
#include <QFileDialog>

CXDirectoryWidget::CXDirectoryWidget(QWidget* parent) : QWidget(parent)
{
	QHBoxLayout* centralLayout = new QHBoxLayout(this);
	centralLayout->setMargin(0);
	centralLayout->setSpacing(5);

	mPathEdit = new QLineEdit(this);
	centralLayout->addWidget(mPathEdit);

	QToolButton* btn = new QToolButton(this);
	btn->setMinimumSize(22, 22);
	btn->setText("...");
	centralLayout->addWidget(btn);

	connect(btn, SIGNAL(clicked()), this, SLOT(onShowDialog()));
	connect(mPathEdit, SIGNAL(editingFinished()), this, SLOT(onEditFinish()));
}

CXDirectoryWidget::~CXDirectoryWidget()
{

}

QString CXDirectoryWidget::path()
{
	return mPath;
}

void CXDirectoryWidget::setPath(const QString& aPath)
{
	if (QDir(aPath).exists())
	{
		mPath = aPath;
		mPathEdit->setText(mPath);
	}
}

void CXDirectoryWidget::onShowDialog()
{
	QString path = QFileDialog::getExistingDirectory(this, "", mPathEdit->text());

	if (!path.isEmpty())
	{
		mPath = path;
		mPathEdit->setText(mPath);
	}
}

void CXDirectoryWidget::onEditFinish()
{
	if (!QDir(mPathEdit->text()).exists())
	{
		mPathEdit->setText(mPath);
		return;
	}

	mPath = mPathEdit->text();
}
