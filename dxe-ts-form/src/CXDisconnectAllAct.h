#ifndef CXDISCONNECTALLACT_H
#define CXDISCONNECTALLACT_H

#include "CXBaseAct.h"

#include <QLabel>

/*!
	Класс акта "Разъединить всех".
*/
class CXDisconnectAllAct : public CXBaseAct
{
public:
	CXDisconnectAllAct(QWidget* parent = 0);
	~CXDisconnectAllAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);
};

#endif // CXDISCONNECTALLACT_H
