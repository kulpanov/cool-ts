#include "CXDisconnectAct.h"

#include <QLabel>

CXDisconnectAct::CXDisconnectAct(QWidget* parent) : CXBaseAct(parent)
{
	mType = E_DisconnectAct;
	mActTypeBox->blockSignals(true);
	mActTypeBox->setCurrentIndex(mType);
	mActTypeBox->blockSignals(false);
	QHBoxLayout* centralLayout = new QHBoxLayout(mContainer);
	//	centralLayout->addWidget(new QLabel(tr("id"), mContainer));
//
//	mIDEdit = new QLineEdit(mContainer);
//	mIDEdit->setValidator(new QRegExpValidator(QRegExp("\\d+"), mIDEdit));
//	centralLayout->addWidget(mIDEdit);

	centralLayout->addStretch();
}

CXDisconnectAct::~CXDisconnectAct()
{

}

void CXDisconnectAct::load(const QDomElement& aElement)
{
  CXBaseAct::load(aElement);
  //mIDEdit->setText(aElement.attribute("id"));
}

QDomElement CXDisconnectAct::getXMLElement(QDomDocument& aDomDocument)
{
	QList <SXXMLData> data;

	data << SXXMLData("name", tr("disconnect"));
	data << SXXMLData("descr", tr("Прервать связь"));
	//data << SXXMLData("id", mIDEdit->text());

	return CXBaseAct::getXMLElement(aDomDocument, data);
}
