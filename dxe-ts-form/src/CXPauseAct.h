#ifndef CXPAUSEACT_H
#define CXPAUSEACT_H

#include "CXBaseAct.h"

#include <QLabel>
#include <QTimeEdit>

/*!
	Класс акта "Пауза".
*/
class CXPauseAct : public CXBaseAct
{
public:
	CXPauseAct(QWidget* parent = 0);
	~CXPauseAct();

	/*!
		Переопределенная функция загрузки акта по XML-объекту.

		\param aElement QDomElement акта.
	*/
	virtual void load(const QDomElement& aElement);

	/*!
		Переопределенная функция получения XML-объекта акта.

		\return QDomElement с заполненными атрибутами.
	*/
	virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
	//QLabel* mFileName;
	QTimeEdit* mDurationEdit;
	QLineEdit* mCommentEdit;
};

//
class CXAct_DTMF : public CXBaseAct
{
public:
  CXAct_DTMF(QWidget* parent = 0);
  virtual ~CXAct_DTMF();

  /*!
    Переопределенная функция загрузки акта по XML-объекту.

    \param aElement QDomElement акта.
  */
  virtual void load(const QDomElement& aElement);

  /*!
    Переопределенная функция получения XML-объекта акта.

    \return QDomElement с заполненными атрибутами.
  */
  virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
  QLineEdit* mAmplitude;
  QLineEdit* mDuration;
  QLineEdit* mPause;
};

//
class CXAct_RTP : public CXBaseAct
{
public:
  CXAct_RTP(QWidget* parent = 0);
  virtual ~CXAct_RTP();

  /*!
    Переопределенная функция загрузки акта по XML-объекту.

    \param aElement QDomElement акта.
  */
  virtual void load(const QDomElement& aElement);

  /*!
    Переопределенная функция получения XML-объекта акта.

    \return QDomElement с заполненными атрибутами.
  */
  virtual QDomElement getXMLElement(QDomDocument& aDomDocument);

private:
};

#endif // CXPAUSEACT_H
