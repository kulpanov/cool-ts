﻿/** \brief Объявления классов-сигналов SDL-системы.
 * Исходящие и входящие сигналы.
 *   sdl_signals.h
 *
 * Created on: 5.12.2010
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#pragma once

#ifdef BUILDING_DLL
#define DLL_DECL __declspec(dllexport)
#else
#define DLL_DECL __declspec(dllimport)
#endif

#include <string>
#include <pthread.h>
#include <inttypes.h>
/** \brief Пространство имён SDL
 * Все описания и реализации касаемые работы с SDL-системой
 * принадлежат этому пространству имён
 */
namespace SDL {

///тип октет, аналог SDL::octet
typedef uint8_t tOctet;


////////////////////////////////////////////////////////////////////////////////
//Вспомогательные макросы объявлений классов сигналов, пример см по коду

///для входящих сигналов
#define DECLARE_SDL_SIGNALIN_HEAD(name)\
  class DLL_DECL CSDL_Signal_##name: public ISDL_Signal {\
    typedef ISDL_Signal base;\
    public:\
      static  const char* sign();\
      virtual const char* type() const{return sign();}\
      virtual const uint32_t index() const;\
      virtual const uint32_t sizeOfData()const;\
      virtual const std::string& toString(std::string& str)const;\
      virtual void* GetSignal(void* _sdl_sig)const;\

///Для исходящих сигналов
#define DECLARE_SDL_SIGNALOUT_HEAD(name)\
class DLL_DECL CSDL_Signal_##name: public ISDL_Signal {\
  typedef ISDL_Signal base;\
  public:\
    static  const char* sign();\
    virtual const char* type() const{return sign();}\
    virtual const uint32_t index() const;\
    virtual const uint32_t sizeOfData()const;\
    virtual const std::string& toString(std::string& str)const;\
    virtual void* GetSignal(void* _sdl_sig)const;\
    virtual ISDL_Signal* Clone(void* _sdl_sig)const\
    { return new CSDL_Signal_##name(GetSignal(_sdl_sig));  }\
  public:\
    explicit CSDL_Signal_##name(void* _sdl_sig):base(_sdl_sig) { }\
    explicit CSDL_Signal_##name(){ }
///Завершающий макрос объявления
#define DECLARE_SDL_SIGNAL_FOOT() }

//#define APPEND_MACRO(M1, M2) M1##M2

////////////////////////////////////////////////////////////////////////////////
//Вспомогательные макросы реализации классов сигналов

///для исходящих сигналов
#define IMPL_SDL_SIGNAL_HEAD(name)\
  const char* CSDL_Signal_##name::sign() {return name->Name; }\
  const uint32_t CSDL_Signal_##name::index() const{ return (uint32_t)name; }\
  const uint32_t CSDL_Signal_##name::sizeOfData()const\
  {return (sizeof(yPDef_##name) - sizeof(xSignalStruct)); }

///для входящих сигналов
#define IMPL_SDL_SIGNALIN_HEAD(name, firstParam)\
  void* CSDL_Signal_##name::GetSignal(void* _notused)const{\
    yPDef_##name* signal = (yPDef_##name*)xGetSignal(name, sdl_signal->Receiver, xEnv);\
    memcpy(& (signal->firstParam),& (sdl_signal->firstParam), sizeOfData());\
    return signal;\
  }

///для выходящих сигналов
#define IMPL_SDL_SIGNALOUT_HEAD(name, firstParam)\
void* CSDL_Signal_##name::GetSignal(void* _sdl_sig)const{\
  yPDef_##name* signal = (yPDef_##name*)malloc(sizeof(yPDef_##name));\
  memcpy(& (signal->firstParam), &( ((yPDef_##name*)_sdl_sig)->firstParam), sizeOfData());\
  return signal;\
}

////////////////////////////////////////////////////////////////////////////////

/** \brief Базовый класс сигнала.
 * Виртуальный класс, наследники переписывают функциональность
 */
class DLL_DECL CSDL_Signal {
protected:
  ///Указатель на sdl сигнал, структура типа xSignalStruct
  void* sdl_sig;
public:
  //inline static  const char* sign(); - нету, базовый класс
  ///тип, строковое представление
  virtual const char* type() const = 0;
  ///индекс, для индексирования
  virtual const uint32_t index() const = 0;
  ///размер пользовательских данных, системные не включены
  virtual const uint32_t sizeOfData()const = 0;
  ///получить сигнал, как правило сделав копию
  virtual void* GetSignal(void* _notused)const = 0;
  ///Клонировать объект, создав по типу
  virtual CSDL_Signal* Clone(void* sdl_sig)const {return NULL;};
  ///Задать Pid
  virtual void SetReceiver(void* _pid);
  ///Получить pid
  virtual void* GetSender();
public:
  ///Проверить сооствествие типа, вариант с строковой подписью
  inline bool isMatched(const char* _sign)const{ return (0==strcmp(_sign, type()));}
  ///Проверить сооствествие типа, вариант с индексом
  inline bool isMatched(const uint32_t _id)const{ return (_id == index());}

protected:
  //закрытые конструктора
  explicit CSDL_Signal(void* _sdl_sig):sdl_sig(_sdl_sig){  }
  explicit CSDL_Signal():sdl_sig(NULL){  }

public:
  virtual ~CSDL_Signal()
  { free((void*)sdl_sig); }

public:
  ///В строку, для средств отладки и отображения
  virtual const std::string& toString(std::string& str)const = 0;
public:
  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    std::string str;
    toString(str);
    printf("SDL: recv:%s\n", (str).c_str());
    return;
  }

};

///класс можно назвать интерфейсом
typedef CSDL_Signal ISDL_Signal;
//
//////////////////////////////////////////////////////////////////////////////////
//
/** \brief Сигнал инициализации, входной, интерфейс к InitIP.
 * Инициализирует систему
 * и задаёт IP и MAC адреса системе.
 */
class DLL_DECL CSDL_Signal_InitIP: public ISDL_Signal {
  typedef ISDL_Signal base;
protected:
  int routeCount;
public:
  //согласно предка
  static  const char* sign();
  virtual const char* type() const{return sign();}
  virtual const uint32_t index() const;
  virtual const uint32_t sizeOfData()const;
  virtual              void* GetSignal(void* _notused)const;

//  static CSDL_Signal_InitIP* Create
//          (uint8_t _myIP[4], uint8_t _myNET[4], uint8_t _myVoIPNAT[4]);
public:
  /**Добавить запись в таблицу маршрутизации.
   * @param _IP
   * @param _NET
   * @param _VoIPNAT   */
  void AddToRoute(uint32_t _destIP, uint32_t _sIP, uint32_t _natVoIP);

  /** Конструктор.
   * @param _myIP IP-адрес системы
   * @param _myMAC MAC-адрес системы   */
  explicit CSDL_Signal_InitIP(uint32_t _myIP, uint32_t _myNET, uint32_t _myVoIPNAT);

public:
  ///
  virtual const std::string& toString(std::string& str)const;
 };
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
/** \brief Сигнал инициализации, входной, интерфейс к InitIP.
 * Инициализирует систему
 * и задаёт IP и MAC адреса системе.
 */
class DLL_DECL CSDL_Signal_InitRTPPort: public ISDL_Signal {
  typedef ISDL_Signal base;
public:
  //согласно предка
  static  const char* sign();
  virtual const char* type() const{return sign();}
  virtual const uint32_t index() const;
  virtual const uint32_t sizeOfData()const;
  virtual              void* GetSignal(void* _notused)const;

public:
  /** Конструктор.
   * @param _port first RTP port
   */
  explicit CSDL_Signal_InitRTPPort(uint32_t _port);

public:
  ///
  virtual const std::string& toString(std::string& str)const;
 };
//////////////////////////////////////////////////////////////////////////////////
/** \brief Ответ на  запрос инициализации модуля IPCOM.
 * Сигнал подтверждения инициализации, выходной, интерфейс к InitIPCOM_RQ.
 */
DECLARE_SDL_SIGNALOUT_HEAD(InitIPCOM_RQ)
  public:
    ///Получить нижнюю границу области номеров UDP-портов для RTP-пакетов.
    const uint16_t getLowerBoundaryOfPortsOfRTP() const;
    ///получить IP
    const uint32_t getMyIP() const;
    ///
    const uint32_t getMyNET() const;
DECLARE_SDL_SIGNAL_FOOT();

////////////////////////////////////////////////////////////////////////////////
/** \brief Подтверждение инициализации модуля IPCOM.
 * Входной. */
DECLARE_SDL_SIGNALIN_HEAD(InitIPCOM_I)
  explicit CSDL_Signal_InitIPCOM_I();
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Ошибка инициализации модуля IPCOM.
 * Выходной. */
DECLARE_SDL_SIGNALIN_HEAD(Err_IPCOM)
  const uint16_t getErr() const;
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Входящий UDP.
 * Входной.
 */
DECLARE_SDL_SIGNALIN_HEAD(UDPDI_long)
  typedef struct {
      uint8_t  Param1[4];
      uint32_t  Param2;
      uint32_t  Param3;
      uint32_t  Param4;
      uint8_t  Param5[1372];
  } SUDPDI_long_data;

  SUDPDI_long_data* UDPDI_long_data;
  ///
  const uint32_t getIP() const;
 /** Конструктор.
  * @param _dIP IP-адрес источника,
  * @param _sPort sourcePort
  * @param _dPort destPort
  * @param _len length of data
  * @param _upd_data the data  */
  explicit CSDL_Signal_UDPDI_long(uint32_t _sIP, uint16_t _sPort, uint16_t _dPort, uint32_t _len, uint8_t* _upd_data);
  //virtual ~CSDL_Signal_UDPDI_long();
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Исходящий UDP.
 * Выходной.
 */
DECLARE_SDL_SIGNALOUT_HEAD(UDPDR_long)

  ///получить source IP
  const uint32_t getIP() const;
  ///Получить source port
  const uint16_t getSPort() const;
  ///Получить dest port
  const uint16_t getDPort() const;
  ///Получить длину данных
  const uint32_t getLengthOfData() const;
  ///Получить данные
  const uint8_t* getData() const;
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Исходящий Err.
 * Диагностический массив. Выходной. */
DECLARE_SDL_SIGNALOUT_HEAD(Err)
  ///Получить данные
  const uint8_t* getErr() const;
  ///len
  const uint32_t getLenOfData() const;
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Исходящий Err.
 * Диагностический массив. Выходной. */
DECLARE_SDL_SIGNALOUT_HEAD(vms_err)
  ///Получить данные
  const uint8_t* getErr() const;
  ///len
  const uint32_t getLenOfData() const;
DECLARE_SDL_SIGNAL_FOOT();
//////////////////////////////////////////////////////////////////////////////////
/** \brief Сигнал записи в таблицу Sender_Table процесса IPCOM.
 * Выходной.
Параметры:
- номер строки таблицы Sender_Table,
- MAC-адрес назначения для отправки IP-пакетов,
- IP-адрес назначения,
- UDP-порт источника (Source Port),
- UDP-порт назначения (Destination Port),
- SSRC,
- длина звуковых данных, передаваемых в RTP-пакете,
  единица равна 0,125 мс (для G.729 должна быть кратна длине кадра - 10 мс),
- аудио-кодек:  1 - G.711 A-law,
                        6 - G.729A
 * */
DECLARE_SDL_SIGNALOUT_HEAD(wr_Sender_Table)
  //Получить данные
  const uint32_t getRowOfTable() const;
  const uint8_t* getDestMAC() const;
  const uint32_t getDestIP() const;
  const uint16_t getDestPort() const;
  const uint16_t getSrcPort() const;
  const uint8_t* getSSRC() const;
  const uint32_t getLenOfRTPFrame() const;
  const uint8_t getCodec() const;
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Сигнал записи в таблицу wr_Receiver_Table процесса IPCOM.
 * Выходной.
Параметры:
- номер строки таблицы Sender_Table,
- MAC-адрес назначения для отправки IP-пакетов,
- IP-адрес назначения,
- UDP-порт источника (Source Port),
- UDP-порт назначения (Destination Port),
- SSRC,
- длина звуковых данных, передаваемых в RTP-пакете,
  единица равна 0,125 мс (для G.729 должна быть кратна длине кадра - 10 мс),
- аудио-кодек:  1 - G.711 A-law,
                        6 - G.729A
 * */
DECLARE_SDL_SIGNALOUT_HEAD(wr_Receiver_Table)
  //Получить данные
  const uint32_t getRowOfTable() const;
  const uint8_t* getDestMAC() const;
  const uint16_t getDestPort() const;
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Сигнал входящий вызов.
 * Запрос на установку соединения. Выходной.
Параметры:
- getId- id соединения,
- getDigitsN - номер абонента
- getDigitsD - доп номер сервиса
- getSrcIP - IP-адрес источника вызова
 * */
DECLARE_SDL_SIGNALOUT_HEAD(vms_setup_env)
  public:
    //Получить данные
    const uint8_t getId() const;
    std::string getDigitsN() const;
    std::string getDigitsD() const;
    const uint32_t getSrcIP() const;

    virtual ~CSDL_Signal_vms_setup_env();
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Сигнал ответ с номером соед на исх вызов.
 * Ответ на установку соединения. Выходной.
Параметры:
- getId- id соединения,
- getDigitsN - номер абонента
- getDigitsD - доп номер сервиса
- getSrcIP - IP-адрес источника вызова
 * */
DECLARE_SDL_SIGNALOUT_HEAD(vms_setup_res)
  public:
    //Получить данные
    const uint8_t getId() const;
    std::string getDigitsN() const;
    std::string getDigitsD() const;
    const uint32_t getSrcIP() const;
    virtual ~CSDL_Signal_vms_setup_res();
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** \brief Сигнал исходящий вызов.
 * Запрос на установку соединения. Входной.
Параметры:
- getId- id соединения,
- getDigitsN - номер абонента
- getDigitsD - доп номер сервиса
- getDstIP - IP-адрес назначения вызова
 * */
//DECLARE_SDL_SIGNALIN_HEAD(vms_setup_env_in)
class DLL_DECL CSDL_Signal_vms_setup_env_in: public CSDL_Signal_vms_setup_env {
    typedef CSDL_Signal_vms_setup_env base;
    public:
      virtual void* GetSignal(void* _sdl_sig)const;

      explicit CSDL_Signal_vms_setup_env_in(uint8_t _id,
          const std::string& _digi_n, const std::string& _digi_d, uint32_t _dsst_ip);
};

//////////////////////////////////////////////////////////////////////////////////
/** Сигнал закончить соединение
 * Выходной.
Параметры:
- getId- id соединения,
 * */
DECLARE_SDL_SIGNALOUT_HEAD(vms_release_env)
  //Получить данные
  const uint8_t getId() const;
DECLARE_SDL_SIGNAL_FOOT();

class DLL_DECL CSDL_Signal_vms_release_env_in: public CSDL_Signal_vms_release_env {
    typedef CSDL_Signal_vms_release_env base;
    public:
      virtual void* GetSignal(void* _sdl_sig)const;

      explicit CSDL_Signal_vms_release_env_in(uint8_t _id);
};

//////////////////////////////////////////////////////////////////////////////////
/** Сигнал разрешить входящее соединение
 * Выходной.
Параметры:
- getId- id соединения,
 * */
DECLARE_SDL_SIGNALOUT_HEAD(vms_connect_env)
  ///
  const uint8_t getId() const;
DECLARE_SDL_SIGNAL_FOOT();

class DLL_DECL CSDL_Signal_vms_connect_env_in: public CSDL_Signal_vms_connect_env {
    typedef CSDL_Signal_vms_connect_env base;
    public:
      virtual void* GetSignal(void* _sdl_sig)const;

      explicit CSDL_Signal_vms_connect_env_in(uint8_t _id);
};

//////////////////////////////////////////////////////////////////////////////////
/** \brief SIP-сигнал INFO.
 * Обработка SIP-сигнала info.
 * Выходной. */
DECLARE_SDL_SIGNALOUT_HEAD(vms_info_env)
  ///Получить id соединения
  const uint8_t getId() const;
  ///Получить info доп набора кнопок
  const uint8_t getData1() const;
  ///Получить info доп набора кнопок
  const uint8_t getData2() const;
  ///Получить info доп набора кнопок
  const uint8_t getData3() const;

//  ///Получить длину данных
//  const uint32_t  getLenOfData() const;
DECLARE_SDL_SIGNAL_FOOT();

//////////////////////////////////////////////////////////////////////////////////
/** Сигнал alert
 * Входной.
Параметры:
- getId- id соединения,
 * */
DECLARE_SDL_SIGNALIN_HEAD(vms_alert_env)
  ///
  const uint8_t getId() const;
  explicit CSDL_Signal_vms_alert_env(uint8_t _id);
DECLARE_SDL_SIGNAL_FOOT();

};//namespace
