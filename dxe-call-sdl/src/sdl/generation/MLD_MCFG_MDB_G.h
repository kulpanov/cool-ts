
/* Program generated by Cadvanced 4.5.0  */
#define XSCT_CADVANCED

#ifndef XX_MLD_MCFG_MDB_G_H
#define XX_MLD_MCFG_MDB_G_H

/*************************************************************************
**                SECTION Types and Forward references                  **
*************************************************************************/

/*****
* PACKAGE MLD_MCFG_MDB_G
* #SDTREF(SDL,C:\prj\amt\dxe-vms-1.11\common\MLD_MCFG_MDB\MLD_MCFG_MDB_G.sun,4,1,9)
******/
extern XCONST struct xPackageIdStruct yPacR_z_MLD_MCFG_MDB_G__MLD_MCFG_MDB_G;

extern void yInit_MLD_MCFG_MDB_G (void);

/*****
* BLOCK TYPE MLD_MCFG_MDB
* <<PACKAGE MLD_MCFG_MDB_G>>
* #SDTREF(SDL,C:\prj\amt\dxe-vms-1.11\common\MLD_MCFG_MDB\MLD_MCFG_MDB_G.sun(1),119(65,100),1,1)
******/
extern XCONST struct xBlockIdStruct yBloR_z_MLD_MCFG_MDB_G_0_MLD_MCFG_MDB;

/*************************************************************************
**                #CODE directives, #HEADING sections                   **
*************************************************************************/
#endif
