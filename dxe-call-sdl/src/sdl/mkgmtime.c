/*
 * mkgmtime.c
 *
 *  Created on: 07 февр. 2015 г.
 *      Author: Kulpanov
 */

#include <windows.h>
#include <time.h>
#include "stdio.h"

#define TICKSPERSEC        10000000
#define TICKSPERMSEC       10000
#define SECSPERDAY         86400
#define SECSPERHOUR        3600
#define SECSPERMIN         60
#define MINSPERHOUR        60
#define HOURSPERDAY        24
#define EPOCHWEEKDAY       1
#define DAYSPERWEEK        7
#define EPOCHYEAR          1601
#define DAYSPERNORMALYEAR  365
#define DAYSPERLEAPYEAR    366
#define MONSPERYEAR        12

#define TICKSTO1970         0x019db1ded53e8000
#define TICKSTO1980         0x01a8e79fe1d58000
#define SECS_1601_TO_1970  ((369 * 365 + 89) * (ULONGLONG)SECSPERDAY)
#define TICKS_1601_TO_1970 (SECS_1601_TO_1970 * TICKSPERSEC)

static const int YearLengths[2] = {DAYSPERNORMALYEAR, DAYSPERLEAPYEAR};
static const int MonthLengths[2][MONSPERYEAR] =
{
        { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
        { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
};

static __inline int IsLeapYear(int Year)
{
  return Year % 4 == 0 && (Year % 100 != 0 || Year % 400 == 0) ? 1 : 0;
}


__time32_t _mkgmtime32(
   struct tm* timeptr
){

  SYSTEMTIME st;
  FILETIME ft;
  int i, ret;

  st.wMilliseconds = 0;
  st.wSecond = timeptr->tm_sec;
  st.wMinute = timeptr->tm_min;
  st.wHour = timeptr->tm_hour;
  st.wDay = timeptr->tm_mday;
  st.wMonth = timeptr->tm_mon+1;
  st.wYear = timeptr->tm_year+1900;

  if(!SystemTimeToFileTime(&st, &ft))
      return -1;

  FileTimeToSystemTime(&ft, &st);
  timeptr->tm_wday = st.wDayOfWeek;
  for(i=timeptr->tm_yday=0; i<st.wMonth-1; i++)
    timeptr->tm_yday += MonthLengths[IsLeapYear(st.wYear)][i];
  timeptr->tm_yday += st.wDay-1;

  ret = ((__time32_t)ft.dwHighDateTime<<32)+ft.dwLowDateTime;
  ret = (ret-TICKS_1601_TO_1970)/TICKSPERSEC;
  return ret;
}
