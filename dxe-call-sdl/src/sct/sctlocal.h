#ifndef X_SCTLOCAL_H
#define X_SCTLOCAL_H


/*******************************************************************************
Copyright by Telesoft Europe AB 1990, 1991.
Copyright by Telelogic Malmoe AB 1991, 1992, 1993, 1994.
Copyright by Telelogic AB 1994 - 2003.
This Program is owned by Telelogic and is protected by national
copyright laws and international copyright treaties. Telelogic
grants you the right to use this Program on one computer or in
one local computer network at any one time.
Under this License you may only modify the source code for the purpose
of adapting it to your environment. You must reproduce and include
any copyright and trademark notices on all copies of the source code.
You may not use, copy, merge, modify or transfer the Program except as
provided in this License.
Telelogic does not warrant that the Program will meet your
requirements or that the operation of the Program will be
uninterrupted and error free. You are solely responsible that the
selection of the Program and the modification of the source code
will achieve your intended results and that the results are actually
obtained.
*******************************************************************************/

/*
# $FileId: sctlocal.h 34 : 2003/03/19 pon
*/


/****+***************************************************************
     Macros
********************************************************************/

#define xSignalOrTimer \
        ((1L << ((long)xSignalEC)) | (1L << ((long)xTimerEC)) | (1L << ((long)xRPCSignalEC)))
#define xVariableOrFormalPar \
        ((1L<<((long)xVariableEC)) | (1L<<((long)xFormalParEC)))
#define xChannelOrSignalRoute \
        ((1L<<((long)xChannelEC)) | (1L<<((long)xSignalrouteEC)))

#define xfShortIdentifierLength 20
#define CheckGRRef(str) (str == (char *)0 ? " " : str)

#define PRINTF(v) {xPrintString(v);}
#define PRINTF2(f,v) \
        {xWriteBuf_Fmt(f,v);}
#define PRINTF3(f,a,b) \
        {xWriteBuf_Fmt(f,a,b);}
#define PRINTF4(f,a,b,c) \
        {xWriteBuf_Fmt(f,a,b,c);}
#define PRINTF5(f,a,b,c,d) \
        {xWriteBuf_Fmt(f,a,b,c,d);}


        /* Number of decimals is maximized to 9 (nanosecends) */
#define TIMEVALUEDECIMALS 4
        /* Number of decimals given normally when time or duration printed */
#define TIMEDECIMALS 2
        /* decimals printed for NOW when entering monitor */


/*---+---------------------------------------------------------------
     xFindState
-------------------------------------------------------------------*/
#ifndef XNOUSEOFSERVICE
#define xFindState(PrsP) \
  ( PrsP->ActiveSrv == (xSrvNode)0 \
  ? ( PrsP->ActivePrd == (xPrdNode)0 \
    ? PrsP->State \
    : PrsP->ActivePrd->State \
    ) \
  : ( PrsP->ActiveSrv->ActivePrd == (xPrdNode)0 \
    ? PrsP->ActiveSrv->State \
    : PrsP->ActiveSrv->ActivePrd->State \
    ) \
  )
#else
#define xFindState(PrsP) \
  ( PrsP->ActivePrd == (xPrdNode)0 ? PrsP->State : PrsP->ActivePrd->State )
#endif


/*---+---------------------------------------------------------------
     xGetState
-------------------------------------------------------------------*/
extern xStateIdNode xGetState (
  xPrsNode      PrsP,
  xPrdNode      PrdP,
  xStateIdNode *StateList,
  int           InState);

/* ----------------------- State properties ---------------------- */
#define NEEDSRECALC(STATEID)       ( STATEID->StateProperties & (int)1 )
#define HASINPUTNONE(STATEID)      ( STATEID->StateProperties & (int)2 )
#define HASVIRTINPUTNONE(STATEID)  ( STATEID->StateProperties & (int)4 )
#define HASPRIOINPUT(STATEID)      ( STATEID->StateProperties & (int)8 )


/* ------------------------ Monitor, Util ------------------------ */

#define XREADYQ_EMPTY          (XSYSD xReadyQueue->Suc == XSYSD xReadyQueue)
#define XREADYQ_FIRST          XSYSD xReadyQueue->Suc
#define XREADYQ_SUC(PRS)       PRS->Suc
#define XBEGIN_PRS_READYQ_LOOP(PRS_VAR) \
   for (PRS_VAR = XSYSD xReadyQueue->Suc; \
        PRS_VAR != XSYSD xReadyQueue; \
        PRS_VAR = PRS_VAR->Suc) {
#define XEND_PRS_READYQ_LOOP \
   }

#define XIS_PID_IN_SYSTEM(PID) \
   (PID.GlobalNodeNr == xGlobalNodeNumber())
#define XPID_TO_PRS(PID)       PID.LocalPId->PrsP
#define XPID_INSTNR(PID)       PID.LocalPId->InstNr
#define XPRS_INSTNR(PRS)       PRS->Self.LocalPId->InstNr
#define XPRS_STATEID(PRS)      PRS->State
#define XPRS_PARENT(PRS)       PRS->Parent
#define XPRS_OFFSPRING(PRS)    PRS->Offspring
#define XPRS_SENDER(PRS)       PRS->Sender
#define XPRS_NEXT_REC_SIG(PRS) PRS->Signal
#define XPID_GLOBID(PID)       PID.LocalPId->GlobalInstanceId

#define XSIGNAL_DATA(SIG)      SIG
#define XSIGNAL_CODE(SIG)      SIG->NameNode
#define XSIGNALID_CODE(SIGID)  SIGID
#define XSIGCODE_IDNODE(CODE)  CODE
#define XSIGNAL_SENDER(SIG)    SIG->Sender
#define XSIGNAL_RECEIVER(SIG)  SIG->Receiver
#define XSIGNAL_IDNODE(SIG)    SIG->NameNode
#define XSIGNAL_GLOBID(SIG)    SIG->GlobalInstanceId
#define XIS_STARTUP_SIGNAL(SIG)  (SIG->NameNode->EC == xStartUpSignalEC)
#define XIS_CONT_SIGNAL(SIG)   (SIG->NameNode == xContSigId)
#define XIS_NONEID_SIGNAL(SIGID)  (SIGID == xNoneSigId)
#define XCONT_SIG_PRIO(SIG)    SIG->Prio
#define XCREATE_SIGNAL(SIG_VAR_PTR, SIG_IDNODE, RECEIVER, SENDER) \
   *SIG_VAR_PTR = xGetSignal(SIG_IDNODE, RECEIVER, SENDER);
#define XRELEASE_SIGNAL(SIG)   xReleaseSignal(&SIG);
#define XSET_RECEIVER(SIG, PID)  SIG->Receiver = PID;
#define SEND_SIGNAL_IN_MONITOR \
   if (xIdNodeSender == xNullId) \
     XSIGNAL_SENDER(Signal) = SDL_NULL; \
   else \
     XSIGNAL_SENDER(Signal) = PrsNodeSender->Self; \
   SDL_Output(Signal  xSigPrioPar(intVar), ViaList);


#define XINPUTPORT_EMPTY(PRS)  (PRS->InputPort.Suc == (xSignalNode)&PRS->InputPort)
#define XBEGIN_INPUTPORT_LOOP(PRS, SIGNAL_VAR) \
   for (SIGNAL_VAR = PRS->InputPort.Suc; \
        SIGNAL_VAR != (xSignalNode)&PRS->InputPort; \
        SIGNAL_VAR = SIGNAL_VAR->Suc) {
#define XEND_INPUTPORT_LOOP \
   }
#define SIGNAL_IN_INPUT_PORT(SIG)  (SIG->Pre != (xSignalNode)0)

#define XTIMERQ_EMPTY         (XSYSD xTimerQueue->Suc == XSYSD xTimerQueue)
#define XTIMERQ_FIRST         XSYSD xTimerQueue->Suc
#ifdef THREADED_XTRACE
#define XBEGIN_TIMERQ_LOOP(TIMER_VAR) \
   for (TIMER_VAR = PrsP->SysD->xTimerQueue->Suc; \
        TIMER_VAR != PrsP->SysD->xTimerQueue; \
        TIMER_VAR = TIMER_VAR->Suc) {
#else
#define XBEGIN_TIMERQ_LOOP(TIMER_VAR) \
   for (TIMER_VAR = XSYSD xTimerQueue->Suc; \
        TIMER_VAR != XSYSD xTimerQueue; \
        TIMER_VAR = TIMER_VAR->Suc) {
#endif /* THREADED_XTRACE */
#define XEND_TIMERQ_LOOP \
   }
#define XTIMERSIGNAL(TIMER)    TIMER
#define XTIMER_IDNODE(TIMER)   TIMER->NameNode
#define XTIMER_SENDER(TIMER)   TIMER->Sender
#define XTIMER_RECEIVER(TIMER) TIMER->Receiver
#define XFIRST_TIMER_TIME      ((xTimerNode)XSYSD xTimerQueue->Suc)->TimerTime


/****+***************************************************************
     Types
********************************************************************/

/*---+---------------------------------------------------------------
     xfCodeType
-------------------------------------------------------------------*/
#ifdef XREADANDWRITEF
typedef enum {
  xfOk, xfAmbiguous, xfNotFound
} xfCodeType;
#endif



/****+***************************************************************
     sctpost
********************************************************************/

#ifdef XCONNECTPM

extern int PMSend (int, int, char *);
extern int PMBroadcast (int, char *);
extern int PM_RPC (int, int, char *, char **);
extern int PMStartTool (int);
extern int PMCheckAlive (int);
extern void PMStopTool (int);
extern int PMGetToolPId (int);
extern void xHandleAllPMSigs (int, int, void *, int);

#ifdef XITEXCOMM
extern xbool xAddToITEXChannelList (xChannelIdNode channel);
#endif

#if defined (XMSCE) || defined (THREADED_MSCTRACE)
extern void xMPrintString (char *);
extern xbool xStartMSC (void);
#endif

#ifdef XSIMULATORUI
extern int StartSimUI (void);
extern void xSendPrintString (char *);
extern void xSendCommandReply (void);
extern void xSendUIChange (void);
extern void xLoopForMessage (void);
#endif

extern void xSendxTrStrReply (int sender, char * str);

extern void xInitPM ( int argc, char *argv[] );
extern void xClosePM (void);
extern void xInPM (void);
extern int xPM_Read_SDL_PId (SDL_PId *);
extern void xGRTraceSymbol  (char *);
#ifdef __cplusplus
extern "C" {
#endif
#ifdef _Windows
#include "post.h"
#else
  extern int SPCheckI (int, double, char*);
  extern int SPCheck  (char *);
  extern int SPCheckC (char *);
#endif
#ifdef __cplusplus
}
#endif

#if defined(XPMCOMM) || defined(XITEXCOMM)
extern void xOutPM (xSignalNode *, xChannelIdNode);
#endif

#ifdef XSDLENVUI
extern int xInit_UI_sdlenv (void);
#endif

#ifdef XCOVERAGE
extern int xStartCoverageViewer (char *TempFileName);
#endif

extern void xUpdateBreakpoints  (xbool);
extern int  xConnectToEditor  (void);
extern void xDisconnectEditor  (xbool);

extern char *xGetTargetDirectory (void);
extern char *xGetInstallDirectory (void);

extern void  xInitGRConversion (void);
extern void  xSetGRConversion (xbool doSet, char *fromStr, char *toStr);
extern char *xGRConversion (xbool reverse, char *str, char *buffer);

#endif
       /* XCONNECTPM */


/****+***************************************************************
     sctutil
********************************************************************/

#ifdef XREADANDWRITEF
extern char * xEntityString[];
extern void xMonListIdNodeECSet (long, xIdNode, xIdNode, xbool);
extern void xOneLevelDecodeId (char *, xIdNode, long, xIdNode *, xfCodeType *, xIdNode);
extern void xfDecodeId (char *, xIdNode, long, xIdNode *, xfCodeType *, xIdNode);
#ifndef XNOUSEOFSERVICE
extern xSrvNode xReadService (char *, xSrvIdNode *, xPrsNode, xbool *);
#endif
extern char * xPrdInstance (xPrdNode);
extern char * xWriteEntity (xIdNode);
#endif
       /* XREADANDWRITEF */

#ifdef XMONITOR
extern char * xSymbolTypeStr [];
extern xbool xGetFileName (char *, char *);
extern xbool xGetAndOpenFile (FILE **, xbool, char *, char *);
extern xbool xGetDirectoryName (char *, char *, xbool *);
extern xbool xReadInstanceNumber (char *, int *, xbool *);
extern xbool xReadEntryNumber (char *, int *, int);
extern xxToken xReadVariable (char *, char *, xbool *, xbool);
extern xVarIdNode xSearchVariable (char *, xIdNode, xfCodeType *);
extern xbool xVariableInProcess (char *, xVarIdNode *, xptrint *);
extern void xPrintPrdVariable (xVarIdNode, xPrdNode);
extern void xPrintPrsVariable (xVarIdNode, xptrint);
extern void xPrintAllPrsVariables (xIdNode, xptrint);
extern void xExamineVariable (char *);
extern void xAssignVariable (tSDLTypeInfo *, xptrint);
extern void xAssignValue (char *);
extern void xListInputPort (char *);
extern int xReadFPars (xPrsIdNode, xSignalNode *, char *);
extern int xReadSignalParameters (xSignalIdNode, xSignalNode *, SDL_PId);
extern void xSymbolTable (xIdNode, int);
extern void xPrintChannel (xIdNode, int);
#ifdef XCOVERAGE
extern void xPrintCoverageArray (xIdNode, int, int);
extern void xCoverageStatistics (xIdNode, int *, int *);
extern void xClearCoverage (xIdNode);
#endif
#endif
       /* XMONITOR */

#ifdef XMSCE
extern void xMSCEditorStopped (void);
extern void xMSCEClose (void);
#ifdef XSIGPATH
extern void  xSetEnvSplitIntoChannels (xbool);
extern xbool xIsEnvSplitIntoChannels (void);
extern xbool xGetDefaultEnvSplitIntoChannels (void);
extern void xMSCEOutput (xSignalNode, int, xIdNode [], int);
#else
extern void xMSCEOutput (xSignalNode, int);
#endif
extern void xMSCEOutputDiscard (xSignalNode);
extern void xMSCETimerOutput (xSignalNode);
extern void xMSCETimerOutputDiscard (xSignalNode);
extern void xMSCECreate (SDL_PId, xSignalNode);
extern void xMSCEUnsuccessfulCreate (xPrsIdNode, SDL_PId, xSignalNode);
extern void xMSCESet (SDL_Time, xSignalNode);
extern void xMSCEReset (xSignalNode);
extern void xMSCENextstate (xPrsNode, int);
extern void xMSCENextstateDiscard (xPrsNode, xSignalNode);
extern void xMSCEStop (xPrsNode);
extern void xMSCEProcedureStart (xPrsNode, xPrdNode);
extern void xMSCETransition (xPrsNode);
extern void xMSCEEnvReceive (xSignalNode);
extern void xMSCEInit (void);
#endif
       /* XMSCE */


/****+***************************************************************
     sctmon
********************************************************************/

#ifdef XREADANDWRITEF
#if defined(XENV) || defined(XCONNECTPM)
extern void xLoopForInput (void);
#endif
#ifdef XSIMULATORUI
extern void xHandleSimUIMessage (int, char *, int);
extern char *xHandlePMInternCommand (int sender, char *mess, int size);
#endif
extern void xNextLine (void);
extern char * xWri_SDL_PId_No_Qua (void *);
extern void xWriteSignalParameters (xSignalNode, char *);
extern xSystemIdNode xGetSystemIdNode (xIdNode);
#endif
       /* XREADANDWRITEF */

#if defined(XMONITOR) || defined(XTRACE) || defined (THREADED_XTRACE)
extern void xIdentifyTransition (xPrsNode, char *);
#endif

#ifdef XGRTRACE
extern int xIsGRTraced (xPrsNode);
#endif

#if defined (XTRACE) || defined (THREADED_XTRACE)
extern void xTraceOutput (xSignalNode);
extern void xTraceOutputDiscard (xSignalNode);
extern void xTraceTimerOutput (xSignalNode);
extern void xTraceTimerOutputDiscard (xSignalNode);
extern void xTraceCreate (xSignalNode);
extern void xTraceUnsuccessfulCreate (xPrsIdNode);
extern void xTraceSet (SDL_Time, xSignalNode);
extern void xTraceReset (xSignalNode);
extern void xTraceResetAction (int, xSignalNode);
extern void xTraceNextstate (xPrsNode, int);
extern void xTraceNextstateDiscard (xPrsNode, xSignalNode);
extern void xTraceStop (xPrsNode);
extern void xTraceTransition (xPrsNode);
#ifndef XNOUSEOFSERVICE
extern void xTraceStartService (xSrvNode);
#endif
extern void xTraceProcedureStart (xPrdNode);
#endif
       /* XTRACE */

#ifdef XBREAKBEFORE
#ifdef THREADED_MSCTRACE
extern void xSetUpCurrentSymbolRef (xPrsNode PrsP);
#else
extern void xSetUpCurrentSymbolRef (void);
#endif /* THREADED_MSCTRACE */
#endif

#ifdef XMONITOR
extern void xCloseAllMonitorFiles (void);
extern void xInitMonitorSystem (void);
extern xbool xVerify (char *);
extern void xPrintScope (xbool);
extern xbool xCheckBreakpointOutput (xSignalNode);
extern void xCheckMonitors (void);
extern SDL_Time xNextMonTime (void);
extern xbool xMonBreakAtSymbol (long int, xbool);
extern void xMonitorSignalLog (xSignalNode, int, xIdNode *, int);
#endif
       /* XMONITOR */


/****+***************************************************************
     sctsdl
********************************************************************/

extern xInputAction xFindInputAction (XSIGTYPE, xPrsNode, xbool);


#endif
       /* X_SCTLOCAL_H */
