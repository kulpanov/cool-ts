/******************************************************************************
 * Copyright by Telesoft Europe AB 1990, 1991.
 * Copyright by TeleLOGIC AB 1991, 1992, 1993, 1994, 1995,
 * Copyright by Telelogic AB 1997, 1998.
 * 
 *  This Program is owned by Telelogic and is protected by national 
 * copyright laws and international copyright treaties. Telelogic 
 * grants you the right to use this Program on one computer or in 
 * one local computer network at any one time. 
 * Under this License you may only modify the source code for the purpose 
 * of adapting it to your environment. You must reproduce and include 
 * any copyright and trademark notices on all copies of the source code. 
 * You may not use, copy, merge, modify or transfer the Program except as
 * provided in this License.
 * Telelogic does not warrant that the Program will meet your
 * requirements or that the operation of the Program will be
 * uninterrupted and error free. You are solely responsible that the
 * selection of the Program and the modification of the source code 
 * will achieve your intended results and that the results are actually 
 * obtained.
 *****************************************************************************/

#ifndef _sdtsym_h
#define _sdtsym_h

#define NOVIRTUAL               0
#define VIRTUAL                 1
#define REDEFINED               2
#define FINALIZED               3

/************************ susdt.h *****************************************/

#ifndef _susdt_h
#define _susdt_h

#define   SYSTEMDIAGRAM         1
#define   BLOCKDIAGRAM          2
#define   SUBSTRUCTUREDIAGRAM   3
#define   SERVICEDIAGRAM        4
#define   PROCESSDIAGRAM        5
#define   PROCEDUREDIAGRAM      6
#define   MACRODIAGRAM          7
                                     /* 8, 9, 10 reserved for SDUNIT,
                                        SDSEQUENCE and SDPAGE  */
#define   UNITDIAGRAM           11
#define   SYSTEMTYPEDIAGRAM     12
#define   BLOCKTYPEDIAGRAM      13
#define   SERVICETYPEDIAGRAM    14
#define   PROCESSTYPEDIAGRAM    15
#define   MSCDIAGRAM            16
                                     /* 17 reserved for SDMSCTIMEFRAME */
#define   SUBMSCDIAGRAM         18
#define   OPERATORDIAGRAM       19
#define   OVERVIEWDIAGRAM       20
#define   CLASSDIAGRAM          21

#define   TEXTDOCUMENT          33
#define   ASN1TEXTDOCUMENT      34
#define   CTEXTDOCUMENT         35
#define   IDLTEXTDOCUMENT       36

#define   HMSCDIAGRAM           39
#define   SSDIAGRAM             40

#define   BUILDTEXTDOCUMENT     41

#define   DEPLOYDIAGRAM         50

#define   SYSTEMPAGE            1
#define   BLOCKPROCESSPAGE      2
#define   SERVICEPAGE           3
#define   PROCESSASSERVICEPAGE  4
#define   PROCESSPAGE           5
#define   PROCEDUREPAGE         6
#define   MACROPAGE             7
#define   OVERVIEWPAGE          8
#define   UNITPAGE              9
#define   SYSTEMTYPEPAGE       10
#define   BLOCKTYPEPROCESSPAGE 11
#define   SERVICETYPEPAGE      12
#define   PROCESSTYPEASSERVICEPAGE 13
#define   PROCESSTYPEPAGE      14
#define   OPERATORPAGE         15
#define   CLASSPAGE            16
#define   HMSCPAGE             17
#define   SSPAGE               18
#define   MSCPAGE              19
#define   DEPLOYPAGE           20

#endif /*_susdt_h*/

/************************ susdt.h *****************************************/


/* Definitions of SDL symbols */
#define DUMMYSYMBOL        0
#define START              1
#define PROCEDURESTART     2
#define MACROINLET         3
#define STOP               4
#define PROCEDURERETURN    5
#define MACROOUTLET        6
#define STATE              7
#define INPUTRIGHT         8
#define INPUTLEFT          (INPUTRIGHT + 1)
#define PRIINPUTRIGHT     10
#define PRIINPUTLEFT      (PRIINPUTRIGHT + 1)
#define CONTINUOUSSIGNAL  12
#define ENABLINGCONDITION 13
#define TASK              14
#define OUTPUTRIGHT       15
#define OUTPUTLEFT        (OUTPUTRIGHT + 1)
#define PRIOUTPUTRIGHT    17
#define PRIOUTPUTLEFT     (PRIOUTPUTRIGHT + 1)
#define PROCEDURECALL     19
#define INCONNECTOR       20
#define CREATEREQUEST     21
#define DECISION          22
#define TRANSITIONOPTION  23
#define MACROCALL         24
#define OUTCONNECTOR      25
#define SAVE              26
#define PROCEDURE         27
#define SDLTEXT           28
#define COMMENT           29
#define COMMENTLEFT       (COMMENT + 1)
#define TEXTEXTENSION     31
#define TEXTEXTENSIONLEFT (TEXTEXTENSION + 1)
#define TEXTREFERENCE     33


/* Definitions of SDL symbols used in a block page */
#define SERVICE             41
#define PROCESS             42
#define BLOCK               43
#define FRAME               44
#define BLOCKSUBSTRUCTURE   45
#define CHANNELSUBSTRUCTURE 46
  /* OSDL symbols */
#define SYSTEMTYPE          47
#define BLOCKTYPE           48
#define SERVICETYPE         49
#define PROCESSTYPE         50


/* heading symbols */
#define HEADING          51
#define EXTENDEDHEADING  52
#define PAGENUMBER       53
  /* OSDL symbols */
  /* GATESYMBOL is used in the palette to draw the gate */
#define GATESYMBOL       54
#define UNITTEXT         55
  /* Operator reference symbol */
#define OPERATOR         56
  /* Class diagram symbols */
#define OMCLASS          57
#define OMOBJECT         58
  /* Text Editor fragment "symbols" */
#define TEXTFRAGMENT     59

/* HMSC symbols */
#define MSCREFERENCE     60
#define HMSCSTART        61
#define HMSCSTOP         62
#define HMSCCONNECTION   63
#define HMSCCONDITION    64

/* state chart symbols */
#define SSSTART          65
#define SSSTOP           66
#define SSSTATE          67


/* line objects */
#define FLOWLINE        101
#define SIGNALROUTE     102
#define DBLSIGNALROUTE  103
#define CHANNEL         104
#define DBLCHANNEL      105
#define CREATELINE      106
#define SUBSTRLINE      107
#define FLOWLINEINBLOCK 108
  /* OSDL symbols */
#define GATELINEIN      109
#define GATELINEOUT     110
#define DBLGATELINE     111
#define CONNECTIONPOINTLINE 112
  /* Class diagram lines */
#define ASSOCIATION     113
#define AGGREGATION     114
#define GENERALIZATION  115
#define INSTANTIATION   116
#define LINEASSOCIATION 117
#define ASSOCIATIONCLASSLINE 118
/* HMSC */
#define HMSCLINE        119
/* state chart */
#define SSLINE          120

/* COMMENTLINE is already defined in scsym.h */
#define SDLCOMMENTLINE  121

  /* SDL lines for type references */
#define SDL_ASSOCIATION 122
#define SDL_DBLASSOCIATION 123
#define SDL_AGGREGATION 124

#define LASTSYMBOL      124

#endif /* _sdtsym_h */
