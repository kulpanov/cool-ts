/******************************************************************************
 * Copyright by Telesoft Europe AB 1990, 1991.
 * Copyright by TeleLOGIC Malmoe AB 1991, 1992, 1993, 1994.
 * Copyright by TeleLOGIC AB 1994, 1995.
 * Copyright by Telelogic AB 1996, 1997, 1998
 *
 *  This Program is owned by Telelogic and is protected by national 
 * copyright laws and international copyright treaties. Telelogic 
 * grants you the right to use this Program on one computer or in 
 * one local computer network at any one time. 
 * Under this License you may only modify the source code for the purpose 
 * of adapting it to your environment. You must reproduce and include 
 * any copyright and trademark notices on all copies of the source code. 
 * You may not use, copy, merge, modify or transfer the Program except as
 * provided in this License.
 * Telelogic does not warrant that the Program will meet your
 * requirements or that the operation of the Program will be
 * uninterrupted and error free. You are solely responsible that the
 * selection of the Program and the modification of the source code 
 * will achieve your intended results and that the results are actually 
 * obtained.
 *****************************************************************************/

#ifndef _post_h
#define _post_h

#include "dll.h"

/* Interface to SDT Services */

/* Macros */

/* Service reply codes */

#define SDT_OK 		0
#define SDT_BUSY    	1
#define SDT_ERRSTR  	2
#define SDT_ERRCODE 	3


/*  Error codes */

#ifndef _suerrid_h
#define SPNOSESSION        1001 /* not yet connected */
#define SPALREADYCONNECTED 1002 /* already connected */
#define SPNOCHANNEL        1003 /* lost contact with postmaster */
#define SPNOPOSTMASTER     1004 /* no postmaster to connect to */
#define SPNOMESSAGE        1005 /* no message when reading */
#define SPTIMEDOUT         1006 /* timeout when connecting */
#define SPNOSUCHPID        1009 /* erroroneous pid value */
#define SPNOTCONNECTED     1010 /* connection failed */
#define SPNOMEMORY         1013 /* no memory left */
#define SPBUFFERTFULL      1014 /* buffer full */

#define SPMANYPOSTMASTERS  1018 /* many postmasters */
#define SPOLDPOSTMASTER    1019 /* old postmaster used (only windows) */

#endif

#define SPTOOLNOTFOUND     1200 /* No such tool to send to */
#define SPBADMESSAGE       1201 /* invalid message */


/* Message handling macros */

#define SPT_MASK        1000
#define SPT_BASE        100

#define SP_MESSAGE(M)           (M)
#define SP_RPCREQUEST(M)        ((M)+100)
#define SP_RPCREPLY(M)          ((M)+200)

#define SPT_RPCREQUEST    (1 * SPT_BASE)
#define SPT_RPCREPLY    (2 * SPT_BASE)
#define SP_GETMESSAGETYPE(M)    (((M)%SPT_MASK/SPT_BASE)*SPT_BASE)

#define SP_ISMESSAGE(M)         (SP_GETMESSAGETYPE(M)==SPT_MESSAGE)
#define SP_ISREQUEST(M)         (SP_GETMESSAGETYPE(M)==SPT_RPCREQUEST)
#define SP_ISREPLY(M)           (SP_GETMESSAGETYPE(M)==SPT_RPCREPLY)
 
#define SP_TOOLCLASS(M)         ((M)-((M)%SPT_MASK))

/* Generate the appropriate reply message to message MX */
#define SP_MAKEREPLY(M)         SP_RPCREPLY((M)-SP_GETMESSAGETYPE(M))

/* Check if message RX is a reply to message MX */
#define SP_MATCHREPLY(MX,RX) \
        (SP_ISREQUEST (MX) && SP_ISREPLY(RX) && \
         (SP_TOOLCLASS (MX) == SP_TOOLCLASS (RX)) && \
         ((MX)%SPT_BASE == (RX)%SPT_BASE))
 
/* function prototype usage macros */

#ifndef SPP
#ifndef XNOPROTO
#define SPP(x) x
#else
#define SPP(x) ()
#endif
#endif


/* Timeout to wait indefinitely for a messge */

#ifndef SPWAITFOREVER
#define SPWAITFOREVER           -1
#endif


/* return values from functions */

#ifndef SPOK
#define SPOK                    0
#endif
#ifndef SPERROR
#define SPERROR                 -1
#endif


/* compiler dependent integer definitions */

#ifndef _suos_h
#define INT16 short
#define INT32 int
#endif


/* Types */

typedef struct SPMLIST  {
  char *str;
  int  id;
} SPMList;


/* Functional interface */

#ifdef __cplusplus
//extern "C" {
#endif

/* Variables */

DLLEXPORT extern int spPortNO;

DLLEXPORT extern INT16 sperrno;

DLLEXPORT extern SPMList spMList[];

/* Functions */
DLLEXPORT int STDCALL GetSPPortNO SPP((void));
DLLEXPORT int STDCALL GetSPerrno SPP((void));
DLLEXPORT SPMList* STDCALL GetSPMList SPP((void));

DLLEXPORT char * STDCALL SPErrorString SPP((int code));

DLLEXPORT int STDCALL SPConvert SPP((char * str));

DLLEXPORT int STDCALL SPInit SPP((int toolType, char *  argv0,  SPMList * mlist));

DLLEXPORT int STDCALL SPExit SPP((void));

DLLEXPORT int STDCALL SPSendToTool SPP((int toolType,
                                        int message,
                                        void *data,
                                        int len));

DLLEXPORT int STDCALL SPSendToPid SPP((int pId, int message, void *data, int len));

DLLEXPORT int STDCALL SPBroadcast SPP((int message, void *data, int len));

DLLEXPORT int STDCALL SPRead SPP((int timeOut,
                                  int *sender,
                                  int *message, 
                                  void **data,
                                  int *len));

DLLEXPORT int STDCALL SPStartPost SPP((int toolType, char *argv0, SPMList * mlist));

DLLEXPORT int STDCALL SPDecodeError SPP(( void* data,
                                          int *event, 
                                          int *lClass,
                                          int *error,
                                          int *pid));

DLLEXPORT int STDCALL SPQuoteString SPP((char *stringToQuote,
                                         char *buffer,
                                         int bufferLength,
                                         int append));

DLLEXPORT int STDCALL SPUnquoteString SPP((char *input,
                                           int inputLength,
                                           char *buffer,
                                           int bufferLength,
                                           int *position));

DLLEXPORT void STDCALL SPFree SPP((void* ptr));

DLLEXPORT int STDCALL SPFindActivePostMasters SPP((int *bufferPid, 
                                                   char** bufferText, 
                                                   int maxBufferlength));




DLLEXPORT int STDCALL SPCheckI(int, double, char*);
DLLEXPORT int STDCALL SPCheck (char *);
DLLEXPORT int STDCALL SPCheckC (char *);
DLLEXPORT int STDCALL SPCheck2I(int, double, char*);
DLLEXPORT int STDCALL SPCheck2(char *);
DLLEXPORT int STDCALL SPCheck2C (char *);
DLLEXPORT int STDCALL SPCheckTool (int, int *);
DLLEXPORT int STDCALL SPLicReport (char*, int, char *);
DLLEXPORT int STDCALL SPDeport (int, char*);
DLLEXPORT int STDCALL SPRequest (int, double, char*);

DLLEXPORT int STDCALL SPInConfiguration (int, char*);

#if defined(_Windows) || defined (_WINDOWS)
typedef void (* SP_PM_MessageCallback) (void);
DLLEXPORT void STDCALL SPRegisterPMCallback (SP_PM_MessageCallback cb);
#endif /* _Windows || _WINDOWS */

#ifdef __cplusplus
//}
#endif

#endif /* _post_h */
