#ifndef X_SCTTHLTYPES_H
#define X_SCTTHLTYPES_H


/*******************************************************************************
Copyright by Telesoft Europe AB 1990, 1991.
Copyright by Telelogic Malmoe AB 1991, 1992, 1993, 1994.
Copyright by Telelogic AB 1994 - 2003.
This Program is owned by Telelogic and is protected by national
copyright laws and international copyright treaties. Telelogic
grants you the right to use this Program on one computer or in
one local computer network at any one time.
Under this License you may only modify the source code for the purpose
of adapting it to your environment. You must reproduce and include
any copyright and trademark notices on all copies of the source code.
You may not use, copy, merge, modify or transfer the Program except as
provided in this License.
Telelogic does not warrant that the Program will meet your
requirements or that the operation of the Program will be
uninterrupted and error free. You are solely responsible that the
selection of the Program and the modification of the source code
will achieve your intended results and that the results are actually
obtained.
*******************************************************************************/

/*
# $FileId: sctthltypes.h 9 : 2003/06/24 jk
*/

/*
FILE INDEX
00   Utility macros
01   Main User oriented #ifdef
02   Dependent #ifdef
03   C Library
04   SDL Model macro expansions
05   Utility #ifdef
06   Constants
07   Forward Declarations
08   SDL predefined types (except SDL_PId)
09   SDL_PId
10   Signal Set
11   Procedure ( = Prd )
12   Process ( = Prs )
13   Timer
14   Signal
15   Symbol Table
16   Global Data Structure
17   sctos
18   sctenv
19   sctsdl
20   sctpost
21   sctutil
*/

#ifdef THREADOSE
#include "ose.h"
#endif /* THREADOSE */
#ifdef THREADVXWORKS
#if defined(THREADED_TRACE) || defined(THREADED_ERROR)
#include "errno.h"
#endif
#include "vxWorks.h"
#include "semLib.h"
#include "msgQLib.h"        /* msgQCreate msgQDelete msgQSend msgQReceive */
                            /* msgQNumMsgs */
#include "taskLib.h"        /* taskSpawn */
#include "semaphore.h"      /* POSIX semaphores */
#endif

/*---+---------------------------------------------------------------
     #ifdef THREADED_POSIX_THREADS
-------------------------------------------------------------------*/
/* Threaded Light OS-integration. Application using POSIX threads.
   Currently tested only on Linux. */

#ifdef THREADED_POSIX_THREADS
#define THREADED
#define XCALENDARCLOCK
#ifndef XENV
#define XENV
#endif
#define XPRSPRIO
#ifndef THREADED_XTRACE
#define XOPT
#endif /* THREADED_XTRACE */
#define XPRSOPT

/* Please check if other system calls need to be protected.
   Example: malloc (calloc) and free. (sctos.c)
   Example: printf, if used
*/


typedef struct {
  int ThreadStackSize;
  int ThreadPrio;
  int MaxQueueLength;
  int MaxMesSize;
} xThreadParam;

#ifdef THREADWIN32
#ifdef XUSING_SCCD  /* Can only be used with the Microsoft compiler */

#define GERXCAT(X,Y) X##Y

#define LIMITS_h GERXCAT(#,include "limits.h")
#define WINDOWS_h GERXCAT(#,include "windows.h")
#define DOS_h  GERXCAT(#,include "dos.h")
LIMITS_h
WINDOWS_h
DOS_h
#else
#include "limits.h"
#include "windows.h"
#include "dos.h"
#endif

#elif THREADSOLARIS
#include <pthread.h>
#include <semaphore.h>
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#include <mqueue.h>
#include <signal.h>
#include <time.h>
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif /* THREADWIN32 */
#if defined(THREADED_TRACE) || defined(THREADED_ERROR)
#include <stdio.h>
#include <errno.h>
#endif

#ifdef THREADED_TRACE
#define THREADED_OSTRACE(P1,P2)         printf(P1,P2);
#else
#define THREADED_OSTRACE(P1,P2)
#endif

/* Global variables, extern definition here, declarations in sctsdl.c via
   macro THREADED_GLOBAL_VARS, initialization in THREADED_GLOBAL_INIT */
#ifdef THREADWIN32
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
extern HANDLE xListMutex;
extern HANDLE xExportMutex;
extern HANDLE xInitSem;
#else
extern HANDLE xListMutex;
extern HANDLE xExportMutex;
extern HANDLE xInitSem;
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#ifdef THREADED_MSCTRACE
extern HANDLE xSendMSCTrace;
extern HANDLE xWaitMSCResp;
#endif /* THREADED_MSCTRACE */
#elif THREADVXWORKS
extern sem_t xListMutex;
extern sem_t xExportMutex;
extern sem_t xInitSem;
#ifdef THREADED_MSCTRACE
extern sem_t xSendMSCTrace;
extern sem_t xWaitMSCResp;
#endif /* THREADED_MSCTRACE */
#elif THREADOSE
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
extern SEMAPHORE * xListMutex;
extern SEMAPHORE * xExportMutex;
extern SEMAPHORE * xInitSem;
#define SDLSIG 1
#define STARTUP 2
#define TCPIPSOCK 3
typedef struct _xSystemData * xSystemDataPtr;
typedef struct xSignalStruct *xSignalNode;
union SIGNAL
{
  SIGSELECT      sigNo;
  xSignalNode    SDLSig;
  xSystemDataPtr SysD;
  int            TCPIPSock;
};
extern union SIGNAL *startup;
#else
extern SEMAPHORE * xListMutex;
extern SEMAPHORE * xExportMutex;
extern SEMAPHORE * xInitSem;
#define MESSAGE 1
#define STARTUP 2
#define TCPIPSOCK 3
typedef char Message[1];
typedef struct _xSystemData * xSystemDataPtr;
typedef struct xSignalStruct *xSignalNode;
union SIGNAL
{
  SIGSELECT   sigNo;
  Message     Mess;
  xSystemDataPtr SysD;
  int         TCPIPSock;
};
extern union SIGNAL *startup;
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#ifdef THREADED_MSCTRACE
extern SEMAPHORE * xSendMSCTrace;
extern SEMAPHORE * xWaitMSCResp;
#endif /* THREADED_MSCTRACE */
#elif THREADSOLARIS
extern pthread_mutex_t xListMutex;
extern pthread_mutex_t xExportMutex;
extern pthread_attr_t Attributes ;
extern sem_t xInitSem;
#ifdef THREADED_MSCTRACE
extern sem_t xSendMSCTrace;
extern sem_t xWaitMSCResp;
#endif /* THREADED_MSCTRACE */
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
extern struct sigaction act;
extern struct mq_attr mqattr;
extern void signal_handler();
#if defined(THREADED_TRACE) || defined(THREADED_ERROR)
extern int errno;
#endif
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif

extern int QueueCounter;
extern int xNumberOfThreads;
extern int xInitPhase;
#ifdef THREADWIN32

#ifdef THREADED_MSCTRACE
#define THREADED_GLOBAL_VARS_EXTRA \
  HANDLE xSendMSCTrace; \
  HANDLE xWaitMSCResp;
#else
#define THREADED_GLOBAL_VARS_EXTRA
#endif /* THREADED_MSCTRACE */

#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_GLOBAL_VARS \
  HANDLE xListMutex; \
  HANDLE xExportMutex; \
  HANDLE xInitSem; \
  int xNumberOfThreads; \
  int xInitPhase; \
  THREADED_GLOBAL_VARS_EXTRA
#else
#define THREADED_GLOBAL_VARS \
  HANDLE xListMutex; \
  HANDLE xExportMutex; \
  HANDLE xInitSem; \
  int xNumberOfThreads; \
  int xInitPhase; \
  THREADED_GLOBAL_VARS_EXTRA
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */

#elif THREADVXWORKS
#define THREADED_GLOBAL_VARS \
  char Message[1] = {'c'}; \
  sem_t xListMutex; \
  sem_t xExportMutex; \
  sem_t xInitSem; \
  int xNumberOfThreads; \
  int xInitPhase; \
  THREADED_GLOBAL_VARS_EXTRA
#ifdef THREADED_MSCTRACE
#define THREADED_GLOBAL_VARS_EXTRA \
  sem_t xSendMSCTrace; \
  sem_t xWaitMSCResp;
#else
#define THREADED_GLOBAL_VARS_EXTRA
#endif /* THREADED_MSCTRACE */

#elif THREADOSE
#ifdef THREADED_MSCTRACE
#define THREADED_GLOBAL_VARS_EXTRA \
  SEMAPHORE * xSendMSCTrace; \
  SEMAPHORE * xWaitMSCResp;
#else
#define THREADED_GLOBAL_VARS_EXTRA
#endif /* THREADED_MSCTRACE */
#define THREADED_GLOBAL_VARS \
  static const SIGSELECT AllSignals[] = {0}; \
  SEMAPHORE * xListMutex; \
  SEMAPHORE * xExportMutex; \
  SEMAPHORE * xInitSem; \
  int xNumberOfThreads; \
  int xInitPhase; \
  THREADED_GLOBAL_VARS_EXTRA

#elif THREADSOLARIS
#ifdef THREADED_MSCTRACE
#define THREADED_GLOBAL_VARS_EXTRA_MSCTRACE \
  sem_t xSendMSCTrace; \
  sem_t xWaitMSCResp;
#else
#define THREADED_GLOBAL_VARS_EXTRA_MSCTRACE
#endif /* THREADLIGT_MSCTRACE */
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#ifdef THREADED_ERROR
#define THREADED_GLOBAL_VARS_EXTRA \
  void signal_handler() \
  { \
    errno = EINTR; \
  }
#else
#define THREADED_GLOBAL_VARS_EXTRA \
  void signal_handler() \
  { \
  }
#endif /* THREADED_ERROR */
#define THREADED_GLOBAL_VARS \
  pthread_mutex_t xListMutex; \
  pthread_mutex_t xExportMutex; \
  sem_t xInitSem; \
  int xNumberOfThreads; \
  int QueueCounter; \
  int xInitPhase;  \
  struct sigaction act; \
  pthread_attr_t Attributes ; \
  struct mq_attr mqattr; \
  THREADED_GLOBAL_VARS_EXTRA \
  THREADED_GLOBAL_VARS_EXTRA_MSCTRACE
#else
#define THREADED_GLOBAL_VARS \
  pthread_mutex_t xListMutex; \
  pthread_mutex_t xExportMutex; \
  sem_t xInitSem; \
  int xNumberOfThreads; \
  int QueueCounter; \
  int xInitPhase;  \
  pthread_attr_t Attributes ; \
  THREADED_GLOBAL_VARS_EXTRA_MSCTRACE
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif

/* Initialize, macro placed in xMainInit in sctsdl.c */
#ifdef THREADWIN32
#ifndef SDT_TICKPERSEC
#define SDT_TICKPERSEC 1000
#endif /*SDT_TICKPERSEC*/
#define SDT_NANOSECPERTICK 1000000
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_GLOBAL_INIT \
  { \
  THREADED_ERROR_VAR \
  xInitSem = CreateSemaphore(NULL, 0 , MAX_INT , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%d, SysD=%d\n", xInitSem, 0) \
  xListMutex = CreateSemaphore(NULL, 1 , MAX_INT , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%d, SysD=%d\n", xListMutex, 0) \
  xExportMutex = CreateSemaphore(NULL, 1 , MAX_INT, NULL); \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%d, SysD=%d\n", xExportMutex, 0) \
  xNumberOfThreads = 0; \
  xInitPhase = 1; \
  }
#else
#define THREADED_GLOBAL_INIT \
  { \
  THREADED_ERROR_VAR \
  xInitSem = CreateSemaphore(NULL, 0 , MAX_INT , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%d, SysD=%d\n", xInitSem, 0) \
  xListMutex = CreateSemaphore(NULL, 1 , MAX_INT , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%d, SysD=%d\n", xListMutex, 0) \
  xExportMutex = CreateSemaphore(NULL, 1 , MAX_INT, NULL); \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%d, SysD=%d\n", xExportMutex, 0) \
  xNumberOfThreads = 0; \
  xInitPhase = 1; \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#define MAX_INT INT_MAX
#define DEF_SEC_ATTR NULL
#define DEFAULT_STACKSIZE 0
#define DEFAULT_PRIO THREAD_PRIORITY_NORMAL
#define DEFAULT_MAXQUEUESIZE 1024
#define DEFAULT_MAXMESSIZE sizeof(xSignalNode)
#elif THREADVXWORKS
#ifndef SDT_TICKPERSEC
#define SDT_TICKPERSEC 100
#endif /*SDT_TICKPERSEC*/
#define SDT_NANOSECPERTICK 10000000
#define VX_TASK_OPTIONS VX_FP_TASK
#define DEFAULT_STACKSIZE 800
#define DEFAULT_PRIO 100
#define DEFAULT_MAXQUEUESIZE 250
#define DEFAULT_MAXMESSIZE sizeof(xSignalNode)
#define THREADED_GLOBAL_INIT \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT sem_init(&xListMutex, 0,1); \
  THREADED_ERROR_REPORT("Error in sem_init, i=%d, SysD=%d\n", i, 0) \
  THREADED_ERROR_RESULT sem_init(&xExportMutex, 0,1); \
  THREADED_ERROR_REPORT("Error in sem_init, i=%d, SysD=%d\n", i, 0) \
  sem_init(&xInitSem, 0, 0); \
  xNumberOfThreads = 0; \
  xInitPhase = 1; \
  }
#elif THREADOSE
#define XMAIN_NAME SDL_main
#define SDT_TICKPERSEC 1000
#define SDT_NANOSECPERTICK 1000000
#define THREADED_GLOBAL_INIT \
  { \
  THREADED_ERROR_VAR \
  xListMutex = create_sem(1); \
  if(xListMutex == NULL) \
  THREADED_ERROR_REPORT("Error in sem_init, i=%d, SysD=%d\n", xListMutex, 0) \
  xExportMutex = create_sem(1); \
  if(xExportMutex == NULL) \
  THREADED_ERROR_REPORT("Error in sem_init, i=%d, SysD=%d\n", xExportMutex, 0) \
  xInitSem = create_sem(0); \
  xNumberOfThreads = 0; \
  xInitPhase = 1; \
  }
#define DEFAULT_STACKSIZE 1024
#define DEFAULT_PRIO      8
#define DEFAULT_MAXQUEUESIZE   1024 
#define DEFAULT_MAXMESSIZE     sizeof(xSignalNode) 
#elif THREADSOLARIS
#define _REENTRANT
#define _POSIX_PTHREAD_SEMANTICS
#define QUEUEFLAG  O_RDWR|O_CREAT|O_EXCL /*258 O_RDWR|O_CREAT */
#define QUEUEMODE 438 /*(438)0666 read and write permission */
#define DEFAULT_STACKSIZE 1024
#define DEFAULT_PRIO 10
#define DEFAULT_MAXQUEUESIZE 128
#define DEFAULT_MAXMESSIZE sizeof(xSignalNode)
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_GLOBAL_INIT \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT pthread_mutex_init(&xListMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%d\n", i, 0) \
  THREADED_ERROR_RESULT pthread_mutex_init(&xExportMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%d\n", i, 0) \
  sem_init(&xInitSem, 2, 0); \
  xNumberOfThreads = 0; \
  QueueCounter = 0; \
  xInitPhase = 1; \
  act.sa_handler = &signal_handler; \
  sigaction(SIGALRM, &act, NULL); \
  }
#else
#define THREADED_GLOBAL_INIT \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT pthread_mutex_init(&xListMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%d\n", i, 0) \
  THREADED_ERROR_RESULT pthread_mutex_init(&xExportMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%d\n", i, 0) \
  sem_init(&xInitSem, 2, 0); \
  xNumberOfThreads = 0; \
  QueueCounter = 0; \
  xInitPhase = 1; \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
/*
#define THREADED_GLOBAL_INIT \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT pthread_mutex_init(&xListMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%d\n", i, 0) \
  THREADED_ERROR_RESULT pthread_mutex_init(&xExportMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%d\n", i, 0) \
  sem_init(&xInitSem, 0, 0); \
  xNumberOfThreads = 0; \
  QueueCounter = 0; \
  xInitPhase = 1; \
  }
*/
#else
#define THREADED_GLOBAL_INIT \
  { \
  THREADED_ERROR_VAR \
  __pthread_initialize(); \
  THREADED_ERROR_RESULT pthread_mutex_init(&xListMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%d\n", i, 0) \
  THREADED_ERROR_RESULT pthread_mutex_init(&xExportMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%d\n", i, 0) \
  sem_init(&xInitSem, 0, 0); \
  xNumberOfThreads = 0; \
  xInitPhase = 1; \
  }
#endif /* THREADVXWORKS */

/* Extra variables needed to handle treads. Will be placed in the xSystemData
   struct maintained for each thread. */
#ifdef THREADWIN32
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_VARS \
  unsigned long xInputPort; \
  HANDLE xThreadId; \
  MSG    win32Mess; \
  HANDLE xInitQueue;
#else
#define THREADED_THREAD_VARS \
  HANDLE xInputPortMutex; \
  HANDLE xInputPortCond; \
  HANDLE xThreadId;
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADVXWORKS
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_VARS \
  MSG_Q_ID  xThreadId;
#else
#define THREADED_THREAD_VARS \
  sem_t xInputPortMutex; \
  MSG_Q_ID  xThreadId; \
  char  xInputPortSignal;
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADOSE
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_VARS \
  PROCESS  xThreadId; \
  union SIGNAL *  xInputPortSignal;
#else
#define THREADED_THREAD_VARS \
  SEMAPHORE * xInputPortMutex; \
  PROCESS  xThreadId; \
  union SIGNAL *  xInputPortSignal;
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#else
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_VARS \
  pthread_t xThreadId; \
  char *   xInputPort; \
  timer_t TimerId; \
  struct itimerspec timerval;
#else
#define THREADED_THREAD_VARS \
  pthread_mutex_t xInputPortMutex; \
  pthread_t xThreadId; \
  pthread_cond_t xInputPortCond;
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif

/* Possibility to initialize variables in THREADED_THREAD_VARS.
   Code will be placed last in function xSysDInit in sctsdl.c */
#ifdef THREADWIN32
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_INIT(SYSD) \
  { \
  THREADED_OSTRACE("THREADED_INITVARS, SysD=%p\n", SYSD) \
  }
#else
#define THREADED_THREAD_INIT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_INITVARS, SysD=%p\n", SYSD) \
  SYSD->xInputPortMutex = CreateSemaphore(NULL, 1, MAX_INT, NULL); \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%p\n",SYSD->xInputPortMutex,SYSD); \
  SYSD->xInputPortCond = CreateSemaphore(NULL, 0, MAX_INT, NULL);  \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%p\n",SYSD->xInputPortCond,SYSD); \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADVXWORKS
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_INIT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_INITVARS, SysD=%p\n", SYSD) \
  }
#else
#define THREADED_THREAD_INIT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_INITVARS, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT sem_init(&SYSD->xInputPortMutex, 0, 1); \
  THREADED_ERROR_REPORT("Error in sem_init, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADOSE
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_INIT(SYSD) \
  { \
  THREADED_OSTRACE("THREADED_INITVARS, SysD=%p\n", SYSD) \
  }
#else
#define THREADED_THREAD_INIT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_INITVARS, SysD=%p\n", SYSD) \
  SYSD->xInputPortMutex = create_sem(1); \
  if(SYSD->xInputPortMutex == NULL)  { \
    THREADED_ERROR_REPORT("Error in sem_init, i=%d, SysD=%p\n", SYSD->xInputPortMutex, SYSD) \
    } \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#else
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_INIT(SYSD) \
  { \
  THREADED_OSTRACE("THREADED_INITVARS, SysD=%p\n", SYSD) \
  }
#else
#define THREADED_THREAD_INIT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_INITVARS, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT pthread_mutex_init(&SYSD->xInputPortMutex, 0); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_init, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_ERROR_RESULT pthread_cond_init(&SYSD->xInputPortCond, 0); \
  THREADED_ERROR_REPORT("Error in pthread_cond_init, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif

/* First statement in a thread. Placed first in xMainLoop in sctsdl.c */
#ifdef THREADWIN32
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_BEGINNING(SYSD) \
  THREADED_ERROR_VAR \
  MSG win32Mess; \
  THREADED_ERROR_RESULT  PeekMessage(&win32Mess, NULL, WM_USER, WM_USER, PM_NOREMOVE); \
  THREADED_ERROR_RESULT ReleaseSemaphore(((xSystemData *)SYSD)->xInitQueue, 1 , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in ReleaseSemaphore, i=%d, SysD=%d\n", i, 0) \
  if (xInitPhase) { \
    xNumberOfThreads++; \
    /*printf("xNumberOfThreads = %d\n",xNumberOfThreads);*/\
    WaitForSingleObject(xInitSem, INFINITE); \
  }
#else
#define THREADED_THREAD_BEGINNING(SYSD) \
  if (xInitPhase) { \
    xNumberOfThreads++; \
    /*printf("xNumberOfThreads = %d\n",xNumberOfThreads);*/\
    WaitForSingleObject(xInitSem, INFINITE); \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADVXWORKS
#define THREADED_THREAD_BEGINNING(SYSD) \
  if (xInitPhase) { \
    sem_wait(&xInitSem); \
  }
#elif THREADOSE
#define THREADED_THREAD_BEGINNING(SYSD) \
  union SIGNAL * xStartupSignal; \
  xSystemData * SysD; \
  xStartupSignal = receive((SIGSELECT *)AllSignals);  \
  SysD = xStartupSignal->SysD; \
  if (xInitPhase) { \
    xNumberOfThreads++; \
    wait_sem(xInitSem); \
  }
#elif THREADSOLARIS
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_THREAD_BEGINNING(SYSD) \
  mqd_t QueueId; \
  struct mq_attr test ; \
  QueueId = mq_open(((xSystemData *)SYSD)->xInputPort, O_RDWR, 0, 0); \
  mq_getattr (QueueId , & test) ; \
  mq_close(QueueId); \
  if (xInitPhase) { \
    xNumberOfThreads++; \
    sem_wait(&xInitSem); \
  }
#else
#define THREADED_THREAD_BEGINNING(SYSD) \
  if (xInitPhase) { \
    xNumberOfThreads++; \
    sem_wait(&xInitSem); \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif /* THREADVXWORKS */

/* Critical region/semaphore to protect SysD.xNewSignals.
   Declarations in THREADED_THREAD_VARS */
#ifdef THREADWIN32
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_LOCK_INPUTPORT(SYSD)
#else
#define THREADED_LOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_LOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT WaitForSingleObject(SYSD->xInputPortMutex,INFINITE); \
  THREADED_ERROR_REPORT("Error in WaitForSingleObject, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADVXWORKS
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_LOCK_INPUTPORT(SYSD)
#else
#define THREADED_LOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_LOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT sem_wait(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in sem_wait, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADOSE
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_LOCK_INPUTPORT(SYSD)
#else
#define THREADED_LOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_LOCK_INPUTPORT, SysD=%p\n", SYSD) \
  wait_sem(SYSD->xInputPortMutex); \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#else
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_LOCK_INPUTPORT(SYSD)
#else
#define THREADED_LOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_LOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT pthread_mutex_lock(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_lock, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif

#ifdef THREADWIN32
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_UNLOCK_INPUTPORT(SYSD)
#else
#define THREADED_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT ReleaseSemaphore(SYSD->xInputPortMutex, 1 , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in ReleaseSemaphore, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADVXWORKS
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_UNLOCK_INPUTPORT(SYSD)
#else
#define THREADED_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT sem_post(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in sem_post, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADOSE
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_UNLOCK_INPUTPORT(SYSD)
#else
#define THREADED_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  signal_sem(SYSD->xInputPortMutex); \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#else
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_UNLOCK_INPUTPORT(SYSD)
#else
#define THREADED_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT pthread_mutex_unlock(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_unlock, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif /* THREADVXWORKS */

/* Wait for new signal inserted into xNewSignals or for next timer.
   Unlock critical region/semaphore so other threads can reach the
   xNewSignals queue. */
#ifdef THREADWIN32
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_WAIT_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  SDL_Duration TimeoutTime; \
  xSignalNode S; \
  char * Buffer; \
  UINT win32Timer; \
  MSG win32Mess; \
  UINT TimeoutTimeMSec; \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_WAIT_INPUTPORT, SysD=%p\n", SYSD) \
  if (SYSD->xTimerQueue->Suc == SYSD->xTimerQueue) { \
    THREADED_ERROR_RESULT (unsigned long)GetMessage(&win32Mess,(HWND)NULL,0,0); \
    THREADED_ERROR_REPORT_NEG("Error in GetMessage, i=%d, SysD=%p\n", i, SYSD) \
    S = (xSignalNode)win32Mess.wParam; \
    S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
    S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
  } else { \
    TimeoutTime = xMinus_SDL_Duration(((xTimerNode)SYSD->xTimerQueue->Suc)->TimerTime,SDL_Now()); \
    TimeoutTimeMSec = TimeoutTime.s * SDT_TICKPERSEC + TimeoutTime.ns/SDT_NANOSECPERTICK; \
    win32Timer = SetTimer((HWND)NULL, 0, TimeoutTimeMSec, (TIMERPROC) NULL ); \
    THREADED_ERROR_RESULT (unsigned long)GetMessage(&win32Mess,(HWND)NULL,0,0);\
    THREADED_ERROR_REPORT_NEG("Error in GetMessage, i=%d, SysD=%p\n", i, SYSD) \
    KillTimer((HWND) NULL, win32Timer); \
    if (win32Mess.message != WM_TIMER) { \
      S = (xSignalNode)(win32Mess.wParam); \
      S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
      S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
      S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
      S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
      } \
    } \
  }
#else
#define THREADED_WAIT_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  SDL_Duration TimeoutTime; \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT ReleaseSemaphore(SYSD->xInputPortMutex,1,NULL); \
  THREADED_ERROR_REPORT_NULL("Error in ReleaseSemaphore, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_OSTRACE("THREADED_WAIT_AND_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  if (SYSD->xTimerQueue->Suc == SYSD->xTimerQueue) { \
    THREADED_ERROR_RESULT (unsigned long)(WaitForSingleObject(SYSD->xInputPortCond,INFINITE)); \
    THREADED_ERROR_REPORT("Error in waitForSingleObject, i=%d, SysD=%p\n", i, SYSD) \
  } else { \
    TimeoutTime = xMinus_SDL_Duration(((xTimerNode)SYSD->xTimerQueue->Suc)->TimerTime,SDL_Now()); \
    THREADED_ERROR_RESULT (unsigned long)(WaitForSingleObject(SYSD->xInputPortCond, \
         TimeoutTime.s*1000 + TimeoutTime.ns/1000000)); \
    THREADED_ERROR_REPORT_WAIT("Error in waitForSingleObject, i=%d, SysD=%p\n", i, SYSD) \
    } \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADVXWORKS
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_WAIT_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  SDL_Duration TimeoutTime; \
  xSignalNode S; \
  THREADED_ERROR_VAR \
  char * Buffer; \
  Buffer = xAlloc(sizeof(xSignalNode)); \
  THREADED_OSTRACE("THREADED_WAIT_INPUTPORT, SysD=%p\n", SYSD); \
  if (SYSD->xTimerQueue->Suc == SYSD->xTimerQueue) { \
    THREADED_ERROR_RESULT msgQReceive(SYSD->xThreadId, (char*)Buffer, sizeof(xSignalNode), WAIT_FOREVER); \
    THREADED_ERROR_REPORT("Error in msgQReceive, i=%d, SysD=%p\n", i, SYSD) \
    S = ((xSignalNode)(Buffer))->Pre; \
    S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
    S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
  } else { \
    int i; \
    TimeoutTime = xMinus_SDL_Duration(((xTimerNode)SYSD->xTimerQueue->Suc)->TimerTime,SDL_Now()); \
    i = msgQReceive(SYSD->xThreadId, (char*)Buffer, sizeof(xSignalNode), \
         TimeoutTime.s*SDT_TICKPERSEC + TimeoutTime.ns/SDT_NANOSECPERTICK); \
    THREADED_ERROR_REPORT_WAIT("Error in msgQReceive, i=%d, SysD=%p\n", i, SYSD) \
    if(i != ERROR) { \
      S = ((xSignalNode)(Buffer))->Pre; \
      S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
      S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
      S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
      S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
      } \
    } \
    xFree((void **)&Buffer); \
  }
#else
#define THREADED_WAIT_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  SDL_Duration TimeoutTime; \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT sem_post(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in sem_post, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_OSTRACE("THREADED_WAIT_AND_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  if (SYSD->xTimerQueue->Suc == SYSD->xTimerQueue) { \
    THREADED_ERROR_RESULT msgQReceive(SYSD->xThreadId,&SYSD->xInputPortSignal, 1, WAIT_FOREVER); \
    THREADED_ERROR_REPORT("Error in msgQReceive, i=%d, SysD=%p\n", i, SYSD) \
  } else { \
    TimeoutTime = xMinus_SDL_Duration(((xTimerNode)SYSD->xTimerQueue->Suc)->TimerTime,SDL_Now()); \
    THREADED_ERROR_RESULT msgQReceive(SYSD->xThreadId, &SYSD->xInputPortSignal, 1, \
         TimeoutTime.s*SDT_TICKPERSEC + TimeoutTime.ns/SDT_NANOSECPERTICK); \
    THREADED_ERROR_REPORT_WAIT("Error in msgQReceive, i=%d, SysD=%p\n", i, SYSD) \
    } \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADOSE
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_WAIT_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  xSignalNode S; \
  SDL_Duration TimeoutTime; \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_WAIT_INPUTPORT, SysD=%p\n", SYSD) \
  if (SYSD->xTimerQueue->Suc == SYSD->xTimerQueue) { \
    SYSD->xInputPortSignal = receive((SIGSELECT *)AllSignals); \
    S = (xSignalNode)(SYSD->xInputPortSignal->SDLSig); \
    S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
    S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
    free_buf(&SYSD->xInputPortSignal); \
  } else { \
    TimeoutTime = xMinus_SDL_Duration(((xTimerNode)SYSD->xTimerQueue->Suc)->TimerTime,SDL_Now()); \
    SYSD->xInputPortSignal = receive_w_tmo(TimeoutTime.s*SDT_TICKPERSEC + TimeoutTime.ns/SDT_NANOSECPERTICK, (SIGSELECT *)AllSignals); \
    if(SYSD->xInputPortSignal !=NIL) { \
      S = (xSignalNode)(SYSD->xInputPortSignal->SDLSig); \
      S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
      S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
      S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
      S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
      free_buf(&SYSD->xInputPortSignal); \
     } \
    } \
  }
#else
#define THREADED_WAIT_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  SDL_Duration TimeoutTime; \
  THREADED_ERROR_VAR \
  signal_sem(SYSD->xInputPortMutex); \
  THREADED_OSTRACE("THREADED_WAIT_AND_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  if (SYSD->xTimerQueue->Suc == SYSD->xTimerQueue) { \
    SYSD->xInputPortSignal = receive((SIGSELECT *)AllSignals); \
    free_buf(&SYSD->xInputPortSignal); \
  } else { \
    TimeoutTime = xMinus_SDL_Duration(((xTimerNode)SYSD->xTimerQueue->Suc)->TimerTime,SDL_Now()); \
    SYSD->xInputPortSignal = receive_w_tmo(TimeoutTime.s*SDT_TICKPERSEC + TimeoutTime.ns/SDT_NANOSECPERTICK, (SIGSELECT *)AllSignals); \
    if(SYSD->xInputPortSignal != NIL) \
      free_buf(&SYSD->xInputPortSignal); \
    } \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#else
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_WAIT_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
    THREADED_ERROR_VAR \
    SDL_Duration TimeoutTime; \
    xSignalNode S; \
    char * Buffer; \
    mqd_t QueueId; \
    Buffer = xAlloc(sizeof(xSignalNode)); \
    THREADED_OSTRACE("THREADED_WAIT_INPUTPORT, SysD=%p\n", SYSD) \
    QueueId = mq_open(SYSD->xInputPort,O_RDWR, 0, 0); \
    THREADED_ERROR_REPORT_OPEN_NEG("Error in mq_open, i=%d, SysD=%p\n", QueueId, SYSD) \
    if (SYSD->xTimerQueue->Suc == SYSD->xTimerQueue) { \
      THREADED_ERROR_RESULT mq_receive(QueueId, Buffer, sizeof(xSignalNode), NULL); \
      THREADED_ERROR_REPORT_NEG("Error in mq_receive, i=%d, SysD=%p\n", i, SYSD) \
      THREADED_ERROR_RESULT mq_close(QueueId); \
      THREADED_ERROR_REPORT("Error in mq_close, i=%d, SysD=%p\n", i, SYSD) \
      S = ((xSignalNode)(Buffer))->Pre; \
      S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
      S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
      S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
      S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
    } else { \
      int i; \
      THREADED_ERROR_RESULT timer_create(CLOCK_REALTIME, NULL, &SYSD->TimerId);; \
      THREADED_ERROR_REPORT("Error in timer_create, i=%d, SysD=%p\n", i, SYSD) \
      TimeoutTime = xMinus_SDL_Duration(((xTimerNode)SYSD->xTimerQueue->Suc)->TimerTime,SDL_Now()); \
      SYSD->timerval.it_value.tv_sec = TimeoutTime.s; \
      SYSD->timerval.it_value.tv_nsec = TimeoutTime.ns; \
      THREADED_ERROR_RESULT timer_settime(SYSD->TimerId, 0, &SYSD->timerval, NULL);; \
      THREADED_ERROR_REPORT("Error in timer_create, i=%d, SysD=%p\n", i, SYSD) \
      i = mq_receive(QueueId, Buffer, sizeof(xSignalNode), NULL); \
      THREADED_ERROR_REPORT_WAIT_NEG("Error in mq_receive, i=%d, SysD=%p\n", i, SYSD) \
      if(i != -1) { \
        S = ((xSignalNode)(Buffer))->Pre; \
        S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
        S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
        S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
        S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
      } \
      THREADED_ERROR_RESULT timer_delete(SYSD->TimerId); \
      THREADED_ERROR_REPORT("Error in timer_delete, i=%d, SysD=%p\n", i, SYSD) \
      THREADED_ERROR_RESULT mq_close(QueueId); \
      THREADED_ERROR_REPORT("Error in mq_close, i=%d, SysD=%p\n", i, SYSD) \
    } \
    xFree((void **)&Buffer); \
  }
#else
#define THREADED_WAIT_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_WAIT_AND_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  if (SYSD->xTimerQueue->Suc == SYSD->xTimerQueue) { \
    THREADED_ERROR_RESULT pthread_cond_wait(&SYSD->xInputPortCond, &SYSD->xInputPortMutex); \
    THREADED_ERROR_REPORT("Error in pthread_cond_wait, i=%d, SysD=%p\n", i, SYSD) \
  } else { \
    THREADED_ERROR_RESULT pthread_cond_timedwait(&SYSD->xInputPortCond, &SYSD->xInputPortMutex, \
         (struct timespec *)&(((xTimerNode)SYSD->xTimerQueue->Suc)->TimerTime)); \
    THREADED_ERROR_REPORT_WAIT("Error in pthread_cond_timedwait, i=%d, SysD=%p\n", i, SYSD) \
  } \
  THREADED_ERROR_RESULT pthread_mutex_unlock(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_unlock, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif

/* Signal that new signal is inserted for thread SysD.
   Unlock critical region/semaphore protecting xNewSignals. */
#ifdef THREADWIN32
#define THREADED_SIGNAL_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_SIGNAL_AND_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT ReleaseSemaphore(SYSD->xInputPortMutex , 1 , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in ReleaseSemaphore, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_ERROR_RESULT ReleaseSemaphore(SYSD->xInputPortCond , 1 , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in ReleaseSemaphore, i=%d, SysD=%p\n", i, SYSD) \
  }
#elif THREADVXWORKS
#define THREADED_SIGNAL_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_SIGNAL_AND_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT msgQSend(SYSD->xThreadId,Message,1,NO_WAIT,MSG_PRI_NORMAL); \
  THREADED_ERROR_REPORT("Error in msgQSend, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_ERROR_RESULT sem_post(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in sem_post, i=%d, SysD=%p\n", i, SYSD) \
  }
#elif THREADOSE
#define THREADED_SIGNAL_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_SIGNAL_AND_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  send(&sig , SYSD->xThreadId); \
  signal_sem(SYSD->xInputPortMutex); \
  }
#else
#define THREADED_SIGNAL_AND_UNLOCK_INPUTPORT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_SIGNAL_AND_UNLOCK_INPUTPORT, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT pthread_cond_signal(&SYSD->xInputPortCond); \
  THREADED_ERROR_REPORT("Error in pthread_cond_signal, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_ERROR_RESULT pthread_mutex_unlock(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_unlock, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif

/* Critical region/semaphore to protect active and avail lists.
   Declaration in THREADED_GLOBAL_VARS
   Initialization in THREADED_GLOBAL_INIT
   Usage in THREADED_LIST (READ_START | WRITE_START | ACCESS_END)  */
#ifdef THREADWIN32
#define THREADED_LISTREAD_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT WaitForSingleObject(xListMutex, INFINITE); \
  THREADED_ERROR_REPORT("Error in WaitForSingleObject, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADVXWORKS
#define THREADED_LISTREAD_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT sem_wait(&xListMutex); \
  THREADED_ERROR_REPORT("Error in sem_wait, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADOSE
#define THREADED_LISTREAD_START \
  { \
  THREADED_ERROR_VAR \
  wait_sem(xListMutex); \
  }
#else
#define THREADED_LISTREAD_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT pthread_mutex_lock(&xListMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_lock, i=%d, SysD=%d\n", i, 0) \
  }
#endif /* THREADVXWORKS */

#ifdef THREADWIN32
#define THREADED_LISTWRITE_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT WaitForSingleObject(xListMutex, INFINITE); \
  THREADED_ERROR_REPORT("Error in WaitForSingleObject, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADVXWORKS
#define THREADED_LISTWRITE_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT sem_wait(&xListMutex); \
  THREADED_ERROR_REPORT("Error in sem_wait, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADOSE
#define THREADED_LISTWRITE_START \
  { \
  THREADED_ERROR_VAR \
  wait_sem(xListMutex); \
  }
#else
#define THREADED_LISTWRITE_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT pthread_mutex_lock(&xListMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_lock, i=%d, SysD=%d\n", i, 0) \
  }
#endif /* THREADVXWORKS */

#ifdef THREADWIN32
#define THREADED_LISTACCESS_END \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT ReleaseSemaphore(xListMutex, 1 , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in ReleaseSemaphore, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADVXWORKS
#define THREADED_LISTACCESS_END \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT sem_post(&xListMutex); \
  THREADED_ERROR_REPORT("Error in sem_post, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADOSE
#define THREADED_LISTACCESS_END \
  { \
  THREADED_ERROR_VAR \
  signal_sem(xListMutex); \
  }
#else
#define THREADED_LISTACCESS_END \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT pthread_mutex_unlock(&xListMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_unlock, i=%d, SysD=%d\n", i, 0) \
  }
#endif

/* Critical region/semaphore to protect import and export actions
   Declaration in THREADED_GLOBAL_VARS
   Initialization in THREADED_GLOBAL_INIT
   Usage in THREADED_EXPORT_START and THREADED_EXPORT_END */
#ifdef THREADWIN32
#define THREADED_EXPORT_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT WaitForSingleObject(xExportMutex, INFINITE); \
  THREADED_ERROR_REPORT("Error in WaitForSingleObject, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADVXWORKS
#define THREADED_EXPORT_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT sem_wait(&xExportMutex); \
  THREADED_ERROR_REPORT("Error in sem_wait, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADOSE
#define THREADED_EXPORT_START \
  { \
  THREADED_ERROR_VAR \
  wait_sem(xExportMutex); \
  }
#else
#define THREADED_EXPORT_START \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT pthread_mutex_lock(&xExportMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_lock, i=%d, SysD=%d\n", i, 0) \
  }
#endif /* THREADVXWORKS */

#ifdef THREADWIN32
#define THREADED_EXPORT_END \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT ReleaseSemaphore(xExportMutex, 1 , NULL); \
  THREADED_ERROR_REPORT_NULL("Error in ReleaseSemaphore, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADVXWORKS
#define THREADED_EXPORT_END \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT sem_post(&xExportMutex); \
  THREADED_ERROR_REPORT("Error in sem_post, i=%d, SysD=%d\n", i, 0) \
  }
#elif THREADOSE
#define THREADED_EXPORT_END \
  { \
  THREADED_ERROR_VAR \
  signal_sem(xExportMutex); \
  }
#else
#define THREADED_EXPORT_END \
  { \
  THREADED_ERROR_VAR \
  THREADED_ERROR_RESULT pthread_mutex_unlock(&xExportMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_unlock, i=%d, SysD=%d\n", i, 0) \
  }
#endif /* THREADVXWORKS */

/* Create and start a new thread, run F(SysD) in thread */
#ifdef THREADWIN32
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_START_THREAD(F, SYSD, THREAD_STACKSIZE, THREAD_PRIO, THREAD_MAXQUEUESIZE, THREAD_MAXMESSIZE) \
  { \
  THREADED_ERROR_VAR \
  MSG win32Mess; \
  (*SYSD).xInitQueue = CreateSemaphore(NULL, 0, MAX_INT, NULL); \
  THREADED_ERROR_REPORT_NULL("Error in CreateSemaphore, i=%d, SysD=%d\n", (*SYSD).xInitQueue, 0) \
  THREADED_OSTRACE("THREADED_START_THREAD, SysD=%p\n", SYSD) \
  (*SYSD).xThreadId = CreateThread(DEF_SEC_ATTR, THREAD_STACKSIZE, (LPTHREAD_START_ROUTINE)F, SYSD, 0, (LPDWORD)&win32Mess.lParam);\
  if((*SYSD).xThreadId == NULL) { \
  THREADED_ERROR_REPORT_NULL("Error in CreateThread, i=%d, SysD=%p\n", (*SYSD).xThreadId, SYSD) \
  } \
  (*SYSD).xInputPort = (unsigned long) win32Mess.lParam; \
  THREADED_ERROR_RESULT SetThreadPriority((*SYSD).xThreadId, THREAD_PRIO); \
  THREADED_ERROR_REPORT_NULL("Error in SetThreadPriority, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_ERROR_RESULT WaitForSingleObject((*SYSD).xInitQueue, INFINITE); \
  THREADED_ERROR_REPORT_WAIT("Error in WaitForSingleObject i=%d, SysD=%p", i, SYSD); \
  CloseHandle((*SYSD).xInitQueue); \
  }
#else
#define THREADED_START_THREAD(F, SYSD, THREAD_STACKSIZE, THREAD_PRIO, THREAD_MAXQUEUESIZE, THREAD_MAXMESSIZE) \
  { \
  THREADED_ERROR_VAR \
  MSG win32Mess; \
  THREADED_OSTRACE("THREADED_START_THREAD, SysD=%p\n", SYSD) \
  (*SYSD).xThreadId = CreateThread(DEF_SEC_ATTR, THREAD_STACKSIZE, (LPTHREAD_START_ROUTINE)F, SYSD, 0, (LPDWORD)&win32Mess.lParam);\
  if((*SYSD).xThreadId == NULL) \
  THREADED_ERROR_REPORT_NULL("Error in CreateThread, i=%d, SysD=%p\n", (*SYSD).xThreadId, SYSD) \
  THREADED_ERROR_RESULT SetThreadPriority((*SYSD).xThreadId, THREAD_PRIO); \
  THREADED_ERROR_REPORT_NULL("Error in SetThreadPriority, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADVXWORKS
#define THREADED_START_THREAD(F, SYSD, THREAD_STACKSIZE, THREAD_PRIO, THREAD_MAXQUEUESIZE, THREAD_MAXMESSIZE) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_START_THREAD, SysD=%p\n", SYSD) \
  (*SYSD).xThreadId = msgQCreate(THREAD_MAXQUEUESIZE, DEFAULT_MAXMESSIZE, MSG_Q_FIFO); \
  if((*SYSD).xThreadId == NULL) \
  THREADED_ERROR_REPORT_NULL("Error in msgQCreate, i=%p, SysD=%p\n",(*SYSD).xThreadId, (*SYSD)); \
  THREADED_ERROR_RESULT taskSpawn("Dummy",THREAD_PRIO, VX_TASK_OPTIONS, THREAD_STACKSIZE, (FUNCPTR)&F, (int)SYSD, 0, 0, 0, 0, 0, 0, 0, 0, 0); \
  THREADED_ERROR_REPORT("Error in taskSpawn, i=%d, SysD=%p\n", i, SYSD) \
  xNumberOfThreads++; \
  }
#elif THREADOSE
#define THREADED_START_THREAD(F, SYSD, THREAD_STACKSIZE, THREAD_PRIO, THREAD_MAXQUEUESIZE, THREAD_MAXMESSIZE) \
  { \
  THREADED_ERROR_VAR \
  union SIGNAL *startup; \
  startup = alloc(sizeof(xSystemDataPtr),STARTUP); \
  THREADED_OSTRACE("THREADED_START_THREAD, SysD=%p\n", SYSD) \
  (*SYSD).xThreadId = create_process(OS_PRI_PROC, "Dummy", F, THREAD_STACKSIZE, THREAD_PRIO, 0, (PROCESS)0, (struct OS_redir_entry *)NULL, 0, 0); \
  if((*SYSD).xThreadId == 0) \
  THREADED_ERROR_REPORT("Error in create_proc, i=%d, SysD=%p\n", (*SYSD).xThreadId, SYSD) \
  xNumberOfThreads++; \
  startup->SysD = SYSD; \
  send(&startup,(*SYSD).xThreadId); \
  start((*SYSD).xThreadId); \
  }
#else
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_START_THREAD(F, SYSD, THREAD_STACKSIZE, THREAD_PRIO, THREAD_MAXQUEUESIZE, THREAD_MAXMESSIZE) \
  { \
  THREADED_ERROR_VAR \
  char * queuename; \
  mqd_t QueueId; \
  queuename =(char *) xAlloc(10); \
  QueueCounter++; \
  pthread_attr_init(&Attributes); \
  pthread_attr_setstacksize(&Attributes, THREAD_STACKSIZE); \
  pthread_attr_setschedpolicy(&Attributes, SCHED_OTHER); \
  pthread_attr_setdetachstate(&Attributes, PTHREAD_CREATE_DETACHED); \
  pthread_attr_setscope(&Attributes, PTHREAD_SCOPE_PROCESS); \
  mqattr.mq_maxmsg = THREAD_MAXQUEUESIZE; \
  mqattr.mq_msgsize = DEFAULT_MAXMESSIZE; \
  mqattr.mq_flags = 0; \
  snprintf(queuename, 10, "/YY%d", QueueCounter); \
  mq_unlink(queuename); \
  QueueId = mq_open(queuename, QUEUEFLAG, QUEUEMODE, &mqattr); \
  THREADED_ERROR_REPORT_OPEN_NEG("Error in mq_open, i=%d, SysD=%p\n", QueueId, SYSD) \
  (*SYSD).xInputPort = queuename; \
  THREADED_ERROR_RESULT mq_close(QueueId); \
  THREADED_ERROR_REPORT_NEG("Error in mq_close, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_OSTRACE("THREADED_START_THREAD, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT pthread_create(&(SYSD)->xThreadId, &Attributes, F, (void *)SYSD); \
  THREADED_ERROR_REPORT("Error in pthread_create, i=%d, SysD=%p\n", i, SYSD) \
  }
#else
#define THREADED_START_THREAD(F, SYSD, THREAD_STACKSIZE, THREAD_PRIO, THREAD_MAXQUEUESIZE, THREAD_MAXMESSIZE) \
  { \
  THREADED_ERROR_VAR \
  pthread_attr_init(&Attributes); \
  pthread_attr_setstacksize(&Attributes, THREAD_STACKSIZE); \
  pthread_attr_setschedpolicy(&Attributes, SCHED_OTHER); \
  pthread_attr_setdetachstate(&Attributes, PTHREAD_CREATE_DETACHED); \
  pthread_attr_setscope(&Attributes, PTHREAD_SCOPE_PROCESS); \
  THREADED_OSTRACE("THREADED_START_THREAD, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT pthread_create(&(SYSD)->xThreadId, &Attributes, F, (void *)SYSD); \
  THREADED_ERROR_REPORT("Error in pthread_create, i=%d, SysD=%p\n", i, SYSD) \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif /* THREADVXWORKS */

/* Stop current thread which is represented by SysD.
   Used only for stop of process instances running in a thread of their own. */
#ifdef THREADWIN32
#define THREADED_STOP_THREAD(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_STOP_THREAD, SysD=%p\n", SYSD) \
  ExitThread(0); \
  }
#elif THREADVXWORKS
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_STOP_THREAD(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_STOP_THREAD, SysD=%p\n", SYSD) \
  msgQDelete(SYSD->xThreadId); \
  taskDelete(taskIdSelf()); \
  }
#else
#define THREADED_STOP_THREAD(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_STOP_THREAD, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT sem_destroy(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in sem_destroy, i=%d, SysD=%p\n", i, SYSD) \
  msgQDelete(SYSD->xThreadId); \
  taskDelete(taskIdSelf()); \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#elif THREADOSE
#define THREADED_STOP_THREAD(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_STOP_THREAD, SysD=%p\n", SYSD) \
  kill_proc(current_process()); \
  }
#else
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#define THREADED_STOP_THREAD(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_STOP_THREAD, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT mq_unlink(SYSD->xInputPort); \
  THREADED_ERROR_REPORT("Error in mq_unlink, i=%d, SysD=%p\n", i, SYSD) \
  xFree((void **)&(SYSD->xInputPort)); \
  pthread_exit(0); \
  }
#else
#define THREADED_STOP_THREAD(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_STOP_THREAD, SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT pthread_mutex_destroy(&SYSD->xInputPortMutex); \
  THREADED_ERROR_REPORT("Error in pthread_mutex_destroy, i=%d, SysD=%p\n", i, SYSD) \
  THREADED_ERROR_RESULT pthread_cond_destroy(&SYSD->xInputPortCond); \
  THREADED_ERROR_REPORT("Error in pthread_cond_destroy, i=%d, SysD=%p\n", i, SYSD) \
  pthread_exit(0); \
  }
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif

/* Actions after all threads has started */
#ifdef THREADWIN32
#define THREADED_MSC_TMO -1
#define THREADED_SUSPEND_THREAD \
  SuspendThread(GetCurrentThread());
#ifdef THREADED_MSCTRACE
#define THREADED_MSC_INIT_SEM \
  xSendMSCTrace = CreateSemaphore(NULL, 1, MAX_INT, NULL); \
  xWaitMSCResp  = CreateSemaphore(NULL, 0, MAX_INT, NULL);
#define THREADED_MSC_SIGNAL_SEM \
  THREADED_ERROR_RESULT ReleaseSemaphore(xWaitMSCResp, 1, NULL); \
  THREADED_ERROR_REPORT_NULL("Error in ReleaseSemaphore, i=%d, SysD=%d\n", i, 0)
#define THREADED_TAKE_SEM \
  if(!startup_msc) { \
   WaitForSingleObject(xSendMSCTrace, INFINITE); \
  }
#define THREADED_WAIT_SEM_REL \
  if(!startup_msc) { \
    WaitForSingleObject(xWaitMSCResp, INFINITE); \
    ReleaseSemaphore(xSendMSCTrace, 1, NULL); \
  }
#else
#define THREADED_MSC_INIT_SEM
#define THREADED_MSC_SIGNAL_SEM
#define THREADED_WAIT_SEM_REL
#endif /* THREADED_MSCTRACE */
#define THREADED_MSC_SET_PRI
#define THREADED_AFTER_THREAD_START \
  THREADED_MSC_INIT \
  { \
  int i; \
  THREADED_OSTRACE("THREADED_AFTER_THREAD_START, %d threads\n", xNumberOfThreads); \
  for (i=0; i<=xNumberOfThreads; i++) ReleaseSemaphore(xInitSem, 1 , NULL); \
  xInitPhase = 0; \
} \
THREADED_MSC_PM_RESP
#ifdef THREADED_SIMPLE_EXAMPLE
#define THREADED_START_EXTTASK \
{ \
  extern void * MyExtTask1(void *); \
  extern void * MyExtTask2(void *); \
  CreateThread(NULL,20480,(LPTHREAD_START_ROUTINE)MyExtTask1,0,0,0); \
  CreateThread(NULL,20480,(LPTHREAD_START_ROUTINE)MyExtTask2,0,0,0); \
}
#else
#ifndef THREADED_START_EXTTASK
#define THREADED_START_EXTTASK
#endif
#endif 

#elif THREADVXWORKS
#define THREADED_MSC_TMO -1
#define THREADED_SUSPEND_THREAD \
  taskSuspend(taskIdSelf());
#ifdef THREADED_MSCTRACE
#define THREADED_MSC_INIT_SEM \
  sem_init(&xSendMSCTrace,0,1); \
  sem_init(&xWaitMSCResp,0,0);
#define THREADED_MSC_SIGNAL_SEM \
  THREADED_ERROR_RESULT sem_post(&xWaitMSCResp); \
  THREADED_ERROR_REPORT_NULL("Error in signal_sem, i=%d, SysD=%d\n", i, 0)
#define THREADED_TAKE_SEM \
  if(!startup_msc) { \
    sem_wait(&xSendMSCTrace); \
  }
#define THREADED_WAIT_SEM_REL \
  if(!startup_msc) { \
    sem_wait(&xWaitMSCResp); \
    sem_post(&xSendMSCTrace); \
  }
#define THREADED_MSC_SET_PRI
#else
#define THREADED_MSC_INIT_SEM
#define THREADED_MSC_SIGNAL_SEM
#define THREADED_WAIT_SEM_REL
#endif /* THREADED_MSCTRACE */
#define THREADED_AFTER_THREAD_START \
  THREADED_MSC_INIT \
  { \
  int i; \
  THREADED_OSTRACE("THREADED_AFTER_THREAD_START, %d threads\n", xNumberOfThreads); \
  for (i=0; i<=xNumberOfThreads; i++) sem_post(&xInitSem); \
  xInitPhase = 0; \
  } \
THREADED_MSC_PM_RESP
#ifdef THREADED_SIMPLE_EXAMPLE
#define THREADED_START_EXTTASK \
{ \
  extern void * MyExtTask1(void *); \
  extern void * MyExtTask2(void *); \
  taskSpawn("Dummy", DEFAULT_PRIO, VX_TASK_OPTIONS, DEFAULT_STACKSIZE, (FUNCPTR)&MyExtTask1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); \
  taskSpawn("Dummy", DEFAULT_PRIO, VX_TASK_OPTIONS, DEFAULT_STACKSIZE, (FUNCPTR)&MyExtTask2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); \
}
#else
#ifndef THREADED_START_EXTTASK
#define THREADED_START_EXTTASK
#endif
#endif /* THREADED_SIMPLE_EXAMPLE */

#elif THREADOSE
#define THREADED_MSC_TMO 0
#define THREADED_SUSPEND_THREAD \
  stop(current_process());
#ifdef THREADED_MSCTRACE
#define THREADED_MSC_SET_PRI \
  set_pri(31);
#define THREADED_MSC_INIT_SEM \
  xSendMSCTrace = create_sem(1); \
  xWaitMSCResp  = create_sem(0);
#define THREADED_MSC_SIGNAL_SEM \
  THREADED_ERROR_RESULT signal_sem(xWaitMSCResp); \
  THREADED_ERROR_REPORT_NULL("Error in signal_sem, i=%d, SysD=%d\n", i, 0) \
  delay(1);
#define THREADED_TAKE_SEM \
  if(!startup_msc) { \
    wait_sem(xSendMSCTrace); \
  }
#define THREADED_WAIT_SEM_REL \
  if(!startup_msc) { \
    wait_sem(xWaitMSCResp); \
    signal_sem(xSendMSCTrace); \
  }
#else
#define THREADED_MSC_INIT_SEM
#define THREADED_MSC_SIGNAL_SEM
#define THREADED_WAIT_SEM_REL
#endif /* THREADED_MSCTRACE */

#define THREADED_AFTER_THREAD_START \
  THREADED_MSC_INIT \
  { \
  int i; \
  THREADED_OSTRACE("THREADED_AFTER_THREAD_START, %d threads\n", xNumberOfThreads); \
  for (i=0; i<=xNumberOfThreads; i++) signal_sem(xInitSem); \
  xInitPhase = 0; \
} \
THREADED_MSC_PM_RESP
#ifdef THREADED_SIMPLE_EXAMPLE
#define THREADED_START_EXTTASK \
{ \
  extern OSENTRYPOINT  MyExtTask1; \
  extern OSENTRYPOINT  MyExtTask2; \
  PROCESS xThreadId1; \
  PROCESS xThreadId2; \
  xThreadId1 = create_process(OS_PRI_PROC, "Dummy", MyExtTask1, 1024, 10, 0, (PROCESS)0, (struct OS_redir_entry *)NULL, 0, 0); \
  xThreadId2 =create_process(OS_PRI_PROC, "Dummy", MyExtTask2, 1024, 10, 0, (PROCESS)0, (struct OS_redir_entry *)NULL, 0, 0); \
  start(xThreadId1); \
  start(xThreadId2); \
}
#else
#ifndef THREADED_START_EXTTASK
#define THREADED_START_EXTTASK
#endif
#endif /* THREADED_SIMPLE_EXAMPLE */

#else /* THREADSOLARIS */
#define THREADED_MSC_TMO 1
#define THREADED_SUSPEND_THREAD \
  pthread_exit(0); 
#ifdef THREADED_MSCTRACE
#define THREADED_MSC_INIT_SEM \
  sem_init(&xSendMSCTrace,0,1); \
  sem_init(&xWaitMSCResp,0,0);
#define THREADED_MSC_SIGNAL_SEM \
  sem_post(&xWaitMSCResp); 
#define THREADED_TAKE_SEM \
  if(!startup_msc) { \
    sem_wait(&xSendMSCTrace); \
  }
#define THREADED_WAIT_SEM_REL \
  if(!startup_msc) { \
    sem_wait(&xWaitMSCResp); \
    sem_post(&xSendMSCTrace); \
  }
#define THREADED_MSC_SET_PRI
#else /* THREADED_MSCTRACE */
#define THREADED_MSC_INIT_SEM
#define THREADED_MSC_SIGNAL_SEM
#define THREADED_WAIT_SEM_REL
#endif /* THREADED_MSCTRACE */
#ifdef THREADED_SIMPLE_EXAMPLE
#define THREADED_START_EXTTASK \
{ \
  extern void * MyExtTask1(void *); \
  extern void * MyExtTask2(void *); \
  pthread_t threadId; \
  pthread_create(&threadId,0,MyExtTask1,0); \
  pthread_create(&threadId,0,MyExtTask2,0); \
}
#else
#ifndef THREADED_START_EXTTASK
#define THREADED_START_EXTTASK
#endif 
#endif /* THREADED_SIMPLE_EXAMPLE */
#define THREADED_AFTER_THREAD_START \
  THREADED_MSC_INIT \
  { \
  int i; \
  THREADED_OSTRACE("THREADED_AFTER_THREAD_START, %d threads\n", xNumberOfThreads); \
  xInitPhase = 0; \
  for (i=0; i<=xNumberOfThreads; i++) sem_post(&xInitSem); \
  } \
 THREADED_MSC_PM_RESP 
#endif /* THREADWIN32 */

#ifdef THREADED_MSCTRACE
extern int PMRead(int timeOut, int *sender, int *message, void **data, int *len);

extern int startup_msc;
#define THREADED_MSC_PM_RESP \
{ \
int sender; \
void * data = NULL; \
int len=0; \
int messtype; \
startup_msc = 0; \
THREADED_START_EXTTASK \
if (XSYSD xNoticeBoard.MSCEStarted) { \
THREADED_MSC_SET_PRI \
while (1) { \
PMRead(THREADED_MSC_TMO, &sender, &messtype, &data, &len);\
THREADED_MSC_SIGNAL_SEM \
} \
} else \
  THREADED_SUSPEND_THREAD \
}
#define THREADED_MSC_INIT \
  xStartMSC(); \
  if (XSYSD xNoticeBoard.MSCEStarted) { \
    XSYSD MSCLogStarted =1; \
    XSYSD MSCETrace = 2; \
    XSYSD MSCSymbolLevel = 2; \
    xMSCEInit(); \
  } \
  THREADED_MSC_INIT_SEM
#else
#define THREADED_MSC_PM_RESP \
  THREADED_START_EXTTASK \
  THREADED_SUSPEND_THREAD
#define THREADED_MSC_INIT
#endif /* THREADED_MSCTRACE */

#ifdef THREADWIN32

#elif THREADVXWORKS

#elif THREADOSE
 
#else /* THREADSOLARIS */

#endif /* THREADVXWORKS */

#ifdef THREADED_MSCTRACE

#else

#endif /* THREADED_MSCTRACE */

#ifdef XSCT_CBASIC
C-Basic generator cannot be used for applications;
#endif

#if defined(XMONITOR)
OS-integration can only be pure application;
#endif

/* Beginning of section implementing alternativ signalsending using VxWorks signals instead of semaphores. Semaphores are not needed when sending signal directly to the VxWorks Queue instead of putting the signal in the receiving process inputqueue. This feature is enabled with the switch THREADED_ALTERNATIVE_SIGNAL_SENDING*/
#ifdef THREADED_ALTERNATIVE_SIGNAL_SENDING
#ifdef THREADWIN32
#define THREADED_SEND_OUTPUT(SYSD) \
  { \
  MSG win32Mess; \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_SEND_OUTPUT,SysD=%p\n", SYSD) \
  win32Mess.lParam = SYSD->xInputPort; \
  win32Mess.wParam = (WPARAM)S; \
  THREADED_ERROR_RESULT PostThreadMessage(win32Mess.lParam, WM_USER+1, win32Mess.wParam, win32Mess.lParam); \
  THREADED_ERROR_REPORT_NULL("Error in postThreadMessage i=%d, SysD=%p\n", i, SYSD) \
  }
#elif THREADVXWORKS
#define THREADED_SEND_OUTPUT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  THREADED_OSTRACE("THREADED_SEND_OUTPUT,SysD=%p\n", SYSD) \
  THREADED_ERROR_RESULT msgQSend(SYSD->xThreadId, (char *)&S, sizeof(xSignalNode),NO_WAIT,MSG_PRI_NORMAL); \
  THREADED_ERROR_REPORT("Error in msgQSend i=%d, SysD=%p\n",i,SYSD) \
  }
#elif THREADSOLARIS
#define THREADED_SEND_OUTPUT(SYSD) \
  { \
  THREADED_ERROR_VAR \
  mqd_t QueueId; \
  THREADED_OSTRACE("THREADED_SEND_OUTPUT,SysD=%p\n", SYSD) \
  QueueId = mq_open(SYSD->xInputPort, 2, 0, 0); \
  THREADED_ERROR_REPORT_OPEN_NEG("Error in mq_open i=%d, SysD=%p\n",QueueId,SYSD) \
  THREADED_ERROR_RESULT mq_send(QueueId, (char*)&S, sizeof(xSignalNode), 0); \
  THREADED_ERROR_REPORT("Error in mq_send i=%d, SysD=%p\n",i,SYSD) \
  THREADED_ERROR_RESULT mq_close(QueueId); \
  THREADED_ERROR_REPORT_NEG("Error in mq_close i=%d, SysD=%p\n",i,SYSD) \
  } 
#elif THREADOSE
#define THREADED_SEND_OUTPUT(SYSD) \
{ \
    THREADED_ERROR_VAR \
    union SIGNAL *sig; \
    THREADED_OSTRACE("THREADED_SEND_OUTPUT,SysD=%p\n", SYSD) \
    sig = alloc(sizeof(xSignalNode),SDLSIG); \
    sig->SDLSig = S; \
    send(&sig,SYSD->xThreadId); \
}
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#else
#ifdef THREADOSE
#define THREADED_SEND_OUTPUT(SYSD) \
{ \
    union SIGNAL *sig; \
    sig = alloc(sizeof(Message),MESSAGE); \
    sig->Mess[0] = 'c'; \
    THREADED_LOCK_INPUTPORT(S->Receiver.LocalPId->PrsP->SysD) \
    S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
    S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
    THREADED_SIGNAL_AND_UNLOCK_INPUTPORT(S->Receiver.LocalPId->PrsP->SysD) \
}
#else
#define THREADED_SEND_OUTPUT(SYSD) \
    THREADED_LOCK_INPUTPORT(S->Receiver.LocalPId->PrsP->SysD) \
    S->Suc = (xSignalNode)&S->Receiver.LocalPId->PrsP->SysD->xNewSignals; \
    S->Pre = S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre->Suc = S; \
    S->Receiver.LocalPId->PrsP->SysD->xNewSignals.Pre = S; \
    THREADED_SIGNAL_AND_UNLOCK_INPUTPORT(S->Receiver.LocalPId->PrsP->SysD)
#endif /* THREADOSE */
#endif /* THREADED_ALTERNATIVE_SIGNAL_SENDING */
#endif /* THREADED_POSIX_THREADS */

#ifdef THREADED_ERROR
#ifdef THREADWIN32
#define THREADED_ERROR_REPORT_NEG(P1,P2,P3)   if(P2==-1) printf(P1,P2,P3);
#define THREADED_ERROR_REPORT_NULL(P1,P2,P3)       if (P2 == NULL) {LPVOID err;printf(P1,P2,P3);FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM,NULL,GetLastError(),MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),(LPTSTR) &err,0,NULL);printf("Err=%s\n",err);}
#define THREADED_ERROR_REPORT(P1,P2,P3)       if (P2) printf(P1,P2,P3);
#define THREADED_ERROR_REPORT_WAIT(P1,P2,P3)  if (P2 == WAIT_FAILED) printf(P1,P2,P3);
#elif THREADVXWORKS
#define THREADED_ERROR_REPORT(P1,P2,P3)       if (P2 == ERROR) printf(P1,P2,P3);
#define THREADED_ERROR_REPORT_NULL(P1,P2,P3)       if (P2 == NULL) printf(P1,P2,P3);
#define THREADED_ERROR_REPORT_WAIT(P1,P2,P3)  if ((P2 == ERROR) && errno!=S_objLib_OBJ_TIMEOUT) printf(P1,P2,P3);
#else
#define THREADED_ERROR_REPORT(P1,P2,P3)       if (P2) {printf(P1,P2,P3); /*fflush(stdout);*/}
#define THREADED_ERROR_REPORT_WAIT(P1,P2,P3)  if (P2 && P2!=ETIMEDOUT) printf(P1,P2,P3);
#define THREADED_ERROR_REPORT_NEG(P1,P2,P3)  if (P2<0) {printf(P1,P2,P3);perror("ERROR");/*fflush(stdout);*/}
#define THREADED_ERROR_REPORT_OPEN_NEG(P1,P2,P3)  if (P2 == (mqd_t)(-1)) {printf(P1,P2,P3);perror("ERROR");/*fflush(stdout);*/}
#define THREADED_ERROR_REPORT_WAIT_NEG(P1,P2,P3)  if (P2<0 && errno != EINTR) {printf(P1,P2,P3);perror("ERROR");}
#endif /* THREADVXWORKS */
#define THREADED_ERROR_RESULT                 i =
#define THREADED_ERROR_VAR                    int i;
#else
#define THREADED_ERROR_REPORT_WAIT_NEG(P1,P2,P3)
#define THREADED_ERROR_REPORT_OPEN_NEG(P1,P2,P3)
#define THREADED_ERROR_REPORT_NEG(P1,P2,P3)
#define THREADED_ERROR_REPORT_NULL(P1,P2,P3)
#define THREADED_ERROR_REPORT(P1,P2,P3)
#define THREADED_ERROR_REPORT_WAIT(P1,P2,P3)
#define THREADED_ERROR_RESULT                 (void)
#define THREADED_ERROR_VAR
#endif

#ifndef THREADED_NO_API
/**************************************************************/
/*         THREADED API MACROS AND DECLARATIONS            */
/**************************************************************/
#endif 
#endif
       /* X_SCTTHLTYPES_H */
