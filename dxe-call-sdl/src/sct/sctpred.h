
#ifndef X_SCTPRED_H
#define X_SCTPRED_H


/*******************************************************************************
Copyright by Telesoft Europe AB 1990-1991.
Copyright by Telelogic Malmoe AB 1991-1994.
Copyright by Telelogic AB 1994 - 2003.
This Program is owned by Telelogic and is protected by national
copyright laws and international copyright treaties. Telelogic
grants you the right to use this Program on one computer or in
one local computer network at any one time.
Under this License you may only modify the source code for the purpose
of adapting it to your environment. You must reproduce and include
any copyright and trademark notices on all copies of the source code.
You may not use, copy, merge, modify or transfer the Program except as
provided in this License.
Telelogic does not warrant that the Program will meet your
requirements or that the operation of the Program will be
uninterrupted and error free. You are solely responsible that the
selection of the Program and the modification of the source code
will achieve your intended results and that the results are actually
obtained.
*******************************************************************************/

/*
# $FileId: sctpred.h 82 : 2003/03/07 jk
*/


/*
FILE INDEX
01   Macros used in assign functions
02   Type info nodes
03   SDL predefined data types
04   Generic implementation of operators
05   Utility functions
06   Write buffer management
07   Generic read and write function
*/


/* This file contains implementation of the predefined data types
   except PId. The functions defined here can be found in the file
   sctpred.c.
*/

#ifndef xOffsetOf
#define xOffsetOf(type, field)  ((xptrint) &((type *) 0)->field)
#endif

#include <stdarg.h>
#include <limits.h>


/****+***************************************************************
01   Macros used in assign functions
********************************************************************/

/* XASS_1_2_3 should be read:
1 = AC  : always copy 
1 = MR  : may reuse (take pointer if temporary object)
1 = AR  : always reuse (take pointer)
2 = ASS : new object assigned to "variable"
2 = TMP : new object temporary
3 = FR  : call free for old value refered to by variable
3 = NF  : do not call free for old value
*/
#define XASS_AC_ASS_FR  (int)25  /* 1+8+16 */
#define XASS_MR_ASS_FR  (int)26  /* 2+8+16 */  /* = XASS */
#define XASS_AR_ASS_FR  (int)28  /* 4+8+16 */

#define XASS_AC_TMP_FR  (int)17  /* 1+0+16 */
#define XASS_MR_TMP_FR  (int)18  /* 2+0+16 */  /* = XASSMAKE */
#define XASS_AR_TMP_FR  (int)20  /* 4+0+16 */

#define XASS_AC_ASS_NF  (int)9   /* 1+8+0  */
#define XASS_MR_ASS_NF  (int)10  /* 2+8+0  */
#define XASS_AR_ASS_NF  (int)12  /* 4+8+0  */

#define XASS_AC_TMP_NF  (int)1   /* 1+0+0  */  /* = XASS2MAKE */
#define XASS_MR_TMP_NF  (int)2   /* 2+0+0  */  /* = XASSMAKE */
#define XASS_AR_TMP_NF  (int)4   /* 4+0+0  */

#define SHOULD_COPY(P)          (P & (int)1)
#define MAY_REUSE(P)            (P & (int)2)
#define SHOULD_REUSE(P)         (P & (int)4)
#define SHOULD_RESULT_BE_ASS(P) (P & (int)8)
#define SHOULD_FREE_OLD(P)      (P & (int)16)

#define SET_ASS_NF(P)           ((P & (int)47) | (int)8)
#define SET_NF(P)               (P & (int)47)

#define SHOULD_MEMCPY(P)        (!(P & (int)32))
#define SET_MEMCPY(P)           (P | (int)32)
#define UNSET_MEMCPY(P)         (P & (int)31)


/****+***************************************************************
02   Type info nodes
********************************************************************/

/* moved her as SDL_Boolean is used by generic data model */
#if !defined(X_COMPACT_BOOL)
typedef int SDL_Boolean;
typedef int SDL_boolean;
#else
typedef unsigned char SDL_Boolean;
typedef unsigned char SDL_boolean;
#endif

/* These types are used by the generic data model, but also by the SDL
   encoders/decoders in the old data model */

typedef enum
{
  /*SDL - standard types*/
  type_SDL_Integer=128,
  type_SDL_Real=129,
  type_SDL_Natural=130,
  type_SDL_Boolean=131,
  type_SDL_Character=132,
  type_SDL_Time=133,
  type_SDL_Duration=134,
  type_SDL_Pid=135,
  type_SDL_Charstring=136,
  type_SDL_Bit=137,
  type_SDL_Bit_string=138,
  type_SDL_Octet=139,
  type_SDL_Octet_string=140,
  type_SDL_IA5String=141,
  type_SDL_NumericString=142,
  type_SDL_PrintableString=143,
  type_SDL_VisibleString=144,
  type_SDL_NULL=145,
  type_SDL_Object_identifier=146,

  /* SDL - standard ctypes */
  type_SDL_ShortInt=150,
  type_SDL_LongInt=151,
  type_SDL_UnsignedShortInt=152,
  type_SDL_UnsignedInt=153,
  type_SDL_UnsignedLongInt=154,
  type_SDL_Float=155,
  type_SDL_Charstar=156,
  type_SDL_Voidstar=157,
  type_SDL_Voidstarstar=158,

  /* SDL - user defined types */
  type_SDL_Syntype=170,
  type_SDL_Inherits=171,
  type_SDL_Enum=172,
  type_SDL_Struct=173,
  type_SDL_Union=174,
  type_SDL_UnionC=175,
  type_SDL_Choice=176,
  type_SDL_ChoicePresent=177,
  type_SDL_Powerset=178,
  type_SDL_GPowerset=179,
  type_SDL_Bag=180,
  type_SDL_String=181,
  type_SDL_LString=182,
  type_SDL_Array=183,
  type_SDL_Carray=184,
  type_SDL_GArray=185,
  type_SDL_Own=186,
  type_SDL_Oref=187,
  type_SDL_Ref=188,
  type_SDL_Userdef=189,
  type_SDL_EmptyType=190,
  type_SDL_ComBuf=191,

  /* SDL - signals */
  type_SDL_Signal=200,
  type_SDL_SignalId=201

} tSDLTypeClass;


#ifndef T_CONST
#define T_CONST const
#endif

#ifndef T_SDL_EXTRA_COMP
#define T_SDL_EXTRA_COMP
#define T_SDL_EXTRA_VALUE
#endif

#ifndef T_SDL_USERDEF_COMP
#define T_SDL_USERDEF_COMP
#endif

#if defined(XREADANDWRITEF) && !defined(T_SDL_NAMES)
#define T_SDL_NAMES
#endif

#ifdef T_SDL_NAMES
#define T_SDL_Names(P) , P
#else
#define T_SDL_Names(P)
#endif

#ifdef T_SIGNAL_SDL_NAMES
#define T_Signal_SDL_Names(P) , P
#else
#define T_Signal_SDL_Names(P)
#endif

#ifdef T_SDL_INFO
#define T_SDL_Info(P) , P
#else
#define T_SDL_Info(P)
#endif

#ifndef XNOUSE_OPFUNCS
#define T_SDL_OPFUNCS(P) , P
#else
#define T_SDL_OPFUNCS(P)
#endif


#define NEEDSFREE(P)    (((tSDLTypeInfo *)(P))->OpNeeds & (unsigned char)1)
#define NEEDSEQUAL(P)   (((tSDLTypeInfo *)(P))->OpNeeds & (unsigned char)2)
#define NEEDSASSIGN(P)  (((tSDLTypeInfo *)(P))->OpNeeds & (unsigned char)4)
#define NEEDSINIT(P)    (((tSDLTypeInfo *)(P))->OpNeeds & (unsigned char)8)

struct tSDLFuncInfo;

/* ------- General type information for SDL types ------- */
typedef T_CONST struct tSDLTypeInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
} tSDLTypeInfo;


/* ------------------ Enumeration type ------------------ */
typedef T_CONST struct {
  int             LiteralValue;
  char           *LiteralName;
} tSDLEnumLiteralInfo;

typedef T_CONST struct tSDLEnumInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLTypeInfo   *CompOrFatherSort;
#ifdef XREADANDWRITEF
  int             NoOfLiterals;
  tSDLEnumLiteralInfo *LiteralList;
#endif
} tSDLEnumInfo;


/* ---------- Syntype, Inherits, Own, Oref, Ref --------- */
typedef T_CONST struct tSDLGenInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLTypeInfo   *CompOrFatherSort;
} tSDLGenInfo;


/* ---------------------- Powerset ---------------------- */
typedef T_CONST struct tSDLPowersetInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLTypeInfo   *CompSort;
  int             Length;
  int             LowestValue;
} tSDLPowersetInfo;


/* ----------------------- Struct ----------------------- */
typedef int (*tGetFunc) (void *);
typedef void (*tAssFunc) (void *, int);

typedef T_CONST struct {
  xptrint         OffsetPresent; /* 0 if not optional */
  void           *DefaultValue;
} tSDLFieldOptInfo;

typedef T_CONST struct {
  tGetFunc        GetTag;
  tAssFunc        AssTag;
} tSDLFieldBitFInfo;


typedef T_CONST struct {
  tSDLTypeInfo   *CompSort;
#ifdef T_SDL_NAMES
  char           *Name;
#endif
  xptrint         Offset;        /* ~0 for bitfield */
  tSDLFieldOptInfo *ExtraInfo;
} tSDLFieldInfo;

typedef T_CONST struct tSDLStructInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLFieldInfo  *Components;
  int             NumOfComponents;
} tSDLStructInfo;


/* -------------------- Choice, Union ------------------- */
typedef T_CONST struct {
  tSDLTypeInfo        *CompSort;
#ifdef T_SDL_NAMES
  char                *Name;
#endif
} tSDLChoiceFieldInfo;

typedef T_CONST struct tSDLChoiceInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLChoiceFieldInfo *Components;
  int                  NumOfComponents;
  xptrint              OffsetToUnion;
  xptrint              TagSortSize;
#ifdef XREADANDWRITEF
  tSDLTypeInfo        *TagSort;
#endif
} tSDLChoiceInfo;


/* ------------------- Array, CArray -------------------- */
typedef T_CONST struct tSDLArrayInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLTypeInfo   *CompSort;
  int             Length;
#ifdef XREADANDWRITEF
  tSDLTypeInfo   *IndexSort;
#endif
#if defined(XREADANDWRITEF) || defined(XEINDEX)
  int             LowestValue;
#endif
} tSDLArrayInfo;


/* ----------------------- GArray ----------------------- */
typedef T_CONST struct tSDLGArrayInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLTypeInfo   *IndexSort;
  tSDLTypeInfo   *CompSort;
  xptrint         yrecSize;
  xptrint         yrecIndexOffset;
  xptrint         yrecDataOffset;
  xptrint         arrayDataOffset;
} tSDLGArrayInfo;


/* ----- GPowerset, Bag, String, Object_Identifier ------ */
typedef T_CONST struct tSDLGenListInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLTypeInfo   *CompSort;
  xptrint         yrecSize;
  xptrint         yrecDataOffset;
} tSDLGenListInfo;


/* ---------------------- LString ----------------------- */
typedef T_CONST struct tSDLLStringInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  tSDLTypeInfo   *CompSort;
  int             MaxLength;
  xptrint         DataOffset;
} tSDLLStringInfo;


/* ---------------------- Userdef ----------------------- */
/* used for user defined types #ADT(T(h)) */
typedef T_CONST struct tSDLUserdefInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SDL_NAMES
  char           *Name;
#endif
#ifdef XREADANDWRITEF
  xIdNode         FatherScope;
  xSortIdNode     SortIdNode;
#endif
  T_SDL_USERDEF_COMP
} tSDLUserdefInfo;


/* ----------------------- Signal ----------------------- */
typedef T_CONST struct {
  tSDLTypeInfo       *ParaSort;
  xptrint             Offset;
} tSDLSignalParaInfo;

typedef T_CONST struct tSDLSignalInfoS {
  tSDLTypeClass   TypeClass;
  unsigned char   OpNeeds;
  xptrint         SortSize;
#ifndef XNOUSE_OPFUNCS
  struct tSDLFuncInfo *OpFuncs;
#endif
  T_SDL_EXTRA_COMP
#ifdef T_SIGNAL_SDL_NAMES
  char           *Name;
#endif
  tSDLSignalParaInfo *Param;
  int             NoOfPara;
} tSDLSignalInfo;

typedef struct tSDLFuncInfo {
  void *(*AssFunc) (void *, void *, int);
  SDL_Boolean (*EqFunc) (void *, void *);
  void (*FreeFunc) (void **);
#ifdef XREADANDWRITEF
  char *(*WriteFunc) (void *);
  int (*ReadFunc) (void *);
#endif
} tSDLFuncInfo;



extern tSDLTypeInfo ySDL_SDL_Integer;
extern tSDLTypeInfo ySDL_SDL_Real;
extern tSDLTypeInfo ySDL_SDL_Natural;
extern tSDLTypeInfo ySDL_SDL_Boolean;
extern tSDLTypeInfo ySDL_SDL_Character;
extern tSDLTypeInfo ySDL_SDL_Time;
extern tSDLTypeInfo ySDL_SDL_Duration;
extern tSDLTypeInfo ySDL_SDL_PId;
extern tSDLTypeInfo ySDL_SDL_Charstring;
extern tSDLTypeInfo ySDL_SDL_Bit;
extern tSDLTypeInfo ySDL_SDL_Bit_String;
extern tSDLTypeInfo ySDL_SDL_Octet;
extern tSDLTypeInfo ySDL_SDL_Octet_String;
extern tSDLTypeInfo ySDL_SDL_IA5String;
extern tSDLTypeInfo ySDL_SDL_NumericString;
extern tSDLTypeInfo ySDL_SDL_PrintableString;
extern tSDLTypeInfo ySDL_SDL_VisibleString;
extern tSDLTypeInfo ySDL_SDL_Null;
extern tSDLGenListInfo ySDL_SDL_Object_Identifier;

/* End section with type info nodes */

extern int         GenericGetValue   (unsigned, void *);
extern void        GenericSetValue   (unsigned, void *, int);





/****+***************************************************************
03   SDL predefined data types
********************************************************************/

/*---+---------------------------------------------------------------
     Boolean
-------------------------------------------------------------------*/

/* moved first
#if !defined(X_COMPACT_BOOL)
typedef int SDL_Boolean;
typedef int SDL_boolean;
#else
typedef unsigned char SDL_Boolean;
typedef unsigned char SDL_boolean;
#endif
*/

#define yAssF_SDL_Boolean(V,E,A)  (V = E)
#define yEqF_SDL_Boolean(E1,E2)   ((E1) == (E2))
#define yNEqF_SDL_Boolean(E1,E2)  ((E1) != (E2))
#define ySrtN_SDL_Boolean         xSrtN_SDL_Boolean

#define SDL_False 0
#define SDL_false 0
#define SDL_True  1
#define SDL_true  1

#define xNot_SDL_Boolean(B)       (!(B))
#define xAnd_SDL_Boolean(B1,B2)   ((B1)&&(B2))
#define xOr_SDL_Boolean(B1,B2)    ((B1)||(B2))
#define xXor_SDL_Boolean(B1,B2)   ((B1)!=(B2))
#define xImpl_SDL_Boolean(B1,B2)  ((B1)<=(B2))

#ifndef ANY_SDL_Boolean
#define ANY_SDL_Boolean           (SDL_Boolean)(GETINTRAND % 2)
#endif


/*---+---------------------------------------------------------------
     Bit
-------------------------------------------------------------------*/

typedef unsigned char SDL_Bit;

#define yAssF_SDL_Bit(V,E,A)  (V = E)
#define yEqF_SDL_Bit(E1,E2)   ((E1) == (E2))
#define yNEqF_SDL_Bit(E1,E2)  ((E1) != (E2))
#define ySrtN_SDL_Bit         xSrtN_SDL_Bit

#define SDL_BIT_LIT(I)        I

#define xNot_SDL_Bit(B)       (SDL_Bit)(!(B))
#define xAnd_SDL_Bit(B1,B2)   (SDL_Bit)((B1)&(B2))
#define xOr_SDL_Bit(B1,B2)    (SDL_Bit)((B1)|(B2))
#define xXor_SDL_Bit(B1,B2)   (SDL_Bit)((B1)^(B2))
#define xImpl_SDL_Bit(B1,B2)  (SDL_Bit)((B1)<=(B2))

#ifndef ANY_SDL_Bit
#define ANY_SDL_Bit           (SDL_Bit)(GETINTRAND % 2)
#endif


#ifndef XNOUSEOFREAL
/*---+---------------------------------------------------------------
     Real
-------------------------------------------------------------------*/

/* Cmicro support for X_SHORT_REAL, use float i ctypes package */

typedef double   SDL_Real;
typedef double   SDL_real;

#define yAssF_SDL_Real(V,E,A)  (V = E)
#define yEqF_SDL_Real(E1,E2)   ((E1) == (E2))
#define yNEqF_SDL_Real(E1,E2)  ((E1) != (E2))
#define ySrtN_SDL_Real         xSrtN_SDL_Real

#define SDL_REAL_LIT(R,I,D)    R
/* Literals as in C */

#ifndef PLUS_INFINITY
#define PLUS_INFINITY 1.79769313486231570e+308
#endif
/* #include <values.h> */
/* #define PLUS_INFINITY MAXDOUBLE */
/* #define MINUS_INFINITY -MAXDOUBLE */

#ifndef MINUS_INFINITY
#define MINUS_INFINITY -PLUS_INFINITY
#endif

#define xMonMinus_SDL_Real(R)   (-(R))
#define xPlus_SDL_Real(R1,R2)   ((R1)+(R2))
#define xMinus_SDL_Real(R1,R2)  ((R1)-(R2))
#define xMult_SDL_Real(R1,R2)   ((R1)*(R2))

#ifdef XEREALDIV
extern SDL_Real xDiv_SDL_Real (SDL_Real, SDL_Real);
#else
#define xDiv_SDL_Real(i,k) ((i)/(k))
#endif

#define xLT_SDL_Real(R1,R2)  ((R1)<(R2))
#define xLE_SDL_Real(R1,R2)  ((R1)<=(R2))
#define xGT_SDL_Real(R1,R2)  ((R1)>(R2))
#define xGE_SDL_Real(R1,R2)  ((R1)>=(R2))

#ifndef ANY_SDL_Real
#define ANY_SDL_Real         (SDL_Real)(GETINTRAND - GETINTRAND_MAX/2)
#endif

#endif  /* XNOUSEOFREAL */


/*---+---------------------------------------------------------------
     Integer
-------------------------------------------------------------------*/

#if !defined(X_LONG_INT)
typedef int SDL_Integer;
typedef int SDL_integer;
#else
typedef long SDL_Integer;
typedef long SDL_integer;
#endif

#define yAssF_SDL_Integer(V,E,A)  (V = E)
#define yEqF_SDL_Integer(E1,E2)   ((E1) == (E2))
#define yNEqF_SDL_Integer(E1,E2)  ((E1) != (E2))
#define ySrtN_SDL_Integer         xSrtN_SDL_Integer

#define SDL_INTEGER_LIT(I)        I
/* Literals as in C */

#define xMonMinus_SDL_Integer(I)   (-(I))
#define xPlus_SDL_Integer(I1,I2)   ((I1)+(I2))
#define xMinus_SDL_Integer(I1,I2)  ((I1)-(I2))
#define xMult_SDL_Integer(I1,I2)   ((I1)*(I2))

#ifdef XEINTDIV
extern SDL_Integer xDiv_SDL_Integer (SDL_Integer, SDL_Integer);
extern SDL_Integer xRem_SDL_Integer (SDL_Integer, SDL_Integer);
#else
#define xDiv_SDL_Integer(i,k)      ((i)/(k))
#define xRem_SDL_Integer(i,k)      ((i)%(k))
#endif

extern SDL_Integer xMod_SDL_Integer (SDL_Integer, SDL_Integer);

#define xLT_SDL_Integer(I1,I2)     ((I1)<(I2))
#define xLE_SDL_Integer(I1,I2)     ((I1)<=(I2))
#define xGT_SDL_Integer(I1,I2)     ((I1)>(I2))
#define xGE_SDL_Integer(I1,I2)     ((I1)>=(I2))

#ifndef XNOUSEOFREAL
#ifdef XEFIXOF
extern SDL_Integer xFix_SDL_Integer (SDL_Real);
#else
#define xFix_SDL_Integer(Re)       ((SDL_Integer)Re)
#endif
#endif

#ifndef XNOUSEOFREAL
#define xFloat_SDL_Integer(I)      ((SDL_Real)I)
#endif

#ifndef ANY_SDL_Integer
#define ANY_SDL_Integer            (SDL_Integer)(GETINTRAND - GETINTRAND_MAX/2)
#endif


/*---+---------------------------------------------------------------
     Natural
-------------------------------------------------------------------*/

#if !defined(X_LONG_INT)
typedef int SDL_Natural;
typedef int SDL_natural;
#else
typedef long SDL_Natural;
typedef long SDL_natural;
#endif

#define yAssF_SDL_Natural(V,E,A)  (V = E)
#define yEqF_SDL_Natural(E1,E2)   ((E1) == (E2))
#define yNEqF_SDL_Natural(E1,E2)  ((E1) != (E2))
#define ySrtN_SDL_Natural         xSrtN_SDL_Natural

#ifdef XTESTF
extern xbool xTest_SDL_Natural (void *);
#endif

#ifdef XERANGE
extern SDL_Natural xTstA_SDL_Natural (SDL_Natural);
#define yTstA_SDL_Natural(N)  xTstA_SDL_Natural(N)
#else
#define xTstA_SDL_Natural(N)  N
#define yTstA_SDL_Natural(N)  N
#endif

#ifdef XEINDEX
extern SDL_Natural xTstI_SDL_Natural (SDL_Natural);
#define yTstI_SDL_Natural(N)  xTstI_SDL_Natural(N)
#else
#define xTstI_SDL_Natural(N)  N
#define yTstI_SDL_Natural(N)  N
#endif

#ifndef ANY_SDL_Natural
#define ANY_SDL_Natural         (SDL_Natural)(GETINTRAND)
#endif


/*---+---------------------------------------------------------------
     Character
-------------------------------------------------------------------*/

typedef char    SDL_Character;
typedef char    SDL_character;

#define yAssF_SDL_Character(V,E,A)  (V = E)
#define yEqF_SDL_Character(E1,E2)   ((E1) == (E2))
#define yNEqF_SDL_Character(E1,E2)  ((E1) != (E2))
#define ySrtN_SDL_Character         xSrtN_SDL_Character

/* predefined literals for non-printable characters,
   other literals as in C */
#define SDL_NUL '\000'
#define SDL_SOH '\001'
#define SDL_STX '\002'
#define SDL_ETX '\003'
#define SDL_EOT '\004'
#define SDL_ENQ '\005'
#define SDL_ACK '\006'
#define SDL_BEL '\007'
#define SDL_BS  '\010'
#define SDL_HT  '\011'
#define SDL_LF  '\012'
#define SDL_VT  '\013'
#define SDL_FF  '\014'
#define SDL_CR  '\015'
#define SDL_SO  '\016'
#define SDL_SI  '\017'
#define SDL_DLE '\020'
#define SDL_DC1 '\021'
#define SDL_DC2 '\022'
#define SDL_DC3 '\023'
#define SDL_DC4 '\024'
#define SDL_NAK '\025'
#define SDL_SYN '\026'
#define SDL_ETB '\027'
#define SDL_CAN '\030'
#define SDL_EM  '\031'
#define SDL_SUB '\032'
#define SDL_ESC '\033'
#define SDL_IS4 '\034'
#define SDL_IS3 '\035'
#define SDL_IS2 '\036'
#define SDL_IS1 '\037'
#define SDL_DEL '\177'
#define SDL_E_NUL '\200'
#define SDL_E_SOH '\201'
#define SDL_E_STX '\202'
#define SDL_E_ETX '\203'
#define SDL_E_EOT '\204'
#define SDL_E_ENQ '\205'
#define SDL_E_ACK '\206'
#define SDL_E_BEL '\207'
#define SDL_E_BS  '\210'
#define SDL_E_HT  '\211'
#define SDL_E_LF  '\212'
#define SDL_E_VT  '\213'
#define SDL_E_FF  '\214'
#define SDL_E_CR  '\215'
#define SDL_E_SO  '\216'
#define SDL_E_SI  '\217'
#define SDL_E_DLE '\220'
#define SDL_E_DC1 '\221'
#define SDL_E_DC2 '\222'
#define SDL_E_DC3 '\223'
#define SDL_E_DC4 '\224'
#define SDL_E_NAK '\225'
#define SDL_E_SYN '\226'
#define SDL_E_ETB '\227'
#define SDL_E_CAN '\230'
#define SDL_E_EM  '\231'
#define SDL_E_SUB '\232'
#define SDL_E_ESC '\233'
#define SDL_E_IS4 '\234'
#define SDL_E_IS3 '\235'
#define SDL_E_IS2 '\236'
#define SDL_E_IS1 '\237'

#define xLT_SDL_Character(C1,C2) \
   (((int)C1<0 ? (int)C1+256 : (int)C1) < ((int)C2<0 ? (int)C2+256 : (int)C2))
#define xLE_SDL_Character(C1,C2) \
   (((int)C1<0 ? (int)C1+256 : (int)C1) <= ((int)C2<0 ? (int)C2+256 : (int)C2))
#define xGT_SDL_Character(C1,C2) \
   (((int)C1<0 ? (int)C1+256 : (int)C1) > ((int)C2<0 ? (int)C2+256 : (int)C2))
#define xGE_SDL_Character(C1,C2) \
   (((int)C1<0 ? (int)C1+256 : (int)C1) >= ((int)C2<0 ? (int)C2+256 : (int)C2))

#define xChr_SDL_Character(I)  ((SDL_Character)xMod_SDL_Integer(I,256))
#define xNum_SDL_Character(C)  ((SDL_Integer)C<0 ? (SDL_Integer)C+256 : (SDL_Integer)C)

#ifndef ANY_SDL_Character
#define ANY_SDL_Character         xChr_SDL_Character(GETINTRAND)
#endif


/*---+---------------------------------------------------------------
     PId
-------------------------------------------------------------------*/

#define ySrtN_SDL_PId             xSrtN_SDL_PId
/* see kernel for implementation of PId */

#ifndef ANY_SDL_PId
#define ANY_SDL_PId             SDL_NULL
#endif


/*---+---------------------------------------------------------------
     Null
-------------------------------------------------------------------*/

typedef int     SDL_Null;

#define yAssF_SDL_Null(V,E,A)  (V = E)
#define yEqF_SDL_Null(E1,E2)   ((E1) == (E2))
#define yNEqF_SDL_Null(E1,E2)  ((E1) != (E2))
#define ySrtN_SDL_Null         xSrtN_SDL_Null

#define SDL_NullValue  0

#ifndef ANY_SDL_Null
#define ANY_SDL_Null           (SDL_Null)0
#endif


/*---+---------------------------------------------------------------
     Charstring
-------------------------------------------------------------------*/

typedef char   *SDL_Charstring;
typedef char   *SDL_charstring;

#define ySrtN_SDL_Charstring         xSrtN_SDL_Charstring
#define SDL_CHARSTRING_LIT(P1, P2)   ((char *)(P1))

#define yAssF_SDL_Charstring(V,E,A)  xAss_SDL_Charstring(&(V),E,A)
#define yEqF_SDL_Charstring(E1,E2)   xEq_SDL_Charstring(E1,E2)
#define yNEqF_SDL_Charstring(E1,E2)  (! xEq_SDL_Charstring(E1,E2))

#define xFree_SDL_Charstring(P)   \
   GenericFreeSort(P,(tSDLTypeInfo *)&ySDL_SDL_Charstring)
#define yFree_SDL_Charstring(P)  xFree_SDL_Charstring(P)

extern void xAss_SDL_Charstring (SDL_Charstring *, SDL_Charstring, int);
extern SDL_Boolean xEq_SDL_Charstring (SDL_Charstring, SDL_Charstring);
extern SDL_Character *yAddr_SDL_Charstring (SDL_Charstring *, SDL_Integer);
extern SDL_Character xExtr_SDL_Charstring (SDL_Charstring, SDL_Integer);
extern SDL_Charstring xMkString_SDL_Charstring (SDL_Character);
extern SDL_Integer xLength_SDL_Charstring (SDL_Charstring);
extern SDL_Character xFirst_SDL_Charstring (SDL_Charstring);
extern SDL_Character xLast_SDL_Charstring (SDL_Charstring);
extern SDL_Charstring xConcat_SDL_Charstring (SDL_Charstring, SDL_Charstring);
extern SDL_Charstring xSubString_SDL_Charstring (SDL_Charstring,SDL_Integer,SDL_Integer);

#ifndef ANY_SDL_Charstring
#define ANY_SDL_Charstring \
   xConcat_SDL_Charstring \
     (xMkString_SDL_Charstring(((SDL_Character)(1+GETINTRAND%127))), \
      xMkString_SDL_Charstring(((SDL_Character)(1+GETINTRAND%127))) \
     )
#endif

/*---+---------------------------------------------------------------
     IA5String
-------------------------------------------------------------------*/

typedef SDL_Charstring  SDL_IA5String;

#define yAssF_SDL_IA5String(V,E,A)  xAss_SDL_Charstring(&(V),E,A)
#define yEqF_SDL_IA5String(E1,E2)   xEq_SDL_Charstring(E1,E2)
#define yNEqF_SDL_IA5String(E1,E2)  (! xEq_SDL_Charstring(E1,E2))
#define ySrtN_SDL_IA5String         xSrtN_SDL_IA5String

#ifdef XTESTF
extern xbool xTest_SDL_IA5String (void *);
#endif

#ifdef XERANGE
extern SDL_IA5String xTstA_SDL_IA5String (SDL_IA5String);
#define yTstA_SDL_IA5String(N)  xTstA_SDL_IA5String(N)
#else
#define xTstA_SDL_IA5String(N)  N
#define yTstA_SDL_IA5String(N)  N
#endif

#ifdef XEINDEX
extern SDL_IA5String xTstI_SDL_IA5String (SDL_IA5String);
#define yTstI_SDL_IA5String(N)  xTstI_SDL_IA5String(N)
#else
#define xTstI_SDL_IA5String(N)  N
#define yTstI_SDL_IA5String(N)  N
#endif

#ifndef ANY_SDL_IA5String
#define ANY_SDL_IA5String           ANY_SDL_Charstring
#endif


/*---+---------------------------------------------------------------
     NumericString
-------------------------------------------------------------------*/

typedef SDL_Charstring  SDL_NumericString;

#define yAssF_SDL_NumericString(V,E,A)  xAss_SDL_Charstring(&(V),E,A)
#define yEqF_SDL_NumericString(E1,E2)   xEq_SDL_Charstring(E1,E2)
#define yNEqF_SDL_NumericString(E1,E2)  (! xEq_SDL_Charstring(E1,E2))
#define ySrtN_SDL_NumericString         xSrtN_SDL_NumericString

#ifdef XTESTF
extern xbool xTest_SDL_NumericString (void *);
#endif

#ifdef XERANGE
extern SDL_NumericString xTstA_SDL_NumericString (SDL_NumericString);
#define yTstA_SDL_NumericString(N)  xTstA_SDL_NumericString(N)
#else
#define xTstA_SDL_NumericString(N)  N
#define yTstA_SDL_NumericString(N)  N
#endif

#ifdef XEINDEX
extern SDL_NumericString xTstI_SDL_NumericString (SDL_NumericString);
#define yTstI_SDL_NumericString(N)  xTstI_SDL_NumericString(N)
#else
#define xTstI_SDL_NumericString(N)  N
#define yTstI_SDL_NumericString(N)  N
#endif

#ifndef ANY_SDL_NumericString
#define ANY_SDL_NumericString       ANY_SDL_Charstring
#endif


/*---+---------------------------------------------------------------
     PrintableString
-------------------------------------------------------------------*/

typedef SDL_Charstring  SDL_PrintableString;

#define yAssF_SDL_PrintableString(V,E,A)  xAss_SDL_Charstring(&(V),E,A)
#define yEqF_SDL_PrintableString(E1,E2)   xEq_SDL_Charstring(E1,E2)
#define yNEqF_SDL_PrintableString(E1,E2)  (! xEq_SDL_Charstring(E1,E2))
#define ySrtN_SDL_PrintableString         xSrtN_SDL_PrintableString

#ifdef XTESTF
extern xbool xTest_SDL_PrintableString (void *);
#endif

#ifdef XERANGE
extern SDL_PrintableString xTstA_SDL_PrintableString (SDL_PrintableString);
#define yTstA_SDL_PrintableString(N)  xTstA_SDL_PrintableString(N)
#else
#define xTstA_SDL_PrintableString(N)  N
#define yTstA_SDL_PrintableString(N)  N
#endif

#ifdef XEINDEX
extern SDL_PrintableString xTstI_SDL_PrintableString (SDL_PrintableString);
#define yTstI_SDL_PrintableString(N)  xTstI_SDL_PrintableString(N)
#else
#define xTstI_SDL_PrintableString(N)  N
#define yTstI_SDL_PrintableString(N)  N
#endif

#ifndef ANY_SDL_PrintableString
#define ANY_SDL_PrintableString       ANY_SDL_Charstring
#endif


/*---+---------------------------------------------------------------
     VisibleString
-------------------------------------------------------------------*/

typedef SDL_Charstring  SDL_VisibleString;

#define yAssF_SDL_VisibleString(V,E,A)  xAss_SDL_Charstring(&(V),E,A)
#define yEqF_SDL_VisibleString(E1,E2)   xEq_SDL_Charstring(E1,E2)
#define yNEqF_SDL_VisibleString(E1,E2)  (! xEq_SDL_Charstring(E1,E2))
#define ySrtN_SDL_VisibleString         xSrtN_SDL_VisibleString

#ifdef XTESTF
extern xbool xTest_SDL_VisibleString (void *);
#endif

#ifdef XERANGE
extern SDL_VisibleString xTstA_SDL_VisibleString (SDL_VisibleString);
#define yTstA_SDL_VisibleString(N)  xTstA_SDL_VisibleString(N)
#else
#define xTstA_SDL_VisibleString(N)  N
#define yTstA_SDL_VisibleString(N)  N
#endif

#ifdef XEINDEX
extern SDL_VisibleString xTstI_SDL_VisibleString (SDL_VisibleString);
#define yTstI_SDL_VisibleString(N)  xTstI_SDL_VisibleString(N)
#else
#define xTstI_SDL_VisibleString(N)  N
#define yTstI_SDL_VisibleString(N)  N
#endif


#ifndef XNOUSEOFOCTETBITSTRING
/*---+---------------------------------------------------------------
     Octet
-------------------------------------------------------------------*/

typedef unsigned char SDL_Octet;

#define yAssF_SDL_Octet(V,E,A)  (V = E)
#define yEqF_SDL_Octet(E1,E2)   ((E1) == (E2))
#define yNEqF_SDL_Octet(E1,E2)  ((E1) != (E2))
#define ySrtN_SDL_Octet         xSrtN_SDL_Octet

extern SDL_Bit *yAddr_SDL_Octet (SDL_Octet *, SDL_Integer);
extern SDL_Bit xExtr_SDL_Octet (SDL_Octet, SDL_Integer);

#define xLT_SDL_Octet(B1,B2)  ((B1)<(B2))
#define xLE_SDL_Octet(B1,B2)  ((B1)<=(B2))
#define xGT_SDL_Octet(B1,B2)  ((B1)>(B2))
#define xGE_SDL_Octet(B1,B2)  ((B1)>=(B2))

#define xNot_SDL_Octet(B)       (SDL_Octet)(~(B))
#define xAnd_SDL_Octet(B1,B2)   (SDL_Octet)((B1)&(B2))
#define xOr_SDL_Octet(B1,B2)    (SDL_Octet)((B1)|(B2))
#define xXor_SDL_Octet(B1,B2)   (SDL_Octet)((B1)^(B2))
#define xImpl_SDL_Octet(B1,B2)  xNot_SDL_Octet(xAnd_SDL_Octet((B1),xNot_SDL_Octet(B2)))
#define xShiftL_SDL_Octet(B,I)  (SDL_Octet)(((B)<<(I)) & 0xFF)
#define xShiftR_SDL_Octet(B,I)  ((B)>>(I))
#define xPlus_SDL_Octet(B1,B2)  (SDL_Octet)(((B1)+(B2)) & 0xFF)
#define xMinus_SDL_Octet(B1,B2) (SDL_Octet)((B1)-(B2))
#define xMult_SDL_Octet(B1,B2)  (SDL_Octet)(((B1)*(B2)) & 0xFF)
#define xI2O_SDL_Octet(I)       (SDL_Octet)(I)
#define xO2I_SDL_Octet(B)       (SDL_Integer)(B)

#ifndef XEINTDIV
#define xDiv_SDL_Octet(B1,B2)   ((B1)/(B2))
#define xMod_SDL_Octet(B1,B2)   ((B1)%(B2))
#define xRem_SDL_Octet(B1,B2)   ((B1)%(B2))
#else
extern SDL_Octet xDiv_SDL_Octet (SDL_Octet, SDL_Octet);
extern SDL_Octet xMod_SDL_Octet (SDL_Octet, SDL_Octet);
extern SDL_Octet xRem_SDL_Octet (SDL_Octet, SDL_Octet);
#endif

extern SDL_Octet xBitStr_SDL_Octet (SDL_Charstring);
extern SDL_Octet xHexStr_SDL_Octet (SDL_Charstring);

#ifndef ANY_SDL_Octet
#define ANY_SDL_Octet           (SDL_Octet)(GETINTRAND % 256)
#endif

#endif  /* XNOUSEOFOCTETBITSTRING */



/*---+---------------------------------------------------------------
     Duration
-------------------------------------------------------------------*/

#if !defined(XTIMEASINT_TICKS) && !defined(XTIMEASINT_SECS)

typedef struct {
  xint32  s, ns;
} SDL_Duration;
typedef SDL_Duration SDL_duration;
#define SDL_DURATION_LIT(R,I,D)    SDL_Duration_Lit((xint32)(I),(xint32)(D))
extern SDL_Duration SDL_Duration_Lit (xint32 s, xint32 ns);

#else

#ifdef XUNSIGNED_TIME
/* Most RTOS's use an unsigned long int to represent time and durations.      */
/* Using an unsigned SDL_Duration (and thus SDL_Time) is compatible with this */
/* design, but introduces the restriction that durations may not be negative. */

typedef unsigned long SDL_Duration;

#else
/* The default is to allow negative durations, which is compliant with SDL    */

typedef long SDL_Duration;

#endif

typedef SDL_Duration  SDL_duration;

#ifdef XTIMEASINT_TICKS
#define SDL_DURATION_LIT(R,I,D)    I
#else
#define SDL_DURATION_LIT(R,I,D)    ((I)*SDT_TICKPERSEC + (D)/SDT_NANOSECPERTICK)
#endif

#endif


#define yAssF_SDL_Duration(V,E,A)  (V = E)
#define yEqF_SDL_Duration(E1,E2)   xEq_SDL_Duration(E1,E2)
#define yNEqF_SDL_Duration(E1,E2)  (! xEq_SDL_Duration(E1,E2))
#define ySrtN_SDL_Duration         xSrtN_SDL_Duration

#if !defined(XTIMEASINT_TICKS) && !defined(XTIMEASINT_SECS)

extern SDL_Duration xPlus_SDL_Duration (SDL_Duration, SDL_Duration);
#define xMinus_SDL_Duration(P1, P2) xPlus_SDL_Duration(P1, xMonMinus_SDL_Duration(P2))
extern SDL_Duration xMonMinus_SDL_Duration (SDL_Duration);

#ifndef XNOUSEOFREAL
extern SDL_Duration xMult_SDL_Duration (SDL_Duration, SDL_Real);
extern SDL_Duration xDiv_SDL_Duration (SDL_Duration, SDL_Real);
#endif

#define xMultRD_SDL_Duration(R,D) xMult_SDL_Duration(D, R)
#define xGT_SDL_Duration(P1, P2)  xLT_SDL_Duration(P2, P1)
#define xGE_SDL_Duration(P1, P2)  xLE_SDL_Duration(P2, P1)
extern SDL_Boolean xLT_SDL_Duration (SDL_Duration, SDL_Duration);
extern SDL_Boolean xLE_SDL_Duration (SDL_Duration, SDL_Duration);
extern SDL_Boolean xEq_SDL_Duration (SDL_Duration, SDL_Duration);

#ifndef ANY_SDL_Duration
#define ANY_SDL_Duration SDL_Duration_Lit(GETINTRAND, GETINTRAND % 1000000000)
#endif

#else /* XTIMEASINT_TICKS || XTIMEASINT_SECS */

#define xPlus_SDL_Duration(a,b)   ((SDL_Duration)(a) + (SDL_Duration)(b))
#define xMinus_SDL_Duration(a,b)  ((SDL_Duration)(a) - (SDL_Duration)(b))
#define xMonMinus_SDL_Duration(a) (-(SDL_Duration)(a))
#define xMult_SDL_Duration(a,b)   ((SDL_Duration)(a)*(SDL_Duration)(b))
#define xMultRD_SDL_Duration(a,b) xMult_SDL_Duration(b,a)
#define xDiv_SDL_Duration(a,b)    ((SDL_Duration)(a)/(SDL_Duration)(b))

#define xGT_SDL_Duration(P1, P2)  (P1 > P2)
#define xGE_SDL_Duration(P1, P2)  (P1 >= P2)
#define xLT_SDL_Duration(P1, P2)  (P1 < P2)
#define xLE_SDL_Duration(P1, P2)  (P1 <= P2)
#define xEq_SDL_Duration(P1, P2)  (P1 == P2)

#ifndef ANY_SDL_Duration
#define ANY_SDL_Duration          GETINTRAND
#endif

#endif


/*---+---------------------------------------------------------------
     Time
-------------------------------------------------------------------*/

typedef SDL_Duration  SDL_Time;
typedef SDL_Time  SDL_time;

#define yAssF_SDL_Time(V,E,A)  yAssF_SDL_Duration(V,E,A)
#define yEqF_SDL_Time(E1,E2)   xEq_SDL_Duration(E1,E2)
#define yNEqF_SDL_Time(E1,E2)  (! xEq_SDL_Duration(E1,E2))
#define ySrtN_SDL_Time         xSrtN_SDL_Time

#define SDL_TIME_LIT(R,I,D)    SDL_DURATION_LIT(R,I,D)
#define SDL_Time_Lit(I,D)      SDL_Duration_Lit(I,D)

#if !defined(XTIMEASINT_TICKS) && !defined(XTIMEASINT_SECS)
#define xPlus_SDL_Time(P1, P2)   xPlus_SDL_Duration(P1, P2)
#define xPlusDT_SDL_Time(P1, P2) xPlus_SDL_Duration(P2, P1)
#define xMinusD_SDL_Time(P1, P2) xPlus_SDL_Duration(P1, xMonMinus_SDL_Duration(P2))
#define xMinusT_SDL_Time(P1, P2) xPlus_SDL_Duration(P1, xMonMinus_SDL_Duration(P2))
#else
#define xPlus_SDL_Time(P1, P2)   (P1 + P2)
#define xPlusDT_SDL_Time(P1, P2) (P1 + P2)
#define xMinusD_SDL_Time(P1, P2) (P1 - P2)
#define xMinusT_SDL_Time(P1, P2) (P1 - P2)
#endif /* !XTIMEASINT_TICKS && !XTIMEASINT_SECS */

#define xEq_SDL_Time(P1, P2)  xEq_SDL_Duration(P1, P2)
#define xLT_SDL_Time(P1, P2)  xLT_SDL_Duration(P1, P2)
#define xLE_SDL_Time(P1, P2)  xLE_SDL_Duration(P1, P2)
#define xGT_SDL_Time(P1, P2)  xGT_SDL_Duration(P1, P2)
#define xGE_SDL_Time(P1, P2)  xGE_SDL_Duration(P1, P2)

#ifndef ANY_SDL_Time
#define ANY_SDL_Time ANY_SDL_Duration
#endif


#ifndef XNOUSEOFOCTETBITSTRING
/*---+---------------------------------------------------------------
     Bit_String
-------------------------------------------------------------------*/

typedef struct {
  int             Length;
  xbool           IsAssigned;
  unsigned char * Bits;
} SDL_Bit_String;

#define yAssF_SDL_Bit_String(V,E,A)  xAss_SDL_Bit_String(&(V),E,A)
#define yEqF_SDL_Bit_String(E1,E2)   xEq_SDL_Bit_String(E1,E2)
#define yNEqF_SDL_Bit_String(E1,E2)  (! xEq_SDL_Bit_String(E1,E2))
#define yFree_SDL_Bit_String(P)      xFree_SDL_Bit_String(P)
#define ySrtN_SDL_Bit_String         xSrtN_SDL_Bit_String

#define xAss_SDL_Bit_String(E1,E2,E3) \
   (void)GenericAssignSort((void *)E1,(void *)E2,E3, \
                           (tSDLTypeInfo *)&ySDL_SDL_Bit_String)
#define xEq_SDL_Bit_String(E1,E2) \
   GenericEqualSort(E1,E2,(tSDLTypeInfo *)&ySDL_SDL_Bit_String)
#define xFree_SDL_Bit_String(P)   \
   GenericFreeSort(P,(tSDLTypeInfo *)&ySDL_SDL_Bit_String)
extern SDL_Bit *yAddr_SDL_Bit_String
         (SDL_Bit_String *, SDL_Integer);
extern SDL_Bit xExtr_SDL_Bit_String
         (SDL_Bit_String, SDL_Integer);
extern SDL_Bit_String *xNot_SDL_Bit_String
         (SDL_Bit_String *, SDL_Bit_String *);
extern SDL_Bit_String *xAnd_SDL_Bit_String
         (SDL_Bit_String *, SDL_Bit_String *, SDL_Bit_String *);
extern SDL_Bit_String *xOr_SDL_Bit_String
         (SDL_Bit_String *, SDL_Bit_String *, SDL_Bit_String *);
extern SDL_Bit_String *xXor_SDL_Bit_String
         (SDL_Bit_String *, SDL_Bit_String *, SDL_Bit_String *);
extern SDL_Bit_String *xImpl_SDL_Bit_String 
         (SDL_Bit_String *, SDL_Bit_String *, SDL_Bit_String *);
extern SDL_Bit_String *xMkString_SDL_Bit_String
         (SDL_Bit, SDL_Bit_String *);
extern SDL_Integer xLength_SDL_Bit_String
         (SDL_Bit_String *);
extern SDL_Bit xFirst_SDL_Bit_String
         (SDL_Bit_String *);
extern SDL_Bit xLast_SDL_Bit_String
         (SDL_Bit_String *);
extern SDL_Bit_String *xConcat_SDL_Bit_String
         (SDL_Bit_String *, SDL_Bit_String *, SDL_Bit_String *);
extern SDL_Bit_String *xSubString_SDL_Bit_String
         (SDL_Bit_String *, SDL_Integer, SDL_Integer, SDL_Bit_String *);
extern SDL_Bit_String *xBitStr_SDL_Bit_String
         (SDL_Charstring, SDL_Bit_String *);
extern SDL_Bit_String *xHexStr_SDL_Bit_String
         (SDL_Charstring, SDL_Bit_String *);

#ifndef ANY_SDL_Bit_String
#define ANY_SDL_Bit_String(R) GenericAnySort(R, &ySDL_SDL_Bit_String)
#endif
#endif  /* XNOUSEOFOCTETBITSTRING */


#ifndef XNOUSEOFOCTETBITSTRING
/*---+---------------------------------------------------------------
     Octet_String
-------------------------------------------------------------------*/

/* An error in the implementation of Octet_String was introduced
   in SDT 3.2, when Octet_String was first supported.
   Octet_Strings should be indexed from 1, not from 0 as the
   SDL Suite implementation assumes. Note that Bit_Strings are indexed
   from 0. The inconsistancy between Octet_Strings and Bit_Strings
   in SDL is inherited from ASN.1.
   In SDT 3.6 this indexing error for Octet_Strings is corrected.
   To obtain the Octet_String behaviour from SDT3.2 - SDT3.5,
   OCTET_STRING_START_INDEX_0 should be defined, for example by
   uncommenting the #define below. It is not necessary to recompile
   the kernel for this to take effect.
   This change will effect the behaviour of indexing of
   Octet_Strings and the start parameter in operator SubString.
*/
/* #define OCTET_STRING_START_INDEX_0 */

typedef SDL_Bit_String SDL_Octet_String;

#define yAssF_SDL_Octet_String(V,E,A)  xAss_SDL_Bit_String(&(V),E,A)
#define xAss_SDL_Octet_String(V,E,A)   xAss_SDL_Bit_String(V,E,A)
#define yEqF_SDL_Octet_String(E1,E2)   yEqF_SDL_Bit_String(E1,E2)
#define yNEqF_SDL_Octet_String(E1,E2)  yNEqF_SDL_Bit_String(E1,E2)
#define yFree_SDL_Octet_String(P)      yFree_SDL_Bit_String(P)
#define ySrtN_SDL_Octet_String         xSrtN_SDL_Octet_String

#ifdef OCTET_STRING_START_INDEX_0
#define yAddr_SDL_Octet_String(B1,B2) yAddr_SDL_Bit_String(B1,B2)
#define xExtr_SDL_Octet_String(B1,B2) xExtr_SDL_Bit_String(B1,B2)
#define xSubString_SDL_Octet_String(B1,B2,B3,B4) xSubString_SDL_Bit_String(B1,B2,B3,B4)
#else
#define yAddr_SDL_Octet_String(B1,B2) yAddr_SDL_Bit_String(B1,B2-1)
#define xExtr_SDL_Octet_String(B1,B2) xExtr_SDL_Bit_String(B1,B2-1)
#define xSubString_SDL_Octet_String(B1,B2,B3,B4) xSubString_SDL_Bit_String(B1,B2-1,B3,B4)
#endif

#define xMkString_SDL_Octet_String(B1,B2)  xMkString_SDL_Bit_String(B1,B2)
#define xLength_SDL_Octet_String(B)  xLength_SDL_Bit_String((SDL_Bit_String*)B)
#define xFirst_SDL_Octet_String(B)  xFirst_SDL_Bit_String(B)
#define xLast_SDL_Octet_String(B)  xLast_SDL_Bit_String(B)
#define xConcat_SDL_Octet_String(B1,B2,B3)  xConcat_SDL_Bit_String(B1,B2,B3)

extern SDL_Octet_String *xBitStr_SDL_Octet_String
         (SDL_Charstring, SDL_Octet_String *);
extern SDL_Octet_String *xHexStr_SDL_Octet_String
         (SDL_Charstring, SDL_Octet_String *);
extern SDL_Bit_String *xBit_String_SDL_Octet_String
         (SDL_Octet_String *, SDL_Octet_String *);
extern SDL_Octet_String *xOctet_String_SDL_Octet_String
         (SDL_Bit_String *, SDL_Octet_String *);

#ifndef ANY_SDL_Octet_String
#define ANY_SDL_Octet_String(R) GenericAnySort(R, &ySDL_SDL_Octet_String)
#endif

#endif  /* XNOUSEOFOCTETBITSTRING */


#ifndef XNOUSEOFOBJECTIDENTIFIER
/*---+---------------------------------------------------------------
     Object_Identifier
-------------------------------------------------------------------*/

typedef struct SDL_Object_Identifier_ystruct *SDL_Object_Identifier_yptr;
typedef struct SDL_Object_Identifier_ystruct {
  SDL_Object_Identifier_yptr Suc;
  SDL_Natural Data;
} SDL_Object_Identifier_yrec;
typedef struct {
  SDL_Object_Identifier_yptr First;
  SDL_Object_Identifier_yptr Last;
  int Length;
  int IsAssigned;
} SDL_Object_Identifier;

#define yAssF_SDL_Object_Identifier(V,E,A)  yAss_SDL_Object_Identifier(&(V),E,A)
#define yEqF_SDL_Object_Identifier(E1,E2)   yEq_SDL_Object_Identifier(E1,E2)
#define yNEqF_SDL_Object_Identifier(E1,E2)  (! yEq_SDL_Object_Identifier(E1,E2))
#define ySrtN_SDL_Object_Identifier         xSrtN_SDL_Object_Identifier

#define yAss_SDL_Object_Identifier(E1,E2,E3) \
  (void)GenericAssignSort((void *)E1,(void *)E2,E3, \
                          (tSDLTypeInfo *)&ySDL_SDL_Object_Identifier)
#define yEq_SDL_Object_Identifier(E1,E2) \
  GenericEqualSort(E1,E2,(tSDLTypeInfo *)&ySDL_SDL_Object_Identifier)
#define yFree_SDL_Object_Identifier(P) \
  GenericFreeSort(P,(tSDLTypeInfo *)&ySDL_SDL_Object_Identifier)

#define yEmptystring_SDL_Object_Identifier(Result) \
  GenString_Emptystring(&ySDL_SDL_Object_Identifier,(xString_Type *)Result)
#define yMkString_SDL_Object_Identifier(Item,Result) \
  GenString_MkString(Item,&ySDL_SDL_Object_Identifier,(xString_Type *)Result)
#define yLength_SDL_Object_Identifier(ST) \
  GenString_Length((xString_Type *)ST,&ySDL_SDL_Object_Identifier)
#define yFirst_SDL_Object_Identifier(ST,Result) \
  GenString_First((xString_Type *)ST,&ySDL_SDL_Object_Identifier,Result)
#define yLast_SDL_Object_Identifier(ST,Result) \
  GenString_Last((xString_Type *)ST,&ySDL_SDL_Object_Identifier,Result)
#define yConcat_SDL_Object_Identifier(ST1,ST2,Result) \
  GenString_Concat((xString_Type *)ST1,(xString_Type *)ST2,&ySDL_SDL_Object_Identifier,(xString_Type *)Result)
#define ySubString_SDL_Object_Identifier(ST,Start,SubLength,Result) \
  GenString_SubString((xString_Type *)ST,Start,SubLength,&ySDL_SDL_Object_Identifier,(xString_Type *)Result)
#define yAppend_SDL_Object_Identifier(ST,Item) \
  GenString_Append((xString_Type *)ST,Item,&ySDL_SDL_Object_Identifier)

#define yExtr_SDL_Object_Identifier(ST,Index) \
  (SDL_Natural *)GenString_Extract((xString_Type *)ST,Index, \
                                   &ySDL_SDL_Object_Identifier)
#define yAddr_SDL_Object_Identifier(ST,Index) \
  (SDL_Natural *)GenString_Extract((xString_Type *)ST,Index, \
                                   &ySDL_SDL_Object_Identifier)

#ifndef ANY_SDL_Object_Identifier
#define ANY_SDL_Object_Identifier(R) GenericAnySort(R, (tSDLTypeInfo *)&ySDL_SDL_Object_Identifier)
#endif

#endif  /* XNOUSEOFOBJECTIDENTIFIER */




/****+***************************************************************
04   Generic implementation of operators
********************************************************************/

/* Type used to represent Powerset. */
typedef struct {
  unsigned long A[1];
} xPowerset_Type;

/* Types used to represent a general String and GPowerset. */
typedef struct xString_ystruct  *xString_yptr;
typedef struct xString_ystruct {
  xString_yptr  Suc;
  int           Data;
} xString_yrec;
typedef struct {
  xString_yptr  First;
  xString_yptr  Last;
  int           Length;
  int           IsAssigned;
} xString_Type;

/* Types used to represent a limited String. */
typedef struct {
  int           Length;
  int           Data[1];
} xLString_Type;

/* Types used to represent a general Bag. */
typedef struct xBag_ystruct  *xBag_yptr;
typedef struct xBag_ystruct {
  xBag_yptr     Suc;
  int           NoOfItems;
  int           Data;
} xBag_yrec;
typedef struct {
  xBag_yptr     First;
  xBag_yptr     Last;
  int           Length;
  int           IsAssigned;
} xBag_Type;

/* Types used to represent a general Array. */
typedef struct xGArray_ystruct  *xGArray_yptr;
typedef struct xGArray_ystruct {
  xGArray_yptr  Suc;
  SDL_Integer   Index;
  SDL_Integer   Data;
} xGArray_yrec;
typedef struct {
  xGArray_yptr  First;
  xGArray_yptr  Last;
  int           IsAssigned;
  SDL_Integer   Data;
} xGArray_Type;

/* Types used to represent a ComBuf. */
typedef struct xComBuf_Struct * xComBuf;
typedef struct xComBuf_Struct {
  int IsAssigned;
  int *el;
} xComBuf_Rec;


extern SDL_Boolean GenericIsAssigned (void *, tSDLTypeInfo *);
extern void        GenericFreeSort   (void **, tSDLTypeInfo *);
extern SDL_Boolean GenericEqualSort  (void *, void *, tSDLTypeInfo *);
extern void *      GenericAssignSort (void *, void *, int, tSDLTypeInfo *);
extern void *      GenericMakeStruct (void *, tSDLTypeInfo *, ...);
extern void *      GenericMakeChoice (void *, tSDLTypeInfo *, int, void *);
extern void *      GenericMakeOwnRef (tSDLTypeInfo *, void *);
extern void *      GenericMakeArray  (void *, tSDLTypeInfo *, void *);
extern void        GenericDefault    (void *, tSDLTypeInfo *);
extern void *      GenericAnySort    (void *, tSDLTypeInfo *);
#ifdef XTESTF
extern int         GenericTstIArray  (int, tSDLArrayInfo *);
#endif


/* GArray */
extern void *      GenGArray_Extract (xGArray_Type *, void *, tSDLGArrayInfo *);
extern void *      GenGArray_Modify  (xGArray_Type *, void *, tSDLGArrayInfo *);

/* Powerset */
#define GenPow_Empty(SDLInfo,Result)  memset((void *)Result,0,(SDLInfo)->SortSize)
extern SDL_Boolean GenPow_In     (int, xPowerset_Type *, tSDLPowersetInfo *);
extern void *      GenPow_Incl   (int, xPowerset_Type *, tSDLPowersetInfo *, xPowerset_Type *);
extern void *      GenPow_Del    (int, xPowerset_Type *, tSDLPowersetInfo *, xPowerset_Type *);
extern void        GenPow_Incl2  (int, xPowerset_Type *, tSDLPowersetInfo *);
extern void        GenPow_Del2   (int, xPowerset_Type *, tSDLPowersetInfo *);
extern SDL_Boolean GenPow_LT     (xPowerset_Type *, xPowerset_Type *, tSDLPowersetInfo *);
extern SDL_Boolean GenPow_LE     (xPowerset_Type *, xPowerset_Type *, tSDLPowersetInfo *);
extern void *      GenPow_And    (xPowerset_Type *, xPowerset_Type *, tSDLPowersetInfo *, xPowerset_Type *);
extern void *      GenPow_Or     (xPowerset_Type *, xPowerset_Type *, tSDLPowersetInfo *, xPowerset_Type *);
extern SDL_Integer GenPow_Length (xPowerset_Type *, tSDLPowersetInfo *);
extern int         GenPow_Take   (xPowerset_Type *, tSDLPowersetInfo *);
extern int         GenPow_Take2  (xPowerset_Type *, SDL_Integer, tSDLPowersetInfo *);

/* Bag, GPowerset */
#define GenBag_Empty(SDLInfo,Result)  memset((void *)Result,0,(SDLInfo)->SortSize)
extern void *      GenBag_Makebag (void *, tSDLGenListInfo *, xBag_Type *);
extern SDL_Boolean GenBag_In      (void *, xBag_Type *, tSDLGenListInfo *);
extern void *      GenBag_Incl    (void *, xBag_Type *, tSDLGenListInfo *, xBag_Type *);
extern void *      GenBag_Del     (void *, xBag_Type *, tSDLGenListInfo *, xBag_Type *);
extern void        GenBag_Incl2   (void *, xBag_Type *, tSDLGenListInfo *);
extern void        GenBag_Del2    (void *, xBag_Type *, tSDLGenListInfo *);
extern SDL_Boolean GenBag_LT      (xBag_Type *, xBag_Type *, tSDLGenListInfo *);
extern SDL_Boolean GenBag_LE      (xBag_Type *, xBag_Type *, tSDLGenListInfo *);
extern void *      GenBag_And     (xBag_Type *, xBag_Type *, tSDLGenListInfo *, xBag_Type *);
extern void *      GenBag_Or      (xBag_Type *, xBag_Type *, tSDLGenListInfo *, xBag_Type *);
extern SDL_Integer GenBag_Length  (xBag_Type *, tSDLGenListInfo *);
extern void *      GenBag_Take    (xBag_Type *, tSDLGenListInfo *, void *);
extern void *      GenBag_Take2   (xBag_Type *, SDL_Integer, tSDLGenListInfo *, void *);

/* String */
#define GenString_Emptystring(SDLInfo,Result) memset((void *)Result,0,(SDLInfo)->SortSize)
extern void *      GenString_MkString (void *, tSDLGenListInfo *, xString_Type *);
extern SDL_Integer GenString_Length   (xString_Type *, tSDLGenListInfo *);
extern void *      GenString_First    (xString_Type *, tSDLGenListInfo *, void *);
extern void *      GenString_Last     (xString_Type *, tSDLGenListInfo *, void *);
extern void *      GenString_Concat   (xString_Type *, xString_Type *, tSDLGenListInfo *, xString_Type *);
extern void *      GenString_SubString
                     (xString_Type *, SDL_Integer, SDL_Integer, tSDLGenListInfo *, xString_Type *);
extern void        GenString_Append   (xString_Type *, void *, tSDLGenListInfo *);
extern void *      GenString_Extract  (xString_Type *, SDL_Integer, tSDLGenListInfo *);

/* LString */
#define GenLString_Emptystring(SDLInfo,Result) memset((void *)Result,0,(SDLInfo)->SortSize)
extern void *      GenLString_MkString (void *, tSDLLStringInfo *, xLString_Type *);
#define GenLString_Length(ST,SDLInfo) (ST)->Length
extern void *      GenLString_First    (xLString_Type *, tSDLLStringInfo *, void *);
extern void *      GenLString_Last     (xLString_Type *, tSDLLStringInfo *, void *);
extern void *      GenLString_Concat   (xLString_Type *, xLString_Type *, tSDLLStringInfo *, xLString_Type *);
extern void *      GenLString_SubString
                     (xLString_Type *, SDL_Integer, SDL_Integer, tSDLLStringInfo *, xLString_Type *);
extern void        GenLString_Append   (xLString_Type *, void *, tSDLLStringInfo *);
extern void *      GenLString_Extract  (xLString_Type *, SDL_Integer, tSDLLStringInfo *);

/* Own */
extern void *      GenOwn_Assign      (void *, void *, int, tSDLTypeInfo*);
extern void *      GenOwn_Copy        (void *, tSDLTypeInfo*);
extern SDL_Boolean GenOwn_Equal       (void *, void *, tSDLTypeInfo *);
extern void *      GenOwn_Pass        (void **yVar);
extern void *      GenericPassOwn     (void **, void *, tSDLTypeInfo *);


extern void * yMkAddr_SDL_Boolean (SDL_Boolean, SDL_Boolean *);
extern void * yMkAddr_SDL_Bit (SDL_Bit, SDL_Bit *);
#ifndef XNOUSEOFOCTETBITSTRING
extern void * yMkAddr_SDL_Octet (SDL_Octet, SDL_Octet *);
#endif
extern void * yMkAddr_SDL_Character (SDL_Character, SDL_Character *);
extern void * yMkAddr_SDL_Charstring (SDL_Charstring, SDL_Charstring *);
#define yMkAddr_SDL_IA5String(P1,P2)  yMkAddr_SDL_Charstring(P1,P2)
#define yMkAddr_SDL_NumericString(P1,P2)  yMkAddr_SDL_Charstring(P1,P2)
#define yMkAddr_SDL_PrintableString(P1,P2)  yMkAddr_SDL_Charstring(P1,P2)
#define yMkAddr_SDL_VisibleString(P1,P2)  yMkAddr_SDL_Charstring(P1,P2)
extern void * yMkAddr_SDL_Duration (SDL_Duration, SDL_Duration *);
extern void * yMkAddr_SDL_Integer (SDL_Integer, SDL_Integer *);
#define yMkAddr_SDL_Natural(P1,P2)  yMkAddr_SDL_Integer(P1,P2)
extern void * yMkAddr_SDL_Null (SDL_Null, SDL_Null *);
#ifndef XNOUSEOFREAL
extern void * yMkAddr_SDL_Real (SDL_Real, SDL_Real *);
#endif
#define yMkAddr_SDL_Time(P1,P2)  yMkAddr_SDL_Duration(P1,P2)

#ifndef XENUM_TYPE
#define XENUM_TYPE int
#endif
extern void * yMkAddr_GenericShortEnum (XENUM_TYPE, XENUM_TYPE *);



/****+***************************************************************
05   Utility functions
********************************************************************/

#ifdef XVALIDATOR_LIB
#define xMax_SDL_Integer 2147483647
extern SDL_Time xMaxTime;
#endif

/*---+---------------------------------------------------------------
     yInit_Predefined
-------------------------------------------------------------------*/

#ifndef XSCT_CMICRO
#ifndef XOPTSORT
extern void yInit_Predefined (void);
#endif
#endif




/**************************************************************/
/*                  ALLOC TEST FUNCTIONS                      */
/**************************************************************/

#ifdef XEALLOC
extern void * xAllocSort (xptrint, tSDLTypeInfo *);
extern void xFreeSort (void **);
extern void xReportPtrList (void);
#endif



/****+***************************************************************
06   Write buffer management
********************************************************************/
#ifdef XREADANDWRITEF

typedef struct {
  unsigned	all;			/* Allocated length of the buffer */
  unsigned	len;			/* Used length of the buffer */
  char		A[4];			/* The array. */
} WriteBufRep;

typedef struct {
  WriteBufRep*	rep;			/* Representation of the buffer */
} WriteBuf;

typedef int	WriteBufStatus;


/*
 * Management:
 *	New	Creates a new buffer with given initial length.
 *	Del	Deletes the buffer.  Clears pointer itself.
 *	Clear	Sets the used length to 0.
 *	Resize	Resizes the buffer to have AT LEAST the given length.
 *		The size of the buffer is never shortened!
 *		Returns 0 on success.
 *	Secure	Resize the buffer to be able to hold another n bytes
 *		if necessary.
 *		Returns 0 on success.
 */

extern WriteBuf * WriteBuf_New (unsigned start_length);

extern void WriteBuf_Del (WriteBuf** buf);

#define	WriteBuf_Clear(BUF)  ((BUF)->rep->len = 0)

extern WriteBufStatus WriteBuf_Resize (WriteBuf* buf, unsigned new_length);

#define	WriteBuf_Secure(BUF, DL)			\
     ( (((BUF)->rep->len+1+(DL)) < ((BUF)->rep->all)) ? 0 :	\
       WriteBuf_Resize((BUF), ((BUF)->rep->len+1+(DL)) ) )



/*
 * Adding data to the buffer.
 *	Add_Char	Append a single character to the buffer.
 *	Add_String	Append a string to the buffer.  If the length given
 *			is 0 strlen() is used to determine the length.
 *	Add_WriteBuf	Append another buffer.
 *	Add_Fmt		Appends using printf format.  The max_len is
 *			the maximum length needed for the data to be added.
 *			Uses stdarg and vsprintf().
 *	Terminate	Append a NUL at the end of the buffer.  Does not
 *			change the length (but may change 'rep').
 *		All these returns 0 on success.
 *	ADD_CHAR	Macro, appends to buffer. NO CHECKS! MUST be preceeded
 *			with a WriteBuf_Secure before use!
 */

extern WriteBufStatus WriteBuf_Add_Char (WriteBuf* buf, char c);
extern WriteBufStatus WriteBuf_Add_String (
	 WriteBuf* buf,  const char* s, unsigned s_len);
extern WriteBufStatus WriteBuf_Add_WriteBuf (WriteBuf* buf, const WriteBuf* b);

extern WriteBufStatus WriteBuf_Add_Fmt (
         WriteBuf* buf, unsigned max_len, const char* fmt, ...);

extern WriteBufStatus WriteBuf_Terminate (WriteBuf* buf);

#define	WriteBuf_ADD_CHAR(BUF,C)  \
	((BUF)->rep->A[((BUF)->rep->len)++] = (C))

#define	WriteBuf_Backup(BUF,N)		\
	((BUF)->rep->len >= (N) ? ((BUF)->rep->len -= (N)),0 : 1)

/* Access:
 *	Length			Returns the used length.
 *	Allocated_Length	Returns the allocated length.
 *	Data			Returns pointer to first character.
 */
#define	WriteBuf_Length(BUF)           ((BUF)->rep->len)
#define	WriteBuf_Allocated_Length(BUF) ((BUF)->rep->all)
#define	WriteBuf_Data(BUF)             ((BUF)->rep->A)

#define	WriteBuf_Set_Length(BUF,LEN)   ((BUF)->rep->len = (LEN))



/****+***************************************************************
07   Generic read and write function
********************************************************************/

#define USE_SDL_SYNTAX_FLAG	(0x00)
#define USE_ASN1_SYNTAX_FLAG	(0x01)
#define USE_ITEX_SYNTAX_FLAG	(0x02)

#define USE_HEX_REF_SYNTAX_FLAG	(0x04)
#define USE_DEREF_SYNTAX_FLAG	(0x08)

#ifdef XMONITOR
extern void xReleasexLoopRefWriteList(void);
extern void xReleasexRefReadList(void);
#endif

extern void xGenericWriteSort(
   WriteBuf     *,
   void         *,
   tSDLTypeInfo *);

extern void xxWriteSort(
  void         *,
  tSDLTypeInfo *);

extern void xWriteBuf_Fmt (const char*, ...);
extern void xAddBuf_Fmt (WriteBuf *, const char *, ...);


extern int xGenericReadSort(
   void         *,
   tSDLTypeInfo *);

extern int xGenericReadOneParameter(
   tSDLTypeInfo *,
   char         *,
   void         * );

extern int xRead_SDL_Charstring (void *);
extern int xReadSDL_Time (SDL_Time *);

/* Some macro to make read/write more backward compatible */
#define xWri_SDL_Charstring(Address) xWriteSort(Address, ySrtN_SDL_Charstring)
#define xWri_SDL_Time(Address)       xWriteSort(Address, ySrtN_SDL_Time)
#define xWri_SDL_Duration(Address)   xWriteSort(Address, ySrtN_SDL_Duration)
#define xWri_SDL_Integer(Address)    xWriteSort(Address, ySrtN_SDL_Integer)
#define xWri_SDL_Character(Address)  xWriteSort(Address, ySrtN_SDL_Character)
#define xWri_SDL_Real(Address)       xWriteSort(Address, ySrtN_SDL_Real)

#define xRead_SDL_Time(Address)       xReadSort(Address, ySrtN_SDL_Time)
#define xRead_SDL_Duration(Address)   xReadSort(Address, ySrtN_SDL_Duration)
#define xRead_SDL_Integer(Address)    xReadSort(Address, ySrtN_SDL_Integer)
#define xRead_SDL_Character(Address)  xReadSort(Address, ySrtN_SDL_Character)
#define xRead_SDL_Real(Address)       xReadSort(Address, ySrtN_SDL_Real)

#define xReadOneParameter(Sort, Prompt, Address) \
   xGenericReadOneParameter((Sort)->TypeInfo, Prompt, Address)
#define xReadSort(Address, Sort) xGenericReadSort(Address, (Sort)->TypeInfo)

#endif  /* XREADANDWRITEF */


#endif  /* X_SCTPRED_H */
