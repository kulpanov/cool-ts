/*******************************************************************************
Copyright by Telesoft Europe AB 1990-1991.
Copyright by Telelogic Malmoe AB 1991-1994.
Copyright by Telelogic AB 1994 - 2003.
This Program is owned by Telelogic and is protected by national
copyright laws and international copyright treaties. Telelogic
grants you the right to use this Program on one computer or in
one local computer network at any one time.
Under this License you may only modify the source code for the purpose
of adapting it to your environment. You must reproduce and include
any copyright and trademark notices on all copies of the source code.
You may not use, copy, merge, modify or transfer the Program except as
provided in this License.
Telelogic does not warrant that the Program will meet your
requirements or that the operation of the Program will be
uninterrupted and error free. You are solely responsible that the
selection of the Program and the modification of the source code
will achieve your intended results and that the results are actually
obtained.
*******************************************************************************/

/*
# $FileId: sctpred.c 146 : 2003/07/21 setoer
*/

/* This file contains implementation of operators and the read and
   write functions for the predefined data types except PId.
*/


/*
FILE INDEX
01   Type info nodes
02   IdNodes for package Predefined
03   SDL predefined data types: Operator implementations
04   Generic implementation of operators
05   Utility functions
06   Write buffer management
07   Generic write function
08   Generic read function
*/


#include "scttypes.h"

#ifndef XSYSD
#define XSYSD
#endif

#ifdef XVALIDATOR_LIB
SDL_Time xMaxTime = {2147483647,0};
#endif


/****+***************************************************************
01   Type info nodes
********************************************************************/

tSDLTypeInfo ySDL_SDL_Integer =
  {type_SDL_Integer, 0, sizeof(SDL_Integer) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE 
   T_SDL_Names("Integer") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_Integer)};
#ifndef XNOUSEOFREAL
tSDLTypeInfo ySDL_SDL_Real =
  {type_SDL_Real, 0, sizeof(SDL_Real) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Real") xRaWF((xIdNode)&yPacR_Predefined) xRaWF(&xSrtR_SDL_Real)};
#endif
tSDLTypeInfo ySDL_SDL_Natural =
  {type_SDL_Natural, 0, sizeof(SDL_Natural) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Natural") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_Natural)};
tSDLTypeInfo ySDL_SDL_Boolean =
  {type_SDL_Boolean, 0, sizeof(SDL_Boolean) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Boolean") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_Boolean)};
tSDLTypeInfo ySDL_SDL_Character =
  {type_SDL_Character, 0, sizeof(SDL_Character) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Character") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_Character)};
tSDLTypeInfo ySDL_SDL_Time =
  {type_SDL_Time, 0, sizeof(SDL_Time) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Time") xRaWF((xIdNode)&yPacR_Predefined) xRaWF(&xSrtR_SDL_Time)};
tSDLTypeInfo ySDL_SDL_Duration =
  {type_SDL_Duration, 0, sizeof(SDL_Duration) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Duration") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_Duration)};
#ifdef XSCT_CMICRO
tSDLTypeInfo ySDL_SDL_PId =
  {type_SDL_Pid, 8, sizeof(SDL_PId) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Pid") xRaWF((xIdNode)&yPacR_Predefined) xRaWF(&xSrtR_SDL_PId)};
#else
tSDLTypeInfo ySDL_SDL_PId =
  {type_SDL_Pid, 0, sizeof(SDL_PId) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Pid") xRaWF((xIdNode)&yPacR_Predefined) xRaWF(&xSrtR_SDL_PId)};
#endif
tSDLTypeInfo ySDL_SDL_Charstring =
  {type_SDL_Charstring, 7, sizeof(SDL_Charstring) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Charstring") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_Charstring)};
tSDLTypeInfo ySDL_SDL_Bit =
  {type_SDL_Bit, 0, sizeof(SDL_Bit) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Bit") xRaWF((xIdNode)&yPacR_Predefined) xRaWF(&xSrtR_SDL_Bit)};
#ifndef XNOUSEOFOCTETBITSTRING
tSDLTypeInfo ySDL_SDL_Bit_String =
  {type_SDL_Bit_string, 7, sizeof(SDL_Bit_String) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Bit_string") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_Bit_String)};
tSDLTypeInfo ySDL_SDL_Octet =
  {type_SDL_Octet, 0, sizeof(SDL_Octet) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("Octet") xRaWF((xIdNode)&yPacR_Predefined) xRaWF(&xSrtR_SDL_Octet)};
tSDLTypeInfo ySDL_SDL_Octet_String =
  {type_SDL_Octet_string, 7, sizeof(SDL_Octet_String) T_SDL_OPFUNCS(0)
   T_SDL_EXTRA_VALUE
   T_SDL_Names("Octet_string") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_Octet_String)};
#endif
tSDLTypeInfo ySDL_SDL_IA5String =
  {type_SDL_IA5String, 7, sizeof(SDL_IA5String) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("IA5String") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_IA5String)};
tSDLTypeInfo ySDL_SDL_NumericString =
  {type_SDL_NumericString, 7, sizeof(SDL_NumericString) T_SDL_OPFUNCS(0)
   T_SDL_EXTRA_VALUE
   T_SDL_Names("NumericString") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_NumericString)};
tSDLTypeInfo ySDL_SDL_PrintableString =
  {type_SDL_PrintableString, 7, sizeof(SDL_PrintableString) T_SDL_OPFUNCS(0)
   T_SDL_EXTRA_VALUE
   T_SDL_Names("PrintableString") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_PrintableString)};
tSDLTypeInfo ySDL_SDL_VisibleString =
  {type_SDL_VisibleString, 7, sizeof(SDL_VisibleString) T_SDL_OPFUNCS(0)
   T_SDL_EXTRA_VALUE
   T_SDL_Names("VisibleString") xRaWF((xIdNode)&yPacR_Predefined)
   xRaWF(&xSrtR_SDL_VisibleString)};
tSDLTypeInfo ySDL_SDL_Null =
  {type_SDL_NULL, 0, sizeof(SDL_Null) T_SDL_OPFUNCS(0) T_SDL_EXTRA_VALUE
   T_SDL_Names("NULL") xRaWF((xIdNode)&yPacR_Predefined) xRaWF(&xSrtR_SDL_Null)};
#ifndef XNOUSEOFOBJECTIDENTIFIER
tSDLGenListInfo ySDL_SDL_Object_Identifier =
  {type_SDL_Object_identifier, 15, sizeof(SDL_Object_Identifier) T_SDL_OPFUNCS(0)
   T_SDL_EXTRA_VALUE T_SDL_Names("Object_identifier")
   xRaWF((xIdNode)&yPacR_Predefined) xRaWF(&xSrtR_SDL_Object_Identifier),
   &ySDL_SDL_Natural, 
   (xptrint)sizeof(SDL_Object_Identifier_yrec),
   xOffsetOf(SDL_Object_Identifier_yrec, Data)};
#endif



#ifndef XSCT_CMICRO
#ifndef XOPTSORT
/****+***************************************************************
02   IdNodes for package Predefined
********************************************************************/

/*---+---------------------------------------------------------------
     Package Predefined
-------------------------------------------------------------------*/

XCONST struct xPackageIdStruct yPacR_Predefined = {xPackageEC
  xSymbTLink((xIdNode)0, (xIdNode)0), xSymbolTableRoot xIdNames("Predefined")
  XCOMMON_EXTRAS xIdNames(0) XPAC_EXTRAS};

/*---+---------------------------------------------------------------
     Boolean
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Boolean = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("boolean")
  XCOMMON_EXTRAS xFreF(0) xAssF(0) xEqF(0) xTestF(0),
  (xptrint)sizeof(SDL_Boolean), xPredef, (xSortIdNode)0, (xSortIdNode)0, 0,
  (long int)1, 0, 0
  xRaWF(&ySDL_SDL_Boolean) XSRT_EXTRAS};
#ifndef XOPTLIT
static XCONST struct xLiteralIdStruct yFalseR = {xLiteralEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&xSrtR_SDL_Boolean xIdNames("false")
  XCOMMON_EXTRAS, 0 XLIT_EXTRAS};
static XCONST struct xLiteralIdStruct yTrueR = {xLiteralEC  xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&xSrtR_SDL_Boolean xIdNames("true")
  XCOMMON_EXTRAS, 1 XLIT_EXTRAS};
#endif

/*---+---------------------------------------------------------------
     Bit
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Bit = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("bit")
  XCOMMON_EXTRAS xFreF(0) xAssF(0) xEqF(0)
  xTestF(0), (xptrint)sizeof(SDL_Bit),
  xPredef, (xSortIdNode)0, (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Bit) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     Bit_String
-------------------------------------------------------------------*/

#ifndef XNOUSEOFOCTETBITSTRING
XCONST struct xSortIdStruct xSrtR_SDL_Bit_String = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("bit_string")
  XCOMMON_EXTRAS
  xFreF(0)
  xAssF(xAss_SDL_Bit_String)
  xEqF(xEq_SDL_Bit_String)
  xTestF(0), (xptrint)sizeof(SDL_Bit_String),
  xPredef, (xSortIdNode)0, (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Bit_String) XSRT_EXTRAS};
#endif /* XNOUSEOFOCTETBITSTRING */

/*---+---------------------------------------------------------------
     Octet
-------------------------------------------------------------------*/

#ifndef XNOUSEOFOCTETBITSTRING
XCONST struct xSortIdStruct xSrtR_SDL_Octet = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("octet")
  XCOMMON_EXTRAS xFreF(0) xAssF(0) xEqF(0)
  xTestF(0), (xptrint)sizeof(SDL_Octet),
  xPredef, (xSortIdNode)0, (xSortIdNode)0, 0, (long int)255, 0, 0
  xRaWF(&ySDL_SDL_Octet) XSRT_EXTRAS};
#endif /* XNOUSEOFOCTETBITSTRING */

/*---+---------------------------------------------------------------
     Octet_String
-------------------------------------------------------------------*/

#ifndef XNOUSEOFOCTETBITSTRING
XCONST struct xSortIdStruct xSrtR_SDL_Octet_String = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("octet_string")
  XCOMMON_EXTRAS
  xFreF(0)
  xAssF(xAss_SDL_Bit_String)
  xEqF(xEq_SDL_Bit_String)
  xTestF(0), (xptrint)sizeof(SDL_Octet_String),
  xPredef, (xSortIdNode)0, (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Octet_String) XSRT_EXTRAS};
#endif /* XNOUSEOFOCTETBITSTRING */

/*---+---------------------------------------------------------------
     Character
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Character = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("character")
  XCOMMON_EXTRAS xFreF(0) xAssF(0) xEqF(0)
  xTestF(0), (xptrint)sizeof(SDL_Character),
  xPredef, (xSortIdNode)0, (xSortIdNode)0, 0, (long int)255, 0, 0
  xRaWF(&ySDL_SDL_Character) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     Charstring
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Charstring = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("charstring")
  XCOMMON_EXTRAS
  xFreF(0)
  xAssF(xAss_SDL_Charstring)
  xEqF(xEq_SDL_Charstring)
  xTestF(0), (xptrint)sizeof(SDL_Charstring),
  xPredef, (xSortIdNode)0, (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Charstring) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     IA5String
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_IA5String = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("IA5String")
  XCOMMON_EXTRAS
  xFreF(0)
  xAssF(xAss_SDL_Charstring)
  xEqF(xEq_SDL_Charstring)
  xTestF(xTest_SDL_IA5String),
  (xptrint)sizeof(SDL_Charstring), xSyntype, &xSrtR_SDL_Charstring,
  (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_IA5String) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     NumericString
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_NumericString = {xSortEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_Predefined
  xIdNames("NumericString") XCOMMON_EXTRAS
  xFreF(0)
  xAssF(xAss_SDL_Charstring)
  xEqF(xEq_SDL_Charstring)
  xTestF(xTest_SDL_NumericString),
  (xptrint)sizeof(SDL_Charstring), xSyntype, &xSrtR_SDL_Charstring,
  (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_NumericString) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     PrintableString
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_PrintableString = {xSortEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_Predefined
  xIdNames("PrintableString") XCOMMON_EXTRAS
  xFreF(0)
  xAssF(xAss_SDL_Charstring)
  xEqF(xEq_SDL_Charstring)
  xTestF(xTest_SDL_PrintableString),
  (xptrint)sizeof(SDL_Charstring), xSyntype, &xSrtR_SDL_Charstring,
  (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_PrintableString) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     VisibleString
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_VisibleString = {xSortEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_Predefined
  xIdNames("VisibleString") XCOMMON_EXTRAS
  xFreF(0)
  xAssF(xAss_SDL_Charstring)
  xEqF(xEq_SDL_Charstring)
  xTestF(xTest_SDL_PrintableString),
  (xptrint)sizeof(SDL_Charstring), xSyntype, &xSrtR_SDL_Charstring,
  (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_VisibleString) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     Duration
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Duration = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("duration")
  XCOMMON_EXTRAS xFreF(0) xAssF(0)
  xEqF(xEq_SDL_Duration)
  xTestF(0),
  (xptrint)sizeof(SDL_Duration), xPredef, (xSortIdNode)0, (xSortIdNode)0,
  0, 0, 0, 0
  xRaWF(&ySDL_SDL_Duration) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     Integer
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Integer = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("integer")
  XCOMMON_EXTRAS xFreF(0) xAssF(0) xEqF(0)
  xTestF(0), (xptrint)sizeof(SDL_Integer),
  xPredef, (xSortIdNode)0, (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Integer) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     Natural
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Natural = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("natural")
  XCOMMON_EXTRAS xFreF(0) xAssF(0) xEqF(0)
  xTestF(xTest_SDL_Natural),
  (xptrint)sizeof(SDL_Integer), xSyntype, &xSrtR_SDL_Integer,
  (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Natural) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     Null
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Null = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("null")
  XCOMMON_EXTRAS xFreF(0) xAssF(0) xEqF(0)
  xTestF(0), (xptrint)sizeof(SDL_Null),
  xPredef, (xSortIdNode)0, (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Null) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     Object_Identifier
-------------------------------------------------------------------*/

#ifndef XNOUSEOFOBJECTIDENTIFIER
XCONST struct xSortIdStruct xSrtR_SDL_Object_Identifier = {xSortEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_Predefined
  xIdNames("object_identifier") XCOMMON_EXTRAS
  xFreF(0)
  xAssF(yAss_SDL_Object_Identifier)
  xEqF(yEq_SDL_Object_Identifier)
  xTestF(0), (xptrint)sizeof(SDL_Object_Identifier), xString, &xSrtR_SDL_Natural,
  (xSortIdNode)0, 0, 0, 0, 0
  xRaWF((tSDLTypeInfo *)&ySDL_SDL_Object_Identifier) XSRT_EXTRAS};
#endif /* XNOUSEOFOBJECTIDENTIFIER */

/*---+---------------------------------------------------------------
     PId
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_PId = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("pid")
  XCOMMON_EXTRAS xFreF(0) xAssF(0)
  xEqF(yEq_SDL_PId)
  xTestF(0), (xptrint)sizeof(SDL_PId), xPredef,
  (xSortIdNode)0, (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_PId) XSRT_EXTRAS};

/*---+---------------------------------------------------------------
     Real
-------------------------------------------------------------------*/

#ifndef XNOUSEOFREAL
XCONST struct xSortIdStruct xSrtR_SDL_Real = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("real") XCOMMON_EXTRAS
  xFreF(0) xAssF(0) xEqF(0)
  xTestF(0), (xptrint)sizeof(SDL_Real), xPredef, (xSortIdNode)0,
  (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Real) XSRT_EXTRAS};
#endif /* XNOUSEOFREAL */

/*---+---------------------------------------------------------------
     Time
-------------------------------------------------------------------*/

XCONST struct xSortIdStruct xSrtR_SDL_Time = {xSortEC xSymbTLink((xIdNode)0,
  (xIdNode)0), (xIdNode)&yPacR_Predefined xIdNames("time") XCOMMON_EXTRAS
  xFreF(0) xAssF(0)
  xEqF(xEq_SDL_Duration)
  xTestF(0), (xptrint)sizeof(SDL_Time), xPredef,
  (xSortIdNode)0, (xSortIdNode)0, 0, 0, 0, 0
  xRaWF(&ySDL_SDL_Time) XSRT_EXTRAS};

#endif /* XOPTSORT */


/*---+---------------------------------------------------------------
     yInit_Predefined
-------------------------------------------------------------------*/

#ifndef XOPTSORT
void yInit_Predefined (void)
{
  xInsertIdNode((xIdNode)&yPacR_Predefined);
  xInsertIdNode((xIdNode)&xSrtR_SDL_Boolean);
#ifndef XOPTLIT
  xInsertIdNode((xIdNode)&yFalseR);
  xInsertIdNode((xIdNode)&yTrueR);
#endif
  xInsertIdNode((xIdNode)&xSrtR_SDL_Bit);
#ifndef XNOUSEOFOCTETBITSTRING
  xInsertIdNode((xIdNode)&xSrtR_SDL_Bit_String);
#endif
  xInsertIdNode((xIdNode)&xSrtR_SDL_Character);
  xInsertIdNode((xIdNode)&xSrtR_SDL_Charstring);
  xInsertIdNode((xIdNode)&xSrtR_SDL_Duration);
  xInsertIdNode((xIdNode)&xSrtR_SDL_IA5String);
  xInsertIdNode((xIdNode)&xSrtR_SDL_Integer);
  xInsertIdNode((xIdNode)&xSrtR_SDL_Natural);
  xInsertIdNode((xIdNode)&xSrtR_SDL_Null);
  xInsertIdNode((xIdNode)&xSrtR_SDL_NumericString);
#ifndef XNOUSEOFOBJECTIDENTIFIER
  xInsertIdNode((xIdNode)&xSrtR_SDL_Object_Identifier);
#endif
#ifndef XNOUSEOFOCTETBITSTRING
  xInsertIdNode((xIdNode)&xSrtR_SDL_Octet);
  xInsertIdNode((xIdNode)&xSrtR_SDL_Octet_String);
#endif
  xInsertIdNode((xIdNode)&xSrtR_SDL_PId);
  xInsertIdNode((xIdNode)&xSrtR_SDL_PrintableString);
#ifndef XNOUSEOFREAL
  xInsertIdNode((xIdNode)&xSrtR_SDL_Real);
#endif
  xInsertIdNode((xIdNode)&xSrtR_SDL_Time);
  xInsertIdNode((xIdNode)&xSrtR_SDL_VisibleString);
#ifdef XREADANDWRITEF
#ifdef XMULTIBYTE_SUPPORT
  (void)setlocale(LC_CTYPE, "" );
#endif
#endif
}

#endif /* XOPTSORT */
#endif /* !XSCT_CMICRO */



/****+***************************************************************
03   SDL predefined data types: Operator implementations
********************************************************************/

#ifndef XNOUSEOFREAL
/*** Real **********************************************************/

#ifdef XEREALDIV
/*---+---------------------------------------------------------------
     xDiv_SDL_Real
-------------------------------------------------------------------*/
SDL_Real xDiv_SDL_Real(SDL_Real i, SDL_Real k)
{
  if (k == 0.0) {
    xSDLOpError("Division in sort Real", "Real division with 0");
    return 0.0;
  } else
    return i/k;
}
#endif /* XEREALDIV */

#endif /* !XNOUSEOFREAL */


/*** Integer *******************************************************/

#if !defined(XNOUSEOFREAL) && defined(XEFIXOF)
/*---+---------------------------------------------------------------
     xFix_SDL_Integer
-------------------------------------------------------------------*/
SDL_Integer xFix_SDL_Integer(SDL_Real Re)
{
  if (Re >= (SDL_Real)(xMax_SDL_Integer)) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xSDLOpError("Fix in sort Integer", "Integer overflow.");
#endif
    return 0;
  } else
    return (SDL_Integer)Re;
}
#endif /* !XNOUSEOFREAL && XEFIXOF */


#ifdef XEINTDIV
/*---+---------------------------------------------------------------
     xDiv_SDL_Integer
-------------------------------------------------------------------*/
SDL_Integer xDiv_SDL_Integer(SDL_Integer i, SDL_Integer k)
{
  if (k == 0) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_DIVIDE_BY_ZERO);
#else
    xSDLOpError("Division in sort Integer", "Integer division with 0");
#endif
    return 0;
  } else
    return i/k;
}


/*---+---------------------------------------------------------------
     xRem_SDL_Integer
-------------------------------------------------------------------*/
SDL_Integer xRem_SDL_Integer(SDL_Integer i, SDL_Integer k)
{
  if (k == 0) {
#ifdef XSCT_CMICRO
    ErrorHandler (ERR_N_DIVIDE_BY_ZERO);
#else
    xSDLOpError("Rem in sort Integer", "Second operand is 0");
#endif
    return 0;
  } else
    return i%k;
}

#endif /* XEINTDIV */


/*---+---------------------------------------------------------------
     xMod_SDL_Integer
-------------------------------------------------------------------*/
#ifndef XNOUSE_MOD_INTEGER
SDL_Integer xMod_SDL_Integer(
  SDL_Integer  i,
  SDL_Integer  k)
{
  SDL_Integer result;
  if (k == 0) {
#ifdef XEINTDIV
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_DIVIDE_BY_ZERO);
#else
    xSDLOpError("Mod in sort Integer", "Second operand is 0");
#endif
#endif
    return 0;
  } else {
    k = abs(k);
    result = i%k;
    if (result < 0) result = result+k;
    return result;
  }
}
#endif /* XNOUSE_MOD_INTEGER */


/*** Natural *******************************************************/

#ifdef XTESTF
/*---+---------------------------------------------------------------
     xTest_SDL_Natural
-------------------------------------------------------------------*/
xbool xTest_SDL_Natural(void *N)
{
  if (*(SDL_Natural *)N < 0) return (xbool)0;
  return (xbool)1;
}
#endif


#ifdef XERANGE
/*---+---------------------------------------------------------------
     xTstA_SDL_Natural
-------------------------------------------------------------------*/
SDL_Natural xTstA_SDL_Natural(SDL_Natural  N)
{
  if (N < 0) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorSubrange((tSDLTypeInfo *)&ySDL_SDL_Natural, (void *)&N);
#endif
  }
  return N;
}
#endif


#ifdef XEINDEX
/*---+---------------------------------------------------------------
     xTstI_SDL_Natural
-------------------------------------------------------------------*/
SDL_Natural xTstI_SDL_Natural(SDL_Natural N)
{
  if (N < 0) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorIndex((tSDLTypeInfo *)&ySDL_SDL_Natural, (void *)&N);
#endif
  }
  return N;
}
#endif


/*** Charstring ****************************************************/

/*---+---------------------------------------------------------------
     xAss_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_ASSIGN_CHARSTRING
void xAss_SDL_Charstring(
  SDL_Charstring *yVar,
  SDL_Charstring  yExpr,
  int             AssType)
{
  if (SHOULD_FREE_OLD(AssType) && *yVar == yExpr)
    (void)GenericAssignSort((void *)yVar,(void *)yVar, AssType,
                            (tSDLTypeInfo *)&ySDL_SDL_Charstring);
  else
    (void)GenericAssignSort((void *)yVar,(void *)&yExpr, AssType,
                            (tSDLTypeInfo *)&ySDL_SDL_Charstring);
}
#endif /* XNOUSE_ASSIGN_CHARSTRING */


/*---+---------------------------------------------------------------
     xEq_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_EQ_CHARSTRING
SDL_Boolean xEq_SDL_Charstring(
  SDL_Charstring C1,
  SDL_Charstring C2)
{
  return GenericEqualSort((void *)&C1, (void *)&C2,
                          (tSDLTypeInfo *)&ySDL_SDL_Charstring);
}
#endif /* XNOUSE_EQ_CHARSTRING */


/*---+---------------------------------------------------------------
     xMkString_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_MKSTRING_CHARSTRING
SDL_Charstring xMkString_SDL_Charstring(
  SDL_Character C)
{
  SDL_Charstring Result;

  if (C == SDL_NUL) {
#ifdef XECSOP
    xSDLOpError("MkString in sort Charstring", "Character NUL not allowed.");
#endif
    return (SDL_Charstring)0;
  }
  Result = (SDL_Charstring)XALLOC((xptrint)(sizeof(C)+2), 
				  (tSDLTypeInfo *)&ySDL_SDL_Charstring);
  Result[0] = 'T';
  Result[1] = C;
  Result[2] = '\0';
  return Result;
}
#endif /* XNOUSE_MKSTRING_CHARSTRING */


/*---+---------------------------------------------------------------
     yAddr_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_MODIFY_CHARSTRING
SDL_Character * yAddr_SDL_Charstring(
  SDL_Charstring *C,
  SDL_Integer     Index)
{
  if ((*C == (SDL_Charstring)0) ||
      (Index<=0) || ((unsigned)Index>=strlen(*C))) {
#ifdef XECSOP
    xSDLOpError("Modify! in sort Charstring", "Index out of bounds.");
#endif
    if (*C != (SDL_Charstring)0)
      return &((*C)[1]);
    else
      return (SDL_Character *)XALLOC((xptrint)2,
				     (tSDLTypeInfo *)&ySDL_SDL_Character);  /* ??? */
  } else
    return &((*C)[Index]);
}
#endif /* XNOUSE_MODIFY_CHARSTRING */


/*---+---------------------------------------------------------------
     xExtr_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_EXTRACT_CHARSTRING
SDL_Character xExtr_SDL_Charstring(
  SDL_Charstring  C,
  SDL_Integer     Index)
{
  char Result;
  if ((C == (SDL_Charstring)0) || (Index<=0) || ((unsigned)Index>=strlen(C))) {
#ifdef XECSOP
    xSDLOpError("Extract! in sort Charstring", "Index out of bounds.");
#endif
    Result = SDL_NUL;
  } else
    Result = C[Index];
  if (C != (SDL_Charstring)0 && C[0] == 'T') xFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_EXTRACT_CHARSTRING */


/*---+---------------------------------------------------------------
     xConcat_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_CONCAT_CHARSTRING
SDL_Charstring xConcat_SDL_Charstring(
  SDL_Charstring  C1,
  SDL_Charstring  C2)
{
  SDL_Charstring Result;

  if (C1 == (SDL_Charstring)0) {
    if (C2 == (SDL_Charstring)0)
      return (SDL_Charstring)0;
    else if (C2[0] == 'T')
      return C2;
    else {
      Result = (SDL_Charstring)XALLOC((xptrint)(strlen(C2)+1),
				      (tSDLTypeInfo *)&ySDL_SDL_Charstring);
      (void)strcpy(Result, C2);
    }
  } else if (C2 == (SDL_Charstring)0) {
    if (C1[0] == 'T')
      return C1;
    else {
      Result = (SDL_Charstring)XALLOC((xptrint)(strlen(C1)+1),
				      (tSDLTypeInfo *)&ySDL_SDL_Charstring);
      (void)strcpy(Result, C1);
    }
  } else {
    Result = (SDL_Charstring)XALLOC((xptrint)(strlen(C1)+strlen(C2)),
				    (tSDLTypeInfo *)&ySDL_SDL_Charstring);
    (void)strcpy(Result, C1);
    (void)strcat(Result, C2+1);
    if (C1[0] == 'T') xFree_SDL_Charstring((void **)&C1);
    if (C2[0] == 'T') xFree_SDL_Charstring((void **)&C2);
  }
  Result[0] = 'T';
  return Result;
}
#endif /* XNOUSE_CONCAT_CHARSTRING */


/*---+---------------------------------------------------------------
     xFirst_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_FIRST_CHARSTRING
SDL_Character xFirst_SDL_Charstring(
  SDL_Charstring  C)
{
  char Result;
  if ((C == (SDL_Charstring)0) || (strlen(C) <= (unsigned)1)) {
#ifdef XECSOP
    xSDLOpError("First in sort Charstring", "Charstring length is zero.");
#endif
    Result = SDL_NUL;
  } else
    Result = C[1];
  if (C != (SDL_Charstring)0 && C[0] == 'T') xFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_FIRST_CHARSTRING */


/*---+---------------------------------------------------------------
     xLast_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_LAST_CHARSTRING
SDL_Character xLast_SDL_Charstring(
  SDL_Charstring  C)
{
  char Result;
  if ((C == (SDL_Charstring)0) || (strlen(C) <= (unsigned)1)) {
#ifdef XECSOP
    xSDLOpError("Last in sort Charstring",
                "Charstring length is zero.");
#endif
    Result = SDL_NUL;
  } else
    Result = C[strlen(C) - 1];
  if (C != (SDL_Charstring)0 && C[0] == 'T') xFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_LAST_CHARSTRING */


/*---+---------------------------------------------------------------
     xLength_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_LENGTH_CHARSTRING
SDL_Integer xLength_SDL_Charstring(
  SDL_Charstring  C)
{
  int Length;
  if (C == (SDL_Charstring)0)
    Length = 0;
  else {
    Length = strlen(C)-1;
    if (C[0] == 'T') xFree_SDL_Charstring((void **)&C);
  }
  return Length;
}
#endif /* XNOUSE_LENGTH_CHARSTRING */


/*---+---------------------------------------------------------------
     xSubString_SDL_Charstring
-------------------------------------------------------------------*/
#ifndef XNOUSE_SUBSTRING_CHARSTRING
SDL_Charstring xSubString_SDL_Charstring(
  SDL_Charstring  C,
  SDL_Integer     Start,
  SDL_Integer     SubLength)
{
  SDL_Charstring Result;

  if ((C == (SDL_Charstring)0) || (strlen(C) <= (unsigned)1)) {
#ifdef XECSOP
    xSDLOpError("SubString in sort Charstring",
                "Charstring length is zero.");
#endif
    Result = (SDL_Charstring)0;
  } else if (Start <= 0) {
#ifdef XECSOP
    xSDLOpError("SubString in sort Charstring",
                "Start is less than or equal to zero.");
#endif
    Result = (SDL_Charstring)0;
  } else if (SubLength < 0) {
#ifdef XECSOP
    xSDLOpError("SubString in sort Charstring",
                "SubLength is less than zero.");
#endif
    Result = (SDL_Charstring)0;
  } else if (SubLength == 0) {    /* No error if length is 0 */
    Result = (SDL_Charstring)0;
  } else if ((unsigned)(Start+SubLength) > strlen(C)) { 
#ifdef XECSOP
    xSDLOpError("SubString in sort Charstring",
                "Start + Substring length is greater than string length.");
#endif
    Result = (SDL_Charstring)0;
  } else {
    Result = (SDL_Charstring)XALLOC((xptrint)(SubLength + 2),
				    (tSDLTypeInfo *)&ySDL_SDL_Charstring);
    (void)strncpy(Result+1, (SDL_Charstring)((xptrint)C + Start), SubLength);
    Result[0] = 'T';
    Result[SubLength+1] = '\0';
  }
  if (C != (SDL_Charstring)0 && C[0] == 'T') xFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_SUBSTRING_CHARSTRING */


/*** IA5String ***********************************************/

#ifdef XTESTF
/*---+---------------------------------------------------------------
     xTest_SDL_IA5String
-------------------------------------------------------------------*/
xbool xTest_SDL_IA5String(void *N)
{
  int i, k;
  SDL_IA5String C;
  if (N && *(SDL_IA5String *)N) {
    C = *(SDL_IA5String *)N;
    for (i=1; C[i] != '\0'; i++) {
      k = (int)C[i];
      if (k < 0 || k > 127)
        return (xbool)0;
    }
  }
  return (xbool)1;
}
#endif


#ifdef XERANGE
/*---+---------------------------------------------------------------
     xTstA_SDL_IA5String
-------------------------------------------------------------------*/
SDL_IA5String xTstA_SDL_IA5String(
  SDL_IA5String  N)
{
  if (! xTest_SDL_IA5String((void *)&N)) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorSubrange((tSDLTypeInfo *)&ySDL_SDL_IA5String, (void *)&N);
#endif
  }
  return N;
}
#endif


#ifdef XEINDEX
/*---+---------------------------------------------------------------
     xTstI_SDL_IA5String
-------------------------------------------------------------------*/
SDL_IA5String xTstI_SDL_IA5String(SDL_IA5String N)
{
  if (! xTest_SDL_IA5String((void *)&N)) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorIndex((tSDLTypeInfo *)&ySDL_SDL_IA5String, (void *)&N);
#endif
  }
  return N;
}
#endif


/*** NumericString *************************************************/

#ifdef XTESTF
/*---+---------------------------------------------------------------
     xTest_SDL_NumericString
-------------------------------------------------------------------*/
xbool xTest_SDL_NumericString(void *N)
{
  int i;
  SDL_NumericString C;
  if (N && *(SDL_NumericString *)N) {
    C = *(SDL_NumericString *)N;
    for (i=1; C[i] != '\0'; i++)
      if ( ( C[i] != ' ' ) &&
           ( ( C[i] < '0' ) || ( C[i] > '9' ) )
         )
        return (xbool)0;
  }
  return (xbool)1;
}
#endif


#ifdef XERANGE
/*---+---------------------------------------------------------------
     xTstA_SDL_NumericString
-------------------------------------------------------------------*/
SDL_NumericString xTstA_SDL_NumericString(SDL_NumericString N)
{
  if (! xTest_SDL_NumericString((void *)&N)) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorSubrange((tSDLTypeInfo *)&ySDL_SDL_NumericString, (void *)&N);
#endif
  }
  return N;
}
#endif


#ifdef XEINDEX
/*---+---------------------------------------------------------------
     xTstI_SDL_NumericString
-------------------------------------------------------------------*/
SDL_NumericString xTstI_SDL_NumericString(SDL_NumericString N)
{
  if (! xTest_SDL_NumericString((void *)&N)) {
#ifdef XSCT_CMICRO
    ErrorHandler (ERR_N_SDL_RANGE);
#else
    xErrorIndex((tSDLTypeInfo *)&ySDL_SDL_NumericString, (void *)&N);
#endif
  }
  return N;
}
#endif


/*** PrintableString ***********************************************/

#ifdef XTESTF
/*---+---------------------------------------------------------------
     xTest_SDL_PrintableString
-------------------------------------------------------------------*/
xbool xTest_SDL_PrintableString(void *N)
{
  int i;
  SDL_PrintableString C;
  if (N && *(SDL_PrintableString *)N) {
    C = *(SDL_PrintableString *)N;
    for (i=1; C[i] != '\0'; i++)
      if ( ( ( C[i] < 'A' ) || ( C[i] > 'Z' ) ) &&
           ( ( C[i] < 'a' ) || ( C[i] > 'z' ) ) &&
           ( ( C[i] < '0' ) || ( C[i] > '9' ) ) &&
           ( C[i] != ' ' ) && ( C[i] != '\'' ) && ( C[i] != '(' ) &&
           ( C[i] != ')' ) && ( C[i] != '+' ) && ( C[i] != ',' ) &&
           ( C[i] != '-' ) && ( C[i] != '.' ) && ( C[i] != '/' ) &&
           ( C[i] != ':' ) && ( C[i] != '=' ) && ( C[i] != '?' )
         )
        return (xbool)0;
  }
  return (xbool)1;
}
#endif


#ifdef XERANGE
/*---+---------------------------------------------------------------
     xTstA_SDL_PrintableString
-------------------------------------------------------------------*/
SDL_PrintableString xTstA_SDL_PrintableString(
  SDL_PrintableString  N)
{
  if (! xTest_SDL_PrintableString((void *)&N)) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorSubrange((tSDLTypeInfo *)&ySDL_SDL_PrintableString, (void *)&N);
#endif
  }
  return N;
}
#endif


#ifdef XEINDEX
/*---+---------------------------------------------------------------
     xTstI_SDL_PrintableString
-------------------------------------------------------------------*/
SDL_PrintableString xTstI_SDL_PrintableString(SDL_PrintableString N)
{
  if (! xTest_SDL_PrintableString((void *)&N)) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorIndex((tSDLTypeInfo *)&ySDL_SDL_PrintableString, (void *)&N);
#endif
  }
  return N;
}
#endif


/*** VisibleString ***********************************************/

#ifdef XTESTF
/*---+---------------------------------------------------------------
     xTest_SDL_VisibleString
-------------------------------------------------------------------*/
xbool xTest_SDL_VisibleString(void *N)
{
  int i;
  SDL_VisibleString C;
  if (N && *(SDL_VisibleString *)N) {
    C = *(SDL_VisibleString *)N;
    for (i=1; C[i] != '\0'; i++)
      if (C[i] < ' ' || C[i] > '~')
        return (xbool)0;
  }
  return (xbool)1;
}
#endif


#ifdef XERANGE
/*---+---------------------------------------------------------------
     xTstA_SDL_VisibleString
-------------------------------------------------------------------*/
SDL_VisibleString xTstA_SDL_VisibleString(
  SDL_VisibleString  N)
{
  if (! xTest_SDL_VisibleString((void *)&N)) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorSubrange((tSDLTypeInfo *)&ySDL_SDL_VisibleString, (void *)&N);
#endif
  }
  return N;
}
#endif


#ifdef XEINDEX
/*---+---------------------------------------------------------------
     xTstI_SDL_VisibleString
-------------------------------------------------------------------*/
SDL_VisibleString xTstI_SDL_VisibleString(SDL_VisibleString N)
{
  if (! xTest_SDL_VisibleString((void *)&N)) {
#ifdef XSCT_CMICRO
    ErrorHandler(ERR_N_SDL_RANGE);
#else
    xErrorIndex((tSDLTypeInfo *)&ySDL_SDL_VisibleString, (void *)&N);
#endif
  }
  return N;
}
#endif


#if !defined(XTIMEASINT_TICKS) && !defined(XTIMEASINT_SECS)
/*** Duration ******************************************************/

/*---+---------------------------------------------------------------
     xPlus_SDL_Duration
-------------------------------------------------------------------*/
#if defined(XREADANDWRITEF) || !defined(XNOUSE_PLUS_DURATION)
SDL_Duration xPlus_SDL_Duration(
  SDL_Duration D1,
  SDL_Duration D2)
{
  D1.s = D1.s + D2.s;
  D1.ns = D1.ns + D2.ns;
#ifdef X_XINT32_INT
  if ( ((D1.s >= 0) && (D1.ns >= 1000000000)) ||
       ((D1.s < 0) && (D1.ns > 0)) ) {
    D1.ns = D1.ns - 1000000000;
    D1.s = D1.s + 1;
  } else if
     ( ((D1.s > 0) && (D1.ns < 0)) ||
       ((D1.s <= 0) && (D1.ns <= -1000000000)) ) {
    D1.ns = D1.ns + 1000000000;
    D1.s = D1.s - 1;
  }
#else
  if ( ((D1.s >= 0L) && (D1.ns >= 1000000000L)) ||
       ((D1.s < 0L) && (D1.ns > 0L)) ) {
    D1.ns = D1.ns - 1000000000L;
    D1.s = D1.s + 1L;
  } else if
     ( ((D1.s > 0L) && (D1.ns < 0L)) ||
       ((D1.s <= 0L) && (D1.ns <= -1000000000L)) ) {
    D1.ns = D1.ns + 1000000000L;
    D1.s = D1.s - 1L;
  }
#endif
  return D1;
}
#endif /* XNOUSE_PLUS_DURATION */


/*---+---------------------------------------------------------------
     xMonMinus_SDL_Duration
-------------------------------------------------------------------*/
#if defined(XREADANDWRITEF) || !defined(XNOUSE_MONMINUS_DURATION)
SDL_Duration xMonMinus_SDL_Duration(
  SDL_Duration D)
{
  D.s = -D.s;
  D.ns = -D.ns;
  return D;
}
#endif /* XNOUSE_MONMINUS_DURATION */


#ifndef XNOUSEOFREAL
/*---+---------------------------------------------------------------
     xMult_SDL_Duration
-------------------------------------------------------------------*/
#ifndef XNOUSE_MULT_DURATION
SDL_Duration xMult_SDL_Duration(
  SDL_Duration D,
  SDL_Real     R)
{
  SDL_Real DTemp;
  DTemp = ((SDL_Real)D.s + (SDL_Real)D.ns*(SDL_Real)1E-9) * R;
  if (DTemp >= 2.147483647E9) {
    /* Overflow */
    D.s = 0;
    D.ns = 0;
  } else {
    D.s = (xint32)DTemp;
    D.ns = (xint32)((DTemp-(SDL_Real)D.s)*1E9);
  }
  return D;
}
#endif /* XNOUSE_MULT_DURATION */


/*---+---------------------------------------------------------------
     xDiv_SDL_Duration
-------------------------------------------------------------------*/
#ifndef XNOUSE_DIV_DURATION
SDL_Duration xDiv_SDL_Duration(
  SDL_Duration D,
  SDL_Real     R)
{
  SDL_Real DTemp;
  DTemp = ((SDL_Real)D.s + (SDL_Real)D.ns*(SDL_Real)1E-9) / R;
  if (DTemp >= 2.147483647E9) {
    /* Overflow */
    D.s = 0;
    D.ns = 0;
  } else {
    D.s = (xint32)DTemp;
    D.ns = (xint32)((DTemp-(SDL_Real)D.s)*1E9);
  }
  return D;
}
#endif /* XNOUSE_DIV_DURATION */

#endif /* ! XNOUSEOFREAL */


/*---+---------------------------------------------------------------
     xLT_SDL_Duration
-------------------------------------------------------------------*/
#ifndef XNOUSE_LT_DURATION
SDL_Boolean xLT_SDL_Duration(
  SDL_Duration D1,
  SDL_Duration D2)
{
  if (D1.s < D2.s) return SDL_True;
  if ((D1.s == D2.s) && (D1.ns < D2.ns)) return SDL_True;
  return SDL_False;
}
#endif /* XNOUSE_LT_DURATION */


/*---+---------------------------------------------------------------
     xLE_SDL_Duration
-------------------------------------------------------------------*/
#ifndef XNOUSE_LE_DURATION
SDL_Boolean xLE_SDL_Duration(
  SDL_Duration D1,
  SDL_Duration D2)
{
  if (D1.s < D2.s) return SDL_True;
  if ((D1.s == D2.s) && (D1.ns <= D2.ns)) return SDL_True;
  return SDL_False;
}
#endif /* XNOUSE_LE_DURATION */


/*---+---------------------------------------------------------------
     xEq_SDL_Duration
-------------------------------------------------------------------*/
#ifndef XNOUSE_EQ_DURATION
SDL_Boolean xEq_SDL_Duration(
  SDL_Duration D1,
  SDL_Duration D2)
{
  if ((D1.s == D2.s) && (D1.ns == D2.ns)) return SDL_True;
  return SDL_False;
}
#endif /* XNOUSE_EQ_DURATION */


/*---+---------------------------------------------------------------
     SDL_Duration_Lit
-------------------------------------------------------------------*/
SDL_Duration SDL_Duration_Lit(
  xint32 s,
  xint32 ns)
{
  SDL_Duration D;
  D.s = s;
  D.ns = ns;
  return D;
}

#endif /* ! XTIMEASINT_TICKS && ! XTIMEASINT_SECS */


#ifndef XNOUSEOFOCTETBITSTRING
/*** Octet *********************************************************/

/*---+---------------------------------------------------------------
     yAddr_SDL_Octet
-------------------------------------------------------------------*/
#ifndef XNOUSE_MODIFY_OCTET
SDL_Bit * yAddr_SDL_Octet(SDL_Octet *B, SDL_Integer Index)
{
  if ((Index < 0) || (Index >= 8)) {
#ifdef XECSOP
    xSDLOpError("Modify! in sort Octet", "Index out of bounds.");
#endif
    return B;
  }
  return B;   /* &((*B).Bits[Index]);   ????? */
}
#endif /* XNOUSE_MODIFY_OCTET */


/*---+---------------------------------------------------------------
     xExtr_SDL_Octet
-------------------------------------------------------------------*/
#ifndef XNOUSE_EXTRACT_OCTET
SDL_Bit xExtr_SDL_Octet(SDL_Octet B, SDL_Integer Index)
{
  if ((Index < 0) || (Index >= 8)) {
#ifdef XECSOP
    xSDLOpError("Extract! in sort Octet", "Index out of bounds.");
#endif
    return 0;
  }
  return (B & ((unsigned char)1 << Index)) ? 1 : 0;
}
#endif /* XNOUSE_EXTRACT_OCTET */


#ifdef XEINTDIV
/*---+---------------------------------------------------------------
     xDiv_SDL_Octet
-------------------------------------------------------------------*/
SDL_Octet xDiv_SDL_Octet(SDL_Octet i, SDL_Octet k)
{
  if (k == 0) {
#ifdef XSCT_CMICRO
    ErrorHandler (ERR_N_DIVIDE_BY_ZERO);
#else
    xSDLOpError("Division in sort Octet", "Octet division with 0");
#endif
    return 0;
  } else
    return (SDL_Octet)(i/k);
}


/*---+---------------------------------------------------------------
     xMod_SDL_Octet
-------------------------------------------------------------------*/
SDL_Octet xMod_SDL_Octet(SDL_Octet i, SDL_Octet k)
{
  if (k == 0) {
#ifdef XSCT_CMICRO
    ErrorHandler (ERR_N_DIVIDE_BY_ZERO);
#else
    xSDLOpError("Mod operator in sort Octet", "Right operand is 0");
#endif
    return 0;
  } else
    return (SDL_Octet)(i%k);
}


/*---+---------------------------------------------------------------
     xRem_SDL_Octet
-------------------------------------------------------------------*/
SDL_Octet xRem_SDL_Octet(SDL_Octet i, SDL_Octet k)
{
  if (k == 0) {
#ifdef XSCT_CMICRO
    ErrorHandler (ERR_N_DIVIDE_BY_ZERO);
#else
    xSDLOpError("Rem operator in sort Octet", "Right operand is 0");
#endif
    return 0;
  } else
    return (SDL_Octet)(i%k);
}

#endif /* XEINTDIV */


/*---+---------------------------------------------------------------
     xBitStr_SDL_Octet
-------------------------------------------------------------------*/
#ifndef XNOUSE_BITSTR_OCTET
SDL_Octet xBitStr_SDL_Octet (SDL_Charstring C)
{
  SDL_Octet Result;
  int len, i;

  len = (C == 0) ? 0 : strlen(C)-1;
  if (len > 8) {
#ifdef XECSOP
    xSDLOpError("BitStr in sort Octet",
                "An Octet should consist of not more than 8 characters 0 or 1.");
#endif
    if (C != (SDL_Charstring)0 && C[0] == 'T')
      yFree_SDL_Charstring((void **)&C);
    return (SDL_Octet)0;
  }
  Result = 0;
  for (i=1; i<=len; i++) {
    Result <<= 1;
    if (C[i] == '1') {
      Result++;
    } else if (C[i] != '0') {
#ifdef XECSOP
      xSDLOpError("BitStr in sort Octet",
                  "An Octet should consist only of characters 0 or 1.");
#endif
      if (C != (SDL_Charstring)0 && C[0] == 'T')
        yFree_SDL_Charstring((void **)&C);
      return (SDL_Octet)0;
    }
  }
  for ( ; i<=8; i++) {
    Result <<= 1;
  }
  if (C != (SDL_Charstring)0 && C[0] == 'T')
    yFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_BITSTR_OCTET */


/*---+---------------------------------------------------------------
     xHexStr_SDL_Octet
-------------------------------------------------------------------*/
#ifndef XNOUSE_HEXSTR_OCTET
SDL_Octet xHexStr_SDL_Octet (SDL_Charstring C)
{
  SDL_Octet Result;

  if ((C == 0) ? 0 : strlen(C)-1 != 2) {
#ifdef XECSOP
    xSDLOpError("HexStr in sort Octet",
                "An Octet should consist of 2 HEX values.");
#endif
    if (C != (SDL_Charstring)0 && C[0] == 'T')
      yFree_SDL_Charstring((void **)&C);
    return (SDL_Octet)0;
  }
  if ('0' <= C[1] && C[1] <= '9') {
    Result = (SDL_Octet)(16 * ((int)(C[1]) - '0'));
  } else if ('A' <= C[1] && C[1] <= 'F') {
    Result = (SDL_Octet)(16 * ((int)(C[1]) - 'A' + 10));
  } else if ('a' <= C[1] && C[1] <= 'f') {
    Result = (SDL_Octet)(16 * ((int)(C[1]) - 'a' + 10));
  } else {
#ifdef XECSOP
    xSDLOpError("HexStr in sort Octet",
                "Illegal character in Charstring (not digit or a-f).");
#endif
    if (C != (SDL_Charstring)0 && C[0] == 'T')
      yFree_SDL_Charstring((void **)&C);
    return (SDL_Octet)0;
  }

  if ('0' <= C[2] && C[2] <= '9') {
    Result += (SDL_Octet)(((int)(C[2]) - '0'));
  } else if ('A' <= C[2] && C[2] <= 'F') {
    Result += (SDL_Octet)(((int)(C[2]) - 'A' + 10));
  } else if ('a' <= C[2] && C[2] <= 'f') {
    Result += (SDL_Octet)(((int)(C[2]) - 'a' + 10));
  } else {
#ifdef XECSOP
    xSDLOpError("HexStr in sort Octet",
                "Illegal character in Charstring (not digit or a-f).");
#endif
    if (C != (SDL_Charstring)0 && C[0] == 'T')
      yFree_SDL_Charstring((void **)&C);
    return (SDL_Octet)0;
  }
  if (C != (SDL_Charstring)0 && C[0] == 'T')
    yFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_HEXSTR_OCTET */
#endif /* XNOUSEOFOCTETBITSTRING */



#ifndef XNOUSEOFOCTETBITSTRING
/*** Bit_String ***************************************************/

/*---+---------------------------------------------------------------
     yAddr_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_MODIFY_BIT_STRING
SDL_Bit * yAddr_SDL_Bit_String(
  SDL_Bit_String *B,
  SDL_Integer     Index)
{
  if ((Index < 0) || (Index >= B->Length)) {
#ifdef XECSOP
    xSDLOpError("Modify! in sort Bit_String/Octet_String", "Index out of bounds.");
#endif
    if (B->Bits != 0)
      return &(B->Bits[0]);
    else
      return (SDL_Bit *)XALLOC((xptrint)1,
			       (tSDLTypeInfo *)&ySDL_SDL_Bit);  /* ??? */
  } else
    return &(B->Bits[Index]);
}
#endif /* XNOUSE_MODIFY_BIT_STRING */


/*---+---------------------------------------------------------------
     xExtr_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_EXTRACT_BIT_STRING
SDL_Bit xExtr_SDL_Bit_String(
  SDL_Bit_String  B,
  SDL_Integer     Index)
{
  SDL_Bit Result;
  if ((Index < 0) || (Index >= B.Length)) {
#ifdef XECSOP
    xSDLOpError("Extract! in sort Bit_String/Octet_String", "Index out of bounds.");
#endif
    Result = 0;
  } else
    Result = B.Bits[Index];
  if (! B.IsAssigned) yFree_SDL_Bit_String((void **)&B);
  return Result;
}
#endif /* XNOUSE_EXTRACT_BIT_STRING */


/*---+---------------------------------------------------------------
     xNot_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_NOT_BIT_STRING
SDL_Bit_String *xNot_SDL_Bit_String (
  SDL_Bit_String *B,
  SDL_Bit_String *Result)
{
  int i;
  Result->Length = B->Length;
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)(B->Length),
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  for (i=0; i<B->Length; i++) {
    Result->Bits[i] = (unsigned char)(! B->Bits[i]);
  }
  if (! B->IsAssigned) yFree_SDL_Bit_String((void **)B);
  return Result;
}
#endif /* XNOUSE_NOT_BIT_STRING */


/*---+---------------------------------------------------------------
     xAnd_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_AND_BIT_STRING
SDL_Bit_String *xAnd_SDL_Bit_String (
  SDL_Bit_String *B1,
  SDL_Bit_String *B2,
  SDL_Bit_String *Result)
{
  int i, Min_Length;

  if (B1->Length >= B2->Length) {
    Result->Length = B1->Length;
    Min_Length = B2->Length;
  } else {
    Result->Length = B2->Length;
    Min_Length = B1->Length;
  }
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)(Result->Length),
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  for (i=0; i<Min_Length; i++)
    Result->Bits[i] = B1->Bits[i] & B2->Bits[i];
  for (i=Min_Length; i<Result->Length; i++)
    Result->Bits[i] = 0;
  if (! B1->IsAssigned) yFree_SDL_Bit_String((void **)B1);
  if (! B2->IsAssigned) yFree_SDL_Bit_String((void **)B2);
  return Result;
}
#endif /* XNOUSE_AND_BIT_STRING */


/*---+---------------------------------------------------------------
     xOr_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_OR_BIT_STRING
SDL_Bit_String *xOr_SDL_Bit_String (
  SDL_Bit_String *B1,
  SDL_Bit_String *B2,
  SDL_Bit_String *Result)
{
  int i, Min_Length;

  if (B1->Length >= B2->Length) {
    Result->Length = B1->Length;
    Min_Length = B2->Length;
  } else {
    Result->Length = B2->Length;
    Min_Length = B1->Length;
  }
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)(Result->Length),
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  for (i=0; i<Min_Length; i++)
    Result->Bits[i] = B1->Bits[i] | B2->Bits[i];
  for (i=Min_Length; i<Result->Length; i++)
    Result->Bits[i] = 1;
  if (! B1->IsAssigned) yFree_SDL_Bit_String((void **)B1);
  if (! B2->IsAssigned) yFree_SDL_Bit_String((void **)B2);
  return Result;
}
#endif /* XNOUSE_OR_BIT_STRING */


/*---+---------------------------------------------------------------
     xXor_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_XOR_BIT_STRING
SDL_Bit_String *xXor_SDL_Bit_String (
  SDL_Bit_String *B1,
  SDL_Bit_String *B2,
  SDL_Bit_String *Result)
{
  int i, Min_Length;

  if (B1->Length >= B2->Length) {
    Result->Length = B1->Length;
    Min_Length = B2->Length;
  } else {
    Result->Length = B2->Length;
    Min_Length = B1->Length;
  }
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)(Result->Length),
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  for (i=0; i<Min_Length; i++)
    Result->Bits[i] = B1->Bits[i] ^ B2->Bits[i];
  for (i=Min_Length; i<Result->Length; i++)
    Result->Bits[i] = 1;
  if (! B1->IsAssigned) yFree_SDL_Bit_String((void **)B1);
  if (! B2->IsAssigned) yFree_SDL_Bit_String((void **)B2);
  return Result;
}
#endif /* XNOUSE_XOR_BIT_STRING */


/*---+---------------------------------------------------------------
     xImpl_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_IMPL_BIT_STRING
SDL_Bit_String *xImpl_SDL_Bit_String (
  SDL_Bit_String *B1,
  SDL_Bit_String *B2,
  SDL_Bit_String *Result)
{
  int i, Min_Length;

  if (B1->Length >= B2->Length) {
    Result->Length = B1->Length;
    Min_Length = B2->Length;
  } else {
    Result->Length = B2->Length;
    Min_Length = B1->Length;
  }
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)(Result->Length),
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  for (i=0; i<Min_Length; i++)
    Result->Bits[i] = (unsigned char)(B1->Bits[i] <= B2->Bits[i]);
  for (i=Min_Length; i<Result->Length; i++)
    Result->Bits[i] = 1;
  if (! B1->IsAssigned) yFree_SDL_Bit_String((void **)B1);
  if (! B2->IsAssigned) yFree_SDL_Bit_String((void **)B2);
  return Result;
}
#endif /* XNOUSE_IMPL_BIT_STRING */


/*---+---------------------------------------------------------------
     xMkString_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_MKSTRING_BIT_STRING
SDL_Bit_String *xMkString_SDL_Bit_String(
  SDL_Bit B,
  SDL_Bit_String *Result)
{
  Result->Length = 1;
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)1,
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  Result->Bits[0] = B;
  return Result;
}
#endif /* XNOUSE_MKSTRING_BIT_STRING */


/*---+---------------------------------------------------------------
     xLength_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_LENGTH_BIT_STRING
SDL_Integer xLength_SDL_Bit_String (SDL_Bit_String *B)
{
  SDL_Integer length;
  length = B->Length;
  if (! B->IsAssigned) yFree_SDL_Bit_String((void **)B);
  return length;
}
#endif /* XNOUSE_LENGTH_BIT_STRING */


/*---+---------------------------------------------------------------
     xFirst_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_FIRST_BIT_STRING
SDL_Bit xFirst_SDL_Bit_String (SDL_Bit_String *B)
{
  SDL_Bit Result;
  if (B->Length == 0) {
#ifdef XECSOP
    xSDLOpError("First in sort Bit_String", "Bit_String length is zero.");
#endif
    Result = (SDL_Bit)0;
  } else {
    Result =  B->Bits[0];
  }
  if (! B->IsAssigned) yFree_SDL_Bit_String((void **)B);
  return Result;
}
#endif /* XNOUSE_FIRST_BIT_STRING */


/*---+---------------------------------------------------------------
     xLast_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_LAST_BIT_STRING
SDL_Bit xLast_SDL_Bit_String (SDL_Bit_String *B)
{
  SDL_Bit Result;
  if (B->Length == 0) {
#ifdef XECSOP
    xSDLOpError("Last in sort Bit_String", "Bit_String length is zero.");
#endif
    Result = (SDL_Bit)0;
  } else {
    Result = B->Bits[B->Length-1];
  }
  if (! B->IsAssigned) yFree_SDL_Bit_String((void **)B);
  return Result;
}
#endif /* XNOUSE_LAST_BIT_STRING */


/*---+---------------------------------------------------------------
     xConcat_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_CONCAT_BIT_STRING
SDL_Bit_String *xConcat_SDL_Bit_String (
  SDL_Bit_String *B1,
  SDL_Bit_String *B2,
  SDL_Bit_String *Result)
{

  Result->Length = B1->Length+B2->Length;
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)(Result->Length),
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  if (B1->Length>0)
    (void)memcpy((void *)Result->Bits, (void *)B1->Bits, B1->Length);
  if (B2->Length>0)
    (void)memcpy((void *)&(Result->Bits[B1->Length]), (void *)B2->Bits, B2->Length);
  if (! B1->IsAssigned) yFree_SDL_Bit_String((void **)B1);
  if (! B2->IsAssigned) yFree_SDL_Bit_String((void **)B2);
  return Result;
}
#endif /* XNOUSE_CONCAT_BIT_STRING */


/*---+---------------------------------------------------------------
     xSubString_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_SUBSTRING_BIT_STRING
SDL_Bit_String *xSubString_SDL_Bit_String (
  SDL_Bit_String *B,
  SDL_Integer     Start,
  SDL_Integer     SubLength,
  SDL_Bit_String *Result)
{
  Result->IsAssigned = (xbool)0;
  Result->Bits = 0;
  if (B->Length == 0) {
#ifdef XECSOP
    xSDLOpError("SubString in sort Bit_String/Octet_String",
                "Bit_String length is zero.");
#endif
    Result->Length = 0;
  } else if (Start < 0) {
#ifdef XECSOP
    xSDLOpError("SubString in sort Bit_String/Octet_String",
                "Substring start too small.");
#endif
    Result->Length = 0;
  } else if (SubLength < 0) {
#ifdef XECSOP
    xSDLOpError("SubString in sort Bit_String/Octet_String",
                "SubLength is less than zero.");
#endif
    Result->Length = 0;
  } else if (SubLength == 0) {
    Result->Length = 0;
  } else if (Start+SubLength > B->Length) { 
#ifdef XECSOP
    xSDLOpError("SubString in sort Bit_String/Octet_String",
                "Start + Substring length is greater than string length.");
#endif
    Result->Length = 0;
  } else {
    Result->Length = SubLength;
    Result->Bits = (unsigned char *)XALLOC((xptrint)(Result->Length),
					   (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
    (void)memcpy((void *)Result->Bits, (void *)&(B->Bits[Start]), Result->Length);
  }
  if (! B->IsAssigned) yFree_SDL_Bit_String((void **)B);
  return Result;
}
#endif /* XNOUSE_SUBSTRING_BIT_STRING */


/*---+---------------------------------------------------------------
     xBitStr_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_BITSTR_BIT_STRING
SDL_Bit_String *xBitStr_SDL_Bit_String (
  SDL_Charstring C,
  SDL_Bit_String *Result)
{
  int i;
  Result->Length = (C == 0) ? 0 : strlen(C)-1;
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)(Result->Length),
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  for (i=0; i<Result->Length; i++) {
    if (C[i+1] == '0') {
      Result->Bits[i] = (SDL_Bit)0;
    } else if (C[i+1] == '1') {
      Result->Bits[i] = (SDL_Bit)1;
    } else {
#ifdef XECSOP
      xSDLOpError("BitStr in sort Bit_String",
		  "Illegal character in Charstring (not 0 or 1)->");
#endif
      Result->Bits[i] = (SDL_Bit)1;
    }
  }
  if (C != (SDL_Charstring)0 && C[0] == 'T') yFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_BITSTR_BIT_STRING */


/*---+---------------------------------------------------------------
     xHexStr_SDL_Bit_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_HEXSTR_BIT_STRING
SDL_Bit_String *xHexStr_SDL_Bit_String (
  SDL_Charstring C,
  SDL_Bit_String *Result)
{
  int C_length, C_index, value;

  C_length = (C == 0) ? 0 : strlen(C)-1;
  Result->Length = C_length*4;
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)Result->Length,
					 (tSDLTypeInfo *)&ySDL_SDL_Bit_String);
  for (C_index=1; C_index<=C_length; C_index++) {
    value = (int)(C[C_index]) - 'a' + 10;
    if (value < 10) value = (int)(C[C_index]) - 'A' + 10;
    if (value < 10) value = (int)(C[C_index]) - '0';
    
    if (value < 0 || value > 15) {
#ifdef XECSOP
      xSDLOpError("HexStr in sort Bit_String",
		  "Illegal character in Charstring (not digit or a-f).");
#endif
      Result->Bits[(C_index-1)*4] = 0;
      Result->Bits[(C_index-1)*4+1] = 0;
      Result->Bits[(C_index-1)*4+2] = 0;
      Result->Bits[(C_index-1)*4+3] = 0;
    } else {
      Result->Bits[(C_index-1)*4] = (unsigned char)(value/8);
      Result->Bits[(C_index-1)*4+1] = (unsigned char)((value/4)%2);
      Result->Bits[(C_index-1)*4+2] = (unsigned char)((value/2)%2);
      Result->Bits[(C_index-1)*4+3] = (unsigned char)(value%2);
    }
  }
  if (C != (SDL_Charstring)0 && C[0] == 'T') yFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_HEXSTR_BIT_STRING */
#endif /* XNOUSEOFOCTETBITSTRING */


#ifndef XNOUSEOFOCTETBITSTRING
/*** Octet_String **************************************************/

/*---+---------------------------------------------------------------
     xBitStr_SDL_Octet_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_BITSTR_OCTET_STRING
SDL_Octet_String *xBitStr_SDL_Octet_String (
  SDL_Charstring C,
  SDL_Octet_String *Result)
{
  int i, len;

  len = (C == 0) ? 0 : strlen(C)-1;
  Result->Length = len/8 + (len%8>0 ? 1 : 0);
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)Result->Length,
					 (tSDLTypeInfo *)&ySDL_SDL_Octet_String);
  
  for (i=0; i<len; i++) {
    Result->Bits[i/8] <<= 1;
    if (C[i+1] == '1') {
      Result->Bits[i/8] += 1;
#ifdef XECSOP
    } else if (C[i+1] != '0') {
      xSDLOpError("BitStr in sort Octet_String",
		  "An Octet_String should consist of 0 and 1.");
#endif
    }
  }
  for ( ; i<8*Result->Length; i++)
    Result->Bits[i/8] <<= 1;
  if (C != (SDL_Charstring)0 && C[0] == 'T') yFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_BITSTR_OCTET_STRING */


/*---+---------------------------------------------------------------
     xHexStr_SDL_Octet_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_HEXSTR_OCTET_STRING
SDL_Octet_String *xHexStr_SDL_Octet_String (
  SDL_Charstring C,
  SDL_Octet_String *Result)
{
  int C_length, C_index, res;

  C_length = (C == 0) ? 0 : strlen(C)-1;
  Result->Length = C_length/2 + (C_length%2 == 1 ? 1 : 0);
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)Result->Length,
					 (tSDLTypeInfo *)&ySDL_SDL_Octet_String);
  for (C_index=1; C_index<=C_length; C_index=C_index+2) {
    if ('0' <= C[C_index] && C[C_index] <= '9') {
      res = 16 * ((int)(C[C_index]) - '0');
    } else if ('A' <= C[C_index] && C[C_index] <= 'F') {
      res = 16 * ((int)(C[C_index]) - 'A' + 10);
    } else if ('a' <= C[C_index] && C[C_index] <= 'f') {
      res = 16 * ((int)(C[C_index]) - 'a' + 10);
    } else {
#ifdef XECSOP
      xSDLOpError("HexStr in sort Octet_String",
		  "Illegal character in Charstring (not digit or a-f).");
#endif
      res = 0;
    }
    
    if (C_index<C_length) {
      if ('0' <= C[C_index+1] && C[C_index+1] <= '9') {
	res = res + ((int)(C[C_index+1]) - '0');
      } else if ('A' <= C[C_index+1] && C[C_index+1] <= 'F') {
	res = res + ((int)(C[C_index+1]) - 'A' + 10);
      } else if ('a' <= C[C_index+1] && C[C_index+1] <= 'f') {
	res = res + ((int)(C[C_index+1]) - 'a' + 10);
#ifdef XECSOP
      } else {
	xSDLOpError("HexStr in sort Octet_String",
		    "Illegal character in Charstring (not digit or a-f).");
#endif
      }
    }
    Result->Bits[(C_index-1)/2] = (unsigned char)res;
  }
  if (C != (SDL_Charstring)0 && C[0] == 'T') yFree_SDL_Charstring((void **)&C);
  return Result;
}
#endif /* XNOUSE_HEXSTR_OCTET_STRING */


/*---+---------------------------------------------------------------
     xBit_String_SDL_Octet_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_BIT_STRING_OCTET_STRING
SDL_Bit_String *xBit_String_SDL_Octet_String (
  SDL_Octet_String *S,
  SDL_Octet_String *Result)
{
  int i;

  Result->Length = S->Length*8;
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)Result->Length,
					 (tSDLTypeInfo *)&ySDL_SDL_Octet_String);
  
  for (i=0; i<Result->Length; i=i+8) {
    Result->Bits[i]   = (unsigned char)( S->Bits[i/8]/128);
    Result->Bits[i+1] = (unsigned char)((S->Bits[i/8]/64)%2);
    Result->Bits[i+2] = (unsigned char)((S->Bits[i/8]/32)%2);
    Result->Bits[i+3] = (unsigned char)((S->Bits[i/8]/16)%2);
    Result->Bits[i+4] = (unsigned char)((S->Bits[i/8]/8)%2);
    Result->Bits[i+5] = (unsigned char)((S->Bits[i/8]/4)%2);
    Result->Bits[i+6] = (unsigned char)((S->Bits[i/8]/2)%2);
    Result->Bits[i+7] = (unsigned char)( S->Bits[i/8]%2);
  }
  if (! S->IsAssigned) yFree_SDL_Bit_String((void **)S);
  return Result;
}
#endif /* XNOUSE_BIT_STRING_OCTET_STRING */


/*---+---------------------------------------------------------------
     xOctet_String_SDL_Octet_String
-------------------------------------------------------------------*/
#ifndef XNOUSE_OCTET_STRING_OCTET_STRING
SDL_Octet_String *xOctet_String_SDL_Octet_String (
  SDL_Bit_String   *S,
  SDL_Octet_String *Result)
{
  int i;

  Result->Length = S->Length/8 + (S->Length%8>0 ? 1 : 0);
  Result->IsAssigned = (xbool)0;
  Result->Bits = (unsigned char *)XALLOC((xptrint)Result->Length,
					 (tSDLTypeInfo *)&ySDL_SDL_Octet_String);
  
  for (i=0; i<S->Length; i++) {
    Result->Bits[i/8] <<= 1;
    Result->Bits[i/8] += S->Bits[i];
  }
  for ( ; i%8 != 0; i++) {
    Result->Bits[i/8] <<= 1;    /* Extending with 0, not 1 as Z105 */
  }
  if (! S->IsAssigned) yFree_SDL_Bit_String((void **)S);
  return Result;
}
#endif /* XNOUSE_OCTET_STRING_OCTET_STRING */
#endif /* XNOUSEOFOCTETBITSTRING */




/****+***************************************************************
04   Generic implementation of operators
********************************************************************/

SDL_Boolean GenericIsAssigned(void *yVar, tSDLTypeInfo * Sort)
{
  int i;

  while (Sort->TypeClass == type_SDL_Syntype ||
         Sort->TypeClass == type_SDL_Inherits)
    Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;

  switch (Sort->TypeClass) {
  case type_SDL_Charstring :
  case type_SDL_IA5String :
  case type_SDL_NumericString :
  case type_SDL_PrintableString :
  case type_SDL_VisibleString :
    if (*(SDL_Charstring *)yVar != 0)
      return (*(SDL_Charstring *)yVar)[0] != 'T';
    return SDL_True;

#ifndef XNOUSEOFOCTETBITSTRING
  case type_SDL_Bit_string :
  case type_SDL_Octet_string :
    return ((SDL_Bit_String *)yVar)->IsAssigned;
#endif

#ifndef XNOUSE_STRUCT
  case type_SDL_Struct :
    for (i=0; i<((tSDLStructInfo *)Sort)->NumOfComponents; i++)
      if (NEEDSFREE(((tSDLStructInfo *)Sort)->Components[i].CompSort))
	return GenericIsAssigned(
           (void **)((xptrint)yVar+((tSDLStructInfo *)Sort)->Components[i].Offset),
	   ((tSDLStructInfo *)Sort)->Components[i].CompSort);
    return SDL_True;
#endif

#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
  case type_SDL_Union :
  case type_SDL_Choice :
    i = GenericGetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, yVar);
    if (NEEDSFREE(((tSDLChoiceInfo *)Sort)->Components[i].CompSort))
      return GenericIsAssigned(
        (void **)((xptrint)yVar+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
	((tSDLChoiceInfo *)Sort)->Components[i].CompSort);
    return SDL_True;
#endif

#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
  case type_SDL_Array :
  case type_SDL_Carray :
    return GenericIsAssigned(yVar, ((tSDLArrayInfo *)Sort)->CompSort);
#endif

#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray :
    return ((xGArray_Type *)yVar)->IsAssigned;
#endif

  case type_SDL_Object_identifier :
  case type_SDL_String :
  case type_SDL_GPowerset :
    return ((xString_Type *)yVar)->IsAssigned;

#ifndef XNOUSE_LSTRING_GENERATOR
  case type_SDL_LString :
    if (((xLString_Type *)yVar)->Length == 0) return SDL_False;
    return GenericIsAssigned(
      (void *)((xptrint)yVar + ((tSDLLStringInfo *)Sort)->DataOffset),
      ((tSDLLStringInfo *)Sort)->CompSort);
#endif

#ifndef XNOUSE_BAG_GENERATOR
  case type_SDL_Bag :
    return ((xBag_Type *)yVar)->IsAssigned;
#endif

#ifndef XNOUSE_OWN_GENERATOR
  case type_SDL_Own :
    return SDL_True;    /* ?? */
#endif

  default :
    return SDL_True;
  }
}



void GenericFreeSort(void **yVar, tSDLTypeInfo *Sort)
{
  int i;

  while (1) {
#ifndef XNOUSE_OPFUNCS
    if (Sort->OpFuncs && Sort->OpFuncs->FreeFunc) {
      Sort->OpFuncs->FreeFunc(yVar);
      return;
    }
#endif
    if ((Sort->TypeClass == type_SDL_Syntype) ||
        (Sort->TypeClass == type_SDL_Inherits))
      Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;
    else
      break;
  }

  switch (Sort->TypeClass) {

#ifndef XNOUSE_FREE_CHARSTRING
  case type_SDL_Charstring :
  case type_SDL_IA5String :
  case type_SDL_NumericString :
  case type_SDL_PrintableString :
  case type_SDL_VisibleString :
    if (*(SDL_Charstring *)yVar != (SDL_Charstring)0)
      XFREE(yVar, strlen(*(SDL_Charstring *)yVar)+1);
    break;
#endif

#ifndef XNOUSE_FREE_BIT_STRING
  case type_SDL_Bit_string :
  case type_SDL_Octet_string :
#ifndef XNOUSEOFOCTETBITSTRING
    if (((SDL_Bit_String *)yVar)->Bits)
      XFREE((void **)&(((SDL_Bit_String *)yVar)->Bits),
	    ((SDL_Bit_String *)yVar)->Length);
#endif
    break;
#endif

#ifndef XNOUSE_STRUCT
  case type_SDL_Struct :
#ifdef __cplusplus
    for (i=((tSDLStructInfo *)Sort)->NumOfComponents-1; i>=0; i--)
#else
    for (i=0; i<((tSDLStructInfo *)Sort)->NumOfComponents; i++)
#endif
      if (NEEDSFREE(((tSDLStructInfo *)Sort)->Components[i].CompSort)) {
	GenericFreeSort((void **)((xptrint)yVar+
				  ((tSDLStructInfo *)Sort)->Components[i].Offset),
			((tSDLStructInfo *)Sort)->Components[i].CompSort);
      }
    break;
#endif

#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
  case type_SDL_Union :
  case type_SDL_Choice :
    i = GenericGetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, yVar);
    if (NEEDSFREE(((tSDLChoiceInfo *)Sort)->Components[i].CompSort)) {
      GenericFreeSort((void **)((xptrint)yVar+
				((tSDLChoiceInfo *)Sort)->OffsetToUnion),
		      ((tSDLChoiceInfo *)Sort)->Components[i].CompSort);
    }
    memset(yVar, 0, Sort->SortSize);
    break;
#endif

#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
  case type_SDL_Array :
  case type_SDL_Carray :
    if (NEEDSFREE(((tSDLArrayInfo *)Sort)->CompSort))
#ifdef __cplusplus
      for (i=((tSDLArrayInfo *)Sort)->Length-1; i>=0; i--) {
#else
      for (i=0; i<((tSDLArrayInfo *)Sort)->Length; i++) {
#endif
	GenericFreeSort((void **)((xptrint)yVar+
				  i*((tSDLArrayInfo *)Sort)->CompSort->SortSize),
			((tSDLArrayInfo *)Sort)->CompSort);
      }
    break;
#endif

#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray :
    {
      xGArray_yptr CompPtr;
      for (CompPtr = ((xGArray_Type *)yVar)->First;
	   CompPtr;
	   CompPtr = ((xGArray_Type *)yVar)->First) {
	if (NEEDSFREE(((tSDLGArrayInfo *)Sort)->IndexSort)) {
	  GenericFreeSort((void **)((xptrint)CompPtr+
				    ((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
			  ((tSDLGArrayInfo *)Sort)->IndexSort);
	}
	if (NEEDSFREE(((tSDLGArrayInfo *)Sort)->CompSort)) {
	  GenericFreeSort((void **)((xptrint)CompPtr+
				    ((tSDLGArrayInfo *)Sort)->yrecDataOffset),
			  ((tSDLGArrayInfo *)Sort)->CompSort);
	}
	((xGArray_Type *)yVar)->First = CompPtr->Suc;
	XFREE((void **)&CompPtr, ((tSDLGArrayInfo *)Sort)->yrecSize);
      }
      if (NEEDSFREE(((tSDLGArrayInfo *)Sort)->CompSort)) {
	GenericFreeSort((void **)((xptrint)yVar+
				  ((tSDLGArrayInfo *)Sort)->arrayDataOffset),
			((tSDLGArrayInfo *)Sort)->CompSort);
      }
    }
    break;
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_GPOWERSET_GENERATOR) || !defined(XNOUSE_BAG_GENERATOR) || !defined(XNOUSE_FREE_OBJECT_IDENTIFIER)
  case type_SDL_Object_identifier :
  case type_SDL_String :
  case type_SDL_GPowerset :
  case type_SDL_Bag :
    {
      xString_yptr CompPtr;
      for (CompPtr = ((xString_Type *)yVar)->First;
	   CompPtr;
	   CompPtr = ((xString_Type *)yVar)->First) {
	if (NEEDSFREE(((tSDLGenListInfo *)Sort)->CompSort)) {
	  GenericFreeSort((void **)((xptrint)CompPtr+
				    ((tSDLGenListInfo *)Sort)->yrecDataOffset),
			  ((tSDLGenListInfo *)Sort)->CompSort);
	}
	((xString_Type *)yVar)->First = CompPtr->Suc;
	XFREE((void **)&CompPtr, ((tSDLGenListInfo *)Sort)->yrecSize);
      }
    }
    break;
#endif

#ifndef XNOUSE_LSTRING_GENERATOR
  case type_SDL_LString :
    for (i = 0; i<((xLString_Type *)yVar)->Length; i++)
      GenericFreeSort(
        (void **)((xptrint)yVar + ((tSDLLStringInfo *)Sort)->DataOffset +
	 	i*((tSDLLStringInfo *)Sort)->CompSort->SortSize),
        ((tSDLLStringInfo *)Sort)->CompSort);
    break;
#endif

#ifndef XNOUSE_OWN_GENERATOR
  case type_SDL_Own :
    {
      if (*yVar) {
	if (NEEDSFREE(((tSDLGenInfo *)Sort)->CompOrFatherSort))
	  GenericFreeSort((void **)(*yVar), ((tSDLGenInfo *)Sort)->CompOrFatherSort);
#ifndef XSCT_CMICRO
#ifdef XTRACE
        if (xShouldBeTraced(xtTen, (xPrsNode)0)) {
          char C[200];
#ifdef T_SDL_NAMES
          sprintf(C, "*     FREE of own data %s at address %p\n",
                  Sort->Name, *yVar);
#else
          sprintf(C, "*     FREE of own data at address %p\n", *yVar);
#endif
          xPrintString(C);
        }
#endif
#endif
#ifdef XEOWN
	xRemoveOwnList((void *)*yVar);
#endif
#ifdef XVALIDATOR_LIB
	XFREE_REF_VAL(yVar, ((tSDLGenInfo *)Sort)->CompOrFatherSort->SortSize);
#else
	XFREE(yVar, ((tSDLGenInfo *)Sort)->CompOrFatherSort->SortSize);
#endif
      }
    }
    break;
#endif

  default :
    break;
  }
}




SDL_Boolean GenericEqualSort(void *yExpr1, void *yExpr2, tSDLTypeInfo *Sort)
{
  SDL_Boolean Result;
  int i;

  while (1) {
#ifndef XNOUSE_OPFUNCS
    if (Sort->OpFuncs && Sort->OpFuncs->EqFunc)
      return Sort->OpFuncs->EqFunc(yExpr1, yExpr2);
#endif
    if ((Sort->TypeClass == type_SDL_Syntype) ||
        (Sort->TypeClass == type_SDL_Inherits))
      Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;
    else
      break;
  }

  if (! NEEDSEQUAL(Sort))
    return !memcmp(yExpr1, yExpr2, Sort->SortSize);

  switch (Sort->TypeClass) {
#ifndef XNOUSE_EQ_CHARSTRING
  case type_SDL_Charstring :
  case type_SDL_IA5String :
  case type_SDL_NumericString :
  case type_SDL_PrintableString :
  case type_SDL_VisibleString :
    if ( (*(SDL_Charstring *)yExpr1 == 0 ||
	  strlen(*(SDL_Charstring *)yExpr1) <= (unsigned)1) &&
	 (*(SDL_Charstring *)yExpr2 == 0 ||
	  strlen(*(SDL_Charstring *)yExpr2) <= (unsigned)1) )
      Result = SDL_True;
    else if (*(SDL_Charstring *)yExpr1 == 0 || *(SDL_Charstring *)yExpr2 == 0)
      Result = SDL_False;
    else
      Result = !strcmp(*(SDL_Charstring *)yExpr1+1, *(SDL_Charstring *)yExpr2+1);
    
    if (*(SDL_Charstring *)yExpr1 != 0 && (*(SDL_Charstring *)yExpr1)[0] == 'T')
      GenericFreeSort((void **)yExpr1, Sort);
    if (*(SDL_Charstring *)yExpr2 != 0 && (*(SDL_Charstring *)yExpr2)[0] == 'T')
      GenericFreeSort((void **)yExpr2, Sort);
    return Result;
#endif

#ifndef XNOUSEOFOCTETBITSTRING
#ifndef XNOUSE_EQ_BIT_STRING
  case type_SDL_Bit_string :
  case type_SDL_Octet_string :
    if (((SDL_Bit_String *)yExpr1)->Length != ((SDL_Bit_String *)yExpr2)->Length)
      Result = SDL_False;
    else
      Result = !memcmp(((SDL_Bit_String *)yExpr1)->Bits,
		       ((SDL_Bit_String *)yExpr2)->Bits,
		       ((SDL_Bit_String *)yExpr1)->Length);
    if (! ((SDL_Bit_String *)yExpr1)->IsAssigned)
      GenericFreeSort((void **)yExpr1, Sort);
    if (! ((SDL_Bit_String *)yExpr2)->IsAssigned)
      GenericFreeSort((void **)yExpr2, Sort);
    return Result;
#endif
#endif

  case type_SDL_Enum :
    if (((tSDLEnumInfo *)Sort)->CompOrFatherSort != 0)
      return GenericEqualSort(yExpr1, yExpr2,
                              ((tSDLEnumInfo *)Sort)->CompOrFatherSort);
    return !memcmp(yExpr1, yExpr2, Sort->SortSize);

#ifndef XNOUSE_STRUCT
  case type_SDL_Struct :
    {
      unsigned char pres;
      int FirstBitF;
      FirstBitF = -1;
      Result = SDL_True;
      for (i=0; i<((tSDLStructInfo *)Sort)->NumOfComponents; i++) {
	if (((tSDLStructInfo *)Sort)->Components[i].Offset == (xptrint)~0) {
	  /* Bitfield */
	  if (FirstBitF == -1) FirstBitF = i;
	  if (Result && i == ((tSDLStructInfo *)Sort)->NumOfComponents-1) {
	    /* Take care of bitfield if last struct component */
            if ( FirstBitF == 0 ) {
              /* Take care if all fields are bitfield */
              Result = !memcmp( yExpr1, yExpr2, ((tSDLStructInfo *)Sort)->SortSize );
            }
            else {
              Result = !memcmp(
	      (void *)(
	        (xptrint)yExpr1+
		((tSDLStructInfo *)Sort)->Components[FirstBitF-1].Offset+
		((tSDLStructInfo *)Sort)->Components[FirstBitF-1].CompSort->SortSize),
	      (void *)(
		(xptrint)yExpr2+
		((tSDLStructInfo *)Sort)->Components[FirstBitF-1].Offset+
		((tSDLStructInfo *)Sort)->Components[FirstBitF-1].CompSort->SortSize),
	      ((tSDLStructInfo *)Sort)->SortSize-
	      ((tSDLStructInfo *)Sort)->Components[FirstBitF-1].Offset-
	      ((tSDLStructInfo *)Sort)->Components[FirstBitF-1].CompSort->SortSize
	      );
            }
          }
	} else {
	  if (Result && FirstBitF >= 0) {
            /* Take care of previous bitfield */
	    if (FirstBitF == 0) {
	      Result = !memcmp(yExpr1, yExpr2,
			       ((tSDLStructInfo *)Sort)->Components[i].Offset-1);
	    } else {
	      Result = !memcmp(
		(void *)(
		  (xptrint)yExpr1+
		  ((tSDLStructInfo *)Sort)->Components[FirstBitF-1].Offset+
		  ((tSDLStructInfo *)Sort)->Components[FirstBitF-1].CompSort->SortSize),
		(void *)(
		  (xptrint)yExpr2+
		  ((tSDLStructInfo *)Sort)->Components[FirstBitF-1].Offset+
		  ((tSDLStructInfo *)Sort)->Components[FirstBitF-1].CompSort->SortSize),
		((tSDLStructInfo *)Sort)->Components[i].Offset-
		((tSDLStructInfo *)Sort)->Components[FirstBitF-1].Offset-
		((tSDLStructInfo *)Sort)->Components[FirstBitF-1].CompSort->SortSize
		);
	    }
	    FirstBitF = -1;
	  }
	  if (((tSDLStructInfo *)Sort)->Components[i].ExtraInfo &&
              ((tSDLStructInfo *)Sort)->Components[i].ExtraInfo->OffsetPresent) {
	    pres = 0;
	    if (*(SDL_Boolean *)((xptrint)yExpr1+
		 ((tSDLStructInfo *)Sort)->Components[i].ExtraInfo->OffsetPresent))
	      pres = 1;
	    if (*(SDL_Boolean *)((xptrint)yExpr2+
		 ((tSDLStructInfo *)Sort)->Components[i].ExtraInfo->OffsetPresent))
	      pres = pres+2;
	  } else
	    pres = 3;

	  if ((pres==3) && Result) {
	    Result =
	      GenericEqualSort(
                (void **)((xptrint)yExpr1+((tSDLStructInfo *)Sort)->Components[i].Offset),
	        (void **)((xptrint)yExpr2+((tSDLStructInfo *)Sort)->Components[i].Offset),
	        ((tSDLStructInfo *)Sort)->Components[i].CompSort);
	  } else {
	    if (Result && (pres==1 || pres==2)) Result = SDL_False;
	    if (NEEDSFREE(((tSDLStructInfo *)Sort)->Components[i].CompSort)) {
	      if ((pres==1 || pres==3) && !GenericIsAssigned(
	           (void *)((xptrint)yExpr1+((tSDLStructInfo *)Sort)->Components[i].Offset),
		   ((tSDLStructInfo *)Sort)->Components[i].CompSort))
		GenericFreeSort(
		  (void **)((xptrint)yExpr1+((tSDLStructInfo *)Sort)->Components[i].Offset),
		  ((tSDLStructInfo *)Sort)->Components[i].CompSort);
	      if ((pres==2 || pres==3) && !GenericIsAssigned(
	           (void **)((xptrint)yExpr2+((tSDLStructInfo *)Sort)->Components[i].Offset),
		   ((tSDLStructInfo *)Sort)->Components[i].CompSort))
		GenericFreeSort(
		  (void **)((xptrint)yExpr2+((tSDLStructInfo *)Sort)->Components[i].Offset),
		  ((tSDLStructInfo *)Sort)->Components[i].CompSort);
	    }
	  }
	}
      }
      return Result;
    }
#endif

#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
  case type_SDL_Union :
  case type_SDL_Choice :
    {
      int Tag1, Tag2;
      Tag1 = GenericGetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, yExpr1);
      Tag2 = GenericGetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, yExpr2);
      if (Tag1 == Tag2)
	 return GenericEqualSort(
		 (void *)((xptrint)yExpr1+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
		 (void *)((xptrint)yExpr2+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
		 ((tSDLChoiceInfo *)Sort)->Components[Tag1].CompSort);
      if (NEEDSFREE(((tSDLChoiceInfo *)Sort)->Components[Tag1].CompSort)) {
	if (!GenericIsAssigned(
	       (void *)((xptrint)yExpr1+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
	       ((tSDLChoiceInfo *)Sort)->Components[Tag1].CompSort))
	  GenericFreeSort(
	    (void **)((xptrint)yExpr1+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
	    ((tSDLChoiceInfo *)Sort)->Components[Tag1].CompSort);
      }
      if (NEEDSFREE(((tSDLChoiceInfo *)Sort)->Components[Tag2].CompSort)) {
	if (!GenericIsAssigned(
	       (void *)((xptrint)yExpr2+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
	       ((tSDLChoiceInfo *)Sort)->Components[Tag2].CompSort))
	  GenericFreeSort(
	    (void **)((xptrint)yExpr2+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
	    ((tSDLChoiceInfo *)Sort)->Components[Tag2].CompSort);
      }
      return SDL_False;
    }
#endif
      
#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
  case type_SDL_Array :
  case type_SDL_Carray :
    Result = SDL_True;
    for (i=0; i<((tSDLArrayInfo *)Sort)->Length; i++) {
      if ( ! GenericEqualSort(
               (void *)((xptrint)yExpr1+i*((tSDLArrayInfo *)Sort)->CompSort->SortSize),
	       (void *)((xptrint)yExpr2+i*((tSDLArrayInfo *)Sort)->CompSort->SortSize),
	       ((tSDLArrayInfo *)Sort)->CompSort))
        Result = SDL_False;
    }
    return Result;
#endif

#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray :
    {
      xGArray_yptr CompPtr1, CompPtr2;
      void *CompDefAddr1, *CompDefAddr2;
      CompDefAddr1 = (void *)((xptrint)yExpr1+
			  ((tSDLGArrayInfo *)Sort)->arrayDataOffset);
      CompDefAddr2 = (void *)((xptrint)yExpr2+
			  ((tSDLGArrayInfo *)Sort)->arrayDataOffset);
      Result = GenericEqualSort(CompDefAddr1, CompDefAddr2,
				((tSDLGArrayInfo *)Sort)->CompSort);
      for (CompPtr1 = ((xGArray_Type *)yExpr1)->First;
	   CompPtr1 && Result;
	   CompPtr1 = CompPtr1->Suc) {
	for (CompPtr2 = ((xGArray_Type *)yExpr2)->First;
	     CompPtr2 && Result;
	     CompPtr2 = CompPtr2->Suc)
	  if (GenericEqualSort(
	       (void *)((xptrint)CompPtr1+((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
	       (void *)((xptrint)CompPtr2+((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
               ((tSDLGArrayInfo *)Sort)->IndexSort) )
	    break;
	if (CompPtr2 != 0) {
	  if (!GenericEqualSort(
	         (void *)((xptrint)CompPtr1+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
	         (void *)((xptrint)CompPtr2+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
                 ((tSDLGArrayInfo *)Sort)->CompSort) )
	    Result = SDL_False;
	} else {
	  if (!GenericEqualSort(
	         (void *)((xptrint)CompPtr1+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
	         CompDefAddr2,
                 ((tSDLGArrayInfo *)Sort)->CompSort) )
	    Result = SDL_False;
	}
      }
      for (CompPtr2 = ((xGArray_Type *)yExpr2)->First;
	   CompPtr2 && Result;
	   CompPtr2 = CompPtr2->Suc) {
	for (CompPtr1 = ((xGArray_Type *)yExpr1)->First;
	     CompPtr1 && Result;
	     CompPtr1 = CompPtr1->Suc)
	  if (GenericEqualSort(
	       (void *)((xptrint)CompPtr2+((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
	       (void *)((xptrint)CompPtr1+((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
               ((tSDLGArrayInfo *)Sort)->IndexSort) )
	    break;
	if (CompPtr1 != 0) {
	  if (!GenericEqualSort(
	         (void *)((xptrint)CompPtr2+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
	         (void *)((xptrint)CompPtr1+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
                 ((tSDLGArrayInfo *)Sort)->CompSort) )
	    Result = SDL_False;
	} else {
	  if (!GenericEqualSort(
	         (void **)((xptrint)CompPtr2+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
	         CompDefAddr1,
                 ((tSDLGArrayInfo *)Sort)->CompSort) )
	    Result = SDL_False;
	}
      }
      if (! ((xGArray_Type *)yExpr1)->IsAssigned) GenericFreeSort((void **)yExpr1, Sort);
      if (! ((xGArray_Type *)yExpr2)->IsAssigned) GenericFreeSort((void **)yExpr2, Sort);
      return Result;
    }
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_EQ_OBJECT_IDENTIFIER)
  case type_SDL_Object_identifier :
  case type_SDL_String :
    {
      xString_yptr CompPtr1, CompPtr2;
      Result = ((xString_Type *)yExpr1)->Length ==
	       ((xString_Type *)yExpr2)->Length;
      CompPtr1 = ((xString_Type *)yExpr1)->First;
      CompPtr2 = ((xString_Type *)yExpr2)->First;
      while (Result && CompPtr1 != 0) {
	if (!GenericEqualSort(
	     (void *)((xptrint)CompPtr1+((tSDLGenListInfo *)Sort)->yrecDataOffset),
	     (void *)((xptrint)CompPtr2+((tSDLGenListInfo *)Sort)->yrecDataOffset),
	     ((tSDLGenListInfo *)Sort)->CompSort) )
	  Result = SDL_False;
	CompPtr1 = CompPtr1->Suc;
	CompPtr2 = CompPtr2->Suc;
      }
      if (! ((xString_Type *)yExpr1)->IsAssigned) GenericFreeSort((void **)yExpr1, Sort);
      if (! ((xString_Type *)yExpr2)->IsAssigned) GenericFreeSort((void **)yExpr2, Sort);
      return Result;
    }
#endif

#ifndef XNOUSE_LSTRING_GENERATOR
  case type_SDL_LString :
    Result = ((xLString_Type *)yExpr1)->Length ==
             ((xLString_Type *)yExpr2)->Length;
    for (i = 0; Result && i<((xLString_Type *)yExpr1)->Length; i++)
      if (!GenericEqualSort(
	    (void *)((xptrint)yExpr1 + ((tSDLLStringInfo *)Sort)->DataOffset +
		     i*((tSDLLStringInfo *)Sort)->CompSort->SortSize),
	    (void *)((xptrint)yExpr2 + ((tSDLLStringInfo *)Sort)->DataOffset +
		     i*((tSDLLStringInfo *)Sort)->CompSort->SortSize),
	    ((tSDLLStringInfo *)Sort)->CompSort) )
	Result = SDL_False;
    if (!GenericIsAssigned(yExpr1, Sort)) GenericFreeSort((void **)yExpr1, Sort);
    if (!GenericIsAssigned(yExpr2, Sort)) GenericFreeSort((void **)yExpr2, Sort);
    return Result;
#endif

#ifndef XNOUSE_GPOWERSET_GENERATOR
  case type_SDL_GPowerset :
    {
      /* xString_yptr, ... used also for GPowerset */
      xString_yptr CompPtr1, CompPtr2;
      SDL_Boolean Found;
      Result = ((xString_Type *)yExpr1)->Length == ((xString_Type *)yExpr2)->Length;
      CompPtr1 = ((xString_Type *)yExpr1)->First;
      while (Result && CompPtr1 != 0) {
	Found = SDL_False;
	CompPtr2 = ((xString_Type *)yExpr2)->First;
	while (!Found && CompPtr2 != 0) {
	  if (GenericEqualSort(
	      (void *)((xptrint)CompPtr1+((tSDLGenListInfo *)Sort)->yrecDataOffset),
	      (void *)((xptrint)CompPtr2+((tSDLGenListInfo *)Sort)->yrecDataOffset),
	      ((tSDLGenListInfo *)Sort)->CompSort) )
	    Found = SDL_True;
	  CompPtr2 = CompPtr2->Suc;
	}
	if (!Found) Result = SDL_False;
	CompPtr1 = CompPtr1->Suc;
      }
      if (! ((xString_Type *)yExpr1)->IsAssigned) GenericFreeSort((void **)yExpr1, Sort);
      if (! ((xString_Type *)yExpr2)->IsAssigned) GenericFreeSort((void **)yExpr2, Sort);
      return Result;
    }
#endif

#ifndef XNOUSE_BAG_GENERATOR
  case type_SDL_Bag :
    {
      xBag_yptr CompPtr1, CompPtr2;
      SDL_Boolean Found;
      Result = ((xBag_Type *)yExpr1)->Length == ((xBag_Type *)yExpr2)->Length;
      CompPtr1 = ((xBag_Type *)yExpr1)->First;
      while (Result && CompPtr1 != 0) {
	Found = SDL_False;
	CompPtr2 = ((xBag_Type *)yExpr2)->First;
	while (!Found && CompPtr2 != 0) {
	  if (GenericEqualSort(
	      (void *)((xptrint)CompPtr1+((tSDLGenListInfo *)Sort)->yrecDataOffset),
	      (void *)((xptrint)CompPtr2+((tSDLGenListInfo *)Sort)->yrecDataOffset),
	      ((tSDLGenListInfo *)Sort)->CompSort) ) {
	    Found = SDL_True;
	    if (CompPtr1->NoOfItems != CompPtr2->NoOfItems)
	      Result = SDL_False;
	  }
	  CompPtr2 = CompPtr2->Suc;
	}
	if (!Found) Result = SDL_False;
	CompPtr1 = CompPtr1->Suc;
      }
      if (! ((xBag_Type *)yExpr1)->IsAssigned) GenericFreeSort((void **)yExpr1, Sort);
      if (! ((xBag_Type *)yExpr2)->IsAssigned) GenericFreeSort((void **)yExpr2, Sort);
      return Result;
    }
#endif

#ifndef XNOUSE_OWN_GENERATOR
  case type_SDL_Own :
    if (*(void **)yExpr1==0 && *(void **)yExpr2==0)
      return SDL_True;
    if (*(void **)yExpr1!=0 && *(void **)yExpr2!=0)
      return GenericEqualSort(*(void **)yExpr1, *(void **)yExpr2,
			      ((tSDLGenInfo *)Sort)->CompOrFatherSort);
    return SDL_False;
#endif

  default :
    return !memcmp(yExpr1, yExpr2, Sort->SortSize);
  }
}



void * GenericAssignSort(void *yVar, void *yExpr, int AssType, tSDLTypeInfo *Sort)
{
  int i;

  while (1) {
#ifndef XNOUSE_OPFUNCS
    if (Sort->OpFuncs && Sort->OpFuncs->AssFunc)
      return Sort->OpFuncs->AssFunc(yVar, yExpr, AssType);
#endif
    if ((Sort->TypeClass == type_SDL_Syntype) ||
        (Sort->TypeClass == type_SDL_Inherits))
      Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;
    else
      break;
  }

  if (! NEEDSASSIGN(Sort)) {
    if (yVar != yExpr) memcpy(yVar, yExpr, Sort->SortSize);
    return yVar;
  }
  if (SHOULD_FREE_OLD(AssType) && yVar != yExpr) {
    GenericFreeSort((void **)yVar, Sort);
  }

  if (yVar != yExpr || !SHOULD_FREE_OLD(AssType)) {
    switch (Sort->TypeClass) {
#ifndef XNOUSE_ASSIGN_CHARSTRING
    case type_SDL_Charstring :
    case type_SDL_IA5String :
    case type_SDL_NumericString :
    case type_SDL_PrintableString :
    case type_SDL_VisibleString :
      if (*(SDL_Charstring *)yExpr != 0 &&
	  (SHOULD_COPY(AssType) ||
	   (MAY_REUSE(AssType) && (*(SDL_Charstring *)yExpr)[0] != 'T'))) {
	*(SDL_Charstring *)yVar =
	  (SDL_Charstring)XALLOC((xptrint)(strlen(*(SDL_Charstring *)yExpr)+1),
		 &ySDL_SDL_Charstring);
	(void)strcpy(*(SDL_Charstring *)yVar, *(SDL_Charstring *)yExpr);
      } else
	*(SDL_Charstring *)yVar = *(SDL_Charstring *)yExpr;
      break;
#endif

#ifndef XNOUSE_ASSIGN_BIT_STRING
    case type_SDL_Bit_string :
    case type_SDL_Octet_string :
#ifndef XNOUSEOFOCTETBITSTRING
      if ((*(SDL_Bit_String *)yExpr).Length > 0 &&
	  (SHOULD_COPY(AssType) ||
	   (MAY_REUSE(AssType) && (*(SDL_Bit_String *)yExpr).IsAssigned))) {
	(*(SDL_Bit_String *)yVar).Bits = (unsigned char *)
	  XALLOC((xptrint)((*(SDL_Bit_String *)yExpr).Length),
		 &ySDL_SDL_Bit_String);
	(void)memcpy((void *)(*(SDL_Bit_String *)yVar).Bits,
		     (void *)(*(SDL_Bit_String *)yExpr).Bits,
		     (*(SDL_Bit_String *)yExpr).Length);
      } else {
	(*(SDL_Bit_String *)yVar).Bits = (*(SDL_Bit_String *)yExpr).Bits;
      }
      (*(SDL_Bit_String *)yVar).Length = (*(SDL_Bit_String *)yExpr).Length;
#endif
      break;
#endif

    case type_SDL_Enum :
      if (((tSDLEnumInfo *)Sort)->CompOrFatherSort != 0)
        (void)GenericAssignSort(yVar, yExpr, AssType,
                                ((tSDLEnumInfo *)Sort)->CompOrFatherSort);
      else
        memcpy(yVar, yExpr, Sort->SortSize);
      break;

#ifndef XNOUSE_STRUCT
    case type_SDL_Struct :
      if (SHOULD_MEMCPY(AssType))
        memcpy(yVar, yExpr, Sort->SortSize);
      for (i=0; i<((tSDLStructInfo *)Sort)->NumOfComponents; i++) {
	if (NEEDSASSIGN(((tSDLStructInfo *)Sort)->Components[i].CompSort)) {
	  (void)GenericAssignSort(
	    (void *)((xptrint)yVar+((tSDLStructInfo *)Sort)->Components[i].Offset),
	    (void *)((xptrint)yExpr+((tSDLStructInfo *)Sort)->Components[i].Offset),
	    SET_MEMCPY(SET_NF(AssType)),
	    ((tSDLStructInfo *)Sort)->Components[i].CompSort);
	}
      }      
      break;
#endif

#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
    case type_SDL_Union :
    case type_SDL_Choice :
      AssType = UNSET_MEMCPY(AssType);
      memset(yVar, 0, ((tSDLChoiceInfo *)Sort)->SortSize);
      i = GenericGetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, yExpr);

      GenericSetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, yVar, i);
      (void)GenericAssignSort(
	(void *)((xptrint)yVar+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
	(void *)((xptrint)yExpr+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
	SET_NF(AssType),
	((tSDLChoiceInfo *)Sort)->Components[i].CompSort);
      break;
#endif

#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
    case type_SDL_Array :
    case type_SDL_Carray :
      for (i=0; i<((tSDLArrayInfo *)Sort)->Length; i++) {
	(void)GenericAssignSort(
	  (void *)((xptrint)yVar+i*((tSDLArrayInfo *)Sort)->CompSort->SortSize),
	  (void *)((xptrint)yExpr+i*((tSDLArrayInfo *)Sort)->CompSort->SortSize),
	  SET_NF(AssType),
	  ((tSDLArrayInfo *)Sort)->CompSort);
      }
      break;
#endif

#ifndef XNOUSE_GARRAY_GENERATOR
    case type_SDL_GArray :
      {
	xGArray_yptr yVarPtr, yExprPtr;
        AssType = UNSET_MEMCPY(AssType);
	if (((xGArray_Type *)yExpr)->First &&
	    (SHOULD_COPY(AssType) ||
	     (MAY_REUSE(AssType) && ((xGArray_Type *)yExpr)->IsAssigned))) {
	  yExprPtr = ((xGArray_Type *)yExpr)->First;
	  yVarPtr = (xGArray_yptr)XALLOC(((tSDLGArrayInfo *)Sort)->yrecSize, Sort);
	  ((xGArray_Type *)yVar)->First = yVarPtr;
	  while (1) {
	    (void)GenericAssignSort(
	      (void *)((xptrint)yVarPtr+((tSDLGArrayInfo *)Sort)->yrecIndexOffset), 
	      (void *)((xptrint)yExprPtr+((tSDLGArrayInfo *)Sort)->yrecIndexOffset), 
	      SET_ASS_NF(AssType),
	      ((tSDLGArrayInfo *)Sort)->IndexSort);
	    (void)GenericAssignSort(
	      (void *)((xptrint)yVarPtr+((tSDLGArrayInfo *)Sort)->yrecDataOffset), 
	      (void *)((xptrint)yExprPtr+((tSDLGArrayInfo *)Sort)->yrecDataOffset), 
	      SET_ASS_NF(AssType),
	      ((tSDLGArrayInfo *)Sort)->CompSort);
	    yExprPtr = yExprPtr->Suc;
	    if (! yExprPtr) break;
	    yVarPtr->Suc = (xGArray_yptr)XALLOC(((tSDLGArrayInfo *)Sort)->yrecSize, Sort);
	    yVarPtr = yVarPtr->Suc;
	  }
	  yVarPtr->Suc = 0;
	  ((xGArray_Type *)yVar)->Last = yVarPtr;
	  (void)GenericAssignSort(
	    (void *)((xptrint)yVar+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
	    (void *)((xptrint)yExpr+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
	    SET_ASS_NF(AssType),
	    ((tSDLGArrayInfo *)Sort)->CompSort);
	} else {
	  ((xGArray_Type *)yVar)->First = ((xGArray_Type *)yExpr)->First;
	  ((xGArray_Type *)yVar)->Last = ((xGArray_Type *)yExpr)->Last;
	  (void)GenericAssignSort(
	    (void *)((xptrint)yVar+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
	    (void *)((xptrint)yExpr+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
	    (SHOULD_COPY(AssType) || (MAY_REUSE(AssType) &&
				      ((xGArray_Type *)yExpr)->IsAssigned)
	     ? XASS_MR_ASS_NF : XASS_AR_ASS_NF),
	    ((tSDLGArrayInfo *)Sort)->CompSort);
	}

	/* Remove components with data equal to default value */
	yVarPtr = ((xGArray_Type *)yVar)->First;
	while (yVarPtr &&
	       GenericEqualSort(
		 (void *)((xptrint)yVarPtr+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
		 (void *)((xptrint)yVar+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
		 ((tSDLGArrayInfo *)Sort)->CompSort)) {
	  ((xGArray_Type *)yVar)->First = yVarPtr->Suc;
	  if (NEEDSFREE(((tSDLGArrayInfo *)Sort)->IndexSort))
	    GenericFreeSort(
	      (void **)((xptrint)yVarPtr+((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
	      ((tSDLGArrayInfo *)Sort)->IndexSort);
	  if (NEEDSFREE(((tSDLGArrayInfo *)Sort)->CompSort))
	    GenericFreeSort(
	      (void **)((xptrint)yVarPtr+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
	      ((tSDLGArrayInfo *)Sort)->CompSort);
	  XFREE((void **)&yVarPtr, ((tSDLGArrayInfo *)Sort)->yrecSize);
	  yVarPtr = ((xGArray_Type *)yVar)->First;
	}
	if (yVarPtr != 0) {
	  while (yVarPtr->Suc != 0) {
	    yExprPtr = yVarPtr->Suc;
	    if (GenericEqualSort(
	          (void *)((xptrint)yExprPtr+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
	          (void *)((xptrint)yVar+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
		  ((tSDLGArrayInfo *)Sort)->CompSort)) {
	      yVarPtr->Suc = yExprPtr->Suc;
	      if (NEEDSFREE(((tSDLGArrayInfo *)Sort)->IndexSort))
		GenericFreeSort(
		  (void **)((xptrint)yExprPtr+((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
		  ((tSDLGArrayInfo *)Sort)->IndexSort);
	      if (NEEDSFREE(((tSDLGArrayInfo *)Sort)->CompSort))
		GenericFreeSort(
		  (void **)((xptrint)yExprPtr+((tSDLGArrayInfo *)Sort)->yrecDataOffset),
		  ((tSDLGArrayInfo *)Sort)->CompSort);
	      XFREE((void **)&yExprPtr, ((tSDLGArrayInfo *)Sort)->yrecSize);
	    } else {
	      yVarPtr = yExprPtr;
	    }
	  }
	}
	((xGArray_Type *)yVar)->Last = yVarPtr;
      }
      break;
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_GPOWERSET_GENERATOR) || !defined(XNOUSE_BAG_GENERATOR) || !defined(XNOUSE_ASSIGN_OBJECT_IDENTIFIER)
    case type_SDL_Object_identifier :
    case type_SDL_String :
    case type_SDL_GPowerset :
    case type_SDL_Bag :
      {
	/* Bag type representation is equal to String and GPowerset as
	   long as NoOfItems and Data is not accessed */
	xBag_yptr yVarPtr, yExprPtr;
        AssType = UNSET_MEMCPY(AssType);
	if (((xBag_Type *)yExpr)->Length > 0 &&
	    (SHOULD_COPY(AssType) ||
	     (MAY_REUSE(AssType) && ((xBag_Type *)yExpr)->IsAssigned))) {
	  yExprPtr = ((xBag_Type *)yExpr)->First;
	  yVarPtr = (xBag_yptr)XALLOC(((tSDLGenListInfo *)Sort)->yrecSize, Sort);
	  ((xBag_Type *)yVar)->First = yVarPtr;
	  while (1) {
	    (void)GenericAssignSort(
	      (void *)((xptrint)yVarPtr+((tSDLGenListInfo *)Sort)->yrecDataOffset), 
	      (void *)((xptrint)yExprPtr+((tSDLGenListInfo *)Sort)->yrecDataOffset), 
	      SET_ASS_NF(AssType),
	      ((tSDLGenListInfo *)Sort)->CompSort);
	    if (Sort->TypeClass == type_SDL_Bag)
	      yVarPtr->NoOfItems = yExprPtr->NoOfItems;
	    yExprPtr = yExprPtr->Suc;
	    if (! yExprPtr) break;
	    yVarPtr->Suc = (xBag_yptr)XALLOC(((tSDLGenListInfo *)Sort)->yrecSize, Sort);
	    yVarPtr = yVarPtr->Suc;
	  }
	  yVarPtr->Suc = 0;
	  ((xBag_Type *)yVar)->Last = yVarPtr;
	} else {
	  ((xBag_Type *)yVar)->First = ((xBag_Type *)yExpr)->First;
	  ((xBag_Type *)yVar)->Last = ((xBag_Type *)yExpr)->Last;
	}
	((xBag_Type *)yVar)->Length = ((xBag_Type *)yExpr)->Length;
      }
      break;
#endif

#ifndef XNOUSE_LSTRING_GENERATOR
    case type_SDL_LString :
      for (i = 0; i<((xLString_Type *)yExpr)->Length; i++)
        (void)GenericAssignSort(
          (void *)((xptrint)yVar + ((tSDLLStringInfo *)Sort)->DataOffset +
                   i*((tSDLLStringInfo *)Sort)->CompSort->SortSize),
          (void *)((xptrint)yExpr + ((tSDLLStringInfo *)Sort)->DataOffset +
                   i*((tSDLLStringInfo *)Sort)->CompSort->SortSize),
          SET_NF(AssType),
          ((tSDLLStringInfo *)Sort)->CompSort);
      ((xLString_Type *)yVar)->Length = ((xLString_Type *)yExpr)->Length;
      break;
#endif

#ifndef XNOUSE_OWN_GENERATOR
    case type_SDL_Own :
      if (*(void **)yExpr != 0 && SHOULD_COPY(AssType)) {
#ifdef XVALIDATOR_LIB
        *(void **)yVar = XALLOC_REF_VAL(((tSDLGenInfo *)Sort)->CompOrFatherSort->SortSize,
                                        ((tSDLGenInfo *)Sort)->SortIdNode);
#else
        *(void **)yVar = XALLOC(((tSDLGenInfo *)Sort)->CompOrFatherSort->SortSize,
		                ((tSDLGenInfo *)Sort)->CompOrFatherSort);
#endif
#ifndef XSCT_CMICRO
#ifdef XTRACE
        if (xShouldBeTraced(xtTen, (xPrsNode)0)) {
          char C[200];
#ifdef T_SDL_NAMES
          sprintf(C, "*     ALLOC of own data %s at address %p\n",
                  Sort->Name, *(void **)yVar);
#else
          sprintf(C, "*     ALLOC of own data at address %p\n", *(void **)yVar);
#endif
          xPrintString(C);
        }
#endif
#endif
#ifdef XEOWN
        xInsertOwnList(*(void **)yVar, Sort);
#endif
                /*yAll_?1();   NO CHECKS */
	(void)GenericAssignSort(*(void **)yVar, *(void **)yExpr, XASS_AC_ASS_NF,
			  ((tSDLGenInfo *)Sort)->CompOrFatherSort);
      } else {
	*(void **)yVar = *(void **)yExpr;
	*(void **)yExpr = 0;
      }
      break;
#endif

    default :
      memcpy(yVar, yExpr, Sort->SortSize);
    }
  }

  /* Set up IsAssigned flag */
  switch (Sort->TypeClass) {
#ifndef XNOUSE_ASSIGN_CHARSTRING
  case type_SDL_Charstring :
  case type_SDL_IA5String :
  case type_SDL_NumericString :
  case type_SDL_PrintableString :
  case type_SDL_VisibleString :
    if (*(SDL_Charstring *)yVar != 0)
      (*(SDL_Charstring *)yVar)[0] = (SHOULD_RESULT_BE_ASS(AssType) ? 'V' : 'T');
    break;
#endif
#ifndef XNOUSE_ASSIGN_BIT_STRING
  case type_SDL_Bit_string :
  case type_SDL_Octet_string :
#ifndef XNOUSEOFOCTETBITSTRING
    (*(SDL_Bit_String *)yVar).IsAssigned = SHOULD_RESULT_BE_ASS(AssType);
#endif
    break;
#endif
#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray :
    (*(xGArray_Type *)yVar).IsAssigned = SHOULD_RESULT_BE_ASS(AssType);
    break;
#endif
#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_GPOWERSET_GENERATOR) || !defined(XNOUSE_ASSIGN_OBJECT_IDENTIFIER)
  case type_SDL_Object_identifier :
  case type_SDL_String :
  case type_SDL_GPowerset :
    (*(xString_Type *)yVar).IsAssigned = SHOULD_RESULT_BE_ASS(AssType);
    break;
#endif
#ifndef XNOUSE_BAG_GENERATOR
  case type_SDL_Bag :
    (*(xBag_Type *)yVar).IsAssigned = SHOULD_RESULT_BE_ASS(AssType);
    break;
#endif
  default :
    break;
  }
  return yVar;
}



static int IsPointerToMake(tSDLTypeInfo *Sort)
{
  while (Sort->TypeClass == type_SDL_Syntype ||
         Sort->TypeClass == type_SDL_Inherits)
    Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;

  switch (Sort->TypeClass) {
  case type_SDL_Own :
  case type_SDL_Oref :
  case type_SDL_Ref :
  case type_SDL_Charstring :
  case type_SDL_IA5String :
  case type_SDL_NumericString :
  case type_SDL_PrintableString :
  case type_SDL_VisibleString :
    return 1;
  default :
    return 0;
  }
}

void *GenericMakeStruct(void *Result, tSDLTypeInfo *Sort, ...)
{
  va_list ap;
  void *Param;
  void *Tmp;
  int   i;
  SDL_Boolean TrueValue;
  TrueValue = 1;
  memset(Result, 0, Sort->SortSize);
  va_start(ap, Sort);
  while (Sort->TypeClass == type_SDL_Syntype ||
         Sort->TypeClass == type_SDL_Inherits)
    Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;

  switch (Sort->TypeClass) {
#ifndef XNOUSE_STRUCT
  case type_SDL_Struct :
    for (i=0; i<((tSDLStructInfo *)Sort)->NumOfComponents; i++) {
      if (((tSDLStructInfo *)Sort)->Components[i].Offset == (xptrint)~0) {
	/* bitfield */
	Param = va_arg(ap, void *);
	((tSDLFieldBitFInfo *)((tSDLStructInfo *)Sort)->Components[i].
	  ExtraInfo)->AssTag(Result, *(int *)Param);
      } else if (((tSDLStructInfo *)Sort)->Components[i].ExtraInfo == 0) {
	/* not optional, no initializer */
	Param = va_arg(ap, void *);
	if (IsPointerToMake(((tSDLStructInfo *)Sort)->Components[i].CompSort)) {
	  Tmp = Param;
	  Param = (void *)&Tmp;
	}
	(void)GenericAssignSort(
  	  (void *)((xptrint)Result+((tSDLStructInfo *)Sort)->Components[i].Offset),
	  Param,
	  XASS_MR_TMP_NF,
	  ((tSDLStructInfo *)Sort)->Components[i].CompSort);
      } else {
	/* optional or initializer */
	Param = va_arg(ap, void *);   /* Present, i.e. 0 or 1 */
	if (Param) {
	  Param = va_arg(ap, void *);
	  if (IsPointerToMake(((tSDLStructInfo *)Sort)->Components[i].CompSort)) {
	    Tmp = Param;
	    Param = (void *)&Tmp;
	  }
	  (void)GenericAssignSort(
            (void *)((xptrint)Result+
	      ((tSDLStructInfo *)Sort)->Components[i].ExtraInfo->OffsetPresent),
	    (void *)&TrueValue,
	    XASS_MR_TMP_NF,
	    &ySDL_SDL_Boolean);
	  (void)GenericAssignSort(
            (void *)((xptrint)Result+((tSDLStructInfo *)Sort)->Components[i].Offset),
	    Param,
	    XASS_MR_TMP_NF,
	    ((tSDLStructInfo *)Sort)->Components[i].CompSort);
	} else if (((tSDLStructInfo *)Sort)->Components[i].ExtraInfo->DefaultValue) {
	  /* no parameter, but initializer */
	  (void)GenericAssignSort(
            (void *)((xptrint)Result+((tSDLStructInfo *)Sort)->Components[i].Offset),
	    ((tSDLStructInfo *)Sort)->Components[i].ExtraInfo->DefaultValue,
	    XASS_MR_TMP_NF,
	    ((tSDLStructInfo *)Sort)->Components[i].CompSort);
	}
      }
    }
    break;
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_GPOWERSET_GENERATOR) || !defined(XNOUSE_BAG_GENERATOR) || !defined(XNOUSE_ASSIGN_OBJECT_IDENTIFIER) || !defined(XNOUSE_LSTRING_GENERATOR) || !defined(XNOUSE_POWERSET_GENERATOR)
  case type_SDL_Object_identifier :
  case type_SDL_String :
  case type_SDL_GPowerset :
  case type_SDL_Bag :
  case type_SDL_LString :
  case type_SDL_Powerset :
    {
      int IsPtr;
      IsPtr = IsPointerToMake(((tSDLGenListInfo *)Sort)->CompSort);
      Param = va_arg(ap, void *);
      while (Param != 0) {
	if (IsPtr) {
	  Tmp = Param;
	  Param = (void *)&Tmp;
	}
	switch (Sort->TypeClass) {
#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_ASSIGN_OBJECT_IDENTIFIER)
	case type_SDL_Object_identifier :
	case type_SDL_String :
	  GenString_Append((xString_Type *)Result, Param, (tSDLGenListInfo *)Sort);
	  break;
#endif

#if !defined(XNOUSE_LSTRING_GENERATOR)
	case type_SDL_LString :
         if (((xLString_Type *)Result)->Length !=
              ((tSDLLStringInfo *)Sort)->MaxLength) {
            (void)GenericAssignSort(
                (void *)((xptrint)((xLString_Type *)Result) +
                         ((tSDLLStringInfo *)Sort)->DataOffset +
                         (((xLString_Type *)Result)->Length)*
                          ((tSDLLStringInfo *)Sort)->CompSort->SortSize), 
                Param, XASS_MR_TMP_NF, ((tSDLLStringInfo *)Sort)->CompSort);
            ((xLString_Type *)Result)->Length++;
          }
#ifdef XECSOP
          else {
            xSDLOpError("Make! string sort with upper size",
                        "String is full. Component not appended");
          }
#endif
	  break;
#endif

#if !defined(XNOUSE_GPOWERSET_GENERATOR) || !defined(XNOUSE_BAG_GENERATOR)
	case type_SDL_GPowerset :
	case type_SDL_Bag :
	  GenBag_Incl2(Param, (xBag_Type *)Result, (tSDLGenListInfo *)Sort);
	  break;
#endif

#if !defined(XNOUSE_POWERSET_GENERATOR)
	case type_SDL_Powerset :
	  GenPow_Incl2(*(SDL_Integer *)Param, (xPowerset_Type *)Result,
		       (tSDLPowersetInfo *)Sort);
	  break;
#endif

	default :
	  break;    
	}
	Param = va_arg(ap, void *);
      }
    }
    break;
#endif

  default :
    break;    
  }
  va_end(ap);
  return Result;
}


#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
void *GenericMakeChoice(void *Result, tSDLTypeInfo *Sort, int Tag, void *Comp)
{
  void *Tmp;
  while (Sort->TypeClass == type_SDL_Syntype ||
         Sort->TypeClass == type_SDL_Inherits)
    Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;
  memset(Result, 0, ((tSDLChoiceInfo *)Sort)->SortSize);
  GenericSetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, Result, Tag);
  if (IsPointerToMake(((tSDLChoiceInfo *)Sort)->Components[Tag].CompSort)) {
    Tmp = Comp;
    Comp = (void *)&Tmp;
  }
  (void)GenericAssignSort(
    (void *)((xptrint)Result+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
    Comp, XASS_MR_TMP_NF, ((tSDLChoiceInfo *)Sort)->Components[Tag].CompSort);
  return Result;
}
#endif

#if !defined(XNOUSE_OWN_GENERATOR) || !defined(XNOUSE_REF_GENERATOR)
void *GenericMakeOwnRef(tSDLTypeInfo *Sort, void *Comp)
{
  void *Tmp;
  void *Result;
  while (Sort->TypeClass == type_SDL_Syntype ||
         Sort->TypeClass == type_SDL_Inherits)
    Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;
  if (IsPointerToMake(((tSDLGenInfo *)Sort)->CompOrFatherSort)) {
    Tmp = Comp;
    Comp = (void *)&Tmp;
  }
#ifdef XVALIDATOR_LIB
  Result = XALLOC_REF_VAL(((tSDLGenInfo *)Sort)->CompOrFatherSort->SortSize,
                          ((tSDLGenInfo *)Sort)->SortIdNode);
#else
  Result = XALLOC(((tSDLGenInfo *)Sort)->CompOrFatherSort->SortSize,
                  ((tSDLGenInfo *)Sort)->CompOrFatherSort);
#endif
#ifndef XSCT_CMICRO
#ifdef XTRACE
  if (xShouldBeTraced(xtTen, (xPrsNode)0)) {
    char C[200];
#ifdef T_SDL_NAMES
    sprintf(C, "*     ALLOC of own data %s at address %p\n",
            Sort->Name, Result);
#else
    sprintf(C, "*     ALLOC of own data at address %p\n", Result);
#endif
    xPrintString(C);
  }
#endif
#endif
#ifdef XEOWN
  xInsertOwnList(Result, Sort);
#endif
  (void)GenericAssignSort(Result, Comp, XASS_MR_ASS_NF,
			  ((tSDLGenInfo *)Sort)->CompOrFatherSort);
  return Result;
}
#endif

#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR) || !defined(XNOUSE_GARRAY_GENERATOR)
void *GenericMakeArray(void *Result, tSDLTypeInfo *Sort, void *Comp)
{
  int i;
  void *Tmp;
  while (Sort->TypeClass == type_SDL_Syntype ||
         Sort->TypeClass == type_SDL_Inherits)
    Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;
  switch (Sort->TypeClass) {
#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
  case type_SDL_Array :
  case type_SDL_Carray :
    if (IsPointerToMake(((tSDLArrayInfo *)Sort)->CompSort)) {
      Tmp = Comp;
      Comp = (void *)&Tmp;
    }
    if (((tSDLArrayInfo *)Sort)->CompSort->SortSize == sizeof(int) &&
        ! NEEDSASSIGN(Sort)) {
      for (i=0; i<((tSDLArrayInfo *)Sort)->Length; i++)
        *(int *)((xptrint)Result+i*((tSDLArrayInfo *)Sort)->CompSort->SortSize) =
          *(int *)Comp;
    } else {
      (void)GenericAssignSort(
         Result, Comp, XASS_MR_TMP_NF, ((tSDLArrayInfo *)Sort)->CompSort);
      for (i=1; i<((tSDLArrayInfo *)Sort)->Length; i++) {
        (void)GenericAssignSort(
          (void *)((xptrint)Result+i*((tSDLArrayInfo *)Sort)->CompSort->SortSize),
          Comp, XASS_AC_TMP_NF, ((tSDLArrayInfo *)Sort)->CompSort);
      }
    }
    return Result;
#endif

#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray :
    memset(Result, 0, ((tSDLGArrayInfo *)Sort)->SortSize);
    if (IsPointerToMake(((tSDLGArrayInfo *)Sort)->CompSort)) {
      Tmp = Comp;
      Comp = (void *)&Tmp;
    }
    (void)GenericAssignSort(
      (void *)((xptrint)Result+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
      Comp, XASS_MR_TMP_NF, ((tSDLGArrayInfo *)Sort)->CompSort);
    return Result;
#endif

  default :
    return Result;
  }
}
#endif /* !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR) || !defined(XNOUSE_GARRAY_GENERATOR) */


void GenericDefault(void *Result, tSDLTypeInfo *Sort)
{
  int i;
  while (Sort->TypeClass == type_SDL_Syntype ||
         Sort->TypeClass == type_SDL_Inherits)
    Sort = ((tSDLGenInfo *)Sort)->CompOrFatherSort;

  memset(Result, 0, Sort->SortSize);

  switch (Sort->TypeClass) {
#ifdef XSCT_CMICRO
  case type_SDL_Pid :
    *(SDL_PId *)Result = SDL_NULL;
    break;
#endif

#ifndef XNOUSE_STRUCT
  case type_SDL_Struct :
    for (i=0; i<((tSDLStructInfo *)Sort)->NumOfComponents; i++) {
      if ( (((tSDLStructInfo *)Sort)->Components[i].Offset != (xptrint)~0) &&
	   ((tSDLStructInfo *)Sort)->Components[i].ExtraInfo &&
	   ((tSDLStructInfo *)Sort)->Components[i].ExtraInfo->DefaultValue) {
	(void)GenericAssignSort(
	  (void *)((xptrint)Result+((tSDLStructInfo *)Sort)->Components[i].Offset),
	  ((tSDLStructInfo *)Sort)->Components[i].ExtraInfo->DefaultValue,
	  XASS_MR_ASS_NF,
	  ((tSDLStructInfo *)Sort)->Components[i].CompSort);
      } else if (NEEDSINIT(((tSDLStructInfo *)Sort)->Components[i].CompSort)) {
        GenericDefault(
          (void *)((xptrint)Result+((tSDLStructInfo *)Sort)->Components[i].Offset),
          ((tSDLStructInfo *)Sort)->Components[i].CompSort);
      }
    }
    break;
#endif

#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
  case type_SDL_Union :
  case type_SDL_Choice :
    if (NEEDSINIT(((tSDLChoiceInfo *)Sort)->Components[0].CompSort))
      GenericDefault(
        (void *)((xptrint)Result+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
        ((tSDLChoiceInfo *)Sort)->Components[0].CompSort);
    break;
#endif

#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
  case type_SDL_Array :
  case type_SDL_Carray :
    if (NEEDSINIT(((tSDLArrayInfo *)Sort)->CompSort)) {
      for (i=0; i<((tSDLArrayInfo *)Sort)->Length; i++) {
        GenericDefault(
          (void *)((xptrint)Result+i*((tSDLArrayInfo *)Sort)->CompSort->SortSize),
          ((tSDLArrayInfo *)Sort)->CompSort);
      }
    }
    break;
#endif

  case type_SDL_String :
  case type_SDL_Object_identifier :
  case type_SDL_GPowerset :
  case type_SDL_Bag :
    ((xBag_Type *)Result)->IsAssigned = 1;
    break;

#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray :
    ((xGArray_Type *)Result)->IsAssigned = 1;
    if (NEEDSINIT(((tSDLGArrayInfo *)Sort)->CompSort))
      GenericDefault(
        (void *)((xptrint)Result+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
        ((tSDLGArrayInfo *)Sort)->CompSort);
    break;
#endif

  default :
    break;
  }
}




#ifndef XSCT_CMICRO
#ifndef XNOUSE_ANY
void * GenericAnySort(void *Result, tSDLTypeInfo *Sort)
{
  int i;
  void *TmpItem;

  memset(Result, 0, Sort->SortSize);
  switch (Sort->TypeClass) {
  case type_SDL_Integer :
    *(SDL_Integer *)Result = (SDL_Integer)(GETINTRAND - GETINTRAND_MAX/2);
    break;
  case type_SDL_Real :
#ifndef XNOUSEOFREAL
    *(SDL_Real *)Result = (SDL_Real)(GETINTRAND - GETINTRAND_MAX/2);
#endif
    break;
  case type_SDL_Natural :
    *(SDL_Natural *)Result = (SDL_Natural)(GETINTRAND);
    break;
  case type_SDL_Boolean :
    *(SDL_Boolean *)Result = (SDL_Boolean)(GETINTRAND % 2);
    break;
  case type_SDL_Character :
    *(SDL_Character *)Result = ((SDL_Character)(GETINTRAND%256));
    break;
  case type_SDL_Time :
  case type_SDL_Duration :
#if !defined(XTIMEASINT_TICKS) && !defined(XTIMEASINT_SECS)
    *(SDL_Duration *)Result = SDL_Duration_Lit(GETINTRAND, GETINTRAND%1000000000);
#else
    *(SDL_Duration *)Result = (SDL_Duration)(GETINTRAND);
#endif
    break;
  case type_SDL_Pid :
    *(SDL_PId *)Result = SDL_NULL;
    break;
  case type_SDL_Bit :
    *(SDL_Bit *)Result = (SDL_Bit)(GETINTRAND % 2);
    break;
  case type_SDL_Octet :
#ifndef XNOUSEOFOCTETBITSTRING
    *(SDL_Octet *)Result = (SDL_Octet)(GETINTRAND % 256);
#endif
    break;
  case type_SDL_Bit_string :
#ifndef XNOUSEOFOCTETBITSTRING
    /* length 1 to 10 */
    ((SDL_Bit_String *)Result)->Length = 1+(int)(GETINTRAND%10);
    ((SDL_Bit_String *)Result)->Bits =
      (unsigned char *)XALLOC(((SDL_Bit_String *)Result)->Length, Sort);
    for (i = ((SDL_Bit_String *)Result)->Length-1; i>=0; i--)
      ((SDL_Bit_String *)Result)->Bits[i] = (SDL_Bit)(GETINTRAND % 2);
#endif
    break;
  case type_SDL_Octet_string :
#ifndef XNOUSEOFOCTETBITSTRING
    /* length 2, 4, 6, 8, or 10 */
    ((SDL_Octet_String *)Result)->Length = 2*(1+(int)(GETINTRAND%5));
    ((SDL_Octet_String *)Result)->Bits =
      (unsigned char *)XALLOC(((SDL_Octet_String *)Result)->Length, Sort);
    for (i = ((SDL_Octet_String *)Result)->Length-1; i>=0; i--)
      ((SDL_Octet_String *)Result)->Bits[i] = (SDL_Bit)(GETINTRAND % 256);
#endif
    break;
  case type_SDL_Charstring :
  case type_SDL_IA5String :
  case type_SDL_VisibleString :
    i = 1+(int)(GETINTRAND%10);
    *(SDL_Charstring *)Result = (SDL_Charstring)XALLOC(i+2, Sort);
    (*(SDL_Charstring *)Result)[i+1] = '\0';
    for (; i>0; i--)
      (*(SDL_Charstring *)Result)[i] = ((SDL_Character)(32+GETINTRAND%96));
    (*(SDL_Charstring *)Result)[0] = 'T';
    break;
  case type_SDL_NumericString :
    i = 1+(int)(GETINTRAND%10);
    *(SDL_Charstring *)Result = (SDL_Charstring)XALLOC(i+2, Sort);
    (*(SDL_Charstring *)Result)[i+1] = '\0';
    for (; i>0; i--)
      (*(SDL_Charstring *)Result)[i] = ((SDL_Character)('0'+GETINTRAND%9));
    (*(SDL_Charstring *)Result)[0] = 'T';
    break;
  case type_SDL_PrintableString :
    {
      int r;
      i = 1+(int)(GETINTRAND%10);
      *(SDL_Charstring *)Result = (SDL_Charstring)XALLOC(i+2, Sort);
      (*(SDL_Charstring *)Result)[i+1] = '\0';
      for (; i>0; i--) {
	r = (SDL_Character)(GETINTRAND%62);
	if (r >= 53)
	  r = r-5;  /* convert 53 to '0' */
	else if (r >= 27)
	  r = r+70; /* convert 27 to 'a' */
	else
	  r = r+65; /* convert 0 to 'A' */
	(*(SDL_Charstring *)Result)[i] = (SDL_Character)r;
      }
      (*(SDL_Charstring *)Result)[0] = 'T';
    }
    break;
  case type_SDL_NULL :
    /* 0 is ok */
    break;
  case type_SDL_ShortInt :
    *(short int *)Result = (short int)(GETINTRAND%65536 - 32768);
    break;
  case type_SDL_LongInt :
    *(long int *)Result = (long int)(GETINTRAND - GETINTRAND_MAX/2);
    break;
  case type_SDL_UnsignedShortInt :
    *(unsigned short int *)Result = (unsigned short int)(GETINTRAND%65536);
    break;
  case type_SDL_UnsignedInt :
    *(unsigned int *)Result = (unsigned int)(GETINTRAND);
    break;
  case type_SDL_UnsignedLongInt :
    *(unsigned long int *)Result = (unsigned long int)(GETINTRAND);
    break;
  case type_SDL_Float :
    *(float *)Result = (float)(GETINTRAND - GETINTRAND_MAX/2);
    break;
  case type_SDL_Charstar :
  case type_SDL_Voidstar :
  case type_SDL_Voidstarstar :
    /* 0 is ok */
    break;
  case type_SDL_Syntype :
    (void)GenericAnySort(Result, ((tSDLGenInfo *)Sort)->CompOrFatherSort);
    /* Something BETTER NEEDED */
    break;
  case type_SDL_Inherits :
    (void)GenericAnySort(Result, ((tSDLGenInfo *)Sort)->CompOrFatherSort);
    break;
  case type_SDL_Enum :
    /* NEEDS info on number of elements */
    break;
#ifndef XNOUSE_STRUCT
  case type_SDL_Struct :
    for (i=0; i<((tSDLStructInfo *)Sort)->NumOfComponents; i++) {
      (void)GenericAnySort(
	(void *)((xptrint)Result+((tSDLStructInfo *)Sort)->Components[i].Offset),
	((tSDLStructInfo *)Sort)->Components[i].CompSort);
    }
    break;
#endif
#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
  case type_SDL_Union :
  case type_SDL_Choice :
    i = (int)(GETINTRAND % ((tSDLChoiceInfo *)Sort)->NumOfComponents);
    GenericSetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, Result, i);
    (void)GenericAnySort
      ((void *)((xptrint)Result+((tSDLChoiceInfo *)Sort)->OffsetToUnion),
      ((tSDLChoiceInfo *)Sort)->Components[i].CompSort);
    break;
#endif
  case type_SDL_UnionC :
    i = (int)(GETINTRAND % ((tSDLChoiceInfo *)Sort)->NumOfComponents);
    (void)GenericAnySort(Result, ((tSDLChoiceInfo *)Sort)->Components[i].CompSort);
    break;
#if !defined(XNOUSE_POWERSET_GENERATOR)
  case type_SDL_Powerset :
    for (i=0; i < ((tSDLPowersetInfo *)Sort)->Length; i++)
      if (GETINTRAND%2) 
	((xPowerset_Type *)Result)->A[i/32] =
	  ((unsigned long)1 << i%32) | ((xPowerset_Type *)Result)->A[i/32];
    break;
#endif
#if !defined(XNOUSE_GPOWERSET_GENERATOR) || !defined(XNOUSE_BAG_GENERATOR)
  case type_SDL_GPowerset :
  case type_SDL_Bag :
    TmpItem = XALLOC(((tSDLGenListInfo *)Sort)->CompSort->SortSize,
                     ((tSDLGenListInfo *)Sort)->CompSort);
    for (i=1+(int)(GETINTRAND%5); i>1; i--) {
      (void)GenericAnySort(TmpItem, ((tSDLGenListInfo *)Sort)->CompSort);
      GenBag_Incl2(TmpItem, (xBag_Type *)Result, (tSDLGenListInfo *)Sort);
    }
    XFREE(&TmpItem, ((tSDLGenListInfo *)Sort)->CompSort->SortSize);
    break;
#endif
#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_ASSIGN_OBJECT_IDENTIFIER)
  case type_SDL_String :
  case type_SDL_Object_identifier :
    TmpItem = XALLOC(((tSDLGenListInfo *)Sort)->CompSort->SortSize,
                     ((tSDLGenListInfo *)Sort)->CompSort);
    for (i=1+(int)(GETINTRAND%5); i>1; i--) {
      (void)GenericAnySort(TmpItem, ((tSDLGenListInfo *)Sort)->CompSort);
      GenString_Append((xString_Type *)Result, TmpItem, (tSDLGenListInfo *)Sort);
    }
    XFREE(&TmpItem, ((tSDLGenListInfo *)Sort)->CompSort->SortSize);
    break;
#endif
#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
  case type_SDL_Array :
  case type_SDL_Carray :
    for (i=0; i<((tSDLArrayInfo *)Sort)->Length; i++)
      (void)GenericAnySort(
        (void *)((xptrint)Result+i*((tSDLArrayInfo *)Sort)->CompSort->SortSize),
	((tSDLArrayInfo *)Sort)->CompSort);
    break;
#endif
#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray :
    (void)GenericAnySort(
      (void *)((xptrint)Result+((tSDLGArrayInfo *)Sort)->arrayDataOffset),
      ((tSDLGArrayInfo *)Sort)->CompSort);
    break;
#endif
  case type_SDL_Own :
  case type_SDL_Oref :
  case type_SDL_Ref :
    /* 0 is ok */
    break;
  case type_SDL_Userdef :
  case type_SDL_EmptyType :
    /* 0 has to do it! */
    break;
  default :
    break;
  }
  return Result;
}
#endif /* XNOUSE_ANY */
#endif /* !XSCT_CMICRO */



#ifdef XEINDEX
int GenericTstIArray(int Index, tSDLArrayInfo *Sort)
{
  if (Index < Sort->LowestValue || Index >= Sort->LowestValue+Sort->Length) {
#ifdef XSCT_CMICRO
    XINDEX_ERROR;
#else
    if (Sort->IndexSort->SortSize == sizeof(char)) {
      unsigned char v1 = (unsigned char)Index;
      xErrorIndex(Sort->IndexSort, (void *)&v1);
    }
    else if (Sort->IndexSort->SortSize == sizeof(short)) {
      unsigned short v2 = (unsigned short)Index;
      xErrorIndex(Sort->IndexSort, (void *)&v2);
    }
    else
      xErrorIndex((tSDLTypeInfo *)Sort->IndexSort, (void *)&Index);
#endif
    return Sort->LowestValue;
  }
  return Index;
}
#endif



#ifndef XNOUSE_POWERSET_GENERATOR
/* ****************************** Powerset ****************************** */

/* GenPow_Empty defined using macro
#define GenPow_Empty(SDLInfo,Result)  memset((void *)Result,0,SDLInfo->SortSize)
*/


SDL_Boolean GenPow_In (int Item, xPowerset_Type *PS, tSDLPowersetInfo *Sort)
{
  Item = Item-Sort->LowestValue;
  if (Item >= 0 && Item < Sort->Length)
    if (((unsigned long)1 << Item%32) & PS->A[Item/32])
      return SDL_True;
  return SDL_False;

}


void * GenPow_Incl (int Item, xPowerset_Type *PS, tSDLPowersetInfo *Sort, xPowerset_Type *Result)
{
  memcpy(Result, PS, Sort->SortSize);
  Item = Item-Sort->LowestValue;
  if (Item >= 0 && Item < Sort->Length)
    Result->A[Item/32] = ((unsigned long)1 << Item%32) | Result->A[Item/32];
  return Result;
}


void * GenPow_Del (int Item, xPowerset_Type *PS, tSDLPowersetInfo *Sort, xPowerset_Type *Result)
{
  memcpy(Result, PS, Sort->SortSize);
  Item = Item-Sort->LowestValue;
  if (Item >= 0 && Item < Sort->Length)
    Result->A[Item/32] = ~((unsigned long)1 << Item%32) & Result->A[Item/32];
  return Result;
}


void GenPow_Incl2 (int Item, xPowerset_Type *PS, tSDLPowersetInfo *Sort)
{
  Item = Item-Sort->LowestValue;
  if (Item >= 0 && Item < Sort->Length)
    PS->A[Item/32] = ((unsigned long)1 << Item%32) | PS->A[Item/32];
}


void GenPow_Del2 (int Item, xPowerset_Type *PS, tSDLPowersetInfo *Sort)
{
  Item = Item-Sort->LowestValue;
  if (Item >= 0 && Item < Sort->Length)
    PS->A[Item/32] = ~((unsigned long)1 << Item%32) & PS->A[Item/32];
}


SDL_Boolean GenPow_LT (xPowerset_Type *PS1, xPowerset_Type *PS2, tSDLPowersetInfo *Sort)
{
  int Item;
  for (Item=0; Item<=(Sort->Length-1)/32; Item++)
    if ((PS2->A[Item] | PS1->A[Item]) ^ PS2->A[Item])
      return SDL_False;
  for (Item=0; Item<=(Sort->Length-1)/32; Item++)
    if (PS1->A[Item] ^ PS2->A[Item])
      return SDL_True;
  return SDL_False;
}


SDL_Boolean GenPow_LE (xPowerset_Type *PS1, xPowerset_Type *PS2, tSDLPowersetInfo *Sort)
{
  int Item;
  for (Item=0; Item<=(Sort->Length-1)/32; Item++)
    if ((PS2->A[Item] | PS1->A[Item]) ^ PS2->A[Item])
      return SDL_False;
  return SDL_True;
}


void * GenPow_And (xPowerset_Type *PS1, xPowerset_Type *PS2, tSDLPowersetInfo *Sort, xPowerset_Type *Result)
{
  int Item;
  for (Item=0; Item<=(Sort->Length-1)/32; Item++)
    Result->A[Item] = PS1->A[Item] & PS2->A[Item];
  return Result;
}


void * GenPow_Or (xPowerset_Type *PS1, xPowerset_Type *PS2, tSDLPowersetInfo *Sort, xPowerset_Type *Result)
{
  int Item;
  for (Item=0; Item<=(Sort->Length-1)/32; Item++)
    Result->A[Item] = PS1->A[Item] | PS2->A[Item];
  return Result;
}


SDL_Integer GenPow_Length (xPowerset_Type *PS, tSDLPowersetInfo *Sort)
{
  int Item, B;
  int NoOfComp = 0;
  for (Item=0; Item<=(Sort->Length-1)/32; Item++)
    for (B=0; B<32; B++)
      if (((unsigned long)1 << B) & PS->A[Item])
	NoOfComp++;
  return NoOfComp;
}


int GenPow_Take (xPowerset_Type *PS, tSDLPowersetInfo *Sort)
{
  int Item, B;
  for (Item=0; Item<=(Sort->Length-1)/32; Item++)
    for (B=0; B<32; B++)
      if (((unsigned long)1 << B) & PS->A[Item])
	return (int)Sort->LowestValue + Item*32+B;
#ifdef XECSOP
  xSDLOpError("Operator Take", "Empty powerset");
#endif
  return 0;
}


int GenPow_Take2 (xPowerset_Type *PS, SDL_Integer Index, tSDLPowersetInfo *Sort)
{
  int Item, B;
  if (Index > 0)
    for (Item=0; Item<=(Sort->Length-1)/32; Item++)
      for (B=0; B<32; B++)
        if (((unsigned long)1 << B) & PS->A[Item])
          if (--Index <= 0)
            return (int)Sort->LowestValue + Item*32+B;
#ifdef XECSOP
  xSDLOpError("Operator Take", "Index out of range");
#endif
  return 0;
}
#endif /* XNOUSE_POWERSET_GENERATOR */



#if !defined(XNOUSE_BAG_GENERATOR) || !defined(XNOUSE_GPOWERSET_GENERATOR)
/* *************************** Bag, GPowerset *************************** */

static xBag_yptr GenBag_InInner(void *Item, xBag_Type *PS, tSDLGenListInfo *Sort)
{
  xBag_yptr Result;
  void *TmpItem;
  
  if (GenericIsAssigned(Item, Sort->CompSort)) {
    for (Result = PS->First;
	 (Result != 0) && ! GenericEqualSort(
	   Item, (void *)((xptrint)Result+Sort->yrecDataOffset), Sort->CompSort);
	 Result = Result->Suc) ;
  } else {
    TmpItem = XALLOC(Sort->CompSort->SortSize, Sort->CompSort);
    (void)GenericAssignSort(TmpItem, Item, XASS_MR_ASS_NF, Sort->CompSort);
    for (Result = PS->First;
	 (Result != 0) && ! GenericEqualSort(
	   TmpItem, (void *)((xptrint)Result+Sort->yrecDataOffset), Sort->CompSort);
	 Result = Result->Suc) ;
    GenericFreeSort((void **)TmpItem, Sort->CompSort);
    XFREE(&TmpItem, Sort->CompSort->SortSize);
  }
  return Result;
}


/* GenBag_Empty is macro
#define GenBag_Empty(SDLInfo,Result)  memset((void *)Result,0,SDLInfo->SortSize)
*/


void * GenBag_Makebag (void *Item, tSDLGenListInfo *Sort, xBag_Type *Result)
{
  Result->First = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
  Result->Last = Result->First;
  (void)GenericAssignSort(
    (void *)((xptrint)Result->First+Sort->yrecDataOffset),
    Item, XASS_MR_ASS_NF, Sort->CompSort);
  if (Sort->TypeClass==type_SDL_Bag)
    Result->First->NoOfItems = 1;
  Result->First->Suc = 0;
  Result->Length = 1;
  Result->IsAssigned = 0;
  return (void *)Result;
}


SDL_Boolean GenBag_In (void *Item, xBag_Type *PS, tSDLGenListInfo *Sort)
{
  SDL_Boolean Result;
  Result = GenBag_InInner(Item, PS, Sort) != 0;
  if (! PS->IsAssigned)
    GenericFreeSort((void **)PS, (tSDLTypeInfo *)Sort);
  return Result;
}


void * GenBag_Incl (void *Item, xBag_Type *PS, tSDLGenListInfo *Sort, xBag_Type *Result)
{
  void *TmpItem;
  xBag_yptr P;
  (void)GenericAssignSort((void *)Result, (void *)PS, XASS_MR_TMP_NF,
			  (tSDLTypeInfo *)Sort);
  TmpItem = XALLOC(Sort->CompSort->SortSize, Sort->CompSort);
  (void)GenericAssignSort(TmpItem, Item, XASS_MR_ASS_NF, Sort->CompSort);
  P = GenBag_InInner(TmpItem, Result, Sort);
  if (P == 0) {
    if (Result->Last == 0) {
      Result->Last = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
      Result->First = Result->Last;
    } else {
      Result->Last->Suc = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
      Result->Last = Result->Last->Suc;
    }
    (void)GenericAssignSort(
      (void *)((xptrint)Result->Last+Sort->yrecDataOffset),
      TmpItem, XASS_AR_ASS_NF, Sort->CompSort);
    Result->Last->Suc = 0;
    if (Sort->TypeClass==type_SDL_Bag)
      Result->Last->NoOfItems = 1;
    Result->Length++;
  } else {
    GenericFreeSort((void **)TmpItem, Sort->CompSort);
    if (Sort->TypeClass==type_SDL_Bag) {
      P->NoOfItems++;
      Result->Length++;
    }
  }
  XFREE(&TmpItem, Sort->CompSort->SortSize);
  return (void *)Result; 
}


void * GenBag_Del (void *Item, xBag_Type *PS, tSDLGenListInfo *Sort, xBag_Type *Result)
{
  void *TmpItem;
  xBag_yptr P, Prev;
  (void)GenericAssignSort((void *)Result, (void *)PS, XASS_MR_TMP_NF,
			  (tSDLTypeInfo *)Sort);
  TmpItem = XALLOC(Sort->CompSort->SortSize, Sort->CompSort);
  (void)GenericAssignSort(TmpItem, Item, XASS_MR_ASS_NF, Sort->CompSort);
  P = GenBag_InInner(TmpItem, Result, Sort);
  GenericFreeSort((void **)TmpItem, Sort->CompSort);
  XFREE(&TmpItem, Sort->CompSort->SortSize);
  if (P == 0) return (void *)Result;
  Result->Length--;
  if (Sort->TypeClass==type_SDL_Bag)
    if (P->NoOfItems > 1) {
      P->NoOfItems--;
      return (void *)Result;
    }
  if (P == Result->First) {
    Result->First = Result->First->Suc;
    if (P == Result->Last) Result->Last = 0;
  } else {
    Prev = Result->First;
    while (Prev->Suc != P) Prev = Prev->Suc;
    Prev->Suc = P->Suc;
    if (P == Result->Last) Result->Last = Prev;
  }
  GenericFreeSort((void **)((xptrint)P+Sort->yrecDataOffset), Sort->CompSort);
  XFREE((void **)&P, Sort->yrecSize);
  return (void *)Result;
}


void GenBag_Incl2 (void *Item, xBag_Type *PS, tSDLGenListInfo *Sort)
{
  void *TmpItem;
  xBag_yptr P;
  TmpItem = XALLOC(Sort->CompSort->SortSize, Sort->CompSort);
  (void)GenericAssignSort(TmpItem, Item, XASS_MR_ASS_NF, Sort->CompSort);
  P = GenBag_InInner(TmpItem, PS, Sort);
  if (P == 0) {
    if (PS->Last == 0) {
      PS->Last = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
      PS->First = PS->Last;
    } else {
      PS->Last->Suc = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
      PS->Last = PS->Last->Suc;
    }
    (void)GenericAssignSort(
      (void **)((xptrint)PS->Last+Sort->yrecDataOffset),
      TmpItem, XASS_AR_ASS_NF, Sort->CompSort);
    PS->Last->Suc = 0;
    if (Sort->TypeClass==type_SDL_Bag)
      PS->Last->NoOfItems = 1;
    PS->Length++;
  } else {
    GenericFreeSort((void **)TmpItem, Sort->CompSort);
    if (Sort->TypeClass==type_SDL_Bag) {
      P->NoOfItems++;
      PS->Length++;
    }
  }
  XFREE(&TmpItem, Sort->CompSort->SortSize);
}


void GenBag_Del2 (void *Item, xBag_Type *PS, tSDLGenListInfo *Sort)
{
  void *TmpItem;
  xBag_yptr P, Prev;
  TmpItem = XALLOC(Sort->CompSort->SortSize, Sort->CompSort);
  (void)GenericAssignSort(TmpItem, Item, XASS_MR_ASS_NF, Sort->CompSort);
  P = GenBag_InInner(TmpItem, PS, Sort);
  GenericFreeSort((void **)TmpItem, Sort->CompSort);
  XFREE(&TmpItem, Sort->CompSort->SortSize);
  if (P == 0) return;
  PS->Length--;
  if (Sort->TypeClass==type_SDL_Bag)
    if (P->NoOfItems > 1) {
      P->NoOfItems--;
      return;
    }
  if (P == PS->First) {
    PS->First = PS->First->Suc;
    if (P == PS->Last) PS->Last = 0;
  } else {
    Prev = PS->First;
    while (Prev->Suc != P) Prev = Prev->Suc;
    Prev->Suc = P->Suc;
    if (P == PS->Last) PS->Last = Prev;
  }
  GenericFreeSort((void **)((xptrint)P+Sort->yrecDataOffset), Sort->CompSort);
  XFREE((void **)&P, Sort->yrecSize);
}


SDL_Boolean GenBag_LT (xBag_Type *PS1, xBag_Type *PS2, tSDLGenListInfo *Sort)
{
  xBag_yptr Temp, Temp2;
  SDL_Boolean Result;
  Result = PS1->Length < PS2->Length;
  for (Temp=PS1->First; Temp!=0 && Result; Temp=Temp->Suc) {
    Temp2 = GenBag_InInner((void *)((xptrint)Temp+Sort->yrecDataOffset), PS2, Sort);
    if (Temp2 == 0)
      Result = SDL_False;
    else if (Sort->TypeClass==type_SDL_Bag) {
      if (Temp->NoOfItems > Temp2->NoOfItems)
	Result = SDL_False;
    }
  }
  if (! PS1->IsAssigned)
    GenericFreeSort((void **)PS1, (tSDLTypeInfo *)Sort);
  if (! PS2->IsAssigned)
    GenericFreeSort((void **)PS2, (tSDLTypeInfo *)Sort);
  return Result;
}


SDL_Boolean GenBag_LE (xBag_Type *PS1, xBag_Type *PS2, tSDLGenListInfo *Sort)
{
  xBag_yptr Temp, Temp2;
  SDL_Boolean Result;
  Result = PS1->Length <= PS2->Length;
  for (Temp=PS1->First; Temp!=0 && Result; Temp=Temp->Suc) {
    Temp2 = GenBag_InInner((void *)((xptrint)Temp+Sort->yrecDataOffset), PS2, Sort);
    if (Temp2 == 0)
      Result = SDL_False;
    else if (Sort->TypeClass==type_SDL_Bag) {
      if (Temp->NoOfItems > Temp2->NoOfItems)
	Result = SDL_False;
    }
  }
  if (! PS1->IsAssigned)
    GenericFreeSort((void **)PS1, (tSDLTypeInfo *)Sort);
  if (! PS2->IsAssigned)
    GenericFreeSort((void **)PS2, (tSDLTypeInfo *)Sort);
  return Result;
}


void * GenBag_And (xBag_Type *PS1, xBag_Type *PS2, tSDLGenListInfo *Sort, xBag_Type *Result)
{
  xBag_yptr Temp, Temp2;
  memset((void *)Result,0,Sort->SortSize);
  for (Temp=PS1->First; Temp!=0; Temp=Temp->Suc) {
    Temp2 = GenBag_InInner((void *)((xptrint)Temp+Sort->yrecDataOffset), PS2, Sort);
    if (Temp2 != 0) {
      if (Result->First == 0) {
        Result->First = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
        Result->Last = Result->First;
      } else {
        Result->Last->Suc = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
        Result->Last = Result->Last->Suc;
      }
      (void)GenericAssignSort(
	(void *)((xptrint)Result->Last+Sort->yrecDataOffset),
	(void *)((xptrint)Temp+Sort->yrecDataOffset),
	XASS_MR_ASS_NF, Sort->CompSort);
      if (Sort->TypeClass==type_SDL_Bag) {
	Result->Last->NoOfItems = 
	  (Temp->NoOfItems>Temp2->NoOfItems ? Temp2->NoOfItems : Temp->NoOfItems);
	Result->Length = Result->Length+Result->Last->NoOfItems;
      } else {
	Result->Length++;
      }
      Result->Last->Suc = 0;
    }
  }
  if (! PS1->IsAssigned)
    GenericFreeSort((void **)PS1, (tSDLTypeInfo *)Sort);
  if (! PS2->IsAssigned)
    GenericFreeSort((void **)PS2, (tSDLTypeInfo *)Sort);
  return (void *)Result; 
}


void * GenBag_Or (xBag_Type *PS1, xBag_Type *PS2, tSDLGenListInfo *Sort, xBag_Type *Result)
{
  xBag_yptr Temp, Temp2;
  (void)GenericAssignSort((void *)Result, (void *)PS2, XASS_MR_TMP_NF,
			  (tSDLTypeInfo *)Sort);
  for (Temp=PS1->First; Temp!=0; Temp=Temp->Suc) {
    Temp2 = GenBag_InInner((void *)((xptrint)Temp+Sort->yrecDataOffset), Result, Sort);
    if (Temp2 == 0) {
      if (Result->First == 0) {
        Result->First = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
        Result->Last = Result->First;
      } else {
        Result->Last->Suc = (xBag_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
        Result->Last = Result->Last->Suc;
      }
      (void)GenericAssignSort(
	(void *)((xptrint)Result->Last+Sort->yrecDataOffset),
	(void *)((xptrint)Temp+Sort->yrecDataOffset),
	XASS_MR_ASS_NF, Sort->CompSort);
      if (Sort->TypeClass==type_SDL_Bag) {
	Result->Last->NoOfItems = Temp->NoOfItems;
	Result->Length = Result->Length+Temp->NoOfItems;
      } else {
	Result->Length++;
      }
      Result->Last->Suc = 0;
    } else if (Sort->TypeClass==type_SDL_Bag) {
      Result->Length = Result->Length+Temp->NoOfItems; \
      Temp2->NoOfItems = Temp2->NoOfItems+Temp->NoOfItems; \
    }
  }
  if (! PS1->IsAssigned)
    GenericFreeSort((void **)PS1, (tSDLTypeInfo *)Sort);
  return (void *)Result; 
}


SDL_Integer GenBag_Length (xBag_Type *PS, tSDLGenListInfo *Sort)
{
  int Result;
  Result = PS->Length;
  if (! PS->IsAssigned)
    GenericFreeSort((void **)PS, (tSDLTypeInfo *)Sort);
  return Result;
}


void * GenBag_Take (xBag_Type *PS, tSDLGenListInfo *Sort, void *Result)
{
  if (PS->First == 0) {
#ifdef XECSOP
    xSDLOpError("Operator Take", "Empty Bag");
#endif
    memset(Result, 0, Sort->CompSort->SortSize);
  } else if (PS->IsAssigned) {
    Result = (void *)((xptrint)PS->First+Sort->yrecDataOffset);
  } else {
    (void)GenericAssignSort(Result,
                            (void *)((xptrint)PS->First+Sort->yrecDataOffset),
                            XASS_AC_TMP_NF, Sort->CompSort);
  }
  if (! PS->IsAssigned)
    GenericFreeSort((void **)PS, (tSDLTypeInfo *)Sort);
  return Result;
}


void * GenBag_Take2 (xBag_Type *PS, SDL_Integer Index, tSDLGenListInfo *Sort, void *Result)
{
  xBag_yptr Tmp;
  int Number = 0;
  for (Tmp=PS->First; Tmp!=0; Tmp=Tmp->Suc) {
    Number = Number + (Sort->TypeClass==type_SDL_Bag ? Tmp->NoOfItems : 1);
    if (Number >= Index) break;
  }
  if (Tmp == 0 || Index <= 0) {
#ifdef XECSOP
    xSDLOpError("Operator Take", "Index out of range");
#endif
    memset(Result, 0, Sort->CompSort->SortSize);
  } else if (PS->IsAssigned) {
    Result = (void *)((xptrint)Tmp+Sort->yrecDataOffset);
  } else {
    (void)GenericAssignSort(Result,
                            (void *)((xptrint)Tmp+Sort->yrecDataOffset),
                            XASS_AC_TMP_NF, Sort->CompSort);
  }
  if (! PS->IsAssigned)
    GenericFreeSort((void **)PS, (tSDLTypeInfo *)Sort);
  return Result;
}

#endif /* !defined(XNOUSE_BAG_GENERATOR) || !defined(XNOUSE_GPOWERSET_GENERATOR) */




#ifndef XNOUSE_GARRAY_GENERATOR
/* ******************************* GArray ******************************* */

void * GenGArray_Extract (
  xGArray_Type   *GA,
  void           *Index,
  tSDLGArrayInfo *Sort)
{
  xGArray_yptr yPtr;
  void * Result;
  void * TmpIndex;
  Result = 0;
  yPtr = GA->First;
  if (GenericIsAssigned(Index, Sort->IndexSort)) {
    while (yPtr && !Result) {
      if (GenericEqualSort(
	   (void *)((xptrint)yPtr+((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
	   Index,
	   Sort->IndexSort))
	Result = (void *)((xptrint)yPtr+((tSDLGArrayInfo *)Sort)->yrecDataOffset);
      yPtr = yPtr->Suc;
    }
  } else {
    TmpIndex = XALLOC(Sort->IndexSort->SortSize, Sort->IndexSort);
    (void)GenericAssignSort(TmpIndex, Index, XASS_MR_ASS_NF, Sort->IndexSort);
    while (yPtr && !Result) {
      if (GenericEqualSort(
	   (void *)((xptrint)yPtr+((tSDLGArrayInfo *)Sort)->yrecIndexOffset),
	   TmpIndex,
	   Sort->IndexSort))
	Result = (void *)((xptrint)yPtr+((tSDLGArrayInfo *)Sort)->yrecDataOffset);
      yPtr = yPtr->Suc;
    }
    GenericFreeSort((void **)TmpIndex, Sort->IndexSort);
    XFREE(&TmpIndex, Sort->IndexSort->SortSize);
  }
  if (!Result)
    Result = (void *)((xptrint)GA+((tSDLGArrayInfo *)Sort)->arrayDataOffset);
  return Result;
}


void * GenGArray_Modify (
  xGArray_Type   *GA,
  void           *Index,
  tSDLGArrayInfo *Sort)
{
  void * Result;
  void * TmpIndex;
  TmpIndex = XALLOC(Sort->IndexSort->SortSize, Sort->IndexSort);
  (void)GenericAssignSort(TmpIndex, Index, XASS_MR_ASS_NF, Sort->IndexSort);
  Result = GenGArray_Extract(GA, Index, Sort);
  if (Result == (void *)((xptrint)GA+Sort->arrayDataOffset)) {
    if (GA->Last) {
      GA->Last->Suc = (xGArray_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
      GA->Last = GA->Last->Suc;
    } else {
      GA->First = (xGArray_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
      GA->Last = GA->First;
    }
    GA->Last->Suc = 0;
    (void)GenericAssignSort(
      (void *)((xptrint)GA->Last+Sort->yrecIndexOffset), 
      TmpIndex, 
      XASS_MR_ASS_NF,
      Sort->IndexSort);
    (void)GenericAssignSort(
      (void *)((xptrint)GA->Last+Sort->yrecDataOffset), 
      (void *)((xptrint)GA+Sort->arrayDataOffset), 
      XASS_MR_ASS_NF,
      Sort->CompSort);
    Result = (void *)((xptrint)GA->Last+Sort->yrecDataOffset);
  }
  GenericFreeSort((void **)TmpIndex, Sort->IndexSort);
  XFREE(&TmpIndex, Sort->IndexSort->SortSize);
  return Result;
}

#endif /* XNOUSE_GARRAY_GENERATOR */




/* ******************************* String ******************************* */

/* GenString_Emptystring defined as macro
#define GenString_Emptystring(SDLInfo,Result) memset((void *)Result,0,SDLInfo->SortSize)
*/


#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_MKSTRING_OBJECT_IDENTIFIER)
void * GenString_MkString (void *Item, tSDLGenListInfo *Sort, xString_Type *Result)
{
  Result->First = (xString_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
  Result->Last = Result->First;
  Result->Length = 1;
  Result->IsAssigned = 0;
  Result->First->Suc = 0;
  (void)GenericAssignSort(
    (void *)((xptrint)Result->First + Sort->yrecDataOffset),
    Item, XASS_MR_ASS_NF, Sort->CompSort);
  return (void *)Result;
}
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_LENGTH_OBJECT_IDENTIFIER)
SDL_Integer GenString_Length (xString_Type *ST, tSDLGenListInfo *Sort)
{
  SDL_Integer Result;
  Result = ST->Length;
  if (! ST->IsAssigned)
    GenericFreeSort((void **)ST, (tSDLTypeInfo *)Sort);
  return Result;
}
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_FIRST_OBJECT_IDENTIFIER)
void * GenString_First (xString_Type *ST, tSDLGenListInfo *Sort, void *Result)
{
  if (ST->Length <= 0) {
#ifdef XECSOP
    xSDLOpError("Operator First", "Length is zero");
#endif
    memset(Result, 0, Sort->CompSort->SortSize);
  } else if (ST->IsAssigned) {
    Result = (void *)((xptrint)ST->First + Sort->yrecDataOffset);
  } else {
    (void)GenericAssignSort(Result,
                            (void *)((xptrint)ST->First + Sort->yrecDataOffset),
                            XASS_AC_TMP_NF, Sort->CompSort);
  }
  if (! ST->IsAssigned)
    GenericFreeSort((void **)ST, (tSDLTypeInfo *)Sort);
  return Result;
}
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_LAST_OBJECT_IDENTIFIER)
void * GenString_Last (xString_Type *ST, tSDLGenListInfo *Sort, void *Result)
{
  if (ST->Length <= 0) {
#ifdef XECSOP
    xSDLOpError("Operator Last", "Length is zero");
#endif
    memset(Result, 0, Sort->CompSort->SortSize);
  } else if (ST->IsAssigned) {
    Result = (void *)((xptrint)ST->Last + Sort->yrecDataOffset);
  } else {
    (void)GenericAssignSort(Result,
                            (void *)((xptrint)ST->Last + Sort->yrecDataOffset),
                            XASS_AC_TMP_NF, Sort->CompSort);
  }
  if (! ST->IsAssigned)
    GenericFreeSort((void **)ST, (tSDLTypeInfo *)Sort);
  return Result;
}
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_CONCAT_OBJECT_IDENTIFIER)
void * GenString_Concat (xString_Type *ST1, xString_Type *ST2, tSDLGenListInfo *Sort, xString_Type *Result)
{
  xString_yptr ResultPtr, ST2Ptr;
  if (ST1->First == 0) {
    (void)GenericAssignSort((void *)Result, (void *)ST2, XASS_MR_TMP_NF,
			    (tSDLTypeInfo *)Sort);
    return (void *)Result;
  }
  (void)GenericAssignSort((void *)Result, (void *)ST1, XASS_MR_TMP_NF,
			  (tSDLTypeInfo *)Sort);
  if (ST2->First == 0)
    return (void *)Result;
  Result->Length = Result->Length + ST2->Length;
  if (ST2->IsAssigned) {
    ST2Ptr = ST2->First;
    ResultPtr = Result->Last;
    while (ST2Ptr) {
      ResultPtr->Suc = (xString_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
      ResultPtr = ResultPtr->Suc;
      (void)GenericAssignSort(
	(void *)((xptrint)ResultPtr+Sort->yrecDataOffset), 
	(void *)((xptrint)ST2Ptr+Sort->yrecDataOffset), 
	XASS_MR_ASS_NF,
	Sort->CompSort);
      ST2Ptr = ST2Ptr->Suc;
    }
    ResultPtr->Suc = 0;
    Result->Last = ResultPtr;
  } else {
    Result->Last->Suc = ST2->First;
    Result->Last = ST2->Last;
  }
  return (void *)Result;
}
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_SUBSTRING_OBJECT_IDENTIFIER)
void * GenString_SubString (
  xString_Type    *ST,
  SDL_Integer      Start,
  SDL_Integer      SubLength,
  tSDLGenListInfo *Sort,
  xString_Type    *Result)
{
  xString_yptr ResultPtr, STPtr;
  int i;

  memset((void *)Result, 0, Sort->SortSize);
  if (ST->Length <= 0) {
#ifdef XECSOP
    xSDLOpError("Operator SubString", "Length is zero");
#endif
  } else if (Start <= 0) {
#ifdef XECSOP
    xSDLOpError("Operator SubString", "Start is less than or equal to zero")
#endif
    ;
  } else if (SubLength < 0) {
#ifdef XECSOP
    xSDLOpError("Operator SubString", "SubLength is less than zero")
#endif
    ;
  } else if (Start+SubLength-1 > ST->Length) {
#ifdef XECSOP
    xSDLOpError("Operator SubString",
		"Start+Substring-1 length is greater than string length")
#endif
    ;
  } else if (SubLength > 0) {
    STPtr = ST->First;
    for (i=1; i<Start; i++) STPtr = STPtr->Suc;
    ResultPtr = (xString_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
    Result->First = ResultPtr;
    (void)GenericAssignSort(
      (void *)((xptrint)ResultPtr+Sort->yrecDataOffset), 
      (void *)((xptrint)STPtr+Sort->yrecDataOffset), 
      XASS_MR_ASS_NF,
      Sort->CompSort);
    for (i=2; i<=SubLength; i++) {
      STPtr = STPtr->Suc;
      ResultPtr->Suc = (xString_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
      ResultPtr = ResultPtr->Suc;
      (void)GenericAssignSort(
	(void *)((xptrint)ResultPtr+Sort->yrecDataOffset), 
	(void *)((xptrint)STPtr+Sort->yrecDataOffset), 
	XASS_MR_ASS_NF,
	Sort->CompSort);
    }
    ResultPtr->Suc = 0;
    Result->Last = ResultPtr;
    Result->Length = SubLength;
  }
  if (! ST->IsAssigned)
    GenericFreeSort((void **)ST, (tSDLTypeInfo *)Sort);
  return (void *)Result;
}
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_ASSIGN_OBJECT_IDENTIFIER) || ( defined(XREADANDWRITEF) && !defined(XNOUSEOFOBJECTIDENTIFIER) )
void GenString_Append (xString_Type *ST, void *Item, tSDLGenListInfo *Sort)
{
  xString_yptr STPtr;
  STPtr = (xString_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
  if (ST->First == 0)
    ST->First = STPtr;
  else
    ST->Last->Suc = STPtr;
  ST->Last = STPtr;
  (void)GenericAssignSort(
    (void *)((xptrint)STPtr+Sort->yrecDataOffset), 
    Item, XASS_MR_ASS_NF, Sort->CompSort);
  ST->Length++;
}
#endif

#if !defined(XNOUSE_STRING_GENERATOR) || !defined(XNOUSE_EXTRACT_OBJECT_IDENTIFIER)  || !defined(XNOUSE_MODIFY_OBJECT_IDENTIFIER)
void * GenString_Extract (
  xString_Type    *ST,
  SDL_Integer      Index,
  tSDLGenListInfo *Sort)
{
  /* Used also for string modify */
  xString_yptr STPtr;
  if (ST->Length == 0) {
#ifdef XECSOP
    xSDLOpError("Extract/Modify of string sort",
		"String is empty. One component added, Index set to 1");
#endif
    ST->First = (xString_yptr)XALLOC(Sort->yrecSize, (tSDLTypeInfo *)Sort);
    ST->Last = ST->First;
    ST->Length = 1;
    ST->First->Suc = 0;
    /* Component value 0 from XALLOC */
    Index = 1;
  } else if (Index > ST->Length) {
#ifdef XECSOP
    xSDLOpError("Extract/Modify of string sort",
		"Index > string length. Index set to 1");
#endif
    Index = 1;
  } else if (Index <= (SDL_Integer)0) {
#ifdef XECSOP
    xSDLOpError("Extract/Modify of string sort",
		"Index <= 0. Index set to 1");
#endif
    Index = 1;
  }
  STPtr = ST->First;
  for (; Index>1; Index--) STPtr = STPtr->Suc;
  return (void *)((xptrint)STPtr+Sort->yrecDataOffset);
}
#endif


#ifndef XNOUSE_LSTRING_GENERATOR
/* ****************************** LString ******************************* */

/* GenLString_Emptystring defined as macro
#define GenLString_Emptystring(SDLInfo,Result) memset((void *)Result,0,SDLInfo->SortSize)
*/


void * GenLString_MkString (void *Item, tSDLLStringInfo *Sort, xLString_Type *Result)
{
  memset((void *)Result, 0, Sort->SortSize);
  Result->Length = 1;
  (void)GenericAssignSort((void *)((xptrint)Result + Sort->DataOffset),
			  Item, XASS_MR_TMP_NF, Sort->CompSort);
  return (void *)Result;
}


/* GenLString_Length defined as macro
#define GenLString_Length(ST,SDLInfo) (ST)->Length
*/


void * GenLString_First (xLString_Type *ST, tSDLLStringInfo *Sort, void *Result)
{
#ifdef XECSOP
  if (ST->Length == 0) {
    xSDLOpError("Operator First", "Length is zero");
  }
#endif
  (void)GenericAssignSort(Result, (void *)((xptrint)ST + Sort->DataOffset),
			  XASS_MR_TMP_NF, Sort->CompSort);
  return Result;
}
#endif

#if !defined(XNOUSE_LSTRING_GENERATOR)
void * GenLString_Last (xLString_Type *ST, tSDLLStringInfo *Sort, void *Result)
{
  int i;
  if (ST->Length == 0) {
#ifdef XECSOP
    xSDLOpError("Operator Last", "Length is zero");
#endif
    i = 0;
  } else
    i = ST->Length - 1;
  (void)GenericAssignSort(
     Result,
     (void *)((xptrint)ST + Sort->DataOffset + i*Sort->CompSort->SortSize),
     XASS_MR_TMP_NF, Sort->CompSort);
  return Result;
}


void * GenLString_Concat (xLString_Type *ST1, xLString_Type *ST2, tSDLLStringInfo *Sort, xLString_Type *Result)
{
  int i, k;
  memset((void *)Result, 0, Sort->SortSize);
  if (ST1->Length+ST2->Length > Sort->MaxLength) {
    k = Sort->MaxLength - ST1->Length;
#ifdef XECSOP
    xSDLOpError("Operator Concat in String",
		"Result length > size. Result stripped");
#endif
  } else
    k = ST2->Length;
  for (i=0; i<ST1->Length; i++) 
    (void)GenericAssignSort(
      (void *)((xptrint)Result+Sort->DataOffset+i*Sort->CompSort->SortSize), 
      (void *)((xptrint)ST1+Sort->DataOffset+i*Sort->CompSort->SortSize),
      XASS_MR_TMP_NF, Sort->CompSort);
  for (i=0; i<k; i++) 
    (void)GenericAssignSort(
      (void *)((xptrint)Result+Sort->DataOffset+(i+ST1->Length)*Sort->CompSort->SortSize), 
      (void *)((xptrint)ST2+Sort->DataOffset+i*Sort->CompSort->SortSize),
      XASS_MR_TMP_NF, Sort->CompSort);
  Result->Length = ST1->Length+k;
  return (void *)Result;
}


void * GenLString_SubString (
  xLString_Type   *ST,
  SDL_Integer      Start,
  SDL_Integer      SubLength,
  tSDLLStringInfo *Sort,
  xLString_Type   *Result)
{
  int i;

  memset((void *)Result, 0, Sort->SortSize);
  if (ST->Length <= 0) {
#ifdef XECSOP
    xSDLOpError("Operator SubString", "Length is zero");
#endif
  } else if (Start <= 0) {
#ifdef XECSOP
    xSDLOpError("Operator SubString", "Start is less than or equal to zero")
#endif
    ;
  } else if (SubLength < 0) {
#ifdef XECSOP
    xSDLOpError("Operator SubString", "SubLength is less than zero")
#endif
    ;
  } else if (Start+SubLength-1 > ST->Length) {
#ifdef XECSOP
    xSDLOpError("Operator SubString",
		"Start+Substring-1 length is greater than string length")
#endif
    ;
  } else if (SubLength > 0) {
    for (i=0; i<SubLength; i++)
      (void)GenericAssignSort(
        (void *)((xptrint)Result+Sort->DataOffset+i*Sort->CompSort->SortSize), 
        (void *)((xptrint)ST+Sort->DataOffset+(Start-1+i)*Sort->CompSort->SortSize),
	XASS_MR_TMP_NF, Sort->CompSort);
    Result->Length = SubLength;
  }
  return (void *)Result;
}


void GenLString_Append (xLString_Type *ST, void *Item, tSDLLStringInfo *Sort)
{
  if (ST->Length != Sort->MaxLength) {
    (void)GenericAssignSort(
       (void *)((xptrint)ST + Sort->DataOffset +
		(ST->Length)*Sort->CompSort->SortSize), 
       Item, XASS_MR_ASS_NF, Sort->CompSort);
    ST->Length++;
  }
#ifdef XECSOP
  else {
    xSDLOpError("Append to string sort with upper size",
		"String is full. Component not appended");
  }
#endif
}


void * GenLString_Extract (
  xLString_Type   *ST,
  SDL_Integer      Index,
  tSDLLStringInfo *Sort)
{
  /* Used also for string modify */
  if (ST->Length == 0) {
#ifdef XECSOP
    xSDLOpError("Extract/Modify of string sort",
		"String is empty. Index set to 1");
#endif
    Index = 1;
  } else if (Index > ST->Length) {
#ifdef XECSOP
    xSDLOpError("Extract/Modify of string sort",
		"Index > string length. Index set to 1");
#endif
    Index = 1;
  } else if (Index <= (SDL_Integer)0) {
#ifdef XECSOP
    xSDLOpError("Extract/Modify of string sort",
		"Index <= 0. Index set to 1");
#endif
    Index = 1;
  }
  return (void *)((xptrint)ST + Sort->DataOffset +
		  (Index-1)*Sort->CompSort->SortSize);
}
#endif  /* XNOUSE_LSTRING_GENERATOR */



#ifndef XNOUSE_OWN_GENERATOR
/* ******************************** Own ******************************** */
void * GenOwn_Assign(void *yVar, void *yExpr, int AssType, tSDLTypeInfo *Sort)
{
  /* wrapper for GenericAssignSort. This function takes Own pointer as
     yExpr parameter. GenericAssignSort takes address of Own pointer as
     yExpr parameter. */
  return GenericAssignSort(yVar, (void *)&yExpr, AssType, Sort);
}

void * GenOwn_Copy(void *yVar, tSDLTypeInfo *Sort)
{
  void * Result;
  if (yVar) {
#ifdef XVALIDATOR_LIB
    Result = XALLOC_REF_VAL(((tSDLGenInfo *)Sort)->CompOrFatherSort->SortSize,
                            ((tSDLGenInfo *)Sort)->SortIdNode);
#else
    Result = XALLOC(((tSDLGenInfo *)Sort)->CompOrFatherSort->SortSize,
                    ((tSDLGenInfo *)Sort)->CompOrFatherSort);
#endif
#ifndef XSCT_CMICRO
#ifdef XTRACE
    if (xShouldBeTraced(xtTen, (xPrsNode)0)) {
      char C[200];
#ifdef T_SDL_NAMES
      sprintf(C, "*     ALLOC of own data %s at address %p\n",
              Sort->Name, Result);
#else
      sprintf(C, "*     ALLOC of own data at address %p\n", Result);
#endif
      xPrintString(C);
    }
#endif
#endif
#ifdef XEOWN
    xInsertOwnList(Result, Sort);
#endif
    return GenericAssignSort(Result, yVar, XASS_AC_TMP_NF,
			     ((tSDLGenInfo *)Sort)->CompOrFatherSort);
  }
  return 0;
}

SDL_Boolean GenOwn_Equal(void *yExpr1, void *yExpr2, tSDLTypeInfo *Sort)
{
  /* wrapper for GenericEqualSort. This function takes Own pointers as
     yExpr parameters. GenericEqualSort takes address of Own pointers as
     yExpr parameters. */
  if (yExpr1==0 && yExpr2==0)
    return SDL_True;
  if (yExpr1!=0 && yExpr2!=0)
    return GenericEqualSort(yExpr1, yExpr2,
			    ((tSDLGenInfo *)Sort)->CompOrFatherSort);
  return SDL_False;
}

void * GenOwn_Pass(void **yVar)
{
  void * Result;
  Result = *yVar;
  *yVar = 0;
  return Result;
}

void * GenericPassOwn(void **yVar, void * Result, tSDLTypeInfo *Sort)
     /* for types containing Own */
{
  return GenericAssignSort(Result, *yVar, XASS_MR_TMP_NF, (tSDLTypeInfo *)Sort);
}

#endif /* XNOUSE_OWN_GENERATOR */



/* ********************************************************************* */
/* Convert to address functions */

void * yMkAddr_SDL_Boolean (SDL_Boolean Value, SDL_Boolean * Result)
{
  *Result = Value;
  return (void *)Result;
}

void * yMkAddr_SDL_Bit (SDL_Bit Value, SDL_Bit * Result)
{
  *Result = Value;
  return (void *)Result;
}

#ifndef XNOUSEOFOCTETBITSTRING
void * yMkAddr_SDL_Octet (SDL_Octet Value, SDL_Octet * Result)
{
  *Result = Value;
  return (void *)Result;
}
#endif

void * yMkAddr_SDL_Character (SDL_Character Value, SDL_Character * Result)
{
  *Result = Value;
  return (void *)Result;
}

void * yMkAddr_SDL_Charstring (SDL_Charstring Value, SDL_Charstring * Result)
{
  *Result = Value;
  return (void *)Result;
}

void * yMkAddr_SDL_Duration (SDL_Duration Value, SDL_Duration * Result)
{
  *Result = Value;
  return (void *)Result;
}

void * yMkAddr_SDL_Integer (SDL_Integer Value, SDL_Integer * Result)
{
  *Result = Value;
  return (void *)Result;
}

void * yMkAddr_SDL_Null (SDL_Null Value, SDL_Null * Result)
{
  *Result = Value;
  return (void *)Result;
}

void * yMkAddr_SDL_PId (SDL_PId Value, SDL_PId * Result)
{
  *Result = Value;
  return (void *)Result;
}

#ifndef XNOUSEOFREAL
void * yMkAddr_SDL_Real (SDL_Real Value, SDL_Real * Result)
{
  *Result = Value;
  return (void *)Result;
}
#endif

void * yMkAddr_GenericShortEnum (XENUM_TYPE Value, XENUM_TYPE * Result)
{
  *Result = Value;
  return (void *)Result;
}



/****+***************************************************************
05   Utility functions
********************************************************************/

/*---+---------------------------------------------------------------
     GenericGetValue
-------------------------------------------------------------------*/
int GenericGetValue(unsigned Size, void * addr)
{
  switch (Size)
    {
    case sizeof(signed char):
      return (int)(*(unsigned char *)addr);
#if (INT_MAX != CHAR_MAX)
    case sizeof(int):
      return *(int *)addr;
#endif
#if (SHRT_MAX != CHAR_MAX) && (SHRT_MAX != INT_MAX)
    case sizeof(short):
      return (int)(*(short *)addr);
#endif
#if (LONG_MAX != CHAR_MAX) && (LONG_MAX != INT_MAX) && (LONG_MAX != SHRT_MAX)
    case sizeof(long):
      return (int)(*(long *)addr);
#endif
    default:
      return *(int *)addr;
    }
}


/*---+---------------------------------------------------------------
     GenericSetValue
-------------------------------------------------------------------*/
void GenericSetValue(unsigned Size, void * addr, int val)
{
  switch (Size)
    {
    case sizeof(signed char):
      *(signed char *)addr = (signed char)val;
      break;
#if (INT_MAX != CHAR_MAX)
    case sizeof(int):
      *(int *)addr = val;
      break;
#endif
#if (SHRT_MAX != CHAR_MAX) && (SHRT_MAX != INT_MAX)
    case sizeof(short):
      *(short *)addr = (short)val;
      break;
#endif
#if (LONG_MAX != CHAR_MAX) && (LONG_MAX != INT_MAX) && (LONG_MAX != SHRT_MAX)
    case sizeof(long):
      *(long *)addr = (long)val;
      break;
#endif
    default:
      *(int *)addr = val;
      break;
    }
}



#ifdef XREADANDWRITEF
/****+***************************************************************
06   Write buffer management
********************************************************************/

#define DEF_LENGTH	(4)		/* MUST be same as array in rep def! */

#define BUF_ALL_POW	(6)		/* Allocation increment 2^6 */
#define BUF_ALL_INC	(1U<<(BUF_ALL_POW))
#define BUF_ALL_MASK	((BUF_ALL_INC)-1)

#define MALLOC_OVERHEAD	(8)


/* #define HAVE_REALLOC */

/*
 * Memory allocation for WriteBufRep:
 *      Linear:
 *              #define XUSEWRITEBUFREP_LIN
 *              Allocated is a multiple of BUF_ALL_INC
 *      Exponential:
 *              this is the default
 *              Allocated size is pow(2,i), where i = 6,7,8,...
 *      Fibonacci:
 *              #define XUSEWRITEBUFREP_FIB
 *              Size is min( pow(2,i), pow(2,i)+pow(2,i-1) )
 */
/* #define XUSEWRITEBUFREP_LIN */
#define XUSEWRITEBUFREP_FIB


/*
 * New
 *	Realloc functionality.  Resize the buffer.
 *	Returns 0 on failure.
 *	Old buffer is NOT deleted on failure!
 *	Old buffer IS deleted on success.
 */
static WriteBufRep* WriteBufRep_New (WriteBufRep* old, unsigned new_length)
{
  unsigned all;
  unsigned bytes = (sizeof(WriteBufRep) + new_length +
		     MALLOC_OVERHEAD - DEF_LENGTH);
  WriteBufRep* me;

#ifdef XUSEWRITEBUFREP_LIN
  if (bytes & BUF_ALL_MASK) {
    bytes += BUF_ALL_INC;
    bytes &= ~(BUF_ALL_MASK);
  }
  else if (!bytes) {
    bytes = BUF_ALL_INC;
  }
#else
  {
    /* Exponential size allocation */
    /* Find top bit set */
    unsigned top_bit = 1U << (BUF_ALL_POW-1);
    unsigned bit     = bytes >> BUF_ALL_POW;
    while ( bit ) {
      top_bit <<= 1;
      bit >>= 1;
    }
#ifdef XUSEWRITEBUFREP_FIB
    if ( bytes & (top_bit-1) ) {
      if ( bytes & (top_bit >> 1 ) ) {
        top_bit <<= 1;
      }
      else {
        top_bit |= (top_bit >> 1);
      }
    }
#else
    if ( bytes & (top_bit-1) )
      top_bit <<= 1;
#endif
    bytes = top_bit;
  }
#endif

  bytes -= MALLOC_OVERHEAD;
  all = bytes - sizeof(WriteBufRep) + DEF_LENGTH;

  if (old && (all < old->all))
    return old;

#if defined(HAVE_REALLOC)
  me = (WriteBufRep *)realloc(old, bytes);
  if (! old)
    me->len = 0;
#else
  me = (WriteBufRep *)xAlloc(bytes);
  if (old) {
    me->len = old->len;
    memcpy(me->A, old->A, old->len);
    if (all > old->len)
      me->A[ old->len + 1 ] = '\0';
#ifdef XVALIDATOR_LIB
    XFREE((void**)&old, sizeof(WriteBufRep) + old->all - DEF_LENGTH);
#else
    xFree((void**)&old);
#endif
  } else {
    me->len = 0;
  }
#endif /* HAVE_REALLOC */
  me->all = all;

  return me;
}


WriteBuf* WriteBuf_New (unsigned start_length)
{
  WriteBuf* me = (WriteBuf *)xAlloc(sizeof(WriteBuf));
  if (me) {
    me->rep = WriteBufRep_New(0, start_length);
    if (!me->rep)
#ifdef XVALIDATOR_LIB
      XFREE((void**)&me, sizeof(WriteBuf));
#else
      xFree((void**)&me);
#endif
  }
  return me;
}



void WriteBuf_Del (WriteBuf** buf)
{
  if ((*buf)->rep) {
#if defined(HAVE_REALLOC)
    free((*buf)->rep);
#else
#ifdef XVALIDATOR_LIB
    XFREE((void**)&((*buf)->rep),
          sizeof(WriteBufRep) + (*buf)->rep->all - DEF_LENGTH);
#else
    xFree((void**)&((*buf)->rep));
#endif
#endif
  }
#ifdef XVALIDATOR_LIB
  XFREE((void**)buf, sizeof(WriteBuf));
#else
  xFree((void**)buf);
#endif
}



WriteBufStatus WriteBuf_Resize (WriteBuf* buf, unsigned new_length)
{
  WriteBufRep* rep = WriteBufRep_New(buf->rep, 
				     (new_length > (buf->rep->len+1) ?
				      new_length : (buf->rep->len+1)));
  if (rep) {
    buf->rep = rep;
    return 0;
  }
  return 1;				/* FAILED */
}




WriteBufStatus WriteBuf_Add_Char (WriteBuf* buf, char c)
{
  if (buf->rep->len >= buf->rep->all) {
    WriteBufStatus status;
    status = WriteBuf_Resize(buf, buf->rep->all + BUF_ALL_INC);
    if (status)
      return status;
  }

  buf->rep->A[ (buf->rep->len)++ ] = c;
  return 0;
}



WriteBufStatus WriteBuf_Add_String (WriteBuf* buf, const char* s, unsigned s_len)
{
  int status;

  if (!s_len)
    s_len = strlen(s);

  status = WriteBuf_Secure(buf, s_len);
  if (status)
    return status;

  memcpy(&(buf->rep->A[ buf->rep->len ]), s, s_len);
  buf->rep->len += s_len;

  return 0;
}



WriteBufStatus WriteBuf_Add_WriteBuf (WriteBuf* buf, const WriteBuf* b)
{
  int status = WriteBuf_Secure(buf, b->rep->len);
  if (status)
    return status;

  memcpy(&(buf->rep->A[ buf->rep->len ]), b->rep->A, b->rep->len);
  buf->rep->len += b->rep->len;

  return 0;
}



WriteBufStatus WriteBuf_Add_Fmt(
  WriteBuf* buf,
  unsigned max_len,
  const char* fmt,
  ...)
{
  va_list ap;
  int status = WriteBuf_Secure(buf, max_len+1); /* + trailing \0 */
  unsigned len = buf->rep->len;
  char*    A   = buf->rep->A;
  if (status)
    return status;

  va_start(ap, fmt);
#if HAVE_VSNPRINTF
  status = vsnprintf(&(A[ len ]), max_len, fmt, ap);
#else
  status = vsprintf(&(A[ len ]), fmt, ap);
#endif
  va_end(ap);

  if ((status >= 0) && (status <= max_len)) {
    buf->rep->len += status;
    return 0;
  }

  return 1;
}


WriteBufStatus WriteBuf_Terminate (WriteBuf* buf)
{
  int status = WriteBuf_Secure(buf, 1);
  if (status)
    return status;
  buf->rep->A[ buf->rep->len ] = '\0';
  return 0;
}
#endif /* XREADANDWRITEF */



#ifdef XREADANDWRITEF
/****+***************************************************************
07   Generic write function
********************************************************************/

#define TIMEVALUEDECIMALS 4
        /* Number of decimals given normally when time or duration printed */

static char * xfCharacterLiteral[] = {
    "NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "BEL",
    "BS",  "HT",  "LF",  "VT",  "FF",  "CR",  "SO",  "SI",
    "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB",
    "CAN", "EM",  "SUB", "ESC", "IS4", "IS3", "IS2", "IS1",
    "DEL",  /* 127 is coded as 32 */
    "E_NUL", "E_SOH", "E_STX", "E_ETX", "E_EOT", "E_ENQ", "E_ACK", "E_BEL",
    "E_BS",  "E_HT",  "E_LF",  "E_VT",  "E_FF",  "E_CR",  "E_SO",  "E_SI",
    "E_DLE", "E_DC1", "E_DC2", "E_DC3", "E_DC4", "E_NAK", "E_SYN", "E_ETB",
    "E_CAN", "E_EM",  "E_SUB", "E_ESC", "E_IS4", "E_IS3", "E_IS2", "E_IS1"
            /* 128->159 is coded as 33->64 */
};


#ifdef X_LONG_INT
#define GENPRINT_INT_FORMAT  "%ld"
#define GENSCAN_INT_FORMAT   "%ld"
#else
#define GENPRINT_INT_FORMAT  "%d"
#define GENSCAN_INT_FORMAT   "%d"
#endif

static const char xfHexCode[] = "0123456789ABCDEF";

#define USE_SDL_SYNTAX(FORMAT)		((((unsigned)Format) & 0x03)==0x00)
#define USE_ASN1_SYNTAX(FORMAT)		((((unsigned)Format) & 0x03)==0x01)
#define USE_ITEX_SYNTAX(FORMAT)		((((unsigned)Format) & 0x03)==0x02)
#define USE_HEX_REF_SYNTAX(FORMAT)	((((unsigned)Format) & 0x04)==0x04)
#define USE_DEREF_SYNTAX(FORMAT)	((((unsigned)Format) & 0x08)==0x08)

/* Buffer for ASN1 Real conversion */
#define RBUF_LEN	(32)


#ifndef HAVE_STRNCASECMP
/* Case insensitive string comparison (unless there already is one around */
#define strncasecmp(A,B,N) xStrnCaseCmp(A,B,N)

static int xStrnCaseCmp(T_CONST char* a, T_CONST char* b, int n)
{
  while( n-- ) {
    if ( (unsigned char)*a != (unsigned char)*b ) {
      unsigned char ca = (unsigned char)*a;
      unsigned char cb = (unsigned char)*b;
      if ( (ca >= (unsigned char)'a') && (ca <= (unsigned char)'z') )
        ca = (ca - (unsigned char)'a') + (unsigned char)'A';
      if ( (cb >= (unsigned char)'a') && (cb <= (unsigned char)'z') )
        cb = (cb - (unsigned char)'a') + (unsigned char)'A';
      if ( ca != cb ) {
        if ( (unsigned char)*a < (unsigned char)*b ) return -1;
        return 1;
      }
    }
    ++a;
    ++b;
  }
  return 0;
}
#endif


/* defaultValue is returned on no match! */
static T_CONST char*
GetEnumName(int value, tSDLEnumLiteralInfo* A, int Alen,
	     T_CONST char* defaultName)
{
  int i;
  for (i = 0; i < Alen; ++i)
    if (value == A[i].LiteralValue)
      return A[i].LiteralName;
  return defaultName;
}

/* Returns index in A of unique match (>=0),  */
/* or -1 for no match or -2 for ambiguous */
static int GetEnumIndex(T_CONST char* name, tSDLEnumLiteralInfo* A, int Alen)
{
  int n = -1;				/* mark no match */
  int l = strlen(name);
  int i;
  for (i = 0; i < Alen; ++i) {
    if (!strncmp(name, A[i].LiteralName, l)) {
      if (n < 0) {
	n = i;
      }
      else {
        if (A[i].LiteralName[l] == '\0') { /* Exact match */
          n = i;
        }
        else if (A[n].LiteralName[l] != '\0') { /* Not Exact match */
          return -2;			/* second match, ambiguous */
        }
      }
    }
  }
  return n;
}

#ifndef XVALIDATOR_LIB
static void LocalWritePId (
  WriteBuf     *buf, 
  void         *Address, 
  tSDLTypeInfo *Sort, 
  int           Format)
{
  SDL_PId* P = (SDL_PId *)Address;
#ifdef XPMCOMM
  if (XSYSD xRaW_use_Global_PId) {
#ifdef X_XPTRINT_LONG
    xAddBuf_Fmt(buf, "%d %lu", P->GlobalNodeNr, (xptrint)(P->LocalPId));
#else
    xAddBuf_Fmt(buf, "%d %u",  P->GlobalNodeNr, (xptrint)(P->LocalPId));
#endif
    return;
  }
#endif /* XPMCOMM */

  if (xEq_SDL_PId_NULL(*P)) {
    (void)WriteBuf_Add_String(buf, "null", 0);
    return;
  }
  if (P->GlobalNodeNr != xGlobalNodeNumber()) {
    xAddBuf_Fmt(buf, "%d %p", P->GlobalNodeNr, (void *)(P->LocalPId));
    return;
  }
    
  if (xNeedsQualifier((xIdNode)(*P).LocalPId->PrsP->NameNode)) {
    xGetQualifier(buf, (xIdNode)(*P).LocalPId->PrsP->NameNode,
		  (*P).LocalPId->PrsP->BlockInstNumber);
    WriteBuf_Add_Char(buf, ' ');
  }
    
  if (
#ifndef XPRSOPT
      /* Process instance alive? */
      xEq_SDL_PId((*P).LocalPId->PrsP->Self, *P)
#else
#  ifdef XNRINST
      (*P).LocalPId->InstNr != 0
#  else
      !(P->LocalPId->InAvailList)
#  endif
#endif /* XPRSOPT */
#ifdef XNRINST
      ) {
    xAddBuf_Fmt(buf, "%s:%d", (*P).LocalPId->PrsP->NameNode->Name,
		(*P).LocalPId->InstNr);
  } else {
    xAddBuf_Fmt(buf, "%s:%d+", (*P).LocalPId->PrsP->NameNode->Name,
                (*P).LocalPId->InstNr);
  }
#else
      ) {
    xAddBuf_Fmt(buf, "%s", (*P).LocalPId->PrsP->NameNode->Name);
  } else {
    xAddBuf_Fmt(buf, "%s+", (*P).LocalPId->PrsP->NameNode->Name);
  }
#endif
}
#endif  /* XVALIDATOR_LIB */



static void ** xLoopRefWriteList = NULL;

#ifdef XMONITOR
void xReleasexLoopRefWriteList(void)
{
  releaseNL((void ***)&xLoopRefWriteList);
}
#endif

#ifndef XVALIDATOR_LIB
extern int xDisplayArrayWithIndex;
#endif

static void
LocalGenericWriteSort(WriteBuf      * buf,
		       void         * Address,
		       tSDLTypeInfo * Sort,
		       int            Format)
{
  char *tmp;

  for(;;) {
#ifndef XNOUSE_OPFUNCS
    if (Sort->OpFuncs && Sort->OpFuncs->WriteFunc) {
      tmp = (Sort->OpFuncs->WriteFunc)(Address);
      WriteBuf_Add_String(buf, tmp, 0);
      return;
    }
#endif

    if ((Sort->TypeClass == type_SDL_Syntype) ||
	 (Sort->TypeClass == type_SDL_Inherits)) {
      Sort = ((tSDLGenInfo*)Sort)->CompOrFatherSort;
    }

    else {
      break;
    }
  }

  switch (Sort->TypeClass) {

  case type_SDL_Integer : /* ----------------------------------------------- */
    /* [-+][0-9]+ */
    (void)WriteBuf_Add_Fmt(buf, 16, XPRINT_INT_FORMAT, 
			       *(SDL_Integer *)Address);
    break;


#ifndef XNOUSEOFREAL
  case type_SDL_Real : /* -------------------------------------------------- */
    /* SDL : %e */
    /* ASN1: '{mantissa ' [0-9]+ ', base ' ('10'|'2') 
       ', exponent ' [-+]?[0-9]+ '}' */
    {
      SDL_Real x = *(SDL_Real*)Address;
      if ( ! USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_Fmt(buf, 124, "%.4f", x);
      } 
      else if (x == 0) {
	(void)WriteBuf_Add_Char(buf, '0');
      }
      else {
	char rbuf[ RBUF_LEN ];
	char*	p = rbuf;

	int e = 0;
	int z = 0;
        int exp_sign = 1;
        int exp_val = 0;

	sprintf(rbuf,"%e",x);	 /* '-'? [0-9] '.' [0-9]+ 'e' [-+][0-9][0-9] */
	if (*p == '-') ++p;	 /*  ^                                       */
	while (*p != '.') ++p; /*             ^                            */
	++p;                     /*                ^                         */
	while (*p != 'e') {    /*                        ^                 */
	  *(p-1) = *p;
	  if ( *p == '0' ) {
	    ++z;
	  }
	  else {
	    z = 0;
	  }
	  ++p;
	  --e;
	}
	*(p-1-z) = '\0';
	++p;

        if ( *p == '-' ) {
          exp_sign = -1;
        }
        ++p;

        while ( *p ) {
          exp_val *= 10;
          exp_val += *p - '0';
          ++p;
        }

        e += exp_val * exp_sign + z;

	(void)WriteBuf_Add_String(buf, "{mantissa ", 10);
	(void)WriteBuf_Add_String(buf, rbuf, 0);
	(void)WriteBuf_Add_String(buf, ", base 10, exponent ", 20);
	(void)WriteBuf_Add_Fmt(buf, 8 ,"%d}", e);
      }
    }
    break;				/* Real */
#endif


  case type_SDL_Natural : /* ----------------------------------------------- */
    /* [0-9]+ */
    (void)WriteBuf_Add_Fmt(buf, 16, XPRINT_INT_FORMAT,
			       *(SDL_Natural *)Address);
    break;


  case type_SDL_Boolean : /* ----------------------------------------------- */
    if (*(SDL_Boolean *)Address) {
      (void)WriteBuf_Add_String(buf, 
				 (USE_ASN1_SYNTAX(Format) ? "TRUE" : "true"),
				 4);
    }
    else {
      (void)WriteBuf_Add_String(buf,
				 (USE_ASN1_SYNTAX(Format) ? "FALSE" : "false"),
				 5);
    }
    break;


  case type_SDL_ShortInt : /* ---------------------------------------------- */
    (void)WriteBuf_Add_Fmt(buf, 6, "%hd", *(short int *)Address);
    break;


  case type_SDL_LongInt : /* ----------------------------------------------- */
    (void)WriteBuf_Add_Fmt(buf, 16, "%ld", *(long int *)Address);
    break;


  case type_SDL_UnsignedShortInt : /* -------------------------------------- */
    (void)WriteBuf_Add_Fmt(buf, 6, "%hu", *(unsigned short int *)Address);
    break;


  case type_SDL_UnsignedInt : /* ------------------------------------------- */
    (void)WriteBuf_Add_Fmt(buf, 16, "%u", *(unsigned int *)Address);
    break;


  case type_SDL_UnsignedLongInt : /* --------------------------------------- */
    (void)WriteBuf_Add_Fmt(buf, 16, "%lu", *(unsigned long int *)Address);
    break;


#ifndef XNOUSEOFREAL
  case type_SDL_Float : /* ------------------------------------------------- */
    {
      SDL_Real r;
      r = (SDL_Real)*(float *)Address;
      LocalGenericWriteSort(buf, (void *)&r, &ySDL_SDL_Real, Format);
    }
    break;
#endif


  case type_SDL_Character : /* --------------------------------------------- */
    {
      unsigned Ch;
      Ch = (unsigned)*(SDL_Character *)Address;
      if (Ch < 0x20) {
	(void)WriteBuf_Add_String(buf, xfCharacterLiteral[ Ch ], 0);
      }
      else if ((Ch & 0x7f) < 0x20) {
	(void)WriteBuf_Add_String(buf, 
				   xfCharacterLiteral[(Ch & 0x7f) + 0x21], 0);
      }
      else if (Ch == 0x7f) {
	(void)WriteBuf_Add_String(buf, xfCharacterLiteral[ 0x20 ], 0);
      }
      else {
	char quote = (USE_ASN1_SYNTAX(Format) ? '"' : '\'');
	(void)WriteBuf_Secure(buf, 4);
	WriteBuf_ADD_CHAR(buf, quote);
	if (Ch == quote)		/* a quote character is rep as four! */
	  WriteBuf_ADD_CHAR(buf, quote);
	WriteBuf_ADD_CHAR(buf, Ch);
	WriteBuf_ADD_CHAR(buf, quote);
      }
    }
    break;


  case type_SDL_Time : /* -------------------------------------------------- */
  case type_SDL_Duration :
    {
      unsigned decimals = TIMEVALUEDECIMALS;
      int s;
      int ns;
      int      is_neg = 0;
#if !defined(XTIMEASINT_TICKS) && !defined(XTIMEASINT_SECS)
      /* { xint32 s, ns } */
      s  = ((SDL_Duration*)Address)->s;
      ns = ((SDL_Duration*)Address)->ns;
      if ((s < 0) || (ns < 0)) {
	s  = -s;
	ns = -ns;
	is_neg = 1;
      }
#else
#  ifdef XUNSIGNED_TIME
      /* unsigned long */
      s  = *(SDL_Duration*)Address / SDT_TICKPERSEC;
      ns = *(SDL_Duration*)Address - (s * SDT_TICKPERSEC);
#  else
      /* long */
      if (*(SDL_Duration*)Address < 0) {
	is_neg = 1;
	s  = -(*(SDL_Duration*)Address) / SDT_TICKPERSEC;
	ns = -(*(SDL_Duration*)Address) - (s * SDT_TICKPERSEC);
      }
      else {
	s  = *(SDL_Duration*)Address / SDT_TICKPERSEC;
	ns = *(SDL_Duration*)Address - (s * SDT_TICKPERSEC);
      }
#  endif
#endif
      /* Rounding */
      switch (decimals) {
      case 0:	ns += 500000000; break;
      case 1:	ns +=  50000000; break;
      case 2:	ns +=   5000000; break;
      case 3:	ns +=    500000; break;
      case 4:	ns +=     50000; break;
      case 5:	ns +=      5000; break;
      case 6:	ns +=       500; break;
      case 7:	ns +=        50; break;
      case 8:	ns +=         5; break;
      default:	decimals = 9;	 break;
      }
      if (ns > 1000000000) {
	ns -= 1000000000;
	++s;
      }
      if (is_neg) {
	(void)WriteBuf_Add_Char(buf, '-');
      }
      (void)WriteBuf_Add_Fmt(buf, 32, "%d.%09d", s, ns);
      /* Backup 9-decimals tokens in the output! */
      (void)WriteBuf_Backup(buf, 9 - decimals);
    }
    break;


  case type_SDL_Pid : /* --------------------------------------------------- */
#ifndef XVALIDATOR_LIB
    LocalWritePId(buf, Address, Sort, Format);
#else
    (void)WriteBuf_Add_String(buf, xWri_SDL_PId(Address), 0);
#endif
    break;

  case type_SDL_Charstring : /* -------------------------------------------- */
  case type_SDL_IA5String :
    {
      char  quote = (USE_ASN1_SYNTAX(Format) ? '"' : '\'') ;
      char *C = (Address ? *(SDL_Charstring*)Address : 0);
#ifdef XMULTIBYTE_SUPPORT
      int length;
#endif

      (void)WriteBuf_Add_Char(buf, quote);
      if (C) {
	++C;				/* Jump over L/V/... flag */

#ifdef XMULTIBYTE_SUPPORT
	length = strlen(C);
#endif
	while (*C != '\0') {

#ifdef XMULTIBYTE_SUPPORT
	  int mlen;
	  mlen = mblen(C, length);
	  if (mlen > 1) {
	    (void)WriteBuf_Add_String(buf, C, mlen);
	    C += mlen;
	    length -= mlen;
	  }

	  else {
#endif
	    unsigned char Ch = (unsigned char)*C;
	    if (Ch < 0x20) {
	      (void)WriteBuf_Secure(buf, 5);
	      WriteBuf_ADD_CHAR(buf, quote);
	      (void)WriteBuf_Add_String(buf, xfCharacterLiteral[ Ch ], 0);
	      WriteBuf_ADD_CHAR(buf, quote);
	    }
	    else if ((Ch & 0x7f) < 0x20) {
	      (void)WriteBuf_Secure(buf, 5);
	      WriteBuf_ADD_CHAR(buf, quote);
	      (void)WriteBuf_Add_String(buf, 
				 xfCharacterLiteral[ (Ch & 0x7f) + 0x21 ], 0);
	      WriteBuf_ADD_CHAR(buf, quote);
	    }
	    else if (Ch == 0x7f) {
	      (void)WriteBuf_Secure(buf, 5);
	      WriteBuf_ADD_CHAR(buf, quote);
	      (void)WriteBuf_Add_String(buf,
					    xfCharacterLiteral[ 0x20 ], 0);
	      WriteBuf_ADD_CHAR(buf, quote);
	    }
	    else {
	      (void)WriteBuf_Add_Char(buf, Ch);
	      if (Ch == quote)
		WriteBuf_Add_Char(buf, quote);
	    }
	    ++C;
#ifdef XMULTIBYTE_SUPPORT
	    --length;
	  } /* else mblen > 1 */ 
#endif
	} /* while(*Ch != 0) */
      } /* if (C) */
      (void)WriteBuf_Add_Char(buf, quote);
    }
    break;

  case type_SDL_Charstar : /* ---------------------------------------------- */
    {
      int length = ( *(char **)Address ? strlen(*(char **)Address) : 0 ) ;
      SDL_Charstring tmp_string = 
	(SDL_Charstring) XALLOC( 2+length,
				 (tSDLTypeInfo *)&ySDL_SDL_Charstring);
      tmp_string[0] = 'T';
      if ( length )
	memcpy( tmp_string + 1 , *(char **)Address, length );
      tmp_string[ length + 1 ] = '\0';
      LocalGenericWriteSort(buf, &tmp_string, &ySDL_SDL_Charstring, Format);
      XFREE((void **)&tmp_string, 2+length );
    }
    break;

  case type_SDL_NumericString : /* ----------------------------------------- */
    LocalGenericWriteSort(buf, Address, &ySDL_SDL_Charstring, Format);
#ifdef XITEXCOMM
    if (USE_ITEX_SYNTAX(Format)) {
      (void)WriteBuf_Add_Char(buf, 'N');
    }
#endif
    break;


  case type_SDL_PrintableString : /* --------------------------------------- */
    LocalGenericWriteSort(buf, Address, &ySDL_SDL_Charstring, Format);
#ifdef XITEXCOMM
    if (USE_ITEX_SYNTAX(Format)) {
      (void)WriteBuf_Add_Char(buf, 'P');
    }
#endif
    break;


  case type_SDL_VisibleString : /* ----------------------------------------- */
    LocalGenericWriteSort(buf, Address, &ySDL_SDL_Charstring, Format);
#ifdef XITEXCOMM
    if (USE_ITEX_SYNTAX(Format)) {
      (void)WriteBuf_Add_Char(buf, 'V');
    }
#endif
    break;


  case type_SDL_Bit : /* --------------------------------------------------- */
    if (USE_SDL_SYNTAX(Format)) {
      (void)WriteBuf_Add_Char(buf, (char)(*(SDL_Bit *)Address ? '1' : '0'));
    }
    else {
      (void)WriteBuf_Add_String(buf, (*(SDL_Bit *)Address ?
					   "'1'B" : "'0'B"), 4);
    }
    break;


#ifndef XNOUSEOFOCTETBITSTRING
  case type_SDL_Bit_string : /* -------------------------------------------- */
    {
      int Length = ((SDL_Bit_String*)Address)->Length;
      SDL_Bit* Array = ((SDL_Bit_String*)Address)->Bits;
      int i;
      (void)WriteBuf_Secure(buf,  Length + 3);
      WriteBuf_ADD_CHAR(buf, '\'');
      for (i = 0; i < Length; ++i) {
	WriteBuf_ADD_CHAR(buf, Array[i] ? '1' : '0');
      }
      WriteBuf_ADD_CHAR(buf, '\'');
      WriteBuf_ADD_CHAR(buf, 'B');
    }
    break;
#endif


#ifndef XNOUSEOFOCTETBITSTRING
  case type_SDL_Octet : /* ------------------------------------------------- */
    {
      SDL_Octet x = *(SDL_Octet *)Address;
#ifdef XITEXCOMM
      if ( USE_ITEX_SYNTAX(Format) ) {
	(void)WriteBuf_Secure(buf, 6);
	WriteBuf_ADD_CHAR(buf, '\'');
	WriteBuf_ADD_CHAR(buf, xfHexCode[ x >> 4 ]);
	WriteBuf_ADD_CHAR(buf, xfHexCode[ x & 0x0f ]);
	WriteBuf_ADD_CHAR(buf, '\'');
	WriteBuf_ADD_CHAR(buf, 'O');
      }
      else
#endif
      if (USE_SDL_SYNTAX(Format)) {
	(void)WriteBuf_Secure(buf, 2);
	WriteBuf_ADD_CHAR(buf, xfHexCode[ x >> 4 ]);
	WriteBuf_ADD_CHAR(buf, xfHexCode[ x & 0x0f ]);
      }
      else {
	(void)WriteBuf_Secure(buf, 5);
	WriteBuf_ADD_CHAR(buf, '\'');
	WriteBuf_ADD_CHAR(buf, xfHexCode[ x >> 4 ]);
	WriteBuf_ADD_CHAR(buf, xfHexCode[ x & 0x0f ]);
	WriteBuf_ADD_CHAR(buf, '\'');
	WriteBuf_ADD_CHAR(buf, 'H');
      }
    }
    break;
#endif


#ifndef XNOUSEOFOCTETBITSTRING
  case type_SDL_Octet_string : /* ------------------------------------------ */
    {
      int		Length = ((SDL_Bit_String *)Address)->Length;
      unsigned char*	Array  = ((SDL_Bit_String *)Address)->Bits;
      int i;
      (void)WriteBuf_Secure(buf, Length*2 + 3);
      WriteBuf_ADD_CHAR(buf, '\'');
      for (i = 0; i < Length; ++i) {
	unsigned char x = Array[i];
	WriteBuf_ADD_CHAR(buf, xfHexCode[ x >> 4 ]);
	WriteBuf_ADD_CHAR(buf, xfHexCode[ x & 0x0f ]);
      }
      WriteBuf_ADD_CHAR(buf, '\'');
      WriteBuf_ADD_CHAR(buf, (USE_ITEX_SYNTAX(Format) ? 'O' : 'H'));
    }
    break;
#endif


  case type_SDL_NULL : /* -------------------------------------------------- */
    if (*(SDL_Null*)Address == SDL_NullValue) {
      (void)WriteBuf_Add_String(buf, (USE_ASN1_SYNTAX(Format) ?
					   "NULL" : "Null"), 4);
    }
    else {
      (void)WriteBuf_Add_String(buf, (USE_ASN1_SYNTAX(Format) ?
					   "Illegal NULL value" :
					   "Illegal Null value"), 18);
    }
    break;


#ifndef XNOUSEOFOBJECTIDENTIFIER
  case type_SDL_Object_identifier : /* ------------------------------------- */
    {
      SDL_Object_Identifier_yptr p = ((SDL_Object_Identifier*)Address)->First;

      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "{ ", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, "(. ", 3);
      }

      while (p) {
	(void)WriteBuf_Add_Fmt(buf, 16, GENPRINT_INT_FORMAT, p->Data);
	p = p->Suc;
	if (p) {
	  if (! USE_ASN1_SYNTAX(Format)) {
	    (void)WriteBuf_Add_String(buf, ", ", 2);
	  }
	  else {
	    (void)WriteBuf_Add_Char(buf, ' ');
	  }
	}
      }

      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " }", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, " .)", 3);
      }
    }
    break;
#endif


  case type_SDL_Syntype : /* ----------------------------------------------- */
  case type_SDL_Inherits :
    /* NOTREACHED */
    /* Handled in fast shortcut before the switch! */
    LocalGenericWriteSort(buf, Address,
			   ((tSDLGenInfo*)Sort)->CompOrFatherSort,
			   Format);
    break;


#ifndef XNOUSE_STRUCT
  case type_SDL_Struct : /* ------------------------------------------------ */
    /* SDL : (. 1, 2.3, false .) */
    /* ASN1: { name1 1, name2 2.3, name3 false } */
    {
      int i;
      int i_max = ((tSDLStructInfo*)Sort)->NumOfComponents;
      int first = 1;
      tSDLFieldInfo* FieldSort = ((tSDLStructInfo*)Sort)->Components;

      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "{ ", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, "(. ", 3);
      }

      for (i = 0; i < i_max; ++i) {
	void* p = 0;		/* Ptr to value */
	int bf_val = 0;		/* bit field value */
	int what = 0;		/* {0,1,2}={NoVal,BitField,Ptr} */

	if (FieldSort[i].Offset == (xptrint)~0) {
	  /* Bit field */
	  if (FieldSort[i].ExtraInfo &&
	       (((tSDLFieldBitFInfo*)(FieldSort[i].ExtraInfo))->GetTag)) {
	    bf_val = ((tSDLFieldBitFInfo*)
		      (FieldSort[i].ExtraInfo))->GetTag(Address);
	  }
	  what = 1;
	}

	else if (FieldSort[i].ExtraInfo == 0
#ifdef XSHOWDEFAULT
                 || (XSHOWDEFAULT && FieldSort[i].ExtraInfo->DefaultValue)
#endif
                 ) {
	  /* Simple mandatory value */
	  p = (void*)((xptrint)Address + FieldSort[i].Offset);
	  what = 2;
	}

	else if (FieldSort[i].ExtraInfo->OffsetPresent) {
	  /* Optional value */
	  if (*(SDL_Boolean*)((xptrint)Address +
			       FieldSort[i].ExtraInfo->OffsetPresent)) {
	    /* Value present */
	    p = (void*)((xptrint)Address + FieldSort[i].Offset);
	    what = 2;
	  }
	  /* else: value not present */
	}

#ifdef T_SDL_NAMES
	if (USE_ASN1_SYNTAX(Format) && what == 0)
	  continue;
#endif
	if (!first)
	  (void)WriteBuf_Add_String(buf, ", ", 2);
        else
	  first = 0;

#ifdef T_SDL_NAMES
	if (USE_ASN1_SYNTAX(Format)) {
	  (void)WriteBuf_Add_String(buf, FieldSort[i].Name, 0);
	  (void)WriteBuf_Add_Char(buf, ' ');
	}
#endif
	if (what == 1) {
	  (void)WriteBuf_Add_Fmt(buf, 12, "%d", bf_val);
	}
	else if (what == 2) {
	  LocalGenericWriteSort(buf, p, FieldSort[i].CompSort,Format);
	}

      }

      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " }", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, " .)", 3);
      }
    }
    break; /* Struct */
#endif


#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
  case type_SDL_Choice : /* ------------------------------------------------ */
  case type_SDL_Union :	/* #UNION */
    /* SDL.UNION:	(. tag, value .) */
    /* ASN1.UNION:	tag:value */
    /* Choice:		tag:value */
    {
      int tag = 0;
      tSDLChoiceFieldInfo* Field =((tSDLChoiceInfo*)Sort)->Components;

      if (!USE_ASN1_SYNTAX(Format) && (Sort->TypeClass != type_SDL_Choice)){
	(void)WriteBuf_Add_String(buf, "(. ", 3);
      }


      LocalGenericWriteSort( buf, Address, ((tSDLChoiceInfo*)Sort)->TagSort,
			     Format);

      tag = GenericGetValue(((tSDLChoiceInfo *)Sort)->TagSortSize, Address);
      if ((tag < 0) || (tag >= ((tSDLChoiceInfo *)Sort)->NumOfComponents)) {
	(void)WriteBuf_Add_String(buf, "BADTAG", 6);
      }
      else {
	if (!USE_ASN1_SYNTAX(Format) && (Sort->TypeClass != type_SDL_Choice)){
	  (void)WriteBuf_Add_String(buf, ", ", 2);
	}
	else {
	  (void)WriteBuf_Add_String(buf, " : ", 3);
	}

	LocalGenericWriteSort(buf, (void*)((xptrint)Address +
				      ((tSDLChoiceInfo*)Sort)->OffsetToUnion),
			      Field[tag].CompSort, Format);
      }

      if (!USE_ASN1_SYNTAX(Format) && (Sort->TypeClass != type_SDL_Choice)){
	(void)WriteBuf_Add_String(buf, " .)", 3);
      }
    }
    break;
#endif


#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
  case type_SDL_ComBuf :
    if (*(xComBuf *)Address == 0) {
      (void)WriteBuf_Add_String(buf, "NULL", 4);
      break;
    }
    Address = &((*(xComBuf *)Address)->el);
    /* fall through */
  case type_SDL_Array : /* ------------------------------------------------- */
  case type_SDL_Carray :
    {
      int i;
      int i_max = ((tSDLArrayInfo*)Sort)->Length;
      xptrint dAddress = ((tSDLArrayInfo*)Sort)->CompSort->SortSize;

#ifndef XVALIDATOR_LIB
#ifdef XMSCE
      if (Sort->TypeClass == type_SDL_ComBuf)
        if (XSYSD MSCLogStarted && i_max>3)
          i_max = 3;
#endif
#endif

      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "{ ", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, "(: ", 3);
      }

      for (i = 0; i < i_max; ++i) {
	if ( i != 0 ) {
	  (void)WriteBuf_Add_String(buf, ", ", 2);
	}
#ifndef XVALIDATOR_LIB
#ifdef XMONITOR
	if ( xDisplayArrayWithIndex ) {
	  long val;
	  void* valPtr = (void*)&val;
	  GenericSetValue( ((tSDLArrayInfo*)Sort)->IndexSort->SortSize, 
			   valPtr, 
			   i + ((tSDLArrayInfo*)Sort)->LowestValue );
	  LocalGenericWriteSort(buf,valPtr, ((tSDLArrayInfo*)Sort)->IndexSort,
				Format);
	  (void)WriteBuf_Add_String(buf, ": ", 2);
	}
#endif
#endif
	LocalGenericWriteSort(buf, Address, ((tSDLArrayInfo*)Sort)->CompSort,
			       Format);
	Address = (void*)((xptrint)Address + dAddress);
      }

      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " }", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, " :)", 3);
      }
    }
    break;
#endif


#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray : /* ------------------------------------------------ */
    {
      xGArray_yptr p = ((xGArray_Type*)Address)->First;
      xptrint	yrecIndexOffset = ((tSDLGArrayInfo*)Sort)->yrecIndexOffset;
      xptrint	yrecDataOffset  = ((tSDLGArrayInfo*)Sort)->yrecDataOffset;

      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "{ ", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, "(: ", 3);
      }

      (void)WriteBuf_Add_String(buf, "(others:", 8);
      LocalGenericWriteSort(buf, 
			    (void*)((xptrint)Address + 
				    ((tSDLGArrayInfo*)Sort)->arrayDataOffset),
			     ((tSDLGArrayInfo*)Sort)->CompSort, Format);
      (void)WriteBuf_Add_Char(buf, ')');

      while (p) {

	(void)WriteBuf_Add_String(buf, ", ", 2);

	(void)WriteBuf_Add_Char(buf, '(');
	LocalGenericWriteSort(buf, 
			       (void*)((xptrint)p + yrecIndexOffset),
			       ((tSDLGArrayInfo*)Sort)->IndexSort, Format);
	(void)WriteBuf_Add_Char(buf, ':');
	LocalGenericWriteSort(buf, 
			       (void*)((xptrint)p + yrecDataOffset),
			       ((tSDLGArrayInfo*)Sort)->CompSort, Format);
	(void)WriteBuf_Add_Char(buf, ')');

	p = p->Suc;
      }

      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " }", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, " :)", 3);
      }
    }
    break;
#endif


#if !defined(XNOUSE_POWERSET_GENERATOR)
  case type_SDL_Powerset : /* ---------------------------------------------- */
    {
      unsigned long* p = (unsigned long*)Address;
      int i = 0;
      int i_max = ((tSDLPowersetInfo*)Sort)->Length;
      int first = 1;

      unsigned long Value;
      void* valPtr = (void*)&Value;

      tSDLTypeInfo* CompSort = ((tSDLPowersetInfo*)Sort)->CompSort;

      while ((CompSort->TypeClass == type_SDL_Syntype) ||
	      (CompSort->TypeClass == type_SDL_Inherits)) {
	CompSort = ((tSDLGenInfo*)CompSort)->CompOrFatherSort;
      }

      (void)WriteBuf_Add_String(buf, "[ ", 2);


      while (i < i_max) {
	int j = 32;
	unsigned long Bit = *p;

	while (j && Bit) {		/* NOTE: this assumes 32 bits! */
	  if (Bit &  1) {

	    if (!first) {
	      (void)WriteBuf_Add_String(buf, ", ", 2);
	    }
	    first = 0;

	    GenericSetValue(CompSort->SortSize, valPtr, 
			     i + ((tSDLPowersetInfo*)Sort)->LowestValue);
	    LocalGenericWriteSort(buf, valPtr, CompSort, Format);

	  } /* if Bit is set */
	  Bit >>= 1;
	  --j;
	  ++i;
	  if (i >= i_max)
	    break;
	} /* Loop over bits */
	i += j;				/* Add remaining bits */
	++p;
      }

      (void)WriteBuf_Add_String(buf, " ]", 2);
    }
    break;
#endif


#if !defined(XNOUSE_GPOWERSET_GENERATOR) || !defined(XNOUSE_STRING_GENERATOR)
  case type_SDL_GPowerset : /* --------------------------------------------- */
  case type_SDL_String :
    {
      xString_yptr p = ((xString_Type*)Address)->First;

      if (Sort->TypeClass == type_SDL_GPowerset) {
	(void)WriteBuf_Add_String(buf, "[ ", 2);
      }
#ifdef XITEXCOMM
      else if (USE_ITEX_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "(: ", 3);
      }
#endif
      else if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "{ ", 2);
      }
      else { /* SDL String */
	(void)WriteBuf_Add_String(buf, "(. ", 3);
      }

      if (p) {
	LocalGenericWriteSort(buf,
			       (void*)((xptrint)p +
				    ((tSDLGenListInfo*)Sort)->yrecDataOffset),
			       ((tSDLGenListInfo*)Sort)->CompSort,
			       Format);
	p = p->Suc;

	while (p) {
	  (void)WriteBuf_Add_String(buf, ", ", 2);
	  LocalGenericWriteSort(buf,
			       (void*)((xptrint)p +
				    ((tSDLGenListInfo*)Sort)->yrecDataOffset),
			       ((tSDLGenListInfo*)Sort)->CompSort,
			       Format);
	  p = p->Suc;
	}
      }

      if (Sort->TypeClass == type_SDL_GPowerset) {
	(void)WriteBuf_Add_String(buf, " ]", 2);
      }
#ifdef XITEXCOMM
      else if (USE_ITEX_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " :)", 3);
      }
#endif
      else if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " }", 2);
      }
      else { /* SDL String */
	(void)WriteBuf_Add_String(buf, " .)", 3);
      }

    }
    break;
#endif


#ifndef XNOUSE_BAG_GENERATOR
  case type_SDL_Bag : /* --------------------------------------------------- */
    {
      xBag_yptr p = ((xBag_Type*)Address)->First;

#ifdef XITEXCOMM
      if (USE_ITEX_SYNTAX(Format))
        (void)WriteBuf_Add_String(buf, "[ ", 2);
      else
#endif
      (void)WriteBuf_Add_String(buf, "{ ", 2);

      while (p) {
	LocalGenericWriteSort(buf,
			       (void*)((xptrint)p +
				     ((tSDLGenListInfo*)Sort)->yrecDataOffset),
			       ((tSDLGenListInfo*)Sort)->CompSort,
			       Format);
	if (p->NoOfItems > 1) {
          if ( ! USE_ASN1_SYNTAX(Format)
#ifdef XITEXCOMM
               && ! USE_ITEX_SYNTAX(Format)
#endif
               ) {
            (void)WriteBuf_Add_Fmt(buf, 12, ":%d", p->NoOfItems);
          }
          else {
            int i = p->NoOfItems;
            while ( --i > 0 ) {
              (void)WriteBuf_Add_String(buf, ", ", 2);
              LocalGenericWriteSort(buf,
			       (void*)((xptrint)p +
				     ((tSDLGenListInfo*)Sort)->yrecDataOffset),
			       ((tSDLGenListInfo*)Sort)->CompSort,
			       Format);
            }
          }
	}
	p = p->Suc;
        if ( p )
          (void)WriteBuf_Add_String(buf, ", ", 2);
      }

#ifdef XITEXCOMM
      if (USE_ITEX_SYNTAX(Format))
        (void)WriteBuf_Add_String(buf, " ]", 2);
      else
#endif
      (void)WriteBuf_Add_String(buf, " }", 2);
    }
    break;
#endif


#ifndef XNOUSE_LSTRING_GENERATOR
  case type_SDL_LString : /* ----------------------------------------------- */
    {
      int i_max = ((xLString_Type*)Address)->Length;
      xptrint p = (xptrint)Address + ((tSDLLStringInfo*)Sort)->DataOffset;
      xptrint dp = ((tSDLLStringInfo*)Sort)->CompSort->SortSize;
      int i;

      if (USE_SDL_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "(. ", 3);
      }
      else if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "{ ", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, "(: ", 3);
      }

      for (i = 0; i < i_max; ++i) {
	if (i > 0) {
	  (void)WriteBuf_Add_String(buf, ", ", 2);
	}

	LocalGenericWriteSort(buf, (void*)p, 
			       ((tSDLLStringInfo*)Sort)->CompSort, Format);
	p += dp;
      }

      if (USE_SDL_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " .)", 3);
      }
      else if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " }", 2);
      }
      else {
	(void)WriteBuf_Add_String(buf, " :)", 3);
      }
    }
    break;
#endif


  case type_SDL_Own : /* --------------------------------------------------- */
  case type_SDL_Oref :
  case type_SDL_Ref :
    if (USE_HEX_REF_SYNTAX(Format)) {
      (void)WriteBuf_Add_Fmt(buf, 20, "%p", *(void**)Address);
    }

    else if (*(void**)Address != NULL) {
      int i;

      i = indexNL(xLoopRefWriteList, *(void**)Address);
      if (i < 0) {
#ifndef XVALIDATOR_LIB
#ifdef XMONITOR
        if (XSYSD xSavingState &&
            xRefAddress(buf, *(void**)Address, xSymbolTableRoot)) {
          return;
        }
#endif
#endif
	addNL(&xLoopRefWriteList, *(void**)Address);
      }
      else {
	(void)WriteBuf_Add_Fmt(buf, 12, "old %d", i + 1);
	return;
      }

      if (!USE_DEREF_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "new ( ", 6);
      }

      LocalGenericWriteSort(buf, *(void**)Address, 
			     ((tSDLGenInfo*)Sort)->CompOrFatherSort, Format);

      if (!USE_DEREF_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, " )", 2);
      }

    }

    else {
      (void)WriteBuf_Add_String(buf, "Null", 4);
    }
    break;




  case type_SDL_Enum : /* -------------------------------------------------- */
  case type_SDL_ChoicePresent : /* ----------------------------------------- */
    {
      int i;
      const char * v;
      i = GenericGetValue(((tSDLEnumInfo*)Sort)->SortSize, Address);
      v = GetEnumName(
            i,
	    ((tSDLEnumInfo*)Sort)->LiteralList,
	    ((tSDLEnumInfo*)Sort)->NoOfLiterals,
	    "Enumeration type out of range");
      (void)WriteBuf_Add_String(buf, v, strlen(v));
    }
    break;


  case type_SDL_EmptyType : /* --------------------------------------------- */
    {
      if (USE_ASN1_SYNTAX(Format)) {
	(void)WriteBuf_Add_String(buf, "{ }", 3 );
      }
      else {
	(void)WriteBuf_Add_String(buf, "(. .)", 5 );
      }
    }
    break;


  case type_SDL_Voidstar : /* ---------------------------------------------- */
  case type_SDL_Voidstarstar : /* ------------------------------------------ */
    {
      (void)WriteBuf_Add_Fmt(buf, 20, "%p", *(void**)Address);
    }
    break;


  case type_SDL_Userdef : /* ----------------------------------------------- */
  case type_SDL_UnionC : /* ------------------------------------------------ */
    /* FALLTHROUGH */

  default: /* -------------------------------------------------------------- */
    {
      const unsigned char* p = (unsigned char*)Address;
      xptrint i_max = Sort->SortSize;
      xptrint i;
      (void)WriteBuf_Add_String(buf, "HEX(", 4);
      for (i = 0; i < i_max; ++i) {
	(void)WriteBuf_Add_Fmt(buf, 2, "%02x", *p);
	++p;
      }
      (void)WriteBuf_Add_Char(buf, ')');
    }
    break;
  }

}


/*---+---------------------------------------------------------------
     xWriteSort
-------------------------------------------------------------------*/
char *xWriteSort(void *In_Addr, xSortIdNode SortNode)
{
  int    index;
  static WriteBuf *buf[20];
  static int       WriteSortLevel = -1;

  if (WriteSortLevel >= 20)
    return "...";

  if (WriteSortLevel == -1) {
    WriteSortLevel = 0;
    for (index = 0; index < 20; index++)
      buf[index] = NULL;
  }
  WriteSortLevel++;
  if ( ! buf[WriteSortLevel-1] )
    buf[WriteSortLevel-1] = WriteBuf_New(2000);
  else
    WriteBuf_Clear(buf[WriteSortLevel-1]);
  xGenericWriteSort(buf[WriteSortLevel-1], In_Addr, SortNode->TypeInfo);
  WriteSortLevel--;
  return WriteBuf_Data(buf[WriteSortLevel]);
}


void xGenericWriteSort(
  WriteBuf     * buf,
  void         * Address,
  tSDLTypeInfo * Sort)
{
  int Format;
  Format = 0;
  if (XSYSD xUse_ASN1_Syntax)   Format = Format+USE_ASN1_SYNTAX_FLAG;
#ifdef XITEXCOMM
  if (XSYSD xUse_Itex_Syntax)   Format = Format+USE_ITEX_SYNTAX_FLAG;
#endif
  if (! XSYSD xUse_NewRef_Syntax) Format = Format+USE_HEX_REF_SYNTAX_FLAG;
  if (XSYSD xUse_DeRef_Syntax)  Format = Format+USE_DEREF_SYNTAX_FLAG;


  if (XSYSD WriteSortLevel >= 20) {
    (void)WriteBuf_Add_String(buf, "...", 3);
  }
  else {
    XSYSD WriteSortLevel++;
    LocalGenericWriteSort(buf, Address, Sort, Format);
    XSYSD WriteSortLevel--;

    if (
#ifndef XVALIDATOR_LIB
#ifdef XMONITOR
        ! XSYSD xSavingState &&
#endif
#endif
        (XSYSD WriteSortLevel == 0) && xLoopRefWriteList) {
      releaseNL((void ***)&xLoopRefWriteList);
    }
  }

  (void)WriteBuf_Terminate(buf);
  return;
}


void xxWriteSort(
  void         * Address,
  tSDLTypeInfo * Sort)
{
  static WriteBuf *buf = 0;
  if (!buf)
    buf = WriteBuf_New(1000);
  else
    WriteBuf_Clear(buf);

  xGenericWriteSort(buf, Address, Sort);
  xPrintString(WriteBuf_Data(buf));

  if (WriteBuf_Length(buf) > 5000)
    WriteBuf_Del(&buf);
}


void xWriteBuf_Fmt (const char* fmt, ...)
{
  va_list ap;
  int maxlen = 1;  /* for trailing \0 */
  int len;
  int Pos = 0;
  void * Param;
  int status;
  
  static WriteBuf *buf = 0;

  if (!buf)
    buf = WriteBuf_New(1000);
  else
    WriteBuf_Clear(buf);
  
  va_start(ap, fmt);
  while (fmt[Pos]) {
    if (fmt[Pos] == '%') {
      Pos++;
      len = 0;
      Param = va_arg(ap, void *);
      if (fmt[Pos] == '-' || fmt[Pos] == '+' ||
	  fmt[Pos] == '#' || fmt[Pos] == ' ') {
	Pos++;
      }
      if (fmt[Pos] == '*' || (fmt[Pos] >= '0' && fmt[Pos] <= '9')) {
	if (fmt[Pos] == '*') {
	  Pos++;
	  len = (int)Param;
	  Param = va_arg(ap, void *);
	} else if (fmt[Pos] >= '0' && fmt[Pos] <= '9') {
	  while (fmt[Pos] >= '0' && fmt[Pos] <= '9') {
	    len = 10*len + (int)fmt[Pos++] - '0';
	  }
	}
        if (fmt[Pos] == '.') {
	  Pos++;
	  while (fmt[Pos] >= '0' && fmt[Pos] <= '9') Pos++;
	}
      }

      if (fmt[Pos] == 'l' || fmt[Pos] == 'L' || fmt[Pos] == 'h') {
	Pos++;
      }

      if (fmt[Pos] == 's') {
	maxlen = maxlen + (len >= strlen((char *)Param) ? len : 
			   strlen((char *)Param));
      } else if (fmt[Pos] == 'd' || fmt[Pos] == 'i' || fmt[Pos] == 'u' ||
		 fmt[Pos] == 'o' || fmt[Pos] == 'x' || fmt[Pos] == 'X' ||
		 fmt[Pos] == 'p') {
	maxlen = maxlen + (len>=12 ? len : 12);
      } else if (fmt[Pos] == 'c' || fmt[Pos] == '%') {
	maxlen = maxlen + (len>=1 ? len : 1);
      } else if (fmt[Pos] == 'f' || fmt[Pos] == 'e' || fmt[Pos] == 'E' ||
		 fmt[Pos] == 'g' || fmt[Pos] == 'G') {
	maxlen = maxlen + (len>=24 ? len : 24);
      } else {
	maxlen = maxlen + 50;
      }
    }
    else {
      maxlen = maxlen + 1;
    }
    Pos++;
  }
  va_end(ap);
  WriteBuf_Secure(buf, maxlen);

  va_start(ap, fmt);
#if HAVE_VSNPRINTF
  status = vsnprintf(&(buf->rep->A[buf->rep->len]), maxlen, fmt, ap);
#else
  status = vsprintf(&(buf->rep->A[buf->rep->len]), fmt, ap);
#endif
  if ((status >= 0) && (status <= maxlen)) {
    buf->rep->len += status;
  }
  va_end(ap);

  WriteBuf_Terminate(buf);
  xPrintString(WriteBuf_Data(buf));
  if (WriteBuf_Length(buf) > 5000)
    WriteBuf_Del(&buf);
}


void xAddBuf_Fmt (WriteBuf* buf, const char* fmt, ...)
{
  va_list ap;
  int maxlen = 1;  /* for trailing \0 */
  int len;
  int Pos = 0;
  void * Param;
  int status;
  
  va_start(ap, fmt);
  while (fmt[Pos]) {
    if (fmt[Pos] == '%') {
      Pos++;
      len = 0;
      Param = va_arg(ap, void *);
      if (fmt[Pos] == '-' || fmt[Pos] == '+' ||
	  fmt[Pos] == '#' || fmt[Pos] == ' ') {
	Pos++;
      }
      if (fmt[Pos] == '*' || (fmt[Pos] >= '0' && fmt[Pos] <= '9')) {
	if (fmt[Pos] == '*') {
	  Pos++;
	  len = (int)Param;
	  Param = va_arg(ap, void *);
	} else if (fmt[Pos] >= '0' && fmt[Pos] <= '9') {
	  while (fmt[Pos] >= '0' && fmt[Pos] <= '9') {
	    len = 10*len + (int)fmt[Pos++] - '0';
	  }
	}
        if (fmt[Pos] == '.') {
	  Pos++;
	  while (fmt[Pos] >= '0' && fmt[Pos] <= '9') Pos++;
	}
      }

      if (fmt[Pos] == 'l' || fmt[Pos] == 'L' || fmt[Pos] == 'h') {
	Pos++;
      }

      if (fmt[Pos] == 's') {
	maxlen = maxlen + (len >= strlen((char *)Param) ? len : 
			   strlen((char *)Param));
      } else if (fmt[Pos] == 'd' || fmt[Pos] == 'i' || fmt[Pos] == 'u' ||
		 fmt[Pos] == 'o' || fmt[Pos] == 'x' || fmt[Pos] == 'X' ||
		 fmt[Pos] == 'p') {
	maxlen = maxlen + (len>=12 ? len : 12);
      } else if (fmt[Pos] == 'c' || fmt[Pos] == '%') {
	maxlen = maxlen + (len>=1 ? len : 1);
      } else if (fmt[Pos] == 'f' || fmt[Pos] == 'e' || fmt[Pos] == 'E' ||
		 fmt[Pos] == 'g' || fmt[Pos] == 'G') {
	maxlen = maxlen + (len>=24 ? len : 24);
      } else {
	maxlen = maxlen + 50;
      }
    }
    else {
      maxlen = maxlen + 1;
    }
    Pos++;
  }
  va_end(ap);
  WriteBuf_Secure(buf, maxlen);

  va_start(ap, fmt);
#if HAVE_VSNPRINTF
  status = vsnprintf(&(buf->rep->A[buf->rep->len]), maxlen, fmt, ap);
#else
  status = vsprintf(&(buf->rep->A[buf->rep->len]), fmt, ap);
#endif
  if ((status >= 0) && (status <= maxlen)) {
    buf->rep->len += status;
  }
  va_end(ap);

  WriteBuf_Terminate(buf);
}
#endif /* XREADANDWRITEF */



#ifdef XREADANDWRITEF
/****+***************************************************************
08   Generic read function
********************************************************************/


#ifdef XVALIDATOR_LIB
# ifndef XSYSD
#   define XSYSD
# endif
# define XNOTINCLUDEFILE (CommandFile->File == NULL)
#else
# define XNOTINCLUDEFILE (XSYSD CommandFile == NULL)
#endif



static void ** xRefReadList = NULL;
#define STR_VAR_LEN     (256)


#ifdef XMONITOR
void xReleasexRefReadList(void)
{
  releaseNL((void ***)&xRefReadList);
}
#endif


/*
 * Converts a hexadecimal digit ([0-9A-Za-z]) into an unsigned
 * value 0x00-0x0f OR 0xff if the digit is not valid.
 */
static unsigned char
LocalHexDigitToVal( const char c )
{
  if ( c >= '0' && (c <= '9' ) )
    return (unsigned char)( c - '0' );
  if ( c >= 'A' && (c <= 'F') )
    return (unsigned char)( c - 'A' + 10 );
  if ( c >= 'a' && (c <= 'f') )
    return (unsigned char)( c - 'a' + 10 );
  return 0xff;
}


#ifndef XNOUSEOFOCTETBITSTRING
/*
 * Bit string needs TWO buffers.  We reuse the one allredy on the stack 
 * as strVar2.
 */
static int
LocalRead_SDL_Bit_String( SDL_Bit_String * Result, char* strVar2 )
{
  xxToken Token;
  char    strVar[MAX_READ_LENGTH];
  int     i, len;
  unsigned value;
  
  Token = xPromptQuestionMark( " (bit_string) : "," (bit_string) : ", strVar);
  if (Token != xxString) {
    xPrintString("Bit_String value should be enclosed in '\n");
    return 0;
  }

  Token = xScanToken(strVar2);
  if ( Token != xxId || strlen(strVar2) != 1 || 
       ( strVar2[0] != 'b' && strVar2[0] != 'B' &&
         strVar2[0] != 'h' && strVar2[0] != 'H'
       )
     ) {
    xPrintString("Bit_String value should end with B or H\n");
    return 0;
  }

  if ( strVar2[0] == 'b' || strVar2[0] == 'B' ) {
    len = strlen(strVar);
    Result->Length = len-2;
    Result->IsAssigned = (xbool)1;
    Result->Bits = (unsigned char *)
               XALLOC( (xptrint)(len-2), &ySDL_SDL_Bit_String );
    for (i=1; i<len-1; i++) {
      if (strVar[i] == '0') {
        Result->Bits[i-1] = 0;
      }
      else if (strVar[i] == '1') {
        Result->Bits[i-1] = 1;
      }
      else {
        xPrintString(
                "Only 0 and 1 allowed in Bit_String value with ''B format\n");
        return 0;
      }
    }
  }

  else {
    len = strlen(strVar)-2;
    Result->Length = 4*len;
    Result->IsAssigned = (xbool)1;
    Result->Bits = (unsigned char *)
                XALLOC((xptrint)(4*len), &ySDL_SDL_Bit_String);
    for (i=1; i<=len; i++) {
      value = LocalHexDigitToVal( strVar[i] );
      if ( value == 0xff ) {
        xPrintString(
               "Only 0-9, A-F allowed in Bit_String value with ''H format\n");
        return 0;
      }
      Result->Bits[(i-1)*4+0] = (unsigned char)((value >> 3) & 1 );
      Result->Bits[(i-1)*4+1] = (unsigned char)((value >> 2) & 1 );
      Result->Bits[(i-1)*4+2] = (unsigned char)((value >> 1) & 1 );
      Result->Bits[(i-1)*4+3] = (unsigned char)((value >> 0) & 1 );
    }
  }
  return 1;
}
#endif



#ifndef XNOUSEOFOCTETBITSTRING
/*
 * Bit string needs TWO buffers.  We reuse the one allredy on the stack 
 * as strVar2.
 */
static int
LocalRead_SDL_Octet_String( void  * Result, char* strVar2 )
{
  xxToken Token;
  char    strVar[MAX_READ_LENGTH];
  int     i;
  unsigned len;
  unsigned char res;
  unsigned char * p;

  Token = xPromptQuestionMark( " (octet_string) : ", " (octet_string) : ",
                               strVar);
  if (Token != xxString) {
    xPrintString("Octet_String value should be enclosed in '\n");
    return 0;
  }

  Token = xScanToken(strVar2);
  if ( Token != xxId || strlen(strVar2) != 1 || 
       ( strVar2[0] != 'h' && strVar2[0] != 'H' &&
         strVar2[0] != 'o' && strVar2[0] != 'O' ) ) {
    xPrintString("Octet_String value should end with H\n");
    return 0;
  }

  len = strlen(strVar)-2;
  if ( len & 1 ) {                      /* If odd length append a '0' */
    strVar[++len] = '0';
    strVar[len+1] = '\'';
    strVar[len+2] = '\0';
  }

  (*(SDL_Octet_String *)Result).Length = len >> 1;
  (*(SDL_Octet_String *)Result).IsAssigned = (xbool)1;
  (*(SDL_Octet_String *)Result).Bits = (unsigned char *)
           XALLOC((xptrint)(len >> 1), &ySDL_SDL_Octet_String);
  p = (*(SDL_Octet_String *)Result).Bits;

  for ( i = 1; (unsigned)i < len; i += 2) {
    res = LocalHexDigitToVal( strVar[i]   );
    *p  = LocalHexDigitToVal( strVar[i+1] );
    if ( res == 0xff || *p == 0xff ) {
      xPrintString(
               "Octet_String value should consist of HEX values: 0-9, A-F\n");
      return 0;
    }
    *p |= res << 4;
    ++p;
  }
  return 1;
}
#endif


/*
 * This also uses a BIG buffer.
 *      Returns: 0 Read failed.
 *               1 Read success.
 *               2 Not Read, end bracket is next token.
 *               3 Not Read, comma is next token.  (or EOF)
 */
int
xGenericReadOneParameter( tSDLTypeInfo* Sort, char* Prompt,
                       void* ResultAddr )
{
  xxToken     Token;
  char        strVar[MAX_READ_LENGTH];

  Token = xScanToken(strVar);
  if (Token != xxComma)
    xUngetToken(Token, strVar);

  Token = xPromptQuestionMark( Prompt, 0, strVar);
  if (Token == xx2QuestionMark ) {
    /* We can not get the fully qualified name here! */
    /* Just print the prompt and current value: " (name) = value : " */
    WriteBuf* buf = WriteBuf_New( 32 );
    (void)WriteBuf_Add_String( buf, " (", 2 );
#ifdef T_SDL_NAMES
    (void)WriteBuf_Add_String( buf, Sort->Name, 0 );
#else
    (void)WriteBuf_Add_String( buf, "?", 1 );
#endif
    (void)WriteBuf_Add_String( buf, ") = ", 4 );
    xGenericWriteSort( buf, ResultAddr, Sort );
    (void)WriteBuf_Add_String( buf, " : ", 3 );
    (void)WriteBuf_Terminate( buf );
    xPrintString( WriteBuf_Data(buf) );
    WriteBuf_Del( &buf );
  }
  else {
    xUngetToken(Token, strVar);
  }
  if (Token == xxRParDot || Token == xxRBracket ||
#ifdef XITEXCOMM
      Token == xxRParColon ||
#endif
      Token == xxRCurlyBracket) return 2;
  if (Token == xxComma) return 3;
  if (Token == xxEOF) return 3;  /* Treat xxEOF the same way as xxComma */

  if (xGenericReadSort(ResultAddr, Sort)) {
    return 1;
  }
  return 0;
}


static int
LocalGenericReadSortHex( void* Address, tSDLTypeInfo* Sort, char* strVar )
{
  char          Prompt[ STR_VAR_LEN ];
  xxToken       Token;
  unsigned char* a = (unsigned char *)Address;
  unsigned char* a_max = a + Sort->SortSize;
  unsigned char* p = (unsigned char*)strVar;


  sprintf(Prompt, " (%s in HEX) : ", Sort->Name );
  Token = xPromptQuestionMark(Prompt, Prompt, strVar);

  if (Token == xxId && strcmp(strVar, "HEX") == 0)
    Token = xScanToken(strVar);
  if (Token == xxLPar)
    Token = xScanToken(strVar);

  if (Token == xxId) {
    while ( *p != '\0' && a < a_max ) {
      unsigned char c1, c0;

      c1 = LocalHexDigitToVal( *p );
      if ( c1 == 0xff ) {
        sprintf(Prompt, "Illegal character (%c) in HEX string\n", *p );
        xPrintString(Prompt);
        return 0;
      }
      ++p;

      c0 = LocalHexDigitToVal( *p );
      if ( c0 == 0xff ) {
          sprintf(Prompt, "Illegal character (%c) in HEX string\n", *p );
          xPrintString(Prompt);
          return 0;
      }
      ++p;

      *a = (c1 << 4) | c0;
      ++a;
    } /* while */
  
    if ( *p != '\0' ) {
      xPrintString("HEX string too long\n");
      return 0;
    }

    if ( ( a < a_max ) &&
         ( ( Sort->TypeClass == type_SDL_Ref  ) ||
           ( Sort->TypeClass == type_SDL_Own  ) ||
           ( Sort->TypeClass == type_SDL_Oref ) ) ) {
      xPrintString("HEX string too short\n");
      return 0;
    }
  }

  else {
    xPrintString("Illegal start of HEX string\n");
    return 0;
  }

  Token = xScanToken(strVar);
  if (Token != xxRPar)
    xUngetToken(Token, strVar);

  return 1;
}



static int
LocalGenericReadSort( void * Address, tSDLTypeInfo* Sort )
{
  xxToken       Token;
  char          strVar[MAX_READ_LENGTH];
  int           i;

  /*
   * Handle types with read functions.
   * Also traverse syntypes and inherited types.
   */
  for(;;) {
#ifndef XNOUSE_OPFUNCS
    if (Sort->OpFuncs && Sort->OpFuncs->ReadFunc) {
      return (Sort->OpFuncs->ReadFunc)(Address);
    }
#endif
    if ( Sort->TypeClass == type_SDL_Syntype ||
         Sort->TypeClass == type_SDL_Inherits ) {
      Sort = ((tSDLGenInfo*)Sort)->CompOrFatherSort;
    }
    else {
      break;
    }
  }


  switch ( Sort->TypeClass ) {

  /*SDL - standard types*/
  case type_SDL_Integer : /* ----------------------------------------------- */
    {
      Token = xPromptQuestionMark( " (integer) : ", " (integer) : ", strVar );
      if ( Token == xxPlus ) {
        Token = xScanToken( strVar );
      }
      else if ( Token == xxMinus ) {
        strVar[0] = '-';
        Token = xScanToken( strVar+1 );
      }
      if (Token != xxId) {
        xPrintString( "Illegal Integer value\n" );
        return 0;
      }

      if ( sscanf( strVar, XSCAN_INT_FORMAT, (SDL_Integer*)Address ) >= 1 ) {
        return 1;
      }

      xPrintString( "Illegal Integer value\n" );
      return 0;
    }
    /* NOTREACHED */
    break;


#ifndef XNOUSEOFREAL
  case type_SDL_Real : /* -------------------------------------------------- */
    {
      Token = xPromptQuestionMark( " (real) : ", " (real) : ", strVar );

      if ( Token != xxLCurlyBracket ) { /* SDL Syntax */

        if ( Token == xxPlus ) {
          Token = xScanToken( strVar );
        }

        else if ( Token == xxMinus ) {
          strVar[0] = '-';
          Token = xScanToken( strVar + 1 );
        }

        if ( Token != xxId ) {
          xPrintString( "Illegal Real value\n" );
          return 0;
        }

        if ( sscanf( strVar, "%lf", (SDL_Real*)Address ) >= 1 ) {
          return 1;
        }
      }

      else {                            /* ASN.1 Syntax */
        /* '{' 'mantissa'? '-'? ('0'|[1-9][0-9]*) , base 10, exponent 2 } */
        int     mantissa = 0;
        int     base = 10;
        int     exponent = 0;
        SDL_Real val = 1.0;

        Token = xScanToken(strVar);

        /* 'mantissa' */
        if ( Token == xxId && !strcmp(strVar, "mantissa") )
          Token = xScanToken(strVar);

        /* '-'? */
        if (Token == xxMinus) {
          strVar[0] = '-';
          Token = xScanToken(strVar+1);
        }

        /* ('0'|[1-9][0-9]*) */
        if ( Token != xxId || ( sscanf(strVar,"%d",&mantissa) < 1) ) {
          xPrintString( "Illegal syntax, word mantissa or integer value expected.\n");
          return 0;
        }

        Token = xScanToken(strVar);

        /* ','? */
        if ( Token == xxComma ) Token = xScanToken(strVar);

        /* 'base'? */
        if ( Token == xxId && !strcmp(strVar, "base") )
          Token = xScanToken(strVar);

        if ( Token != xxId || ( sscanf(strVar,"%d",&base) < 1) ||
             (base != 2 && base != 10) ) {
          xPrintString( "Illegal syntax, word base or 2 or 10 expected.\n");
          return 0;
        }
        Token = xScanToken( strVar );

        /* ','? */
        if ( Token == xxComma ) Token = xScanToken(strVar);

        /* 'exponent'? */
        if ( Token == xxId && !strcmp(strVar, "exponent") )
          Token = xScanToken(strVar);

        /* '-'? */
        if (Token == xxMinus) {
          strVar[0] = '-';
          Token = xScanToken(strVar+1);
        }

        if (Token != xxId || sscanf(strVar, "%d", &exponent) < 1) {
          xPrintString("Illegal syntax, word exponent or integer value expected.\n");
          return 0;
        }

        /* Check trailing } */
        Token = xScanToken(strVar);
        if (Token != xxRCurlyBracket) xUngetToken(Token, strVar);


        while ( exponent < 0 ) {
          ++exponent;
          val /= base;
        }
        while ( exponent > 0 ) {
          --exponent;
          val *= base;
        }

        *(SDL_Real*)Address = val * mantissa;
        return 1;
      }
      xPrintString( "Illegal Real value\n" );
      return 0;
    }
    /* NOTREACHED */
    break;
#endif

  case type_SDL_Natural : /* ----------------------------------------------- */
    {
      Token = xPromptQuestionMark( " (natural) : ", " (natural) : ", strVar );
      if ( Token == xxPlus ) {
        Token = xScanToken( strVar );
      }
      else if ( Token == xxMinus ) {
        strVar[0] = '-';
        Token = xScanToken( strVar+1 );
      }
      if (Token != xxId) {
        xPrintString( "Illegal Natural value\n" );
        return 0;
      }

      if ( (sscanf( strVar, XSCAN_INT_FORMAT, (SDL_Natural*)Address ) >= 1) &&
           ( *(SDL_Natural*)Address >= 0 ) ) {
        return 1;
      }

      xPrintString( "Illegal Natural value\n" );
      return 0;
    }
    /* NOTREACHED */
    break;


  case type_SDL_Boolean : /* ----------------------------------------------- */
    {
      Token = xPromptQuestionMark( " (boolean) : ", " (boolean) : ", strVar );

      /* Turn into UPPER case */
      for ( i = 0; strVar[i]; ++i ) {
        if ( (strVar[i] >= 'a') && (strVar[i] <= 'z') ) {
          strVar[i] -= 'a';
          strVar[i] += 'A';
        }
      }

      if ( !strcmp( "TRUE", strVar ) || !strcmp( "T", strVar ) ) {
        *(SDL_Boolean *)Address = SDL_True;
        return 1;
      }
      
      else if ( !strcmp( "FALSE", strVar ) || !strcmp( "F", strVar ) ) {
        *(SDL_Boolean *)Address = SDL_False;
        return 1;
      }

      xPrintString( "Illegal Boolean value\n" );
      return 0;
    }
    /* NOTREACHED */
    break;


  case type_SDL_Character : /* --------------------------------------------- */
    {
      Token = xPromptQuestionMark( " (character) : ", " (character) : ",
                                   strVar);
      switch ( Token ) {

      case xxId:
        if ( strVar[1] == '\0' ) {
          *(SDL_Character*)Address = strVar[0];
          return 1;
        }
        else {
          for ( i = 0; i <= 64; ++i ) {
            if ( !strcmp( strVar, xfCharacterLiteral[i] ) ) {
              *(SDL_Character*)Address = (SDL_Character)( i < 32 ? 
                                                          i : i - 32 + 127 );
              return 1;
            }
          }
        }
        break;

      case xxString:                    /* 'x' or '\x' */
        i = strlen( strVar );
        if ( i == 3 ||
             ( i == 4 && strVar[1] == '\'' && strVar[2] == '\'' ) ) {
          *(SDL_Character*)Address = strVar[1];
          return 1;
        }
        break;

      case xxASN1String:                /* "x" or """" */
        i = strlen(strVar);
        if (i == 3 ||
            (i == 4 && strVar[1] == '\"' && strVar[2] == '\"') ) { /*"*/
          *(SDL_Character *)Address = strVar[1];
          return 1;
        }
        break;

      case xxSlash:             *(SDL_Character*)Address = '/'; return 1;
      case xxColon:             *(SDL_Character*)Address = ':'; return 1;
      case xxMinus:             *(SDL_Character*)Address = '-'; return 1;
      case xxPlus:              *(SDL_Character*)Address = '+'; return 1;
      case xxStar:              *(SDL_Character*)Address = '*'; return 1;
      case xxComma:             *(SDL_Character*)Address = ','; return 1;
      case xxSemicolon:         *(SDL_Character*)Address = ';'; return 1;
      case xxArrowHead:         *(SDL_Character*)Address = '^'; return 1;
      case xxLPar:              *(SDL_Character*)Address = '('; return 1;
      case xxRPar:              *(SDL_Character*)Address = ')'; return 1;
      case xxDot:               *(SDL_Character*)Address = '.'; return 1;
      case xxRBracket:          *(SDL_Character*)Address = ']'; return 1;
      case xxLCurlyBracket:     *(SDL_Character*)Address = '{'; return 1;
      case xxRCurlyBracket:     *(SDL_Character*)Address = '}'; return 1;
      case xxAt:                *(SDL_Character*)Address = '@'; return 1;
      case xxQuestionMark:      *(SDL_Character*)Address = '?'; return 1;
      case xxExclMark:          *(SDL_Character*)Address = '!'; return 1;
        /* Used as cancel in SImUI!
      case xxLBracket:          *(SDL_Character)Address = '[';  return 1;
        */

      default:
        break;
      }
      xPrintString("Illegal character value\n");
      return 0;
    }
    /* NOTREACHED */
    break;


  case type_SDL_Time : /* -------------------------------------------------- */
    {                                   /* ('0'|[1-9][0-9]*)'.' */
      SDL_Time val;
      Token = xPromptQuestionMark( " (time) : ", " (time) : ", strVar );
      xUngetToken( Token, strVar );
      i = xReadSDL_Time( &val );
      if ( i )
        *(SDL_Time*)Address = val;
      return i;
    }
    /* NOTREACHED */
    break;


  case type_SDL_Duration : /* ---------------------------------------------- */
    {
      SDL_Duration val;
      Token = xPromptQuestionMark(" (duration) : ", " (duration) : ", strVar );
      xUngetToken( Token, strVar );
      i = xReadSDL_Time( &val );
      if ( i )
        *(SDL_Duration*)Address = val;
      return i;
    }
    /* NOTREACHED */
    break;


  case type_SDL_Pid : /* --------------------------------------------------- */
#ifdef XVALIDATOR_LIB
    return xRead_SDL_PId(Address);
#else
    {
      xPrsNode    PrsNode;
      xPrsIdNode  ProcessId;

#ifdef XPMCOMM
      if ( XSYSD xRaW_use_Global_PId ) {
        Token = xScanToken(strVar);
        if ( Token != xxId ) return 0;
        if ( sscanf(strVar, "%d", &(((SDL_PId *)Address)->GlobalNodeNr)) != 1 )
          return 0;
        Token = xScanToken(strVar);
        if ( Token != xxId ) return 0;
#ifdef X_XPTRINT_LONG
        if ( sscanf(strVar, "%lu", (xptrint*)&(((SDL_PId *)Address)->LocalPId)) != 1 )
          return 0;
#else
        if ( sscanf(strVar, "%u", (xptrint*)&(((SDL_PId *)Address)->LocalPId)) != 1 )
          return 0;
#endif
        return 1;
      }
#endif

      PrsNode = xReadProcess( " (PId) : ", &ProcessId, (xIdNode)0);
      if (ProcessId == xNullId) {
        *(SDL_PId *)Address = SDL_NULL;
        return 1;
      }
      if (PrsNode == (xPrsNode)0)
        return 0;
      *(SDL_PId *)Address = PrsNode->Self;
      return 1;
    }
#endif
    /* NOTREACHED */
    break;


  case type_SDL_Charstring : /* -------------------------------------------- */
    return xRead_SDL_Charstring( Address );


  case type_SDL_Bit : /* --------------------------------------------------- */
    Token = xScanToken(strVar);
    if (Token==xxString) {
      /* ASN.1 syntax: 'b'B */
      if ( strlen( strVar ) != 3 ) {
        xPrintString("Bit length should be 1\n");
        return 0;
      }
      else if ( strVar[1] == '1' ) {
        i = 1;
      }
      else if ( strVar[1] == '0' ) {
        i = 0;
      }
      else {
        xPrintString("Bit value should should be '0' or '1'\n");
        return 0;
      }

      /* Check trailing 'B' */
      Token = xScanToken(strVar);
      if (Token != xxId || strlen(strVar) != 1 || 
          (strVar[0] != 'b' && strVar[0] != 'B') ) {
        xPrintString("Bit value should end with 'B'\n");
        return 0;
      }

      *(SDL_Bit*)Address = (SDL_Bit)i;
      return 1;

    } 
    else {
      /* SDL syntax */
      xUngetToken(Token, strVar);
      Token = xPromptQuestionMark( " (bit) : ", " (bit) : ", strVar );
      if ( (Token == xxId) && (sscanf(strVar, "%d", &i) >= 1) &&
           ( i==0 || i==1 ) ) {
        *(SDL_Bit *)Address = (SDL_Bit)i;
        return 1;
      }
      xPrintString("Bit value should be 0 or 1\n");
      return 0;
    }
    /* NOTREACHED */
    break;


#ifndef XNOUSEOFOCTETBITSTRING
  case type_SDL_Bit_string : /* -------------------------------------------- */
    return LocalRead_SDL_Bit_String( (SDL_Bit_String*)Address, strVar );
#endif


#ifndef XNOUSEOFOCTETBITSTRING
  case type_SDL_Octet : /* ------------------------------------------------- */
    {
      SDL_Octet val1 = 0;
      SDL_Octet val2 = 0;

      Token = xScanToken(strVar);
      if (Token==xxString) {
        /* ASN.1 syntax */

        /* Assign result */
        if (strlen(strVar)==4) {

          val1 = LocalHexDigitToVal( strVar[1] );
          val2 = LocalHexDigitToVal( strVar[2] );

          if ( val1 == 0xff || val2 == 0xff ) {
            xPrintString(
                      "Octet value should consist of HEX values: 0-9, A-F\n");
            return 0;
          }
          val2 |= val1 << 4;

        } else {
          xPrintString("Octet length should be 2\n");
          return 0;
        }

        /* Check 'H' */
        Token = xScanToken(strVar);
        if ( Token != xxId || strlen(strVar) != 1 || 
             ( strVar[0] != 'h' && strVar[0] != 'H' &&
               strVar[0] != 'o' && strVar[0] != 'O' ) ) {
          xPrintString("Octet value should end with 'H'\n");
          return 0;
        }
        *(SDL_Octet *)Address = val2;
        return 1;
      }

      else {
        /* SDL syntax */
        xUngetToken(Token, strVar);
        Token = xPromptQuestionMark( " (octet) : ", " (octet) : ", strVar);
        if ( Token == xxId ) {
          i = strlen( strVar );
          if ( i == 1 ) {
            val2 = LocalHexDigitToVal( strVar[0] );
          }
          else if ( i == 2 ) {
            val1 = LocalHexDigitToVal( strVar[0] );
            val2 = LocalHexDigitToVal( strVar[1] );
          }
          else {
            val1 = 0xff;                /* mark bad */
          }
          if ( val1 != 0xff && val2 != 0xff ) {
            val2 += val1 << 4;
            *(SDL_Octet *)Address = val2;
            return 1;
          }
        }
        xPrintString("Illegal Octet value. It should be two HEX values.\n");
        return 0;
      }
    }
    /* NOTREACHED */
    break;
#endif


#ifndef XNOUSEOFOCTETBITSTRING
  case type_SDL_Octet_string : /* ------------------------------------------ */
    return LocalRead_SDL_Octet_String( (SDL_Bit_String*)Address, strVar );
#endif


  case type_SDL_IA5String : /* --------------------------------------------- */
    return xRead_SDL_Charstring( Address );


  case type_SDL_NumericString : /* ----------------------------------------- */
    i = xRead_SDL_Charstring( Address );
#ifdef XITEXCOMM
    /* Silently consume any trailing 'N' */
    if ( i ) {
      Token = xScanToken(strVar);
      if (Token != xxId || strlen(strVar) != 1 || strVar[0] != 'N')
        xUngetToken(Token, strVar);
    }
#endif
    return i;

    
  case type_SDL_PrintableString : /* --------------------------------------- */
    i = xRead_SDL_Charstring( Address );
#ifdef XITEXCOMM
    /* Silently consume any trailing 'P' */
    if ( i ) {
      Token = xScanToken(strVar);
      if (Token != xxId || strlen(strVar) != 1 || strVar[0] != 'P')
        xUngetToken(Token, strVar);
    }
#endif
    return i;


  case type_SDL_VisibleString : /* ----------------------------------------- */
    i = xRead_SDL_Charstring( Address );
#ifdef XITEXCOMM
    /* Silently consume any trailing 'V' */
    if ( i ) {
      Token = xScanToken(strVar);
      if (Token != xxId || strlen(strVar) != 1 || strVar[0] != 'V')
        xUngetToken(Token, strVar);
    }
#endif
    return i;


  case type_SDL_NULL : /* -------------------------------------------------- */
    Token = xPromptQuestionMark( " (null) : ", " (null) : ", strVar );
    if (Token == xxNull) {
      *(SDL_Null *)Address = SDL_NullValue;
      return 1;
    }
    xPrintString("Illegal Null value. Null is the only legal value.\n");
    return 0;


#ifndef XNOUSEOFOBJECTIDENTIFIER
  case type_SDL_Object_identifier : /* ------------------------------------- */
    {

      (*(SDL_Object_Identifier *)Address).First = 0;
      (*(SDL_Object_Identifier *)Address).Last = 0;
      (*(SDL_Object_Identifier *)Address).Length = 0;
      (*(SDL_Object_Identifier *)Address).IsAssigned = 0;

      Token = xScanToken(strVar);
      if (XVALUE_SYNTAX &&
#ifdef XITEXCOMM
          Token != xxLParColon &&               /* (: */
#endif
          Token != xxLParDot &&                 /* (. */
          Token != xxLCurlyBracket &&           /* {  */
          Token != xxQuestionMark &&            /* ?  */
          Token != xxEoln) {                    /* \n */
        xPrintString("String value should start with (. or {\n");
        return 0;
      }

      /* Put back any ? or \n */
      if (
#ifdef XITEXCOMM
          Token != xxLParColon &&               /* (: */
#endif
          Token != xxLParDot &&                 /* (. */
          Token != xxLCurlyBracket ) {          /* {  */
        xUngetToken(Token, strVar);
      }


      while (
#ifdef XITEXCOMM
             Token != xxRParColon &&            /* :) */
#endif
             Token != xxRParDot &&              /* .) */
             Token != xxRCurlyBracket) {
        SDL_Natural Item = 0;

        i = xGenericReadOneParameter( &ySDL_SDL_Natural,
                                   " (natural) : ", (void *)&Item);
        if (i == 0) {                           /* failed */
          return 0;
        }
        else if (i == 2) {                      /* .) or ] or } */
          Token = xScanToken(strVar);
          if (
#ifdef XITEXCOMM
              Token == xxRParColon ||
#endif
              Token == xxRParDot ||
              Token == xxRCurlyBracket) {
            break;
          }
          return 0;
        }
        else if ( i == 1 ||                     /* A value was read! */
                  i == 3 ) {                    /* A ',' is short for default*/
          yAppend_SDL_Object_Identifier((SDL_Object_Identifier *)Address,
                                        &Item);
        }
        Token = xScanToken(strVar);
        if (
#ifdef XITEXCOMM
            Token != xxRParColon &&
#endif
            Token != xxRParDot &&
            Token != xxRCurlyBracket)
          xUngetToken(Token, strVar);
      }
      (*(SDL_Object_Identifier *)Address).IsAssigned = 1;
    }
    return 1;
#endif


  /* SDL - user defined types */
  case type_SDL_Syntype : /* ----------------------------------------------- */
  case type_SDL_Inherits : /* ---------------------------------------------- */
    /* NOTREACHED: Hanlded above */
    return 0;


  case type_SDL_Enum : /* -------------------------------------------------- */
    {

      Token = xScanToken(strVar);
      if ( Token == xxEoln ) {
        if (XSYSD xSaveLine != (char *)0) return 0;
        if (XNOTINCLUDEFILE) {
          /* If reading include file do no ouput Prompt
             to prevent UI from trigger on ": " in Prompt */
          sprintf(strVar, " (%s) : ", Sort->Name);
          xPrintString(strVar);
        }
        Token = xScanToken(strVar);
      }

      if (Token == xxMinus) {
        xPrintString("Default name not allowed\n");
        return 0;
      }

      while ( Token == xxQuestionMark || Token == xxEoln ) {
        WriteBuf* buf = WriteBuf_New(16);
        for ( i = 0; i < ((tSDLEnumInfo*)Sort)->NoOfLiterals; ++i ) {
          WriteBuf_Add_String( buf, 
                            ((tSDLEnumInfo*)Sort)->LiteralList[i].LiteralName,
                               0 );
          WriteBuf_Add_Char(buf, ' ' );
          WriteBuf_Terminate( buf );
          xPrintString( WriteBuf_Data( buf ) );
          WriteBuf_Clear( buf );
        }
        xPrintString( ": " );
        WriteBuf_Del( &buf );
        if (Token != xxEoln) 
          xSkipLine();
        Token = xScanToken(strVar);

        if (Token == xxMinus) {
          xPrintString("Default name not allowed\n");
          return 0;
        }
      }

      i = GetEnumIndex( strVar, 
                        ((tSDLEnumInfo*)Sort)->LiteralList,
                        ((tSDLEnumInfo*)Sort)->NoOfLiterals );
      if ( i == -1 ) {
        xPrintString("Name was not found\n" );
        return 0;
      }
      else if ( i == -2 ) {
        xPrintString("Name was ambiguous\n" );
        return 0;
      }

      i = ((tSDLEnumInfo*)Sort)->LiteralList[i].LiteralValue;
      GenericSetValue( ((tSDLEnumInfo*)Sort)->SortSize, Address, i );
    }
    return 1;


#ifndef XNOUSE_STRUCT
  case type_SDL_Struct : /* ------------------------------------------------ */
    {
      int i_max = ((tSDLStructInfo*)Sort)->NumOfComponents;
      tSDLFieldInfo* FieldSort = ((tSDLStructInfo*)Sort)->Components;
      int status;
      void*     p;
      int bf_val = 0;

      Token = xScanToken(strVar);
      if ( XVALUE_SYNTAX &&
           Token != xxLParDot && 
           Token != xxLCurlyBracket &&
           Token != xxQuestionMark &&
           Token != xxEoln ) {
        xPrintString("Struct value should start with (. or {\n");
        return 0;
      }
      if ( Token != xxLParDot && 
           Token != xxLCurlyBracket)
        xUngetToken(Token, strVar);

      /* Make all optional fields NOT present */
      for ( i = 0; i < (unsigned)i_max; ++i ) {
        if ( FieldSort[i].Offset != ~0 &&
             FieldSort[i].ExtraInfo != 0 &&
             FieldSort[i].ExtraInfo->OffsetPresent != 0 ) {
          *(SDL_Boolean*)( (xptrint)Address +
                          FieldSort[i].ExtraInfo->OffsetPresent ) = SDL_False;
        }
      }

      i = 0;  /* i==0 -> use SDL syntax, i==1 -> use ASN.1 syntax */
      if (XASN1_SYNTAX) i = 1;
      if (Token == xxLParDot) i = 0;
      if (Token == xxLCurlyBracket) i = 1;

      /*SDL*/
      if ( i == 0 ) {

        for ( i = 0; i < i_max; ++i ) {

          /* Find the address of the item to read in the struct */
          if ( FieldSort[i].Offset != (xptrint)~0 ) {
            /* NOT a bit field */
            p = (void*)( (xptrint)Address + FieldSort[i].Offset );

            /* If item has a default value assign that first. */
            if ( FieldSort[i].ExtraInfo != 0 &&
                 FieldSort[i].ExtraInfo->DefaultValue != 0  ) {
              /* Initialise with default value */
              (void)GenericAssignSort( p,
                                       FieldSort[i].ExtraInfo->DefaultValue,
                                       XASS_AC_ASS_FR,
                                       FieldSort[i].CompSort );
            }

          }
          else {
            /* Bit field */
            p = (void*) &bf_val;
          }

          if ( i == 0 ) {
            Token = xScanToken(strVar);
            xUngetToken(Token, strVar);
            if (Token == xxComma)
              continue;    /* optional component not present */
          }

          /* Build a prompt */
          sprintf(strVar, " %s (%s) : ", 
                  FieldSort[i].Name,             /* Name of item */
                  FieldSort[i].CompSort->Name ); /* Name of item type */

          status = xGenericReadOneParameter( FieldSort[i].CompSort, strVar, p);

          if (status == 0) return 0;                    /* Failed */
          if (status == 1) {                            /* Assigned */
            if ( FieldSort[i].Offset == (xptrint)~0 ) {
              /* Real assign of bit fields */
              ((tSDLFieldBitFInfo*)
               (FieldSort[i].ExtraInfo))->AssTag(Address,bf_val);
            }
            /* Handle present field if optional component */
            else if ( FieldSort[i].Offset != (xptrint)~0 &&
                 FieldSort[i].ExtraInfo != 0 &&
                 FieldSort[i].ExtraInfo->OffsetPresent != 0 ) {
              *(SDL_Boolean*)( (xptrint)Address +
                            FieldSort[i].ExtraInfo->OffsetPresent ) = SDL_True;
            }
          }
        } /* for */

        Token = xScanToken(strVar);
        if (Token != xxRParDot)
          xUngetToken(Token, strVar);
        return 1;
      } /* SDL */

      /*ASN.1*/
      for (;;) {
        int component = 0;

        Token = xScanToken(strVar);
        if ( Token == xxEoln ) {
          if (XSYSD xSaveLine != (char *)0) return 0;
          if (XNOTINCLUDEFILE) {
            /* If reading include file do no ouput Prompt
               to prevent UI from trigger on ": " in Prompt */
            xPrintString("Component : ");
          }
        }
        else {
          xUngetToken( Token, strVar );
        }

        /* Find which component to read */
        for(;;) {

          Token = xScanToken(strVar);

          if ( Token == xxComma ) {             /* ','  IGNORED */
            /* IGNORED */
          }

          else if ( Token == xxMinus ) {        /* '-'  ILLEGAL */
            xPrintString("Default name not allowed\n");
            return 0;
          }

          else if ( Token == xxRCurlyBracket ) { /* '}' DONE */
            return 1;
          }

          else if ( Token == xxEoln ||          /* '\n' Prompt */
                    Token == xxQuestionMark ) { /* '?'  Prompt and skip line */
            /* List all component names */
            WriteBuf* buf = WriteBuf_New( 32 );
            for ( i = 0; i < i_max; ++i ) {
              (void)WriteBuf_Add_String( buf, FieldSort[i].Name, 0 );
              (void)WriteBuf_Add_Char( buf, ' ' );
              (void)WriteBuf_Terminate( buf );
              xPrintString( WriteBuf_Data( buf ) );
              WriteBuf_Clear( buf );
            }
            WriteBuf_Del( &buf );
            xPrintString( ": " );
            /* Ignore rest of input line */
            if ( Token != xxEoln )
              xSkipLine();
          }

          else {                                /* *    turn into a component*/
            /* Match name to components */
            int len = strlen( strVar );
            if ( len >= 0 ) {
              int count = 0;
              int i_sav = 0;
              for ( i = 0; i < i_max; ++i ) {
                if ( !strncasecmp( strVar, FieldSort[i].Name, len ) ) {
                  ++count;
                  if ( count > 1 ) {
                    if (FieldSort[i].Name[len] == '\0') { /* Exact match */
                      count--;
                      i_sav = i;
                    }
                    else if (FieldSort[i_sav].Name[len] == '\0') { /* Exact match */
                      count--;
                    }
                  }
                  else {
                    i_sav = i;
                  }
                }
              }
              if ( count == 0 ) {
                xPrintString( "Name was not found\n" );
                return 0;
              }
              else if ( count != 1 ) {
                xPrintString( "Name was ambiguous\n" );
                return 0;
              }
              component = i_sav;
              break;
            }
          }
        }

        /* Read component */
        /* .. Make prompt 'name (type-name) : ' */
        sprintf(strVar, " %s (%s) : ", 
                FieldSort[component].Name,               /* Name of item */
                FieldSort[component].CompSort->Name ); /* Name of item type */

        /* Find the address of the item to read in the struct */
        if ( FieldSort[component].Offset != (xptrint)~0 ) {
          /* NOT a bit field */
          p = (void*)( (xptrint)Address + FieldSort[component].Offset );
        }
        else {
          /* Bit field */
          p = (void*) &bf_val;
        }

        status = xGenericReadOneParameter( FieldSort[component].CompSort,
                                        strVar, p );

        if (status != 1) return 0;                      /* Failed */

        if ( FieldSort[component].Offset == (xptrint)~0 ) {
          /* Real assign of bit fields */
          ((tSDLFieldBitFInfo*)
           (FieldSort[component].ExtraInfo))->AssTag(Address,bf_val);
        }
        /* Handle present field if optional component */
        else if ( FieldSort[component].Offset != (xptrint)~0 &&
                  FieldSort[component].ExtraInfo != 0 &&
                  FieldSort[component].ExtraInfo->OffsetPresent != 0 ) {
          *(SDL_Boolean*)( (xptrint)Address +
                    FieldSort[component].ExtraInfo->OffsetPresent ) = SDL_True;
        }
      }
    }
    break;
#endif


#if !defined(XNOUSE_CHOICE) || !defined(XNOUSE_UNION)
  case type_SDL_Union : /* ------------------------------------------------- */
  case type_SDL_Choice : /* ------------------------------------------------ */
    {
      xbool Opening = (xbool)0;         /* flag to indicate starting {/(. */
      int i_max = ((tSDLChoiceInfo*)Sort)->NumOfComponents;
      tSDLChoiceFieldInfo* FieldSort = ((tSDLChoiceInfo*)Sort)->Components;
      int tag = -1;
      int len;

      if (Sort->TypeClass == type_SDL_Union) {  /* unions may start with a */
        Token = xScanToken(strVar);             /* '{' or a '(.'.  If it is */
        if (Token != xxLParDot &&               /* ignore it but remember */
            Token != xxLCurlyBracket) {         /* to read ending '}' or */
          xUngetToken(Token, strVar);           /* '.)' as well!  */
        }
        else {
          Opening = (xbool)1;
        }

        /* Read integer tag */
        sprintf( strVar, "%s (%s)",
                 "Present", ((tSDLChoiceInfo *)Sort)->TagSort->Name );
        if ( ! xGenericReadOneParameter( ((tSDLChoiceInfo *)Sort)->TagSort,
                                         strVar, Address ) )
          return 0;
        tag = GenericGetValue( ((tSDLChoiceInfo *)Sort)->TagSortSize, Address);
        if ( tag >= i_max || tag < 0 ) {
          xPrintString( "Error: tag value out of range!\n");
          return 0;
        }
      }

      else {

        /* Read the present flag */
        /* P1 : "Present (<type-name>present) : " */
        /* P2 : "(<type-name>present) :"   <<-- THIS IS SKIPPED! */
        /* P3+: "<list of possible components> : " */

        Token = xScanToken(strVar);
        if (Token == xxComma)
          Token = xScanToken(strVar);

        if (Token == xxEOF) return 0;
        if (Token == xxEoln) {
          if (XSYSD xSaveLine != (char *)0) return 0;
          if (XNOTINCLUDEFILE) {
            /* If reading include file do no ouput Prompt
               to prevent UI from trigger on ": " in Prompt */
            /* P1: "Present (<type-name>present) : " */
            sprintf( strVar, "Present (%spresent) : ", Sort->Name );
            xPrintString( strVar );
          }
          Token = xScanToken(strVar);
        }
        while (Token == xxQuestionMark) {
          sprintf( strVar, "Present (%spresent) : ", Sort->Name );
          xPrintString( strVar );
          xSkipLine();
          Token = xScanToken(strVar);
        }

        while ( Token == xxEoln ||
                Token == xxQuestionMark ) {
          if (XSYSD xSaveLine != (char *)0 && Token == xxEoln ) return 0;
          if ( XNOTINCLUDEFILE ) {
            WriteBuf* buf = WriteBuf_New( 32 );
            for ( i = 0; i < i_max; ++i ) {
              (void)WriteBuf_Add_String( buf, FieldSort[i].Name, 0 );
              (void)WriteBuf_Add_Char( buf, ' ' );
              (void)WriteBuf_Terminate( buf );
              xPrintString( WriteBuf_Data( buf ) );
              WriteBuf_Clear( buf );
            }
            xPrintString( ": " );
            WriteBuf_Del( &buf );
          }
          Token = xScanToken( strVar );
        }

        /* Now translate the component name into an integer */
        len = strlen( strVar );
        if ( len > 0 ) {
          int count = 0;
          for ( i = 0; i < i_max; ++i ) {
            if ( !strncmp( strVar, FieldSort[i].Name, len ) ) {
              ++count;
              if ( count > 1 ) {
                if (FieldSort[i].Name[len] == '\0') { /* Exact match */
                  count--;
                  tag = i;
                }
                else if (FieldSort[tag].Name[len] == '\0') { /* Exact match */
                  count--;
                }
              }
              else {
                tag = i;
              }
            }
          }
          if ( count > 1 ) {
            xPrintString( "Name was ambiguous\n" );
            return 0;
          }
        }
        if ( tag < 0 ) {
          xPrintString( "Name was not found\n" );
          return 0;
        }


      }

      /* Ignore any ':' and ',' */
      Token = xScanToken(strVar);
      if ( (Token != xxColon) && (Token != xxComma) )
        xUngetToken(Token, strVar);


      /* Read the value corresponding to the tag */
      sprintf(strVar, " %s (%s) : ", FieldSort[tag].Name,
              FieldSort[tag].CompSort->Name);

      if (! xGenericReadOneParameter( FieldSort[tag].CompSort, strVar,
                           (void *)((xptrint)Address +
                                    ((tSDLChoiceInfo*)Sort)->OffsetToUnion) ))
        return 0;

      /* Set the tag */
      if (Sort->TypeClass != type_SDL_Union)
        GenericSetValue( ((tSDLChoiceInfo *)Sort)->TagSortSize, Address, tag );


      if ( Opening && Sort->TypeClass == type_SDL_Union) {
        /* Allow closing bracket only if opening bracket is read */
        Token = xScanToken(strVar);
        if (Token != xxRParDot && Token != xxRCurlyBracket)
          xUngetToken(Token, strVar);
      }
    }
    return 1;
#endif


#if !defined(XNOUSE_POWERSET_GENERATOR)
  case type_SDL_Powerset : /* ---------------------------------------------- */
    {
      tSDLTypeInfo* CompSort = ((tSDLPowersetInfo*)Sort)->CompSort;
      int i_max = ((tSDLPowersetInfo*)Sort)->Length;
      unsigned long* p = (unsigned long*)Address;
      int j;
      long      Value;
      void*     valPtr = (void*)&Value;

      while ( (CompSort->TypeClass == type_SDL_Syntype) ||
              (CompSort->TypeClass == type_SDL_Inherits ) ) {
        CompSort = ((tSDLGenInfo*)CompSort)->CompOrFatherSort;
      }

      /* Clear the bit mask */
      for ( i = 0, j = 0; i < i_max; i += 32, ++j ) {
        p[j] = 0;
      }
      /* Array is (Length/32 + 1) */

      Token = xScanToken( strVar );
      if ( Token == xxLBracket ) {
        Token = xScanToken( strVar );
      }
      else if ( XVALUE_SYNTAX &&
                Token != xxQuestionMark &&
                Token != xxEoln ) {
        xPrintString("Powerset value should start with [\n");
        return 0;
      }

      while ( Token != xxRBracket ) {

        if ( Token != xxComma )                 /* Ignore any commas  */
          xUngetToken( Token, strVar );

        /* Read the value */
        j = LocalGenericReadSort( valPtr, CompSort );
        if ( j == 0 )
          return 0;
        i = GenericGetValue( CompSort->SortSize, valPtr );

        /* Calculate bit position */
        i -= ((tSDLPowersetInfo*)Sort)->LowestValue;
        if ( i < 0 ) {
          /* BAD! Must NOT be less than 0 */
          xPrintString("Powerset value smaller than minimum.\n");
          return 0;
        }
        if ( i >= i_max ) {
          /* BAD Too large! */
          xPrintString("Powerset value too large.\n");
          return 0;
        }

        /* Set the bit */
        p[ ( (unsigned)i) >> 5 ] |= 1U << ( ((unsigned)i) & 0x1f );

        /* Read the next token */
        Token = xScanToken( strVar );
      }
      return 1;
    }
    /* NOTREACHED */
    break;
#endif


#if !defined(XNOUSE_GPOWERSET_GENERATOR) || !defined(XNOUSE_STRING_GENERATOR)
  case type_SDL_GPowerset : /* --------------------------------------------- */
  case type_SDL_String : /* ------------------------------------------------ */
    {
      tSDLTypeInfo* CompSort = ((tSDLGenListInfo*)Sort)->CompSort;
      xxToken end_token = xxRParDot;
      void*     Item;

      ((xString_Type*)Address)->First = 0;
      ((xString_Type*)Address)->Last = 0;
      ((xString_Type*)Address)->Length = 0;
      ((xString_Type*)Address)->IsAssigned = 0;

      Token = xScanToken(strVar);
      if ( Sort->TypeClass == type_SDL_GPowerset ) {
        if ( XVALUE_SYNTAX &&
             Token != xxLBracket &&
             Token != xxQuestionMark &&
             Token != xxEoln ) {
          xPrintString("Powerset value should start with [\n");
          return 0;
        }
        if ( Token != xxLBracket ) {
          xUngetToken(Token, strVar);
        }
        end_token = xxRBracket;
      }
      else {                                    /* string */
        if ( Token == xxLCurlyBracket ) {
          end_token = xxRCurlyBracket;
        }
        else if ( Token == xxLParDot ) {
          end_token = xxRParDot;
        }
#ifdef XITEXCOMM
        else if ( Token == xxLParColon ) {
          end_token = xxRParColon;
        }
#endif
        else if ( XVALUE_SYNTAX &&
                  Token != xxQuestionMark &&
                  Token != xxEoln ) {
          xPrintString("String value should start with (. or {\n");
          return 0;
        }
        else {
          xUngetToken(Token, strVar);
        }
      }

      /* Create a temporary Item */
      Item = (void *)XALLOC( CompSort->SortSize, 0);

      while ( Token != end_token ) {
        /* Clear contents of temporary value */
        (void)memset( Item, 0, CompSort->SortSize );

        (void)sprintf(strVar, " (%s) : ", ((tSDLPowersetInfo*)Sort)->Name);
        i = xGenericReadOneParameter( CompSort, strVar, (void *)Item);
        if (i == 0) {                           /* Failed */
          XFREE( &Item, CompSort->SortSize );
          return 0;
        }
        else if ( i == 1 ||                     /* Read one value! */
                  i == 3 ) {                    /* A ',' (short for default) */
          /* Add to object */
          if ( Sort->TypeClass == type_SDL_GPowerset ) {
            GenBag_Incl2( Item, (xBag_Type*)Address, (tSDLGenListInfo*)Sort );
          }
#if !defined(XNOUSE_STRING_GENERATOR)
          else {
            GenString_Append( (xString_Type*)Address, Item,
                              (tSDLGenListInfo*)Sort );
          }
#endif
          GenericFreeSort( (void**)Item, CompSort );
        }
        else if (i == 2) {                      /* } or .) or ] */
          Token = xScanToken(strVar);
          if ( Token == end_token )
            break;
          xPrintString("Wrong end token\n");

          XFREE( &Item, CompSort->SortSize );
          return 0;
        }
        Token = xScanToken(strVar);
        if ( Token != end_token )
          xUngetToken(Token, strVar);
      } /* while */
      XFREE( &Item, CompSort->SortSize );
      ((xString_Type*)Address)->IsAssigned = 1;
      return 1;
    }
    /* NOTREACHED */
    break;
#endif


#ifndef XNOUSE_BAG_GENERATOR
  case type_SDL_Bag : /* --------------------------------------------------- */
    {
      tSDLTypeInfo* CompSort = ((tSDLGenListInfo*)Sort)->CompSort;
      void*     Item;

      ((xBag_Type*)Address)->First = 0;
      ((xBag_Type*)Address)->Last = 0;
      ((xBag_Type*)Address)->Length = 0;
      ((xBag_Type*)Address)->IsAssigned = 0;

      Token = xScanToken(strVar);

      if (XVALUE_SYNTAX &&
#ifdef XITEXCOMM
          Token != xxLBracket &&
#endif
          Token != xxLCurlyBracket && Token != xxQuestionMark &&
          Token != xxEoln) {
        xPrintString("Bag value should start with {\n");
        return 0;
      }
      if (
#ifdef XITEXCOMM
          Token != xxLBracket &&
#endif
          Token != xxLCurlyBracket ) {
        xUngetToken(Token, strVar);
      }

      /* Create a temporary Item */
      Item = (void *)XALLOC( CompSort->SortSize, 0);

      while (
#ifdef XITEXCOMM
             Token != xxRBracket &&
#endif
             Token != xxRCurlyBracket) {

        (void)memset((void *)Item, 0, CompSort->SortSize );

        (void)sprintf(strVar, " (%s) : ", ((tSDLGenListInfo*)Sort)->Name);
        i = xGenericReadOneParameter( CompSort, strVar, (void *)Item);
        if (i == 0) {                           /* Read failed. */
          XFREE( &Item, CompSort->SortSize );
          return 0;
        }
        else if (i == 2) {                      /* Read end token */
          Token = xScanToken(strVar);
          if (
#ifdef XITEXCOMM
              Token == xxRBracket ||
#endif
              Token == xxRCurlyBracket) break;
          XFREE( &Item, CompSort->SortSize );
          return 0;
        }
        GenBag_Incl2( Item, (xBag_Type*)Address, (tSDLGenListInfo*)Sort );

        /* Look for ': <natural>' */
        Token = xScanToken(strVar);
        if (Token == xxColon) {
          Token = xScanToken(strVar);
          if (Token == xxId && sscanf(strVar, "%d", &i) && i >= 1) {
            while ( i > 1 ) {
              GenBag_Incl2( Item, (xBag_Type*)Address, (tSDLGenListInfo*)Sort);
              --i;
            }
            Token = xScanToken(strVar);
          } else {
            xPrintString("Illegal syntax for number of items");
            return 0;
          }
        }
        if (
#ifdef XITEXCOMM
            Token != xxRBracket &&
#endif
            Token != xxRCurlyBracket)
          xUngetToken(Token, strVar);
      }
      XFREE( &Item, CompSort->SortSize );
      ((xBag_Type*)Address)->IsAssigned = 1;
    }
    return 1;
#endif


#ifndef XNOUSE_LSTRING_GENERATOR
  case type_SDL_LString : /* ----------------------------------------------- */
    {
      int i_max = ((tSDLLStringInfo*)Sort)->MaxLength;
      xptrint p = (xptrint)Address + ((tSDLLStringInfo*)Sort)->DataOffset;
      xptrint dp = ((tSDLLStringInfo*)Sort)->CompSort->SortSize;
      tSDLTypeInfo* CompSort = ((tSDLGenListInfo*)Sort)->CompSort;
      int Res;

      Token = xScanToken(strVar);
      if (XVALUE_SYNTAX &&
#ifdef XITEXCOMM
          Token != xxLParColon &&
#endif
          Token != xxLParDot && Token != xxLCurlyBracket &&
          Token != xxQuestionMark && Token != xxEoln) {
        xPrintString("String value should start with (. or {\n");
        return 0;
      }
      if (
#ifdef XITEXCOMM
          Token != xxLParColon &&
#endif
          Token != xxLParDot && Token != xxLCurlyBracket)
        xUngetToken(Token, strVar);
      i = 0;
      while (
#ifdef XITEXCOMM
             Token != xxRParColon &&
#endif
             Token != xxRParDot && Token != xxRCurlyBracket) {

        /* Clear contents of temporary value */
        (void)memset( (void*)p, 0, CompSort->SortSize );

        (void)sprintf(strVar, " (%s) : ", CompSort->Name);
        Res = xGenericReadOneParameter(CompSort, strVar, (void*)p );
        if (Res == 0) return 0;
        if (Res == 2) {
          Token = xScanToken(strVar);
          if (
#ifdef XITEXCOMM
              Token == xxRParColon ||
#endif
              Token == xxRParDot || Token == xxRCurlyBracket) {
            break;
          }
          return 0;
        }
        Token = xScanToken(strVar);
        if (
#ifdef XITEXCOMM
            Token != xxRParColon &&
#endif
            Token != xxRParDot && Token != xxRCurlyBracket)
          xUngetToken(Token, strVar);
        ++i;
        if ( i >= i_max )
          break;
        p += dp;
      }
      ((xLString_Type *)Address)->Length = i;
    }
    return 1;
#endif


#if !defined(XNOUSE_ARRAY_GENERATOR) || !defined(XNOUSE_CARRAY_GENERATOR)
  case type_SDL_ComBuf :
    Token = xScanToken(strVar);
    if (Token == xxNull) {
      *(xComBuf *)Address = 0;
      /* Probably never needed to free old value */
      return 1;
    }
    else {
      xUngetToken(Token, strVar);
    }

    if (*(xComBuf *)Address == 0) {
      *(xComBuf *)Address = (xComBuf)xAlloc((((tSDLArrayInfo*)Sort)->Length+1)*
                                            sizeof(int));
      (*(xComBuf *)Address)->IsAssigned = 1;
    }
    Address = &((*(xComBuf *)Address)->el);
    /* fall through */
  case type_SDL_Array : /* ------------------------------------------------- */
  case type_SDL_Carray : /* ------------------------------------------------ */
    {
      int i_min = ((tSDLArrayInfo*)Sort)->LowestValue;    /* Inclusive */
      int i_max = i_min + ((tSDLArrayInfo*)Sort)->Length; /* Inclusive  */
      tSDLTypeInfo* CompSort = ((tSDLArrayInfo*)Sort)->CompSort;
      int isOthers = 1;
      int Index;                        /* in range [i_min,i_max) */
      unsigned long iVal;               /* Used for index value. */


      Token = xScanToken(strVar);
      if (XVALUE_SYNTAX &&
          Token != xxLParColon && Token != xxLCurlyBracket && 
          Token != xxQuestionMark && Token != xxEoln) {
        xPrintString("Array value should start with (: or {\n");
        return 0;
      }
      if (Token == xxLParColon || Token == xxLCurlyBracket)
        Token = xScanToken(strVar);

      if (Token == xxLPar) {
        Token = xScanToken(strVar);
        if (Token != xxId || xfEqualIdString(strVar, "others") != 2) {
          xUngetToken(Token, strVar);
          Token = xxLPar;
        }
      }

      if (Token == xxId && xfEqualIdString(strVar, "others") == 2) {
        while (Token != xxRParColon && Token != xxRCurlyBracket) {
          if ( isOthers ) {
            Index = i_min;
          }
          else {
            if (Token == xxComma)
              Token = xScanToken(strVar);
            if (Token == xxLPar)
              Token = xScanToken(strVar);
            if (Token == xxEoln || Token == xxQuestionMark) {
              sprintf(strVar, "Index (%s) : ", ySDL_SDL_Integer.Name);
              xPrintString( strVar );
              Token = xScanToken(strVar);
            }
            if (Token == xxRParColon || Token == xxRCurlyBracket)
              break;
            xUngetToken(Token, strVar);

            i = LocalGenericReadSort( &iVal,((tSDLArrayInfo*)Sort)->IndexSort);
            if ( !i ) {
              xPrintString( "Illegal index value\n" );
              return 0;
            }
            Index = GenericGetValue(
                                   ((tSDLArrayInfo*)Sort)->IndexSort->SortSize,
                                   &iVal );
            if ( Index < i_min || Index >= i_max ) {
              xPrintString( "Array index out of range\n" ); 
              return 0;
            }
          }

          Token = xScanToken(strVar);
          if (Token == xxColon)
            Token = xScanToken(strVar);

          if (Token == xxEoln || Token == xxQuestionMark) {
            if (XNOTINCLUDEFILE) {
              /* If reading include file do no ouput Prompt
                 to prevent UI from trigger on ": " in Prompt */
              if ( isOthers ) {
                sprintf( strVar, "Others component (%s) : ", CompSort->Name);
              }
              else {
                sprintf( strVar, "Component (%s) : ", CompSort->Name);
              }
              xPrintString( strVar );
            }
          }
          else {
            xUngetToken(Token, strVar);
          }

          i = LocalGenericReadSort( (void*)( (xptrint)Address +
                                             ( (Index - i_min) * 
                                               CompSort->SortSize ) ),
                                    CompSort );
          if ( !i )
            return 0;
          
          if ( isOthers ) {
            void* p = (void*)( (xptrint)Address +
                               (Index - i_min) * CompSort->SortSize );
            isOthers = 0;

            /* for each other index copy component */
            for ( Index = i_min + 1 ; Index < i_max; Index++) {
              void* q = (void*)((xptrint)Address + 
                                (Index - i_min) * CompSort->SortSize);

              (void)GenericAssignSort(q, p, XASS_MR_ASS_FR, CompSort );

            }
          }
          Token = xScanToken(strVar);
          if (Token == xxRPar)
            Token = xScanToken(strVar);
        } /* while */
        return 1;
      } /* if ( Token == xxId || "others" ) */


      /* for each index read component */
      for (Index = i_min; Index < i_max; Index++) {

        if (Token == xxComma)
          Token = xScanToken(strVar);

        if (Token == xxEoln) {
          if (XNOTINCLUDEFILE) {
            /* If reading include file do no ouput Prompt
               to prevent UI from trigger on ": " in Prompt */
            WriteBuf* buf = WriteBuf_New( 16 );
            GenericSetValue( ((tSDLArrayInfo*)Sort)->IndexSort->SortSize, 
                             &iVal, Index );
            WriteBuf_Add_String( buf, " Index=", 7 );
            xGenericWriteSort( buf, &iVal, ((tSDLArrayInfo*)Sort)->IndexSort );
            WriteBuf_Add_String( buf, " : ", 3 );
            WriteBuf_Terminate( buf );
            xPrintString( WriteBuf_Data( buf ) );
            WriteBuf_Del( &buf );
          }
          Token = xScanToken(strVar);
        }

        if (Token == xxRParColon || Token == xxRCurlyBracket) {
          xPrintString("Rest of array is set to NULL\n");
          /* for each other index set to NULL */
          /* .. This is not entirely correct, should use the default value! */
          while ( Index < i_max ) {
            void* p = (void *)((xptrint)Address +
                               (Index-i_min) * CompSort->SortSize);

            (void)memset(p, 0, CompSort->SortSize);
            ++Index;
          }
          break;
        }

        xUngetToken(Token, strVar);

        i = LocalGenericReadSort( (void*)( (xptrint)Address +
                                          (Index-i_min) * CompSort->SortSize ),
                                  CompSort );
        if ( !i )
          return 0;
        Token = xScanToken(strVar);
      } /* for each Index */

      if (Token != xxRParColon && Token != xxRCurlyBracket) {
        xUngetToken(Token, strVar);
      }
    }
    return 1;
#endif


#ifndef XNOUSE_GARRAY_GENERATOR
  case type_SDL_GArray : /* ------------------------------------------------ */
    {
      int isOthers = 1;
      tSDLTypeInfo*     IndexSort = ((tSDLGArrayInfo*)Sort)->IndexSort;
      tSDLTypeInfo*     CompSort  = ((tSDLGArrayInfo*)Sort)->CompSort;
      void*     Index;
      void*     Value;

      Token = xScanToken(strVar);
      if (XVALUE_SYNTAX &&
          Token != xxLParColon && Token != xxLCurlyBracket &&
          Token != xxQuestionMark && Token != xxEoln) {
        xPrintString("Array value should start with (:\n");
        return 0;
      }

      if (Token != xxLParColon && Token != xxLCurlyBracket)
        xUngetToken(Token, strVar);

      Index = (void *)XALLOC( IndexSort->SortSize, 0);
      Value = (void *)XALLOC( CompSort->SortSize,  0);
      while (Token != xxRParColon && Token != xxRCurlyBracket) {

        Token = xScanToken(strVar);
        if (Token == xxLPar)
          Token = xScanToken(strVar);

        /* Read Index value or others */
        if ( !isOthers ) {
          /* Read Index */
          if ( Token == xxEoln || Token == xxQuestionMark ) {
            if (XNOTINCLUDEFILE) {
              /* If reading include file do no ouput Prompt
                 to prevent UI from trigger on ": " in Prompt */
              (void)sprintf(strVar, " Index (%s) : ", IndexSort->Name);
              xPrintString(strVar);
            }
            Token = xScanToken(strVar);
          }
          if (Token == xxRParColon || Token == xxRCurlyBracket)
            break;
          xUngetToken(Token, strVar);
          (void)memset( Index, 0, IndexSort->SortSize );
          i = LocalGenericReadSort( Index, IndexSort );
          if ( !i ) {
            xPrintString("Illegal index value\n");
            XFREE( &Index, IndexSort->SortSize );
            XFREE( &Value, CompSort->SortSize );
            return 0;
          }
          Token = xScanToken(strVar);
        }

        else {
          /* Read others */
          if (Token == xxEoln || Token == xxQuestionMark) {
            xPrintString(" others");
          } else if (Token == xxId) {
            if (xfEqualIdString(strVar, "others") != 2) {
              xPrintString("others expected\n");
              XFREE( &Index, IndexSort->SortSize );
              XFREE( &Value, CompSort->SortSize );
              return 0;
            }
            Token = xScanToken(strVar);
          } else {
            xPrintString("others expected\n");
            XFREE( &Index, IndexSort->SortSize );
            XFREE( &Value, CompSort->SortSize );
            return 0;
          }
        }

        if (Token != xxColon)
          xUngetToken(Token, strVar);

        if ( isOthers ) {
          (void)sprintf(strVar,
                        " Others component (%s) : ", CompSort->Name);
        }
        else {
          (void)sprintf(strVar, " Component (%s) : ", CompSort->Name);
        }

        /* Read the value */
        (void)memset( Value, 0, CompSort->SortSize);
        i = xGenericReadOneParameter( CompSort, strVar, Value );
        if ( i == 0 ) {
          xPrintString("Illegal component value\n");
          if ( !isOthers ) {
            /* Index has a valid value that needs to be destroyed */
            GenericFreeSort((void **)Index, IndexSort );
          }
          else {
            XFREE( &Index, IndexSort->SortSize );
          }
          XFREE( &Value, CompSort->SortSize );
          return 0;
        }
        if (isOthers) {
          void* ptr;
          if ( IsPointerToMake(CompSort) ) {
            ptr = *(void**)Value;
          }
          else {
            ptr = Value;
          }
          (void)GenericAssignSort(Address,
                                  GenericMakeArray(Address, Sort, ptr),
                                  XASS_MR_ASS_NF,
                                  Sort );
        } else {
          (void)GenericAssignSort( GenGArray_Modify( (xGArray_Type *)Address, 
                                                      Index, 
                                                      (tSDLGArrayInfo*)Sort ),
                                    Value,
                                    XASS_MR_ASS_FR,
                                    CompSort );
          GenericFreeSort( (void **)Index, IndexSort );
        }

        GenericFreeSort( (void **)Value, CompSort );

        Token = xScanToken(strVar);
        if (Token == xxRPar) Token = xScanToken(strVar);
        if (Token == xxComma) Token = xScanToken(strVar);
        if (Token != xxRParColon && Token != xxRCurlyBracket)
          xUngetToken(Token, strVar);
        isOthers = (xbool)0;
      } /* while (Token != xxRParColon) */

      XFREE( &Index, IndexSort->SortSize );
      XFREE( &Value, CompSort->SortSize );

      ((xGArray_Type*)Address)->IsAssigned = 1;
    }
    return 1;
#endif


  case type_SDL_Own : /* --------------------------------------------------- */
  case type_SDL_Oref : /* -------------------------------------------------- */
  case type_SDL_Ref : /* --------------------------------------------------- */
    {
      tSDLTypeInfo* CompSort = ((tSDLGenInfo*)Sort)->CompOrFatherSort;
      void* Item;

      Token = xScanToken(strVar);

      if (Token == xxId && (strcmp(strVar, "new") == 0 || 
                            strcmp(strVar, "New") == 0 ||
                            strcmp(strVar, "NEW") == 0 ) ) {
        Token = xScanToken(strVar);
        if (Token != xxLPar) 
          xUngetToken(Token, strVar);

        Item = (void *)XALLOC( CompSort->SortSize, 0);
        addNL( &xRefReadList, Item);

        i = LocalGenericReadSort( Item, CompSort );
        if ( !i ) {
          xPrintString("Illegal value\n");
          XFREE( &Item, CompSort->SortSize);
          return 0;
        }

        *((void **)Address) = Item;
#ifndef XVALIDATOR_LIB
#ifdef XEOWN
        if ( Sort->TypeClass == type_SDL_Own)
          xInsertOwnList( Item, Sort );
#endif
#endif
        Token = xScanToken(strVar);
        if (Token != xxRPar)
          xUngetToken(Token, strVar);
        return 1;
      }

      else if (Token == xxId && ( strcmp(strVar, "old") == 0 ||
                                  strcmp(strVar, "Old") == 0 ||
                                  strcmp(strVar, "OLD") == 0 ) ) {
        SDL_Integer intVal;

        Token = xScanToken(strVar);
        if (Token != xxLPar)
          xUngetToken(Token, strVar);

        i = LocalGenericReadSort( &intVal, &ySDL_SDL_Integer );
        if ( !i ) {
          xPrintString("Illegal value\n");
          return 0;
        }

        if (intVal <= 0 || intVal > lenNL(xRefReadList)) {
          xPrintString("Illegal value\n");
          return 0;
        }

        *((void **)Address) = xRefReadList[ intVal - 1 ];
#ifndef XVALIDATOR_LIB
#ifdef XEOWN
        if (Sort->TypeClass == type_SDL_Own) {
          if ( ! xInOwnList(*((void **)Address)) )
            xInsertOwnList(*((void **)Address), Sort);
        }
#endif
#endif

        Token = xScanToken(strVar);
        if (Token != xxRPar)
          xUngetToken(Token, strVar);
        return 1;
      }

#ifndef XVALIDATOR_LIB
#ifdef XMONITOR
      else if (XSYSD xRestoringState &&
               Token == xxId && strcmp(strVar, "Ref") == 0) {
        *((void **)Address) = xGetRefAddress(strVar);
        return 1;
      }
#endif
#endif

      else if (Token == xxNull) {
        *(void **)Address = NULL;
        return 1;
      }

      else if ( XDEREF_SYNTAX ) {
        Token = xScanToken(strVar);
        if (Token != xxLPar)
          xUngetToken(Token, strVar);

        Item = (void *)XALLOC( CompSort->SortSize, 0);
        addNL(&xRefReadList, Item);

        i = LocalGenericReadSort( Item, CompSort );
        if ( !i ) {
          xPrintString("Illegal value\n");
          XFREE(&Item,CompSort->SortSize);
          return 0;
        }

        *(void **)Address = Item;
#ifndef XVALIDATOR_LIB
#ifdef XEOWN
        if (Sort->TypeClass == type_SDL_Own)
          xInsertOwnList( Item, Sort );
#endif
#endif
        Token = xScanToken(strVar);
        if (Token != xxRPar)
          xUngetToken(Token, strVar);
        return 1;
      }

      else if (Token == xxId) {
        if ( sscanf(strVar, "%p", &Item) == 1 ) {
          *(void **)Address = Item;
          return 1;
        }
        return 0;
      }

      xUngetToken(Token, strVar);
      return LocalGenericReadSortHex( Address, Sort, strVar );
    } /* Own/Ref/ORef */
    /* NOTREACHED */
    break;


  /* SDL - standard ctypes */
  case type_SDL_ShortInt : /* ---------------------------------------------- */
    {
      Token = xPromptQuestionMark(" (ShortInt) : ", " (ShortInt) : ", strVar );
      if ( Token == xxPlus ) {
        Token = xScanToken( strVar );
      }
      else if ( Token == xxMinus ) {
        strVar[0] = '-';
        Token = xScanToken( strVar+1 );
      }
      if (Token != xxId) {
        xPrintString( "Illegal ShortInt value\n" );
        return 0;
      }

      if ( sscanf( strVar, "%hd", (short int*)Address ) >= 1 ) {
        return 1;
      }

      xPrintString( "Illegal ShortInt value\n" );
    }
    return 0;


  case type_SDL_LongInt : /* ----------------------------------------------- */
    {
      Token = xPromptQuestionMark( " (LongInt) : ", " (LongInt) : ", strVar );
      if ( Token == xxPlus ) {
        Token = xScanToken( strVar );
      }
      else if ( Token == xxMinus ) {
        strVar[0] = '-';
        Token = xScanToken( strVar+1 );
      }
      if (Token != xxId) {
        xPrintString( "Illegal LongInt value\n" );
        return 0;
      }

      if ( sscanf( strVar, "%ld", (long*)Address ) >= 1 ) {
        return 1;
      }

      xPrintString( "Illegal LongInt value\n" );
    }
    return 0;


  case type_SDL_UnsignedShortInt : /* -------------------------------------- */
    {
      Token = xPromptQuestionMark( " (UnsignedShortInt) : ",
                                   " (UnsignedShortInt) : ", strVar);
      if ( Token == xxPlus ) {
        Token = xScanToken( strVar );
      }
      if (Token != xxId) {
        xPrintString( "Illegal UnsignedShortInt value\n" );
        return 0;
      }

      if ( sscanf( strVar, "%hu", (unsigned short*)Address ) >= 1 ) {
        return 1;
      }

      xPrintString( "Illegal UnsignedShortInt value\n" );
    }
    return 0;


  case type_SDL_UnsignedInt : /* ------------------------------------------- */
    {
      Token = xPromptQuestionMark( " (UnsignedInt) : ",
                                   " (UnsignedInt) : ",strVar );
      if ( Token == xxPlus ) {
        Token = xScanToken( strVar );
      }
      if (Token != xxId) {
        xPrintString( "Illegal UnsignedInt value\n" );
        return 0;
      }

      if ( sscanf( strVar, "%u", (unsigned*)Address ) >= 1 ) {
        return 1;
      }

      xPrintString( "Illegal UnsignedInt value\n" );
    }
    return 0;


  case type_SDL_UnsignedLongInt : /* --------------------------------------- */
    {
      Token = xPromptQuestionMark( " (UnsignedLongInt) : ",
                                   " (UnsignedLongInt) : ",  strVar );
      if ( Token == xxPlus ) {
        Token = xScanToken( strVar );
      }
      if (Token != xxId) {
        xPrintString( "Illegal UnsignedLongInt value\n" );
        return 0;
      }

      if ( sscanf( strVar, "%lu", (unsigned long*)Address ) >= 1 ) {
        return 1;
      }

      xPrintString( "Illegal UnsignedLongInt value\n" );
    }
    return 0;


#ifndef XNOUSEOFREAL
  case type_SDL_Float : /* ------------------------------------------------- */
    {
      SDL_Real r;
      if (LocalGenericReadSort( &r, &ySDL_SDL_Real )) {
        *(float*)Address = (float)r;
        return 1;
      }
    }
    return 0;
#endif

  case type_SDL_Charstar : /* ---------------------------------------------- */
    {
      SDL_Charstring temp = (SDL_Charstring)0;
      if ( LocalGenericReadSort( &temp, &ySDL_SDL_Charstring ) && temp ) {
        unsigned int len;
        temp[0] = 'T';
        len = strlen( temp ) - 1;

#ifdef XVALIDATOR_LIB
        *(char**)Address = (char*)XALLOC_REF_VAL(len+1, Sort->SortIdNode );
#else
        *(char**)Address = (char*)XALLOC(len+1, Sort );
#endif
        if (len>0)
          memcpy(*(char**)Address, temp+1, len);
        (*(char**)Address)[len] = '\0';
        if (temp != 0 && temp[0] == 'T')
          xFree_SDL_Charstring((void **)&temp);
        return 1;
        
      }
    }
    return 0;


  case type_SDL_EmptyType : /* --------------------------------------------- */
    /* SDL:   (. .) */
    /* ASN.1: { }   */
    {
      xxToken endToken = xxRParDot;
      Token = xScanToken( strVar );
      if ( Token == xxLCurlyBracket ) {
        endToken = xxRCurlyBracket;
      }
      else if ( Token != xxLParDot ) {
        xPrintString( "Illegal EmptyType value\n" );
        return 0;
      }

      Token = xScanToken( strVar );
      while ( Token != endToken ) {
        if ( Token != xxEoln ) {
          xPrintString( "Illegal EmptyType value\n" );
          return 0;
        }
        Token = xScanToken( strVar );
      }
      /* No need to assign anything... */
    }
    return 1;


  case type_SDL_Voidstar : /* ---------------------------------------------- */
  case type_SDL_Voidstarstar : /* ------------------------------------------ */
    {
      void* Item;
      Token = xScanToken( strVar );
      if ( Token == xxId && sscanf(strVar, "%p", &Item) == 1 ) {
        *(void **)Address = Item;
        return 1;
      }
    }
    break;


  case type_SDL_Userdef : /* ----------------------------------------------- */
  case type_SDL_Signal : /* ------------------------------------------------ */
  case type_SDL_SignalId : /* ---------------------------------------------- */
  case type_SDL_UnionC : /* ------------------------------------------------ */
  default: /* -------------------------------------------------------------- */
    return LocalGenericReadSortHex( Address, Sort, strVar );
   
  }

  return 0;
}




int
xGenericReadSort( void         * Result_Addr,
                  tSDLTypeInfo * Sort)
{
  static int xReadSortLevel = 0;
  int result;

  xReadSortLevel++;
  result = LocalGenericReadSort( Result_Addr, Sort );
  xReadSortLevel--;
  if (
#ifndef XVALIDATOR_LIB
#ifdef XMONITOR
      ! XSYSD xRestoringState &&
#endif
#endif
      xReadSortLevel == 0 && xRefReadList ) {
    releaseNL( (void ***)&xRefReadList );
  }
  return result;
}


/*---+---------------------------------------------------------------
     xReadSDL_Time
-------------------------------------------------------------------*/
int xReadSDL_Time(
  SDL_Time *T)
{
  xxToken Token;
  char    strVar[256];
  char   *TempStr;
  int     Pos;
  xbool   GotSomething = (xbool)0;
  xbool   Plus, Minus;

  (*T).s = (xint32)0;
  (*T).ns = (xint32)0;
  Token = xScanToken(strVar);
  Plus = (xbool)0;
  Minus = (xbool)0;
  if (Token == xxPlus) {
    Plus = (xbool)1;
    Token = xScanToken(strVar);
  } else if (Token == xxMinus) {
    Minus = (xbool)1;
    Token = xScanToken(strVar);
  }
  if (Token != xxId) {
    xPrintString("Illegal syntax\n");
    return 0;
  }
  TempStr = strVar;
  while (*TempStr >= '0' && *TempStr <= '9') {
    GotSomething = (xbool)1;
    if ((*T).s > xMaxTime.s / 10) {
      xPrintString("Integer overflow\n");
      return 0;                        /* overflow */
    }
    if ((*T).s * 10 > xMaxTime.s - (*TempStr - '0')) {
      xPrintString("Integer overflow\n");
      return 0;                        /* overflow */
    }
    (*T).s = (*T).s * 10 + (*TempStr - '0');
    TempStr++;
  }
  if (*TempStr == '.') {
    TempStr++;
    for (Pos = 1; Pos <= 9; Pos++) {
      (*T).ns *= 10;
      if (*TempStr >= '0' && *TempStr <= '9') {
        GotSomething = (xbool)1;
        (*T).ns += (*TempStr - '0');
        TempStr++;
      }
    }
  }
  if (GotSomething) {
#if defined(XMONITOR) && ! defined(XCALENDARCLOCK) && !defined(XVALIDATOR_LIB)
    if (Minus)
      *T = xMinusD_SDL_Time(XSYSD NowInMonitor, *T);
    else if (Plus)
      *T = xPlus_SDL_Time(XSYSD NowInMonitor, *T);
#else
    if (Minus)
      *T = xMinusD_SDL_Time(SDL_Now(), *T);
    else if (Plus)
      *T = xPlus_SDL_Time(SDL_Now(), *T);
#endif
    return 1;
  } else {
    xPrintString("Illegal syntax\n");
    return 0;
  }
}


/*---+---------------------------------------------------------------
     xRead_SDL_Charstring
-------------------------------------------------------------------*/
int xRead_SDL_Charstring(
  void  * Result)
{
  xxToken Token;
  char    strVar[MAX_READ_LENGTH];
  char    UnPrint[6]; /* Max length of CharacterLiteral is 5 */
  int     IRes, IstrVar, IUnPrint, I;
  
  Token = xPromptQuestionMark(" (charstring) : ",
    " (charstring) : ", strVar);
  if (Token != xxString && Token != xxASN1String) {
    xPrintString("Charstring value should start with ' or \"\n");
    return 0;
  }

  yFree_SDL_Charstring((void **)Result);
  *(SDL_Charstring *)Result = (SDL_Charstring)
    XALLOC((xptrint)(strlen(strVar)), (tSDLTypeInfo *)&ySDL_SDL_Charstring);
  (*(SDL_Charstring *)Result)[0] = 'V';

  IRes = 1;
  for (IstrVar=1; strVar[IstrVar+1]!='\0'; IstrVar++) {
    if ( ( Token == xxString &&
           strVar[IstrVar] == '\'' && strVar[IstrVar+1] != '\'') ||
         ( Token == xxASN1String &&
           strVar[IstrVar] == '"' && strVar[IstrVar+1] != '"' )
       ) {
      IUnPrint = 0;
      if (Token == xxString) {
        for (IstrVar++; strVar[IstrVar]!='\'' && IUnPrint < 5; IstrVar++)
          UnPrint[IUnPrint++] = strVar[IstrVar];
      } else {
        for (IstrVar++; strVar[IstrVar]!='"' && IUnPrint < 5; IstrVar++)
          UnPrint[IUnPrint++] = strVar[IstrVar];
      }
      UnPrint[IUnPrint] = '\0';
      for (I=0; I<=64 && strcmp(UnPrint, xfCharacterLiteral[I])!=0; I++)
        ;
      if (I < 32)
        (*(SDL_Charstring *)Result)[IRes++] = (char)I;
      else if (I < 65)
        (*(SDL_Charstring *)Result)[IRes++] = (char)(127 - 32 + I);
      else {
        xPrintString("Illegal unprintable character in charstring\n");
        (*(SDL_Charstring *)Result)[IRes] = '\0';
        return 0;
      }
    } else {
      if ( ( Token == xxString &&
             strVar[IstrVar] == '\'' && strVar[IstrVar+1] == '\'' ) ||
           ( Token == xxASN1String &&
             strVar[IstrVar] == '"' && strVar[IstrVar+1] == '"' )
         ) {
        IstrVar++;
      }
      (*(SDL_Charstring *)Result)[IRes++] = strVar[IstrVar];
    }
  }
  (*(SDL_Charstring *)Result)[IRes] = '\0';
  return 1;
}


#endif /* XREADANDWRITEF */
