/******************************************************************************
 * Copyright by Telelogic AB 1994-1997
 * Copyright by Telelogic AB 1998
 *
 *  This Program is owned by Telelogic and is protected by national 
 * copyright laws and international copyright treaties. Telelogic 
 * grants you the right to use this Program on one computer or in 
 * one local computer network at any one time. 
 * Under this License you may only modify the source code for the purpose 
 * of adapting it to your environment. You must reproduce and include 
 * any copyright and trademark notices on all copies of the source code. 
 * You may not use, copy, merge, modify or transfer the Program except as
 * provided in this License.
 * Telelogic does not warrant that the Program will meet your
 * requirements or that the operation of the Program will be
 * uninterrupted and error free. You are solely responsible that the
 * selection of the Program and the modification of the source code 
 * will achieve your intended results and that the results are actually 
 * obtained.
 *****************************************************************************/

#ifndef _itex2_h
#define _itex2_h


/*======================================================================*/
/* ITEX specific message event definitions */

#define IEBXCONVERTTOGR			SP_RPCREQUEST (IET_BASE + 50)
#define IEBXCONVERTTOGRREPLY		SP_RPCREPLY   (IET_BASE + 50)

#define IEBXOPENEDDOCUMENTS		SP_RPCREQUEST (IET_BASE + 51)
#define IEBXOPENEDDOCUMENTSREPLY	SP_RPCREPLY   (IET_BASE + 51)

#define IEBXGETBUFFIDFROMPATH		SP_RPCREQUEST (IET_BASE + 52)
#define IEBXGETBUFFIDFROMPATHREPLY	SP_RPCREPLY   (IET_BASE + 52)

#define IEBXGETBUFFIDFROMMPPATH		SP_RPCREQUEST (IET_BASE + 53)
#define IEBXGETBUFFIDFROMMPPATHREPLY	SP_RPCREPLY   (IET_BASE + 53)

#define IEBXCONVERTTOMP			SP_RPCREQUEST (IET_BASE + 54)
#define IEBXCONVERTTOMPREPLY		SP_RPCREPLY   (IET_BASE + 54)

#define IEBXCONVERTSELTOMP		SP_RPCREQUEST (IET_BASE + 55)
#define IEBXCONVERTSELTOMPREPLY		SP_RPCREPLY   (IET_BASE + 55)

#define IEBXMERGEDOCUMENTS		SP_RPCREQUEST (IET_BASE + 56)
#define IEBXMERGEDOCUMENTSREPLY		SP_RPCREPLY   (IET_BASE + 56)

#define IEBXCLOSEDOCUMENT		SP_RPCREQUEST (IET_BASE + 57)
#define IEBXCLOSEDOCUMENTREPLY		SP_RPCREPLY   (IET_BASE + 57)

#define IEBXSAVE			SP_RPCREQUEST (IET_BASE + 58)
#define IEBXSAVEREPLY			SP_RPCREPLY   (IET_BASE + 58)

#define IEBXSELECTOR			SP_RPCREQUEST (IET_BASE + 59)
#define IEBXSELECTORREPLY		SP_RPCREPLY   (IET_BASE + 59)

#define IEBXSELECTALL			SP_RPCREQUEST (IET_BASE + 60)
#define IEBXSELECTALLREPLY		SP_RPCREPLY   (IET_BASE + 60)

#define IEBXDESELECTALL			SP_RPCREQUEST (IET_BASE + 61)
#define IEBXDESELECTALLREPLY		SP_RPCREPLY   (IET_BASE + 61)

#define IEBXISSELECTED			SP_RPCREQUEST (IET_BASE + 62)
#define IEBXISSELECTEDREPLY		SP_RPCREPLY   (IET_BASE + 62)

#define IEBXGETDOCUMENTMODIFYTIME	SP_RPCREQUEST (IET_BASE + 63)
#define IEBXGETDOCUMENTMODIFYTIMEREPLY	SP_RPCREPLY   (IET_BASE + 63)

#define IEBXGETPATH			SP_RPCREQUEST (IET_BASE + 64)
#define IEBXGETPATHREPLY		SP_RPCREPLY   (IET_BASE + 64)

#define IEBXGETMPPATH			SP_RPCREQUEST (IET_BASE + 65)
#define IEBXGETMPPATHREPLY		SP_RPCREPLY   (IET_BASE + 65)

#define IEBXFINDTABLE			SP_RPCREQUEST (IET_BASE + 66)
#define IEBXFINDTABLEREPLY		SP_RPCREPLY   (IET_BASE + 66)

#define IEBXCLOSETABLE			SP_RPCREQUEST (IET_BASE + 67)
#define IEBXCLOSETABLEREPLY		SP_RPCREPLY   (IET_BASE + 67)

#define IEBXGETTABLESTATE		SP_RPCREQUEST (IET_BASE + 68)
#define IEBXGETTABLESTATEREPLY		SP_RPCREPLY   (IET_BASE + 68)

#define IEBXGETROWNUMBER		SP_RPCREQUEST (IET_BASE + 69)
#define IEBXGETROWNUMBERREPLY		SP_RPCREPLY   (IET_BASE + 69)

#define IEBXSELECTROW			SP_RPCREQUEST (IET_BASE + 70)
#define IEBXSELECTROWREPLY		SP_RPCREPLY   (IET_BASE + 70)

#define IEBXCLEARSELECTION		SP_RPCREQUEST (IET_BASE + 71)
#define IEBXCLEARSELECTIONREPLY		SP_RPCREPLY   (IET_BASE + 71)

#define IEBXROWSSELECTED		SP_RPCREQUEST (IET_BASE + 72)
#define IEBXROWSSELECTEDREPLY		SP_RPCREPLY   (IET_BASE + 72)

#define IEBXANALYZE			SP_RPCREQUEST (IET_BASE + 73)
#define IEBXANALYZEREPLY		SP_RPCREPLY   (IET_BASE + 73)

#endif /* _itex2_h */
