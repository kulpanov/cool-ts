/******************************************************************************
 *
 * Copyright 1997 Telelogic AB
 * Copyright 1998 Telelogic AB
 * 
 *  This Program is owned by Telelogic and is protected by national 
 * copyright laws and international copyright treaties. Telelogic 
 * grants you the right to use this Program on one computer or in 
 * one local computer network at any one time. 
 * Under this License you may only modify the source code for the purpose 
 * of adapting it to your environment. You must reproduce and include 
 * any copyright and trademark notices on all copies of the source code. 
 * You may not use, copy, merge, modify or transfer the Program except as
 * provided in this License.
 * Telelogic does not warrant that the Program will meet your
 * requirements or that the operation of the Program will be
 * uninterrupted and error free. You are solely responsible that the
 * selection of the Program and the modification of the source code 
 * will achieve your intended results and that the results are actually 
 * obtained.
 *****************************************************************************/

#ifndef _dll_h
#define _dll_h

#if defined _Windows || defined _WINDOWS
/* Macros for DLL support on Windows */

/* USING_DLL Should be defined by applications that uses Tau DLL's */

#ifdef __DLL__
/* Lib as DLL */

#define STDCALL __stdcall

/* DLL modifier for Variables */
#define DLLENTRY

/* DLL modifiers for Functions */
#define DLLEXPORT __declspec(dllexport)
#define DLLIMPORT __declspec(dllimport)
#endif /* __DLL__ */

#ifndef __DLL__
#define STDCALL
#ifndef USING_DLL
#define DLLENTRY

#define DLLEXPORT
#define DLLIMPORT
#endif /* ~USING_DLL */

#ifdef USING_DLL
#undef STDCALL
#define STDCALL __stdcall
#define DLLENTRY  __declspec(dllimport)

#define DLLEXPORT __declspec(dllimport)
#define DLLIMPORT
#endif /* USING_DLL */
#endif /* ~__DLL__ */

#else /* POSIX */
#define STDCALL
#define DLLENTRY
#define DLLEXPORT
#define DLLIMPORT
#endif /* POSIX */

#endif /* _dll_h */
/* end of file */







