/******************************************************************************
 * Copyright by Telesoft Europe AB 1990, 1991.
 * Copyright by TeleLOGIC Malmoe AB 1991, 1992, 1993, 1994.
 * Copyright by TeleLOGIC AB 1994, 1995.
 * Copyright by Telelogic AB 1996, 1997, 1998.
 *  This Program is owned by Telelogic and is protected by national 
 * copyright laws and international copyright treaties. Telelogic 
 * grants you the right to use this Program on one computer or in 
 * one local computer network at any one time. 
 * Under this License you may only modify the source code for the purpose 
 * of adapting it to your environment. You must reproduce and include 
 * any copyright and trademark notices on all copies of the source code. 
 * You may not use, copy, merge, modify or transfer the Program except as
 * provided in this License.
 * Telelogic does not warrant that the Program will meet your
 * requirements or that the operation of the Program will be
 * uninterrupted and error free. You are solely responsible that the
 * selection of the Program and the modification of the source code 
 * will achieve your intended results and that the results are actually 
 * obtained.
 *****************************************************************************/

#ifndef _sdt_h
#define _sdt_h

#include "itex2.h"

/************************ tool and message definitions ********************/

#define SET_POST                1000    
#define SET_EXTERN              2000    
#define SET_SDT                 3000    /* abstract sdt tool, some common services*/
#define SET_EDITOR              4000    /* abstract editor, common services */

#define SET_ORGANIZER           5000    
#define SET_SDLE                6000
#define SET_MSCE                7000
#define SET_INFOSERVER          9000
#define SET_FILEVIEWER          10000   /* Search list manager */
#define SET_COVERAGEVIEWER      11000   /* Coverage Browser */
#define SET_XREFVIEWER          12000   /* Cross-Reference Browser */
#define SET_TYPEVIEWER          13000   /* Type Browser */
#define SET_TREEVIEWER          14000
#define SET_PREFERENCES         17000
#define SET_SIMULATOR_UI        18000
#define SET_ANALYZER            19000
#define SET_VALIDATOR           25000
#define SET_SIMULATOR           26000
#define SET_SDLENV              27000
#define SET_LINK                32000
#define SET_CMICRO_TESTER       33000
#define SET_HELPVIEWER          34000
#define SET_OME                 35000
#define SET_OMINFOSERVER        36000

#define SET_TE                  38000

#define IET_BASE                40000   /* The TTCN Editor */
#define IET_EXTERN              41000   /* Remote commands (Public interface) */
#define IET_ACCESS              43000   /* Access library applications */
#define IET_SIMULATOR_UI        44000   /* Simulator user interface (controller) */
#define IET_C_CODE_GENERATOR    47000   /* The C code generator */

#define SET_TARGETINGEXPERT     60000   /* Targeting Expert */

#define SET_CMICRO_RECORDER           95000
#define SET_CMICRO_PREEMPTION         96000
#define SET_CMICRO_LAUTERBACH_GATEWAY 97000
#define SET_CMICRO_BODYBUILDER        98000
#define SET_CMICRO_DOSTESTER          99000

/********************** POST *********************************************/
#define SESTART                         SP_RPCREQUEST (SET_POST+1)
#define SESTARTREPLY                    SP_RPCREPLY   (SET_POST+1)

#define SESTOP                          SP_RPCREQUEST (SET_SDT+1)
#define SESTOPREPLY                     SP_RPCREPLY (SET_SDT+1)

#define SEGETTOOLTYPE                   SP_RPCREQUEST (SET_POST+3)
#define SEGETTOOLTYPEREPLY              SP_RPCREPLY   (SET_POST+3)

#define SEGETTOOLPID                    SP_RPCREQUEST (SET_POST+4)
#define SEGETTOOLPIDREPLY               SP_RPCREPLY   (SET_POST+4)

#define SEADDTOOL                       SP_RPCREQUEST (SET_POST+5)
#define SEADDTOOLREPLY                  SP_RPCREPLY   (SET_POST+5)

#define SEADDTOOLSUBSCRIPTION           SP_RPCREQUEST  (SET_POST+6)
#define SEADDTOOLSUBSCRIPTIONREPLY      SP_RPCREPLY (SET_POST+6)

#define SESTARTTRACE                    SP_RPCREQUEST (SET_POST+7)
#define SESTARTTRACEREPLY               SP_RPCREPLY   (SET_POST+7)

#define SESTOPTRACE                     SP_RPCREQUEST (SET_POST+8)
#define SESTOPTRACEREPLY                SP_RPCREPLY   (SET_POST+8)

#define SEGETINSTALLDIR                 SP_RPCREQUEST (SET_POST+12)
#define SEGETINSTALLDIRREPLY            SP_RPCREPLY   (SET_POST+12)

/********************* SDT - ORGANIZER **********************************/
#define SESHOWREF                       SP_RPCREQUEST (SET_ORGANIZER+4)
#define SESHOWREFREPLY                  SP_RPCREPLY (SET_ORGANIZER+4)

#define SELISTSYSTEMFILES               SP_RPCREQUEST (SET_ORGANIZER+7)
#define SELISTSYSTEMFILESREPLY          SP_RPCREPLY (SET_ORGANIZER+7)

#define SEOPENSYSTEM                    SP_RPCREQUEST (SET_ORGANIZER+5)
#define SEOPENSYSTEMREPLY               SP_RPCREPLY (SET_ORGANIZER+5)

#define SESAVEALL                       SP_RPCREQUEST (SET_ORGANIZER+3)
#define SESAVEALLREPLY                  SP_RPCREPLY (SET_ORGANIZER+3)

#define SEGETDIRECTORY                  SP_RPCREQUEST (SET_ORGANIZER+18)
#define SEGETDIRECTORYREPLY             SP_RPCREPLY (SET_ORGANIZER+18)

#define SENEWSYSTEM                     SP_RPCREQUEST (SET_ORGANIZER+27)
#define SENEWSYSTEMREPLY                SP_RPCREPLY (SET_ORGANIZER+27)

#define SEADDEXISTING                   SP_RPCREQUEST (SET_ORGANIZER+28)
#define SEADDEXISTINGREPLY              SP_RPCREPLY (SET_ORGANIZER+28)

#define SEADDLOCALLINKFILE              SP_RPCREQUEST (SET_ORGANIZER+32)
#define SEADDLOCALLINKFILEREPLY         SP_RPCREPLY (SET_ORGANIZER+32)

#define SEMERGELOCALLINKFILE            SP_RPCREQUEST (SET_ORGANIZER+33)
#define SEMERGELOCALLINKFILEREPLY       SP_RPCREPLY (SET_ORGANIZER+33)

#define SECONVERTTOGR                   SP_RPCREQUEST (SET_ORGANIZER+34)
#define SECONVERTTOGRREPLY              SP_RPCREPLY (SET_ORGANIZER+34)

#define SESHOWDIAGRAM                   SP_RPCREQUEST (SET_ORGANIZER+38)
#define SESHOWDIAGRAMREPLY              SP_RPCREPLY (SET_ORGANIZER+38)

#define SENEWSTART                      SP_RPCREQUEST (SET_ORGANIZER+46)
#define SENEWSTARTREPLY                 SP_RPCREPLY (SET_ORGANIZER+46)

/***************** EDITORS **********************************************/
#define SEOBTAINGRREF                   SP_RPCREQUEST (SET_SDT+70)
#define SEOBTAINGRREFREPLY              SP_RPCREPLY (SET_SDT+70)

#define SELOAD                          SP_RPCREQUEST (SET_EDITOR+3)
#define SELOADREPLY                     SP_RPCREPLY (SET_EDITOR+3)

#define SEUNLOAD                        SP_RPCREQUEST (SET_EDITOR+4)
#define SEUNLOADREPLY                   SP_RPCREPLY (SET_EDITOR+4)

#define SESHOW                          SP_RPCREQUEST (SET_EDITOR+5)
#define SESHOWREPLY                     SP_RPCREPLY (SET_EDITOR+5)

#define SESAVE                          SP_RPCREQUEST (SET_EDITOR+1)
#define SESAVEREPLY                     SP_RPCREPLY (SET_EDITOR+1)

#define SESDLECREATEDIAGRAM             SP_RPCREQUEST (SET_SDLE+6)
#define SESDLECREATEDIAGRAMREPLY        SP_RPCREPLY (SET_SDLE+6)

#define SESDLECIFCREATEDIAGRAM          SP_RPCREQUEST (SET_SDLE+9)
#define SESDLECIFCREATEDIAGRAMREPLY     SP_RPCREPLY (SET_SDLE+9)
 
#define SESDLECIFCREATEPAGE             SP_RPCREQUEST (SET_SDLE+10)
#define SESDLECIFCREATEPAGEREPLY        SP_RPCREPLY (SET_SDLE+10)
 
#define SESDLECIFINSERTOBJECT           SP_RPCREQUEST (SET_SDLE+11)
#define SESDLECIFINSERTOBJECTREPLY      SP_RPCREPLY (SET_SDLE+11)
 
#define SESDLEBREAKPOINTREF             SP_RPCREQUEST (SET_SDLE+12)
#define SESDLEBREAKPOINTREFREPLY        SP_RPCREPLY (SET_SDLE+12)

#define SEMSCECREATEDIAGRAM             SP_RPCREQUEST (SET_MSCE+3)
#define SEMSCECREATEDIAGRAMREPLY        SP_RPCREPLY (SET_MSCE+3)

#define SEOMECREATEDIAGRAM              SP_RPCREQUEST (SET_OME+1)
#define SEOMECREATEDIAGRAMREPLY         SP_RPCREPLY (SET_OME+1)

#define SEOMECIFCREATEDIAGRAM           SP_RPCREQUEST (SET_OME+5)
#define SEOMECIFCREATEDIAGRAMREPLY      SP_RPCREPLY (SET_OME+5)

#define SEOMECIFCREATEPAGE              SP_RPCREQUEST (SET_OME+6)
#define SEOMECIFCREATEPAGEREPLY         SP_RPCREPLY (SET_OME+6)

#define SEOMECIFINSERTOBJECT            SP_RPCREQUEST (SET_OME+7)
#define SEOMECIFINSERTOBJECTREPLY       SP_RPCREPLY (SET_OME+7)

#define SESHOWOBJECT                    SP_RPCREQUEST (SET_EDITOR+11)
#define SESHOWOBJECTREPLY               SP_RPCREPLY (SET_EDITOR+11)

#define SEGETOBJECTTEXT                 SP_RPCREQUEST (SET_EDITOR+21)
#define SEGETOBJECTTEXTREPLY            SP_RPCREPLY (SET_EDITOR+21)

#define SESELECTOBJECT                  SP_RPCREQUEST (SET_EDITOR+9)
#define SESELECTOBJECTREPLY             SP_RPCREPLY (SET_EDITOR+9)

#define SEINSERTOBJECT                  SP_RPCREQUEST (SET_EDITOR+6)
#define SEINSERTOBJECTREPLY             SP_RPCREPLY (SET_EDITOR+6)

#define SESDLEINSERTOBJECT              SP_RPCREQUEST (SET_SDLE+8)
#define SESDLEINSERTOBJECTREPLY         SP_RPCREPLY (SET_SDLE+8)

#define SEREMOVEOBJECT                  SP_RPCREQUEST (SET_EDITOR+7)
#define SEREMOVEOBJECTREPLY             SP_RPCREPLY (SET_EDITOR+7)

#define SEMENUADD                       SP_RPCREQUEST (SET_SDT+51)
#define SEMENUADDREPLY                  SP_RPCREPLY (SET_SDT+51)

#define SEMENUDELETE                    SP_RPCREQUEST (SET_SDT+52)
#define SEMENUDELETEREPLY               SP_RPCREPLY (SET_SDT+52)

#define SEMENUCLEAR                     SP_RPCREQUEST (SET_SDT+53)
#define SEMENUCLEARREPLY                SP_RPCREPLY (SET_SDT+53)

#define SEMENUADDITEM                   SP_RPCREQUEST (SET_SDT+54)
#define SEMENUADDITEMREPLY              SP_RPCREPLY (SET_SDT+54)

#define SEMENULOAD                      SP_RPCREQUEST (SET_SDT+55)
#define SEMENULOADREPLY                 SP_RPCREPLY (SET_SDT+55)

#define SEMENUSAVE                      SP_RPCREQUEST (SET_SDT+56)
#define SEMENUSAVEREPLY                 SP_RPCREPLY (SET_SDT+56)

#define SEPOPUP	                        SP_RPCREQUEST (SET_SDT+60)
#define SEPOPUPREPLY                    SP_RPCREPLY (SET_SDT+60)

#define SEDISPLAYKEY                    SP_RPCREQUEST (SET_SDT+71)
#define SEDISPLAYKEYREPLY               SP_RPCREPLY (SET_SDT+71)

#define SELISTKEY                       SP_RPCREQUEST (SET_SDT+72)
#define SELISTKEYREPLY                  SP_RPCREPLY (SET_SDT+72)

#define SECREATEATTRIBUTE               SP_RPCREQUEST (SET_SDT+73)
#define SECREATEATTRIBUTEREPLY          SP_RPCREPLY (SET_SDT+73)

#define SEREADATTRIBUTE                 SP_RPCREQUEST (SET_SDT+74)
#define SEREADATTRIBUTEREPLY            SP_RPCREPLY (SET_SDT+74)

#define SEUPDATEATTRIBUTE               SP_RPCREQUEST (SET_SDT+75)
#define SEUPDATEATTRIBUTEREPLY          SP_RPCREPLY (SET_SDT+75)

#define SEDELETEATTRIBUTE               SP_RPCREQUEST (SET_SDT+76)
#define SEDELETEATTRIBUTEREPLY          SP_RPCREPLY (SET_SDT+76)

#define SEGRPR                          SP_RPCREQUEST (SET_ANALYZER+3)
#define SEGRPRREPLY                     SP_RPCREPLY (SET_ANALYZER+3)

#define SEMSCEGRPR                      SP_RPCREQUEST (SET_MSCE+5) 
#define SEMSCEGRPRREPLY                 SP_RPCREPLY (SET_MSCE+5)
 
#define SEHMSCEGRPR                     SP_RPCREQUEST (SET_OME+8) 
#define SEHMSCEGRPRREPLY                SP_RPCREPLY (SET_OME+8) 

#define SETIDYUP                        SP_RPCREQUEST (SET_EDITOR+17)
#define SETIDYUPREPLY                   SP_RPCREPLY (SET_EDITOR+17)

#define SETECREATEDIAGRAM               SP_RPCREQUEST (SET_TE+1)
#define SETECREATEDIAGRAMREPLY          SP_RPCREPLY (SET_TE+1)

#define SETESHOWPOSITION                SP_RPCREQUEST (SET_TE+3)
#define SETESHOWPOSITIONREPLY           SP_RPCREPLY (SET_TE+3)

#define SETESELECTTEXT                  SP_RPCREQUEST (SET_TE+4)
#define SETESELECTTEXTREPLY             SP_RPCREPLY (SET_TE+4)

#define SEGETDIAGRAMINFO                SP_RPCREQUEST (SET_EDITOR+23)
#define SEGETDIAGRAMINFOREPLY           SP_RPCREPLY (SET_EDITOR+23)

/***************** INFOSERVER **********************************************/
#define SELOADDEFINITIONMAP             SP_RPCREQUEST (SET_INFOSERVER+3)
#define SELOADDEFINITIONMAPREPLY        SP_RPCREPLY (SET_INFOSERVER+3)

/***************** OM INFOSERVER *******************************************/
#define SEGETOBJECTS                    SP_RPCREQUEST (SET_OMINFOSERVER+1)
#define SEGETOBJECTSREPLY               SP_RPCREPLY (SET_OMINFOSERVER+1)

/***************** SIMULATORS **********************************************/
#define SESIMUICOMMAND                  SP_RPCREQUEST (SET_SIMULATOR_UI+1)
#define SESIMUICOMMANDREPLY             SP_RPCREPLY (SET_SIMULATOR_UI+1)

#define SESDLSIGNAL                     SP_MESSAGE (SET_SIMULATOR+1)
#define SESIMULATORCOMMAND              SP_MESSAGE (SET_SIMULATOR+2)

#define SESIMUISTRING                   SP_RPCREQUEST (SET_SIMULATOR_UI+3)
#define SESIMUISTRINGREPLY              SP_RPCREPLY (SET_SIMULATOR_UI+3)

#define SESIMUICHANGE                   SP_RPCREQUEST (SET_SIMULATOR_UI+2)
#define SESIMUICHANGEREPLY              SP_RPCREPLY (SET_SIMULATOR_UI+2)

#define SESIMUIINTERNCOMMAND            SP_RPCREQUEST (SET_SIMULATOR_UI+7)
#define SESIMUIINTERNCOMMANDREPLY       SP_RPCREPLY (SET_SIMULATOR_UI+7)

#define SEVALCOM                        SP_RPCREQUEST (SET_VALIDATOR+1)
#define SEVALCOMREPLY                   SP_RPCREPLY (SET_VALIDATOR+1)

/***************** COVERAGEVIEWER ******************************************/

#define SECOVOPEN                       SP_RPCREQUEST (SET_COVERAGEVIEWER+1)
#define SECOVOPENREPLY                  SP_RPCREPLY   (SET_COVERAGEVIEWER+1)

#define SECOVMODE                       SP_RPCREQUEST (SET_COVERAGEVIEWER+2)
#define SECOVMODEREPLY                  SP_RPCREPLY   (SET_COVERAGEVIEWER+2)

#define SECOVVISIBILITY                 SP_RPCREQUEST (SET_COVERAGEVIEWER+3)
#define SECOVVISIBILITYREPLY            SP_RPCREPLY   (SET_COVERAGEVIEWER+3)

/***************** NOTIFICATIONS **********************************************/
#define SESTARTNOTIFY                   SP_MESSAGE (SET_POST+5)
#define SEOPFAILED                      SP_MESSAGE (SET_POST+4)
#define SESTOPNOTIFY                    SP_MESSAGE (SET_SDT+3)
#define SESTRUCTURENOTIFY               SP_MESSAGE (SET_ORGANIZER+1)
#define SEDIRECTORYNOTIFY               SP_MESSAGE (SET_ORGANIZER+3)
#define SELOADNOTIFY                    SP_MESSAGE (SET_EDITOR+1)
#define SEUNLOADNOTIFY                  SP_MESSAGE (SET_EDITOR+2)
#define SEDIRTYNOTIFY                   SP_MESSAGE (SET_EDITOR+4)
#define SESAVENOTIFY                    SP_MESSAGE (SET_EDITOR+5)
#define SESDLENEWNOTIFY                 SP_MESSAGE (SET_SDLE+1)
#define SEMSCENEWNOTIFY                 SP_MESSAGE (SET_MSCE+2)
#define SEMSCEADDNOTIFY                 SP_MESSAGE (SET_MSCE+3)
#define SEOMENEWNOTIFY                  SP_MESSAGE (SET_OME+1)


/***************** SPMList **********************************************/
/* Moved to post.h  but still here for reference purpose */
/*

SPMList spMList[] = 
{{ "post",              SET_POST},
 { "extern",            SET_EXTERN},
 { "sdt",               SET_ORGANIZER},
 { "organizer",         SET_ORGANIZER},
 { "sdle",              SET_SDLE},
 { "msce",              SET_MSCE},
 { "ome",               SET_OME},
 { "te",                SET_TE},
 { "info",              SET_INFOSERVER},
 { "simulator",         SET_SIMULATOR},
 { "sdlenv",            SET_SDLENV},

 { "start",             SESTART},
 { "stop",              SESTOP},
 { "gettool",           SEGETTOOLTYPE},
 { "getpid",            SEGETTOOLPID},
 { "addtool",           SEADDTOOL},
 { "addtoolsubscr",     SEADDTOOLSUBSCRIPTION},
 { "starttrace",        SESTARTTRACE},
 { "stoptrace",         SESTOPTRACE},

 { "showsource",        SESHOWREF},
 { "listsystem",        SELISTSYSTEMFILES},
 { "newsystem",         SENEWSYSTEM},
 { "open",              SEOPENSYSTEM},
 { "saveall",           SESAVEALL},
 { "addexisting",       SEADDEXISTING},
 { "showdiagram",       SESHOWDIAGRAM},
 { "addlocallinkfile",  SEADDLOCALLINKFILE},
 { "mergelocallinkfile",SEMERGELOCALLINKFILE},
 { "converttosdlgr",    SECONVERTTOGR},

 { "obtainsource",      SEOBTAINGRREF},
 { "load",              SELOAD},
 { "unload",            SEUNLOAD},
 { "show",              SESHOW},
 { "save",              SESAVE},
 { "sdlcreate",         SESDLECREATEDIAGRAM},
 { "sdlcifcreate",      SESDLECIFCREATEDIAGRAM},
 { "sdlcifcreatepage",  SESDLECIFCREATEPAGE},
 { "sdlcifinsertobject",SESDLECIFINSERTOBJECT},
 { "grpr",              SEGRPR},
 { "mscgrpr",           SEMSCEGRPR},
 { "hmscgrpr",          SEHMSCEGRPR},
 { "tidyup",            SETIDYUP},
 { "msccreate",         SEMSCECREATEDIAGRAM},
 { "omcreate",          SEOMECREATEDIAGRAM},
 { "omcifcreate",       SEOMECIFCREATEDIAGRAM},
 { "omcifcreatepage",   SEOMECIFCREATEPAGE},
 { "omcifinsertobject", SEOMECIFINSERTOBJECT},
 { "selectobject",      SESELECTOBJECT},
 { "getobjecttext",     SEGETOBJECTTEXT},
 { "showobject",        SESHOWOBJECT},
 { "sdlinsertobject",   SESDLEINSERTOBJECT},
 { "mscinsertobject",   SEINSERTOBJECT},
 { "removeobject",      SEREMOVEOBJECT},
 { "getdiagraminfo",    SEGETDIAGRAMINFO},
 { "menuload",          SEMENULOAD},
 { "menusave",          SEMENUSAVE},
 { "menuadd",           SEMENUADD},
 { "menudelete",        SEMENUDELETE},
 { "menuclear",         SEMENUCLEAR},
 { "menuadditem",       SEMENUADDITEM},
 { "displaykey",        SEDISPLAYKEY},
 { "listkey",           SELISTKEY},
 { "createattribute",   SECREATEATTRIBUTE},
 { "readattribute",     SEREADATTRIBUTE},
 { "updateattribute",   SEUPDATEATTRIBUTE},
 { "deleteattribute",   SEDELETEATTRIBUTE},

 { "loaddefinition",    SELOADDEFINITIONMAP},

 { "teshowposition",    SETESHOWPOSITION},
 { "teselecttext",      SETESELECTTEXT},
 { "tecreate",          SETECREATEDIAGRAM},

 { "base",              IET_BASE},
 { "itex",              IET_BASE},  alias for base 
 { "extern",            IET_EXTERN},
 { "access",            IET_ACCESS},
 { "simui",             IET_SIMULATOR_UI},
 { "ccg",               IET_C_CODE_GENERATOR},

 { "openeddocuments",           IEBXOPENEDDOCUMENTS},
 { "getbuffidfrompath",         IEBXGETBUFFIDFROMPATH},
 { "getbuffidfrommppath",       IEBXGETBUFFIDFROMMPPATH},

 { "save",                      IEBXSAVE},
 { "selectall",                 IEBXSELECTALL},
 { "deselectall",               IEBXDESELECTALL},
 { "isselected",                IEBXISSELECTED}, 
 { "closedocument",             IEBXCLOSEDOCUMENT},
 { "getpath",                   IEBXGETPATH},
 { "getmppath",                 IEBXGETMPPATH},
 { "getmodifytime",             IEBXGETDOCUMENTMODIFYTIME},
 { "findtable",                 IEBXFINDTABLE},
 { "converttomp",               IEBXCONVERTTOMP}, 
 { "analyze",                   IEBXANALYZE},

 { "closetable",                IEBXCLOSETABLE},
 { "gettablestate",             IEBXGETTABLESTATE},
 { "getrownumber",              IEBXGETROWNUMBER},
 { "selectrow",                 IEBXSELECTROW},
 { "clearselection",            IEBXCLEARSELECTION},
 { "rowsselected",              IEBXROWSSELECTED},
 { "convertseltomp",            IEBXCONVERTSELTOMP},
 { "converttogr",               IEBXCONVERTTOGR},
 { "mergedocuments",            IEBXMERGEDOCUMENTS},
 { "selector",                  IEBXSELECTOR},

 { "",                          SESIMUICOMMAND},not for external use 
 { "",                          SEVALCOM},      not for external use 

 { "",                          0}
};
  */

#endif /* _sdt_h */
