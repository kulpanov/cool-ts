/************************************************************
 * Copyright (c) 1994, 1999  TeleLOGIC Malmoe AB. All rights reserved.
 *
 * File:   $Id: 
 * Author: Jan Karlsson & Per Madsen 1994
 *
 * Module: SDT/ITEX Tool Communication Definitions
 ************************************************************/

/* These definitions are a part of the complete ITEX postmaster definitions
   used by the code generator.
*/

#ifndef _itex_h
#define _itex_h

#define IET_BASE                40000 /* The TTCN Editor */
#define IET_EXTERN              41000 /* Remote commands (Public interface) */
#define IET_TIMEMANAGER         42000 /* Bridge between SDT/ITEX simulators */
#define IET_ACCESS              43000 /* Access library applications */
#define IET_SIMULATOR_UI        44000 /* Simulator user interface (controller) */
#define IET_SIMULATOR           45000 /* Simulator processes (MTC,PTC) */
#define IET_HELP                46000 /* The on-line help tool */
#define IET_C_CODE_GENERATOR    47000 /* The C code generator */
#define IET_ASN_CODER_FRAMEWORK 48000 /* For Asn1util */
#define IET_TARGET_SIMULATOR    49000 /* Target Simulator */

/* IET_BASE */

#define IEBCONVERTTOGR                  SP_RPCREQUEST (IET_BASE + 1)
#define IEBCONVERTTOGRREPLY             SP_RPCREPLY   (IET_BASE + 1)

#define IEBLOADSYSTEM                   SP_RPCREQUEST (IET_BASE + 2)
#define IEBLOADSYSTEMREPLY              SP_RPCREPLY   (IET_BASE + 2)

#define IEBUNLOADSYSTEM                 SP_RPCREQUEST (IET_BASE + 3)
#define IEBUNLOADSYSTEMREPLY            SP_RPCREPLY   (IET_BASE + 3)

#define IEBADDDOCUMENT                  SP_RPCREQUEST (IET_BASE + 4)
#define IEBADDDOCUMENTREPLY             SP_RPCREPLY   (IET_BASE + 4)

#define IEBREMOVEDOCUMENT               SP_RPCREQUEST (IET_BASE + 5)
#define IEBREMOVEDOCUMENTREPLY          SP_RPCREPLY   (IET_BASE + 5)

#define IEBCONNECTDOCUMENT              SP_RPCREQUEST (IET_BASE + 6)
#define IEBCONNECTDOCUMENTREPLY         SP_RPCREPLY   (IET_BASE + 6)

#define IEBDISCONNECTDOCUMENT           SP_RPCREQUEST (IET_BASE + 7)
#define IEBDISCONNECTDOCUMENTREPLY      SP_RPCREPLY   (IET_BASE + 7)

#define IEBCONVERTTOMP                  SP_RPCREQUEST (IET_BASE + 8)
#define IEBCONVERTTOMPREPLY             SP_RPCREPLY   (IET_BASE + 8)

#define IEBPRINT                        SP_RPCREQUEST (IET_BASE + 9)
#define IEBPRINTREPLY                   SP_RPCREPLY   (IET_BASE + 9)

#define IEBANALYZE                      SP_RPCREQUEST (IET_BASE + 10)
#define IEBANALYZEREPLY                 SP_RPCREPLY   (IET_BASE + 10)

#define IEBMAKESYSTEM                   SP_RPCREQUEST (IET_BASE + 11)
#define IEBMAKESYSTEMREPLY              SP_RPCREPLY   (IET_BASE + 11)

#define IEBSIMULATOR                    SP_RPCREQUEST (IET_BASE + 12)
#define IEBSIMULATORREPLY               SP_RPCREPLY   (IET_BASE + 12)

#define IEBSTARTACCESS                  SP_RPCREQUEST (IET_BASE + 13)
#define IEBSTARTACCESSREPLY             SP_RPCREPLY   (IET_BASE + 13)

#define IEBGENERATEFLATVIEW             SP_RPCREQUEST (IET_BASE + 14)
#define IEBGENERATEFLATVIEWREPLY        SP_RPCREPLY   (IET_BASE + 14)

#define IEBSAVE                         SP_RPCREQUEST (IET_BASE + 15)
#define IEBSAVEREPLY                    SP_RPCREPLY   (IET_BASE + 15)

#define IEBMODIFYDOCUMENT               SP_RPCREQUEST (IET_BASE + 16)
#define IEBMODIFYDOCUMENTREPLY          SP_RPCREPLY   (IET_BASE + 16)

#define IEBFINDTABLE                    SP_RPCREQUEST (IET_BASE + 17)
#define IEBFINDTABLEREPLY               SP_RPCREPLY   (IET_BASE + 17)

#define IEBUPDATEASSOCLIST              SP_RPCREQUEST (IET_BASE + 18)
#define IEBUPDATEASSOCLISTREPLY         SP_RPCREPLY   (IET_BASE + 18)

#define IEBDOORSEXTRACTSTRUCTURE	SP_RPCREQUEST (IET_BASE + 20)
#define IEBDOORSEXTRACTSTRUCTUREREPLY	SP_RPCREPLY   (IET_BASE + 20)

#define IEBDOORSNAVIGATE		SP_RPCREQUEST (IET_BASE + 21)
#define IEBDOORSNAVIGATEREPLY		SP_RPCREPLY   (IET_BASE + 21)

#define IEBDOORSSETID			SP_RPCREQUEST (IET_BASE + 22)
#define IEBDOORSSETIDREPLY		SP_RPCREPLY   (IET_BASE + 22)

/* Notifications which are issued by ITEX. */

#define IEBRENAMEDDOCUMENTNOTIFY        SP_MESSAGE    (IET_BASE + 1)
#define IEBCONNECTEDDOCUMENTNOTIFY      SP_MESSAGE    (IET_BASE + 2)
#define IEBSHOWNOTIFY                   SP_MESSAGE    (IET_BASE + 3)
#define IEBUNSHOWNOTIFY                 SP_MESSAGE    (IET_BASE + 4)

/* Notifications which are sent to ITEX. */

#define IEBXABORTANALYSISNOTIFY         SP_MESSAGE    (IET_BASE + 50)

/* IET_TIMEMANAGER */

#define IETMSCHEDULE                    SP_RPCREQUEST (IET_TIMEMANAGER+1)
#define IETMSCHEDULEREPLY               SP_RPCREPLY   (IET_TIMEMANAGER+1)

#define IETMGETSIGNAL                   SP_RPCREQUEST (IET_TIMEMANAGER+2)
#define IETMGETSIGNALREPLY              SP_RPCREPLY   (IET_TIMEMANAGER+2)

#define IETMSENDSIGNAL                  SP_RPCREQUEST (IET_TIMEMANAGER+3)
#define IETMSENDSIGNALREPLY             SP_RPCREPLY   (IET_TIMEMANAGER+3)

#define IETMSIMULATORSTARTED            SP_RPCREQUEST (IET_TIMEMANAGER+4)
#define IETMSIMULATORSTARTEDREPLY       SP_RPCREPLY   (IET_TIMEMANAGER+4)

#endif /* _itex_h */
