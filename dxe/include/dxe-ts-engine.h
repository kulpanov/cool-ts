#ifndef DXETESTSYSTEM_GLOBAL_H
#define DXETESTSYSTEM_GLOBAL_H

#if defined(DXETESTSYSTEM_LIBRARY)
#define DLL_DECL __declspec(dllexport)
#else

#define DLL_DECL __declspec(dllimport)
#endif

#include <QtCore/QPair>
#include <QtCore/QVector>
#include <QtCore/QString>

namespace DXE{

/** SMsg_log структура передачи сообщений в лог
 */
struct DLL_DECL SMsg_log{
  //кто запустил (ex 1.2)
  QString id;
  //сообщение
  QString msg;
  //код возврата
  int result;
  //показывать ли полную дату
  bool fulldate;
public:
  SMsg_log(const QString& _id, const QString& _msg
      , int _result, bool _fulldate = false);

  SMsg_log(const SMsg_log& _msg);

  SMsg_log();

  QString toString() const;
};

/** Логгер работы
 */
class DLL_DECL ILogListener{
public:
  enum{
    pass = 0  // успешно
    ,fail     //не успешно
    ,msg      //с сообщением
    ,started  //стартовал
  };
  enum{
    fulldate = true //вывести полную дату
  };
  enum{
    dlgRTP = 0
  };

  virtual void recvMsg(const SMsg_log&) = 0;

  virtual void recvMsg(const QString& _fname, const QString& _fname_script, const QVector<QPair<double, double> >& _vec) = 0;

  virtual void runDlg(int , void*) = 0;

protected:
  virtual ~ILogListener(){  }
};
typedef ILogListener* ILogListener_ptr;

/** \brief CTestScript - класс сценария испытаний
 * */
class DLL_DECL CTestScript {
public:
  ///завершить выполнения сценариев при первой возможности
  static bool need_cancel;
  static int count_executers;
  enum {nowaitForExecute = true, waitForExecute = false};
#define dontwaitForExecute CTestScript::nowaitForExecute
public:
  static QString testsPath;
public:
  /** Иниициализировать подсистему исполнения сценариев
   * @param _log_listener - логгер работы  */
  static void init(ILogListener* _log_listener = NULL);

  ///Завершить работу, освободить ресурсы
  static void release();

  /** Выполнить файл сценария
   * @param _fname имя файла сценария
   * @param _logid имя ветки в журнале работы сценария
   * @param _nowait запустить параллельно, без ожидания завершения
   * @return результат испытаний: прошел/не прошел   */
  static bool executeFile(const QString& _fname, const QString& _logid = ".", bool _nowait = false);

  /** Ожидать выполнение всех сценариев.
   */
  static void waitForDoneOfExecute();

  ///Отменить выполнение всех запущенных сценариев
  static void cancelAll();
 };

}


#endif // DXETESTSYSTEM_GLOBAL_H
