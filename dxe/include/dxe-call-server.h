/** \brief Интерфейс класса сервера.
 *  Created on: 16.01.2012
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef DXE_VMS_SERVER_H_
#define DXE_VMS_SERVER_H_

#include <string>
#include "../../dxe-utils/utils/utils.h"
#include "../../dxe-utils/utils/netutils.h"
#include "wavStreams.h"

//TODO: весь dxe-call-server сделать с поддержкой Qt?
///Пространство имён DXE
namespace DXE {

/** \brief Интерфейс управляющего соединением.
 * Управляет одним соединением, порождается ICallService
 * при установке входящего соединения, или
 * при запросе клиента об исходящем соединении
 * Интерфейс работы с внешним клиентом(верхний уровень) */
class ICallManager{
public:
  ///Статусы соединения
  enum EStatus{
    _reserved = -1      //резервирован, не может быть использован для входящего
    ,_free = 0
    ,_idle = _free          //свободен
    ,_call_out1           //исх step1
    ,_call_out2           //исх step2
    ,_call_in               //вход
    ,_busy                  //установлен
    ,_call_off              //закрываюсь
  };
static std::string EStatustoString(volatile int s)
  {return s==EStatus::_free?"free"
      :s==EStatus::_call_out1?"call_out1"
      :s==EStatus::_call_out2?"call_out2"
      :s==EStatus::_call_in?"call_in"
      :s==EStatus::_busy?"busy"
      :s==EStatus::_call_off?"call_off":"other";}
static const uint32_t infinity = (uint32_t)-1;

public:
//Методы получения статуса управляющего
///получить текущий статус соединения
  virtual int getStatus() = 0;
///получить последнее сообщение
  virtual const CString& getLastMsg() = 0;
///объект резервирован для будущего использования
  virtual bool isReserved() = 0;
///Занят, соединён, обрабатываю вызов
  virtual bool isBusy() = 0;
///Свободен, ожидаю соединения
  virtual bool isFree() = 0;
///Выполняю исходящий вызов, но не соединён
  virtual bool isCalling() = 0;
public:
  //
  enum EListen_Variant{
    _variantDef = 0, _variantAudio, _lastVariant
  };

  //Методы работы с RTP-потокам, на транслирование и прослушку.
/** Задать слушающий RTP-поток.
 * @param _wav RTP-поток куда будут поступать входящий звуковой поток
 * @return код ошибки , 0-OK*/
  virtual int setListener(IWavStream *_wav, EListen_Variant _variant = _variantDef) = 0;
///Слушаю, слушающий поток задан
  virtual int isListening() = 0;

/** Остановить сохранение входящего звукового потока
 * После вызова все вхдящие RTP-пакеты сохранятся где-либо не будут\
 * @param _release - при этом удалить ранее установленный объект RTP-потока. */
  virtual void stopListen(EListen_Variant _variant = _lastVariant, bool _release = true) = 0;
public:
  /** Задать транслирующий RTP-поток
   * @param _wav RTP-поток откуда будут поступать данные для транслирования
   * исходящего звукового трафика
   * @return код ошибки, 0-OK   */
  virtual int setPlayer(IWavStream* _wav) = 0;;
  ///Проигрываю поток
  virtual int isPlaying() = 0;;
  /** Остановить проигрывание RTP-потока.
   * После вызова этого метода управляющий будет транслировать тишину.
   * (нули в линейноом коде)
   * @param _release при этом удалить ранее заданный объект RTP-потока  */
  virtual void stopPlaying(bool _release = true) = 0;

public:
  /** Выполнить исходящий вызов.
   * @param _number номер вызова
   * @param _add номер источника вызова
   * @param _timeout макс время ожидания установления соединения
   * @param _s_ip IPадрес шлюза, через которого выполняется соединение.
   * @return код возврата
   * 0 - соединение устновлено
   * ETIMEDOUT - вышло макс время на установление соединения   */
  virtual int call(const CString& _number, const CString& _add
      , int _timeout, IP _s_ip) = 0;;

  /** Установить параметры ожидания входящего вызова
   * @param _number - номер вызова
   * @param _add - доп номер вызова
   * @param _s_ip - источник вызова
   * @return  */
  virtual int setConnectData(const CString& _number, const CString& _add
      , const IP& _s_ip) = 0;

public:
  ///Завершить соединение
  virtual void close(){}//= 0;;
  ///Удалить объект, осводить ресурсы
  virtual ICallManager* release() = 0;
public:
  //Методы получения информации об последнем установленном соединении
  ///Получить номер, с кем установлено соединение
  virtual CString getConnectNumber() = 0;
  ///Получить номер, источника соединения
  virtual CString getConnectDigits() = 0;
  ///Получить IP-шлюза соединения
  virtual IP getConnectIP() = 0;

public:
  enum{
    rtpRestoreDef
    ,rtpPacketSize
    ,rtpJitter
    ,rtpQuant
  };
  virtual int setParams(int param, int arg) = 0;
public:
#define WSEC(ms) ms*1000
  //группа методов ожидания события на соединении
  ///пауза, ms
  virtual int waitfor(uint32_t _ms) = 0;;
  ///ждать установления соединения, не более ms
  virtual int waitforCall(uint32_t _ms = infinity) = 0;

  enum {andReserv = true};
  ///ждать завершения звонка, не более ms
  virtual int waitforClose(uint32_t _ms = infinity, bool _andReserv = false) = 0;
protected:
   virtual ~ICallManager(){ }
};
#define ICall ICallManager

/** \brief Интерфейс управляющего соединением.
 * Интерфейс работы с SIP протоколом(нижним уровнем)*/
class ICallManager_SIP{
public:
  /** Начать входящее соединение
   * @param _id
   * @param _number
   * @param _add
   * @param _s_ip
   */
  virtual void putInvite(const int _id, const CString& _number, const CString& _add
      , const IP& _s_ip) = 0;

  /** Начать исходящее соединение
   * @param _id
   * @param _number
   * @param _add
   * @param _s_ip */
  virtual void putSetup_res(const int _id, const CString& _number, const CString& _add
      , const IP& _s_ip) = 0;

  /** \brief Подать SIP Info сигнал
   * Принять и обработать SIP INFO сигнал
   * @param _data1 данные
   */
  virtual void putSIP_Info(int _data1, int _data2, int _data3) = 0;

  /** Отбить входящие соединение
   */
  virtual void putRelease(const int _id) = 0;

  /** Принять входщие соединение
   */
  virtual void putConnect(const int _id) = 0;

  /** Установить порты RTP
   * @param _d_rtp
   * @param _d_ip
   */
  virtual void putWrSenderTable(int _d_rtp, int _d_ip) = 0;
protected:
   virtual ~ICallManager_SIP(){ }
};



/** ICallService - Интерфейс диспетчера соединений.
 * Основной класс работы сервиса голосовых сообщений. */
class ICallService {
public:
  /** \brief Cоздать/вернуть управляющего соединением
   * @return ICallManager
   */
  virtual ICallManager* createCallManager(
      ICallManager::EStatus _init = ICallManager::_free) = 0;

  /**
   * @param call
   */
  virtual void releaseCallManager(const ICallManager* call) = 0;

  /** \brief Закрыть все соединени
   *    */
  virtual void closeAll() = 0;

  /** \brief Старт работы системы
   * @param _opts - опции запуска, не используется */
  virtual void start(const std::string& _opts) = 0;

  /** Удалить экземпляр объекта.
   * Завершает все соединения, закрывает все ресурсы,
   * @return NULL
   */
  virtual ICallService* release(){return NULL;}// bug in QSharedPointer = 0;

  ///
  virtual void loadParams(int type = 0) = 0;

  ///Сохранить параметры, метод интерфейса
  virtual void saveParams() = 0;
  ///
  virtual const pair<const IP&, const IP&> getIPNet() = 0;
  ///Установить параметры DTMF
  virtual void setDTMFParams(int _ampl, int _duration, int _pause) = 0;
public:
  virtual ~ICallService(){ release(); }//только через release
public:
  ///make-функция создаения экземпляра класса
  static ICallService* create(const std::string& _opts);
};

}


#endif /* SMAINDATA_H_ */
