#ifndef UTILS_H
#define UTILS_H
//class CPacket_RTP;

#include <string>
#include <map>
#include <stdlib.h>
#include <stdio.h>
//#include <QString>

#include "utils/netutils.h"
using namespace std;

//
#if defined(DXEUTILS_LIBRARY)
#  define DXEUTILSSHARED_EXPORT Q_DECL_EXPORT
#else
#  define DXEUTILSSHARED_EXPORT Q_DECL_IMPORT
#endif


extern int16_t a2l(uint8_t _a);

extern uint8_t l2a(int16_t _l);

////////////////////////////////////////////////////////////////////////////////
/** \brief Интерфейс класса звукового RTP-потока
 */
class IWavStream {
private:
  int ref_count;
public:
/*  IWavStream* operator=(IWavStream* _lhs){
    if(_lhs==NULL){
      if(0 == --ref_count){
        //release();
        LOG1(INFO, "stream:0x%x, ref_count==0, must be call release\n", (int)this);
      }
    }else
      ref_count ++;
    LOG2(INFO, "stream:0x%x, ref_count=%d\n", (int)this, ref_count);
    return this;
  }*/
  IWavStream* release();
public:
  ///Сбросить маркер, перейти в начало
  virtual void reset(){ };
  ///Закрыть все системные ресурсы, открытые потоком
  virtual void close(){ };
  ///Прочитать звуковой поток, сформировать RTP пакет
  virtual CPacket_RTP* read(){ return NULL; };
  ///Записать RTP-пакет в звуковой поток
  virtual void write(CPacket_RTP* _packet){ (void)_packet; };
  ///Проверить на маркер конца потока
  virtual bool isEndOfStream(){return false;};
  ///Удалить экземпляр потока

  virtual bool isFull(){return false;};

protected:
  double dB;
  double freq;
  uint8_t* data;
  int pcktSize;
public:
  double getdB(){return dB;};
  double getFreq(){return freq;};
  int packetSize(int _psz = 0){
    if(_psz > CPacket_RTP::length) _psz = CPacket_RTP::length;
    if(_psz > 0) pcktSize = _psz;
    return pcktSize;
  }
protected:
  virtual ~IWavStream ();

  IWavStream();

public:
  /**Make-функция создания экземпляра класса
   * @param _opt строка опций потога
   * @return экземпляр класса  */
  static IWavStream* create(const std::string& _opt, std::string& _strerror);
  static std::string wavsPath;
  static int rtp_packet_size;
};

#define IWav IWavStream

#endif // UTILS_H
