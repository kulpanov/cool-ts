/*
 * dxe-ts-utils.h
 *
 *  Created on: 03.02.2012
 *      Author: Kulpanov
 */

#ifndef DXE_TS_UTILS_H_
#define DXE_TS_UTILS_H_

#include <QObject>
#include <QSharedPointer>
#include <QVector>
#include <QtAlgorithms>
#include "dxe-call-server.h"
#include "dxe-ts-engine.h"


using namespace std;

namespace DXE{
using namespace DXE;
/** \brief Базовый класс тестера потоков
 *
 */
class DLL_DECL CWavTester {
  IWavStream* stream_gauge;
  IWavStream* stream_fortest;
  int work_time;
  static const int begin_offset = 0;
  static const int end_offset = 0;
protected:
  //максимальная разница
  double max_error_Db;
//  //параметры идеального потока
//  double gauge_min_Db;
//  double gauge_max_Db;
  //параметры тестируемого потока
  double fortest_min_Db;
  double fortest_max_Db;
public:
  bool saveTestStreamToFile;
public:
  double getMaxError(){return max_error_Db;};
//  double getGaugeMingb(){return gauge_min_Db;};
//  double getGaugeMaxDb(){return gauge_max_Db;};
  double getForTestMinDb(){return fortest_min_Db;};
  double getForTestMaxDb(){return fortest_max_Db;};
public:
  CWavTester(IWavStream* _stream_gauge, IWavStream* _stream_fortest, int _measure_time);

  bool test(int _time_ms);
  bool test2(int _time_ms);

  virtual ~CWavTester(){
//    stream_gauge = stream_gauge->release();
//    stream_fortest = stream_fortest->release();
  }
};

/** Логгер работы стенда испытаний
 *  */
class CLogger{
public:
  static const QString str_empty;
  static const QString str_dash;
public:
  //методы логгирования
  //void scriptCall(const QString& _descr);
///Сценарий стартовал
  void scriptBegin(const QString& _descr);
///Сценарий завершен неудачно
  void scriptFail(const QString& _descr, const QString& _msg = str_empty);
///Сценарий завершен удачно
  void scriptPass(const QString& _descr);
///Акт стартовал
  void actBegin(const QString& _descr, const QString& _params = str_empty);
///Акт завершен удачно
  void actPass(const QString& _descr, const QString& _correct_id = str_empty);
///Акт сообщает
  void actMessage(const QString& _msg);
///Акт завершен н
  void actFail(const QString& _descr, const QString& _msg = str_empty, const QString& _correct_id = str_empty);

///Установить уровень
  QSharedPointer<CLogger> getChild(int _n);

  void scriptVec(const QString& _fname, const QVector<QPair<double, double> >&_logvec){
    if(listener)
      listener->recvMsg(_fname, _logvec);
  }
public:
  CLogger(ILogListener* _log_listener, QString _logId):
    listener(_log_listener), logId(_logId)
  {  }

private:
  inline void logTo(const SMsg_log& _logmsg){
    LOG1(INFO, "%s\n", _logmsg.toString().toUtf8().begin());
    if(listener)
      listener->recvMsg(_logmsg);
  }

private:
  ILogListener_ptr listener;
  QString logId;
  //int level;
};
typedef QSharedPointer<CLogger> CLogger_ptr;


/** \brief Установившееся соединение.
 *
 */
class CConnection {
protected:
  CLogger_ptr logger;
  ICallManager* call_out;
  string play_opt_call_out;
  ICallManager* call_in;
  string play_opt_call_in;
public:
//  CConnection operator = (const CConnection& _lhs){
//
//  }
//
//  CConnection&  operator=(CConnection&& _lhs) {
//    *this = _lhs;
//    return *this;
//  }
  std::string &stream_byDir(int _pos){
    return (_pos == 0)? play_opt_call_out : play_opt_call_in;
  }

  ICallManager* call_byDir(int _pos){
    return (_pos == 0)? call_out : call_in;
  }

  CConnection(ICallManager* m1, ICallManager* m2, CLogger_ptr _log);

  virtual ~CConnection();
};
typedef QSharedPointer<CConnection> CConnection_ptr;

/** \brief Список соединений
 */
class CListOfConnections:public QVector<CConnection_ptr>{
  typedef QVector<CConnection_ptr> base;
public:

  CConnection_ptr at(int i){
    if(i>=size()){
      return CConnection_ptr();
    }
    return operator[](i);
  }

public:
  CListOfConnections()
  :base(127){//макс 127*2=254!
    qFill(begin(), end(), CConnection_ptr());
  }

  virtual ~CListOfConnections(){
//всё само!
//    for(auto it=begin(); it!=end(); ++it)
//      delete (*it);
  }
};
typedef QSharedPointer<CListOfConnections> CListOfConnections_ptr;

typedef QSharedPointer<ICallService> ICallService_ptr;
typedef QSharedPointer<ICallManager> ICallManager_ptr;

}

#endif /* DXE_TS_UTILS_H_ */
