<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Генерация синуса">
  <action target_ip="192.168.1.115" target="601388001" out="601" descr="Установить соединение" actNo="1" id="1" name="connect" src="388" gw="388"/>
  <action dir="0" freq="440" ampl="-6" descr="Подключить генератор" type="sin" actNo="2" id="1" name="play"/>
  <action dir="1" freq="300" ampl="-6" descr="Подключить генератор" type="sin" actNo="3" id="1" name="play"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="4" id="1" name="listen" filename="12.wav"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="5" id="1" name="listen" filename="11.wav"/>
  <action descr="Пауза" actNo="6" comment="" id="" name="pause" pause="10000"/>
  <action dir="0" descr="Отключить генератор" actNo="7" id="1" name="stopPlay"/>
  <action dir="1" descr="Отключить генератор" actNo="8" id="1" name="stopPlay"/>
  <action descr="Прервать связь" actNo="9" id="1" name="disconnect"/>
 </script>
