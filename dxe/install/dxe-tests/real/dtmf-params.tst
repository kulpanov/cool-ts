<?xml version="1.0" encoding="UTF-8"?>
 <script descr="con-discon1">
  <action duration="50" descr="DTMF" actNo="1" amplitude="-15" id="" name="dtmf" pause="50"/>
  <action target_ip="192.168.1.5" target="380*---388001" out="380" descr="Установить соединение" actNo="2" id="1" name="connect" src="388" gw="*---388"/>
  <action descr="Пауза" actNo="3" comment="" id="" name="pause" pause="5000"/>
  <action descr="Прервать связь" actNo="4" id="1" name="disconnect"/>
  <action descr="Пауза" actNo="5" comment="" id="" name="pause" pause="5000"/>
  <action duration="40" descr="DTMF" actNo="6" amplitude="-10" id="" name="dtmf" pause="40"/>
  <action target_ip="192.168.1.5" target="380*---388001" out="380" descr="Установить соединение" actNo="7" id="1" name="connect" src="388" gw="*---388"/>
  <action descr="Пауза" actNo="8" comment="" id="" name="pause" pause="5000"/>
  <action descr="Прервать связь" actNo="9" id="1" name="disconnect"/>
  <action descr="Пауза" actNo="10" comment="" id="" name="pause" pause="5000"/>
  <action duration="30" descr="DTMF" actNo="11" amplitude="-5" id="" name="dtmf" pause="30"/>
  <action target_ip="192.168.1.5" target="380*---388001" out="380" descr="Установить соединение" actNo="12" id="1" name="connect" src="388" gw="*---388"/>
  <action descr="Пауза" actNo="13" comment="" id="" name="pause" pause="5000"/>
  <action descr="Прервать связь" actNo="14" id="1" name="disconnect"/>
 </script>
