<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Генерация синуса">
  <action target_ip="192.168.1.115" target="602388001" out="602" descr="Установить соединение" actNo="1" id="1" name="connect" src="388" gw="388"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="2" id="1" name="listen" filename="out.wav"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="3" id="1" name="listen" filename="in.wav"/>
  <action descr="Пауза" actNo="4" comment="" id="" name="pause" pause="5000"/>
  <action dir="1" freq="1000" ampl="-10" descr="Подключить генератор" type="sin" actNo="5" id="1" name="play"/>
  <action descr="Пауза" actNo="6" comment="" id="" name="pause" pause="3000"/>
  <action dir="1" freq="2000" ampl="-10" descr="Подключить генератор" type="sin" actNo="7" id="1" name="play"/>
  <action descr="Пауза" actNo="8" comment="" id="" name="pause" pause="3000"/>
  <action dir="1" freq="3000" ampl="-10" descr="Подключить генератор" type="sin" actNo="9" id="1" name="play"/>
  <action descr="Пауза" actNo="10" comment="" id="" name="pause" pause="3000"/>
  <action descr="Прервать связь" actNo="11" id="1" name="disconnect"/>
 </script>
