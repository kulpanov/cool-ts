<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Три соединения, генератор синуса, тест потока">
  <action target_ip="192.168.1.5" target="380*-388001" out="380" descr="Установить соединение" actNo="1" id="1" name="connect" src="388" gw="*-388"/>
  <action target_ip="192.168.1.5" target="382*-388002" out="382" descr="Установить соединение" actNo="2" id="2" name="connect" src="388" gw="*-388"/>
  <action target_ip="192.168.1.5" target="383*-388003" out="383" descr="Установить соединение" actNo="3" id="3" name="connect" src="388" gw="*-388"/>
  <action target_ip="192.168.1.5" target="381*-388004" out="381" descr="Установить соединение" actNo="4" id="4" name="connect" src="388" gw="*-388"/>
  <action dir="0" freq="300" ampl="-12" descr="Подключить генератор" type="sin" actNo="5" id="1" name="play"/>
  <action dir="0" freq="440" ampl="-6" descr="Подключить генератор" type="sin" actNo="6" id="2" name="play"/>
  <action dir="0" freq="880" ampl="-3" descr="Подключить генератор" type="sin" actNo="7" id="3" name="play"/>
  <action dir="0" freq="1000" ampl="-2" descr="Подключить генератор" type="sin" actNo="8" id="4" name="play"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="9" id="1" name="test_koef" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="10" id="2" name="test_koef" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="11" id="3" name="test_koef" low="0"/>
  <action descr="Разъединить всех" actNo="12" id="" name="disconnect_all"/>
 </script>
