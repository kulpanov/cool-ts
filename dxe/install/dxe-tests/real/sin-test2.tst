<?xml version="1.0" encoding="UTF-8"?>
 <script descr="синус на измеритель">
  <action target_ip="192.168.1.5" target="601388001" out="601" descr="Установить соединение" actNo="1" id="1" name="connect" src="388" gw="388"/>
  <action dir="1" freq="300" ampl="-10" descr="Подключить генератор" type="sin" actNo="2" id="1" name="play"/>
  <action dir="0" freq="300" ampl="-10" descr="Подключить генератор" type="sin" actNo="3" id="1" name="play"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="4" id="1" name="test_abs" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="5" id="1" name="test_koef" low="0"/>
  <action dir="0" freq="600" ampl="-10" descr="Подключить генератор" type="sin" actNo="6" id="1" name="play"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="7" id="1" name="test_abs" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="8" id="1" name="test_koef" low="0"/>
  <action dir="0" freq="900" ampl="-10" descr="Подключить генератор" type="sin" actNo="9" id="1" name="play"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="10" id="1" name="test_abs" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="11" id="1" name="test_koef" low="0"/>
  <action dir="0" freq="1200" ampl="-10" descr="Подключить генератор" type="sin" actNo="12" id="1" name="play"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="13" id="1" name="test_abs" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="14" id="1" name="test_koef" low="0"/>
  <action dir="0" freq="1500" ampl="-10" descr="Подключить генератор" type="sin" actNo="15" id="1" name="play"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="16" id="1" name="test_abs" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="17" id="1" name="test_koef" low="0"/>
  <action dir="0" freq="1800" ampl="-10" descr="Подключить генератор" type="sin" actNo="18" id="1" name="play"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="19" id="1" name="test_abs" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="20" id="1" name="test_koef" low="0"/>
  <action measuretype="0" dir="0" descr="Отрисовать измерения" actNo="21" id="1" name="plot" fname="1.png"/>
  <action measuretype="1" dir="0" descr="Отрисовать измерения" actNo="22" id="1" name="plot" fname="2.png"/>
  <action measuretype="0" dir="2" descr="Отрисовать измерения" actNo="23" id="1" name="plot" fname="3.png"/>
  <action measuretype="1" dir="2" descr="Отрисовать измерения" actNo="24" id="1" name="plot" fname="4.png"/>
  <action descr="Прервать связь" actNo="25" id="1" name="disconnect"/>
 </script>
