<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Три соединения, генератор синуса, запись в файлы">
  <action target_ip="192.168.1.5" target="601388001" out="601" descr="Установить соединение" actNo="1" id="1" name="connect" src="388" gw="388"/>
  <action target_ip="192.168.1.5" target="602388002" out="602" descr="Установить соединение" actNo="2" id="2" name="connect" src="388" gw="388"/>
  <action target_ip="192.168.1.5" target="603388003" out="603" descr="Установить соединение" actNo="3" id="3" name="connect" src="388" gw="388"/>
  <action dir="0" freq="300" ampl="-12" descr="Подключить генератор" type="sin" actNo="4" id="1" name="play"/>
  <action dir="0" freq="440" ampl="-6" descr="Подключить генератор" type="sin" actNo="5" id="2" name="play"/>
  <action dir="0" freq="880" ampl="-3" descr="Подключить генератор" type="sin" actNo="6" id="3" name="play"/>
  <action descr="Пауза" actNo="7" comment="" id="" name="pause" pause="3000"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="8" id="1" name="test_abs" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="9" id="2" name="test_abs" low="0"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" actNo="10" id="3" name="test_abs" low="0"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="11" id="1" name="listen" filename="10.wav"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="12" id="2" name="listen" filename="11.wav"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="13" id="3" name="listen" filename="12.wav"/>
  <action descr="Пауза" actNo="14" comment="" id="" name="pause" pause="5000"/>
  <action dir="1" descr="Прослушать" type="sound_out" actNo="15" id="3" name="listen"/>
  <action descr="Пауза" actNo="16" comment="" id="" name="pause" pause="5000"/>
  <action descr="Разъединить всех" actNo="17" id="" name="disconnect_all"/>
 </script>
