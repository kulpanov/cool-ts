<?xml version="1.0" encoding="UTF-8"?>
 <script descr="CO-SL">
  <action target_ip="" target="000" out="" descr="Установить соединение" actNo="1" id="" name="connect" src="388" gw=""/>
  <action target_ip="" target="" out="" descr="Cоединиться с абонентом" actNo="2" id="" name="connect_half"/>
  <action dir="0" freq="1000" ampl="-1.0" descr="Подключить генератор" type="sin" actNo="3" id="" name="play"/>
  <action hi="0" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="4" id="" name="test_koef" low="0"/>
  <action hi="0" check="0" mtime="100" dir="0" descr="Измерить уровень(rms)" actNo="5" id="" name="test_abs" low="0"/>
  <action dir="0" descr="Отключить генератор" actNo="6" id="" name="stopPlay"/>
  <action dir="0" descr="Передать файл" type="file_in" actNo="7" id="" name="play" filename=""/>
  <action dir="0" descr="Прослушать" type="sound_out" actNo="8" id="" name="listen"/>
  <action dir="0" descr="Остановить прослушку" actNo="9" id="" name="stopListen"/>
  <action descr="Разъединить всех" actNo="10" id="" name="disconnect_all"/>
  <action descr="Прервать связь" actNo="11" id="" name="disconnect"/>
  <action loop="0" sname="" descr="Вызвать сценарий" conn_id="" actNo="12" id="" name="call_script"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="13" id="" name="listen" filename=""/>
  <action dir="0" descr="Остановить запись" actNo="14" id="" name="stopAccept"/>
  <action duration="30" descr="DTMF" actNo="15" amplitude="-5" id="" name="dtmf" pause="30"/>
  <action genid="" dir="0" last="350" ampl="0" descr="Отрисовать АЧХ" first="350" genside="0" actNo="16" step="10" id="" name="plot_rms" fname=""/>
  <action genid="" dir="0" last="350" ampl="0" descr="Отрисовать АЧХ" first="350" genside="0" actNo="17" step="10" id="" name="plot_ratio" fname=""/>
  <action descr="Прервать связь" actNo="18" id="" name="disconnect"/>
 </script>
