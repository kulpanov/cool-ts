<?xml version="1.0" encoding="UTF-8"?>
 <script descr="изм-АЧХ-120">
  <action dir="0" freq="300" ampl="-10" descr="Подключить генератор" type="sin" actNo="1" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="2" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="3" id="" name="stopPlay"/>
  <action dir="0" freq="400" ampl="-10" descr="Подключить генератор" type="sin" actNo="4" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="5" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="6" id="" name="stopPlay"/>
  <action dir="0" freq="500" ampl="-10" descr="Подключить генератор" type="sin" actNo="7" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="8" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="9" id="" name="stopPlay"/>
  <action dir="0" freq="600" ampl="-10" descr="Подключить генератор" type="sin" actNo="10" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="11" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="12" id="" name="stopPlay"/>
  <action dir="0" freq="700" ampl="-10" descr="Подключить генератор" type="sin" actNo="13" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="14" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="15" id="" name="stopPlay"/>
  <action dir="0" freq="1020" ampl="-10" descr="Подключить генератор" type="sin" actNo="16" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="17" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="18" id="" name="stopPlay"/>
  <action dir="0" freq="1600" ampl="-10" descr="Подключить генератор" type="sin" actNo="19" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="20" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="21" id="" name="stopPlay"/>
  <action dir="0" freq="1700" ampl="-10" descr="Подключить генератор" type="sin" actNo="22" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="23" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="24" id="" name="stopPlay"/>
  <action dir="0" freq="1800" ampl="-10" descr="Подключить генератор" type="sin" actNo="25" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="26" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="27" id="" name="stopPlay"/>
  <action dir="0" freq="1900" ampl="-10" descr="Подключить генератор" type="sin" actNo="28" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="29" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="30" id="" name="stopPlay"/>
  <action dir="0" freq="2000" ampl="-10" descr="Подключить генератор" type="sin" actNo="31" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="32" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="33" id="" name="stopPlay"/>
  <action dir="0" freq="2200" ampl="-10" descr="Подключить генератор" type="sin" actNo="34" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="35" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="36" id="" name="stopPlay"/>
  <action dir="0" freq="2400" ampl="-10" descr="Подключить генератор" type="sin" actNo="37" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="38" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="39" id="" name="stopPlay"/>
  <action dir="0" freq="2600" ampl="-10" descr="Подключить генератор" type="sin" actNo="40" id="" name="play"/>
  <action hi="-8" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="41" id="" name="test_koef" low="-12"/>
  <action dir="0" descr="Отключить генератор" actNo="42" id="" name="stopPlay"/>
  <action dir="0" freq="2809" ampl="-10" descr="Подключить генератор" type="sin" actNo="43" id="" name="play"/>
  <action hi="-5" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="44" id="" name="test_koef" low="-9"/>
  <action dir="0" descr="Отключить генератор" actNo="45" id="" name="stopPlay"/>
  <action dir="0" freq="3000" ampl="-10" descr="Подключить генератор" type="sin" actNo="46" id="" name="play"/>
  <action hi="-5" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="47" id="" name="test_koef" low="-9"/>
  <action dir="0" descr="Отключить генератор" actNo="48" id="" name="stopPlay"/>
  <action dir="0" freq="3100" ampl="-10" descr="Подключить генератор" type="sin" actNo="49" id="" name="play"/>
  <action hi="-5" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="50" id="" name="test_koef" low="-9"/>
  <action dir="0" descr="Отключить генератор" actNo="51" id="" name="stopPlay"/>
  <action dir="0" freq="3200" ampl="-10" descr="Подключить генератор" type="sin" actNo="52" id="" name="play"/>
  <action hi="-5" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="53" id="" name="test_koef" low="-9"/>
  <action dir="0" descr="Отключить генератор" actNo="54" id="" name="stopPlay"/>
  <action dir="0" freq="3300" ampl="-10" descr="Подключить генератор" type="sin" actNo="55" id="" name="play"/>
  <action hi="-5" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="56" id="" name="test_koef" low="-9"/>
  <action dir="0" descr="Отключить генератор" actNo="57" id="" name="stopPlay"/>
  <action dir="0" freq="3400" ampl="-10" descr="Подключить генератор" type="sin" actNo="58" id="" name="play"/>
  <action hi="-5" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="59" id="" name="test_koef" low="-9"/>
  <action dir="0" descr="Отключить генератор" actNo="60" id="" name="stopPlay"/>
  <action dir="1" freq="300" ampl="-10" descr="Подключить генератор" type="sin" actNo="61" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="62" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="63" id="" name="stopPlay"/>
  <action dir="1" freq="400" ampl="-10" descr="Подключить генератор" type="sin" actNo="64" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="65" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="66" id="" name="stopPlay"/>
  <action dir="1" freq="500" ampl="-10" descr="Подключить генератор" type="sin" actNo="67" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="68" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="69" id="" name="stopPlay"/>
  <action dir="1" freq="600" ampl="-10" descr="Подключить генератор" type="sin" actNo="70" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="71" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="72" id="" name="stopPlay"/>
  <action dir="1" freq="700" ampl="-10" descr="Подключить генератор" type="sin" actNo="73" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="74" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="75" id="" name="stopPlay"/>
  <action dir="1" freq="1020" ampl="-10" descr="Подключить генератор" type="sin" actNo="76" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="77" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="78" id="" name="stopPlay"/>
  <action dir="1" freq="1600" ampl="-10" descr="Подключить генератор" type="sin" actNo="79" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="80" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="81" id="" name="stopPlay"/>
  <action dir="1" freq="1700" ampl="-10" descr="Подключить генератор" type="sin" actNo="82" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="83" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="84" id="" name="stopPlay"/>
  <action dir="1" freq="1800" ampl="-10" descr="Подключить генератор" type="sin" actNo="85" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="86" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="87" id="" name="stopPlay"/>
  <action dir="1" freq="1900" ampl="-10" descr="Подключить генератор" type="sin" actNo="88" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="89" id="" name="test_koef" low="-3"/>
  <action dir="1" descr="Отключить генератор" actNo="90" id="" name="stopPlay"/>
  <action dir="1" freq="2000" ampl="-10" descr="Подключить генератор" type="sin" actNo="91" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="92" id="" name="test_koef" low="-4"/>
  <action dir="1" descr="Отключить генератор" actNo="93" id="" name="stopPlay"/>
  <action dir="1" freq="2200" ampl="-10" descr="Подключить генератор" type="sin" actNo="94" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="95" id="" name="test_koef" low="-4"/>
  <action dir="1" descr="Отключить генератор" actNo="96" id="" name="stopPlay"/>
  <action dir="1" freq="2400" ampl="-10" descr="Подключить генератор" type="sin" actNo="97" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="98" id="" name="test_koef" low="-4"/>
  <action dir="1" descr="Отключить генератор" actNo="99" id="" name="stopPlay"/>
  <action dir="1" freq="2600" ampl="-10" descr="Подключить генератор" type="sin" actNo="100" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="101" id="" name="test_koef" low="-4"/>
  <action dir="1" descr="Отключить генератор" actNo="102" id="" name="stopPlay"/>
  <action dir="1" freq="2800" ampl="-10" descr="Подключить генератор" type="sin" actNo="103" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="104" id="" name="test_koef" low="-4"/>
  <action dir="1" descr="Отключить генератор" actNo="105" id="" name="stopPlay"/>
  <action dir="1" freq="3000" ampl="-10" descr="Подключить генератор" type="sin" actNo="106" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="107" id="" name="test_koef" low="-6"/>
  <action dir="1" descr="Отключить генератор" actNo="108" id="" name="stopPlay"/>
  <action dir="1" freq="3100" ampl="-10" descr="Подключить генератор" type="sin" actNo="109" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="110" id="" name="test_koef" low="-6"/>
  <action dir="1" descr="Отключить генератор" actNo="111" id="" name="stopPlay"/>
  <action dir="1" freq="3200" ampl="-10" descr="Подключить генератор" type="sin" actNo="112" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="113" id="" name="test_koef" low="-6"/>
  <action dir="1" descr="Отключить генератор" actNo="114" id="" name="stopPlay"/>
  <action dir="1" freq="3300" ampl="-10" descr="Подключить генератор" type="sin" actNo="115" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="116" id="" name="test_koef" low="-6"/>
  <action dir="1" descr="Отключить генератор" actNo="117" id="" name="stopPlay"/>
  <action dir="1" freq="3400" ampl="-10" descr="Подключить генератор" type="sin" actNo="118" id="" name="play"/>
  <action hi="0.5" check="0" mtime="100" dir="0" descr="Измерить коэф передачи" actNo="119" id="" name="test_koef" low="-6"/>
  <action dir="1" descr="Отключить генератор" actNo="120" id="" name="stopPlay"/>
 </script>
