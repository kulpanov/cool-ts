<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Arm-CO-GW-7ch">
  <action loop="1" sname="Arm-configure_DTMF.tst" descr="Вызвать сценарий" conn_id="7" actNo="1" id="" name="call_script"/>
  <action target_ip="192.168.1.5" target="20007*-9007" out="20007" descr="Установить соединение" actNo="2" id="7" name="connect" src="388" gw="*-9"/>
  <action dir="0" freq="1000" ampl="-10" descr="Подключить генератор" type="sin" actNo="3" id="7" name="play"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="4" id="7" name="listen" filename="Arm-CO-GW-7ch-in.wav"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="5" id="7" name="listen" filename="Arm-CO-GW-7ch-out.wav"/>
  <action loop="1" sname="Arm-1kHz-i_ch.tst" descr="Вызвать сценарий" conn_id="7" actNo="6" id="" name="call_script"/>
  <action descr="Прервать связь" actNo="7" id="7" name="disconnect"/>
 </script>
