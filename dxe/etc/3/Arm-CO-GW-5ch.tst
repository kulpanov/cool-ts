<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Arm-CO-GW-5ch">
  <action loop="1" sname="Arm-configure_DTMF.tst" descr="Вызвать сценарий" conn_id="5" actNo="1" id="" name="call_script"/>
  <action target_ip="192.168.1.13" target="20005005005" out="20005" descr="Установить соединение" actNo="2" id="5" name="connect" src="388" gw="005"/>
  <action descr="Пауза" actNo="3" id="" name="pause" pause="11000"/>
  <action dir="0" freq="1000" ampl="-10" descr="Подключить генератор" type="sin" actNo="4" id="5" name="play"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="5" id="5" name="listen" filename="Arm-CO-GW-5ch-in.wav"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="6" id="5" name="listen" filename="Arm-CO-GW-5ch-out.wav"/>
  <action loop="1" sname="Arm-1kHz-i_ch.tst" descr="Вызвать сценарий" conn_id="5" actNo="7" id="" name="call_script"/>
  <action descr="Прервать связь" actNo="8" id="5" name="disconnect"/>
 </script>
