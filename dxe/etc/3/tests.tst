<?xml version="1.0" encoding="UTF-8"?>
 <script descr="CO-SL">
  <action dir="0" freq="440" ampl="-6" descr="Подключить генератор" type="sin" actNo="1" id="" name="play"/>
  <action dir="1" freq="440" ampl="-10" descr="Подключить генератор" type="sin" actNo="2" id="" name="play"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="3" id="" name="listen" filename="D:/_prj/amt/ws3/dxe/install/tmp/1.wav"/>
  <action dir="0" descr="Прослушать" type="sound_out" actNo="4" id="" name="listen"/>
  <action descr="Пауза" actNo="5" id="" name="pause" pause="5000"/>
  <action dir="0" descr="Остановить запись" actNo="6" id="" name="stopAccept"/>
  <action descr="Пауза" actNo="7" id="" name="pause" pause="5000"/>
  <action dir="0" descr="Остановить прослушку" actNo="8" id="" name="stopListen"/>
  <action descr="Пауза" actNo="9" id="" name="pause" pause="10000"/>
  <action dir="0" descr="Отключить генератор" actNo="10" id="" name="stopPlay"/>
  <action dir="1" descr="Отключить генератор" actNo="11" id="" name="stopPlay"/>
 </script>
