<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Arm-1kHz-ich">
  <action descr="Пауза" actNo="1" id="" name="pause" pause="5000"/>
  <action dir="0" descr="Отключить генератор" actNo="2" id="" name="stopPlay"/>
  <action dir="1" freq="1000" ampl="-10" descr="Подключить генератор" type="sin" actNo="3" id="" name="play"/>
  <action descr="Пауза" actNo="4" id="" name="pause" pause="5000"/>
  <action dir="1" descr="Остановить запись" actNo="5" id="" name="stopAccept"/>
  <action dir="0" descr="Остановить запись" actNo="6" id="" name="stopAccept"/>
  <action dir="1" descr="Отключить генератор" actNo="7" id="" name="stopPlay"/>
 </script>
