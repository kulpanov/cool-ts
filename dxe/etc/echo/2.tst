<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Сценарий">
  <action dir="0" freq="440" ampl="-6" descr="Подключить генератор" type="sin" actNo="1" id="" name="play"/>
  <action dir="1" freq="300" ampl="-10" descr="Подключить генератор" type="sin" actNo="2" id="" name="play"/>
  <action descr="Пауза" actNo="3" id="" name="pause" pause="5000"/>
  <action hi="1" check="1" mtime="100" dir="1" descr="Измерить коэф передачи" actNo="4" id="" name="test_koef" low="-1"/>
  <action hi="0" check="0" mtime="100" dir="0" descr="Измерить уровень(rms)" actNo="5" id="" name="test_abs" low="0"/>
  <action dir="0" descr="Отключить генератор" actNo="6" id="" name="stopPlay"/>
 </script>
