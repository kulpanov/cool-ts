<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Описание">
  <action target_ip="127.0.0.1" target="001001001" out="001" descr="Установить соединение" id="1" name="connect" src="388" gw="001"/>
  <action dir="0" freq="440" ampl="-6.1" descr="Подключить генератор" type="sin" id="1" name="play"/>
  <action hi="1" check="0" mtime="100" dir="1" descr="Измерить коэф передачи" id="1" name="test_koef" low="1"/>
  <action dir="0" freq="220" ampl="-10" descr="Подключить генератор" type="sin" id="1" name="play"/>
  <action hi="0" check="0" mtime="100" dir="1" descr="Измерить уровень(rms)" id="1" name="test_abs" low="0"/>
  <action descr="Прервать связь" id="1" name="disconnect"/>
 </script>
