<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Arm-CO-GW-16ch">
  <action loop="1" sname="Arm-configure_DTMF.tst" descr="Вызвать сценарий" conn_id="16" actNo="1" id="" name="call_script"/>
  <action target_ip="192.168.1.13" target="20016016016" out="20016" descr="Установить соединение" actNo="2" id="16" name="connect" src="388" gw="016"/>
  <action dir="0" freq="1000" ampl="-16" descr="Подключить генератор" type="sin" actNo="3" id="16" name="play"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="4" id="16" name="listen" filename="Arm-CO-GW-16ch-in.wav"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="5" id="16" name="listen" filename="Arm-CO-GW-16ch-out.wav"/>
  <action loop="1" sname="Arm-1kHz-i_ch.tst" descr="Вызвать сценарий" conn_id="16" actNo="6" id="" name="call_script"/>
  <action descr="Прервать связь" actNo="7" id="16" name="disconnect"/>
 </script>
