<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Arm-CO-GW-17ch">
  <action loop="1" sname="Arm-configure_DTMF.tst" descr="Вызвать сценарий" conn_id="17" actNo="1" id="" name="call_script"/>
  <action target_ip="192.168.1.13" target="20017017017" out="20017" descr="Установить соединение" actNo="2" id="17" name="connect" src="388" gw="017"/>
  <action dir="0" freq="1000" ampl="-17" descr="Подключить генератор" type="sin" actNo="3" id="17" name="play"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="4" id="17" name="listen" filename="Arm-CO-GW-17ch-in.wav"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="5" id="17" name="listen" filename="Arm-CO-GW-17ch-out.wav"/>
  <action loop="1" sname="Arm-1kHz-i_ch.tst" descr="Вызвать сценарий" conn_id="17" actNo="6" id="" name="call_script"/>
  <action descr="Прервать связь" actNo="7" id="17" name="disconnect"/>
 </script>
