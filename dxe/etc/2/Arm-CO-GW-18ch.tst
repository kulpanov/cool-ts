<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Arm-CO-GW-18ch">
  <action loop="1" sname="Arm-configure_DTMF.tst" descr="Вызвать сценарий" conn_id="18" actNo="1" id="" name="call_script"/>
  <action target_ip="192.168.1.13" target="20018018018" out="20018" descr="Установить соединение" actNo="2" id="18" name="connect" src="388" gw="018"/>
  <action dir="0" freq="1000" ampl="-18" descr="Подключить генератор" type="sin" actNo="3" id="18" name="play"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="4" id="18" name="listen" filename="Arm-CO-GW-18ch-in.wav"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="5" id="18" name="listen" filename="Arm-CO-GW-18ch-out.wav"/>
  <action loop="1" sname="Arm-1kHz-i_ch.tst" descr="Вызвать сценарий" conn_id="18" actNo="6" id="" name="call_script"/>
  <action descr="Прервать связь" actNo="7" id="18" name="disconnect"/>
 </script>
