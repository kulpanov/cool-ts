<?xml version="1.0" encoding="UTF-8"?>
 <script descr="Arm-CO-GW-13ch">
  <action loop="1" sname="Arm-configure_DTMF.tst" descr="Вызвать сценарий" conn_id="13" actNo="1" id="" name="call_script"/>
  <action target_ip="192.168.1.13" target="20013013013" out="20013" descr="Установить соединение" actNo="2" id="13" name="connect" src="388" gw="013"/>
  <action dir="0" freq="1000" ampl="-13" descr="Подключить генератор" type="sin" actNo="3" id="13" name="play"/>
  <action dir="1" descr="Записать файл" type="file_out" actNo="4" id="13" name="listen" filename="Arm-CO-GW-13ch-in.wav"/>
  <action dir="0" descr="Записать файл" type="file_out" actNo="5" id="13" name="listen" filename="Arm-CO-GW-13ch-out.wav"/>
  <action loop="1" sname="Arm-1kHz-i_ch.tst" descr="Вызвать сценарий" conn_id="13" actNo="6" id="" name="call_script"/>
  <action descr="Прервать связь" actNo="7" id="13" name="disconnect"/>
 </script>
