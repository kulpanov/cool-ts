/** \brief Реализация методов CService_Call_in.
 * CService_Call_in.cpp
 *
 *  Created on: 13.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include "define.h"
#include "utils/utils.h"
#include "vms_signals.h"
#include "CdxeCallService.h"
#include "CdxeCallManager.h"

namespace DXE {

//Release - отбой звонка
void CCallManager::putSetup_res(const int _id, const CString& _number, const CString& _add
    , const IP& _s_ip){

  boost::mutex::scoped_lock locker(lock);

  ASSERT(_number == connect_number);
  ASSERT(_add == connect_add);
  //ASSERT(_s_ip == connect_ip);

  if(! isCalling_out(1)){
//    CVMS_Signal_Release_out release(_id);
//    vms.sendToSDL(&release);
    LOG4(ERRR, "call(%d):putSetup_res: %d:%s:%s was rejected\n", getId(), _id, _number.c_str(), _add.c_str());
    return ;
  }
  id = _id;//связать c SDL-объектом
  LOG4(TSTL1, "call(-1):putSetup_res: id=%d, %d:%s:%s \n", getId(), _id, connect_number.c_str(), connect_add.c_str());

  queue_ofMails.push(SMailBox(_connect, _id));
  cond.notify_one();//
//  while(mailBox.cmd == _ok){   mailBox.cond.wait(locker); }
//  mailBox.cmd = _connect;
//  mailBox.cond.notify_one();
  //mailBox.cond.wait(locker);
//  if(false == mailBox.cond.timed_wait(locker, boost::posix_time::seconds(30))){
//    LOG(ERRR, "call: Timeout, putSetup_res\n");
//  }

  return ;
}

//Invite - запрос на соединение
void CCallManager::putInvite(const int _id, const CString& _number, const CString& _add, const IP& _s_ip){
  if(_number.size() == 0){
    LOG(ERRR, "callManager:putInvite:Недопустимое значение number");
    return ;
  }
  if(_add.size() == 0){
    LOG(ERRR, "callManager:putInvite:Недопустимое значение add");
    return ;
  }
  if(! isFree()){
    //уже работаем, вернуть отказ в SDL
    CVMS_Signal_Release_out release(_id);// - не доджна SDL пропустить это!
    vms.sendToSDL(&release);
    LOG4(ERRR, "call(%d):putInvite: invite %d:%s:%s must be rejected\n"
        , getId(), _id, _number.c_str(), _add.c_str());
    return ;
  }

  {boost::mutex::scoped_lock locker(lock);
  setStatus(_call_in);
  id = _id;//связать c SDL-объектом
  connect_number = _number;
  connect_add = _add;
  connect_ip = _s_ip;
  LOG4(TSTL1, "call(%d):putInvite: %d:%s:%s \n", getId(), _id, connect_number.c_str(), connect_add.c_str());

    call_in_thread = boost::thread(boost::bind(&CCallManager::call_in, this));
  }
  return ;
}

void CCallManager::putSIP_Info(int _data1, int _data2, int _data3){
  boost::mutex::scoped_lock locker(lock);
  if(! isBusy()) return;
  LOG1(TSTL1, "call(%d):putSIP_Info, not implement\n", getId());
  //TODO: когад буишь реализовывать, см call_out там ожидаются только connect
//  mailBox.cmd = _SIP_INFO;
//  mailBox.data0 = _data2;
//  mailBox.cond.notify_all();
//  mailBox.cond.wait(locker);
}

//Connect - подтвердить звонок
void CCallManager::putConnect(const int _id){
  boost::mutex::scoped_lock locker(lock);
  LOG3(TSTL1, "call(%d):putConnect:id=%d, status %d\n", getId(), _id, getStatus());
  if( ! isCalling_out(2) ){
    LOG4(WARN, "call(%d):putConnect: %d:%s:%s was rejected\n"
        , getId(), _id, connect_number.c_str(), connect_add.c_str());
    return;
  }

  ASSERT(id == _id);//связать c SDL-объектом
  //mailBox.cond.wait(locker);  //и ждать как сервер освободится
  queue_ofMails.push(SMailBox(_connect, _id));
  cond.notify_one();//
//  while(mailBox.cmd != _ok){ mailBox.cond.wait(locker); }
//  mailBox.cmd = _connect;
//  mailBox.data0 = _id;
//  mailBox.cond.notify_one();//уведомить
//  if(false == mailBox.cond.timed_wait(locker, boost::posix_time::seconds(10))){
//    LOG(ERRR, "call: Timeout, putConnect\n");
//  }
}
//Release - отбой звонка
void CCallManager::putRelease(const int _id){
  //ASSERT(_id == id);
  //id = -1;//освободить!
  { boost::mutex::scoped_lock locker(lock);
    if( isFree()) return;
    LOG3(TSTL1, "call(%d):putRelease:id=%d, status %d\n", getId(), _id, getStatus());
    id = -(id + 100); //инвертируем id - признак освобождения, управл более не связан с SDL-объетом
    if(! isCallOff()){
      queue_ofMails.push(SMailBox(_release, 0));
      cond.notify_one();
//      //избегаем повторного статуса
//      while(mailBox.cmd != _ok){   mailBox.cond.wait(locker); }
//      //setStatus(_call_off);
//      mailBox.cmd = _release;
//      mailBox.cond.notify_one();
    }
//    if(false == mailBox.cond.timed_wait(locker, boost::posix_time::seconds(10))){
//      LOG(ERRR, "call: Timeout, putRelease\n");
//    }
    //setStatus(_free);
  }
}

//
void CCallManager::putWrSenderTable(int _d_rtp, int _d_ip) {
  boost::mutex::scoped_lock(lock_rtp);
  LOG2(TSTL1, "call(%d):putWrSenderTable:status %d\n", getId(), getStatus());
  rtp_out = _d_rtp;
  rtp_gw = _d_ip;
  cond_rtpDest.notify_all();
}
}

