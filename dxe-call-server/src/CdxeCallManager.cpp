/** \brief Реализация методов CService_Call_in.
 * CService_Call_in.cpp
 *
 *  Created on: 13.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#include <thread>

#include <boost/asio.hpp>
using boost::asio::ip::udp;

#include "define.h"
#include "utils/utils.h"
#include "vms_signals.h"
#include "CdxeCallService.h"
#include "CdxeCallManager.h"

namespace DXE {


int CCallManager::setParams(int param, int arg) {
  boost::mutex::scoped_lock locker(lock);
  return setParams_in(param, arg);
}


int CCallManager::setParams_in(int param, int arg){
  LOG3(DEVL2, "call(%d):SetParams:%d:%d\n", getId(),  param,  arg);
  switch(param){
  case rtpRestoreDef:
    params.packetSize = IWavStream::rtp_packet_size / 8;
    params.jitter = 0;
    params.quant = 80000;
    b_timer.setBasePeriod( params.packetSize , params.jitter, params.quant);
    if(NULL != play_stream)
      play_stream->packetSize( params.packetSize * 8);
    break;
  case rtpPacketSize:
    if(arg > 60) arg = 60;
    if(arg < 10) arg = 10;
    params.packetSize = arg;
    b_timer.setBasePeriod( params.packetSize , params.jitter, params.quant);
    if(NULL != play_stream)
      play_stream->packetSize( params.packetSize * 8);
    break;
  case rtpJitter:
    if(arg > params.packetSize) arg = params.packetSize;
    if(arg < 0) arg = 0;
    params.jitter = arg;
    b_timer.setBasePeriod( params.packetSize , params.jitter, params.quant);
    break;
  case rtpQuant:
    if(arg > 81000) arg = 81000;
    if(arg < 79000) arg = 79000;
    params.quant = arg;
    b_timer.setBasePeriod( params.packetSize , params.jitter, params.quant);
    break;
  }
  LOG4(DEVL1, "call(%d):SetParams:%d:%d:%d\n", getId(),  params.packetSize,  params.jitter,  params.quant);
  return 0;
}

int CCallManager::sendDTMFs(const CString& _dtmfNumbers) {
  if (_dtmfNumbers.size() == 0) return EOK;
  int res = EOK;
  LOG3(TSTL1, "call(%d): sendDTMFs: %s, %d\n", getId(), _dtmfNumbers.c_str(), params.packetSize);
  tVoiceMsg q = vms.getDTMF().get(_dtmfNumbers, params.packetSize * 8);
  //разлочить на время паузы
  res = playDTMF(q); //до конца потока
  play_stream = defPlay;
  return res;
}

///ждать входящего вызова от источника с номеров source не более ms!
int CCallManager::setConnectData(const CString& _number, const CString& _add, const IP& _s_ip) {
  { boost::mutex::scoped_lock locker(lock);
    connect_number = _number;
    connect_add = _number;
    connect_ip = _s_ip;
  }
  LOG3(INFO, "call(%d): setConnectData(%s, %s)\n", getId(), connect_number.c_str(), connect_add.c_str());
  return 0;
}

bool CCallManager::checkConnectData(const CString& _number, const CString& _add,
    const IP& _s_ip, bool _strong) {
  LOG3(TSTL1, "CCallManager::checkConnectData: connect_number=%s, income_number=%s, reserv=%d\n", connect_number.c_str(), _number.c_str(), isReserved());
  {
    boost::mutex::scoped_lock locker(lock);
    if (_strong || connect_number.size() > 0)
      return (_number == connect_number);
  }
  return true;
}

//обработка перед установкой соединения
int CCallManager::beginCall(const CString& _number, const CString& _add,
    const IP& _s_ip) {
  try {

    LOG4(INFO, "call(%d):callBegin, number=%s, digi=%s, ip=%s\n", getId(), _number.c_str(), _add.c_str(), _s_ip.toString().c_str());
    LOGTIME(INFO);
    { boost::mutex::scoped_lock locker(lock_rtp);
      //int wait_counter = 0;
      if(0 == rtp_out)
      if(false == cond_rtpDest.timed_wait(locker, boost::posix_time::seconds(5))){
        if(0 == rtp_out){ //it's paranoid code!
          //if(++wait_counter>10)
          throw Ev(ETIMEDOUT, "call(%d):%s: Timeout on wr_senderTable\n", getId(), connect_number.c_str());
        }else
          LOG(ERRR, "CCallManager::beginCall:notify_all failed?\n");
      } }

    rtp_in = vms.getRTP_in(getId());
//    LOG4(TSTL1, "call(%d):get ports:gw=0x%x, rtp_out=%d, rtp_in=%d\n", getId(), rtp_gw.getIP(), rtp_out, rtp_in);

    //запускаем сокеты на прослушку
    rtp_out_count = 0;
    rtp_out_timeStamp = 0;
    rtp_out += vms.net.rtp_out_offs;
    rtp_in  += vms.net.rtp_in_offs;
    //создать сокет входящего трафика
    {
      boost::mutex::scoped_lock lck(vms.getVMSLock());
      uint32_t _ip = vms.getIPNet().first;
       if(vms.net.sipPort_in != vms.net.sipPort_out){
         //отладочный режим запускаемся на одном ip, надо вернуть обман SDL
         _ip = vms.realIP;
       }
       socket_rtp_in = udp::socket(vms.getIOS(), {boost::asio::ip::address_v4(_ip), rtp_in});
       boost::asio::socket_base::receive_buffer_size opt_bf_rcsize(8192);
       socket_rtp_in.set_option(opt_bf_rcsize);
//       try{
//         boost::asio::socket_base::receive_buffer_size option;
//         socket_rtp_in.get_option(option);
//         LOG2(DEVL2, "call(%d):callBegin: socket_rtp_in: receive_buffer_size=%d\n", getId(), option.value() );
//       }catch(P_EXC e){
//         E_CATCH(e);
//       }
//       try{
//         boost::asio::socket_base::linger option;
//         socket_rtp_in.get_option(option);
//         LOG3(DEVL2, "call(%d):callBegin: socket_rtp_in: linger=%d, rtp_in=%d, rtp_ip=%s\n", getId(), option.enabled(), option.timeout());
//       }catch(P_EXC e){
//         E_CATCH(e);
//       }
//       try{
//         boost::asio::socket_base::bytes_readable command(true);
//         socket_rtp_in.io_control(command);
//         LOG2(DEVL2, "call(%d):callBegin: socket_rtp_in: io_control=%d\n", getId(), command.get());
//       }catch(P_EXC e){
//         E_CATCH(e);
//       }
//       try{
//         boost::asio::socket_base::receive_low_watermark option;
//         socket_rtp_in.get_option(option);
//         LOG2(DEVL2, "call(%d):callBegin: socket_rtp_in: receive_low_watermark=%d\n", getId(), option.value());
//       }catch(P_EXC e){
//         E_CATCH(e);
//       }
    }
    //стартовать RTP-слушателя
    //rtp_listener = boost::thread(rtpListener_static, this);
    LOG4(TSTL1, "call(%d):callBegin: rtp_out=%d, rtp_in=%d, rtp_ip=%s\n", getId(), rtp_out, rtp_in, rtp_gw.toString().c_str());
    setRTPListener();
  } catch (EXC e) {
    E_CATCH(e);
    throw e;
  } catch (P_EXC e) {
    E_CATCH(e);
    throw e;
  }

  return EOK;
}

//основной цикл соединения
void CCallManager::continueCall() {
  play_stream = defPlay;
  setStatus (_busy);
  playNTimes(infinity);
}

//Обработка после окончания вызова
void CCallManager::endCall() {
  boost::mutex::scoped_lock locker(lock);
  LOG4(INFO, "call(%d)::callEnd, number=%s, add=%s, ip=%s\n", getId(), connect_number.c_str(), connect_add.c_str(), connect_ip.toString().c_str());
  LOGTIME(INFO);
  //
  setStatus(_call_off);
  if(getId() >= 0 ) //освободить индекс
  {
    CVMS_Signal_Release_out release(getId());
    vms.sendToSDL(&release);
    //id = -(id + 100); //инвертируем id - признак освобождения, управл более не связан с SDL-объетом
  }
//  boost::this_thread::sleep(boost::posix_time::milliseconds(600));
  //зачищаем
  stopListen();
  stopPlaying();

  rtp_out = 0;
  rtp_in = 0;
  rtp_gw = (uint32_t) (0);
  socket_rtp_in.close();
  socket_rtp_out.close();
  connect_number = "";
  connect_add = "";
  dtmfNumber = "";
  connect_ip = IP(uint32_t(0));

  while(!queue_ofMails.empty())queue_ofMails.pop();
  markForReserved(); setStatus(_free);
  LOG1(INFO, "call(%d)::callEnd, completed\n", getId());
  LOGTIME(INFO);
  return;
}

//закрыть соединение
void CCallManager::close() {
  boost::mutex::scoped_lock locker(lock);
  if (!isFree()) {
    LOG3(TSTL1, "call(%d): close: status: (%d), number %s\n", getId(), getStatus(), connect_number.c_str());
    LOGTIME(TSTL1);
    if(! isCallOff()){
      //исключаем повторный calloff
      //id - здесь не трогать, в endCall
      queue_ofMails.push(SMailBox(_release, 0));
      cond.notify_one();//  - решить вопрос
    }
    //ожидать пока управл не освободится
    LOG2(TSTL1, "call(%d): close: wait status (%d)\n",getId(), getStatus());
    while (!isFree()) {
      cond_status.timed_wait(locker,  boost::posix_time::seconds(1));
    }
  }
  LOG2(TSTL1, "call(%d):close: number %s, cleanup threads\n", getId(), connect_number.c_str());
  call_out_thread.join();
  call_in_thread.join();
  LOG(INFO, "call():close:successfully\n");
  return;
}

//
ICallManager* CCallManager::release() {
  LOG1(TSTL1, "call(%d): release\n", getId());
  markForReserved();
  close();
  vms.releaseCallManager(this);
  LOG1(TSTL1, "call(%d): delete\n", getId());
  delete this;
  return NULL;
}

//CService_Call_in
CCallManager::CCallManager(CCallService& _vms, ICallManager::EStatus _init) :
    vms(_vms), id(-1), status(_idle), rtp_in(0)
  , rtp_out(0), rtp_gw(), play_stream(NULL), defPlay(NULL), defListen(NULL)
  , socket_rtp_in(vms.getIOS()), socket_rtp_out(vms.getIOS(), udp::endpoint(udp::v4(), 0)) {

  listen_stream[_variantDef]    = defListen;
  listen_stream[_variantAudio]  = defListen;
  listen_stream[_variantFile]   = defListen;
  lock_listen_stream = 0;
//  listen_stream = NULL;
  //резервирован, не может быть использован для входящего
  forReserved = false;
  if (_init == _reserved)
    forReserved = true; //status = _reserved;
  rtp_out_timeStamp = 0;
  rtp_out_count = 0;

  //mailBox.cmd = _ok;
  string strerr;
  defPlay = IWavStream::create(" ", strerr); //==тишина!!!
  play_stream = defPlay;
  lock_play_stream = 0;
  setStatus(_free);
  setParams_in(rtpRestoreDef, 0);
  LOG2(TSTL1, "call(%d): ctor completed: (%s)\n", getId(), connect_number.c_str());
}

//~CService_Call_in
CCallManager::~CCallManager() {
  LOG3(TSTL1, "call(%d): dtor: (%d) number %s\n", getId(), getStatus(),
      connect_number.c_str());
  markForReserved();
  close();
  //на всякий случай
  //mailBox.cond.notify_all();
  Sleep(200);
//  lock.lock();
//  if(rtp_listener) rtp_listener->interrupt();
//  if(call_out_thread) call_out_thread->interrupt();
//  if(call_in_thread) call_in_thread->interrupt();
//
  defPlay->release();

//  tVoiceMsg::Save_qmsg_toWAV(dup_audio, string("wavs/last_listened" + dtmfNumber + ".wav").c_str());

//  lock.unlock();
  LOG2(TSTL1, "call(%d): dtor completed: (%s)\n", getId(),  connect_number.c_str());
}

//
CString CCallManager::parseCallNumber(CString& _number) {
  //"1234*-5678-12"
  unsigned int starPos = _number.find('*');
  if (starPos == string::npos) {
    return "";
  }
  CString _dtmfNumber = _number.substr(starPos + 1, string::npos);
  _number = _number.substr(0, starPos);
  return _dtmfNumber;
}

}

