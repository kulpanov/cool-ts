/** rtp.cpp
 * Метод-слушатель входящего RTP-потока
 *  Created on: 23.03.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <string>
#include <unistd.h>
#include <queue>
#define BOOST_SCOPE_EXIT_ALL(...) //stub for syntax analyser
#include <boost/scope_exit.hpp>

#include "define.h"
#include "CdxeCallManager.h"
#include "CdxeCallService.h"

namespace DXE {

//цикл проигрывания
int CCallManager::playDTMF(const tVoiceMsg& _source) {
  //TODO: 1 объединить с методом playNTimes, реализовать как обработчики ioservice
  try {
//    b_timer.setBasePeriod( IWavStream::rtp_packet_size / 8, 0, 0);

    int rtp_count = 0;
    int res = EOK;
    if(_source.empty()) throw E(0xfeed, "Empty source stream");
    auto msg_it = _source.begin();
    do {
      //проверка внешней команды -
      if(! queue_ofMails.empty())
      if (_release == queue_ofMails.front().cmd) {
//        //внешний отбой, прекратить вещать, выйти из потока
        return 1;
        //mailBox.cond.notify_all(); - здесь не делать! это сделает endCall,
        //когда завершит потоки, закроет сокеты, и только после этого вернет работу sdl
      }

      b_timer.setTimer();

      lock.unlock();
      //boost::cv_status cv_st = mailBox.cond.wait_for(locker,  chrono::milliseconds(100));
      res = WaitForSingleObject(b_timer.getTimer(), INFINITE);
      lock.lock();

      switch (res) {
      case WAIT_OBJECT_0:
      { //выдать порцию сообщения в сеть
        b_timer.calc_balance();

        CPacket_RTP* rtp_packet = *msg_it;
        msg_it++;
        ASSERT(rtp_packet);

        rtp_packet->setCounts(rtp_out_count++, rtp_out_timeStamp);
        rtp_out_timeStamp += rtp_packet->getLengthOfPayload();
        rtp_count++;
//        if(rtp_packet->GetLength() < 100)
//          LOG3(DEVL2, "call(%d):rtp  out:%d, len=%d\n", getId(), rtp_count, rtp_packet->GetLength());

          boost::asio::ip::udp::endpoint ep_sip_out
               = {boost::asio::ip::address_v4(rtp_gw.getIP()), rtp_out};
          try{
            socket_rtp_out.send_to(
                boost::asio::buffer( rtp_packet->GetData(), rtp_packet->GetLength()), ep_sip_out);
          }catch(P_EXC e){
            throw Ev(WSAGetLastError(), "%s: sendto", e.what());
          }
        //BOOST_SCOPE_EXIT_ALL(& rtp_packet){ delete rtp_packet; };
      }
      break;
      default:
        throw Ev(WSAGetLastError(), "call(%d):waitforEndOfCall: WaitForSingleObject failed with message %s", strerror(GetLastError()));
      }

    } while ((msg_it != _source.end()) && (res == EOK));
    LOG2(DEVL1, "call(%d):DTMF:: rtp_out_count:%d\n", getId(), rtp_out_count);
  } catch (EXC e) {
    E_CATCH(e);
    throw e;
  } catch (P_EXC e) {
    E_CATCH(e);
    throw e;
  }
  return EOK;
}

//extern int IWavStream::rtp_packet_size;

//цикл проигрывания
int CCallManager::playNTimes(int _times) {
  try {
//    setParams_in(rtpRestoreDef, 0);
//    LOG2(DEVL1, "call(%d):playNTimes1: 0x%x\n", getId(), play_stream);
    int rtp_count = 0;
    int res = EOK;
    do {
      //проверка внешней команды -
      if(! queue_ofMails.empty())
      if (_release == queue_ofMails.front().cmd) {
//        //внешний отбой, прекратить вещать, выйти из потока
        return 1;
        //mailBox.cond.notify_all(); - здесь не делать! это сделает endCall,
        //когда завершит потоки, закроет сокеты, и только после этого вернет работу sdl
      }

      b_timer.setTimer();
     // LOG2(DEVL1, "call(%d):playNTimes2: 0x%x\n", getId(), play_stream);
      lock.unlock();
      //boost::cv_status cv_st = mailBox.cond.wait_for(locker,  chrono::milliseconds(100));
      res = WaitForSingleObject(b_timer.getTimer(), INFINITE);
      lock.lock();
      switch (res) {
      case WAIT_OBJECT_0:
      { //выдать порцию сообщения в сеть
        b_timer.calc_balance();
//       LOG2(DEVL1, "call(%d):playNTimes3: 0x%x\n", getId(), play_stream);
        if(NULL == play_stream) break;
        CPacket_RTP* rtp_packet = NULL;
        {
          //boost::mutex::scoped_lock(lock_play_stream);
          int unlock_position = 0;
          if(lock_play_stream.compare_exchange_weak(unlock_position, 1, memory_order_seq_cst)){
//            LOG2(DEVL1, "call(%d):playNTimes4: 0x%x\n", getId(), play_stream);
           if(NULL != play_stream){
             rtp_packet = play_stream->read();
             lock_play_stream = (0);
//            LOG2(DEVL1, "call(%d):playNTimes:rtp_packet 0x%x\n", getId(), rtp_packet);
             if(NULL == rtp_packet) break;
           }
           lock_play_stream = (0);
          }else
            break;
         }

        rtp_packet->setCounts(rtp_out_count++, rtp_out_timeStamp);
        rtp_out_timeStamp += rtp_packet->getLengthOfPayload();
        rtp_count++;
//        if(rtp_packet->GetLength() < 100)
//          LOG3(DEVL2, "call(%d):rtp  out:%d, len=%d\n", getId(), rtp_count, rtp_packet->GetLength());

        boost::asio::ip::udp::endpoint ep_sip_out
              = {boost::asio::ip::address_v4(rtp_gw.getIP()), rtp_out};
         try{
           socket_rtp_out.send_to(boost::asio::buffer( rtp_packet->GetData()
               , rtp_packet->GetLength()), ep_sip_out);
         }catch(P_EXC e){
           throw Ev(WSAGetLastError(), "%s: sendto", e.what());
         }

        BOOST_SCOPE_EXIT_ALL(& rtp_packet){ delete rtp_packet; };
      }
      break;
      default:
        throw Ev(WSAGetLastError(), "call(%d):waitforEndOfCall: WaitForSingleObject failed with message %s", strerror(GetLastError()));
      }

      if(_times == (int)infinity && play_stream->isEndOfStream()){
        stopPlaying();
      }
    } while ((_times == (int)infinity || ! play_stream->isEndOfStream()) && (res == EOK));
  } catch (EXC e) {
    E_CATCH(e);
    throw e;
  } catch (P_EXC e) {
    E_CATCH(e);
    throw e;
  }
  return EOK;
}

void CCallManager::setRTPListener()
{
  socket_rtp_in.async_receive_from(boost::asio::buffer(buf_rtp_in), ep_rtp_in,
      boost::bind(&CCallManager::onRTPListen, this,
        boost::asio::placeholders::error,
        boost::asio::placeholders::bytes_transferred));
}

//RTP_listener - слушатель RTP-потока
void CCallManager::onRTPListen(const boost::system::error_code& error, std::size_t bytes_transferred) {
  if(false == (! error)){
    int vl = ERRR;
    if(error.value() == 995) vl = TSTL1;
    LOG3(vl, "call(unk):onRTPListen: error=%d:%s, rtp_in_count=%d\n", error.value(), error.message().c_str(), rtp_in_count);
    return ;
  }
  do try {
      ++rtp_in_count;
      int unlock_position = 0;
      if(lock_listen_stream.compare_exchange_strong(unlock_position, 1, memory_order_seq_cst))
      {
        int cond  = listen_stream[_variantDef] == NULL?0:1;
            cond += listen_stream[_variantAudio] == NULL?0:2;
            cond += listen_stream[_variantFile] == NULL?0:4;

        if (cond == 0) break;

        //накапливаем входящие пакеты
    //    if(bytes_transferred < 100)

        CPacket_RTP rtp_packet(buf_rtp_in.elems, bytes_transferred);
//        LOG3(DEVL1, "call(%d):rtp_in_count:%d, number=%d\n", getId(), rtp_in_count, rtp_packet.getSeqNumber());
        if (cond & 1){
          listen_stream[_variantDef]->write(&rtp_packet);
        }
        if (cond & 2){
          listen_stream[_variantAudio]->write(&rtp_packet);
        }
        if (cond & 4){
          listen_stream[_variantFile]->write(&rtp_packet);
        }
        BOOST_SCOPE_EXIT_ALL(this){ lock_listen_stream = (0); };
      }
  } catch (P_EXC e) {
    E_CATCH(e);
  }
  while(0);
  //
  setRTPListener();
  return ;
}

int CCallManager::setListener(IWavStream *_wav, EListen_Variant _variant){
  if((defListen !=  listen_stream[_variant])) return EBUSY;

  int unlock_position = 0;
  while(!lock_listen_stream.compare_exchange_weak(unlock_position, 1, memory_order_seq_cst))
    Sleep(1);

  listen_stream[_variant] = _wav;

  lock_listen_stream = (0);
  return EOK;
}

void CCallManager::stopListen(EListen_Variant _variant, bool _release){
  if(_variant == _lastVariant){
    stopListen(_variantDef, _release);
    stopListen(_variantAudio, _release);
    stopListen(_variantFile, _release);
    return ;
  }
  LOG2(INFO, "call(%d):stopListen: rtp_in_count:%d\n", getId(), rtp_in_count);
  IWavStream * wav = listen_stream[_variant];
  if(defListen == wav)  return ;

  int unlock_position = 0;
  while(!lock_listen_stream.compare_exchange_weak(unlock_position, 1, memory_order_seq_cst))
    Sleep(1);

  listen_stream[_variant] = defListen;
  if(_release)wav->release();

  lock_listen_stream = (0);
}

int CCallManager::isListening(EListen_Variant _variant){
  if(! isBusy()) return 0;
//  boost::mutex::scoped_lock(lock_listen_stream);
  if(_variant == _lastVariant){
    return CCallManager::isListening(_variantDef) || CCallManager::isListening(_variantAudio) || CCallManager::isListening(_variantFile);
  }
  return (defListen != listen_stream[_variant]);
}


int CCallManager::isPlaying(){
  if(! isBusy()) return 0;
//  boost::mutex::scoped_lock(lock_play_stream);
  if(defPlay == play_stream) return 0;
  if(play_stream->getdB() == 0xDB000DB) return 0xDB;//стыд и срам, я плачу
  return (1);
}

//
int CCallManager::setPlayer(IWavStream* _wav){
  if(NULL == _wav) {
    stopPlaying();
    return EOK;
  }
  if(defPlay != play_stream)  stopPlaying();
  int unlock_position = 0;
  while(!lock_play_stream.compare_exchange_weak(unlock_position, 1, memory_order_seq_cst))
    Sleep(1);

//  { boost::mutex::scoped_lock(lock_play_stream);
  play_stream = _wav;
  play_stream->packetSize( params.packetSize * 8);
  LOG2(DEVL1, "call(%d):SetPlayer pcktSize:%d\n", getId(),  params.packetSize);
//  }
  lock_play_stream = (0);
  return EOK;
}

void CCallManager::stopPlaying(bool _release){
//  boost::mutex::scoped_lock(lock_play_stream);
  int unlock_position = 0;
  while(!lock_play_stream.compare_exchange_weak(unlock_position, 1, memory_order_seq_cst))
    Sleep(1);

  if(play_stream || defPlay != play_stream){
    LOG2(DEVL1, "call(%d):stopPlaying: rtp_out_count:%d\n", getId(), rtp_out_count);
    if(_release && (play_stream != defPlay)){
      play_stream->release();
      play_stream = NULL;
    }
  }
  play_stream = defPlay;
  lock_play_stream = (0);
  //cond_play_stream.notify_all();
}


}
