﻿/** CService_Call_in.h
 * Описание классов обработчиков SDL-сообщений.
 *
 *  Created on: 13.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef CSDL_SIGNALS_H_
#define CSDL_SIGNALS_H_

#include <boost/asio.hpp>
#include <string>
#include <unistd.h>
#include <algorithm>

#include "define.h"
#include "utils/netutils.h"
#include "CdxeCallService.h"
#include "CdxeCallManager.h"

using boost::array;

namespace DXE {
class CVMS_Signal_Release_out;
/**\brief CVMS_Signal_Init - иниц SDL-системы
 * Обработчик InitIPCOM_RQ сигнала
 */
class CVMS_Signal_Init: public SDL::CSDL_Signal_InitIPCOM_RQ {

  typedef SDL::CSDL_Signal_InitIPCOM_RQ base;
protected:
  ///ответный сигнал - подтверждение иниц.
  SDL::CSDL_Signal_InitIPCOM_I init_ipcom;
  ///мастер объект
  CCallService& vms;
public:
  ///Клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Init(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //Получить отправителя сигнала
    vms.defReceiver = GetSender();
    //Залогировать сигнал
    IFLOG(DEVL1) {
      const uint16_t lowPortOfRTP = getLowerBoundaryOfPortsOfRTP();
      //const uint32_t ip = getMyIP();
      //const uint32_t net = getMyIP();
      string str;
      IP(getMyIP()).toString(str);
      //LOG3(3, "Init ip=0x%x, net=0x%x, rtp=%d\n", ip, net, lowPortOfRTP);
      LOG2(DEVL1, " InitIPCOM_RQ UDP_port:%d, MyIP:%s\n", lowPortOfRTP, str.c_str());

      //std::string str1, str2;
      LOG(DEVL1, " the SDL subsystem started successfully\n");
//    LOG2(2, " myIP=%s, myMAC=%s\n"
//        , vms.myIP.toString(str1).c_str()
//        , vms.myMAC.toString(str2).c_str());
      LOGTIME(DEVL1);
//      LOG(INFO, " InitIPCOM_I\n");
//      LOGTIME(INFO);
    }
    //нижняя граница портов RTP
    vms.lowBounderOfRTP = getLowerBoundaryOfPortsOfRTP();

    //ответ на инициализацию, отправляем его
    vms.sendToSDL(&init_ipcom);
    return;
  }

public:
  explicit CVMS_Signal_Init(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/** \brief CVMS_Signal_Err - сигнал Err
 * Системные сообщения SDL-библиотеки
 */
class CVMS_Signal_Err: public SDL::CSDL_Signal_Err {

  typedef SDL::CSDL_Signal_Err base;
protected:
  ///Мастер объект
  CCallService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Err(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //std::string str;toString(str);
    //Залогировать сигнал
    if(!vms.service_started)   return;
//    IFLOG(DEVL1/*vms.err_log_level*/){
      const uint8_t* err_data = getErr();
      LOG10(DEVL1, " Err= %2X %2X %2X %2X %2X %2X %2X %2X %2X %2X\n"
          , err_data[0], err_data[1], err_data[2], err_data[3], err_data[4]
          , err_data[5], err_data[6], err_data[7], err_data[8], err_data[9]);
//    }
//    IFLOG(DEVL2/*vms.err_log_level*/){
//      int i;
//      int len = getLenOfData();
////      LOG5(DEVL2/*vms.err_log_level*/
////          , " %2X %2X %2X %2X %2X\n"
////          , err_data[3], err_data[4], err_data[5], err_data[6], err_data[7]);
//        for (i = 10; i < 20; i++)
//          LOG1(DEVL2, " %2X", err_data[i]);
//        LOG(TSTL0, " \n");
//        for (i = 20; i < 30; i++)
//          LOG1(TSTL0, " %2X", err_data[i]);
//        LOG(TSTL0, " \n");
//        for (i = 30; i < 40; i++)
//          LOG1(TSTL0, " %2X", err_data[i]);
//        LOG(TSTL0, " \n");
//      }
//      LOGTIME(DEVL1);
//    }
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Err(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/** \brief CVMS_Signal_Err - сигнал Err
 * Системные сообщения SDL-библиотеки
 */
class CVMS_Signal_vms_err: public SDL::CSDL_Signal_vms_err {

  typedef SDL::CSDL_Signal_vms_err base;
protected:
  ///Мастер объект
  CCallService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_vms_err(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //std::string str;toString(str);
    //Залогировать сигнал
    if(!vms.service_started)   return;
    IFLOG(DEVL1/*vms.err_log_level*/){
      string s;
      LOG1(DEVL1, "%s\n", toString(s).c_str());
    }
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_vms_err(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};


/**\brief CVMS_Signal_wr_Sender_Table - сигнал wr_Sender_Table
 */
class CVMS_Signal_wr_Sender_Table: public SDL::CSDL_Signal_wr_Sender_Table {
  typedef SDL::CSDL_Signal_wr_Sender_Table base;
protected:
  ///мастер-объект
  CCallService& vms;
public:
  ///Клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_wr_Sender_Table(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //LOG1(DEVL1, "SDL wrSenderTable, id=%d\n", getRowOfTable());
    //vms.setRTP_out(getRowOfTable(), getSrcPort(), getDestIP());
//    LOG3(TSTL1, "wr_Sender_Table: ind=%d, S_UDP_port=%d, D_UDP_port=%d\n"
//              , getRowOfTable(), getSrcPort(), getDestPort());
    if(getDestPort() == 0){
      //LOG1(TSTL1, "wr_Sender_Table(%d): getSrcPort() == 0\n", getRowOfTable());
      return ;
    }
    ICallManager_SIP* call = vms.getCallManager(getRowOfTable());
    if(NULL == call){
      LOG1(WARN, "wr_Sender_Table(%d): no such callManager\n", getRowOfTable());
    }else
      call->putWrSenderTable(getDestPort(), getDestIP());

    if(!vms.service_started) return;

    IFLOG(TSTL1){
      std::string str, str1;
      LOG3(TSTL1, "SDL wr_Sender_Table: id=%d, S_UDP_port=%d, D_UDP_port=%d\n"
          , getRowOfTable(), getSrcPort(), getDestPort());
      LOG5(TSTL1, ", SSRC=%d, SizePacket=%d, Codec=%d, MAC=%s, IPaddr=%s\n"
          , 0, getLenOfRTPFrame(), getCodec(), MAC(getDestMAC()).toString(str1).c_str(),
          (IP(getDestIP()).toString(str)).c_str());
    }
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_wr_Sender_Table(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_wr_Receiver_Table - сигнал wr_Receiver_Table
 *
 */
class CVMS_Signal_wr_Receiver_Table: public SDL::CSDL_Signal_wr_Receiver_Table {
  typedef SDL::CSDL_Signal_wr_Receiver_Table base;
protected:
  ///Мастер-объект
  CCallService& vms;
public:
  ///Клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_wr_Receiver_Table(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //Задать RTP-порты для исходящего трафика
    //vms.setRTP_in(getRowOfTable(), getDestPort());
    if(getDestPort() == 0)
      return ;

    vms.setRTP_in(getRowOfTable(), getDestPort());
//    CCallManager* call = vms.getCallManager(getRowOfTable());
//    if(NULL == call){
//      LOG1(TSTL1, "wr_Receiver_Table(%d): no such callManager\n", getRowOfTable());
//    }else
//      call->setRTP_in(getDestPort());
    if(!vms.service_started)   return;
    LOG2(/*vms.err_log_level*/TSTL1, " wr_Receiver_Table: Ind=%d, D_UDP_port=%d\n", getRowOfTable(), getDestPort());
    LOGTIME(vms.err_log_level);
//    LOG1(4, "  dMAC=%s\n"
//        , MAC(getDestMAC()).toString(str).c_str());
//    LOG4("  lenOfRTP=%d, codec=%d\n",  getLenOfRTPFrame(), getCodec());
//    int row = getRowOfTable();
//    if(row >= pool_ofService_Call_in.s)
//    pool_ofService_Call_in[]
    return;//base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_wr_Receiver_Table(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_UDPDR_long - сигнал UDPDR_long
 * Исходящий UDP-пакет.
 */
class CVMS_Signal_UDPDR_long: public SDL::CSDL_Signal_UDPDR_long {
  typedef SDL::CSDL_Signal_UDPDR_long base;
protected:
  ///мастер-объект
  CCallService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_UDPDR_long(vms, GetSignal(_sdl_sig));
  }
  //обработчик для алгоритма замены
  static bool IsReturn(char i) {
    return (i == '\r');
  }

  const std::string& toString(std::string& str) const {
    IFLOG(DEVL1) {
      str = "UDPDR_long";
      str += ": IP="; str += IP(getIP()).toString();  str += ", data=";
      //залогировать сигнал
      const int maxLen = 512;
      int l =  getLengthOfData(); if(l>maxLen) l = maxLen;
      str.append( (char*)getData(), l);
      std::replace_if(str.begin(), str.end(), IsReturn, ' ');
        //LOG2(DEVL1, " UDPDI_long: IP=%s, data=\n%s\n", IP(getIP()).toString(str).c_str(), s);
    }
    return str;
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //залогировать сигнал
//    IFLOG(DEVL1) {
//      //std::array<char, 512> s;
//      int maxLen = 255;
//      if (DEVL2 < log_level) {
//        maxLen = 512;
//      }
//      int l =  getLengthOfData(); if(l>maxLen) l = maxLen;
////      memcpy(s, getData(), l);
////      s[l] = '\0';
//      string str1(getData(), l);
//      std::replace_if(str1.begin(), str1.end(), IsReturn, ' ');
//      LOG2(DEVL1, " UDPDR_long: IP=%s, data=\n%s\n", IP(getIP()).toString().c_str(), str1);
//    }
//    //Отправить данные пакета в сеть
//    const uint8_t* buf = getData();
//    const uint32_t len = getLengthOfData();
//
    string s;
    LOG1(DEVL1, "SDL: %s\n", toString(s).c_str());
    vms.sendToSIP(getIP(), getData(), getLengthOfData());
//
//    vms.sip_other.sin_addr.S_un.S_addr = htonl(getIP());
//    if (-1
//        == (sendto(vms.socket_sipudp_out, (const char*) buf, len, 0,
//            (sockaddr*) &(vms.sip_other), sizeof(sockaddr_in))))
//      throw E(WSAGetLastError(), "UDPDR_long:sendto");
//
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_UDPDR_long(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_UDPDI_long - сигнал UDPDI_long
 * Входной для SDL сигнал.
 */
class CVMS_Signal_UDPDI_long: public SDL::CSDL_Signal_UDPDI_long {
  typedef SDL::CSDL_Signal_UDPDI_long base;
public:
  static bool IsReturn(char i) {
    return (i == '\r');
  }

  const std::string& toString(std::string& str) const {
    IFLOG(DEVL1) {
      str = "UDPDI_long";
      str += ": IP="; str += IP(getIP()).toString();  str += ", data=";
      //залогировать сигнал
      const int maxLen = 512;
//        if (DEVL2 < log_level) {
//          maxLen = 512;
//        }
      int l =   UDPDI_long_data->Param4; if(l>maxLen) l = maxLen;
      str.append((char*)UDPDI_long_data->Param5, l);
      std::replace_if(str.begin(), str.end(), IsReturn, ' ');
        //LOG2(DEVL1, " UDPDI_long: IP=%s, data=\n%s\n", IP(getIP()).toString(str).c_str(), s);
    }
    return str;
  }

public:
  explicit CVMS_Signal_UDPDI_long(uint32_t _sIP, uint16_t _sPort, uint16_t _dPort,
      uint32_t _len, uint8_t* _upd_data) :
      base(_sIP, _sPort, _dPort, _len, _upd_data) {
  }
};

/**\brief CVMS_Signal_Release - сигнал Release
 * Отбой соединения. для SDL выходной. */
class CVMS_Signal_Release: public SDL::CSDL_Signal_vms_release_env {
  typedef SDL::CSDL_Signal_vms_release_env base;
protected:
  ///мастер-объект
  CCallService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Release(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия
  virtual void OnReceive(void* data) {
    //Отбой соединения, по его номеру
    LOG1(DEVL1, "SDL Release, id=%d\n", getId());
    ICallManager_SIP* call = vms.getCallManager(getId());
    if(NULL == call){
      LOG(TSTL1, "release: no such callManager\n");
      return ;
    }
    call->putRelease(getId());
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Release(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_Release_out - сигнал Release
 *  Отбой соединения. для SDL входной. */
class CVMS_Signal_Release_out: public SDL::CSDL_Signal_vms_release_env_in {
  typedef SDL::CSDL_Signal_vms_release_env_in base;
public:
  explicit CVMS_Signal_Release_out(uint8_t _id)
    :base(_id)
  {  }
};

/**\brief CVMS_Signal_Setup - Сигнал входящий вызов.
 * Запрос на установку соединения. для SDL выходной. */
class CVMS_Signal_Setup: public SDL::CSDL_Signal_vms_setup_env {
  typedef SDL::CSDL_Signal_vms_setup_env base;
protected:
  ///мастер-объект
  CCallService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Setup(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия
  virtual void OnReceive(void* data) {
    //Определить номер и доб номер абонента
    LOG4(DEVL1, "SDL Setup, id=%d, number=%s, add=%s, ip=%x\n", getId(), getDigitsN().c_str(), getDigitsD().c_str(), getSrcIP());
    //входящий вызов - запрос на соединение
    //получить управл через id - внутренний метод
    ICallManager_SIP* call = vms.getCallManager_forCallIn(getId(), getDigitsN(),
        getDigitsD(), getSrcIP());
// 		CCallManager* call = vms.getCallManager(getId(), CCallService::_setup);
    if(NULL == call){
      LOG1(WARN, "setup(%d): no such callManager, release\n", getId());
      CVMS_Signal_Release_out release(getId());
      vms.sendToSDL(&release);
      return;
    }
    call->putInvite(getId(), getDigitsN(), getDigitsD(), getSrcIP());
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Setup(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_Setup - Сигнал входящий вызов.
 * Запрос на установку соединения. для SDL выходной. */
class CVMS_Signal_Setup_res: public SDL::CSDL_Signal_vms_setup_res {
  typedef SDL::CSDL_Signal_vms_setup_res base;
protected:
  ///мастер-объект
  CCallService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Setup_res(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия
  virtual void OnReceive(void* data) {
    //Определить номер и доб номер абонента
    LOG4(DEVL1, "SDL Setup_res, id=%d, number=%s, add=%s, ip=%x\n", getId(), getDigitsN().c_str(), getDigitsD().c_str(), getSrcIP());

    ICallManager_SIP* call = vms.getCallManager_forCallOut(getId(), getDigitsN(),
        getDigitsD(), getSrcIP());
    if(NULL == call){
      LOG1(ERRR, "setup_res(%d): no such callManager, release\n", getId());
      CVMS_Signal_Release_out release(getId());
      vms.sendToSDL(&release);
      return;
    }
    call->putSetup_res(getId(), getDigitsN(), getDigitsD(), getSrcIP());
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Setup_res(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_Setup_out - Сигнал исходящий вызов.
 * Запрос на установку соединения. для SDL входной. */
class CVMS_Signal_Setup_out: public SDL::CSDL_Signal_vms_setup_env_in {
  typedef SDL::CSDL_Signal_vms_setup_env_in base;
public:
  explicit CVMS_Signal_Setup_out(
      const std::string& _digi_n, const std::string& _digi_d, uint32_t _dsst_ip) :
      base(-1, _digi_n, _digi_d, _dsst_ip)
  {  }
};

/**\brief CVMS_Signal_Connect - сигнал Connect
 * Принятие соединения. для SDL выходной. */
class CVMS_Signal_Connect: public SDL::CSDL_Signal_vms_connect_env {
  typedef SDL::CSDL_Signal_vms_connect_env base;
protected:
  ///мастер-объект
  CCallService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Connect(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия
  virtual void OnReceive(void* data) {
    //Принятие соединения, по его номеру
    LOG1(DEVL1, "SDL Connect, id=%d\n", getId());
    ICallManager_SIP* call = vms.getCallManager_forCallOut(getId());
    if(NULL == call){
      LOG1(WARN, "connect(%d): no such callManager, release\n", getId());
      CVMS_Signal_Release_out release(getId());
      vms.sendToSDL(&release);
      return ;
    }
    call->putConnect(getId());
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Connect(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_Connect - сигнал Connect
 * Принятие соединения. для SDL входной. */
class CVMS_Signal_Connect_out: public SDL::CSDL_Signal_vms_connect_env_in {
  typedef SDL::CSDL_Signal_vms_connect_env_in base;
public:
  explicit CVMS_Signal_Connect_out(uint8_t _id)
    :base(_id)
  {  }
};

/** \brief CVMS_Signal_Info - сигнал Info
 * Cообщения SIP-протокола, поступающие после установки соединения
 */
class CVMS_Signal_Info: public SDL::CSDL_Signal_vms_info_env {
  typedef SDL::CSDL_Signal_vms_info_env base;
protected:
  ///Мастер объект
  CCallService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Info(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //сообщить соотв потоку, что был сигнал!
    //LOG(7, " info!!!\n");
    //Залогировать сигнал
    IFLOG(DEVL1){
      string str; toString(str);
      LOG1(DEVL1, " DEVL1 %s\n", str.c_str());
      LOGTIME(DEVL1);
    }
    try{
      ICallManager_SIP* call = vms.getCallManager(getId());
      if(NULL == call){
        LOG1(WARN, "setup(%d): no such callManager\n", getId());
        return;
      }
      call->putSIP_Info(getData1(), getData2(), getData3());
    }catch(P_EXC e){
      E_CATCH(e);
    }
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Info(CCallService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

}

#endif /* CSERVICE_CALL_IN_H_ */
