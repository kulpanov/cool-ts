/** sdl_listener.cpp
 * Метод-слушатель SDL-сигналов
 *  Created on: 23.03.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <string>
#include <unistd.h>
#include <algorithm>

#include "define.h"

#include "vms_signals.h"
#include "CdxeCallService.h"

namespace DXE {

//void CCallService::call_SDL_listener(CCallService* _me){
////  boost::mutex::scoped_lock locker(_me->lock);
////  if(NULL == _me->call_out_thread){
////    _me->call_out_thread = new boost::thread(call_out_static,
////        _me, _number, _add, _s_ip, _timeout);
////    return ;
////  }
////  locker.unlock();
//  try{
//    _me->SDL_listener();
//  }catch(...){
//    CLOG(ERRR, "UNEXPECTED ERROR, EXIT\n", _me);LOGTIME(ERRR);
//    exit(1);
//  }
//}

//Метод-слушатель входящих SDL-сигналов
void* CCallService::SDL_listener() {
  LOG(TSTL1, "SDL_listener: thread started\n");
  try {
    err_log_level = log_level + 1;
    //создать сокет для исходящего SIP-потока
//    if (-1 == (socket_sipudp_out = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
//      throw E(WSAGetLastError(), "SDL_listener:socket");
//    //заполнить поля сокета
//    memset((char *) &sip_other, 0, sizeof(sockaddr_in));
//    sip_other.sin_family = AF_INET;
//    sip_other.sin_port = htons(net.sipPort_out);
    //sip_other.sin_addr.S_un.S_addr = htonl(gwIP);

    //добавляем обработчики исходящих сигналов SDL
    sdl_vms.addListener(new CVMS_Signal_Err(*this));
    sdl_vms.addListener(new CVMS_Signal_vms_err(*this));
    sdl_vms.addListener(new CVMS_Signal_wr_Receiver_Table(*this));
    sdl_vms.addListener(new CVMS_Signal_wr_Sender_Table(*this));

    //Старт системы, строка запуска зарезервирована для будущего использования
    //По старту происходит создание отдельных потока SDL-системы
    sdl_vms.start(" ");

    {///выбрать все сообщения, по старту системы
      int timeOut = 3;//
      do {
        SDL::CSDL_Signal* sig = sdl_vms.receive(timeOut);
        if (NULL == sig)
          break;//timout - признак выборки всей очереди сообщений
        delete sig;
      } while (1);
    }
    //добавляем обработчиков
    sdl_vms.addListener(new CVMS_Signal_Init(*this));
    sdl_vms.addListener(new CVMS_Signal_UDPDR_long(*this));
    sdl_vms.addListener(new CVMS_Signal_Setup(*this));
    sdl_vms.addListener(new CVMS_Signal_Release(*this));
    sdl_vms.addListener(new CVMS_Signal_Connect(*this));
    sdl_vms.addListener(new CVMS_Signal_Info(*this));
    sdl_vms.addListener(new CVMS_Signal_Setup(*this));
    sdl_vms.addListener(new CVMS_Signal_Setup_res(*this));
    {//Создаём сигналы инициализации
      SDL::CSDL_Signal_InitIP initIP(myIP, myNET, 0);
      //и отправляем его
      sendToSDL(&initIP);
    }
    {
      SDL::CSDL_Signal_InitRTPPort initPort(firstPort);
      sendToSDL(&initPort);
    }
    //ждём остальных
    start_barier->wait();

    {//Основной цикл выборки сообщений
      int timeOut = 2;
      while (1) {
        //Блокировка потока на ожидании сигнала,
        //также поток разблокируетя по истечению timeOut секунд
        SDL::CSDL_Signal* sig = sdl_vms.receive(timeOut);
        if (NULL == sig) {
          err_log_level = INFO;
          timeOut = 180;
          continue;
        }
        delete sig;
      }
    }
    //...


  } catch (P_EXC e) {
    //в случае искл ситуации - залогировать и выйти
    E_CATCH(e);
  }

  CCallService::Exit(1, this);
  return NULL;
}

}
