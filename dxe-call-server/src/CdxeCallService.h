/** \brief Основной класс сервера.
 * CVoiceMailService.h
 * Описание основных классов сервиса.
 *  Created on: 11.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef CCallService_H_
#define CCallService_H_

#include <string>

#ifndef HAVE_REMOTE
#define HAVE_REMOTE
#endif
#include <winsock2.h>

#include <pthread.h>

#include <set>
#include <memory>

#define BOOST_THREAD_USE_LIB
#include <boost/thread.hpp>
#include <boost/date_time.hpp>

//#define BOOST_ASIO_ENABLE_HANDLER_TRACKING
#include <boost/asio.hpp>

//#include "define.h"
#include "utils/xml/iniFileXML.h"
#include "utils/netutils.h"
#include "dtmfgen.h"
#include "dxe-call-server.h"
#include "sdl_sys.h"

///Пространство имён DXE
namespace DXE {

//forward
class CCallManager;
class CCallService;


/** \brief CVMS_system - класс работы с SDL-системой.
 *Предок берётся из библиотеки SDL.
 *Класс не переписывает каких либо методов.
 */
class CVMS_system: public SDL::CSDL_system {
typedef SDL::CSDL_system base;
public:
  CCallService* vmService;
public:
  virtual void addListener(const SDL::CSDL_Signal *_sig){
    base::addListener(_sig);
  }
private:
  boost::mutex vms_lock;
public:
//методы доступа к управл
  boost::mutex& getVMSLock(){return vms_lock;};

public:
  CVMS_system():base(),vmService(NULL){};
};

/** \brief CCallService - Диспетчер соединений.
 * Основной класс работы сервиса голосовых сообщений.
 */
class CCallService:public ICallService {
protected:
   ///Основной объект работы с SDL-системой
  CVMS_system sdl_vms;
private:
  //
  boost::asio::io_service ioservice;
  //
  void startIOService();
public:
  boost::asio::io_service& getIOS(){return ioservice;};

public:
  int err_log_level;
protected:
  std::string fname_settings;
  int indx;
public:
  //принимающий SDL-сообщения в vms
  void* defReceiver;
protected:
  ///мутекс синхронизации
  boost::mutex lock;
  ///стартовый барьер для потоков
  boost::barrier* start_barier;

protected:
    ///IP адрес компьютера
    IP myIP;
    ///Подсеть компьютера
    IP myNET;
    ///
    uint32_t firstPort;
public:
    IP realIP;
    IP gwIP;
public:
  virtual const pair<const IP&, const IP&> getIPNet(){
    return pair<const IP&, const IP&>( myIP, myNET);
  }
public:
  enum { IPDATA_BUFLEN=2048};
  struct sparams{
    ///порт для ввода SIP-сообщений
    IP_PORT sipPort_in;
    ///порт для вывода SIP-сообщений
    IP_PORT sipPort_out;
    int rtp_in_offs;
    int rtp_out_offs;
  };
  sparams net;
//  sparams sdl;
  ///Нижняя граница портов RTP-потоков
  IP_PORT lowBounderOfRTP;

public:
  ///выходной сокет для SIP
//  int socket_sipudp_out;
//  ///структура настройки сокета
//  struct sockaddr_in sip_other;
protected:
  ///Число потоков пула соединений
  //int poolCount;
  ///Макс число потоков пула соединений TODO: вернуть 254 и в SDL тоже!
#define POOL_MAX_COUNT 254
  ///Пул потоков соединений, динамичный
  std::vector<CCallManager*> calls;
protected:
  uint16_t rtp_in[POOL_MAX_COUNT];
  pair<uint16_t, IP> rtp_out[POOL_MAX_COUNT];
public:
  ///сервис стартовал
  bool service_started;
//  //sip scocket
//  int sip_in_socket;
protected:
  ///поток слушающий SIPсообщения
  boost::thread sdl_listener;
  ///Метод-слушатель входящих SDL-сигналов
  void* SDL_listener();
//  static void call_SDL_listener(CCallService* _me);
protected:
  //sip socket
  boost::array<uint8_t, IPDATA_BUFLEN> sip_in_buffer;
  boost::asio::ip::udp::endpoint ep_sip_in;
  boost::asio::ip::udp::endpoint ep_sip_out;
  boost::asio::ip::udp::socket sip_in_socket;
  boost::asio::ip::udp::socket sip_out_socket;
public:
  ///поток слушающий SIPсообщения
  //boost::thread sip_listener;
  ///Установить SIP слушателя
  void setSIPListener();
  ///Метод-слушатель SIP-сообщений
  void onSIPListen(const boost::system::error_code& error, std::size_t bytes_transferred);
  ///отправить SIP-пакет
  void sendToSIP( uint32_t _ip, const uint8_t* buf, const uint32_t len);
protected:
  ///Обработчик сигналов ОС
  void signalHandler(/*const boost::system::error_code& ec,*/ int signal_number);
public:
  //методы доступа к управл со стороны SIP(снизу)
  boost::mutex& getVMSLock(){return sdl_vms.getVMSLock();};

  /** Установить порт входящего потока
   * @param _rtp_in номер порта */
  void setRTP_in(int _pos, int _rtp_in) {
    ASSERT(_pos <= POOL_MAX_COUNT);
    boost::mutex::scoped_lock(lock);
    rtp_in[_pos] = _rtp_in;
  }

  int getRTP_in(int _pos) {
    ASSERT(_pos <= POOL_MAX_COUNT);
    boost::mutex::scoped_lock(lock);
    return rtp_in[_pos];
  }

  /** Установить порт исходящего потока
   * @param _rtp_out номер порта */
  void setRTP_out(int _pos, int _rtp_out, const IP& _ip) {
    ASSERT(_pos <= POOL_MAX_COUNT);
    boost::mutex::scoped_lock(lock);
    rtp_out[_pos].first = _rtp_out;
    rtp_out[_pos].second = _ip;
  }

  const pair<uint16_t, IP>& getRTP_out(int _pos) {
    ASSERT(_pos <= POOL_MAX_COUNT);
    boost::mutex::scoped_lock locker(lock);
    return rtp_out[_pos];
  }

  /** \brief Послать SDL-сигнал в библиотеку SDL/
   * @param _sig - сигнал
   */
  void sendToSDL(SDL::CSDL_Signal* _sig);

  ///возвращает созданного управляющего
  ICallManager_SIP* getCallManager(int _id);
  ///возвращает созданного управляющего ожидающего завершения исходящего соединения
  ICallManager_SIP* getCallManager_forCallOut(int _id);
  ///
  ICallManager_SIP* getCallManager_forCallOut(const uint32_t _id,
      const CString& _number, const CString& _add, const IP& _s_ip);

  ///возвращает созданного управляющего ожидающего входящего соединения
  ICallManager_SIP* getCallManager_forCallIn(const uint32_t _id,
      const CString& _number, const CString& _add, const IP& _s_ip);

public:
  //методы и структуры интерфейса
  ///метод интерфейса, создаёт управляющего
  virtual ICallManager* createCallManager(ICallManager::EStatus _init = ICallManager::_free);
  ///убираем управл из списка
  virtual void releaseCallManager(const ICallManager* call);

  /** \brief Старт работы системы
   * @param _opts - опции запуска, не используется
   */
  virtual void start(const std::string& _opts);

  //закрыть все соединения
  virtual void closeAll();

  ///Загрузить параметры
  virtual void loadParams(int type = 0);

  ///Сохранить параметры
  virtual void saveParams();

  ///
  virtual void setDTMFParams(int _ampl, int _duration, int _pause);

  ///удалить себя, метод интерфейса
  virtual ICallService* release();
private:
  //std::map<char, tVoiceMsg*> dtmfList;
  CDTMFGenerator dtmf_gen;
  tVoiceMsg wavq_hello;
  boost::mutex muHello;
public:
  //
  CDTMFGenerator& getDTMF();
  //
  const tVoiceMsg& getHello(int rtpSize){
    boost::mutex::scoped_lock lck(muHello);
    tVoiceMsg::Clear_qmsg(wavq_hello);
    tVoiceMsg::Load_qmsg_fromWAV(wavq_hello, "wavs\\hello.wav", rtpSize);
    return wavq_hello;
  };
protected:
  ///конструктор
  CCallService();
public:
  ///выход из программы
  static void Exit(int code, CCallService* self);
  ///make-функция
  //static CCallService& Create();
  ///деструктор
  virtual ~CCallService();
friend
  class ICallService;
};

}

#endif /* SMAINDATA_H_ */
