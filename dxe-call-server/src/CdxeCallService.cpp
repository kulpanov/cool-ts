/** \brief  Реализация класса CVoiceMailService.
 * CVoiceMailService.cpp
 *  Created on: 11.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#include <time.h>
#include <winsock2.h>
#include <windows.h>
#include <signal.h>

#include "define.h"
#include "utils/netutils.h"
#include "utils/xml/iniFileXML.h"
#include "vms_signals.h"
#include "CdxeCallService.h"
#include "CdxeCallManager.h"

//#include <boost/asio.hpp>
//#include <boost/bind.hpp>
namespace DXE {
using boost::asio::ip::udp;
//обработчик сигналов ОС
void CCallService::signalHandler(/*const boost::system::error_code& ec, */int signal_number){
  if(signal_number == SIGTERM){
    printf("flushing buffers, trying close all and exit\n");
    LOGTIME(ERRR);
    closeAll();
    delete this;
    exit(0);
  }
}

void CCallService::sendToSDL(SDL::CSDL_Signal* _sig){
  _sig->SetReceiver(defReceiver);

  std::string s;_sig->toString(s);
  LOG1(DEVL1, "SDL: send: %s\n", s.c_str());
  sdl_vms.send(_sig);
}

//получить управляющего по id соединения
ICallManager_SIP* CCallService::getCallManager(int _id) {
  CCallManager* res = NULL;
  boost::mutex::scoped_lock locker(lock);
  LOG2(TSTL1, "getCallManager:id=%d, csize=%d\n", _id, calls.size());
  auto it = calls.begin();
  for( ; it != calls.end(); it++){
    LOG5(TSTL0, "getCallManager:0x%p,id=%d,status=%d,number=%s,add=%s\n", (*it), (*it)->getId(), (*it)->getStatus(), (*it)->getConnectNumber().c_str(), (*it)->getConnectDigits().c_str());

    if(!(*it)->isReserved())
    if((*it)->getId() == _id){
        if(! (*it)->isFree()){
          res = (*it);
        }else{
          res = NULL;
          //UNLOCK(&lock);
          LOG2(WARN, "getCallManager(%d):(*it=0x%p)->getStatus() == _free\n", _id, (*it));
          //ASSERT(0 && "getCallManager():(*it)->getStatus() == _free\n");
        }
      break;
    }
  }
  return res;
}


//найти первого, кто ожидает соединения(_call) и вернуть
//Вот здеь мб косяк, тк при одновременном установлении неск исх соединений
//- неизвестно для кого из них пришел connect
ICallManager_SIP* CCallService::getCallManager_forCallOut(int _id) {
  boost::mutex::scoped_lock locker(lock);
  LOG2(TSTL1, "getCallManager_forCallOut:id=%d, csize=%d\n", _id, calls.size());
  auto it = calls.begin();
  for( ; it != calls.end(); it++){
    LOG5(TSTL0, "getCallManager_forCallOut:0x%p,id=%d,status=%d,number=%s,add=%s\n", (*it), (*it)->getId(), (*it)->getStatus(), (*it)->getConnectNumber().c_str(), (*it)->getConnectDigits().c_str());

    if((*it)->isCalling_out(2))
      if((*it)->getId() == _id)
      {
        return (*it);
      }
  }
  return NULL;
}

ICallManager_SIP* CCallService::getCallManager_forCallOut(const uint32_t _id,
    const CString& _number, const CString& _add, const IP& _s_ip) {
  CCallManager* res = NULL;
  boost::mutex::scoped_lock locker(lock);
  LOG3(TSTL1, "getCallManager_forCallOut:number=%s,add=%s, csize=%d\n", _number.c_str(), _add.c_str(), calls.size());
  auto it = calls.begin();
  for( ; it != calls.end(); it++){
    LOG5(TSTL0, "getCallManager_forCallOut:0x%p,id=%d,status=%d,number=%s,add=%s\n", (*it), (*it)->getId(), (*it)->getStatus(), (*it)->getConnectNumber().c_str(), (*it)->getConnectDigits().c_str());
    if((*it)->isCalling_out(1))
      if((*it)->checkConnectData(_number, _add, _s_ip, true))
      {
        res = (*it);
        break;
      }
  }

  return res;
}

//входящий вызов,найти свободного (_free) и вернуть
//если управл ждет звонка от определённого источника
ICallManager_SIP* CCallService::getCallManager_forCallIn(const uint32_t _id,
    const CString& _number, const CString& _add, const IP& _s_ip) {
  CCallManager* res = NULL;
  boost::mutex::scoped_lock locker(lock);
  LOG3(TSTL1, "CCallService::getCallManager_forCallIn:number=%s,add=%s, calls_size=%d\n", _number.c_str(), _add.c_str(), calls.size());
  auto it = calls.begin();
  for( ; it != calls.end(); it++){
    LOG5(TSTL0, "CCallService::getCallManager_forCallIn:0x%p,id=%d,status=%d,number=%s,add=%s\n", (*it), (*it)->getId(), (*it)->getStatus(), (*it)->getConnectNumber().c_str(), (*it)->getConnectDigits().c_str());
    if(!(*it)->isReserved())
      //if((*it)->getId() == -1)
        if((*it)->isFree())
          if((*it)->checkConnectData(_number, _add, _s_ip))
          {
            res = (*it);
            break;
          }
  }

  return res;
}

///внутренний метод, убираем управл из списка
void CCallService::releaseCallManager(const ICallManager* _call_manager){
  boost::mutex::scoped_lock locker(lock);
  LOG2(INFO, "CCallService::releaseCallManager=0x%p:calls.size()=%d\n", _call_manager, calls.size()-1);
  auto it = calls.begin();
  for( ; it != calls.end(); it++){
      if((*it) == _call_manager)
      {
        calls.erase(it);
        break;
      }
  }
}

//удалить себя!
ICallService* CCallService::release(){
  delete this;
  return NULL;
}

//закончить
void CCallService::Exit(int code, CCallService* self){
  CLOG(ERRR, "UNEXPECTED ERROR, EXIT!", self);LOGTIME(ERRR);
//  self->release();
  exit(code);
}

//Общие конфиг данные
void CCallService::loadParams(int type){
  //Читаем файл настроек
  CIniFileXML cfg;
  cfg.SetPath(fname_settings);
  cfg.ReadFile(10);
  string cfg_str;
  boost::mutex::scoped_lock locker(lock);
  //Уровень вывода лога
  cfg_str = cfg.GetAttribute("General/logLevel","value","3");
  log_level = atoi(cfg_str.c_str());

  cfg_str = cfg.GetAttribute("General/indx", "value", "1");
  indx = cstr::toInt(cfg_str);
  //    if(type == 0){
  //читаем мой IP
  cfg_str = cfg.GetAttribute("General/myIP", "value", "127.0.0.1");
  myIP    = IP(cfg_str);

  firstPort = atoi(cfg.GetAttribute("General/myPort", "value", "5064").c_str());
  LOG1(DEVL1, "MyPort=%d\n", firstPort);
  //моя сеть
  cfg_str = cfg.GetAttribute("General/myNet","value","255.255.255.0");
  myNET   = IP(cfg_str);

  //Порт для входящих SIP-соединений
  cfg_str = cfg.GetAttribute("General/SIPin","value","5060");
  net.sipPort_in = atoi(cfg_str.c_str()); //порт для SIP-сообщений
  //Порт для исходящих SIP-соединений
  cfg_str = cfg.GetAttribute("General/SIPout","value","5061");
  net.sipPort_out = atoi(cfg_str.c_str());
//смещение портов и адресов для отладочного запуска на одном ip
  net.rtp_in_offs   = (net.sipPort_in == 5060 )? 0 : 10000;
  net.rtp_out_offs  = (net.sipPort_out == 5060 )? 0 : 10000;
  if(net.sipPort_in != net.sipPort_out){
    realIP  = IP(cfg.GetAttribute("General/realIP", "value", "127.0.0.1"));
    gwIP    = IP(cfg.GetAttribute("General/gwIP", "value", "127.0.0.1"));
  }

  int packetSize = cfg.GetValueI("RTP/packetSize", 60);
  if(packetSize>60) packetSize = 60;
  if(packetSize<20) packetSize = 20;
  IWavStream::rtp_packet_size = packetSize * 8;
  LOG1(DEVL1, "rtp_packet_size=%d\n", IWavStream::rtp_packet_size);

  tVoiceMsg::Load_qmsg_fromWAV(wavq_hello, "wavs\\hello.wav", IWavStream::rtp_packet_size);
}

//сохранить параметры
void CCallService::saveParams(){
  //нет таких параметров
}

void CCallService::setDTMFParams(int _ampl, int _duration, int _pause){
  boost::mutex::scoped_lock locker(lock);
  dtmf_gen = CDTMFGenerator(_duration, _pause, _ampl);
}

//jmp_buf env;
void SignalHandler_TERM(int sig_number){
  printf("terminate\n");
  //longjmp(env, sig_number);
}

CDTMFGenerator& CCallService::getDTMF(){
  //boost::mutex::scoped_lock locker(lock);
  return dtmf_gen;
}

#define BOOST_SCOPE_EXIT_ALL(...) //stub for syntax analyser

//Старт работы
void CCallService::start(const std::string& _opts){
  //Залогировать старт работы
  try{
  time_t curtime = time(NULL);
  LOG2(INFO, "DXE CallService started with opts '%s', at %s\n", _opts.c_str(), ctime(&curtime));
  fname_settings = _opts;

  //Загрузить параметры
  loadParams();
  //запустить ios
  startIOService();
  Sleep(500);
  //инит барьер на старт потоков
  (start_barier = new boost::barrier(2));

  sdl_listener = boost::thread(boost::bind(&CCallService::SDL_listener, this));
  Sleep(500);

  //и Я последний, count=3, пошли работать
  start_barier->wait();
  Sleep(5000);

  service_started = true;
  //установить слушателя SIP
    udp::resolver resolver(getIOS());

    uint32_t _ip = myIP;
    if(net.sipPort_in != net.sipPort_out){
      //отладочный режим запускаемся на одном ip, надо вернуть обман SDL
      _ip = realIP;
    }
    sip_in_socket = udp::socket(getIOS(), {boost::asio::ip::address_v4(_ip), net.sipPort_in});

    sip_out_socket = udp::socket(getIOS(), udp::v4());

    setSIPListener();
  }catch(std::exception &e){
    LOG1(ERRR, "start: %s\n", e.what());
  }
}

//закрыть все соединения
void CCallService::closeAll(){
  //boost::mutex::scoped_lock locker(lock);
  auto it = calls.begin();
  for( ; it != calls.end(); it++){
    (*it)->close();
  }
}

///метод интерфейса, создаёт управляющего
ICallManager* CCallService::createCallManager(ICallManager::EStatus _init){
  //и вставляем туда созданного управляющего
  CCallManager* call = new CCallManager(*this, _init);
  boost::mutex::scoped_lock locker(lock);
  calls.push_back(call);
  LOG2(INFO, "CCallService::createCallManager=0x%p:calls.size()=%d\n", call, calls.size());
  return call;
}

//watch dog timer, keep ios alive
static void onTimer_WD(boost::asio::io_service* _ios){
  static boost::asio::deadline_timer* timer_wd = NULL;
  delete timer_wd; timer_wd = NULL;
  if(_ios->stopped()) return;
  timer_wd = new boost::asio::deadline_timer(*_ios, boost::posix_time::seconds(10));
  timer_wd->async_wait(boost::bind(&onTimer_WD, _ios));
}

void CCallService::startIOService() {
  onTimer_WD(&ioservice);
  boost::thread bt([this]() {
    if(!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL)){
      int dwError = GetLastError();
      LOG1(ERRR, "CCallService::startIOService: SetThreadPriority %d\n", dwError);
    }
    ioservice.run(); });
  //::SetThreadPriority(bt.native_handle(), THREAD_PRIORITY_TIME_CRITICAL);
  bt.detach();
}


//собрать
CCallService::CCallService():
  defReceiver(NULL),
  sip_in_socket(ioservice), sip_out_socket(ioservice), dtmf_gen(120, 120, -5)
{
  service_started = false;
  lowBounderOfRTP = 0;
//  socket_sipudp_out = 0;
  //Иниц сокет-подсистему Windows
//  WSADATA lpWSAData;
//  if (WSAStartup(MAKEWORD(2,2), &lpWSAData)!=0) {
//    throw E(WSAGetLastError(), ":CVoiceMailService:WSAStartup");
//  }
  //себя как принимающий сервис
  sdl_vms.vmService = this;
}

//зачистить
CCallService::~CCallService() {
  LOG(INFO, "CCallService: dtor:\n");
  closeAll();
  Sleep(2000);
//  pthread_cancel(sip_listener);
  sdl_listener.interrupt();
//  if(! sdl_listener.try_join_until(boost::chrono::seconds(5)))
  Sleep(1000);
  sdl_listener.detach();// join();

//  pthread_cancel(sdl_listener);
//  closesocket(socket_sipudp_out);
//  for(int i = 0; i<poolCount; i++)
//    delete pool_ofCall_in[i];
  ioservice.stop();
  while(!ioservice.stopped()) Sleep(1);
  WSACleanup();
  //pthread_cond_destroy(&cond);
  LOG(INFO, "CCallService: dtor completed:\n");
}

//создать
ICallService* ICallService::create(const std::string& _opts){
  //if(vmService == NULL) vmService = new CCallService();
  return new CCallService();
}

}


