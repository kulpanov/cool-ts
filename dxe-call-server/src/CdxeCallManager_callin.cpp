/** \brief Реализация методов CService_Call_in обработки входящих звонков и сценариев.
 * callIn_scripts.cpp
 *
 *  Created on: 14.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <vector>
#include <string>

#include "define.h"
#include "utils/netutils.h"
#include "vms_signals.h"
#include "CdxeCallManager.h"
#include <windows.h>

namespace DXE {
using namespace std;

///Обработать входящие соединение
void CCallManager::call_in() {
  LOG4(TSTL1, "call(%d):call_in1:number=%s, add=%s, ip=%s\n", getId(), connect_number.c_str(), connect_add.c_str(), connect_ip.toString().c_str() );
  try {
    if(!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL)){
      int dwError = GetLastError();
      LOG1(ERRR, " CCallManager::call_in: SetThreadPriority %d\n", dwError);
    }

    boost::mutex::scoped_lock locker(lock);
    //разрешение входящего
    callAllow();
    //связь установлена!
    beginCall(connect_number, connect_add, connect_ip);

    LOG1(INFO, "call(%d):call_in:listen/play rtp's and waitforEndOfCall\n", getId());
    setParams_in(rtpRestoreDef, 0);
    tVoiceMsg wavq_hello;// = vms.getHello(params.packetSize * 8);
    tVoiceMsg::Load_qmsg_fromWAV(wavq_hello, "wavs\\hello.wav", params.packetSize * 8);
    LOG2(DEVL1, "call(%d):call_in:hello is loaded, sz=%d\n", getId(), wavq_hello.size());
    if(EOK == playDTMF(wavq_hello))
      continueCall();

  } catch (P_EXC e) {
    E_CATCH(e);
    //исключение, завершить соединение
  }
  LOG2(TSTL1, "call(%d):call_in5:number=%s:endOfCall\n", getId(), connect_number.c_str());
  endCall();
}

//разрешить исходящее соединение
void CCallManager::callAllow() {
  //входящее соединение - разрешить
  LOG1(TSTL1, "call(%d):begin of call_in, callAllow\n", getId());
  CVMS_Signal_Connect_out allow(getId());
  vms.sendToSDL(&allow);
}

}

