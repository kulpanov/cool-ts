/** \brief Реализация методов CService_Call_in обработки входящих звонков и сценариев.
 * callIn_scripts.cpp
 *
 *  Created on: 14.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <vector>
#include <string>
//#include <windows.h>

#include "define.h"
#include "vms_signals.h"
#include "CdxeCallManager.h"

#define BOOST_SCOPE_EXIT_ALL(...) //stub for syntax analyser
#include <boost/scope_exit.hpp>

namespace DXE {
using namespace std;

//исходящий вызов
int CCallManager::call(const CString& _number, const CString& _add
    , int _timeout, IP _s_ip){
  LOG1(INFO, "call(%d):call\n", getId());
  if(_number.size() == 0)
    throw E(0, "callManager:call:Недопустимое значение number");
  if(_add.size() == 0)
    throw E(0, "callManager:call:Недопустимое значение add");

  if(! isFree()){
    //уже работаем, отказ
    LOG3(INFO, "call(%d): %s:%s isn't free\n", getId(), _number.c_str(), _add.c_str());
    throw E(EBUSY, "callManager:call:Управляющий занят");
  }
  //распарсить общий номер в прямой и dtmf номер
  boost::mutex::scoped_lock locker(lock);
//  boost::mutex::scoped_lock(vms.getVMSLock());
  //BOOST_SCOPE_EXIT_ALL(){ MB_SCOPE_EXIT; };
  try{

    LOG4(INFO, "call(%d):call_out1:step1:number=%s, add=%s, ip=%s\n", getId(), _number.c_str(), _add.c_str(), _s_ip.toString().c_str() );
    LOGTIME (INFO);
    //ASSERT(mailBox.cmd == _ok);
    ASSERT(queue_ofMails.empty());

    //ждать ответа от эксклюзивно
    CString direct_number = _number;
    dtmfNumber = parseCallNumber(direct_number);
    connect_number = direct_number;
    connect_add = _add;
    connect_ip = _s_ip;
    {
    boost::mutex::scoped_lock lck(vms.getVMSLock());
    setStatus(_call_out1);
    forReserved = false;//снимаем резерв
    callSetup();

    if(queue_ofMails.empty())
    if(false == cond.timed_wait(locker, boost::posix_time::seconds(60))){
      LOG(ERRR, "call: Timeout, setupRes\n");
      throw E(0xA, "callManager:call:Таймаут ответа SDL системы");
    }
    ASSERT(!queue_ofMails.empty());
    if(_connect != queue_ofMails.front().cmd){
      LOG1(ERRR, "call: not putSetup_res, cmd=%d\n", queue_ofMails.front().cmd);
      throw E(0xB, "callManager:call:Ответом SDL должен быть connect");
    }
    queue_ofMails.pop();
    LOG1(INFO, "call(%d):call_out1: step2\n", getId());
    setStatus(_call_out2);
    }
    call_out_thread = boost::thread(boost::bind( &CCallManager::call_out, this));
  }catch(CException& e){
    markForReserved();setStatus(_free);
    throw e;
  }catch(std::exception& e){
    markForReserved();setStatus(_free);
    throw e;
  }
  return EOK;
}


//void CCallManager::call_out_static(CCallManager* _me
//    /*, CString _number, CString _add, IP _s_ip, const uint32_t _timeout*/){
//
// //  boost::mutex::scoped_lock locker(_me->lock);
//  CLOG(TSTL1, "call_out: thread started\n", _me);
// //  if(NULL == _me->call_out_thread){
// //    _me->call_out_thread = new boost::thread(call_out_static, _me);
// //    return ;
// //  }
// //  locker.unlock();
//  try {
//    _me->call_out(/*_number, _add, _s_ip, _timeout*/);
//  } catch (P_EXC e) {
//    CLOG(ALRM, "call_out: uncatched exception:", _me);
//    CLOG1(ERRR, "%s\n", _me, e.what());
//  }
// //  locker.lock();
// //  delete _me->call_out_thread;
// //  _me->call_out_thread = NULL;
//  CLOG(TSTL1, "call_out: thread finished\n", _me);
//}

//Обработать исходящий вызов
//void CCallManager::processCallOut(const CString& _number,
//    const CString& _add, const IP& _s_ip, const uint32_t _timeout) {
void CCallManager::call_out() {
  try {
    if(!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL)){
      int dwError = GetLastError();
      LOG1(ERRR, "CCallManager::call_out: SetThreadPriority %d\n", dwError);
    }

    boost::mutex::scoped_lock locker(lock);
    ASSERT(status == _call_out2);
    L_again:
    {
    if(queue_ofMails.empty())
      if (false == cond.timed_wait(locker, boost::posix_time::seconds(60))){
        LOG1(WARN, "call(%d):call_out2:timeout of connect!\n", getId() );
        goto L_exit;
      }

    ASSERT(!queue_ofMails.empty());
    if(_release == queue_ofMails.front().cmd){
      LOG1(WARN, "call(%d):call_out2:cmd=release!\n", getId());
      //на выход, здесь не удалять, удалить сообщение только в endCall
      goto L_exit;
    }

    SMailBox mail = queue_ofMails.front();
    queue_ofMails.pop();
    if(_connect !=  mail.cmd) {
      LOG2(WARN, "call(%d):call_out2: cmd=%d, it's NOT connect!\n", getId(), queue_ofMails.front().cmd);
      goto L_again;//ждём только connect!
    }
    //it's connect!
    ASSERT(id == mail.data0);
    }

    //начинаем соединение
    beginCall(connect_number, connect_add, connect_ip);

//    LOG5(TSTL1, "call(%d):callBegin, number=%s, dtmf=%s, digi=%s, ip=%s\n"
//        , getId(), connect_number.c_str(), dtmfNumber.c_str(), connect_add.c_str(), connect_ip.toString().c_str());

    setParams_in(rtpRestoreDef, 0);
    //далее добираем dtmf сигналы
    if(EOK != sendDTMFs(dtmfNumber)) goto L_exit;

    LOG1(INFO, "call(%d):call_out3:listen/play rtp's and waitforEndOfCall\n", getId());
    //активное соединение
    continueCall();

    L_exit:;
  } catch (P_EXC e) {
    E_CATCH(e);
  }
  LOG2(TSTL1, "call(%d):call_out:number=%s:endOfCall\n", getId(), connect_number.c_str());
  endCall();
  return;
}

//начать исходящее соединение
void CCallManager::callSetup() {
  LOG2(TSTL1, "call(%d):begin of call_out, callSetup, number=%s \n", -1, connect_number.c_str());
  CVMS_Signal_Setup_out setup(connect_number, connect_add, connect_ip);
  vms.sendToSDL(&setup);
  //ожидаем setup_res
  //ASSERT(mailBox.cmd == _ok);
}

}

