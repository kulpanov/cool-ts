/** \brief Реализация методов CService_Call_in.
 * CService_Call_in.cpp
 *
 *  Created on: 13.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <string.h>

#include "define.h"
#include "utils/utils.h"
#include "vms_signals.h"
#include "CdxeCallService.h"
#include "CdxeCallManager.h"


namespace DXE {

int CCallManager::waitfor(uint32_t _ms) {
  LOG3(INFO, "call(%d): waitfor(%d), number %s\n", getId(), _ms, connect_number.c_str());
  Sleep(_ms);
  return EOK;
}

//#include <boost/chrono.hpp>
//#include <boost/date_time.hpp>

///ждать устновления соединения, не более ms
int CCallManager::waitforCall(uint32_t _ms) {
  boost::mutex::scoped_lock locker(lock);
  LOG3(INFO, "call(%d): waitforCall(%d), number=%s\n", getId(), _ms, connect_number.c_str() );
  while(! isBusy()){
    if(false == cond_status.timed_wait(locker, boost::posix_time::milliseconds(_ms))){
      if(!isBusy()){
        return ETIMEDOUT;
      }else
        LOG(ERRR, "CCallManager::waitforCall:notify_all failed?\n");
    }
  }
  return 0;
}


///ждать завершения соединения, не более ms
int CCallManager::waitforClose(uint32_t _ms, bool _andReserv) {
  boost::mutex::scoped_lock locker(lock);
  LOG2(INFO, "call(%d): waitforClose, number=%s\n", getId(), connect_number.c_str());
  while(!isFree()){
    if(false == cond_status.timed_wait(locker, boost::posix_time::milliseconds(_ms))){
      if(!isFree()){return ETIMEDOUT;}else LOG(ERRR, "CCallManager::waitforClose:notify_all failed?\n");
    }
//    if(boost::cv_status::timeout == cond_status.wait_for(locker
//                          , boost::chrono::milliseconds(_ms))) return ETIMEDOUT;
  }
  if(_andReserv)markForReserved();
  return 0;
}

//int CCallManager::waitforClose_nolock(uint32_t _ms) {
//  LOG2(INFO, "call(%d): waitforClose(%d)\n", getId(), _ms);
//  int res = 0;
//  do {
//    if (isFree()){
//      return EOK;
//    }
//    res = waitforSignal(_ms, &cond_status, &lock, nounlock);
//  } while (res != ETIMEDOUT);
//  LOG2(TSTL1, "call(%d): waitforClose(%d), time out\n", getId(), _ms);
//  return ETIMEDOUT;
//}
//
// //ждать окончания потока, если таковой есть, не более ms
//int CCallManager::waitforListening(uint32_t _ms) {
//  LOG2(INFO, "call(%d): waitforListening(%d)\n", getId(), _ms);
//  int res = 0;
//  do {
//    if (!isListening())
//      return EOK;
//    //isBusy()
//    res = waitforSignal(_ms, &cond_listen_stream, &lock_listen_stream);
//  } while (res != ETIMEDOUT);
//  LOG2(TSTL1, "call(%d): waitforListening(%d), time out\n", getId(), _ms);
//  return ETIMEDOUT;
//}
//
//int CCallManager::waitforPlaying(uint32_t _ms) {
//  LOG2(INFO, "call(%d): waitforPlaying(%d)\n", getId(), _ms);
//  int res = 0;
//  do {
//    if (!isPlaying())
//      return EOK;
//    //isBusy()
//    res = waitforSignal(_ms, &cond_play_stream, &lock_play_stream);
//  } while (res != ETIMEDOUT);
//  LOG2(TSTL1, "call(%d): waitforListening(%d), time out\n", getId(), _ms);
//  return ETIMEDOUT;
//}

}

