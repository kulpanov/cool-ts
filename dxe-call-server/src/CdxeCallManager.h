/**CService_Call_in.h
 * Объявления класса вызова.
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef CSERVICE_CALL_IN_H_
#define CSERVICE_CALL_IN_H_

#include <string>
#include <unistd.h>
#include <atomic>

#include "define.h"
#include "utils/utils.h"
#include "utils/netutils.h"

#include "CdxeCallService.h"

#define BOOST_THREAD_USE_LIB
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/asio.hpp>


namespace DXE {

/** \brief Управляющий соединением.
 * Обработчик из пула соединений, обрабатывает входящие и исходящие соединения.
 * ToDO: возможность повторного использования, сейчас нет
 */
class CCallManager: public ICallManager, public ICallManager_SIP {
protected:
  ///мастер -объект
  CCallService & vms;
protected:
  ///номер соединения - связь c объектами SDL, только! -1=не связан!
  volatile std::atomic_int id;
public:
  ///Id входящего соединения
  inline volatile int getId() const {
    return id;
  }

protected:
  //информация о соединении
  ///номер - источника
  CString connect_number;
  ///доп номер - приемника
  CString connect_add;
  ///ip соединения
  IP connect_ip;
  ///dtmf number
  CString dtmfNumber;
  tVoiceMsg dup_audio;
public:
  virtual CString getConnectNumber() {
    boost::mutex::scoped_lock(lock);
    return connect_number;
  }

  virtual IP getConnectIP() {
    boost::mutex::scoped_lock(lock);
    return connect_ip;
//        LOCK(&lock);
//        IP c_tmp = connect_ip;
//        UNLOCK(&lock);
//        return c_tmp;
  }

  virtual CString getConnectDigits() {
    boost::mutex::scoped_lock(lock);
    return connect_add;
//      lock.lock();//LOCK(&lock);
//      CString c_tmp = connect_add;
//      lock.unlock();//UNLOCK(&lock);
//      return c_tmp;
  }
  ///
  virtual int setConnectData(const CString& _number, const CString& _add,
      const IP& _s_ip);

  ///
  bool checkConnectData(const CString& _number, const CString& _add,
      const IP& _s_ip, bool _strong = false);

protected:
  ///основной mutex
  boost::mutex lock;
  //boost::unique_lock<boost::mutex> lock;
protected:
  ///сигнализация измения статуса
  boost::condition_variable cond_status;
  ///статус соединения @see EStatus
  volatile atomic_int status;
  void setStatus(volatile int _status) {
    if (status != _status) {
      status = _status;
      cond_status.notify_all(); //оповестит всех!
      //volatile int st = status;
      LOG2(TSTL1, "call(%d), status=%s\n", getId(),EStatustoString(status).c_str());
    }
  }

public:
  ///Получить тек статус-состояние объекта
  virtual int getStatus() {
    return (volatile int) status;
  }

  ///резервирован, не может быть использован для входящего
  virtual bool isReserved() {
    //return (getStatus() == _reserved);
    return forReserved;
  }

  virtual bool isBusy() {
    return (status == _busy);
  }

  virtual bool isFree() {
    return (status <= _free);
  }

  virtual bool isCalling() {
    volatile std::atomic_int st = status;
    return ((st == _call_in) || (st == _call_out2)|| (st == _call_out1)) & (id >= -1);
  }

  virtual bool isCallOff(){
    return (status == _call_off);
  }
protected:
  bool isCalling_in() {
    return (getStatus() == _call_in) & (id >= -1);
  }

  bool isCalling_out(int _step) {
    if((id < -1))
      return false;
    volatile std::atomic_int st = status;
    if(1 == _step)return ((st == _call_out1));
    if(2 == _step)return ((st == _call_out2));
    return ((st == _call_out2)|| (st == _call_out1));
  }

protected:
  ///Входящий RTP-порт
  IP_PORT rtp_in;
  ///Исходящий RTP-порт
  IP_PORT rtp_out;
  ///адрес источника запроса на соединение
  IP rtp_gw;
  ///mutex полей доступа к rtp in|out
  boost::mutex lock_rtp;
  ///condvar локирования при setRTP_dst
  boost::condition_variable cond_rtpDest;
public:
  /** \brief Установить RTP порт назначения исходящего потока.
   * vms передаёт данные сигнали потокам - обработчикам соединения.
   * @param _d_rtp - RTP-порт для исходящих соединений
   * @param _d_ip - IP для исходящих соединений   */
  virtual void putWrSenderTable(int _d_rtp, int _d_ip);

protected:
  ///счётчик исх RTP-пакетов, заполняет соотв поле внутри RTP-пакета
  int rtp_out_count;
  ///timeStamp исх RTP-пакетов, заполняет соотв поле внутри RTP-пакета
  int rtp_out_timeStamp;
//  ///сокет для выходного RTP-потока
//  int socket_rtp_out;
//  ///поля сокета
//  sockaddr_in addr_rtp_out;
  //поток выводной
  IWavStream *play_stream;
  //mutex работы с выходным потоком
  std::atomic<int> lock_play_stream;
  //
  IWavStream *defPlay;
public:
  //задать проигрывателя,
  virtual int setPlayer(IWavStream *_wav);
  //остановить проигрывание
  virtual void stopPlaying(bool _release = true);
  //
  virtual int isPlaying();
protected:
  ///число принятых rtp
  int rtp_in_count = 0;
  //поток входной
  IWavStream *listen_stream[_lastVariant];//[ICallManager::_lastVariant];
  //mutex работы с входным потоком
  //boost::mutex lock_listen_stream;
  std::atomic<int> lock_listen_stream;
  //
  IWavStream * defListen;
  ///стартовать слушателя RTP на ios
  void setRTPListener();
  ///принять пакет, обработчик события от ios
  void onRTPListen(const boost::system::error_code& error, std::size_t bytes_transferred);
public:
  //
  virtual int setListener(IWavStream *_wav, EListen_Variant _variant = _variantDef);
  //
  virtual void stopListen(EListen_Variant _variant = _lastVariant, bool _release = true);
  //
  virtual int isListening(EListen_Variant _variant = _lastVariant);
protected:
  ///сокет для входного RTP-потока
  boost::asio::ip::udp::socket socket_rtp_in;
  boost::asio::ip::udp::endpoint ep_rtp_in;
  boost::array<uint8_t, CCallService::IPDATA_BUFLEN> buf_rtp_in;
  std::list<CPacket_RTP> rtp_packet_local_buffer;
  //и выходного
  boost::asio::ip::udp::socket socket_rtp_out;

//  boost::asio::ip::udp::endpoint remote_endpoint;
//  int socket_rtp_in;
//  ///поля сокета
//  sockaddr_in addr_rtp_in;
protected:
  ///команды объекту
  enum ECmd {
    _ok = 0,
    _release, _connect, /* _SIP_INFO*/
  };
  ///mailbox

  struct SMailBox {
    ///condvar уведомления в mailbox
    ECmd cmd;
    int data0;
    SMailBox(ECmd _cmd, int _data0):cmd(_cmd),data0(_data0){  }
  };
  boost::condition_variable cond;
  std::queue<SMailBox> queue_ofMails;

#define MB_SCOPE_EXIT     mailBox.cmd = _ok; mailBox.cond.notify_one();

protected:
  ///транслирование звука в сокет до конца звонка
  void continueCall();

  CTimer_balanced b_timer;
  ///
  int playNTimes(int _times = infinity);
  ///
  int playDTMF(const tVoiceMsg& _source);

  /** \brief Обработка перед установкой разрешения
   * @return номер сценария или ошибки
   */
  int beginCall(const CString& _number, const CString& _add, const IP& _s_ip);

  ///Обработка после окончания вызова
  void endCall();

//protected:
//  virtual void onSIP_INFO(int _data){ };
public:
  virtual void close();

  //исходящие соединения
  virtual int call(const CString& _number, const CString& _add, int _timeout,
      IP _s_ip);

protected:
  boost::thread call_out_thread;
  //boost::barrier* call_out_barrier;

//  static void call_out_static(CCallManager*/*, CString, CString, IP, const uint32_t _timeout*/);

  void call_out(/*const CString& _number, const CString& _add, const IP& _s_ip,
      const uint32_t _timeout*/);

  void callSetup(/*const CString& _number, const CString& _add, const IP& _s_ip*/);

protected:
  //pthread_t call_in_thread;
  boost::thread call_in_thread;

//  static void call_in_static(CCallManager*);

  void call_in();

  void callAllow();

protected:
  bool forReserved;
  void markForReserved() {
    forReserved = true;
    LOG1(TSTL1, "call(%d): markForReserved\n", getId());
  }
public:
  virtual ICallManager* release();

protected:
  struct SParams{
    int packetSize;
    int jitter;
    int quant;
  };
  SParams params;
public:
  ///задать параметры вещания
  virtual int setParams(int param, int arg);
protected:
  int setParams_in(int param, int arg);
public:
  //группа методов ожидания к-либо события
  ///пауза, ms
  virtual int waitfor(uint32_t _ms);
  ///ждать входящего вызова, не более ms
  virtual int waitforCall(uint32_t _ms = infinity);
  ///ждать входящего вызова от источника с номеров source не более ms!
  //virtual int waitforCall(uint32_t _ms, const CString& _income_number);
  ///ждать завершения звонка, не более ms
  virtual int waitforClose(uint32_t _ms = infinity, bool _andReserv = false);
//  ///ждать окончания выводного потока, если таковой есть, не более ms
//  virtual int waitforPlaying(uint32_t _ms = infinity);
//  ///ждать окончания входного потока, если таковой есть, не более ms
//  virtual int waitforListening(uint32_t _ms = infinity);

protected:
  //virtual int waitforClose_nolock(uint32_t _ms);
  int sendDTMFs(const CString& _dtmfNumbers);
protected:
  CString lastMsg;

public:
  /**
   * @return
   */
  virtual const CString& getLastMsg() {
    return lastMsg;
  }
  ;
public:
  //реализация ICallManager_SIP
  virtual void putSetup_res(const int _id, const CString& _number,
      const CString& _add, const IP& _s_ip);
  /** \brief
   * @param _id
   * @param _number
   * @param _add
   * @param _s_ip
   */
  virtual void putInvite(const int _id, const CString& _number,
      const CString& _add, const IP& _s_ip);

  /** \brief Подать SIP Info сигнал
   * Принять и обработать SIP INFO сигнал
   * @param _data1 данные
   */
  virtual void putSIP_Info(int _data1, int _data2, int _data3);

  /** Отбить входящие соединение
   */
  virtual void putRelease(const int _id);

  /** Принять входщие соединение
   */
  virtual void putConnect(const int _id);

protected:
  ///конструктор
  CCallManager(CCallService& _vms, ICallManager::EStatus _init);
  ///деструктор
  virtual ~CCallManager();
public:
  //[-,0-9]
  ///выделить из _number прямой номер и dtmf-номер
  static CString parseCallNumber(CString& _dtmfNumber);

  friend class CCallService;
};

}

#endif /* CSERVICE_CALL_IN_H_ */
