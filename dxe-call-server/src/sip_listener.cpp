/** sip_listener.cpp
 * Слушатель SIP-сообщений
 *  Created on: 23.03.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <string>
#include <unistd.h>

#include <boost/asio.hpp>
using boost::asio::ip::udp;

#include "define.h"

#include "vms_signals.h"
#include "CdxeCallService.h"

//extern SDL::CSDL_system vms;
namespace DXE {


//#define NPACK 10

void CCallService::sendToSIP( uint32_t _ip, const uint8_t* buf, const uint32_t len){
  try{
    if(net.sipPort_in != net.sipPort_out){
      //отладочный режим запускаемся на одном ip, надо вернуть обман SDL
      _ip = realIP;
    }
    udp::endpoint ep_sip_out = {boost::asio::ip::address_v4(_ip), net.sipPort_out};
      //= {boost::asio::ip::address_v4().from_string("192.168.233.2"), net.sipPort_out};
    sip_out_socket.send_to(boost::asio::buffer(buf, len), ep_sip_out);
  }catch(P_EXC e){
    LOG1(ERRR, "sendToSIP: %s\n", e.what());
  }
}

void CCallService::setSIPListener(){
  try{
    sip_in_socket.async_receive_from(boost::asio::buffer(sip_in_buffer), ep_sip_in,
        boost::bind(&CCallService::onSIPListen, this,
          boost::asio::placeholders::error,
          boost::asio::placeholders::bytes_transferred));
  }catch(P_EXC e){
    LOG1(ERRR, "setSIPListener: %s\n", e.what());
//    exit(1);
  }
}


//Метод-слушатель SIP-сообщений
void CCallService::onSIPListen(const boost::system::error_code& error, std::size_t bytes_transferred) {
  //структуры сокетов, моего и отправителя
//  struct sockaddr_in sip_me, sip_other;
//
//  int s =0, slen = sizeof(sip_other);
//  uint8_t buf[IPDATA_BUFLEN];
//  LOG(TSTL1, "SIP_listener: thread started\n");
  if(false == (! error)){
    LOG2(ERRR, "onSIPListen: error %d:%s\n", error.value(), error.message().c_str());
    return ;
  }
  try {
    //создать слушающий сокет
//    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
//      throw E(WSAGetLastError(), ":SIP_listener::socket");
//    //заполнить поля сокета и связать их с сокетом
//    memset((char *) &sip_me, 0, sizeof(sip_me));
//    sip_me.sin_family = AF_INET;
//    sip_me.sin_port = htons(net.sipPort_in);
//    sip_me.sin_addr.s_addr = htonl(INADDR_ANY);
//
//    if (::bind(s, (sockaddr *) &sip_me, sizeof(sip_me)) == -1)
//      throw E(WSAGetLastError(), ":SIP_listener::bind");
//
//    //ждём остальных @see CCallService::start(std::string _opts)
//    start_barier->wait();
//    int len = 0;
//    while (1) {
//      //принять пакет
//      if (-1 ==
//         (len = recvfrom(s, (char*) buf, IPDATA_BUFLEN, 0, (sockaddr *) &sip_other,
//                          &slen))){
//        int res = WSAGetLastError();
//        if(10004 == res){
//          closesocket(s);
//          //thread cancel
//          goto L_exit;
//        }
//        throw E(res, "SIP_listener:recvfrom");
//      }
//      printf("Received packet from %s:%d\nData:\n%s\n", inet_ntoa(sip_other.sin_addr),
//          ntohs(sip_other.sin_port), buf);
//      if(! service_started)
//        continue;//игнорить все внешние пакеты, пока не стартаунли полнотью

      //IP- брикета, что мне прислал пакет
      //создаём и отправляем в SDL-систему

      int sIP(ep_sip_in.address().to_v4().to_ulong());
      if(net.sipPort_in != net.sipPort_out){
        //отладочный режим запускаемся на одном ip, надо вернуть обман SDL
        sIP = gwIP;
      }
      CVMS_Signal_UDPDI_long udp_packet(sIP, 5060, 5060, bytes_transferred, sip_in_buffer.elems);
      {
//        boost::mutex::scoped_lock lck(getVMSLock());
        sendToSDL(&udp_packet);
        LOGTIME(DEVL1);
      }
//    }
//L_exit: ;
  } catch (P_EXC e) {
    //в случае искл ситуации логируем и выходим
    E_CATCH(e);
  }

  setSIPListener();
  //CCallService::Exit(1, this);
  return ;
}

}
