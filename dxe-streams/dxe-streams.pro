#-------------------------------------------------
#
# Project created by QtCreator 2012-03-02T09:38:58
#
#-------------------------------------------------

QT       += multimedia
QT       -= gui

TARGET = dxe-streams
TEMPLATE = lib

QMAKE_LIBDIR += ../dxe/install

QMAKE_CXXFLAGS += -std=c++11

DESTDIR = ../dxe/install

LIBS     += -ldxe-utils 
#LIBS     += -static dxe-utils.a

INCLUDEPATH += include ../dxe-utils

DEFINES += DXEUTILS_LIBRARY

SOURCES += \
    src/dtmfgen.cpp \
    src/wavStreams.cpp \
    src/wavstream_gen.cpp \
    src/wavstream_file.cpp \
    src/wavstream_dev.cpp \
    src/wavstream_echo.cpp \
    src/recievethread.cpp \
    #src/audiooutputstream_thread.cpp \
    #src/audioinputstream_thread.cpp


HEADERS += include/wavStreams.h \
    include/dtmfgen.h \
    src/wavstream_gen.h \
    src/wavstream_file.h \
    src/wavstream_echo.h \
    src/wavstream_dev.h \
    src/recievethread.h \
    #src/audiooutputstream.h \
    #src/audioinputstream.h
