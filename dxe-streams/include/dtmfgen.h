#ifndef WAVSTREAM_GEN_H_
#define WAVSTREAM_GEN_H_

#include <math.h>
#include <iostream>
#include <string>

#include "utils/netutils.h"

#define PI 3.14159265
#define rtp_size  CPacket_RTP::length

using namespace std;

/**Поток генерирующий RTP-поток в виде синусоиды
 * Генерирует синус, с заданными характеристиками.
 * Пример задания хар-к:
 * <item type="sin" freq="440" ampl="-6">
 * Будет сгенерирован синус с частотой 440Гц, ампилтудой -6дБ */
class CDTMFGenerator
{
  public:
    //480 байт
  tVoiceMsg get(const std::string& _digits, int packetSize);

  public:
    CDTMFGenerator(int _len, int _pause, double _amplitude);

    CDTMFGenerator():CDTMFGenerator(60, 60, -5){  };

  private:
    static constexpr double samplingRate = 8000;
    double amplitude;
    unsigned long sampleCount;
    unsigned long len;
    unsigned long pause;
};

#endif // AUDIOINPUTSTREAM_H
