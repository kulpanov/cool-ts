#include <string>

#include "utils/netutils.h"
#include "dtmfgen.h"

extern uint8_t l2a(int16_t _l);

//из make функции создать
CDTMFGenerator::CDTMFGenerator(int _len, int _pause, double _amplitude)
{
  if(_len > 1000 || _len < 40) _len = 60;
  if(_pause > 1000 || _pause < 40) _pause = 60;
  if(_amplitude > 0 || _amplitude < -40) _amplitude = -5;

  len   = (_len * samplingRate) / 1000;//samples
  pause = (_pause * samplingRate) / 1000;
  sampleCount = 0;

  amplitude = pow(10, _amplitude / 20) * 4031 / 2;//dB
}


//480 байт
tVoiceMsg CDTMFGenerator::get(const std::string& _digits, int packetSize){
  tVoiceMsg q;
  double frequency_low = 440;
  double frequency_high = 440;
  //шаг1 - генерация сигнала
  int i = 0;
  uint8_t data[CPacket_RTP::length];
  if(packetSize>CPacket_RTP::length) packetSize = CPacket_RTP::length;
  double accdec_factor = 0;

  auto digit = _digits.begin();
  while(digit != _digits.end()){
    //
    switch(*digit){
    case '0':
      frequency_low = 941;      frequency_high = 1336;
    break;
    case '1':
      frequency_low = 697;      frequency_high = 1209;
    break;
    case '2':
      frequency_low = 697;      frequency_high = 1336;
    break;
    case '3':
      frequency_low = 697;      frequency_high = 1477;
    break;
    case '4':
      frequency_low = 770;      frequency_high = 1209;
    break;
    case '5':
      frequency_low = 770;      frequency_high = 1336;
    break;
    case '6':
      frequency_low = 770;      frequency_high = 1477;
    break;
    case '7':
      frequency_low = 852;      frequency_high = 1209;
    break;
    case '8':
      frequency_low = 852;      frequency_high = 1336;
    break;
    case '9':
      frequency_low = 852;      frequency_high = 1477;
    break;
    case '-':
      //frequency_low = 0;frequency_high = 0;
      memset(&data, l2a(0), packetSize);
      for(int i=0; i<1000/60; i++){//pause 1sec
        q.push_back(new CPacket_RTP(8, 0, 0, data, packetSize));//60ms
      }
      digit ++;
      continue;
    break;
    }
    double omega_low  = 2 * PI * (frequency_low / samplingRate);
    double omega_high = 2 * PI * (frequency_high / samplingRate);
    //
    while(sampleCount<len){
      double value = sin(omega_low * sampleCount) * amplitude * accdec_factor;
      value += sin(omega_high * sampleCount) * (amplitude+3) * accdec_factor;

      data[i] = l2a((int16_t)value);
      if(i == packetSize-1){
        q.push_back(new CPacket_RTP(8, 0, 0, data, packetSize));
        i = 0;
      }else
        i ++;
      if(sampleCount<40)
        accdec_factor += 0.025; //1/40
      if(sampleCount>len - 40)
        accdec_factor -= 0.025;

      sampleCount++;

    }
    while(sampleCount<len+pause){
      data[i] = l2a(0);
      if(i == packetSize-1){
        q.push_back(new CPacket_RTP(8, 0, 0, data, packetSize));
        i = 0;
      }else
        i ++;
      sampleCount++;
    }
    sampleCount = 0;
    digit ++;
  }
  if(i != 0){
    //дозаполнить тишиной до конца пакета
    memset(&data[i], l2a(0), packetSize - i);
    q.push_back(new CPacket_RTP(8, 0, 0, data, packetSize));
  }
  return q;
}

