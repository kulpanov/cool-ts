/*
 * wavstream_dev.h
 *
 *  Created on: 12.03.2012
 *      Author: tsvet
 */

#ifndef WAVSTREAM_DEV_H_
#define WAVSTREAM_DEV_H_

#include <QObject>
#include <QAudioOutput>
#include <QAudioInput>
#include <QAudioFormat>
#include <QIODevice>
#include <QAudioDeviceInfo>
#include <QDebug>
#include <QFile>
#include <QTimer>
#include <QBuffer>

#include <math.h>
#include <iostream>
#include <atomic>

#include "wavStreams.h"
#include "recievethread.h"

using namespace std;

#define rtp_size  CPacket_RTP::length

/*
//поток
class CWavStream_din : public QObject, public IWavStream
{
  Q_OBJECT

  private:
    QAudioInput* audio;
    QByteArray data;
    int indx;
    QBuffer* buffer;

  public:
    //перейти в начало
    virtual void reset();
    //остановить
    virtual void close();
    //480 байт
    virtual CPacket_RTP* read();
    //проверка конца потока
    virtual bool isEndOfStream();
    //удаление буферов
    virtual IWavStream* release();

  public slots:
    void dataReady();

  public:
    CWavStream_din();
};

class QAIProcess : public QThread {
  Q_OBJECT
  protected:
    QMutex* mutex;
  private:
    QAudioInput* audio;
    QByteArray data;
    int indx;
    QBuffer* buffer;

  public:
    virtual void run();
    //перейти в начало
    virtual void reset();
    //остановить
    virtual void close();
    //480 байт
    virtual CPacket_RTP* read();
    //проверка конца потока
    virtual bool isEndOfStream();
    //удаление буферов
    virtual void release();
  public slots:
    void dataReady();

  public:
    QAIProcess():audio(NULL), buffer(NULL){
      moveToThread(this);
      mutex = new QMutex();
    };
};

//поток
class CWavStream_din2 : public QObject, public IWavStream
{
  Q_OBJECT
private:
  QAIProcess* in_process;
//  private:
//    QAudioInput* audio;
//    QByteArray data;
//    int indx;
//    QBuffer* buffer;
//
  public:
    //перейти в начало
    virtual void reset();
    //остановить
    virtual void close();
    //480 байт
    virtual CPacket_RTP* read();
    //проверка конца потока
    virtual bool isEndOfStream();
    //удаление буферов
    virtual IWavStream* release();
//
//  public slots:
//    void dataReady();

  public:
    CWavStream_din2();
};


*Класс выводящий RTP-поток во звуковый вывод
 * Пример задания хар-к:
 * <item type="sound_out" >
 * Выведит поток в звуковой вывод
class CWavStream_dout : public QObject, public IWavStream
{
  Q_OBJECT
  public slots:
    void slot_writeMoreData();
  private:
    QRecieveThread* receiveThread;
    QAudioOutput* audio;
    QIODevice* pAudioIOBuffer;
    QTimer* timerUpdate;

    float samplingRate;

    char* data;
    short* buffer;
    int sizeNolBuffer;

  public:
    //перейти в начало
    virtual void reset();
    //остановить
    virtual void close();
    //запись 480
    virtual void write(CPacket_RTP* _packet);
    //удаление буферов
    virtual IWavStream* release();


  public:
    void start();

  public:
    CWavStream_dout();
};

//поток
class CWavStream_dout2 : public QObject, public IWavStream
{
  Q_OBJECT

  private:
    QAudioOutput* audio;
    QIODevice* pAudioIOBuffer;
    int bufCount;

  public:
    //перейти в начало
    virtual void reset();
    //остановить
    virtual void close();
    //запись 480
    virtual void write(CPacket_RTP* _packet);
    //удаление буферов
    virtual IWavStream* release();


  public:
    void start();

  public:
    CWavStream_dout2();
};
*/


class QAOProcess:public QObject {
  Q_OBJECT
  public slots:
    void addData(const uint8_t* _data, const int len);
    void releaseAO();

  private:
    QThread* thread;
    QAudioOutput* audio;
    QIODevice* pAudioIOBuffer;
    QByteArray value;
    volatile int writeCount;
    volatile std::atomic_int key;
  protected:
  signals:
        void finished();
  public:
    static QObject* getInstance();
  protected:
    QAOProcess();

    virtual ~QAOProcess();
};


//поток
class CWavStream_dout3 : public QObject, public IWavStream
{
  Q_OBJECT
  QObject* ao;
  //tVoiceMsg dup_audio;
  private:
    //QAOProcess* out_process;
  signals:
    void addData(const uint8_t* _data, const int len);
    void releaseAO();
  public:
//    //перейти в начало
//    virtual void reset();
//    //остановить
//    virtual void close();
    //запись 480
    virtual void write(CPacket_RTP* _packet);
    //удаление буферов
    virtual IWavStream* release();
  public:
    CWavStream_dout3();
};

#endif /* WAVSTREAM_DEV_H_ */
