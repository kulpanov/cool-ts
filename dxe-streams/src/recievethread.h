#ifndef RECIEVETHREAD_H
#define RECIEVETHREAD_H
#include <QThread>
#include <QMutex>
#include <QWaitCondition>

#include <QFile>
#include <QVector>

#include "wavStreams.h"

class QRecieveThread : public QThread{
  Q_OBJECT ;

  protected:
    QByteArray data;
    QMutex* mutex;
  public :
    void read(short*& _data, int& _countBytes);
    void write(char* _data, int _countBytes);
    void release();
    bool isBufferEmpty();
  public:
    QRecieveThread();
    virtual ~QRecieveThread(){}
};

#endif // RECIEVETHREAD_H
