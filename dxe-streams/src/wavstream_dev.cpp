/*
 * wavstream_dev.cpp
 *
 *  Created on: 12.03.2012
 *      Author: tsvet
 */
#include "wavstream_dev.h"


// //из make функции создать
//CWavStream_din::CWavStream_din()
//{
//
//  // Set up the format, eg.
//  QAudioFormat format;
//  format.setFrequency(8000);
//  format.setChannels(1);
//  format.setSampleSize(16);
//  format.setCodec("audio/pcm");
//  format.setByteOrder(QAudioFormat::LittleEndian);
//  format.setSampleType(QAudioFormat::UnSignedInt);
//
//  QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
//  if (!info.isFormatSupported(format))
//  {
//    format = info.nearestFormat(format);
//  }
//  audio = new QAudioInput(format, this);
//  buffer = new QBuffer();
//  buffer->open( QIODevice::ReadWrite | QIODevice::Truncate );
//  audio = new QAudioInput(format, this);
//
//  indx = 0;
//  audio->start(buffer);
//  connect(buffer, SIGNAL(readyRead()), this, SLOT(dataReady()));
//}
//
// //перейти в начало
//void CWavStream_din::reset(){
//  buffer->reset();
//};
//
// //480 байт
//CPacket_RTP* CWavStream_din::read(){
//  //переходим в начало
//  QByteArray tmp = data.left(rtp_size * 2);
//  data.remove(0,tmp.size());
//  tmp = ConvertToALaw(tmp);
//  return new CPacket_RTP(8, 0, 0, (uint8_t*)tmp.data(), tmp.size());
//};
//
//
// //проверка конца потока
//bool CWavStream_din::isEndOfStream(){
//  return (data.size() == 0);
//};
//
// //удаление всего
//IWavStream* CWavStream_din::release(){
//  close();
//  delete buffer;
//  buffer = NULL;
//  delete audio;
//  audio = NULL;
//  indx = 0;
//  return IWavStream::release();
//};
//
//void CWavStream_din::close(){
// //просто останавливаем все
// if(audio != NULL) audio->stop();
// if(buffer != NULL){
//   buffer->close();
//   disconnect(buffer, SIGNAL(readyRead()), this, SLOT(dataReady()));
// }
//};
//
//void CWavStream_din::dataReady() {
//  if(indx < buffer->size()){
//    buffer->seek(indx);
//    QByteArray tmp = buffer->read(buffer->size());
//    indx += tmp.size();
//    data.append(tmp);
//  };
//};
//
//
// //перейти в начало
//void QAIProcess::reset(){
//  buffer->reset();
//};
//
// //удаление всего
//void QAIProcess::release(){
//  close();
//  delete mutex;
//  delete buffer;
//  buffer = NULL;
//  delete audio;
//  audio = NULL;
//  indx = 0;
//};
//
// //остановить
//void QAIProcess::close(){
//   //просто останавливаем все
//   if(audio != NULL) audio->stop();
//   if(buffer != NULL){
//     buffer->close();
//     disconnect(buffer, SIGNAL(readyRead()), this, SLOT(dataReady()));
//   }
//};
// //480 байт
//CPacket_RTP* QAIProcess::read(){
//  mutex->lock();
//  //переходим в начало
//  QByteArray tmp = data.left(rtp_size * 2);
//  data.remove(0,tmp.size());
//  mutex->unlock();
//  tmp = ConvertToALaw(tmp);
//  return new CPacket_RTP(8, 0, 0, (uint8_t*)tmp.data(), tmp.size());
//};
// //проверка конца потока
//bool QAIProcess::isEndOfStream(){
//  return (data.size() == 0);
//};
//void QAIProcess::run(){
//  //moveToThread(this);
//  // Set up the format, eg.
//  QAudioFormat format;
//  format.setFrequency(8000);
//  format.setChannels(1);
//  format.setSampleSize(16);
//  format.setCodec("audio/pcm");
//  format.setByteOrder(QAudioFormat::LittleEndian);
//  format.setSampleType(QAudioFormat::UnSignedInt);
//
//  QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
//  if (!info.isFormatSupported(format))
//  {
//    format = info.nearestFormat(format);
//  }
//  audio = new QAudioInput(format, this);
//  buffer = new QBuffer();
//  buffer->open( QIODevice::ReadWrite | QIODevice::Truncate );
//  audio = new QAudioInput(format, this);
//
//  indx = 0;
//  audio->start(buffer);
//  connect(buffer, SIGNAL(readyRead()), this, SLOT(dataReady()));
//  exec();
//};
//
//void QAIProcess::dataReady() {
//  if(indx < buffer->size()){
//    mutex->lock();
//    buffer->seek(indx);
//    QByteArray tmp = buffer->read(buffer->size());
//    indx += tmp.size();
//    data.append(tmp);
//    mutex->unlock();
//  };
//};
//
// //перейти в начало
//void CWavStream_din2::reset(){
//  in_process->reset();
//};
// //остановить
//void CWavStream_din2::close(){
//  in_process->close();
//};
// //480 байт
//CPacket_RTP* CWavStream_din2::read(){
//  return in_process->read();
//};
// //проверка конца потока
//bool CWavStream_din2::isEndOfStream(){
//  return in_process->isEndOfStream();
//};
// //удаление буферов
//IWavStream* CWavStream_din2::release(){
//  in_process->release();
//  delete in_process;
//  return IWavStream::release();
//};
//
//CWavStream_din2::CWavStream_din2(){
//  in_process = new QAIProcess();
//  in_process->start();
//};
//
//
//
//
// //из make функции создать
//CWavStream_dout::CWavStream_dout()
//{
//  audio = NULL;
//  sizeNolBuffer = 0;
//  receiveThread = new QRecieveThread();
//  start();
//}
//
// //перейти в начало
//void CWavStream_dout::reset(){
//  //остановить
//  close();
//  //запустить заново
//  start();
//};
//
// //запись 480
//void CWavStream_dout::write(CPacket_RTP* _packet){
//  if(!receiveThread->isRunning())
//    start();
//  if(timerUpdate->isActive()){
//    slot_writeMoreData();
//  }
//  receiveThread->write(
//      (char*)_packet->getDataOfPayload(),_packet->getLengthOfPayload());
//};
//
//
// //удаление буферов
//IWavStream* CWavStream_dout::release(){
//  receiveThread->release();
//  close();
//  if(audio != NULL) delete audio;
//  return IWavStream::release();
//};
//
//
//void CWavStream_dout::start(){
//  QAudioFormat format;
//  samplingRate = 8000;
//  // set up the format you want, eg.
//  format.setFrequency(samplingRate);
//  format.setChannels(1);
//  format.setSampleSize(16);
//  format.setCodec("audio/pcm");
//  format.setByteOrder(QAudioFormat::LittleEndian);
//  format.setSampleType(QAudioFormat::SignedInt);
//
//  QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
//  if (!info.isFormatSupported(format)) {
//    format = info.nearestFormat(format);
//  }
//
//  if(audio != NULL)
//    delete audio;
//
//  audio = new QAudioOutput(format, this);
//  audio->setBufferSize(5000);//in bytes
//
//  timerUpdate = new QTimer(this);
//  QObject::connect(timerUpdate, SIGNAL(timeout()), this, SLOT(slot_writeMoreData()));
//  //timerUpdate->setInterval(1);
//  timerUpdate->start(1);
//  receiveThread->start();
//
//  pAudioIOBuffer = audio->start();
//
//  //генерация буфера из файла
//  unsigned int periodSize = audio->periodSize();
//  sizeNolBuffer = periodSize;
//  buffer = (short*)calloc(round(sizeNolBuffer / 2), sizeof(short));
//
//
//  slot_writeMoreData();
//
//}
//
//void CWavStream_dout::slot_writeMoreData()
//{
//  int nbBytes = audio->bytesFree();
//  if (nbBytes > 0) {
//    if (sizeNolBuffer < nbBytes) {
//      delete[] buffer;
//      sizeNolBuffer = (int)(nbBytes / 2);
//      buffer = (short*)calloc(sizeNolBuffer, sizeof(short));
//    }
//    receiveThread->read(buffer, nbBytes);
//    pAudioIOBuffer->write((const char*) buffer, nbBytes);
// }
//}
//
//void CWavStream_dout::close(){
//   QObject::disconnect(timerUpdate, SIGNAL(timeout()), this, SLOT(slot_writeMoreData()));
//   audio->stop();
//   if(buffer != NULL)
//    delete[] buffer;
//   receiveThread->exit(0);
//};
//
// //из make функции создать
//CWavStream_dout2::CWavStream_dout2()
//{
//  audio = NULL;
//  //QObject::moveToThread(this->thread());
//  start();
//}
//
// //перейти в начало
//void CWavStream_dout2::reset(){
//  //остановить
//  close();
//  //запустить заново
//  start();
//};
//
// //запись 480
//void CWavStream_dout2::write(CPacket_RTP* _packet){
//  char* data = (char*)_packet->getDataOfPayload();
//  QByteArray value;
//  for(int i = 0; i < _packet->getLengthOfPayload(); i++){
//    int16_t l = a2l(data[i]);
//    value.append((char*)&(l),2);
//  };
//  pAudioIOBuffer->write((const char*) value.data(), value.size());
//};
//
//
// //удаление буферов
//IWavStream* CWavStream_dout2::release(){
//  close();
//  if(audio != NULL){
//    delete audio;
//    audio = NULL;
//  }
//  return IWavStream::release();
//};
//
//
//void CWavStream_dout2::start(){
//  QAudioFormat format;
//  samplingRate = 8000;
//  // set up the format you want, eg.
//  format.setFrequency(samplingRate);
//  format.setChannels(1);
//  format.setSampleSize(16);
//  format.setCodec("audio/pcm");
//  format.setByteOrder(QAudioFormat::LittleEndian);
//  format.setSampleType(QAudioFormat::SignedInt);
//
//  QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
//  if (!info.isFormatSupported(format)) {
//    format = info.nearestFormat(format);
//  }
//
//  if(audio != NULL)
//    delete audio;
//
//  audio = new QAudioOutput(format, this);
//  audio->setBufferSize(5000);//in bytes
//
//  pAudioIOBuffer = audio->start();
//
//}
//
//void CWavStream_dout2::close(){
//   if(audio != NULL){
//     audio->stop();
//   };
//};

////перейти в начало
//void QAOProcess::reset(){
//  //остановить
//  close();
//  //запустить заново
//  start();
//}


QObject* QAOProcess::getInstance(){
  QAOProcess* ao = new QAOProcess();

  return ao;
}


QAOProcess::QAOProcess():
  audio(NULL), pAudioIOBuffer(NULL), writeCount(0){
  key = 0;

  QAudioFormat format;
  float samplingRate = 8000;
  // set up the format you want, eg.
  //format.setFrequency(samplingRate);
  format.setSampleRate(samplingRate);
  format.setChannelCount(1);
  format.setSampleSize(16);
  format.setCodec("audio/pcm");//возможно заменить кодек?
  format.setByteOrder(QAudioFormat::LittleEndian);
  format.setSampleType(QAudioFormat::SignedInt);


 auto listOfDevInfo = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
//  for_each(listOfDevInfo.begin(), listOfDevInfo.end(), [](QAudioDeviceInfo& e)
//      {
//         qDebug() << (e.deviceName().toLocal8Bit());
//      });

  QAudioDeviceInfo& info = listOfDevInfo[0];//QAudioDeviceInfo::defaultOutputDevice();
  //if (!info.isFormatSupported(format)) format = info.nearestFormat(format);
  //qDebug() << info.deviceName();
  //exit(0);
  audio = new QAudioOutput(info, format, this);
  audio->setBufferSize(1000000);//in bytes

  pAudioIOBuffer = audio->start();
  if(audio->error() != 0){
    LOG1(ERRR, "Не могу открыть аудиовывод, ошибка %d\n", audio->error());
  }
  //qDebug() << 2;
  //  audio->setNotifyInterval(50);
  //  QObject::connect(audio, SIGNAL(notify()), this, SLOT(slot_writeMoreData()));
  writeCount = 0;

  thread = new QThread();
  moveToThread(thread);
  //connect(thread, SIGNAL(started()), this, SLOT(process()));
  connect(this, SIGNAL(finished()), thread, SLOT(quit()));
  connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
  thread->start();
}

//удаление всего
QAOProcess::~QAOProcess(){
  //LOG(INFO, "QAOProcess::~QAOProcess, step 1 \n");
//захватить
  int i=0;  while(! key.compare_exchange_strong(i, 1)) Sleep(0);
  //LOG(INFO, "QAOProcess::~QAOProcess, step 2 \n");
  delete audio;
  //LOG(INFO, "QAOProcess::~QAOProcess, step4\n");
}

//запись 480
void QAOProcess::addData(const uint8_t* _data, const int _len){

  for(int i = 0; i < _len; i++){
    int16_t pcm = a2l(_data[i]);
    value.append(( const char*) &pcm, sizeof(int16_t));
  }
//  value.append(( const char*) &_data, _len);
  if(++writeCount<2) return;
  if(NULL == pAudioIOBuffer) return;
  int i = 0; if(! key.compare_exchange_strong(i, 1)) return;
  pAudioIOBuffer->write(value.data(), value.size());
  key = 0;//освободить

  value.clear();
}

void QAOProcess::releaseAO(){
  LOG(INFO, "QAOProcess::releaseAO\n");
  audio->stop();
  audio->blockSignals(true);
  emit finished();
}

//void QAOProcess::slot_writeMoreData(){
//  int nbBytes = audio->bytesFree();
//
//  if(nbBytes==0) return;
//
//  if(nbBytes < 1111){
//
//  }
//}
//
//void QAOProcess::run(){
//  moveToThread(this);
//
//  QAudioFormat format;
//  float samplingRate = 8000;
//  // set up the format you want, eg.
//  format.setFrequency(samplingRate);
//  format.setChannels(1);
//  format.setSampleSize(16);
//  format.setCodec("audio/pcm");
//  format.setByteOrder(QAudioFormat::LittleEndian);
//  format.setSampleType(QAudioFormat::SignedInt);
//
//  QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
//  if (!info.isFormatSupported(format)) {
//    format = info.nearestFormat(format);
//  }
//
//  if(audio != NULL) delete audio;
//
//  audio = new QAudioOutput(format, this);
//  audio->setBufferSize(1000000);//in bytes
// //  audio->setNotifyInterval(50);
// //  QObject::connect(audio, SIGNAL(notify()), this, SLOT(slot_writeMoreData()));
//  writeCount = 0;
//  pAudioIOBuffer = audio->start();
// //  audio->suspend();
// //  int periodSize = audio->periodSize();
//
//  QThread::setTerminationEnabled(true);
//  exec();
//}
//
// //перейти в начало
//void CWavStream_dout3::reset(){
//  //out_process->reset();
//}
//
// //остановить
//void CWavStream_dout3::close(){
//  //out_process->close();
//}

//запись 480
void CWavStream_dout3::write(CPacket_RTP* _packet){
  //dup_audio.push_back(_packet);
  emit addData(_packet->getDataOfPayload(), _packet->getLengthOfPayload());
}

//удаление буферов
IWavStream* CWavStream_dout3::release(){
  //tVoiceMsg::Save_qmsg_toWAV(dup_audio, "wavs/last_listened.wav");
  emit releaseAO();

  disconnect(this, SIGNAL(addData(const uint8_t*, const int))
      , ao , SLOT(addData(const uint8_t*, const int)));

  disconnect(this, SIGNAL(releaseAO()), ao , SLOT(releaseAO()));

  return IWavStream::release();
}

//

CWavStream_dout3::CWavStream_dout3(){
  ao = QAOProcess::getInstance();
  connect(this, SIGNAL(addData(const uint8_t*, const int))
      , ao , SLOT(addData(const uint8_t*, const int))
      , Qt::ConnectionType::QueuedConnection);

  connect(this, SIGNAL(releaseAO())
      , ao , SLOT(releaseAO())
      , Qt::ConnectionType::QueuedConnection);

}
