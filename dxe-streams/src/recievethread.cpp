#include <stdio.h>
#include "recievethread.h"

void QRecieveThread::write(char* _data, int _countBytes){
  //заполняем данные
  mutex->lock();
  for(int i = 0; i < _countBytes; i++){
    short lineCode = a2l(_data[i]);
    data.append((char*)&lineCode,2);
    //testFile->write((char*)&lineCode,2);
  };
  mutex->unlock();
};

void QRecieveThread::read(short*& _data, int& _countBytes){
  if(data.size() == 0){
    _countBytes = 0;
    return;
  };
  //заполняем данные
  mutex->lock();
  int i = 0;
  short* tmp = (short*)data.data();
  //берем данные и удаляем их
  for(i = 0; ((i * 2) < _countBytes) && (data.size() > (i * 2)); i ++){
    _data[i] = tmp[i];
    //testFile->write((char*)&_data[i],2);
  };
  _countBytes = i * 2;
  data.remove(0,_countBytes);
  mutex->unlock();
};

void QRecieveThread::release(){
  //удаляем буфер
  mutex->lock();
  data.clear();
  mutex->unlock();
};

bool QRecieveThread::isBufferEmpty(){
  return (data.size() > 0) ? true:false;
};

QRecieveThread::QRecieveThread(){
  mutex = new QMutex();
};

