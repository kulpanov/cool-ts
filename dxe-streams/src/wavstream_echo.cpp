/*
 * wavstream_echo.cpp
 *
 *  Created on: 12.03.2012
 *      Author: tsvet
 */
#include "wavstream_echo.h"
//////////////////////////////////////////////////////////////////////////////
//50 -max!
QByteArray* shared_buffer[50] = {NULL, };
QMutex* shared_mutex[50]= {NULL, };

//
//из make функции создать
CWavStream_shared::CWavStream_shared(int _pair_id)
:pair_id(_pair_id)
{
  if(NULL == shared_mutex[pair_id]){
    shared_mutex[pair_id] = new QMutex();
    shared_buffer[pair_id] = new QByteArray();
  }
  mutex = shared_mutex[pair_id];
  buffer = shared_buffer[pair_id];
}

CWavStream_shared::CWavStream_shared()
:pair_id(0)
{
  mutex = new QMutex();
  buffer = new QByteArray();
}

//480 байт
CPacket_RTP* CWavStream_shared::read(){
  mutex->lock();
  CPacket_RTP* rtp = NULL;
  if(shared_buffer[pair_id] != NULL){
    QByteArray data = buffer->left(rtp_size);
    buffer->remove(0,data.size());
    rtp = new CPacket_RTP(8, 0, 0, (uint8_t*)data.data(), data.size());
  }
  mutex->unlock();
  return rtp;
}

//запись 480
void CWavStream_shared::write(CPacket_RTP* _packet){
  mutex->lock();
  if(shared_buffer[pair_id] != NULL){
    buffer->append(
        (char*)_packet->getDataOfPayload(), _packet->getLengthOfPayload());
  }
  mutex->unlock();
}

//проверка конца потока
bool CWavStream_shared::isEndOfStream(){
  if(shared_buffer[pair_id] == NULL) return true;
  return (buffer->size() == 0);
}


IWavStream* CWavStream_shared::release(){
  close();
  mutex->lock();
  if(shared_buffer[pair_id] == NULL){
    delete shared_mutex[pair_id];
    shared_mutex[pair_id] = NULL;
    return NULL;
  }
  delete shared_buffer[pair_id];
  shared_buffer[pair_id] = NULL;
  mutex->unlock();
  return NULL;
}
