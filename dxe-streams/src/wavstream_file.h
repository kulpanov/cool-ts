#ifndef WAVSTREAM_FILE_H_
#define WAVSTREAM_FILE_H_

#include <QObject>
#include <QtMultimedia/QAudioInput>
#include <QtMultimedia/QAudioFormat>
#include <QIODevice>
#include <QtMultimedia/QAudioDeviceInfo>
#include <QDebug>
#include <QFile>
#include <QBuffer>
#include <QTimer>

#include <math.h>
#include <iostream>
#include "wavStreams.h"
#include "recievethread.h"
//#include "aLaw.h"

#define PI 3.14159265

using namespace std;

/**Класс формирующий RTP-поток из файла
 * Пример задания хар-к:
 * <item type="file_in" filename="test.wav">
 * Сформирует поток из файла с именем test.wav*/
class CWavStream_fin : public IWavStream
{
  private:
    QByteArray data;
    int position;
  protected:
    void GetHeaderInfo();
  public:
    //перейти в начало
    virtual void reset();
    //остановить
    virtual void close();
    //480 байт
    virtual CPacket_RTP* read();
    //проверка конца потока
    virtual bool isEndOfStream();
    //удаление буферов
    //virtual IWavStream* release();

  public:
    CWavStream_fin(QString _fileName);
    CWavStream_fin(const CWavStream_fin& _right);
};


/**Класс сохраняющий RTP-поток в файл
 * Пример задания хар-к:
 * <item type="file_out" filename="test.wav">
 * Сохранит поток в файл с именем test.wav*/
class CWavStream_fout : public IWavStream
{
  private:
    QString fileName;
    QByteArray data;
  protected:
    void SetHeaderInfo();
  public:
    //перейти в начало
    virtual void reset();
    //остановить
    virtual void close();
    //480 байт
    //virtual CPacket_RTP* read();
    //запись 480
    virtual void write(CPacket_RTP* _packet);
    //следующий элемент
    //virtual bool next();
    //проверка конца потока
    //virtual bool isEndOfStream();
    //удаление буферов
    //virtual IWavStream* release();

  public:
    CWavStream_fout(QString _fileName);
};

/**Класс сохраняющий RTP-поток в ОЗУ
 * Пример задания хар-к:
 * <item type="mem">
 * Сохранит поток в оперативной памяти для дальнейшего использования*/
class CWavStream_mem : public IWavStream
{
  private:
    QByteArray buffer;
  public:
    //перейти в начало
    virtual void reset();
    //остановить
    virtual void close();
    //480 байт
    virtual CPacket_RTP* read();
    //
    virtual void write(CPacket_RTP* _packet);
    //проверка конца потока
    virtual bool isEndOfStream();

    virtual IWavStream* release();

    virtual bool isFull(){
      return buffer.size() > 4000;
    }
  public:
    CWavStream_mem();
};


#endif // AUDIOINPUTSTREAM_H
