#ifndef WAVSTREAM_GEN_H_
#define WAVSTREAM_GEN_H_

#include <QObject>
#include <QAudioInput>
#include <QAudioFormat>
#include <QIODevice>
#include <QAudioDeviceInfo>
#include <QDebug>
#include <QFile>
#include <QBuffer>
#include <QTimer>

#include <math.h>
#include <iostream>
#include "wavStreams.h"
#include "recievethread.h"
//#include "aLaw.h"

#define PI 3.14159265
#define rtp_size  CPacket_RTP::length

using namespace std;

/**Поток генерирующий RTP-поток в виде синусоиды
 * Генерирует синус, с заданными характеристиками.
 * Пример задания хар-к:
 * <item type="sin" freq="440" ampl="-6">
 * Будет сгенерирован синус с частотой 440Гц, ампилтудой -6дБ */
class CWavStream_sin : public IWavStream
{
  private:
    double samplingRate;

    unsigned long IDWrittenSample;
    double amplitude;
    double omega;
  public:
//    //перейти в начало
//    virtual void reset();
//    //остановить
//    virtual void close();
    //480 байт
    virtual CPacket_RTP* read();
//    //запись 480
//    virtual void write(CPacket_RTP* _packet);
//    //следующий элемент
//    //virtual bool next();
//    //проверка конца потока
//    virtual bool isEndOfStream();
//    //удаление буферов
//    //virtual IWavStream* release();

  public:
    CWavStream_sin(int _frequency, double _amplitude);

   virtual ~CWavStream_sin();
};

//поток генерирующий text
class CWavStream_text : public IWavStream
{
  private:
    char* text;
  public:
    //перейти в начало
    //virtual void reset();
    //остановить
    //virtual void close();
    //480 байт
    virtual CPacket_RTP* read();
    //запись 480
    //virtual void write(CPacket_RTP* _packet);
    //следующий элемент
    //virtual bool next();
    //проверка конца потока
    //virtual bool isEndOfStream();
    //удаление буферов
    //virtual IWavStream* release();

  public:
    CWavStream_text(const char* _text);
};

//поток генерирующий синусоиду
class CWavStream_silence : public IWavStream
{
    virtual CPacket_RTP* read();
  public:
  CWavStream_silence();
};


#endif // AUDIOINPUTSTREAM_H
