#include "wavstream_gen.h"

//из make функции создать
CWavStream_sin::CWavStream_sin(int _frequency, double _amplitude)
{
  samplingRate = 8000;
  IDWrittenSample = 0;
  omega = 2 * PI * ((double)_frequency / samplingRate);
  double power = (double)_amplitude / 20;
  amplitude = pow(10, power) * 4031;//3956;//4036;//4031

  dB = _amplitude;
  freq = _frequency;
}

CWavStream_sin::~CWavStream_sin(){
}

//480 байт
CPacket_RTP* CWavStream_sin::read(){
  for(int i = 0; i < pcktSize; i++){
    //заполняем данные
    double value = sin(omega * IDWrittenSample++) * amplitude;
    data[i] = l2a(qRound(value));
  }
  return new CPacket_RTP(8, 0, 0, (uint8_t*)data, pcktSize);
}


////////////////////////////////////////////////////////////////////////////////
//из make функции создать
CWavStream_text::CWavStream_text(const char* _text)
{
  text = strdup(_text);
}

//480 байт
CPacket_RTP* CWavStream_text::read(){
  uint8_t data[CPacket_RTP::length];
  int offs = 0; int len = strlen(text);
  for(; offs<(CPacket_RTP::length - len); offs+=len){
    memcpy((void*)(data + offs), text, len);
  }

  uint8_t type = 8;
  //uint32_t len = CPacket_RTP::length;
  return new CPacket_RTP(type, 0, 0, data, offs);
};

////////////////////////////////////////////////////////////////////////////////
CWavStream_silence::CWavStream_silence(){
  dB = -60; freq = 0;
}

CPacket_RTP* CWavStream_silence::CWavStream_silence::read(){
  char data[rtp_size];
  for(int i = 0; i < pcktSize; i++){
    data[i] = l2a(0);
  }
  return new CPacket_RTP(8, 0, 0, (uint8_t*)data, pcktSize);
}
