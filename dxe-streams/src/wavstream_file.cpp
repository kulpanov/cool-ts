#include "wavstream_file.h"
#include <QtCore/QtCore>

#define SIZE_OF_HEADER  50//58-8

void CWavStream_fout::SetHeaderInfo(){
  int _sizeOfData = data.size();
  QByteArray title;

  title.append("RIFF");
  //размер секции
  int tmp_int = _sizeOfData + SIZE_OF_HEADER;
  title.append((char*) &tmp_int, 4) ;
  title.append("WAVE");
  title.append("fmt ");
  tmp_int = 18;
  title.append((char*) &tmp_int, 4) ;
  //кодировка
  short tmp_short = 6;
  title.append((char*) &tmp_short, 2) ;
  //каналы
  tmp_short = 1;
  title.append((char*) &tmp_short, 2) ;
  //sampleRate
  tmp_int = 8000;
  title.append((char*) &tmp_int, 4) ;
  //byteRate
  tmp_int = 8000;
  title.append((char*) &tmp_int, 4) ;
  //blockalign
  tmp_short = 1;
  title.append((char*) &tmp_short, 2) ;
  //bitpersample
  tmp_int = 8;
  title.append((char*) &tmp_int, 4) ;

  title.append("fact") ;
  title.append((char*)&(tmp_int = 4), 4) ;
  title.append((char*)&(tmp_int = 0), 4) ;

  title.append("data") ;

  title.append((char*)&(tmp_int = _sizeOfData), 4) ;

  data.insert(0, title);
};

//из make функции создать
CWavStream_fout::CWavStream_fout(QString _fileName)
:fileName(_fileName)
{
  if(QDir::isAbsolutePath(_fileName))
    fileName = _fileName;//QString::fromUtf8(_fname);
  else{
    fileName = QString().fromUtf8(IWavStream::wavsPath.c_str()) + "/" +_fileName;//QString::fromUtf8(_fname);
  }
  LOG1(TSTL1, "CWavStream_fout::CWavStream_fout: %s\n", fileName.toUtf8().begin());
//  QFile file(fileName);
//  if (!file.open(QIODevice::WriteOnly))
//      throw Ev(file.error(), "Не могу открыть файл на запись:%s", fileName.toUtf8().begin());
}

//перейти в начало
void CWavStream_fout::reset(){
  //закрываем
  close();
  //перевзводим
  data.remove(0, data.size());
};

////480 байт
//CPacket_RTP* QAudioOutputStream_File::read(){
//  return NULL;
//};

//запись 480
void CWavStream_fout::write(CPacket_RTP* _packet){
  data.append(
      (char*)_packet->getDataOfPayload(),_packet->getLengthOfPayload());
//  if(_packet->getLengthOfPayload()<100)
//  LOG2(DEVL3, "stream:fout:write sz=%d,packet=%d\n", data.size(), _packet->getLengthOfPayload());
}

// //следующий элемент
//bool QAudioOutputStream_File::next(){
//  return false;
//};
//
//проверка конца потока
//bool QAudioOutputStream_File::isEndOfStream(){
//  return false;
//};
//
// //удаление всего
//IWavStream* QAudioOutputStream_File::release(){
//  close();
//  //file->remove();
//  return IWavStream::release();
//};

void CWavStream_fout::close(){
  LOG1(DEVL2, "stream:fout: sz=%d\n", data.size());
  if(data.size() == 0 && QFile::exists(fileName)){
    LOG1(WARN, "stream:fout: sz=%d, szIsZero\n", data.size());
    QFile file(fileName);
    if (file.open(QIODevice::Append)){
      file.write("0", 1);
      file.close();
    }
    return ;
  }
  //QFile::remove(fileName);
  if(fileName.indexOf(".raw") == -1){
    SetHeaderInfo();
  }
  QFile file(fileName);
  if (file.open(QIODevice::WriteOnly)){
    file.write(data.data(),data.size());
    file.close();
  }
}

//////////////////////////////////////////////////////////////////////////////
//
//из make функции создать
CWavStream_fin::CWavStream_fin(QString _fileName)
  :position(0)
{
  if(! QDir::isAbsolutePath(_fileName)){
    _fileName = QString().fromUtf8(IWavStream::wavsPath.c_str()) + "/" +_fileName;//QString::fromUtf8(_fname);
  }
  LOG1(TSTL1, "CWavStream_fin::CWavStream_fin: %s\n", _fileName.toUtf8().begin());

  QFile file(_fileName);
  if (!file.open(QIODevice::ReadOnly))
    throw Ev(file.error(), "Не могу открыть файл на чтение:%s", _fileName.toUtf8().begin());

  data = file.readAll();
  if(_fileName.indexOf(".raw") == -1){
    //GetHeaderInfo(file);
    data.remove(0, 58);//sizeof(tVoiceMsg::wavHeader));
  }
}

CWavStream_fin::CWavStream_fin(const CWavStream_fin& _right)
  :data(_right.data), position(0){
}

//перейти в начало
void CWavStream_fin::reset(){
  position = 0;
}

//480 байт
CPacket_RTP* CWavStream_fin::read(){
  ASSERT(data.size() > position);
  QByteArray buff = data.mid(position, pcktSize);

  if(buff.length() < pcktSize){
    QByteArray filledBA(pcktSize - buff.length(), 0xD5);//i2a(0), silence value of A-law
    buff.append(filledBA);
  }
  const uint8_t* dta = static_cast<const uint8_t*>(static_cast<const void*>(buff.constData()));
  CPacket_RTP* packet = new CPacket_RTP(8, 0, 0, dta, buff.length());
  position +=  buff.length();
  return packet;
}

//проверка конца потока
bool CWavStream_fin::isEndOfStream(){
  return position>=data.size();
}


void CWavStream_fin::close(){
}

////////////////////////////////////////////////////////////////////////////////
//из make функции создать
CWavStream_mem::CWavStream_mem()
{
//
}

//перейти в начало
void CWavStream_mem::reset(){
}

////480 байт
//CPacket_RTP* QAudioOutputStream_File::read(){
//  return NULL;
//};

//запись 480
void CWavStream_mem::write(CPacket_RTP* _packet){
  buffer.append(
      (char*)_packet->getDataOfPayload(),_packet->getLengthOfPayload());
 // LOG1(INFO, " %d", buffer.size());
}

//480 байт
CPacket_RTP* CWavStream_mem::read(){
  QByteArray data = buffer.left(pcktSize);
  buffer.remove(0, data.size());
  return new CPacket_RTP(8, 0, 0, (uint8_t*)data.data(), data.size());
}

void CWavStream_mem::close(){
}

//проверка конца потока
bool CWavStream_mem::isEndOfStream(){
  return (buffer.size() == 0);
}

IWavStream* CWavStream_mem::release(){
  return NULL;
}

