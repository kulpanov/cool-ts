/*
 * wavstream_echo.h
 *
 *  Created on: 12.03.2012
 *      Author: tsvet
 */

#ifndef WAVSTREAM_ECHO_H_
#define WAVSTREAM_ECHO_H_

#include <QObject>
#include <QBuffer>
#include <QMutex>

#include "wavStreams.h"
#include "recievethread.h"

#define rtp_size  CPacket_RTP::length

//поток читающий данные из файла
class CWavStream_shared : public QObject, public IWavStream
{
  Q_OBJECT
  private:
    QByteArray* buffer;
    QMutex* mutex;
    int pair_id;
  public:
    //480 байт
    virtual CPacket_RTP* read();
    //запись 480
    virtual void write(CPacket_RTP* _packet);
    //проверка конца потока
    virtual bool isEndOfStream();

    virtual IWavStream* release();

  public:
    CWavStream_shared(int _pair_id);
    CWavStream_shared();
};


#endif /* WAVSTREAM_ECHO_H_ */
